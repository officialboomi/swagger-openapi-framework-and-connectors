# Google Big Query API
This connector supports the REST API for Google Big Query, a data platform for customers to create, manage, share and query data.m

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string



#### Google Big Query API REST API Service URL

The URL for the Google Big Query API REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### AuthenticationType

Allows user to select between BASIC and OAUTH 2.0 Authentication

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| CancelProjectsJobsCancelByProjectIdAndJobId - CancelProjectsJobsCancelByProjectIdAndJobId | jobs Requests that a job be cancelled. This call will return immediately, and the client will need to poll for the job status to see if the cancel completed successfully. Cancelled jobs may still incur costs. CancelProjectsJobsCancelByProjectIdAndJobId  |
| GetIamPolicyGetIamPolicyByResource - GetIamPolicyGetIamPolicyByResource | tables Gets the access control policy for a resource. Returns an empty policy if the resource exists and does not have a policy set. GetIamPolicyGetIamPolicyByResource  |
| InsertAllProjectsDatasetsTablesTableIdInsertAll - InsertAllProjectsDatasetsTablesTableIdInsertAll | tabledata Streams data into BigQuery one record at a time without needing to run a load job. Requires the WRITER dataset role. InsertAllProjectsDatasetsTablesTableIdInsertAll  |
| InsertProjectsDatasetsByProjectId - InsertProjectsDatasetsByProjectId | datasets Creates a new empty dataset. InsertProjectsDatasetsByProjectId  |
| InsertProjectsDatasetsRoutinesByProjectId - InsertProjectsDatasetsRoutinesByProjectId | routines Creates a new routine in the dataset. InsertProjectsDatasetsRoutinesByProjectId  |
| InsertProjectsDatasetsTablesByProjectId - InsertProjectsDatasetsTablesByProjectId | tables Creates a new, empty table in the dataset. InsertProjectsDatasetsTablesByProjectId  |
| InsertProjectsJobsByProjectId - InsertProjectsJobsByProjectId | jobs Starts a new asynchronous job. Requires the Can View project role. InsertProjectsJobsByProjectId  |
| QueryProjectsQueriesByProjectId - QueryProjectsQueriesByProjectId | jobs Runs a BigQuery SQL query synchronously and returns query results if the query completes within a specified timeout. QueryProjectsQueriesByProjectId  |
| SetIamPolicySetIamPolicyByResource - SetIamPolicySetIamPolicyByResource | tables Sets the access control policy on the specified resource. Replaces any existing policy. Can return `NOT_FOUND`, `INVALID_ARGUMENT`, and `PERMISSION_DENIED` errors. SetIamPolicySetIamPolicyByResource  |
| TestIamPermissionsTestIamPermissionsByResource - TestIamPermissionsTestIamPermissionsByResource | tables Returns permissions that a caller has on the specified resource. If the resource does not exist, this will return an empty set of permissions, not a `NOT_FOUND` error. Note: This operation is designed to be used for building permission-aware UIs and command-line tools, not for authorization checking. This operation may "fail open" without warning. TestIamPermissionsTestIamPermissionsByResource  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| DeleteProjectsDatasetsByProjectIdAndDatasetId - DeleteProjectsDatasetsByProjectIdAndDatasetId | datasets Deletes the dataset specified by the datasetId value. Before you can delete a dataset, you must delete all its tables, either manually or by specifying deleteContents. Immediately after deletion, you can create another dataset with the same name. DeleteProjectsDatasetsByProjectIdAndDatasetId  |
| DeleteProjectsDatasetsModelsModelIdByProjectId - DeleteProjectsDatasetsModelsModelIdByProjectId | models Deletes the model specified by modelId from the dataset. DeleteProjectsDatasetsModelsModelIdByProjectId  |
| DeleteProjectsDatasetsRoutinesRoutineId - DeleteProjectsDatasetsRoutinesRoutineId | routines Deletes the routine specified by routineId from the dataset. DeleteProjectsDatasetsRoutinesRoutineId  |
| DeleteProjectsDatasetsTablesTableIdByProjectId - DeleteProjectsDatasetsTablesTableIdByProjectId | tables Deletes the table specified by tableId from the dataset. If the table contains data, all the data will be deleted. DeleteProjectsDatasetsTablesTableIdByProjectId  |
| DeleteProjectsJobsDeleteByProjectIdAndJobId - DeleteProjectsJobsDeleteByProjectIdAndJobId | jobs Requests that a job is deleted. This call will return when the job is deleted. This method is available in limited preview. DeleteProjectsJobsDeleteByProjectIdAndJobId  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| GetProjectsDatasetsByProjectIdAndDatasetId - GetProjectsDatasetsByProjectIdAndDatasetId | datasets Returns the dataset specified by datasetID. GetProjectsDatasetsByProjectIdAndDatasetId  |
| GetProjectsDatasetsModelsModelIdByProjectId - GetProjectsDatasetsModelsModelIdByProjectId | models Gets the specified model resource by model ID. GetProjectsDatasetsModelsModelIdByProjectId  |
| GetProjectsDatasetsRoutinesRoutineId - GetProjectsDatasetsRoutinesRoutineId | routines Gets the specified routine resource by routine ID. GetProjectsDatasetsRoutinesRoutineId  |
| GetProjectsDatasetsTablesTableIdByProjectId - GetProjectsDatasetsTablesTableIdByProjectId | tables Gets the specified table resource by table ID. This method does not return the data in the table, it only returns the table resource, which describes the structure of this table. GetProjectsDatasetsTablesTableIdByProjectId  |
| GetProjectsJobsByProjectIdAndJobId - GetProjectsJobsByProjectIdAndJobId | jobs Returns information about a specific job. Job information is available for a six month period after creation. Requires that you're the person who ran the job, or have the Is Owner project role. GetProjectsJobsByProjectIdAndJobId  |
| GetServiceAccountProjectsServiceAccountByProjectId - GetServiceAccountProjectsServiceAccountByProjectId | projects Returns the email address of the service account for your project used for interactions with Google Cloud KMS. GetServiceAccountProjectsServiceAccountByProjectId  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| GetQueryResultsProjectsQueriesByProjectIdAndJobId - GetQueryResultsProjectsQueriesByProjectIdAndJobId | jobs Retrieves the results of a query job. GetQueryResultsProjectsQueriesByProjectIdAndJobId  |
| ListProjects - ListProjects | projects Lists all projects to which you have been granted any project role. ListProjects  |
| ListProjectsDatasetsByProjectId - ListProjectsDatasetsByProjectId | datasets Lists all datasets in the specified project to which you have been granted the READER dataset role. ListProjectsDatasetsByProjectId  |
| ListProjectsDatasetsModelsByProjectId - ListProjectsDatasetsModelsByProjectId | models Lists all models in the specified dataset. Requires the READER dataset role. After retrieving the list of models, you can get information about a particular model by calling the models.get method. ListProjectsDatasetsModelsByProjectId  |
| ListProjectsDatasetsRoutinesByProjectId - ListProjectsDatasetsRoutinesByProjectId | routines Lists all routines in the specified dataset. Requires the READER dataset role. ListProjectsDatasetsRoutinesByProjectId  |
| ListProjectsDatasetsTablesByProjectId - ListProjectsDatasetsTablesByProjectId | tables Lists all tables in the specified dataset. Requires the READER dataset role. ListProjectsDatasetsTablesByProjectId  |
| ListProjectsDatasetsTablesTableIdData - ListProjectsDatasetsTablesTableIdData | tabledata Retrieves table data from a specified set of rows. Requires the READER dataset role. ListProjectsDatasetsTablesTableIdData  |
| ListProjectsDatasetsTablesTableIdRowAccessPolicies - ListProjectsDatasetsTablesTableIdRowAccessPolicies | rowAccessPolicies Lists all row access policies on the specified table. ListProjectsDatasetsTablesTableIdRowAccessPolicies  |
| ListProjectsJobsByProjectId - ListProjectsJobsByProjectId | jobs Lists all jobs that you started in the specified project. Job information is available for a six month period after creation. The job list is sorted in reverse chronological order, by job creation time. Requires the Can View project role, or the Is Owner project role if you set the allUsers property. ListProjectsJobsByProjectId  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| PatchProjectsDatasetsByProjectIdAndDatasetId - PatchProjectsDatasetsByProjectIdAndDatasetId (PATCH) | datasets Updates information in an existing dataset. The update method replaces the entire dataset resource, whereas the patch method only replaces fields that are provided in the submitted dataset resource. This method supports patch semantics. PatchProjectsDatasetsByProjectIdAndDatasetId  |
| PatchProjectsDatasetsModelsModelIdByProjectId - PatchProjectsDatasetsModelsModelIdByProjectId (PATCH) | models Patch specific fields in the specified model. PatchProjectsDatasetsModelsModelIdByProjectId  |
| PatchProjectsDatasetsTablesTableIdByProjectId - PatchProjectsDatasetsTablesTableIdByProjectId (PATCH) | tables Updates information in an existing table. The update method replaces the entire table resource, whereas the patch method only replaces fields that are provided in the submitted table resource. This method supports patch semantics. PatchProjectsDatasetsTablesTableIdByProjectId  |
| UpdateProjectsDatasetsByProjectIdAndDatasetId - UpdateProjectsDatasetsByProjectIdAndDatasetId (PUT) | datasets Updates information in an existing dataset. The update method replaces the entire dataset resource, whereas the patch method only replaces fields that are provided in the submitted dataset resource. UpdateProjectsDatasetsByProjectIdAndDatasetId  |
| UpdateProjectsDatasetsRoutinesRoutineId - UpdateProjectsDatasetsRoutinesRoutineId (PUT) | routines Updates information in an existing routine. The update method replaces the entire Routine resource. UpdateProjectsDatasetsRoutinesRoutineId  |
| UpdateProjectsDatasetsTablesTableIdByProjectId - UpdateProjectsDatasetsTablesTableIdByProjectId (PUT) | tables Updates information in an existing table. The update method replaces the entire table resource, whereas the patch method only replaces fields that are provided in the submitted table resource. UpdateProjectsDatasetsTablesTableIdByProjectId  |


