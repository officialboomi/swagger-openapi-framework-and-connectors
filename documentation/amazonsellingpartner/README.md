# Amazon Selling Partner API
This contector enables Boomi developers to build processes that call the Selling Partner APIs.  To get Selling Partner API sandbox endpoints for each regions, refer to SP-API sandbox endpoints.

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string

**Default Value** - https://sellingpartnerapi-na.amazon.com

##### Allowed Values

 * North America (Canada, US, Mexico, and Brazil marketplaces)
 * Europe (Spain, UK, France, Netherlands, Germany, Italy, Sweden, Poland, Egypt, Turkey, United Arab Emirates, and India marketplaces)
 * Far East (Singapore, Australia, and Japan marketplaces)
 * North America Sandbox


#### OAuth 2.0 Client ID

OAuth Client ID for refresh token grant type.  You must have the client_id to request an LWA access token. The client_id appears after you register your app. To get this value, refer to Viewing your developer information.

**Type** - string



#### OAuth 2.0 Client Secret

You must have the client_secret to request an LWA access token. The client_secret appears after you register your app. To get this value, refer to Viewing your developer information.

**Type** - password



#### OAuth 2.0 Refresh Token

The LWA refresh token, get this value when the selling partner authorizes your application. If you have your own Selling Partner account, you can self authorize your application to get the refresh token. For more information, refer to Authorizing Selling Partner API applications.

**Type** - password



#### Scope

OAuth Scope

**Type** - string

**Default Value** - sellingpartnerapi::migration



#### Access Key

Access keys are long-term credentials for an IAM user, obtained just after creating an IAM user while configuring your IAM policies and entities for SP-API. https://developer-docs.amazon.com/sp-api/docs/creating-and-configuring-iam-policies-and-entities#step-2-create-an-iam-user https://developer-docs.amazon.com/sp-api/docs/creating-and-configuring-iam-policies-and-entities

**Type** - password



#### Secret Key

 Access keys are long-term credentials for an IAM user, obtained just after creating an IAM user while configuring your IAM policies and entities for SP-API.

**Type** - password



#### Region

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string



#### Use temporary credentials

Use STS (Secure Token Service) to get temporary credentials

**Type** - boolean

**Default Value** - true



#### IAM User

Identity and Access Management (IAM) User for getting STS Temporary Credentials

**Type** - string



#### API Role

Role for getting STS Temporary Credential

**Type** - string

**Default Value** - SellingPartnerAPIRole



#### STS Region

Override the region for executing STS transaction to get temporary token. Otherwise the region value above is used.

**Type** - string



#### Sleep Time (ms)

The amount of time in milliseconds to wait between API calls. This is to avoid Amazon maximum rate limit throttling violations.

**Type** - integer

**Default Value** - 0

# Operation Tab


## POST
### Operation Fields


##### Amazon Selling Partner REST API Service Specification URL

The URL for the Amazon Selling Partner API REST API Service server.

**Type** - string

###### Allowed Values

 * Catalog Items 2020-12-01
 * Catalog Items 2022-04-01
 * FBA Inbound Eligibilty
 * Feeds 2021-06-30
 * Finances
 * Fulfillment Inbound
 * Fulfillment Outbound 2020-07-01
 * Listings Items 2021-08-01
 * Merchant Fulfillment
 * Orders
 * Reports 2021-06-30
 * Sales
 * Sellers
 * Shipment Invoicing
 * Shipping


##### Alternate Service Specification URL

This will override the default swagger file embedded in the connector so you can provide a url to any Swagger/OAS file.

**Type** - string



## PUT
### Operation Fields


##### Amazon Selling Partner REST API Service Specification URL

The URL for the Amazon Selling Partner API REST API Service server.

**Type** - string

###### Allowed Values

 * Catalog Items 2020-12-01
 * Catalog Items 2022-04-01
 * FBA Inbound Eligibilty
 * Feeds 2021-06-30
 * Finances
 * Fulfillment Inbound
 * Fulfillment Outbound 2020-07-01
 * Listings Items 2021-08-01
 * Merchant Fulfillment
 * Orders
 * Reports 2021-06-30
 * Sales
 * Sellers
 * Shipment Invoicing
 * Shipping


##### Alternate Service Specification URL

This will override the default swagger file embedded in the connector so you can provide a url to any Swagger/OAS file.

**Type** - string



## GET
### Operation Fields


##### Amazon Selling Partner REST API Service Specification URL

The URL for the Amazon Selling Partner API REST API Service server.

**Type** - string

###### Allowed Values

 * Catalog Items 2020-12-01
 * Catalog Items 2022-04-01
 * FBA Inbound Eligibilty
 * Feeds 2021-06-30
 * Finances
 * Fulfillment Inbound
 * Fulfillment Outbound 2020-07-01
 * Listings Items 2021-08-01
 * Merchant Fulfillment
 * Orders
 * Reports 2021-06-30
 * Sales
 * Sellers
 * Shipment Invoicing
 * Shipping


##### Alternate Service Specification URL

This will override the default swagger file embedded in the connector so you can provide a url to any Swagger/OAS file.

**Type** - string



##### Use Restricted Data Token

Generate and use an access token to access restricted Pii data. https://developer-docs.amazon.com/sp-api/docs/tokens-api-use-case-guide

**Type** - boolean



##### Restricted Data Elements

A comma delimited list of the restricted data elements to access. For example: buyerInfo,shippingAddress

**Type** - string



##### Target Application

The application ID for the target application to which access is being delegated. For example: amz1.sellerapps.app.target-application

**Type** - string



##### Use Record Specific RDT

Create an RDT for specific record IDs in the path. For example /orders/v0/orders/123-1234567-1234567 or /orders/v0/orders/123-1234567-1234567/orderItems. Otherwise a generic RDT will be created and reused. For example /mfn/v0/shipments/{shipmentId}

**Type** - boolean



## DELETE
### Operation Fields


##### Amazon Selling Partner REST API Service Specification URL

The URL for the Amazon Selling Partner API REST API Service server.

**Type** - string

###### Allowed Values

 * Catalog Items 2020-12-01
 * Catalog Items 2022-04-01
 * FBA Inbound Eligibilty
 * Feeds 2021-06-30
 * Finances
 * Fulfillment Inbound
 * Fulfillment Outbound 2020-07-01
 * Listings Items 2021-08-01
 * Merchant Fulfillment
 * Orders
 * Reports 2021-06-30
 * Sales
 * Sellers
 * Shipment Invoicing
 * Shipping


##### Alternate Service Specification URL

This will override the default swagger file embedded in the connector so you can provide a url to any Swagger/OAS file.

**Type** - string



## QUERY
### Operation Fields


##### Amazon Selling Partner REST API Service Specification URL

The URL for the Amazon Selling Partner API REST API Service server.

**Type** - string

###### Allowed Values

 * Catalog Items 2020-12-01
 * Catalog Items 2022-04-01
 * FBA Inbound Eligibilty
 * Feeds 2021-06-30
 * Finances
 * Fulfillment Inbound
 * Fulfillment Outbound 2020-07-01
 * Listings Items 2021-08-01
 * Merchant Fulfillment
 * Orders
 * Reports 2021-06-30
 * Sales
 * Sellers
 * Shipment Invoicing
 * Shipping


##### Alternate Service Specification URL

This will override the default swagger file embedded in the connector so you can provide a url to any Swagger/OAS file.

**Type** - string



##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Use Restricted Data Token

Generate and use an access token to access restricted Pii data. https://developer-docs.amazon.com/sp-api/docs/tokens-api-use-case-guide

**Type** - boolean



##### Restricted Data Elements *

A comma delimited list of the restricted data elements to access. For example: buyerInfo,shippingAddress

**Type** - string



##### Target Application

The application ID for the target application to which access is being delegated. For example: amz1.sellerapps.app.target-application

**Type** - string


### Query Options


#### Fields

The connector does not support field selection. All fields will be returned by default.


#### Filters

The query filter supports any number of non-nested expressions which will be AND'ed together.

Example:
((foo lessThan 30) AND (baz lessThan 42))

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts




# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


## Object Operations Provided

| Object | Query | Get | Create | Update | Delete 
| --- |:---:|:---:|:---:|:---:|:---:|
| fbaInbound | X |  |  |  |  |
| feeds_2021-06-30 | X | X | X |  | X |
| financesV0 | X |  |  |  |  |
| fulfillmentInboundV0 | X |  | X | X |  |
| fulfillmentOutbound_2020-07-01 | X | X | X | X |  |
| listingsItems_2021-08-01 |  | X |  | X | X |
| merchantFulfillmentV0 |  | X | X | X | X |
| openapi | X | X | X | X | X |
| ordersV0 | X | X | X | X |  |
| reports_2021-06-30 | X | X | X |  | X |
| sales | X |  |  |  |  |
| sellers | X |  |  |  |  |
| shipmentInvoicingV0 | X | X | X |  |  |
| shipping | X | X | X |  |  |

# 


# Operations and Object Types Provided

### DELETE Operation Types
| Label | Help Text |
| --- | --- |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| GetOrder - Returns the order that you specify.  **Usage Plan:... | ordersV0 Returns the order that you specify.  **Usage Plan:**    Rate (requests per second)   Burst     ----   ----     0.0167   20    The `x-amzn-RateLimit-Limit` response header returns the usage plan rate limits that were applied to the requested operation, when available. The table above indicates the default rate and burst values for this operation. Selling partners whose business demands require higher throughput may see higher rate and burst values then those shown here. For more information, see [Usage Plans and Rate Limits in the Selling Partner API](doc:usage-plans-and-rate-limits-in-the-sp-api).  |
| GetOrderAddress - Returns the shipping address for the order that yo... | ordersV0 Returns the shipping address for the order that you specify.  **Usage Plan:**    Rate (requests per second)   Burst     ----   ----     0.0167   20    The `x-amzn-RateLimit-Limit` response header returns the usage plan rate limits that were applied to the requested operation, when available. The table above indicates the default rate and burst values for this operation. Selling partners whose business demands require higher throughput may see higher rate and burst values then those shown here. For more information, see [Usage Plans and Rate Limits in the Selling Partner API](doc:usage-plans-and-rate-limits-in-the-sp-api).  |
| GetOrderBuyerInfo - Returns buyer information for the order that you s... | ordersV0 Returns buyer information for the order that you specify.  **Usage Plan:**    Rate (requests per second)   Burst     ----   ----     0.0167   20    The `x-amzn-RateLimit-Limit` response header returns the usage plan rate limits that were applied to the requested operation, when available. The table above indicates the default rate and burst values for this operation. Selling partners whose business demands require higher throughput may see higher rate and burst values then those shown here. For more information, see [Usage Plans and Rate Limits in the Selling Partner API](doc:usage-plans-and-rate-limits-in-the-sp-api).  |
| GetOrderItems - Returns detailed order item information for the or... | ordersV0 Returns detailed order item information for the order that you specify. If NextToken is provided, it's used to retrieve the next page of order items.  __Note__: When an order is in the Pending state (the order has been placed but payment has not been authorized), the getOrderItems operation does not return information about pricing, taxes, shipping charges, gift status or promotions for the order items in the order. After an order leaves the Pending state (this occurs when payment has been authorized) and enters the Unshipped, Partially Shipped, or Shipped state, the getOrderItems operation returns information about pricing, taxes, shipping charges, gift status and promotions for the order items in the order.  **Usage Plan:**    Rate (requests per second)   Burst     ----   ----     0.5   30    The `x-amzn-RateLimit-Limit` response header returns the usage plan rate limits that were applied to the requested operation, when available. The table above indicates the default rate and burst values for this operation. Selling partners whose business demands require higher throughput may see higher rate and burst values then those shown here. For more information, see [Usage Plans and Rate Limits in the Selling Partner API](doc:usage-plans-and-rate-limits-in-the-sp-api).  |
| GetOrderItemsApprovals - Returns detailed order items approvals information... | approvals Returns detailed order items approvals information for the order specified. If NextToken is provided, it's used to retrieve the next page of order items approvals.  **Usage Plans:**    Plan type   Rate (requests per second)   Burst     ----   ----   ----    Default  0.5   30    Selling partner specific  Variable   Variable    The x-amzn-RateLimit-Limit response header returns the usage plan rate limits that were applied to the requested operation. Rate limits for some selling partners will vary from the default rate and burst shown in the table above. For more information, see "Usage Plans and Rate Limits" in the Selling Partner API documentation.  |
| GetOrderItemsBuyerInfo - Returns buyer information for the order items in t... | ordersV0 Returns buyer information for the order items in the order that you specify.  **Usage Plan:**    Rate (requests per second)   Burst     ----   ----     0.5   30    The `x-amzn-RateLimit-Limit` response header returns the usage plan rate limits that were applied to the requested operation, when available. The table above indicates the default rate and burst values for this operation. Selling partners whose business demands require higher throughput may see higher rate and burst values then those shown here. For more information, see [Usage Plans and Rate Limits in the Selling Partner API](doc:usage-plans-and-rate-limits-in-the-sp-api).  |
| GetOrderRegulatedInfo - Returns regulated information for the order that y... | ordersV0 Returns regulated information for the order that you specify.  **Usage Plan:**    Rate (requests per second)   Burst     ----   ----     0.5   30    The `x-amzn-RateLimit-Limit` response header returns the usage plan rate limits that were applied to the requested operation, when available. The table above indicates the default rate and burst values for this operation. Selling partners whose business demands require higher throughput may see higher rate and burst values then those shown here. For more information, see [Usage Plans and Rate Limits in the Selling Partner API](doc:usage-plans-and-rate-limits-in-the-sp-api).  |
| GetOrders - Returns orders created or updated during the time ... | ordersV0 Returns orders created or updated during the time frame indicated by the specified parameters. You can also apply a range of filtering criteria to narrow the list of orders returned. If NextToken is present, that will be used to retrieve the orders instead of other criteria.  **Usage Plan:**    Rate (requests per second)   Burst     ----   ----     0.0167   20    The `x-amzn-RateLimit-Limit` response header returns the usage plan rate limits that were applied to the requested operation, when available. The table above indicates the default rate and burst values for this operation. Selling partners whose business demands require higher throughput may see higher rate and burst values then those shown here. For more information, see [Usage Plans and Rate Limits in the Selling Partner API](doc:usage-plans-and-rate-limits-in-the-sp-api).  |


### PATCH Operation Types
| Label | Help Text |
| --- | --- |
| UpdateVerificationStatus - Updates (approves or rejects) the verification sta... (PATCH) | ordersV0 Updates (approves or rejects) the verification status of an order containing regulated products.  **Usage Plan:**    Rate (requests per second)   Burst     ----   ----     0.5   30    The `x-amzn-RateLimit-Limit` response header returns the usage plan rate limits that were applied to the requested operation, when available. The table above indicates the default rate and burst values for this operation. Selling partners whose business demands require higher throughput may see higher rate and burst values then those shown here. For more information, see [Usage Plans and Rate Limits in the Selling Partner API](doc:usage-plans-and-rate-limits-in-the-sp-api).  |


### POST Operation Types
| Label | Help Text |
| --- | --- |
| ConfirmShipment - Updates the shipment confirmation status for a spe... | ordersV0 Updates the shipment confirmation status for a specified order.  **Usage Plan:**    Rate (requests per second)   Burst     ----   ----     2   10    The `x-amzn-RateLimit-Limit` response header returns the usage plan rate limits that were applied to the requested operation, when available. The table above indicates the default rate and burst values for this operation. Selling partners whose business demands require higher throughput may see higher rate and burst values then those shown here. For more information, see [Usage Plans and Rate Limits in the Selling Partner API](doc:usage-plans-and-rate-limits-in-the-sp-api).  |
| UpdateOrderItemsApprovals - Update the order items approvals for an order that... | approvals Update the order items approvals for an order that you specify.  **Usage Plan:**    Rate (requests per second)   Burst     ----   ----     5   15    The `x-amzn-RateLimit-Limit` response header returns the usage plan rate limits that were applied to the requested operation, when available. The table above indicates the default rate and burst values for this operation. Selling partners whose business demands require higher throughput may see higher rate and burst values then those shown here. For more information, see [Usage Plans and Rate Limits in the Selling Partner API](doc:usage-plans-and-rate-limits-in-the-sp-api).  |
| UpdateShipmentStatus - Update the shipment status for an order that you s... | shipment Update the shipment status for an order that you specify.  **Usage Plan:**    Rate (requests per second)   Burst     ----   ----     5   15    The `x-amzn-RateLimit-Limit` response header returns the usage plan rate limits that were applied to the requested operation, when available. The table above indicates the default rate and burst values for this operation. Selling partners whose business demands require higher throughput may see higher rate and burst values then those shown here. For more information, see [Usage Plans and Rate Limits in the Selling Partner API](doc:usage-plans-and-rate-limits-in-the-sp-api).  |


### PUT Operation Types
| Label | Help Text |
| --- | --- |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| GetOrderItems - Returns detailed order item information for the or... | ordersV0 Returns detailed order item information for the order that you specify. If NextToken is provided, it's used to retrieve the next page of order items.  __Note__: When an order is in the Pending state (the order has been placed but payment has not been authorized), the getOrderItems operation does not return information about pricing, taxes, shipping charges, gift status or promotions for the order items in the order. After an order leaves the Pending state (this occurs when payment has been authorized) and enters the Unshipped, Partially Shipped, or Shipped state, the getOrderItems operation returns information about pricing, taxes, shipping charges, gift status and promotions for the order items in the order.  **Usage Plan:**    Rate (requests per second)   Burst     ----   ----     0.5   30    The `x-amzn-RateLimit-Limit` response header returns the usage plan rate limits that were applied to the requested operation, when available. The table above indicates the default rate and burst values for this operation. Selling partners whose business demands require higher throughput may see higher rate and burst values then those shown here. For more information, see [Usage Plans and Rate Limits in the Selling Partner API](doc:usage-plans-and-rate-limits-in-the-sp-api).  |
| GetOrderItemsApprovals - Returns detailed order items approvals information... | approvals Returns detailed order items approvals information for the order specified. If NextToken is provided, it's used to retrieve the next page of order items approvals.  **Usage Plans:**    Plan type   Rate (requests per second)   Burst     ----   ----   ----    Default  0.5   30    Selling partner specific  Variable   Variable    The x-amzn-RateLimit-Limit response header returns the usage plan rate limits that were applied to the requested operation. Rate limits for some selling partners will vary from the default rate and burst shown in the table above. For more information, see "Usage Plans and Rate Limits" in the Selling Partner API documentation.  |
| GetOrderItemsBuyerInfo - Returns buyer information for the order items in t... | ordersV0 Returns buyer information for the order items in the order that you specify.  **Usage Plan:**    Rate (requests per second)   Burst     ----   ----     0.5   30    The `x-amzn-RateLimit-Limit` response header returns the usage plan rate limits that were applied to the requested operation, when available. The table above indicates the default rate and burst values for this operation. Selling partners whose business demands require higher throughput may see higher rate and burst values then those shown here. For more information, see [Usage Plans and Rate Limits in the Selling Partner API](doc:usage-plans-and-rate-limits-in-the-sp-api).  |
| GetOrders - Returns orders created or updated during the time ... | ordersV0 Returns orders created or updated during the time frame indicated by the specified parameters. You can also apply a range of filtering criteria to narrow the list of orders returned. If NextToken is present, that will be used to retrieve the orders instead of other criteria.  **Usage Plan:**    Rate (requests per second)   Burst     ----   ----     0.0167   20    The `x-amzn-RateLimit-Limit` response header returns the usage plan rate limits that were applied to the requested operation, when available. The table above indicates the default rate and burst values for this operation. Selling partners whose business demands require higher throughput may see higher rate and burst values then those shown here. For more information, see [Usage Plans and Rate Limits in the Selling Partner API](doc:usage-plans-and-rate-limits-in-the-sp-api).  |


