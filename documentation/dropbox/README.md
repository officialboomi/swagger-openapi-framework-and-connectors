# Dropbox REST API Connector
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### Dropbox Service Definition

The URL for the Cybersource descriptor file

**Type** - string

##### Allowed Values

 * Team
 * User


#### Service URL

The server name for the CyberSource service

**Type** - string

**Default Value** - https://api.cybersource.com



#### OAuth 2.0

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - oauth

# Operation Tab


## EXECUTE
# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| AccountCustomFields PostAccountCustomFields - Creates an account custom field. | Creates an account custom field. | /v2.1/accounts/{accountId}/custom\_fields\_\_\_post |
| Accounts PostAccounts - Creates new accounts. | Creates new accounts. | /v2.1/accounts\_\_\_post |
| AccountSignatures PostAccountSignatures -  |  **helpText NOT SET IN DESCRIPTOR FILE** | /v2.1/accounts/{accountId}/signatures\_\_\_post |
| BCCEmailArchive PostBCCEmailArchive - Creates a BCC email archive configuration. | Creates a BCC email archive configuration. | /v2.1/accounts/{accountId}/settings/bcc\_email\_archives\_\_\_post |
| BillingPayments PostPayment - Posts a payment to a past due invoice. | Posts a payment to a past due invoice. | /v2.1/accounts/{accountId}/billing\_payments\_\_\_post |
| Brands PostBrands - Creates one or more brand profiles for an account. | Creates one or more brand profiles for an account. | /v2.1/accounts/{accountId}/brands\_\_\_post |
| BulkSendV2CRUD PostBulkSendList - Creates a bulk send list | Creates a bulk send list | /v2.1/accounts/{accountId}/bulk\_send\_lists\_\_\_post |
| BulkSendV2Send PostBulkSendRequest - Creates a bulk send request | Creates a bulk send request | /v2.1/accounts/{accountId}/bulk\_send\_lists/{bulkSendListId}/send\_\_\_post |
| BulkSendV2Test PostBulkSendTestRequest - Creates a bulk send test | Creates a bulk send test | /v2.1/accounts/{accountId}/bulk\_send\_lists/{bulkSendListId}/test\_\_\_post |
| ChunkedUploads PostChunkedUploads - Initiate a new chunked upload. | Initiate a new chunked upload. | /v2.1/accounts/{accountId}/chunked\_uploads\_\_\_post |
| CloudStorage PostCloudStorage - Configures the redirect URL information  for one o... | Configures the redirect URL information  for one o... | /v2.1/accounts/{accountId}/users/{userId}/cloud\_storage\_\_\_post |
| Connect PostConnectConfiguration - Creates a connect configuration for the specified ... | Creates a connect configuration for the specified ... | /v2.1/accounts/{accountId}/connect\_\_\_post |
| Contacts PostContacts - Imports new contacts into a contacts list. | Imports new contacts into a contacts list. | /v2.1/accounts/{accountId}/contacts\_\_\_post |
| CustomFields PostCustomFields - Creates envelope custom fields for an envelope. | Creates envelope custom fields for an envelope. | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/custom\_fields\_\_\_post |
| CustomFields PostTemplateCustomFields - Creates custom document fields in an existing temp... | Creates custom document fields in an existing temp... | /v2.1/accounts/{accountId}/templates/{templateId}/custom\_fields\_\_\_post |
| DocumentFields PostDocumentFields - Creates custom document fields in an existing enve... | Creates custom document fields in an existing enve... | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/documents/{documentId}/fields\_\_\_post |
| DocumentFields PostTemplateDocumentFields - Creates custom document fields in an existing temp... | Creates custom document fields in an existing temp... | /v2.1/accounts/{accountId}/templates/{templateId}/documents/{documentId}/fields\_\_\_post |
| EmailSettings PostEmailSettings - Adds email setting overrides to an envelope. | Adds email setting overrides to an envelope. | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/email\_settings\_\_\_post |
| Envelopes PostEnvelopes - Creates an envelope. | Creates an envelope. | /v2.1/accounts/{accountId}/envelopes\_\_\_post |
| EnvelopeTransferRules PostEnvelopeTransferRules - Creates an envelope transfer rule. | Creates an envelope transfer rule. | /v2.1/accounts/{accountId}/envelopes/transfer\_rules\_\_\_post |
| Groups PostGroups - Creates one or more groups for the account. | Creates one or more groups for the account. | /v2.1/accounts/{accountId}/groups\_\_\_post |
| Lock PostEnvelopeLock - Locks an envelope. | Locks an envelope. | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/lock\_\_\_post |
| Lock PostTemplateLock - Locks a template. | Locks a template. | /v2.1/accounts/{accountId}/templates/{templateId}/lock\_\_\_post |
| Notary PostNotary - Registers the current user as a notary. | Registers the current user as a notary. | /v2.1/current\_user/notary\_\_\_post |
| NotaryJurisdictions PostNotaryJurisdictions - Creates a jurisdiction object. | Creates a jurisdiction object. | /v2.1/current\_user/notary/jurisdictions\_\_\_post |
| PermissionProfiles PostPermissionProfiles - Creates a new permission profile for an account. | Creates a new permission profile for an account. | /v2.1/accounts/{accountId}/permission\_profiles\_\_\_post |
| PowerForms PostPowerForm - Creates a new PowerForm | Creates a new PowerForm | /v2.1/accounts/{accountId}/powerforms\_\_\_post |
| Recipients PostRecipientProofFileResourceToken - Creates a resource token for a sender to request I... | Creates a resource token for a sender to request I... | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/recipients/{recipientId}/identity\_proof\_token\_\_\_post |
| Recipients PostRecipients - Adds one or more recipients to an envelope. | Adds one or more recipients to an envelope. | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/recipients\_\_\_post |
| Recipients PostRecipientTabs - Adds tabs for a recipient. | Adds tabs for a recipient. | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/recipients/{recipientId}/tabs\_\_\_post |
| Recipients PostTemplateRecipients - Adds tabs for a recipient. | Adds tabs for a recipient. | /v2.1/accounts/{accountId}/templates/{templateId}/recipients\_\_\_post |
| Recipients PostTemplateRecipientTabs - Adds tabs for a recipient. | Adds tabs for a recipient. | /v2.1/accounts/{accountId}/templates/{templateId}/recipients/{recipientId}/tabs\_\_\_post |
| ReportsInProduct PostReportInProductCreate -  |  **helpText NOT SET IN DESCRIPTOR FILE** | /v2.1/accounts/{accountId}/reports\_\_\_post |
| ResponsiveHtml PostDocumentResponsiveHtmlPreview - Creates a preview of the responsive version of a d... | Creates a preview of the responsive version of a d... | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/documents/{documentId}/responsive\_html\_preview\_\_\_post |
| ResponsiveHtml PostResponsiveHtmlPreview - Creates a preview of the responsive versions of al... | Creates a preview of the responsive versions of al... | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/responsive\_html\_preview\_\_\_post |
| ResponsiveHtml PostTemplateDocumentResponsiveHtmlPreview - Creates a preview of the responsive version of a t... | Creates a preview of the responsive version of a t... | /v2.1/accounts/{accountId}/templates/{templateId}/documents/{documentId}/responsive\_html\_preview\_\_\_post |
| ResponsiveHtml PostTemplateResponsiveHtmlPreview - Creates a preview of the responsive versions of al... | Creates a preview of the responsive versions of al... | /v2.1/accounts/{accountId}/templates/{templateId}/responsive\_html\_preview\_\_\_post |
| SigningGroups PostSigningGroups - Creates a signing group.  | Creates a signing group.  | /v2.1/accounts/{accountId}/signing\_groups\_\_\_post |
| Tabs PostDocumentTabs -  |  **helpText NOT SET IN DESCRIPTOR FILE** | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/documents/{documentId}/tabs\_\_\_post |
| Tabs PostTabDefinitions - Creates a custom tab. | Creates a custom tab. | /v2.1/accounts/{accountId}/tab\_definitions\_\_\_post |
| Tabs PostTemplateDocumentTabs - Create Template Document Tabs | Create Template Document Tabs | /v2.1/accounts/{accountId}/templates/{templateId}/documents/{documentId}/tabs\_\_\_post |
| Templates PostDocumentTemplates - Adds templates to a document in an  envelope. | Adds templates to a document in an  envelope. | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/documents/{documentId}/templates\_\_\_post |
| Templates PostEnvelopeTemplates - Adds templates to an envelope. | Adds templates to an envelope. | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/templates\_\_\_post |
| Templates PostTemplates - Creates one or more templates. | Creates one or more templates. | /v2.1/accounts/{accountId}/templates\_\_\_post |
| Users PostUsers - Adds new users to the specified account. | Adds new users to the specified account. | /v2.1/accounts/{accountId}/users\_\_\_post |
| UserSignatures PostUserSignatures - Adds user Signature and initials images to a Signa... | Adds user Signature and initials images to a Signa... | /v2.1/accounts/{accountId}/users/{userId}/signatures\_\_\_post |
| Views PostAccountConsoleView - Returns a URL to the authentication view UI. | Returns a URL to the authentication view UI. | /v2.1/accounts/{accountId}/views/console\_\_\_post |
| Views PostEnvelopeCorrectView - Returns a URL to the envelope correction UI. | Returns a URL to the envelope correction UI. | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/views/correct\_\_\_post |
| Views PostEnvelopeEditView - Returns a URL to the edit view UI. | Returns a URL to the edit view UI. | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/views/edit\_\_\_post |
| Views PostEnvelopeRecipientPreview - Creates an envelope recipient preview. | Creates an envelope recipient preview. | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/views/recipient\_preview\_\_\_post |
| Views PostEnvelopeRecipientSharedView - Returns a URL to the shared recipient view UI for ... | Returns a URL to the shared recipient view UI for ... | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/views/shared\_\_\_post |
| Views PostEnvelopeRecipientView - Returns a URL to the recipient view UI. | Returns a URL to the recipient view UI. | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/views/recipient\_\_\_post |
| Views PostEnvelopeSenderView - Returns a URL to the sender view UI. | Returns a URL to the sender view UI. | /v2.1/accounts/{accountId}/envelopes/{envelopeId}/views/sender\_\_\_post |
| Views PostTemplateEditView - Gets a URL for a template edit view. | Gets a URL for a template edit view. | /v2.1/accounts/{accountId}/templates/{templateId}/views/edit\_\_\_post |
| Views PostTemplateRecipientPreview - Creates a template recipient preview. | Creates a template recipient preview. | /v2.1/accounts/{accountId}/templates/{templateId}/views/recipient\_preview\_\_\_post |
| Workspace PostWorkspace - Create a Workspace | Create a Workspace | /v2.1/accounts/{accountId}/workspaces\_\_\_post |
| WorkspaceFile PostWorkspaceFiles - Creates a workspace file. | Creates a workspace file. | /v2.1/accounts/{accountId}/workspaces/{workspaceId}/folders/{folderId}/files\_\_\_post |


