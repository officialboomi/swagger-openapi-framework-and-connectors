# Boomi Flow
This connector wrappers REST API for the Boomi Flow platform. Refer here for more info https://manywho.github.io/docs-api/ .

# Connection Tab
## Connection Fields


#### Boomi Flow REST API Service URL

The URL for the Boomi Flow REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default openapi.json file so you can provide a url to any swagger file.

**Type** - string



#### URL

The URL for the Service service

**Type** - string



#### AuthenticationType

Allows user to select between BASIC and OAUTH 2.0 Authentication

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 flavors: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE/EXECUTE


## UPDATE


## GET


## GET


## DELETE


## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  (Supported Types:  date, number, string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Admin Admin - Reset Password | Admin Admin The password reset API is only applicable to Flow Builders. For running users, the user identity is managed by the underlying Service (e.g. Salesforce, Box, Google), and therefore user resets should be performed on the underlying system, not within ManyWho. The password reset API requires two separate API calls to complete. The first API call sends the user the password reset notification. The second API call performs the actual password change, based on the token provided in the notification.  If a notification is provided, the <code>redirectUrl</code> property should include two parameters in the content - one for the notification result (<code>{0}</code>) and one for the reset token (<code>{1}</code>). The platform will automatically parse the notification result and <code>callbackUri</code> values at these positions in the content.  The result parameter has the following possible values:  * **OK:** The password reset was correctly processed * **ALREADY_PROCESSED**: The password reset token has already been processed by the Platform and the user is re-using the link  To add the password verification URL to the notification, simply add `PASSWORD_URL_HERE` to the content of the message and the platform will replace this with the actual verification callback URL, which will in turn forward to the `redirectUrl`. Reset Password  | /api/admin/1/directory/user/password\_\_\_post |
| Admin Provisioning - Provision a Tenant | Admin Provisioning This endpoint requests the provisioning of a new tenant, and also creates a new user if one doesn't exist with the given email address. If a value is given for the `notification` object, then the provisioning email sent to the user will be overridden with the provided message. Provision a Tenant  | /api/admin/1/provisioning\_\_\_post |
| Admin Runtimes - Create Runtime | Admin Runtimes Creates a new runtime inside the current organization. Create Runtime  | /api/admin/1/organization/runtimes\_\_\_post |
| Admin Stores - Create Store | Admin Stores Creates a new store, generating an ID, a keypair for encrypting incoming API requests and a keypair for ensuring payload integrity. Create Store  | /api/admin/1/stores\_\_\_post |
| Admin Stores - Migrates data to Store | Admin Stores Starts a migration of data from the current tenant into the selected store. Migrates data to Store  | /api/admin/1/stores/migrations\_\_\_post |
| Admin Tenants and Subtenants - Create Subtenant | Admin Tenants and Subtenants Used to create a new subtenant underneath the current tenant. The <code>developerName</code> must be unique, and can only contain letters and numbers, with no spaces. Create Subtenant  | /api/admin/1/tenant/subtenants\_\_\_post |
| Admin Tenants and Subtenants - Update Tenant | Admin Tenants and Subtenants Used to update the tenant object for the current tenant. Tenants provide a central place for flow builders to build, manage and deploy flows. Update Tenant  | /api/admin/1/tenant\_\_\_post |
| Admin Users - Add User to Tenant | Admin Users Add User to Tenant  | /api/admin/1/users\_\_\_post |
| AdminOrganizationInviteTenants - Invite Tenant to Organization | AdminOrganizationInviteTenants Creates and sends an invitation for another tenant to join the currently-authenticated organization. Invite Tenant to Organization  | /api/admin/1/organization/invites/tenants\_\_\_post |
| AdminOrganizationInviteUsers - Invite User to Organization | AdminOrganizationInviteUsers Creates and sends an invitation for a user to join the currently-authenticated organization. Invite User to Organization  | /api/admin/1/organization/invites/users\_\_\_post |
| AdminOrganizationTenants - Create Organization Tenant | AdminOrganizationTenants Create a new tenant inside the current organization Create Organization Tenant  | /api/admin/1/organization/tenants\_\_\_post |
| Draw Assets - Create Folder | Draw Assets Create an empty "folder" in the current tenant's asset storage Create Folder  | /api/draw/1/assets\_\_\_post |
| Draw Assets - Generate Upload URL | Draw Assets Generate a signed upload URL, which should be used to submit the asset to (using `PUT`). A `contentType` is required in this request. Generate Upload URL  | /api/draw/1/assets/upload\_\_\_post |
| Draw Authentication - Authenticate | Draw Authentication Authenticate  | /api/draw/1/authentication\_\_\_post |
| Draw Flow - Create/Update Flow | Draw Flow Used to create new flows or update existing ones. The flow object represents an entire flow application.  <aside class="alert alert-info"> If the <code>id</code> property is included in the request, the platform will update the flow with a matching ID. If the <code>updateByName</code> property is set to <code>true</code>, the platform will update the flow with the matching <code>developerName</code> property. </aside> Create/Update Flow  | /api/draw/1/flow\_\_\_post |
| Draw Flow Graph - Update Flow Graph | Draw Flow Graph Used to update a flow graph. <aside class="alert alert-info"> The only properties that can be updated in each element are: <ul><li>developerName</li><li>developerSummary</li><li>x</li><li>y</li><li>height</li><li>width</li></ul></aside> Update Flow Graph  | /api/draw/1/graph/flow\_\_\_post |
| Draw Group Element - Create/Update Group Element | Draw Group Element Used to create new group elements or update existing ones.  <aside class="alert alert-info"> If the <code>id</code> property is included in the request, the platform will update the element with a matching ID. If the <code>updateByName</code> property is set to <code>true</code>, the platform will update the element with the matching <code>developerName</code> property. </aside> Create/Update Group Element  | /api/draw/1/flow/{flow}/{editingToken}/element/group\_\_\_post |
| Draw Macro Element - Create/Update Macro Elements | Draw Macro Element Used to create new macro elements or update existing ones.  <aside class="alert alert-info">  If the <code>id</code> property is included in the request, the platform will update the element with a matching ID. If the <code>updateByName</code> property is set to <code>true</code>, the platform will update the element with the matching <code>developerName</code> property. </aside> Create/Update Macro Elements  | /api/draw/1/element/macro\_\_\_post |
| Draw Map Element - Create/Update Map Elements | Draw Map Element Used to create new map elements or update existing ones.  <aside class="alert alert-info">  If the <code>id</code> property is included in the request, the platform will update the element with a matching ID. If the <code>updateByName</code> property is set to <code>true</code>, the platform will update the element with the matching <code>developerName</code> property. </aside> Create/Update Map Elements  | /api/draw/1/flow/{flow}/{editingToken}/element/map\_\_\_post |
| Draw Navigation Element - Create/Update Navigation Elements | Draw Navigation Element Used to create new navigation elements or update existing ones.  <aside class="alert alert-info">  If the <code>id</code> property is included in the request, the platform will update the element with a matching ID. If the <code>updateByName</code> property is set to <code>true</code>, the platform will update the element with the matching <code>developerName</code> property. </aside> Create/Update Navigation Elements  | /api/draw/1/flow/{flow}/{editingToken}/element/navigation\_\_\_post |
| Draw Page Element - Create/Update Page Elements | Draw Page Element Used to create new page elements or update existing ones.  <aside class="alert alert-info">  If the <code>id</code> property is included in the request, the platform will update the element with a matching ID. If the <code>updateByName</code> property is set to <code>true</code>, the platform will update the element with the matching <code>developerName</code> property. </aside> Create/Update Page Elements  | /api/draw/1/element/page\_\_\_post |
| Draw Service Element - Create/Update Service Elements | Draw Service Element Used to create new service elements or update existing ones.  <aside class="alert alert-info">  If the <code>id</code> property is included in the request, the platform will update the element with a matching ID. If the <code>updateByName</code> property is set to <code>true</code>, the platform will update the element with the matching <code>developerName</code> property. </aside> Create/Update Service Elements  | /api/draw/1/element/service\_\_\_post |
| Draw Service Element - Describe Service Element | Draw Service Element Used to obtain a describe response for a service element. Describe Service Element  | /api/draw/1/element/service/describe\_\_\_post |
| Draw Service Element - Install Service Element | Draw Service Element Used to obtain an install response for a service element. Install Service Element  | /api/draw/1/element/service/install\_\_\_post |
| Draw Tag Element - Create/Update Tag Elements | Draw Tag Element Used to create new tag elements or update existing ones.  <aside class="alert alert-info">  If the <code>id</code> property is included in the request, the platform will update the element with a matching ID. If the <code>updateByName</code> property is set to <code>true</code>, the platform will update the element with the matching <code>developerName</code> property. </aside> Create/Update Tag Elements  | /api/draw/1/element/tag\_\_\_post |
| Draw Type Element - Create/Update Type Elements | Draw Type Element Used to create new type elements or update existing ones.  <aside class="alert alert-info">  If the <code>id</code> property is included in the request, the platform will update the element with a matching ID. If the <code>updateByName</code> property is set to <code>true</code>, the platform will update the element with the matching <code>developerName</code> property. </aside> Create/Update Type Elements  | /api/draw/1/element/type\_\_\_post |
| Draw Value Element - Create/Update Value Elements | Draw Value Element Used to create new value elements or update existing ones.  <aside class="alert alert-info">  If the <code>id</code> property is included in the request, the platform will update the element with a matching ID. If the <code>updateByName</code> property is set to <code>true</code>, the platform will update the element with the matching <code>developerName</code> property. </aside> Create/Update Value Elements  | /api/draw/1/element/value\_\_\_post |
| Notifications Notifications - Mark all notifications as read for the current use... | Notifications Notifications Marks all the notifications for the current user as read Mark all notifications as read for the current user  | /api/notifications/1/me/read\_\_\_post |
| Package Package - Get Flow Sharing Token | Package Package Get the sharing token for the last published version of a flow Get Flow Sharing Token  | /api/package/1/flow/{flow}/share\_\_\_post |
| Package Package - Get Flow Version Sharing Token | Package Package Get the sharing token for a specific version of a flow Get Flow Version Sharing Token  | /api/package/1/flow/{flow}/{version}/share\_\_\_post |
| Package Package - Import Package | Package Package This allows you to import a flow package into a Tenant. It’s important to note that this is not the same as cloning a flow. If you import a flow package into a tenant that contains a flow with the same flow ID, the flow in the target tenant will be overwritten with the flow package being imported. Import Package  | /api/package/1/flow\_\_\_post |
| Package Package - Import Package with Flow Sharing Token | Package Package Import a flow via a unique sharing token Import Package with Flow Sharing Token  | /api/package/1/shared/flow\_\_\_post |
| Run Data - Load Data from Service | Run Data Load Data from Service  | /api/run/1/service/data\_\_\_post |
| Run Files - Delete File from Service | Run Files Delete File from Service  | /api/run/1/service/file/delete\_\_\_post |
| Run Files - Load Files from Service | Run Files Load Files from Service  | /api/run/1/service/file\_\_\_post |
| Run Files - Upload File to Service | Run Files Upload File to Service  | /api/run/1/service/file/content\_\_\_post |
| Run Run - Add Listener | Run Run Used to add a listener to the state <code>stateId</code> from the details in the request body Add Listener  | /api/run/1/state/{stateId}/listener\_\_\_post |
| Run Run - Authenticate (SAML) | Run Run Used to authenticate with SAML Authenticate (SAML)  | /api/run/1/saml\_\_\_post |
| Run Run - Event | Run Run Used to get the invoke type of the service event provided Event  | /api/run/1/event\_\_\_post |
| Run Run - Import State | Run Run Used to import a state into the tenant from JSON Import State  | /api/run/1/state/package\_\_\_post |
| Run Run - Initialize a Flow | Run Run Used to initialize a flow Initialize a Flow  | /api/run/1\_\_\_post |
| Run Run - Initialize a Flow (Simple) | Run Run Used to initialise a flow and authenitcate into it.  <aside class="alert alert-info"> Either a flow ID or developer name must be provided to initialize a flow. </aside> Initialize a Flow (Simple)  | /api/run/1/state\_\_\_post |
| Run Run - Response From Service | Run Run Used to get the invoke type of the service response provided Response From Service  | /api/run/1/response\_\_\_post |
| Run Run - Set Flow State Values | Run Run Used to set values in the flow state with the given data. This endpoint requires a runtime authentication token, which means it can only be used with non-public flows. Set Flow State Values  | /api/run/1/state/{stateId}/values\_\_\_post |
| Translate Cultures - Create/Update Content Value Culture | Translate Cultures Used to create new content value cultures or update existing ones.  <aside class="alert alert-info">  If the <code>id</code> property is included in the request, the platform will update the culture with a matching ID. </aside> Create/Update Content Value Culture  | /api/translate/1/culture\_\_\_post |
| Translate Map Element - Update Map Element Translation | Translate Map Element Update Map Element Translation  | /api/translate/1/flow/{flow}/{editingToken}/element/map\_\_\_post |
| Translate Navigation Element - Update Navigation Translation | Translate Navigation Element Update Navigation Translation  | /api/translate/1/flow/{flow}/{editingToken}/element/navigation\_\_\_post |
| Translate Page Element - Update Page Translation | Translate Page Element Update Page Translation  | /api/translate/1/element/page\_\_\_post |
| Translate Type Element - Update Type Translation | Translate Type Element Update Type Translation  | /api/translate/1/element/type\_\_\_post |
| Translate Value Element - Update Value Translation | Translate Value Element Update Value Translation  | /api/translate/1/element/value\_\_\_post |


### DELETE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Admin Runtimes - Delete Runtime | Admin Runtimes Deletes a runtime by ID in the current organization.              Runtimes can be associated with one or more tenants, and deleting a runtime will take effect globally, removing access from all associated tenants. Delete Runtime  | /api/admin/1/organization/runtimes/{id}\_\_\_delete |
| Admin States - Delete Multiple States | Admin States Delete multiple flow states Delete Multiple States  | /api/admin/1/states\_\_\_delete |
| Admin States - Delete State | Admin States Delete an individual flow state Delete State  | /api/admin/1/states/{id}\_\_\_delete |
| Admin Stores - Delete Store | Admin Stores Delete a store from the current tenant. This instantly invalidates any keys from the store that are still in-use. Delete Store  | /api/admin/1/stores/{id}\_\_\_delete |
| Admin Tenants and Subtenants - Delete Tenant | Admin Tenants and Subtenants <aside class="alert alert-error">             Use this API with extreme caution. Although we do keep backups of tenants, recovering from tenant delete             operations takes time and is chargeable. A request to delete tenants does require verification via email             before the actual delete is performed             </aside> Delete Tenant  | /api/admin/1/tenant\_\_\_delete |
| Admin Tenants and Subtenants - Delete Tenant Data | Admin Tenants and Subtenants <aside class="alert alert-error">             Use this API with extreme caution. Although we do keep backups of tenant data, recovering from tenant data             deletion operations takes time and is chargeable. A request to delete tenant data does require verification             via email before the actual delete is performed. However, make sure you check your settings carefully as             dependency validations are ignored. You have been warned!             </aside> Delete Tenant Data  | /api/admin/1/tenant/data\_\_\_delete |
| Admin Users - Remove User from Tenant | Admin Users Remove User from Tenant  | /api/admin/1/users/{id}\_\_\_delete |
| AdminOrganizationInviteTenants - Reject Tenant Invitation | AdminOrganizationInviteTenants Rejects a pending invitation for a tenant to join an organization. Reject Tenant Invitation  | /api/admin/1/organization/invites/tenants/{id}\_\_\_delete |
| AdminOrganizationInviteUsers - Reject User Invitation | AdminOrganizationInviteUsers Rejects a pending invitation for a user to join an organization. Reject User Invitation  | /api/admin/1/organization/invites/users/{id}\_\_\_delete |
| AdminOrganizationTenants - Remove Tenant from Organization | AdminOrganizationTenants Removed the specified tenant from the current organization. Remove Tenant from Organization  | /api/admin/1/organization/tenants/{id}\_\_\_delete |
| AdminOrganizationUsers - Remove User from Organization | AdminOrganizationUsers Removed the specified users from the current organization. Remove User from Organization  | /api/admin/1/organization/users/{id}\_\_\_delete |
| Draw Assets - Delete Asset | Draw Assets Delete an individual asset (or folder) Delete Asset  | /api/draw/1/assets\_\_\_delete |
| Draw Flow - Delete Flow | Draw Flow Used to delete an existing flow Delete Flow  | /api/draw/1/flow/{id}\_\_\_delete |
| Draw Flow - Remove Element from Flow | Draw Flow Used to remove an imported element from a flow Remove Element from Flow  | /api/draw/1/element/flow/{flow}/{elementType}/{id}\_\_\_delete |
| Draw Group Element - Delete Group Element | Draw Group Element Used to delete an existing group element. Delete Group Element  | /api/draw/1/flow/{flow}/{editingToken}/element/group/{id}\_\_\_delete |
| Draw Macro Element - Delete Macro Element | Draw Macro Element Used to delete an existing macro element. Delete Macro Element  | /api/draw/1/element/macro/{id}\_\_\_delete |
| Draw Map Element - Delete Map Element | Draw Map Element Used to delete an existing map element. Delete Map Element  | /api/draw/1/flow/{flow}/{editingToken}/element/map/{id}\_\_\_delete |
| Draw Navigation Element - Delete Navigation Element | Draw Navigation Element Used to delete an existing navigation element. Delete Navigation Element  | /api/draw/1/flow/{flow}/{editingToken}/element/navigation/{id}\_\_\_delete |
| Draw Page Element - Delete Page Element | Draw Page Element Used to delete an existing page element. Delete Page Element  | /api/draw/1/element/page/{id}\_\_\_delete |
| Draw Service Element - Delete Service Element | Draw Service Element Used to delete an existing service element. Delete Service Element  | /api/draw/1/element/service/{id}\_\_\_delete |
| Draw Tag Element - Delete Tag Element | Draw Tag Element Used to delete an existing tag element. Delete Tag Element  | /api/draw/1/element/tag/{id}\_\_\_delete |
| Draw Type Element - Delete Type Element | Draw Type Element Used to delete an existing type element. Delete Type Element  | /api/draw/1/element/type/{id}\_\_\_delete |
| Draw Value Element - Delete Value Element | Draw Value Element Used to delete an existing value element. Delete Value Element  | /api/draw/1/element/value/{id}\_\_\_delete |
| Play Play - Delete Player | Play Play Delete a player by name Delete Player  | /{tenantId}/play/{name}\_\_\_delete |
| Run Run - Remove Listener | Run Run Used to remove the listener on the state <code>stateId</code> with the id <code>listenerId</code> Remove Listener  | /api/run/1/state/{stateId}/listener/{listenerId}\_\_\_delete |
| Translate Cultures - Delete Content Value Culture | Translate Cultures Used to delete an existing content value culture. Delete Content Value Culture  | /api/translate/1/culture/{id}\_\_\_delete |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Admin Runtimes - List Runtime Failures | Admin Runtimes If the Flow cloud encounters an error communicating with a local runtime, details about the failure will be available from this endpoint. List Runtime Failures  | /api/admin/1/organization/runtimes/{id}/failures\_\_\_get |
| Admin Runtimes - List Runtimes | Admin Runtimes Returns a list of runtimes associated with the current organization. List Runtimes  | /api/admin/1/organization/runtimes\_\_\_get |
| Admin Runtimes - Load Runtime | Admin Runtimes Loads an associated runtime by ID in the current organization. Load Runtime  | /api/admin/1/organization/runtimes/{id}\_\_\_get |
| Admin States - List States | Admin States Get a listing of all the flow states in the current tenant. List States  | /api/admin/1/states\_\_\_get |
| Admin States - List States by Flow | Admin States Get a listing of all the flow states grouped by flow in the current tenant. List States by Flow  | /api/admin/1/states/flow\_\_\_get |
| Admin States - List States for a Flow | Admin States Get a listing of all the flow states for a specific flow in the current tenant. List States for a Flow  | /api/admin/1/states/flow/{id}\_\_\_get |
| Admin States - List States for a Flow Version | Admin States Get a listing of all the flow states for a specific flow version in the current tenant. List States for a Flow Version  | /api/admin/1/states/flow/{id}/{version}\_\_\_get |
| Admin States - Load State | Admin States Load an individual flow state Load State  | /api/admin/1/states/{id}\_\_\_get |
| Admin Stores - Find Store | Admin Stores Finds a store by ID, in the current tenant. Find Store  | /api/admin/1/stores/{id}\_\_\_get |
| Admin Stores - List Stores | Admin Stores Returns a list of stores in the current tenant. List Stores  | /api/admin/1/stores\_\_\_get |
| Admin Stores - Used to list all the store migrations, ordered by ... | Admin Stores Used to list all the store migrations, ordered by the creation date  | /api/admin/1/stores/migrations\_\_\_get |
| Admin Tenants and Subtenants - List the Runtimes for a Tenant | Admin Tenants and Subtenants Used to get the list of runtimes for the current tenant. List the Runtimes for a Tenant  | /api/admin/1/tenant/runtimes\_\_\_get |
| Admin Tenants and Subtenants - Load Subtenants | Admin Tenants and Subtenants Used to list all the subtenants of the current tenant. Load Subtenants  | /api/admin/1/tenant/subtenants\_\_\_get |
| Admin Tenants and Subtenants - Load Tenant | Admin Tenants and Subtenants Used to get the tenant object for the current tenant. Tenants provide a central place for flow builders to  build, manage and deploy flows. Load Tenant  | /api/admin/1/tenant\_\_\_get |
| Admin Users - List Users | Admin Users Get a listing of all users inside the current tenant. List Users  | /api/admin/1/users\_\_\_get |
| Admin Users - Load the Current User | Admin Users Get the currently-authenticated user's information Load the Current User  | /api/admin/1/users/me\_\_\_get |
| Admin Users - Load the Current User's settings | Admin Users Load the settings for the currently-authenticated user Load the Current User's settings  | /api/admin/1/users/me/settings\_\_\_get |
| Admin Users - Load User | Admin Users Load an individual user from the current tenant. Load User  | /api/admin/1/users/{id}\_\_\_get |
| AdminOrganizationInvite - List Organization Invitations | AdminOrganizationInvite Lists all the invitations that the current user is able to action. List Organization Invitations  | /api/admin/1/organization/invites\_\_\_get |
| AdminOrganizationInviteTenants - List Organization Tenant Invitations | AdminOrganizationInviteTenants Lists all the invitations that have been sent from the current organization. List Organization Tenant Invitations  | /api/admin/1/organization/invites/tenants\_\_\_get |
| AdminOrganizationInviteUsers - List Organization User Invitations | AdminOrganizationInviteUsers Lists all the invitations that have been sent from the current organization to users. List Organization User Invitations  | /api/admin/1/organization/invites/users\_\_\_get |
| AdminOrganizationTenants - List Organization Tenants | AdminOrganizationTenants Lists all the tenants that are a part of this organization. List Organization Tenants  | /api/admin/1/organization/tenants\_\_\_get |
| AdminOrganizationUsers - List Organization Users | AdminOrganizationUsers Lists all the users that are a part of this organization. List Organization Users  | /api/admin/1/organization/users\_\_\_get |
| Draw Assets - List Assets | Draw Assets Get a listing of all the assets in the current tenant List Assets  | /api/draw/1/assets\_\_\_get |
| Draw Authentication - Switch Tenant | Draw Authentication Switch Tenant  | /api/draw/1/authentication/{tenant}\_\_\_get |
| Draw Draw - Get Custom Styles | Draw Draw Get Custom Styles  | /css/tenant/{tenantId}/customstyles\_\_\_get |
| Draw Draw - List Element Dependencies | Draw Draw List the flattened tree of elements that a specific element depends on, including all registered dependencies of dependencies. List Element Dependencies  | /api/draw/1/dependencies/{id}\_\_\_get |
| Draw Draw - List Element Dependents | Draw Draw List the flattened tree of elements that depends on a specific element. List Element Dependents  | /api/draw/1/dependents/{id}\_\_\_get |
| Draw Flow - Get Flow | Draw Flow Used to get an existing flow by ID. Get Flow  | /api/draw/1/flow/{id}\_\_\_get |
| Draw Flow - List Elements in Flow | Draw Flow Used to list all the elements of a type used in a flow List Elements in Flow  | /api/draw/1/element/flow/{flow}/{elementType}\_\_\_get |
| Draw Flow - List Flows | Draw Flow Used to list and filter existing flows.  ### Filter  The filter can take the following formats:  * <code>developerName eq '{developer_name}'</code>: Filter the list of flows where the <code>developerName</code> property exactly matches the provided developer name (case insensitive) * <code>substringof(developerName, '{developer_name}')</code>: Filter the list of flows where the <code>developerName</code> property partially matches the provided developer name (case insensitive) List Flows  | /api/draw/1/flow\_\_\_get |
| Draw Flow Graph - Get Flow Graph | Draw Flow Graph Used to get an existing flow graph. The flow graph provides the coordinate and basic configuration information of map and group elements. Get Flow Graph  | /api/draw/1/graph/flow/{flow}\_\_\_get |
| Draw Flow Snapshot - Get Flow Snapshot | Draw Flow Snapshot Used to get a single flow snapshot. Get Flow Snapshot  | /api/draw/1/flow/snap/{flow}/{version}\_\_\_get |
| Draw Flow Snapshot - List Flow Snapshots | Draw Flow Snapshot Used to get the list of all snapshots for a particular flow. List Flow Snapshots  | /api/draw/1/flow/snap/{flow}\_\_\_get |
| Draw Group Element - Get Group Element | Draw Group Element Used to get an existing group element. Get Group Element  | /api/draw/1/flow/{flow}/{editingToken}/element/group/{id}\_\_\_get |
| Draw Group Element - List Group Elements | Draw Group Element Used to list and filter existing group elements.  ### Filter  The filter can take the following formats:  * <code>developerName eq '{developer_name}'</code>: Filter the list of elements where the <code>developerName</code> property exactly matches the provided developer name (case insensitive) * <code>substringof(developerName, '{developer_name}')</code>: Filter the list of elements where the <code>developerName</code> property partially matches the provided developer name (case insensitive) List Group Elements  | /api/draw/1/flow/{flow}/{editingToken}/element/group\_\_\_get |
| Draw Macro Element - Get Macro Element | Draw Macro Element Used to get an existing macro element. Get Macro Element  | /api/draw/1/element/macro/{id}\_\_\_get |
| Draw Macro Element - List Macro Elements | Draw Macro Element Used to list and filter existing macro elements.  ### Filter  The filter can take the following formats:  * <code>developerName eq '{developer_name}'</code>: Filter the list of elements where the <code>developerName</code> property exactly matches the provided developer name (case insensitive) * <code>substringof(developerName, '{developer_name}')</code>: Filter the list of elements where the <code>developerName</code> property partially matches the provided developer name (case insensitive) List Macro Elements  | /api/draw/1/element/macro\_\_\_get |
| Draw Map Element - Get Map Element | Draw Map Element Used to get an existing map element. Get Map Element  | /api/draw/1/flow/{flow}/{editingToken}/element/map/{id}\_\_\_get |
| Draw Map Element - List Map Elements | Draw Map Element Used to filter existing map elements.  ### Filter  The filter can take the following formats:  * <code>developerName eq '{developer_name}'</code>: Filter the list of elements where the <code>developerName</code> property exactly matches the provided developer name (case insensitive) * <code>substringof(developerName, '{developer_name}')</code>: Filter the list of elements where the <code>developerName</code> property partially matches the provided developer name (case insensitive) List Map Elements  | /api/draw/1/flow/{flow}/{editingToken}/element/map\_\_\_get |
| Draw Navigation Element - Get Navigation Element | Draw Navigation Element Used to get an existing navigation element. Get Navigation Element  | /api/draw/1/flow/{flow}/{editingToken}/element/navigation/{id}\_\_\_get |
| Draw Navigation Element - List Navigation Elements | Draw Navigation Element Used to filter existing navigation elements.  ### Filter  The filter can take the following formats:  * <code>developerName eq '{developer_name}'</code>: Filter the list of elements where the <code>developerName</code> property exactly matches the provided developer name (case insensitive) * <code>substringof(developerName, '{developer_name}')</code>: Filter the list of elements where the <code>developerName</code> property partially matches the provided developer name (case insensitive) List Navigation Elements  | /api/draw/1/flow/{flow}/{editingToken}/element/navigation\_\_\_get |
| Draw Page Element - Get Page Element | Draw Page Element Used to get an existing page element. Get Page Element  | /api/draw/1/element/page/{id}\_\_\_get |
| Draw Page Element - List Page Elements | Draw Page Element Used to list and filter existing page elements.  ### Filter  The filter can take the following formats:  * <code>developerName eq '{developer_name}'</code>: Filter the list of elements where the <code>developerName</code> property exactly matches the provided developer name (case insensitive) * <code>substringof(developerName, '{developer_name}')</code>: Filter the list of elements where the <code>developerName</code> property partially matches the provided developer name (case insensitive) List Page Elements  | /api/draw/1/element/page\_\_\_get |
| Draw Service Element - Get Service Element | Draw Service Element Used to get an existing service element. Get Service Element  | /api/draw/1/element/service/{id}\_\_\_get |
| Draw Service Element - List Service Elements | Draw Service Element Used to list and filter existing service elements.  ### Filter  The filter can take the following formats:  * <code>developerName eq '{developer_name}'</code>: Filter the list of elements where the <code>developerName</code> property exactly matches the provided developer name (case insensitive) * <code>substringof(developerName, '{developer_name}')</code>: Filter the list of elements where the <code>developerName</code> property partially matches the provided developer name (case insensitive) List Service Elements  | /api/draw/1/element/service\_\_\_get |
| Draw Tag Element - Get Tag Element | Draw Tag Element Used to get an existing tag element. Get Tag Element  | /api/draw/1/element/tag/{id}\_\_\_get |
| Draw Tag Element - List Tag Elements | Draw Tag Element Used to list and filter existing tag elements.  ### Filter  The filter can take the following formats:  * <code>developerName eq '{developer_name}'</code>: Filter the list of elements where the <code>developerName</code> property exactly matches the provided developer name (case insensitive) * <code>substringof(developerName, '{developer_name}')</code>: Filter the list of elements where the <code>developerName</code> property partially matches the provided developer name (case insensitive) List Tag Elements  | /api/draw/1/element/tag\_\_\_get |
| Draw Type Element - Get Type Element | Draw Type Element Used to get an existing type element. Get Type Element  | /api/draw/1/element/type/{id}\_\_\_get |
| Draw Type Element - List Type Elements | Draw Type Element Used to list and filter existing type elements.  ### Filter  The filter can take the following formats:  * <code>developerName eq '{developer_name}'</code>: Filter the list of elements where the <code>developerName</code> property exactly matches the provided developer name (case insensitive) * <code>substringof(developerName, '{developer_name}')</code>: Filter the list of elements where the <code>developerName</code> property partially matches the provided developer name (case insensitive) List Type Elements  | /api/draw/1/element/type\_\_\_get |
| Draw Value Element - Get Value Element | Draw Value Element Used to get an existing value element. Get Value Element  | /api/draw/1/element/value/{id}\_\_\_get |
| Draw Value Element - List Value Element References | Draw Value Element Used to list and filter value element references, which are in a condensed format to help flow builders create merge fields in content.  ### Supported Element Types  <code>VARIABLE</code>: A reusable value containing data of the specified content type <code>LITERAL</code>: A simple, often single use value type containing a text or numeric value  ### Filter  The filter can take the following formats:  * <code>developerName eq '{developer_name}'</code>: Filter the list of elements where the <code>developerName</code> property exactly matches the provided developer name (case insensitive) * <code>substringof(developerName, '{developer_name}')</code>: Filter the list of elements where the <code>developerName</code> property partially matches the provided developer name (case insensitive) List Value Element References  | /api/draw/1/element/value/reference\_\_\_get |
| Draw Value Element - List Value Elements | Draw Value Element Used to list and filter existing value elements.  ### Filter  The filter can take the following formats:  * <code>developerName eq '{developer_name}'</code>: Filter the list of elements where the <code>developerName</code> property exactly matches the provided developer name (case insensitive) * <code>substringof(developerName, '{developer_name}')</code>: Filter the list of elements where the <code>developerName</code> property partially matches the provided developer name (case insensitive) List Value Elements  | /api/draw/1/element/value\_\_\_get |
| GetPlayerForTenant - Get Player | Play Play Get the contents of a player by name Get Player  | /{tenantId}/play/{playerName}\_\_\_get |
| GetPlayers1 - Get Players | Play Play Get the names of all the players available in this tenant Get Players  | /{tenantId}/player\_\_\_get |
| GetPlayers2 - Get Players | Play Play Get the names of all the players available in this tenant Get Players  | /{tenantId}/play\_\_\_get |
| Notifications Notifications - Finds a notification for the current user | Notifications Notifications Finds the notification and marks it as read Finds a notification for the current user  | /api/notifications/1/{id}\_\_\_get |
| Notifications Notifications - List Notifications | Notifications Notifications Get all the notifications that have been sent from inside a tenant List Notifications  | /api/notifications/1\_\_\_get |
| Notifications Notifications - List Notifications for User | Notifications Notifications Get all the unread notifications that have been sent to the currently logged in user, across all tenants List Notifications for User  | /api/notifications/1/me\_\_\_get |
| Package Package - Create Package for Flow | Package Package Create a package of the latest version of a flow snapshot Create Package for Flow  | /api/package/1/flow/{id}\_\_\_get |
| Package Package - Create Package for Flow Version | Package Package Get the package of a specific version of a Flow Snapshot Create Package for Flow Version  | /api/package/1/flow/{id}/{version}\_\_\_get |
| Run Flow - List Flows | Run Flow Used to list and filter existing snapshotted flows.  ### Filter  The filter can take the following formats:  * <code>developerName eq '{developer_name}'</code>: Filter the list of flows where the <code>developerName</code> property exactly matches the provided developer name (case insensitive) * <code>substringof(developerName, '{developer_name}')</code>: Filter the list of flows where the <code>developerName</code> property partially matches the provided developer name (case insensitive) List Flows  | /api/run/1/flow\_\_\_get |
| Run Flow - Load Flow by ID | Run Flow Load Flow by ID  | /api/run/1/flow/{id}\_\_\_get |
| Run Flow - Load Flow by Name | Run Flow Load Flow by Name  | /api/run/1/flow/name/{name}\_\_\_get |
| Run Log - Get Log | Run Log Get the execution log of a state Get Log  | /api/run/1/state/{stateId}/log\_\_\_get |
| Run Run - Authenticate (OAuth 1.0a) | Run Run Used to authenticate with OAuth 1.0a.              <aside class="alert alert-info"> The OAuth 1.0a API returns a callback URI. </aside> Authenticate (OAuth 1.0a)  | /api/run/1/oauth\_\_\_get |
| Run Run - Authenticate (OAuth 2.0) | Run Run Used to authenticate with OAuth 2.0.              <aside class="alert alert-info"> The OAuth 2.0 API redirects the user back to the application with a successful authentication. </aside> Authenticate (OAuth 2.0)  | /api/run/1/oauth2\_\_\_get |
| Run Run - Authorization Check | Run Run Check if the currently authenticated user has permission to access the state at its current position Authorization Check  | /api/run/1/authorization/{state}\_\_\_get |
| Run Run - Check Flow State Changes | Run Run Used to check if a change has occurred to the state, by comparing the current state token of the state with the provided one Check Flow State Changes  | /api/run/1/state/{stateId}/ping/{stateToken}\_\_\_get |
| Run Run - Export State | Run Run Used to export a state from the tenant into JSON.  <aside class="alert alert-info"> Users only have permissions to export States for which they are the Running User. </aside> Export State  | /api/run/1/state/package/{stateId}\_\_\_get |
| Run Run - Get Authentication Context | Run Run When you initialize a Flow, you are provided with the authentication context in the response.  However, you can also retrieve and login to Services individually.  It’s important to note that despite authentication being done against a Flow state, the returned Runtime Authentication Token is valid across all Flow States. Get Authentication Context  | /api/run/1/authentication/{stateId}\_\_\_get |
| Run Run - Get Flow State History | Run Run Returns a sequence of visited map elements (excluding the current one) in the order of visiting them Get Flow State History  | /api/run/1/state/{stateId}/history\_\_\_get |
| Run Run - Get Flow State Value | Run Run Used to get the value for the id provided in the flow state Get Flow State Value  | /api/run/1/state/{stateId}/values/{id}\_\_\_get |
| Run Run - Get Flow State Value by Name | Run Run Used to get the value for the name provided in the flow state Get Flow State Value by Name  | /api/run/1/state/{stateId}/values/name/{name}\_\_\_get |
| Run Run - Get Flow State Values | Run Run Used to get all the values in the flow state. This endpoint requires a runtime authentication token, which means it can only be used with non-public flows. Get Flow State Values  | /api/run/1/state/{stateId}/values\_\_\_get |
| Run Run - Join Flow State | Run Run Used to join the state provided Join Flow State  | /api/run/1/state/{stateId}\_\_\_get |
| Service Invoker - Get Invoker Requests | Service Invoker Get the metadata for a specific request sent to a service Get Invoker Requests  | /api/service/1/requests/{id}\_\_\_get |
| Service Invoker - List Flow Invoker Requests | Service Invoker Get the metadata for every request sent to a Service from a specific Flow List Flow Invoker Requests  | /api/service/1/requests/flow/{id}\_\_\_get |
| Service Invoker - List Flow Version Invoker Requests | Service Invoker Get the metadata for every request sent to a Service from a specific version of a Flow List Flow Version Invoker Requests  | /api/service/1/requests/flow/{id}/{version}\_\_\_get |
| Service Invoker - List Invoker Requests | Service Invoker Get the metadata for every request sent to a Service List Invoker Requests  | /api/service/1/requests\_\_\_get |
| Service Invoker - List State Invoker Requests | Service Invoker Get the metadata for every request sent to a Service from a specific State List State Invoker Requests  | /api/service/1/requests/state/{id}\_\_\_get |
| Service Invoker - Retry Invoker Requests | Service Invoker Send the request to the service again Retry Invoker Requests  | /api/service/1/requests/{id}/retry\_\_\_get |
| Translate Cultures - Get Content Value Culture | Translate Cultures Used to get an existing content value culture. Get Content Value Culture  | /api/translate/1/culture/{id}\_\_\_get |
| Translate Cultures - List Content Value Cultures | Translate Cultures Used to get existing content value cultures. List Content Value Cultures  | /api/translate/1/culture\_\_\_get |
| Translate Flow - Get Flow Translation | Translate Flow Get Flow Translation  | /api/translate/1/flow/{id}\_\_\_get |
| Translate Flow - List Flow Translations | Translate Flow Used to filter existing flow objects that are available for translation.  ### Filter  The filter can take the following formats:  * <code>developerName eq '{developer_name}'</code>: Filter the list of flows where the <code>developerName</code> property exactly matches the provided developer name (case insensitive) * <code>substringof(developerName, '{developer_name}')</code>: Filter the list of flows where the <code>developerName</code> property partially matches the provided developer name (case insensitive) List Flow Translations  | /api/translate/1/flow\_\_\_get |
| Translate Map Element - Get Map Element Translation | Translate Map Element Get Map Element Translation  | /api/translate/1/flow/{flow}/{editingToken}/element/map/{id}\_\_\_get |
| Translate Navigation Element - Get Navigation Translation | Translate Navigation Element Get Navigation Translation  | /api/translate/1/flow/{flow}/{editingToken}/element/navigation/{id}\_\_\_get |
| Translate Page Element - Get Page Transation | Translate Page Element Get Page Transation  | /api/translate/1/element/page/{id}\_\_\_get |
| Translate Type Element - Get Type Translation | Translate Type Element Get Type Translation  | /api/translate/1/element/type/{id}\_\_\_get |
| Translate Value Element - Get Value Translation | Translate Value Element Get Value Translation  | /api/translate/1/element/value/{id}\_\_\_get |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Admin Runtimes - Load Runtime | Admin Runtimes Loads an associated runtime by ID in the current organization. Load Runtime  | /api/admin/1/organization/runtimes/{id}\_\_\_get |
| Admin States - List States for a Flow | Admin States Get a listing of all the flow states for a specific flow in the current tenant. List States for a Flow  | /api/admin/1/states/flow/{id}\_\_\_get |
| Admin States - Load State | Admin States Load an individual flow state Load State  | /api/admin/1/states/{id}\_\_\_get |
| Admin Stores - Find Store | Admin Stores Finds a store by ID, in the current tenant. Find Store  | /api/admin/1/stores/{id}\_\_\_get |
| Admin Users - Load User | Admin Users Load an individual user from the current tenant. Load User  | /api/admin/1/users/{id}\_\_\_get |
| Draw Authentication - Switch Tenant | Draw Authentication Switch Tenant  | /api/draw/1/authentication/{tenant}\_\_\_get |
| Draw Draw - List Element Dependencies | Draw Draw List the flattened tree of elements that a specific element depends on, including all registered dependencies of dependencies. List Element Dependencies  | /api/draw/1/dependencies/{id}\_\_\_get |
| Draw Draw - List Element Dependents | Draw Draw List the flattened tree of elements that depends on a specific element. List Element Dependents  | /api/draw/1/dependents/{id}\_\_\_get |
| Draw Flow - Get Flow | Draw Flow Used to get an existing flow by ID. Get Flow  | /api/draw/1/flow/{id}\_\_\_get |
| Draw Flow Graph - Get Flow Graph | Draw Flow Graph Used to get an existing flow graph. The flow graph provides the coordinate and basic configuration information of map and group elements. Get Flow Graph  | /api/draw/1/graph/flow/{flow}\_\_\_get |
| Draw Flow Snapshot - List Flow Snapshots | Draw Flow Snapshot Used to get the list of all snapshots for a particular flow. List Flow Snapshots  | /api/draw/1/flow/snap/{flow}\_\_\_get |
| Draw Macro Element - Get Macro Element | Draw Macro Element Used to get an existing macro element. Get Macro Element  | /api/draw/1/element/macro/{id}\_\_\_get |
| Draw Page Element - Get Page Element | Draw Page Element Used to get an existing page element. Get Page Element  | /api/draw/1/element/page/{id}\_\_\_get |
| Draw Service Element - Get Service Element | Draw Service Element Used to get an existing service element. Get Service Element  | /api/draw/1/element/service/{id}\_\_\_get |
| Draw Tag Element - Get Tag Element | Draw Tag Element Used to get an existing tag element. Get Tag Element  | /api/draw/1/element/tag/{id}\_\_\_get |
| Draw Type Element - Get Type Element | Draw Type Element Used to get an existing type element. Get Type Element  | /api/draw/1/element/type/{id}\_\_\_get |
| Draw Value Element - Get Value Element | Draw Value Element Used to get an existing value element. Get Value Element  | /api/draw/1/element/value/{id}\_\_\_get |
| Notifications Notifications - Finds a notification for the current user | Notifications Notifications Finds the notification and marks it as read Finds a notification for the current user  | /api/notifications/1/{id}\_\_\_get |
| Package Package - Create Package for Flow | Package Package Create a package of the latest version of a flow snapshot Create Package for Flow  | /api/package/1/flow/{id}\_\_\_get |
| Run Flow - Load Flow by ID | Run Flow Load Flow by ID  | /api/run/1/flow/{id}\_\_\_get |
| Run Flow - Load Flow by Name | Run Flow Load Flow by Name  | /api/run/1/flow/name/{name}\_\_\_get |
| Run Run - Authorization Check | Run Run Check if the currently authenticated user has permission to access the state at its current position Authorization Check  | /api/run/1/authorization/{state}\_\_\_get |
| Run Run - Export State | Run Run Used to export a state from the tenant into JSON.  <aside class="alert alert-info"> Users only have permissions to export States for which they are the Running User. </aside> Export State  | /api/run/1/state/package/{stateId}\_\_\_get |
| Run Run - Get Authentication Context | Run Run When you initialize a Flow, you are provided with the authentication context in the response.  However, you can also retrieve and login to Services individually.  It’s important to note that despite authentication being done against a Flow state, the returned Runtime Authentication Token is valid across all Flow States. Get Authentication Context  | /api/run/1/authentication/{stateId}\_\_\_get |
| Run Run - Join Flow State | Run Run Used to join the state provided Join Flow State  | /api/run/1/state/{stateId}\_\_\_get |
| Service Invoker - Get Invoker Requests | Service Invoker Get the metadata for a specific request sent to a service Get Invoker Requests  | /api/service/1/requests/{id}\_\_\_get |
| Service Invoker - List Flow Invoker Requests | Service Invoker Get the metadata for every request sent to a Service from a specific Flow List Flow Invoker Requests  | /api/service/1/requests/flow/{id}\_\_\_get |
| Service Invoker - List State Invoker Requests | Service Invoker Get the metadata for every request sent to a Service from a specific State List State Invoker Requests  | /api/service/1/requests/state/{id}\_\_\_get |
| Translate Cultures - Get Content Value Culture | Translate Cultures Used to get an existing content value culture. Get Content Value Culture  | /api/translate/1/culture/{id}\_\_\_get |
| Translate Flow - Get Flow Translation | Translate Flow Get Flow Translation  | /api/translate/1/flow/{id}\_\_\_get |
| Translate Page Element - Get Page Transation | Translate Page Element Get Page Transation  | /api/translate/1/element/page/{id}\_\_\_get |
| Translate Type Element - Get Type Translation | Translate Type Element Get Type Translation  | /api/translate/1/element/type/{id}\_\_\_get |
| Translate Value Element - Get Value Translation | Translate Value Element Get Value Translation  | /api/translate/1/element/value/{id}\_\_\_get |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |


### UPDATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Admin Admin - Apply Password Reset (POST) | Admin Admin Apply the actual password change for an account, using the token generated from the notification callback sent in the "Reset Password" endpoint.  The token is not provided in the notification, but rather the token is provided after the user clicks on the notification link. The token will either be parsed into the provided <code>redirectUrl</code> (if specified) or provided in the REST response from a GET request to the notification callback URL provided in the notification. Apply Password Reset  | /api/admin/1/directory/user/credential/{token}\_\_\_post |
| Admin Runtimes - Update Runtime (PUT) | Admin Runtimes Updates a runtime by ID in the current organization.              Runtimes can be associated with one or more tenants, and any changes to properties take effect globally, and are reflected across all associated tenants. Update Runtime  | /api/admin/1/organization/runtimes/{id}\_\_\_put |
| Admin Stores - Update Store (PUT) | Admin Stores Updates a store by ID, in the current tenant. Update Store  | /api/admin/1/stores/{id}\_\_\_put |
| Admin Users - Update the Current User (PUT) | Admin Users Update the currently-authenticated user's information Update the Current User  | /api/admin/1/users/me\_\_\_put |
| Admin Users - Update the Current User's settings (PUT) | Admin Users Update the settings for the currently-authenticated user Update the Current User's settings  | /api/admin/1/users/me/settings\_\_\_put |
| Admin Users - Update User (PUT) | Admin Users Update information for an individual user in the current tenant. Update User  | /api/admin/1/users/{id}\_\_\_put |
| AdminOrganizationInviteTenants - Accept Tenant Invitation (PUT) | AdminOrganizationInviteTenants Accepts a pending invitation for a tenant to join an organization. Accept Tenant Invitation  | /api/admin/1/organization/invites/tenants/{id}\_\_\_put |
| AdminOrganizationInviteUsers - Accept User Invitation (PUT) | AdminOrganizationInviteUsers Accepts a pending invitation for a user to join an organization. Accept User Invitation  | /api/admin/1/organization/invites/users/{id}\_\_\_put |
| Draw Assets - Move Asset (PUT) | Draw Assets Move an asset from one location to another (can also be used to rename an asset). Move Asset  | /api/draw/1/assets\_\_\_put |
| Draw Flow - Import Element into Flow (POST) | Draw Flow Used to import an existing element into a flow Import Element into Flow  | /api/draw/1/element/flow/{flow}/{elementType}/{id}\_\_\_post |
| Draw Flow Snapshot - Activate a Flow Snapshot (POST) | Draw Flow Snapshot Used to activate and/or make default a flow snapshot version Activate a Flow Snapshot  | /api/draw/1/flow/activation/{flow}/{version}/{isDefault}/{isActivated}\_\_\_post |
| Draw Flow Snapshot - Create Flow Snapshot (POST) | Draw Flow Snapshot Used to create a flow snapshot. Create Flow Snapshot  | /api/draw/1/flow/snap/{flow}\_\_\_post |
| Draw Flow Snapshot - Revert a Flow Snapshot (POST) | Draw Flow Snapshot Used to take an flow snapshot and apply it to the current flow being modelled. This is equivalent to undoing changes to a flow for all flow builders.  To revert a flow snapshot for running users, simply activate and make default the appropriate previous flow snapshot version.  <aside class="alert alert-warning"> If the flow being actively edited by other flow builders does not have a flow snapshot, all changes will be lost. The platform can only recover old flow versions from snapshots. </aside> Revert a Flow Snapshot  | /api/draw/1/flow/revert/{flow}/{version}\_\_\_post |
| Play Play - Update Player (POST) | Play Play The player content should be sent as <code>application/x-www-form-urlencoded; charset=UTF8</code> request with the body of the request set to <code>=player content goes here</code> Update Player  | /{tenantId}/play/{playerName}\_\_\_post |
| Run Run - Authenticate (POST) | Run Run Authenticate with the given authentication credentials to the given state Authenticate  | /api/run/1/authentication/{stateId}\_\_\_post |
| Run Run - Flow Out (POST) | Run Run Initiate a flow out from an Outcome that is configured with a Flow Out.  <aside class="alert alert-info"> If the flow out state is already running, it will be joined.  Otherwise, it will be initialised. </aside> Flow Out  | /api/run/1/state/out/{stateId}/{selectedOutcomeId}\_\_\_post |
| Run Run - Get Navigation (POST) | Run Run Activate a navigation and if the current Flow state is on a navigation item, then that item is highlighted. Get Navigation  | /api/run/1/navigation/{stateId}\_\_\_post |
| Run Run - Invoke Flow State (POST) | Run Run Used to invoke a flow state Invoke Flow State  | /api/run/1/state/{stateId}\_\_\_post |


