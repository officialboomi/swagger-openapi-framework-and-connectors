# Visa Cybersource Connector
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### CyberSource Service Definition

The URL for the Cybersource descriptor file

**Type** - string

##### Allowed Values

 * Payments
 * Transaction Search and Details


#### Service URL

The server name for the CyberSource service

**Type** - string

**Default Value** - https://api.cybersource.com



#### Merchant ID

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string



#### Key ID

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - password



#### Key Value

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - password

# Operation Tab


## CREATE


## GET
# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| AuthReversal - Process an Authorization Reversal | reversal Include the payment ID in the POST request to reverse the payment amount. Process an Authorization Reversal  | /v2/payments/{id}/reversals\_\_\_post |
| CapturePayment - Capture a Payment | capture Include the payment ID in the POST request to capture the payment amount. Capture a Payment  | /v2/payments/{id}/captures\_\_\_post |
| CreateCredit - Process a Credit | credit POST to the credit resource to credit funds to a specified credit card. Process a Credit  | /v2/credits/\_\_\_post |
| CreatePayment - Process a Payment | payment Authorize the payment for the transaction.  Process a Payment  | /v2/payments/\_\_\_post |
| RefundCapture - Refund a Capture | refund Include the capture ID in the POST request to refund the captured amount.  Refund a Capture  | /v2/captures/{id}/refunds\_\_\_post |
| RefundPayment - Refund a Payment | refund Include the payment ID in the POST request to refund the payment amount.  Refund a Payment  | /v2/payments/{id}/refunds\_\_\_post |
| VoidCapture - Void a Capture | voId Include the capture ID in the POST request to cancel the capture. Void a Capture  | /v2/captures/{id}/voids\_\_\_post |
| VoidCredit - Void a Credit | voId Include the credit ID in the POST request to cancel the credit. Void a Credit  | /v2/credits/{id}/voids\_\_\_post |
| VoidPayment - Void a Payment | voId Include the payment ID in the POST request to cancel the payment. Void a Payment  | /v2/payments/{id}/voids\_\_\_post |
| VoidRefund - Void a Refund | voId Include the refund ID in the POST request to cancel the refund. Void a Refund  | /v2/refunds/{id}/voids\_\_\_post |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| GetAuthReversal - Retrieve an Authorization Reversal | reversal Include the authorization reversal ID in the GET request to retrieve the authorization reversal details.  Retrieve an Authorization Reversal  | /v2/reversals/{id}\_\_\_get |
| GetCapture - Retrieve a Capture | capture Include the capture ID in the GET request to retrieve the capture details.  Retrieve a Capture  | /v2/captures/{id}\_\_\_get |
| GetCredit - Retrieve a Credit | credit Include the credit ID in the GET request to return details of the credit. Retrieve a Credit  | /v2/credits/{id}\_\_\_get |
| GetPayment - Retrieve a Payment | payment Include the payment ID in the GET request to retrieve the payment details. Retrieve a Payment  | /v2/payments/{id}\_\_\_get |
| GetRefund - Retrieve a Refund | refund Include the refund ID in the GET request to to retrieve the refund details. Retrieve a Refund  | /v2/refunds/{id}\_\_\_get |
| GetVoId - Retrieve A VoId | voId Include the void ID in the GET request to retrieve the void details. Retrieve A VoId  | /v2/voids/{id}\_\_\_get |


