# Workday
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### URL

The URL for the Workday API service

**Type** - string



#### Workday OpenAPI Service

The URL for the Workday OpenAPI descriptor file

**Type** - string

##### Allowed Values

 * absenceManagement_v1_2020R2Swagger
 * accountsPayable_v1_2020R2Swagger
 * budgets_v1_2020R2Swagger
 * businessProcess_v1_2020R2Swagger
 * common_v1_2020R2Swagger
 * compensation_v1_2020R2Swagger
 * compensation_v2_2020R2Swagger
 * customerAccounts_v1_2020R2Swagger
 * expense_v1_2020R2Swagger
 * globalPayroll_v1_2020R2Swagger
 * holiday_v1_2020R2Swagger
 * payroll_v1_2020R2Swagger
 * performanceEnablement_v3_2020R2Swagger
 * performanceManagement_v1_2020R2Swagger
 * performanceManagement_v2_2020R2Swagger
 * person_v1_2020R2Swagger
 * procurement_v1_2020R2Swagger
 * procurement_v2_2020R2Swagger
 * projects_v1_2020R2Swagger
 * recruiting_v1_2020R2Swagger
 * recruiting_v2_2020R2Swagger
 * request_v1_2020R2Swagger
 * revenue_v1_2020R2Swagger
 * staffing_v1_2020R2Swagger
 * staffing_v2_2020R2Swagger
 * studentAcademicFoundation_v1_2020R2Swagger
 * studentCore_v1_2020R2Swagger
 * studentCurriculum_v1_2020R2Swagger
 * studentEngagement_v1_2020R2Swagger
 * studentFinance_v1_2020R2Swagger
 * systemMetrics_v1_2020R2Swagger
 * talentManagement_v1_2020R2Swagger
 * timeTracking_v1_2020R2Swagger


#### Other Oracle REST API Swagger URL

This will override selections so you can provide a url to any swagger file.

**Type** - string



#### Workday Tenant ID

Specify the ID of the Workday instance for your company. This is used to construct the full URL for the REST API.

**Type** - string



#### AuthenticationType

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - oauth

# Operation Tab


## CREATE


## UPDATE


## DELETE


## GET


## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1


### Query Options


#### Fields

The connector does not support field selection. All fields will be returned by default.


#### Filters

The query filter supports any number of non-nested expressions which will be AND'ed together.

Example:
((foo lessThan 30) AND (baz lessThan 42))

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts




# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

## Object Operations Provided

| Object | Query | Get | Create | Update | Delete 
| --- |:---:|:---:|:---:|:---:|:---:|
| absenceManagement_v1_2020R2Swagger | X | X |  |  |  |
| accountsPayable_v1_2020R2Swagger | X | X | X |  |  |
| budgets_v1_2020R2Swagger |  |  | X |  |  |
| businessProcess_v1_2020R2Swagger | X | X | X |  |  |
| common_v1_2020R2Swagger | X | X | X |  |  |
| compensation_v1_2020R2Swagger | X | X | X |  |  |
| compensation_v2_2020R2Swagger | X | X | X | X | X |
| customerAccounts_v1_2020R2Swagger | X | X | X |  |  |
| expense_v1_2020R2Swagger | X | X | X | X | X |
| globalPayroll_v1_2020R2Swagger | X | X |  | X |  |
| holiday_v1_2020R2Swagger | X |  |  |  |  |
| payroll_v1_2020R2Swagger | X | X | X | X | X |
| performanceEnablement_v3_2020R2Swagger | X | X | X |  |  |
| performanceManagement_v1_2020R2Swagger | X | X | X |  |  |
| performanceManagement_v2_2020R2Swagger | X | X | X |  |  |
| person_v1_2020R2Swagger | X | X | X |  |  |
| procurement_v1_2020R2Swagger | X | X | X | X | X |
| procurement_v2_2020R2Swagger | X | X | X | X | X |
| projects_v1_2020R2Swagger | X | X | X | X |  |
| recruiting_v1_2020R2Swagger | X | X | X |  |  |
| recruiting_v2_2020R2Swagger | X | X | X |  |  |
| request_v1_2020R2Swagger | X | X | X |  |  |
| revenue_v1_2020R2Swagger | X | X |  | X |  |
| staffing_v1_2020R2Swagger | X | X | X | X | X |
| staffing_v2_2020R2Swagger (1) | X | X | X | X | X |
| staffing_v2_2020R2Swagger | X | X | X | X | X |
| studentAcademicFoundation_v1_2020R2Swagger | X | X |  |  |  |
| studentCore_v1_2020R2Swagger | X | X |  |  |  |
| studentCurriculum_v1_2020R2Swagger | X | X |  |  |  |
| studentEngagement_v1_2020R2Swagger | X | X |  |  |  |
| studentFinance_v1_2020R2Swagger | X | X | X | X |  |
| systemMetrics_v1_2020R2Swagger | X | X |  |  |  |
| talentManagement_v1_2020R2Swagger | X | X | X |  |  |
| timeTracking_v1_2020R2Swagger | X | X | X | X | X |
