# SolarWinds Service Desk API
This contector enables Boomi developers to build processes that call the SolarWinds Service Desk API.

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string



#### SolarWinds Service Desk API REST API Service URL

The URL for the SolarWinds Service Desk API REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### AuthenticationType

Allows user to select between BASIC and OAUTH 2.0 Authentication

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE/POST
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE/PATCH
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE/PUT
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| DeleteAssetById - Delete other asset | Other Asset Delete other asset  |
| DeleteCatalogItemById - Delete catalog item | Catalog Item Delete catalog item  |
| DeleteCategoryById - Delete category | Category Delete category  |
| DeleteChangeById - Delete change | Change Delete change  |
| DeleteChangeCatalogById - Delete change catalog | Change Catalog Delete change catalog  |
| DeleteCommentById - Delete comment | Comment Delete comment  |
| DeleteConfigurationItemById - Delete configurationitem | Configuration Item Delete configurationitem  |
| DeleteContractById - Delete contract | Contract Delete contract  |
| DeleteDepartmentById - Delete department | Department Delete department  |
| DeleteGroupById - Delete group | Group Delete group  |
| DeleteHardwareById - Delete hardware | Hardware Delete hardware  |
| DeleteIncidentById - Delete incident | Incident Delete incident  |
| DeleteitemById - Delete contract's item | Contract Delete contract's item  |
| DeleteMemebershipById - Delete membership. Example: curl -H "X-Samanage-Au... | Membership Delete membership. Example: curl -H "X-Samanage-Authorization: Bearer TOKEN" -H 'Accept: application/xml' -H 'Content-Type:text/xml' -X DELETE https://api.samanage.com/memberships/1.xml  |
| DeleteMobileById - Delete mobile device | Mobile Device Delete mobile device  |
| DeleteProblemById - Delete problem | Problem Delete problem  |
| DeletePurchaseById - Delete purchase | Purchase Delete purchase  |
| DeletePurchaseOrderById - Delete purchase order | Purchase Order Delete purchase order  |
| DeleteReleaseById - Delete release | Release Delete release  |
| DeleteRoleById - Delete role | Role Delete role  |
| DeleteSiteById - Delete site | Site Delete site  |
| DeleteSolutionById - Delete solution | Solution Delete solution  |
| DeleteTaskById - Delete task | Task Delete task  |
| DeleteTimeTrackyById - Delete time track | Time Track Delete time track  |
| DeleteUserById - Delete user | User Delete user  |
| DeleteVendorById - Delete vendor | Vendor Delete vendor  |
| DeleteWarrantyById - Delete warranty | Hardware Delete warranty  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| GetAssetById - Get other asset | Other Asset Get other asset  |
| GetCatalogItemById - Get catalog item | Catalog Item Get catalog item  |
| GetCategoryById - Get category | Category Get category  |
| GetChangeById - Get change | Change Get change  |
| GetChangeCatalogById - Get change catalog | Change Catalog Get change catalog  |
| GetConfigurationItemById - Get configuration item | Configuration Item Get configuration item  |
| GetContractById - Get contract | Contract Get contract  |
| GetDepartmentById - Get department | Department Get department  |
| GetGroupById - Get group | Group Get group  |
| GetHardwareById - Get hardware | Hardware Get hardware  |
| GetIncidentById - Get incident | Incident Get incident  |
| GetMobileById - Get mobile device | Mobile Device Get mobile device  |
| GetPrinterById - Get printer | Printer Get printer  |
| GetProblemById - Get problem | Problem Get problem  |
| GetPurchaseOrderById - Get purchase order | Purchase Order Get purchase order  |
| GetReleaseById - Get release | Release Get release  |
| GetRoleById - Get role | Role Get role  |
| GetSiteById - Get site | Site Get site  |
| GetSoftwareById - Get software | Software Get software  |
| GetSolutionById - Get solution | Solution Get solution  |
| GetUserById - Get user | User Get user  |
| GetVendorById - Get vendor | Vendor Get vendor  |


### PATCH Operation Types
| Label | Help Text |
| --- | --- |


### POST Operation Types
| Label | Help Text |
| --- | --- |
| CreateAsset - Create new asset | Other Asset Create new asset  |
| CreateAttachment - Create a new attachment: curl -H 'X-Samanage-Autho... | Attachment Create a new attachment: <p>curl -H 'X-Samanage-Authorization: Bearer TOKEN' <br>-F 'file[attachable_type]=PARENT_OBJECT_TYPE' <br>-F 'file[attachable_id]=PARENT_OBJECT_ID' <br>-F 'file[attachment]=@/PATH/TO/FILE' <br>-H 'Accept: application/vnd.samanage.v1.3+xml' <br>-H 'Content-Type: multipart/form-data' <br>-X POST https://api.samanage.com/attachments.xml/json </p>  |
| CreateCatalogItem - Create new catalog item | Catalog Item Create new catalog item  |
| CreateCategory - Create new category | Category Create new category  |
| CreateChange - Create new change | Change Create new change  |
| CreateChangeCatalog - Create new change catalog | Change Catalog Create new change catalog  |
| CreateChangeRequest - Request a Change | Change Request Request a Change  |
| CreateComment - Create new Comment | Comment Create new Comment  |
| CreateConfigurationItem - Create new configurationitem | Configuration Item Create new configurationitem  |
| CreateContract - Create new contract | Contract Create new contract  |
| CreateDepartment - Create new department | Department Create new department  |
| CreateGroup - Create new group | Group Create new group  |
| CreateHardware - Create new hardware | Hardware Create new hardware  |
| CreateIncident - Create new incident | Incident Create new incident  |
| CreateItem - Create new contract's Item | Contract Create new contract's Item  |
| CreateMembership - Create new Membership. Memberships are used to rel... | Membership Create new Membership. Memberships are used to relate a user to a group. Example: curl -H "X-Samanage-Authorization: Bearer TOKEN" -H 'Accept: application/xml' -H 'Content-Type:text/xml' -X POST https://api.samanage.com/memberships.xml?group_id=1&user_ids=1  |
| CreateMobile - Create new mobile device | Mobile Device Create new mobile device  |
| CreateProblem - Create new problem | Problem Create new problem  |
| CreatePurchase - Create new purchase | Purchase Create new purchase  |
| CreatePurchaseOrder - Create new purchase order | Purchase Order Create new purchase order  |
| CreateRelease - Create new release | Release Create new release  |
| CreateRole - Create new role | Role Create new role  |
| CreateServiceRequest - Request a Service | Service Request Request a Service  |
| CreateSite - Create new site | Site Create new site  |
| CreateSolution - Create new solution | Solution Create new solution  |
| CreateTask - Create new Task | Task Create new Task  |
| CreateTimeTrack - Create new time track | Time Track Create new time track  |
| CreateUser - Create new user | User Create new user  |
| CreateVendor - Create new vendor | Vendor Create new vendor  |
| CreateWarranty - Create new Warranty | Hardware Create new Warranty  |


### PUT Operation Types
| Label | Help Text |
| --- | --- |
| AppendDependentAssets - Append multiple dependent assets (PUT) | Configuration Item Append multiple dependent assets  |
| DeleteAssetLink - Delete dpendency by id (PUT) | Configuration Item Delete dpendency by id  |
| UpdateAssetById - Update other asset with specified fields (PUT) | Other Asset Update other asset with specified fields  |
| UpdateCatalogItemById - Update catalog item with specified fields (PUT) | Catalog Item Update catalog item with specified fields  |
| UpdateCategoryById - Update category with specified fields (PUT) | Category Update category with specified fields  |
| UpdateChangeById - Update change with specified fields (PUT) | Change Update change with specified fields  |
| UpdateChangeCatalogById - Update change catalog with specified fields (PUT) | Change Catalog Update change catalog with specified fields  |
| UpdateCommentById - Update comment with specified fields (PUT) | Comment Update comment with specified fields  |
| UpdateConfigurationItemById - Update configuration item with specified fields (PUT) | Configuration Item Update configuration item with specified fields  |
| UpdateContractById - Update contract with specified fields (PUT) | Contract Update contract with specified fields  |
| UpdateDepartmentById - Update department with specified fields (PUT) | Department Update department with specified fields  |
| UpdateGroupById - Update group with specified fields (PUT) | Group Update group with specified fields  |
| UpdateHardwareById - Update hardware with specified fields (PUT) | Hardware Update hardware with specified fields  |
| UpdateIncidentById - Update incident with specified fields (PUT) | Incident Update incident with specified fields  |
| UpdateItemById - Update contract's item with specified fields (PUT) | Contract Update contract's item with specified fields  |
| UpdateMobileById - Update mobile device with specified fields (PUT) | Mobile Device Update mobile device with specified fields  |
| UpdatePrinterById - Update printer with specified fields (PUT) | Printer Update printer with specified fields  |
| UpdateProblemById - Update problem with specified fields (PUT) | Problem Update problem with specified fields  |
| UpdatePurchaseById - Update purchase with specified fields (PUT) | Purchase Update purchase with specified fields  |
| UpdatePurchaseOrderById - Update purchase order with specified fields (PUT) | Purchase Order Update purchase order with specified fields  |
| UpdateReleaseById - Update release with specified fields (PUT) | Release Update release with specified fields  |
| UpdateRoleById - Update role with specified fields (PUT) | Role Update role with specified fields  |
| UpdateSiteById - Update site with specified fields (PUT) | Site Update site with specified fields  |
| UpdateSolutionById - Update solution with specified fields (PUT) | Solution Update solution with specified fields  |
| UpdateTaskById - Update task with specified fields (PUT) | Task Update task with specified fields  |
| UpdateTimeTrackyById - Update time track with specified fields (PUT) | Time Track Update time track with specified fields  |
| UpdateUserById - Update user with specified fields (PUT) | User Update user with specified fields  |
| UpdateVendorById - Update vendor with specified fields (PUT) | Vendor Update vendor with specified fields  |
| UpdateWarrantyById - Update warranty with specified fields (PUT) | Hardware Update warranty with specified fields  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| GetAssets - List of other assets | Other Asset List of other assets  |
| GetAuditById - Get audits of a specific object. Example: curl -X ... | Audit Get audits of a specific object. Example: curl -X GET "https://api.samanage.com/incidents/1/audits" -H  "accept: application/json" -H  "X-Samanage-Authorization: Bearer TOKEN"  |
| GetAudits - List of audits | Audit List of audits  |
| GetCatalogItems - List of catalog items | Catalog Item List of catalog items  |
| GetCategories - List of categories | Category List of categories  |
| GetChangeCatalogs - List of change catalogs | Change Catalog List of change catalogs  |
| GetChanges - List of changes | Change List of changes  |
| GetConfigurationItems - List of configuration items | Configuration Item List of configuration items  |
| GetContracts - List of contracts | Contract List of contracts  |
| GetCPurchaseOrders - List of purchase orders | Purchase Order List of purchase orders  |
| GetDepartments - List of Departments | Department List of Departments  |
| GetGroups - List of groups | Group List of groups  |
| GetHardwares - List of hardwares | Hardware List of hardwares  |
| GetIncidents - List of incidents | Incident List of incidents  |
| GetMobiles - List of mobile devices | Mobile Device List of mobile devices  |
| GetPrinters - List of printers | Printer List of printers  |
| GetProblems - List of problems | Problem List of problems  |
| GetRelease - List of release | Release List of release  |
| GetRisks - List of risks | Risk List of risks  |
| GetRoles - List of roles | Role List of roles  |
| GetSites - List of Sites | Site List of Sites  |
| GetSoftwares - List of softwares | Software List of softwares  |
| GetSolutions - List of solutions | Solution List of solutions  |
| GetTimeTracks - List of time tracks | Time Track List of time tracks  |
| GetUsers - List of Users | User List of Users  |
| GetVendors - List of vendors | Vendor List of vendors  |
| GetWarranties - List of warranties | Hardware List of warranties  |


