# This connector provides access to the Sage Business Cloud APIs. For more information, click here: https://developer.sage.com/api/
This connector provides access to the Sage Business Cloud APIs. For more information, click here: https://developer.sage.com/api/

# Connection Tab
## Connection Fields


#### Sage Business Cloud REST API Service URL

The URL for the Sage Business Cloud REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default openapi.json file so you can provide a url to any swagger file.

**Type** - string



#### URL

The URL for the Service service

**Type** - string



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE/EXECUTE


## UPDATE


## GET


## GET


## DELETE


## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  (Supported Types:  date, number, string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Employeepaydetails - Add Pay Details | Employee Add new pay details for a single employee Add Pay Details  | /employees/{employeeId}/payDetails\_\_\_post |


### DELETE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| EmployeeById - Get Employee by Id | Employee Retrieve a single employee Get Employee by Id  | /employees/{employeeId}\_\_\_get |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| EmployeeById - Get Employee by Id | Employee Retrieve a single employee Get Employee by Id  | /employees/{employeeId}\_\_\_get |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| AbsenceBalances - Get Absence Balances | Absence Balance Retrieve a paginated list of absence balances Get Absence Balances  | /absenceBalances\_\_\_get |
| Absences - Get Absences | Absence Retrieve a paginated list of absences Get Absences  | /absences\_\_\_get |
| Bonuses - Get Bonuses | Bonus Retrieve a paginated list of bonuses Get Bonuses  | /bonuses\_\_\_get |
| Employees - Get Employees | Employee Retrieve a paginated list of employees Get Employees  | /employees\_\_\_get |
| Salaries - Get Salaries | Salary Retrieve a paginated list of salaries Get Salaries  | /salaries\_\_\_get |
| Timesheets - Get Timesheets | Timesheet Retrieve a paginated list of timesheets Get Timesheets  | /timesheets\_\_\_get |


### UPDATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |


