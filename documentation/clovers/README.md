# Clovers
Clovers REST API Connector

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string

**Default Value** - https://ats.webhook.clovers-development.com/

##### Allowed Values

 * Development
 * QA
 * Staging
 * Production


#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### Client ID

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string



#### Signature Secret

The secret for signing data and sending to the server

**Type** - password

# Operation Tab


## CREATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string

# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| AddCandidate - adds a Candidate and an Application item | integration-partners Adds an item to the system adds a Candidate and an Application item  |
| AddHiringTeam - adds a HiringTeam item | integration-partners Adds an item to the system adds a HiringTeam item  |
| AddJob - adds a Job item | integration-partners Adds an item to the system adds a Job item  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |


### GET Operation Types
| Label | Help Text |
| --- | --- |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |


