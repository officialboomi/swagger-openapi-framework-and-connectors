# MPESA Connector
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### OpenAPI Swagger URL

The URL for the OpenAPI descriptor file

**Type** - string

**Default Value** - resources/mpesa/MPESASwagger.json



#### URL

The URL for the OpenAPI service

**Type** - string

**Default Value** - https://sandbox.safaricom.co.ke/mpesa



#### AuthenticationType

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string

**Default Value** - NONE

##### Allowed Values

 * None
 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - oauth

# Operation Tab


## CREATE
# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Account Balance - account balance API | account balance API | /accountbalance/v1/query\_\_\_post |
| B2B - Business to Business | Business to Business | /b2b/v1/paymentrequest\_\_\_post |
| B2C - Business to Customer | Business to Customer | /b2c/v1/paymentrequest\_\_\_post |
| C2B Register URL - Register C2B URLs | Register C2B URLs | /c2b/v1/registerurl\_\_\_post |
| C2B Simulate Transaction - Customer to Business | Customer to Business | /c2b/v1/simulate\_\_\_post |
| Lipa Na Mpesa Online - STK Transaction | STK Transaction | /stkpush/v1/processrequest\_\_\_post |
| Lipa Na Mpesa Online Query - STK Push Query | STK Push Query | /stkpushquery/v1/query\_\_\_post |
| Reversal - Transaction reversal | Transaction reversal | /reversal/v1/request\_\_\_post |
| Transaction Status - Transaction status API | Transaction status API | /transactionstatus/v1/query\_\_\_post |


