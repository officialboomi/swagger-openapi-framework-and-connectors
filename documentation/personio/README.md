# Personio API
This contector enables Boomi developers to build processes that call the Personio APIs.

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string

**Default Value** - https://api.personio.de/v1



#### Personio API REST API Service URL

The URL for the Personio API REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### Client ID

The OAUTH Client ID  for Bullhorn custom OAuth Authentication. Leave blank for Authorization Code authentication.

**Type** - string



#### Client Secret

The OAUTH Client Secret  for Bullhorn custom OAuth Authentication. Leave blank for Authorization Code authentication.

**Type** - password

# Operation Tab


## CREATE/POST
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE/PUT
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Absences - This endpoint is responsible for deleting absence ... | Absences This endpoint is responsible for deleting absence period data for the company employees.  |
| Attendances - This endpoint is responsible for deleting attendan... | Attendances This endpoint is responsible for deleting attendance data for the company employees.  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Absences - Show Absence Period | Absences Show Absence Period  |
| Employees - Lists all the custom atrributes. | Employees Lists all the custom atrributes.  |
| Employees - Retrieve the absence balance for a specific employ... | Employees Retrieve the absence balance for a specific employee  |
| Employees - Show employee by ID | Employees Show employee by ID  |
| Employees - Show employee's profile picture. If profile pictur... | Employees Show employee's profile picture. If profile picture is missing, the 404 error will be thrown. The `Profile Picture` attribute has to be whitelisted.  |


### PATCH Operation Types
| Label | Help Text |
| --- | --- |
| Attendances - This endpoint is responsible for updating attendan... (PATCH) | Attendances This endpoint is responsible for updating attendance data for the company employees. Attributes are not required and if not specified, the current value will be used. It is not possible to change the employee id.  |
| Employees - Update an employee (PATCH) | Employees Update existing employee. Note: Updating of Email field is not currently supported.  Update an employee  |


### POST Operation Types
| Label | Help Text |
| --- | --- |
| Absences - This endpoint is responsible for adding absence da... | Absences This endpoint is responsible for adding absence data for the company employees.  |
| Attendances - This endpoint is responsible for adding attendance... | Attendances This endpoint is responsible for adding attendance data for the company employees. It is possible to add attendances for one or many employees at the same time. The payload sent on the request should be a list of attendance periods, in the form of an array containing attendance period objects.  |
| Auth - Request Authentication Token | Auth Request Authentication Token  |
| Employees - Create an employee | Employees Creates new employee. Status of the employee will be set to `active` if `hire_date` provided is in the past. Otherwise status will be set to `onboarding`. This endpoint responds with `id` of created employee in case of success.  Create an employee  |


### PUT Operation Types
| Label | Help Text |
| --- | --- |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Absences - Provides a list of available absences types. For e... | Absences Provides a list of available absences types. For example 'Paid vacation', 'Parental leave' or 'Home office'  |
| Absences - This endpoint is responsible for fetching absence ... | Absences This endpoint is responsible for fetching absence data for the company employees.The result can be `paginated` and `filtered` by period and/or specific employee/employees. The result contains a list of absence periods.  |
| Attendances - Fetch attendance data for the company employees. T... | Attendances Fetch attendance data for the company employees. The result can be `paginated` and `filtered` by period, the date and/or time they were updated, and/or specific employee/employees. The result contains a list of attendances.  |
| Employees - List Company Employees | Employees List Company Employees  |


