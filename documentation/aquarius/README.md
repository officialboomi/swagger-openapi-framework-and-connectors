# Visa Cybersource Connector
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### OpenAPI Swagger URL

The URL for the OpenAPI descriptor file

**Type** - string

**Default Value** - resources/aquarius/swagger.json



#### URL

The URL for the Service service

**Type** - string



#### AuthenticationType

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - oauth

# Operation Tab


## CREATE


## GET


## DELETE


## UPDATE


## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| ddo - Get a list of DDOs that match with the executed qu... | ddo  Get a list of DDOs that match with the executed query.  | /api/v1/aquarius/assets/ddo/query\_\_\_post |
| ddo - Register DDO of a new asset | ddo  Register DDO of a new asset  | /api/v1/aquarius/assets/ddo\_\_\_post |
| ddo - Validate metadata content. | ddo  Validate metadata content.  | /api/v1/aquarius/assets/ddo/validate\_\_\_post |


### DELETE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| ddo - Retire metadata of an asset | ddo  Retire metadata of an asset  | /api/v1/aquarius/assets/ddo/{did}\_\_\_delete |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| ddo - Get DDO of a particular asset. | ddo  Get DDO of a particular asset.  | /api/v1/aquarius/assets/ddo/{did}\_\_\_get |
| metadata - Get metadata of a particular asset | metadata  Get metadata of a particular asset  | /api/v1/aquarius/assets/metadata/{did}\_\_\_get |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| ddo - Get a list of DDOs that match with the given text. | ddo  Get a list of DDOs that match with the given text.  | /api/v1/aquarius/assets/ddo/query\_\_\_get |
| ddo - Get all asset IDs. | ddo  Get all asset IDs.  | /api/v1/aquarius/assets\_\_\_get |
| ddo - Get DDO of all assets. | ddo  Get DDO of all assets.  | /api/v1/aquarius/assets/ddo\_\_\_get |


### UPDATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| ddo - Update DDO of an existing asset (PUT) | ddo  Update DDO of an existing asset  | /api/v1/aquarius/assets/ddo/{did}\_\_\_put |


