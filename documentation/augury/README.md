# Augury REST API
REST interface to Augury

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string



#### Augury REST API REST API Service URL

The URL for the Augury REST API REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### AuthenticationType

Allows user to select between BASIC and OAUTH 2.0 Authentication

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE/POST
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE/PATCH
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE/PUT
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### DELETE Operation Types
| Label | Help Text |
| --- | --- |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Get-machines-id-features - Retrieve Features for a given Machine ID | Features Point Features are generated for each Sensor Plane of an  Augury-Endpoint installed on a Machine.  This API-Endpoint returns an array of Point Feature objects for a Machine for a given time range.  The default behaviour (providing no optional parameters) is to return the features from the last day for a Machine.  Required parameters:   - AssetID: a unique alphanumeric ID assigned to the machine by the customer   - ID: a unique UUID assigned to every machine by Augury  Retrieve Features for a given Machine ID  |
| Retrieve-hierarchies-machines-health-changes - Retrieve all machine diagnosed health changes unde... | Health Statuses Hierarchies:   - A hierarchy may present a: building, branch, facility or region   - A hierarchy may contain multiple machines  This API aggregates all machine health changes diagnosed for machines associated under the given hierarchy.  A hierarchy unique identifier must be supplied in the following formats:   - ID: a unique UUID assigned to every hierarchy by Augury  A hierarchy machine health status includes:   - Health status changes from multiple different machines   - Each health status change includes the machines general information, the health level and array of faults diagnosed for the machine  Each hierarchy may have zero or more health status changes, which may occur when:   - A machine associated to the hierarchy has a health level deterioration (Eg. going from acceptable to alarm)   - A machine associated to the hierarchy has a health level improvement (Eg. going from alarm to acceptable)   - A machine associated to the hierarchy doesn't change, but a new mechanical fault is detected (Eg. machine is in alarm, diagnosed with bearing wear, and a additional fault is diagnosed)  Optional Filters:   - health_level - filter by one or more specific machine health levels (possible values: acceptable, monitor, alarm, danger)   - fault - filter by one or more fault types (bearing_wear, misalignment, and more...)   - since/until - filter health changes created within a specific time frame Retrieve all machine diagnosed health changes under a hierarchy (by ID)  |
| Retrieve-machines-health-changes - Retrieve machine diagnosed health changes (by Asse... | Health Statuses Each time a monitored machine health is diagnosed a new health status is created.  A machine unique identifier must be supplied in one of the following formats:   - AssetID: a unique alphanumeric ID assigned to the machine by the customer   - ID: a unique UUID assigned to every machine by Augury  The machine health status includes:   - The machine's general information, identifiers, name and mechanical components   - The machine's general health level (acceptable, monitor, alarm or danger)   - An array of faults containing zero or more mechanical failures diagnosed on the machine  Each machine may have zero or more health status changes, which may occur when:   - The machine's health level deteriorated (Eg. going from acceptable to alarm)   - The machine's health level improved (Eg. going from alarm to acceptable)   - The machine's health level doesn't change, but a new mechanical fault is detected (Eg. machine is in alarm, diagnosed with bearing wear, and a additional fault is diagnosed)  Optional Filters:   - health_level - filter by one or more specific machine health levels (possible values: acceptable, monitor, alarm, danger)   - fault - filter by one or more fault types (bearing_wear, misalignment, and more...)   - since/until - filter health changes created within a specific time frame Retrieve machine diagnosed health changes (by AssetId or ID)  |


### PATCH Operation Types
| Label | Help Text |
| --- | --- |


### POST Operation Types
| Label | Help Text |
| --- | --- |
| Create-machines-repairs - This API-Endpoint is in order to create/notify Aug... | Repairs This API-Endpoint is in order to create/notify Augury about a repair made on a specific machine.   This allows us to get additional context regarding the machine's current health status, and assosicate health improvements with the latest repairs done on the machine.   Required parameters:   - AssetID: a unique alphanumeric ID assigned to the machine by the customer   - ID: a unique UUID assigned to every machine by Augury    Payload:   - summary (required): a short description of the repair made   - user_email (required): the email of the user who made the repair (needs to have a corresponding user in Augury, set with the same email)   - repair_date (required): the date at which the repair was made   - repair_cost (optional): the cost of the repair made     - value: the value of the repair cost (Eg. 100.0)     - currency: the currency of the cost, in ISO 4217 format (Eg. USD, EUR, etc)   |
| Genereate-oauth2-token - Generate Access Token | Authentication Headers   - Authorization     - For example for clientId: client_01, clientSecret: secret, the header is:       - Authorization: Basic Y2xpZW50XzAxOnNlY3JldA==     - Note the header in "Try it out" input is ignored     - Instead press the lock in the top right corner, in the pop up enter your clientId as user and clientSecret as password  Body:   - grant_type - should always be client_credentials   - scopes - should always be open_api Generate Access Token  |


### PUT Operation Types
| Label | Help Text |
| --- | --- |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Retrieve-hierarchies-machines-health-changes - Retrieve all machine diagnosed health changes unde... | Health Statuses Hierarchies:   - A hierarchy may present a: building, branch, facility or region   - A hierarchy may contain multiple machines  This API aggregates all machine health changes diagnosed for machines associated under the given hierarchy.  A hierarchy unique identifier must be supplied in the following formats:   - ID: a unique UUID assigned to every hierarchy by Augury  A hierarchy machine health status includes:   - Health status changes from multiple different machines   - Each health status change includes the machines general information, the health level and array of faults diagnosed for the machine  Each hierarchy may have zero or more health status changes, which may occur when:   - A machine associated to the hierarchy has a health level deterioration (Eg. going from acceptable to alarm)   - A machine associated to the hierarchy has a health level improvement (Eg. going from alarm to acceptable)   - A machine associated to the hierarchy doesn't change, but a new mechanical fault is detected (Eg. machine is in alarm, diagnosed with bearing wear, and a additional fault is diagnosed)  Optional Filters:   - health_level - filter by one or more specific machine health levels (possible values: acceptable, monitor, alarm, danger)   - fault - filter by one or more fault types (bearing_wear, misalignment, and more...)   - since/until - filter health changes created within a specific time frame Retrieve all machine diagnosed health changes under a hierarchy (by ID)  |
| Retrieve-machines-health-changes - Retrieve machine diagnosed health changes (by Asse... | Health Statuses Each time a monitored machine health is diagnosed a new health status is created.  A machine unique identifier must be supplied in one of the following formats:   - AssetID: a unique alphanumeric ID assigned to the machine by the customer   - ID: a unique UUID assigned to every machine by Augury  The machine health status includes:   - The machine's general information, identifiers, name and mechanical components   - The machine's general health level (acceptable, monitor, alarm or danger)   - An array of faults containing zero or more mechanical failures diagnosed on the machine  Each machine may have zero or more health status changes, which may occur when:   - The machine's health level deteriorated (Eg. going from acceptable to alarm)   - The machine's health level improved (Eg. going from alarm to acceptable)   - The machine's health level doesn't change, but a new mechanical fault is detected (Eg. machine is in alarm, diagnosed with bearing wear, and a additional fault is diagnosed)  Optional Filters:   - health_level - filter by one or more specific machine health levels (possible values: acceptable, monitor, alarm, danger)   - fault - filter by one or more fault types (bearing_wear, misalignment, and more...)   - since/until - filter health changes created within a specific time frame Retrieve machine diagnosed health changes (by AssetId or ID)  |


