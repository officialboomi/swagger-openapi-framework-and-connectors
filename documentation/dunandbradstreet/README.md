# Dun and Bradstreet Direct API
Dun and Bradstreet Direct API provides the ability to: Identify entities with whom you do business or want to do business. Enrich your information about entities using the extensive D&B data. Research allows customers to place research requests on subject businesses. Monitor changes to entities and content to keep your information up-to-date.For more information please refer to https://directplus.documentation.dnb.com/

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string



#### Dun and Bradstreet Direct API REST API Service URL

The URL for the Dun and Bradstreet Direct API REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### OAuth 2.0 Client Credentials

OAuth 2.0 Client Credentials authentication is available for select end points with specific scope values configured. For more information refer to: https://directplus.documentation.dnb.com/html/pages/Authentication.html

**Type** - oauth

# Operation Tab


## CREATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| DeleteRegistration - Delete a Registration | registrations Provides the ability to delete an existing Monitoring Registration. This is a two-step process: request and confirm. First, submit the request, then submit the confirmation using the acktoken provided in the delete response. Delete a Registration  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| GetRegistration - Get Registration Details | registrations Provides the ability to get the details of an existing Monitoring Registration. Get Registration Details  |


### POST Operation Types
| Label | Help Text |
| --- | --- |


### PUT Operation Types
| Label | Help Text |
| --- | --- |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |


