# Stripe Connector
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### Stripe Swagger URL

The URL for the Stripe Swagger descriptor file

**Type** - string

**Default Value** - resources/stripe/stripeapi.json



#### URL

The URL for the Stripe service

**Type** - string



#### Secret Key

The Secret API Key available from your Stripe dashboard

**Type** - password

# Operation Tab


## CREATE
### Operation Fields


##### Maximum Profile Depth

Limits the number of the depth when parsing recursive references in schema definitions

**Type** - integer

**Default Value** - 30



## UPDATE
### Operation Fields


##### Maximum Profile Depth

Limits the number of the depth when parsing recursive references in schema definitions

**Type** - integer

**Default Value** - 30



## GET
### Operation Fields


##### Maximum Profile Depth

Limits the number of the depth when parsing recursive references in schema definitions

**Type** - integer

**Default Value** - 30



## DELETE


## QUERY
### Operation Fields


##### Maximum Profile Depth

Limits the number of the depth when parsing recursive references in schema definitions

**Type** - integer

**Default Value** - 30



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Page Size

The number of documents per request

**Type** - integer

**Default Value** - 5



##### Offset

The number of rows to skip before beginning to return rows. An offset of 0 is the same as omitting the offset parameter.

**Type** - integer

**Default Value** - 0


### Query Options


#### Fields

The connector does not support field selection. All fields will be returned by default.


#### Filters

The query filter supports any number of non-nested expressions which will be AND'ed together.

Example:
((foo lessThan 30) AND (baz lessThan 42))

#### Filter Operators

 * Equal To  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts




# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.
 * Alternate URL for expanding results, populated with a Link value from a prior query operation
 * Additional URI Query Parameters
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Post3dSecure - Initiate 3D Secure authentication. | <p>Initiate 3D Secure authentication.</p>  | /v1/3d\_secure\_\_\_post |
| PostAccount - Updates a connected <a href="/docs/connect/account... | <p>Updates a connected <a href="/docs/connect/accounts">Express or Custom account</a> by setting the values of the parameters passed. Any parameters not provided are left unchanged. Most parameters can be changed only for Custom accounts. (These are marked <strong>Custom Only</strong> below.) Parameters marked <strong>Custom and Express</strong> are supported by both account types.</p>  <p>To update your own account, use the <a href="https://dashboard.stripe.com/account">Dashboard</a>. Refer to our <a href="/docs/connect/updating-accounts">Connect</a> documentation to learn more about updating accounts.</p>  | /v1/account\_\_\_post |
| PostAccountBankAccounts - Create an external account for a given account. | <p>Create an external account for a given account.</p>  | /v1/account/bank\_accounts\_\_\_post |
| PostAccountExternalAccounts - Create an external account for a given account. | <p>Create an external account for a given account.</p>  | /v1/account/external\_accounts\_\_\_post |
| PostAccountLinks - Creates an AccountLink object that returns a singl... | <p>Creates an AccountLink object that returns a single-use Stripe URL that the user can redirect their user to in order to take them through the Connect Onboarding flow.</p>  | /v1/account\_links\_\_\_post |
| PostAccountLoginLinks - Creates a single-use login link for an Express acc... | <p>Creates a single-use login link for an Express account to access their Stripe dashboard.</p>  <p><strong>You may only create login links for <a href="/docs/connect/express-accounts">Express accounts</a> connected to your platform</strong>.</p>  | /v1/account/login\_links\_\_\_post |
| PostAccountPeople - Creates a new person. | <p>Creates a new person.</p>  | /v1/account/people\_\_\_post |
| PostAccountPersons - Creates a new person. | <p>Creates a new person.</p>  | /v1/account/persons\_\_\_post |
| PostAccounts - With <a href="/docs/connect">Connect</a>, you can ... | <p>With <a href="/docs/connect">Connect</a>, you can create Stripe accounts for your users. To do this, you’ll first need to <a href="https://dashboard.stripe.com/account/applications/settings">register your platform</a>.</p>  <p>For Standard accounts, parameters other than <code>country</code>, <code>email</code>, and <code>type</code> are used to prefill the account application that we ask the account holder to complete.</p>  | /v1/accounts\_\_\_post |
| PostAccountsAccountBankAccounts - Create an external account for a given account. | <p>Create an external account for a given account.</p>  | /v1/accounts/{account}/bank\_accounts\_\_\_post |
| PostAccountsAccountExternalAccounts - Create an external account for a given account. | <p>Create an external account for a given account.</p>  | /v1/accounts/{account}/external\_accounts\_\_\_post |
| PostAccountsAccountLoginLinks - Creates a single-use login link for an Express acc... | <p>Creates a single-use login link for an Express account to access their Stripe dashboard.</p>  <p><strong>You may only create login links for <a href="/docs/connect/express-accounts">Express accounts</a> connected to your platform</strong>.</p>  | /v1/accounts/{account}/login\_links\_\_\_post |
| PostAccountsAccountPeople - Creates a new person. | <p>Creates a new person.</p>  | /v1/accounts/{account}/people\_\_\_post |
| PostAccountsAccountPersons - Creates a new person. | <p>Creates a new person.</p>  | /v1/accounts/{account}/persons\_\_\_post |
| PostAccountsAccountReject - With <a href="/docs/connect">Connect</a>, you may ... | <p>With <a href="/docs/connect">Connect</a>, you may flag accounts as suspicious.</p>  <p>Test-mode Custom and Express accounts can be rejected at any time. Accounts created using live-mode keys may only be rejected once all balances are zero.</p>  | /v1/accounts/{account}/reject\_\_\_post |
| PostApplePayDomains - Create an apple pay domain. | <p>Create an apple pay domain.</p>  | /v1/apple\_pay/domains\_\_\_post |
| PostApplicationFeesIdRefund -  |  **helpText NOT SET IN DESCRIPTOR FILE** | /v1/application\_fees/{id}/refund\_\_\_post |
| PostApplicationFeesIdRefunds - Refunds an application fee that has previously bee... | <p>Refunds an application fee that has previously been collected but not yet refunded. Funds will be refunded to the Stripe account from which the fee was originally collected.</p>  <p>You can optionally refund only part of an application fee. You can do so multiple times, until the entire fee has been refunded.</p>  <p>Once entirely refunded, an application fee can’t be refunded again. This method will raise an error when called on an already-refunded application fee, or when trying to refund more money than is left on an application fee.</p>  | /v1/application\_fees/{id}/refunds\_\_\_post |
| PostCharges - To charge a credit card or other payment source, y... | <p>To charge a credit card or other payment source, you create a <code>Charge</code> object. If your API key is in test mode, the supplied payment source (e.g., card) won’t actually be charged, although everything else will occur as if in live mode. (Stripe assumes that the charge would have completed successfully).</p>  | /v1/charges\_\_\_post |
| PostChargesChargeCapture - Capture the payment of an existing, uncaptured, ch... | <p>Capture the payment of an existing, uncaptured, charge. This is the second half of the two-step payment flow, where first you <a href="#create_charge">created a charge</a> with the capture option set to false.</p>  <p>Uncaptured payments expire exactly seven days after they are created. If they are not captured by that point in time, they will be marked as refunded and will no longer be capturable.</p>  | /v1/charges/{charge}/capture\_\_\_post |
| PostChargesChargeDispute -  |  **helpText NOT SET IN DESCRIPTOR FILE** | /v1/charges/{charge}/dispute\_\_\_post |
| PostChargesChargeDisputeClose -  |  **helpText NOT SET IN DESCRIPTOR FILE** | /v1/charges/{charge}/dispute/close\_\_\_post |
| PostChargesChargeRefund - When you create a new refund, you must specify a C... | <p>When you create a new refund, you must specify a Charge or a PaymentIntent object on which to create it.</p>  <p>Creating a new refund will refund a charge that has previously been created but not yet refunded. Funds will be refunded to the credit or debit card that was originally charged.</p>  <p>You can optionally refund only part of a charge. You can do so multiple times, until the entire charge has been refunded.</p>  <p>Once entirely refunded, a charge can’t be refunded again. This method will raise an error when called on an already-refunded charge, or when trying to refund more money than is left on a charge.</p>  | /v1/charges/{charge}/refund\_\_\_post |
| PostChargesChargeRefunds - Create a refund. | <p>Create a refund.</p>  | /v1/charges/{charge}/refunds\_\_\_post |
| PostCheckoutSessions - Creates a Session object. | <p>Creates a Session object.</p>  | /v1/checkout/sessions\_\_\_post |
| PostCoupons - You can create coupons easily via the <a href="htt... | <p>You can create coupons easily via the <a href="https://dashboard.stripe.com/coupons">coupon management</a> page of the Stripe dashboard. Coupon creation is also accessible via the API if you need to create coupons on the fly.</p>  <p>A coupon has either a <code>percent_off</code> or an <code>amount_off</code> and <code>currency</code>. If you set an <code>amount_off</code>, that amount will be subtracted from any invoice’s subtotal. For example, an invoice with a subtotal of <currency>100</currency> will have a final total of <currency>0</currency> if a coupon with an <code>amount_off</code> of <amount>200</amount> is applied to it and an invoice with a subtotal of <currency>300</currency> will have a final total of <currency>100</currency> if a coupon with an <code>amount_off</code> of <amount>200</amount> is applied to it.</p>  | /v1/coupons\_\_\_post |
| PostCreditNotes - Issue a credit note to adjust the amount of a fina... | <p>Issue a credit note to adjust the amount of a finalized invoice. For a <code>status=open</code> invoice, a credit note reduces its <code>amount_due</code>. For a <code>status=paid</code> invoice, a credit note does not affect its <code>amount_due</code>. Instead, it can result in any combination of the following:</p>  <ul>  <li>Refund: create a new refund (using <code>refund_amount</code>) or link an existing refund (using <code>refund</code>).</li>  <li>Customer balance credit: credit the customer’s balance (using <code>credit_amount</code>) which will be automatically applied to their next invoice when it’s finalized.</li>  <li>Outside of Stripe credit: record the amount that is or will be credited outside of Stripe (using <code>out_of_band_amount</code>).</li> </ul>  <p>For post-payment credit notes the sum of the refund, credit and outside of Stripe amounts must equal the credit note total.</p>  <p>You may issue multiple credit notes for an invoice. Each credit note will increment the invoice’s <code>pre_payment_credit_notes_amount</code> or <code>post_payment_credit_notes_amount</code> depending on its <code>status</code> at the time of credit note creation.</p>  | /v1/credit\_notes\_\_\_post |
| PostCreditNotesIdVoid - Marks a credit note as void. Learn more about <a h... | <p>Marks a credit note as void. Learn more about <a href="/docs/billing/invoices/credit-notes#voiding">voiding credit notes</a>.</p>  | /v1/credit\_notes/{id}/void\_\_\_post |
| PostCustomers - Creates a new customer object. | <p>Creates a new customer object.</p>  | /v1/customers\_\_\_post |
| PostCustomersCustomerBalanceTransactions - Creates an immutable transaction that updates the ... | <p>Creates an immutable transaction that updates the customer’s <a href="/docs/api/customers/object#customer_object-balance"><code>balance</code></a>.</p>  | /v1/customers/{customer}/balance\_transactions\_\_\_post |
| PostCustomersCustomerBankAccounts - When you create a new credit card, you must specif... | <p>When you create a new credit card, you must specify a customer or recipient on which to create it.</p>  <p>If the card’s owner has no default card, then the new card will become the default. However, if the owner already has a default, then it will not change. To change the default, you should <a href="/docs/api#update_customer">update the customer</a> to have a new <code>default_source</code>.</p>  | /v1/customers/{customer}/bank\_accounts\_\_\_post |
| PostCustomersCustomerBankAccountsIdVerify - Verify a specified bank account for a given custom... | <p>Verify a specified bank account for a given customer.</p>  | /v1/customers/{customer}/bank\_accounts/{id}/verify\_\_\_post |
| PostCustomersCustomerCards - When you create a new credit card, you must specif... | <p>When you create a new credit card, you must specify a customer or recipient on which to create it.</p>  <p>If the card’s owner has no default card, then the new card will become the default. However, if the owner already has a default, then it will not change. To change the default, you should <a href="/docs/api#update_customer">update the customer</a> to have a new <code>default_source</code>.</p>  | /v1/customers/{customer}/cards\_\_\_post |
| PostCustomersCustomerSources - When you create a new credit card, you must specif... | <p>When you create a new credit card, you must specify a customer or recipient on which to create it.</p>  <p>If the card’s owner has no default card, then the new card will become the default. However, if the owner already has a default, then it will not change. To change the default, you should <a href="/docs/api#update_customer">update the customer</a> to have a new <code>default_source</code>.</p>  | /v1/customers/{customer}/sources\_\_\_post |
| PostCustomersCustomerSourcesIdVerify - Verify a specified bank account for a given custom... | <p>Verify a specified bank account for a given customer.</p>  | /v1/customers/{customer}/sources/{id}/verify\_\_\_post |
| PostCustomersCustomerSubscriptions - Creates a new subscription on an existing customer... | <p>Creates a new subscription on an existing customer.</p>  | /v1/customers/{customer}/subscriptions\_\_\_post |
| PostCustomersCustomerTaxIds - Creates a new <code>TaxID</code> object for a cust... | <p>Creates a new <code>TaxID</code> object for a customer.</p>  | /v1/customers/{customer}/tax\_ids\_\_\_post |
| PostDisputesDisputeClose - Closing the dispute for a charge indicates that yo... | <p>Closing the dispute for a charge indicates that you do not have any evidence to submit and are essentially dismissing the dispute, acknowledging it as lost.</p>  <p>The status of the dispute will change from <code>needs_response</code> to <code>lost</code>. <em>Closing a dispute is irreversible</em>.</p>  | /v1/disputes/{dispute}/close\_\_\_post |
| PostEphemeralKeys - Creates a short-lived API key for a given resource... | <p>Creates a short-lived API key for a given resource.</p>  | /v1/ephemeral\_keys\_\_\_post |
| PostFileLinks - Creates a new file link object. | <p>Creates a new file link object.</p>  | /v1/file\_links\_\_\_post |
| PostFiles - To upload a file to Stripe, you’ll need to send ... | <p>To upload a file to Stripe, you’ll need to send a request of type <code>multipart/form-data</code>. The request should contain the file you would like to upload, as well as the parameters for creating a file.</p>  <p>All of Stripe’s officially supported Client libraries should have support for sending <code>multipart/form-data</code>.</p>  | /v1/files\_\_\_post |
| PostInvoiceitems - Creates an item to be added to a draft invoice. If... | <p>Creates an item to be added to a draft invoice. If no invoice is specified, the item will be on the next invoice created for the customer specified.</p>  | /v1/invoiceitems\_\_\_post |
| PostInvoices - This endpoint creates a draft invoice for a given ... | <p>This endpoint creates a draft invoice for a given customer. The draft invoice created pulls in all pending invoice items on that customer, including prorations.</p>  | /v1/invoices\_\_\_post |
| PostInvoicesInvoiceFinalize - Stripe automatically finalizes drafts before sendi... | <p>Stripe automatically finalizes drafts before sending and attempting payment on invoices. However, if you’d like to finalize a draft invoice manually, you can do so using this method.</p>  | /v1/invoices/{invoice}/finalize\_\_\_post |
| PostInvoicesInvoiceMarkUncollectible - Marking an invoice as uncollectible is useful for ... | <p>Marking an invoice as uncollectible is useful for keeping track of bad debts that can be written off for accounting purposes.</p>  | /v1/invoices/{invoice}/mark\_uncollectible\_\_\_post |
| PostInvoicesInvoicePay - Stripe automatically creates and then attempts to ... | <p>Stripe automatically creates and then attempts to collect payment on invoices for customers on subscriptions according to your <a href="https://dashboard.stripe.com/account/billing/automatic">subscriptions settings</a>. However, if you’d like to attempt payment on an invoice out of the normal collection schedule or for some other reason, you can do so.</p>  | /v1/invoices/{invoice}/pay\_\_\_post |
| PostInvoicesInvoiceSend - Stripe will automatically send invoices to custome... | <p>Stripe will automatically send invoices to customers according to your <a href="https://dashboard.stripe.com/account/billing/automatic">subscriptions settings</a>. However, if you’d like to manually send an invoice to your customer out of the normal schedule, you can do so. When sending invoices that have already been paid, there will be no reference to the payment in the email.</p>  <p>Requests made in test-mode result in no emails being sent, despite sending an <code>invoice.sent</code> event.</p>  | /v1/invoices/{invoice}/send\_\_\_post |
| PostInvoicesInvoiceVoid - Mark a finalized invoice as void. This cannot be u... | <p>Mark a finalized invoice as void. This cannot be undone. Voiding an invoice is similar to <a href="#delete_invoice">deletion</a>, however it only applies to finalized invoices and maintains a papertrail where the invoice can still be found.</p>  | /v1/invoices/{invoice}/void\_\_\_post |
| PostIssuingAuthorizationsAuthorizationApprove - Approves a pending Issuing <code>Authorization</co... | <p>Approves a pending Issuing <code>Authorization</code> object.</p>  | /v1/issuing/authorizations/{authorization}/approve\_\_\_post |
| PostIssuingAuthorizationsAuthorizationDecline - Declines a pending Issuing <code>Authorization</co... | <p>Declines a pending Issuing <code>Authorization</code> object.</p>  | /v1/issuing/authorizations/{authorization}/decline\_\_\_post |
| PostIssuingCardholders - Creates a new Issuing <code>Cardholder</code> obje... | <p>Creates a new Issuing <code>Cardholder</code> object that can be issued cards.</p>  | /v1/issuing/cardholders\_\_\_post |
| PostIssuingCards - Creates an Issuing <code>Card</code> object. | <p>Creates an Issuing <code>Card</code> object.</p>  | /v1/issuing/cards\_\_\_post |
| PostIssuingCardsCardPin - Updates the PIN for a card, subject to cardholder ... | <p>Updates the PIN for a card, subject to cardholder verification. See <a href="/docs/issuing/pin_management">Retrieve and update cardholder PIN</a></p>  | /v1/issuing/cards/{card}/pin\_\_\_post |
| PostIssuingDisputes - Creates an Issuing <code>Dispute</code> object. | <p>Creates an Issuing <code>Dispute</code> object.</p>  | /v1/issuing/disputes\_\_\_post |
| PostIssuingVerifications - Some actions (eg: updating a PIN) need confirmatio... | <p>Some actions (eg: updating a PIN) need confirmation from the cardholder</p>  | /v1/issuing/verifications\_\_\_post |
| PostOrders - Creates a new order object. | <p>Creates a new order object.</p>  | /v1/orders\_\_\_post |
| PostOrdersIdPay - Pay an order by providing a <code>source</code> to... | <p>Pay an order by providing a <code>source</code> to create a payment.</p>  | /v1/orders/{id}/pay\_\_\_post |
| PostOrdersIdReturns - Return all or part of an order. The order must hav... | <p>Return all or part of an order. The order must have a status of <code>paid</code> or <code>fulfilled</code> before it can be returned. Once all items have been returned, the order will become <code>canceled</code> or <code>returned</code> depending on which status the order started in.</p>  | /v1/orders/{id}/returns\_\_\_post |
| PostPaymentIntents - Creates a PaymentIntent object.  After the Payment... | <p>Creates a PaymentIntent object.</p>  <p>After the PaymentIntent is created, attach a payment method and <a href="/docs/api/payment_intents/confirm">confirm</a> to continue the payment. You can read more about the different payment flows available via the Payment Intents API <a href="/docs/payments/payment-intents">here</a>.</p>  <p>When <code>confirm=true</code> is used during creation, it is equivalent to creating and confirming the PaymentIntent in the same call. You may use any parameters available in the <a href="/docs/api/payment_intents/confirm">confirm API</a> when <code>confirm=true</code> is supplied.</p>  | /v1/payment\_intents\_\_\_post |
| PostPaymentIntentsIntentCancel - A PaymentIntent object can be canceled when it is ... | <p>A PaymentIntent object can be canceled when it is in one of these statuses: <code>requires_payment_method</code>, <code>requires_capture</code>, <code>requires_confirmation</code>, <code>requires_action</code>. </p>  <p>Once canceled, no additional charges will be made by the PaymentIntent and any operations on the PaymentIntent will fail with an error. For PaymentIntents with <code>status='requires_capture'</code>, the remaining <code>amount_capturable</code> will automatically be refunded.</p>  | /v1/payment\_intents/{intent}/cancel\_\_\_post |
| PostPaymentIntentsIntentCapture - Capture the funds of an existing uncaptured Paymen... | <p>Capture the funds of an existing uncaptured PaymentIntent when its status is <code>requires_capture</code>.</p>  <p>Uncaptured PaymentIntents will be canceled exactly seven days after they are created.</p>  <p>Learn more about <a href="/docs/payments/capture-later">separate authorization and capture</a>.</p>  | /v1/payment\_intents/{intent}/capture\_\_\_post |
| PostPaymentIntentsIntentConfirm - Confirm that your customer intends to pay with cur... | <p>Confirm that your customer intends to pay with current or provided payment method. Upon confirmation, the PaymentIntent will attempt to initiate a payment.</p>  <p>If the selected payment method requires additional authentication steps, the PaymentIntent will transition to the <code>requires_action</code> status and suggest additional actions via <code>next_action</code>. If payment fails, the PaymentIntent will transition to the <code>requires_payment_method</code> status. If payment succeeds, the PaymentIntent will transition to the <code>succeeded</code> status (or <code>requires_capture</code>, if <code>capture_method</code> is set to <code>manual</code>).</p>  <p>If the <code>confirmation_method</code> is <code>automatic</code>, payment may be attempted using our <a href="/docs/stripe-js/reference#stripe-handle-card-payment">client SDKs</a> and the PaymentIntent’s <a href="#payment_intent_object-client_secret">client_secret</a>. After <code>next_action</code>s are handled by the client, no additional confirmation is required to complete the payment.</p>  <p>If the <code>confirmation_method</code> is <code>manual</code>, all payment attempts must be initiated using a secret key. If any actions are required for the payment, the PaymentIntent will return to the <code>requires_confirmation</code> state after those actions are completed. Your server needs to then explicitly re-confirm the PaymentIntent to initiate the next payment attempt. Read the <a href="/docs/payments/payment-intents/web-manual">expanded documentation</a> to learn more about manual confirmation.</p>  | /v1/payment\_intents/{intent}/confirm\_\_\_post |
| PostPaymentMethods - Creates a PaymentMethod object. Read the <a href="... | <p>Creates a PaymentMethod object. Read the <a href="/docs/stripe-js/reference#stripe-create-payment-method">Stripe.js reference</a> to learn how to create PaymentMethods via Stripe.js.</p>  | /v1/payment\_methods\_\_\_post |
| PostPaymentMethodsPaymentMethodAttach - Attaches a PaymentMethod object to a Customer.  To... | <p>Attaches a PaymentMethod object to a Customer.</p>  <p>To use this PaymentMethod as the default for invoice or subscription payments, set <a href="/docs/api/customers/update#update_customer-invoice_settings-default_payment_method"><code>invoice_settings.default_payment_method</code></a>, on the Customer to the PaymentMethod’s ID.</p>  | /v1/payment\_methods/{payment\_method}/attach\_\_\_post |
| PostPaymentMethodsPaymentMethodDetach - Detaches a PaymentMethod object from a Customer. | <p>Detaches a PaymentMethod object from a Customer.</p>  | /v1/payment\_methods/{payment\_method}/detach\_\_\_post |
| PostPayouts - To send funds to your own bank account, you create... | <p>To send funds to your own bank account, you create a new payout object. Your <a href="#balance">Stripe balance</a> must be able to cover the payout amount, or you’ll receive an “Insufficient Funds�? error.</p>  <p>If your API key is in test mode, money won’t actually be sent, though everything else will occur as if in live mode.</p>  <p>If you are creating a manual payout on a Stripe account that uses multiple payment source types, you’ll need to specify the source type balance that the payout should draw from. The <a href="#balance_object">balance object</a> details available and pending amounts by source type.</p>  | /v1/payouts\_\_\_post |
| PostPayoutsPayoutCancel - A previously created payout can be canceled if it ... | <p>A previously created payout can be canceled if it has not yet been paid out. Funds will be refunded to your available balance. You may not cancel automatic Stripe payouts.</p>  | /v1/payouts/{payout}/cancel\_\_\_post |
| PostPlans - You can create plans using the API, or in the Stri... | <p>You can create plans using the API, or in the Stripe <a href="https://dashboard.stripe.com/subscriptions/products">Dashboard</a>.</p>  | /v1/plans\_\_\_post |
| PostProducts - Creates a new product object. To create a product ... | <p>Creates a new product object. To create a product for use with orders, see <a href="#create_product">Products</a>.</p>  | /v1/products\_\_\_post |
| PostRadarValueListItems - Creates a new <code>ValueListItem</code> object, w... | <p>Creates a new <code>ValueListItem</code> object, which is added to the specified parent value list.</p>  | /v1/radar/value\_list\_items\_\_\_post |
| PostRadarValueLists - Creates a new <code>ValueList</code> object, which... | <p>Creates a new <code>ValueList</code> object, which can then be referenced in rules.</p>  | /v1/radar/value\_lists\_\_\_post |
| PostRecipients - Creates a new <code>Recipient</code> object and ve... | <p>Creates a new <code>Recipient</code> object and verifies the recipient’s identity. Also verifies the recipient’s bank account information or debit card, if either is provided.</p>  | /v1/recipients\_\_\_post |
| PostRefunds - Create a refund. | <p>Create a refund.</p>  | /v1/refunds\_\_\_post |
| PostReportingReportRuns - Creates a new object and begin running the report.... | <p>Creates a new object and begin running the report. (Requires a <a href="https://stripe.com/docs/keys#test-live-modes">live-mode API key</a>.)</p>  | /v1/reporting/report\_runs\_\_\_post |
| PostReviewsReviewApprove - Approves a <code>Review</code> object, closing it ... | <p>Approves a <code>Review</code> object, closing it and removing it from the list of reviews.</p>  | /v1/reviews/{review}/approve\_\_\_post |
| PostSetupIntents - Creates a SetupIntent object.  After the SetupInte... | <p>Creates a SetupIntent object.</p>  <p>After the SetupIntent is created, attach a payment method and <a href="/docs/api/setup_intents/confirm">confirm</a> to collect any required permissions to charge the payment method later.</p>  | /v1/setup\_intents\_\_\_post |
| PostSetupIntentsIntentCancel - A SetupIntent object can be canceled when it is in... | <p>A SetupIntent object can be canceled when it is in one of these statuses: <code>requires_payment_method</code>, <code>requires_capture</code>, <code>requires_confirmation</code>, <code>requires_action</code>. </p>  <p>Once canceled, setup is abandoned and any operations on the SetupIntent will fail with an error.</p>  | /v1/setup\_intents/{intent}/cancel\_\_\_post |
| PostSetupIntentsIntentConfirm - Confirm that your customer intends to set up the c... | <p>Confirm that your customer intends to set up the current or provided payment method. For example, you would confirm a SetupIntent when a customer hits the “Save�? button on a payment method management page on your website.</p>  <p>If the selected payment method does not require any additional steps from the customer, the SetupIntent will transition to the <code>succeeded</code> status.</p>  <p>Otherwise, it will transition to the <code>requires_action</code> status and suggest additional actions via <code>next_action</code>. If setup fails, the SetupIntent will transition to the <code>requires_payment_method</code> status.</p>  | /v1/setup\_intents/{intent}/confirm\_\_\_post |
| PostSkus - Creates a new SKU associated with a product. | <p>Creates a new SKU associated with a product.</p>  | /v1/skus\_\_\_post |
| PostSources - Creates a new source object. | <p>Creates a new source object.</p>  | /v1/sources\_\_\_post |
| PostSourcesSourceVerify - Verify a given source. | <p>Verify a given source.</p>  | /v1/sources/{source}/verify\_\_\_post |
| PostSubscriptionItems - Adds a new item to an existing subscription. No ex... | <p>Adds a new item to an existing subscription. No existing items will be changed or replaced.</p>  | /v1/subscription\_items\_\_\_post |
| PostSubscriptionItemsSubscriptionItemUsageRecords - Creates a usage record for a specified subscriptio... | <p>Creates a usage record for a specified subscription item and date, and fills it with a quantity.</p>  <p>Usage records provide <code>quantity</code> information that Stripe uses to track how much a customer is using your service. With usage information and the pricing model set up by the <a href="https://stripe.com/docs/billing/subscriptions/metered-billing">metered billing</a> plan, Stripe helps you send accurate invoices to your customers.</p>  <p>The default calculation for usage is to add up all the <code>quantity</code> values of the usage records within a billing period. You can change this default behavior with the billing plan’s <code>aggregate_usage</code> <a href="https://stripe.com/docs/api/plans/create#create_plan-aggregate_usage">parameter</a>. When there is more than one usage record with the same timestamp, Stripe adds the <code>quantity</code> values together. In most cases, this is the desired resolution, however, you can change this behavior with the <code>action</code> parameter.</p>  <p>The default pricing model for metered billing is <a href="https://stripe.com/docs/api/plans/object#plan_object-billing_scheme">per-unit pricing</a>. For finer granularity, you can configure metered billing to have a <a href="https://stripe.com/docs/billing/subscriptions/tiers">tiered pricing</a> model.</p>  | /v1/subscription\_items/{subscription\_item}/usage\_records\_\_\_post |
| PostSubscriptions - Creates a new subscription on an existing customer... | <p>Creates a new subscription on an existing customer.</p>  | /v1/subscriptions\_\_\_post |
| PostSubscriptionSchedules - Creates a new subscription schedule object. | <p>Creates a new subscription schedule object.</p>  | /v1/subscription\_schedules\_\_\_post |
| PostSubscriptionSchedulesScheduleCancel - Cancels a subscription schedule and its associated... | <p>Cancels a subscription schedule and its associated subscription immediately (if the subscription schedule has an active subscription). A subscription schedule can only be canceled if its status is <code>not_started</code> or <code>active</code>.</p>  | /v1/subscription\_schedules/{schedule}/cancel\_\_\_post |
| PostSubscriptionSchedulesScheduleRelease - Releases the subscription schedule immediately, wh... | <p>Releases the subscription schedule immediately, which will stop scheduling of its phases, but leave any existing subscription in place. A schedule can only be released if its status is <code>not_started</code> or <code>active</code>. If the subscription schedule is currently associated with a subscription, releasing it will remove its <code>subscription</code> property and set the subscription’s ID to the <code>released_subscription</code> property.</p>  | /v1/subscription\_schedules/{schedule}/release\_\_\_post |
| PostTaxRates - Creates a new tax rate. | <p>Creates a new tax rate.</p>  | /v1/tax\_rates\_\_\_post |
| PostTerminalConnectionTokens - To connect to a reader the Stripe Terminal SDK nee... | <p>To connect to a reader the Stripe Terminal SDK needs to retrieve a short-lived connection token from Stripe, proxied through your server. On your backend, add an endpoint that creates and returns a connection token.</p>  | /v1/terminal/connection\_tokens\_\_\_post |
| PostTerminalLocations - Creates a new <code>Location</code> object. | <p>Creates a new <code>Location</code> object.</p>  | /v1/terminal/locations\_\_\_post |
| PostTerminalReaders - Creates a new <code>Reader</code> object. | <p>Creates a new <code>Reader</code> object.</p>  | /v1/terminal/readers\_\_\_post |
| PostTokens - Creates a single-use token that represents a bank ... | <p>Creates a single-use token that represents a bank account’s details. This token can be used with any API method in place of a bank account dictionary. This token can be used only once, by attaching it to a <a href="#accounts">Custom account</a>.</p>  | /v1/tokens\_\_\_post |
| PostTopups - Top up the balance of an account | <p>Top up the balance of an account</p>  | /v1/topups\_\_\_post |
| PostTopupsTopupCancel - Cancels a top-up. Only pending top-ups can be canc... | <p>Cancels a top-up. Only pending top-ups can be canceled.</p>  | /v1/topups/{topup}/cancel\_\_\_post |
| PostTransfers - To send funds from your Stripe account to a connec... | <p>To send funds from your Stripe account to a connected account, you create a new transfer object. Your <a href="#balance">Stripe balance</a> must be able to cover the transfer amount, or you’ll receive an “Insufficient Funds�? error.</p>  | /v1/transfers\_\_\_post |
| PostTransfersIdReversals - When you create a new reversal, you must specify a... | <p>When you create a new reversal, you must specify a transfer to create it on.</p>  <p>When reversing transfers, you can optionally reverse part of the transfer. You can do so as many times as you wish until the entire transfer has been reversed.</p>  <p>Once entirely reversed, a transfer can’t be reversed again. This method will return an error when called on an already-reversed transfer, or when trying to reverse more money than is left on a transfer.</p>  | /v1/transfers/{id}/reversals\_\_\_post |
| PostWebhookEndpoints - A webhook endpoint must have a <code>url</code> an... | <p>A webhook endpoint must have a <code>url</code> and a list of <code>enabled_events</code>. You may optionally specify the Boolean <code>connect</code> parameter. If set to true, then a Connect webhook endpoint that notifies the specified <code>url</code> about events from all connected accounts is created; otherwise an account webhook endpoint that notifies the specified <code>url</code> only about events from your account is created. You can also create webhook endpoints in the <a href="https://dashboard.stripe.com/account/webhooks">webhooks settings</a> section of the Dashboard.</p>  | /v1/webhook\_endpoints\_\_\_post |


### DELETE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| DeleteAccountBankAccountsId - Delete a specified external account for a given ac... | <p>Delete a specified external account for a given account.</p>  | /v1/account/bank\_accounts/{id}\_\_\_delete |
| DeleteAccountExternalAccountsId - Delete a specified external account for a given ac... | <p>Delete a specified external account for a given account.</p>  | /v1/account/external\_accounts/{id}\_\_\_delete |
| DeleteAccountPeoplePerson - Deletes an existing person’s relationship to the... | <p>Deletes an existing person’s relationship to the account’s legal entity. Any person with a relationship for an account can be deleted through the API, except if the person is the <code>account_opener</code>. If your integration is using the <code>executive</code> parameter, you cannot delete the only verified <code>executive</code> on file.</p>  | /v1/account/people/{person}\_\_\_delete |
| DeleteAccountPersonsPerson - Deletes an existing person’s relationship to the... | <p>Deletes an existing person’s relationship to the account’s legal entity. Any person with a relationship for an account can be deleted through the API, except if the person is the <code>account_opener</code>. If your integration is using the <code>executive</code> parameter, you cannot delete the only verified <code>executive</code> on file.</p>  | /v1/account/persons/{person}\_\_\_delete |
| DeleteAccountsAccount - With <a href="/docs/connect">Connect</a>, you can ... | <p>With <a href="/docs/connect">Connect</a>, you can delete Custom or Express accounts you manage.</p>  <p>Accounts created using test-mode keys can be deleted at any time. Accounts created using live-mode keys can only be deleted once all balances are zero.</p>  <p>If you want to delete your own account, use the <a href="https://dashboard.stripe.com/account/data">data tab in your account settings</a> instead.</p>  | /v1/accounts/{account}\_\_\_delete |
| DeleteApplePayDomainsDomain - Delete an apple pay domain. | <p>Delete an apple pay domain.</p>  | /v1/apple\_pay/domains/{domain}\_\_\_delete |
| DeleteCouponsCoupon - You can delete coupons via the <a href="https://da... | <p>You can delete coupons via the <a href="https://dashboard.stripe.com/coupons">coupon management</a> page of the Stripe dashboard. However, deleting a coupon does not affect any customers who have already applied the coupon; it means that new customers can’t redeem the coupon. You can also delete coupons via the API.</p>  | /v1/coupons/{coupon}\_\_\_delete |
| DeleteCustomersCustomer - Permanently deletes a customer. It cannot be undon... | <p>Permanently deletes a customer. It cannot be undone. Also immediately cancels any active subscriptions on the customer.</p>  | /v1/customers/{customer}\_\_\_delete |
| DeleteCustomersCustomerDiscount - Removes the currently applied discount on a custom... | <p>Removes the currently applied discount on a customer.</p>  | /v1/customers/{customer}/discount\_\_\_delete |
| DeleteEphemeralKeysKey - Invalidates a short-lived API key for a given reso... | <p>Invalidates a short-lived API key for a given resource.</p>  | /v1/ephemeral\_keys/{key}\_\_\_delete |
| DeleteInvoiceitemsInvoiceitem - Deletes an invoice item, removing it from an invoi... | <p>Deletes an invoice item, removing it from an invoice. Deleting invoice items is only possible when they’re not attached to invoices, or if it’s attached to a draft invoice.</p>  | /v1/invoiceitems/{invoiceitem}\_\_\_delete |
| DeleteInvoicesInvoice - Permanently deletes a draft invoice. This cannot b... | <p>Permanently deletes a draft invoice. This cannot be undone. Attempts to delete invoices that are no longer in a draft state will fail; once an invoice has been finalized, it must be <a href="#void_invoice">voided</a>.</p>  | /v1/invoices/{invoice}\_\_\_delete |
| DeletePlansPlan - Deleting plans means new subscribers can’t be ad... | <p>Deleting plans means new subscribers can’t be added. Existing subscribers aren’t affected.</p>  | /v1/plans/{plan}\_\_\_delete |
| DeleteProductsId - Delete a product. Deleting a product with type=<co... | <p>Delete a product. Deleting a product with type=<code>good</code> is only possible if it has no SKUs associated with it. Deleting a product with type=<code>service</code> is only possible if it has no plans associated with it.</p>  | /v1/products/{id}\_\_\_delete |
| DeleteRadarValueListItemsItem - Deletes a <code>ValueListItem</code> object, remov... | <p>Deletes a <code>ValueListItem</code> object, removing it from its parent value list.</p>  | /v1/radar/value\_list\_items/{item}\_\_\_delete |
| DeleteRadarValueListsValueList - Deletes a <code>ValueList</code> object, also dele... | <p>Deletes a <code>ValueList</code> object, also deleting any items contained within the value list. To be deleted, a value list must not be referenced in any rules.</p>  | /v1/radar/value\_lists/{value\_list}\_\_\_delete |
| DeleteRecipientsId - Permanently deletes a recipient. It cannot be undo... | <p>Permanently deletes a recipient. It cannot be undone.</p>  | /v1/recipients/{id}\_\_\_delete |
| DeleteSkusId - Delete a SKU. Deleting a SKU is only possible unti... | <p>Delete a SKU. Deleting a SKU is only possible until it has been used in an order.</p>  | /v1/skus/{id}\_\_\_delete |
| DeleteSubscriptionItemsItem - Deletes an item from the subscription. Removing a ... | <p>Deletes an item from the subscription. Removing a subscription item from a subscription will not cancel the subscription.</p>  | /v1/subscription\_items/{item}\_\_\_delete |
| DeleteSubscriptionsSubscriptionExposedId - Cancels a customer’s subscription immediately. T... | <p>Cancels a customer’s subscription immediately. The customer will not be charged again for the subscription.</p>  <p>Note, however, that any pending invoice items that you’ve created will still be charged for at the end of the period, unless manually <a href="#delete_invoiceitem">deleted</a>. If you’ve set the subscription to cancel at the end of the period, any pending prorations will also be left in place and collected at the end of the period. But if the subscription is set to cancel immediately, pending prorations will be removed.</p>  <p>By default, upon subscription cancellation, Stripe will stop automatic collection of all finalized invoices for the customer. This is intended to prevent unexpected payment attempts after the customer has canceled a subscription. However, you can resume automatic collection of the invoices manually after subscription cancellation to have us proceed. Or, you could check for unpaid invoices before allowing the customer to cancel the subscription at all.</p>  | /v1/subscriptions/{subscription\_exposed\_id}\_\_\_delete |
| DeleteSubscriptionsSubscriptionExposedIdDiscount - Removes the currently applied discount on a subscr... | <p>Removes the currently applied discount on a subscription.</p>  | /v1/subscriptions/{subscription\_exposed\_id}/discount\_\_\_delete |
| DeleteTerminalLocationsLocation - Deletes a <code>Location</code> object. | <p>Deletes a <code>Location</code> object.</p>  | /v1/terminal/locations/{location}\_\_\_delete |
| DeleteTerminalReadersReader - Deletes a <code>Reader</code> object. | <p>Deletes a <code>Reader</code> object.</p>  | /v1/terminal/readers/{reader}\_\_\_delete |
| DeleteWebhookEndpointsWebhookEndpoint - You can also delete webhook endpoints via the <a h... | <p>You can also delete webhook endpoints via the <a href="https://dashboard.stripe.com/account/webhooks">webhook endpoint management</a> page of the Stripe dashboard.</p>  | /v1/webhook\_endpoints/{webhook\_endpoint}\_\_\_delete |


### DELETE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| DeleteAccount - With <a href="/docs/connect">Connect</a>, you can ... | <p>With <a href="/docs/connect">Connect</a>, you can delete Custom or Express accounts you manage.</p>  <p>Accounts created using test-mode keys can be deleted at any time. Accounts created using live-mode keys can only be deleted once all balances are zero.</p>  <p>If you want to delete your own account, use the <a href="https://dashboard.stripe.com/account/data">data tab in your account settings</a> instead.</p>  | /v1/account\_\_\_delete |
| DeleteAccountBankAccountsId - Delete a specified external account for a given ac... | <p>Delete a specified external account for a given account.</p>  | /v1/account/bank\_accounts/{id}\_\_\_delete |
| DeleteAccountExternalAccountsId - Delete a specified external account for a given ac... | <p>Delete a specified external account for a given account.</p>  | /v1/account/external\_accounts/{id}\_\_\_delete |
| DeleteAccountPeoplePerson - Deletes an existing person’s relationship to the... | <p>Deletes an existing person’s relationship to the account’s legal entity. Any person with a relationship for an account can be deleted through the API, except if the person is the <code>account_opener</code>. If your integration is using the <code>executive</code> parameter, you cannot delete the only verified <code>executive</code> on file.</p>  | /v1/account/people/{person}\_\_\_delete |
| DeleteAccountPersonsPerson - Deletes an existing person’s relationship to the... | <p>Deletes an existing person’s relationship to the account’s legal entity. Any person with a relationship for an account can be deleted through the API, except if the person is the <code>account_opener</code>. If your integration is using the <code>executive</code> parameter, you cannot delete the only verified <code>executive</code> on file.</p>  | /v1/account/persons/{person}\_\_\_delete |
| DeleteAccountsAccount - With <a href="/docs/connect">Connect</a>, you can ... | <p>With <a href="/docs/connect">Connect</a>, you can delete Custom or Express accounts you manage.</p>  <p>Accounts created using test-mode keys can be deleted at any time. Accounts created using live-mode keys can only be deleted once all balances are zero.</p>  <p>If you want to delete your own account, use the <a href="https://dashboard.stripe.com/account/data">data tab in your account settings</a> instead.</p>  | /v1/accounts/{account}\_\_\_delete |
| DeleteAccountsAccountBankAccountsId - Delete a specified external account for a given ac... | <p>Delete a specified external account for a given account.</p>  | /v1/accounts/{account}/bank\_accounts/{id}\_\_\_delete |
| DeleteAccountsAccountExternalAccountsId - Delete a specified external account for a given ac... | <p>Delete a specified external account for a given account.</p>  | /v1/accounts/{account}/external\_accounts/{id}\_\_\_delete |
| DeleteAccountsAccountPeoplePerson - Deletes an existing person’s relationship to the... | <p>Deletes an existing person’s relationship to the account’s legal entity. Any person with a relationship for an account can be deleted through the API, except if the person is the <code>account_opener</code>. If your integration is using the <code>executive</code> parameter, you cannot delete the only verified <code>executive</code> on file.</p>  | /v1/accounts/{account}/people/{person}\_\_\_delete |
| DeleteAccountsAccountPersonsPerson - Deletes an existing person’s relationship to the... | <p>Deletes an existing person’s relationship to the account’s legal entity. Any person with a relationship for an account can be deleted through the API, except if the person is the <code>account_opener</code>. If your integration is using the <code>executive</code> parameter, you cannot delete the only verified <code>executive</code> on file.</p>  | /v1/accounts/{account}/persons/{person}\_\_\_delete |
| DeleteApplePayDomainsDomain - Delete an apple pay domain. | <p>Delete an apple pay domain.</p>  | /v1/apple\_pay/domains/{domain}\_\_\_delete |
| DeleteCouponsCoupon - You can delete coupons via the <a href="https://da... | <p>You can delete coupons via the <a href="https://dashboard.stripe.com/coupons">coupon management</a> page of the Stripe dashboard. However, deleting a coupon does not affect any customers who have already applied the coupon; it means that new customers can’t redeem the coupon. You can also delete coupons via the API.</p>  | /v1/coupons/{coupon}\_\_\_delete |
| DeleteCustomersCustomer - Permanently deletes a customer. It cannot be undon... | <p>Permanently deletes a customer. It cannot be undone. Also immediately cancels any active subscriptions on the customer.</p>  | /v1/customers/{customer}\_\_\_delete |
| DeleteCustomersCustomerBankAccountsId - Delete a specified source for a given customer. | <p>Delete a specified source for a given customer.</p>  | /v1/customers/{customer}/bank\_accounts/{id}\_\_\_delete |
| DeleteCustomersCustomerCardsId - Delete a specified source for a given customer. | <p>Delete a specified source for a given customer.</p>  | /v1/customers/{customer}/cards/{id}\_\_\_delete |
| DeleteCustomersCustomerDiscount - Removes the currently applied discount on a custom... | <p>Removes the currently applied discount on a customer.</p>  | /v1/customers/{customer}/discount\_\_\_delete |
| DeleteCustomersCustomerSourcesId - Delete a specified source for a given customer. | <p>Delete a specified source for a given customer.</p>  | /v1/customers/{customer}/sources/{id}\_\_\_delete |
| DeleteCustomersCustomerSubscriptionsSubscriptionExposedId - Cancels a customer’s subscription. If you set th... | <p>Cancels a customer’s subscription. If you set the <code>at_period_end</code> parameter to <code>true</code>, the subscription will remain active until the end of the period, at which point it will be canceled and not renewed. Otherwise, with the default <code>false</code> value, the subscription is terminated immediately. In either case, the customer will not be charged again for the subscription.</p>  <p>Note, however, that any pending invoice items that you’ve created will still be charged for at the end of the period, unless manually <a href="#delete_invoiceitem">deleted</a>. If you’ve set the subscription to cancel at the end of the period, any pending prorations will also be left in place and collected at the end of the period. But if the subscription is set to cancel immediately, pending prorations will be removed.</p>  <p>By default, upon subscription cancellation, Stripe will stop automatic collection of all finalized invoices for the customer. This is intended to prevent unexpected payment attempts after the customer has canceled a subscription. However, you can resume automatic collection of the invoices manually after subscription cancellation to have us proceed. Or, you could check for unpaid invoices before allowing the customer to cancel the subscription at all.</p>  | /v1/customers/{customer}/subscriptions/{subscription\_exposed\_id}\_\_\_delete |
| DeleteCustomersCustomerSubscriptionsSubscriptionExposedIdDiscount - Removes the currently applied discount on a custom... | <p>Removes the currently applied discount on a customer.</p>  | /v1/customers/{customer}/subscriptions/{subscription\_exposed\_id}/discount\_\_\_delete |
| DeleteCustomersCustomerTaxIdsId - Deletes an existing <code>TaxID</code> object. | <p>Deletes an existing <code>TaxID</code> object.</p>  | /v1/customers/{customer}/tax\_ids/{id}\_\_\_delete |
| DeleteEphemeralKeysKey - Invalidates a short-lived API key for a given reso... | <p>Invalidates a short-lived API key for a given resource.</p>  | /v1/ephemeral\_keys/{key}\_\_\_delete |
| DeleteInvoiceitemsInvoiceitem - Deletes an invoice item, removing it from an invoi... | <p>Deletes an invoice item, removing it from an invoice. Deleting invoice items is only possible when they’re not attached to invoices, or if it’s attached to a draft invoice.</p>  | /v1/invoiceitems/{invoiceitem}\_\_\_delete |
| DeleteInvoicesInvoice - Permanently deletes a draft invoice. This cannot b... | <p>Permanently deletes a draft invoice. This cannot be undone. Attempts to delete invoices that are no longer in a draft state will fail; once an invoice has been finalized, it must be <a href="#void_invoice">voided</a>.</p>  | /v1/invoices/{invoice}\_\_\_delete |
| DeletePlansPlan - Deleting plans means new subscribers can’t be ad... | <p>Deleting plans means new subscribers can’t be added. Existing subscribers aren’t affected.</p>  | /v1/plans/{plan}\_\_\_delete |
| DeleteProductsId - Delete a product. Deleting a product with type=<co... | <p>Delete a product. Deleting a product with type=<code>good</code> is only possible if it has no SKUs associated with it. Deleting a product with type=<code>service</code> is only possible if it has no plans associated with it.</p>  | /v1/products/{id}\_\_\_delete |
| DeleteRadarValueListItemsItem - Deletes a <code>ValueListItem</code> object, remov... | <p>Deletes a <code>ValueListItem</code> object, removing it from its parent value list.</p>  | /v1/radar/value\_list\_items/{item}\_\_\_delete |
| DeleteRadarValueListsValueList - Deletes a <code>ValueList</code> object, also dele... | <p>Deletes a <code>ValueList</code> object, also deleting any items contained within the value list. To be deleted, a value list must not be referenced in any rules.</p>  | /v1/radar/value\_lists/{value\_list}\_\_\_delete |
| DeleteRecipientsId - Permanently deletes a recipient. It cannot be undo... | <p>Permanently deletes a recipient. It cannot be undone.</p>  | /v1/recipients/{id}\_\_\_delete |
| DeleteSkusId - Delete a SKU. Deleting a SKU is only possible unti... | <p>Delete a SKU. Deleting a SKU is only possible until it has been used in an order.</p>  | /v1/skus/{id}\_\_\_delete |
| DeleteSubscriptionItemsItem - Deletes an item from the subscription. Removing a ... | <p>Deletes an item from the subscription. Removing a subscription item from a subscription will not cancel the subscription.</p>  | /v1/subscription\_items/{item}\_\_\_delete |
| DeleteSubscriptionsSubscriptionExposedId - Cancels a customer’s subscription immediately. T... | <p>Cancels a customer’s subscription immediately. The customer will not be charged again for the subscription.</p>  <p>Note, however, that any pending invoice items that you’ve created will still be charged for at the end of the period, unless manually <a href="#delete_invoiceitem">deleted</a>. If you’ve set the subscription to cancel at the end of the period, any pending prorations will also be left in place and collected at the end of the period. But if the subscription is set to cancel immediately, pending prorations will be removed.</p>  <p>By default, upon subscription cancellation, Stripe will stop automatic collection of all finalized invoices for the customer. This is intended to prevent unexpected payment attempts after the customer has canceled a subscription. However, you can resume automatic collection of the invoices manually after subscription cancellation to have us proceed. Or, you could check for unpaid invoices before allowing the customer to cancel the subscription at all.</p>  | /v1/subscriptions/{subscription\_exposed\_id}\_\_\_delete |
| DeleteSubscriptionsSubscriptionExposedIdDiscount - Removes the currently applied discount on a subscr... | <p>Removes the currently applied discount on a subscription.</p>  | /v1/subscriptions/{subscription\_exposed\_id}/discount\_\_\_delete |
| DeleteTerminalLocationsLocation - Deletes a <code>Location</code> object. | <p>Deletes a <code>Location</code> object.</p>  | /v1/terminal/locations/{location}\_\_\_delete |
| DeleteTerminalReadersReader - Deletes a <code>Reader</code> object. | <p>Deletes a <code>Reader</code> object.</p>  | /v1/terminal/readers/{reader}\_\_\_delete |
| DeleteWebhookEndpointsWebhookEndpoint - You can also delete webhook endpoints via the <a h... | <p>You can also delete webhook endpoints via the <a href="https://dashboard.stripe.com/account/webhooks">webhook endpoint management</a> page of the Stripe dashboard.</p>  | /v1/webhook\_endpoints/{webhook\_endpoint}\_\_\_delete |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Get3dSecureThreeDSecure - Retrieves a 3D Secure object. | <p>Retrieves a 3D Secure object.</p>  | /v1/3d\_secure/{three\_d\_secure}\_\_\_get |
| GetAccount - Retrieves the details of an account. | <p>Retrieves the details of an account.</p>  | /v1/account\_\_\_get |
| GetAccountBankAccountsId - Retrieve a specified external account for a given ... | <p>Retrieve a specified external account for a given account.</p>  | /v1/account/bank\_accounts/{id}\_\_\_get |
| GetAccountCapabilities - Returns a list of capabilities associated with the... | <p>Returns a list of capabilities associated with the account. The capabilities are returned sorted by creation date, with the most recent capability appearing first.</p>  | /v1/account/capabilities\_\_\_get |
| GetAccountCapabilitiesCapability - Retrieves information about the specified Account ... | <p>Retrieves information about the specified Account Capability.</p>  | /v1/account/capabilities/{capability}\_\_\_get |
| GetAccountExternalAccountsId - Retrieve a specified external account for a given ... | <p>Retrieve a specified external account for a given account.</p>  | /v1/account/external\_accounts/{id}\_\_\_get |
| GetAccountPeoplePerson - Retrieves an existing person. | <p>Retrieves an existing person.</p>  | /v1/account/people/{person}\_\_\_get |
| GetAccountPersonsPerson - Retrieves an existing person. | <p>Retrieves an existing person.</p>  | /v1/account/persons/{person}\_\_\_get |
| GetAccountsAccount - Retrieves the details of an account. | <p>Retrieves the details of an account.</p>  | /v1/accounts/{account}\_\_\_get |
| GetAccountsAccountBankAccountsId - Retrieve a specified external account for a given ... | <p>Retrieve a specified external account for a given account.</p>  | /v1/accounts/{account}/bank\_accounts/{id}\_\_\_get |
| GetAccountsAccountCapabilities - Returns a list of capabilities associated with the... | <p>Returns a list of capabilities associated with the account. The capabilities are returned sorted by creation date, with the most recent capability appearing first.</p>  | /v1/accounts/{account}/capabilities\_\_\_get |
| GetAccountsAccountCapabilitiesCapability - Retrieves information about the specified Account ... | <p>Retrieves information about the specified Account Capability.</p>  | /v1/accounts/{account}/capabilities/{capability}\_\_\_get |
| GetAccountsAccountExternalAccountsId - Retrieve a specified external account for a given ... | <p>Retrieve a specified external account for a given account.</p>  | /v1/accounts/{account}/external\_accounts/{id}\_\_\_get |
| GetAccountsAccountPeoplePerson - Retrieves an existing person. | <p>Retrieves an existing person.</p>  | /v1/accounts/{account}/people/{person}\_\_\_get |
| GetAccountsAccountPersonsPerson - Retrieves an existing person. | <p>Retrieves an existing person.</p>  | /v1/accounts/{account}/persons/{person}\_\_\_get |
| GetApplePayDomainsDomain - Retrieve an apple pay domain. | <p>Retrieve an apple pay domain.</p>  | /v1/apple\_pay/domains/{domain}\_\_\_get |
| GetApplicationFeesFeeRefundsId - By default, you can see the 10 most recent refunds... | <p>By default, you can see the 10 most recent refunds stored directly on the application fee object, but you can also retrieve details about a specific refund stored on the application fee.</p>  | /v1/application\_fees/{fee}/refunds/{id}\_\_\_get |
| GetApplicationFeesId - Retrieves the details of an application fee that y... | <p>Retrieves the details of an application fee that your account has collected. The same information is returned when refunding the application fee.</p>  | /v1/application\_fees/{id}\_\_\_get |
| GetBalance - Retrieves the current account balance, based on th... | <p>Retrieves the current account balance, based on the authentication that was used to make the request.  For a sample request, see <a href="/docs/connect/account-balances#accounting-for-negative-balances">Accounting for negative balances</a>.</p>  | /v1/balance\_\_\_get |
| GetBalanceHistoryId - Retrieves the balance transaction with the given I... | <p>Retrieves the balance transaction with the given ID.</p>  <p>Note that this endpoint previously used the path <code>/v1/balance/history/:id</code>.</p>  | /v1/balance/history/{id}\_\_\_get |
| GetBalanceTransactionsId - Retrieves the balance transaction with the given I... | <p>Retrieves the balance transaction with the given ID.</p>  <p>Note that this endpoint previously used the path <code>/v1/balance/history/:id</code>.</p>  | /v1/balance\_transactions/{id}\_\_\_get |
| GetBitcoinReceiversId - Retrieves the Bitcoin receiver with the given ID. | <p>Retrieves the Bitcoin receiver with the given ID.</p>  | /v1/bitcoin/receivers/{id}\_\_\_get |
| GetChargesCharge - Retrieves the details of a charge that has previou... | <p>Retrieves the details of a charge that has previously been created. Supply the unique charge ID that was returned from your previous request, and Stripe will return the corresponding charge information. The same information is returned when creating or refunding the charge.</p>  | /v1/charges/{charge}\_\_\_get |
| GetChargesChargeDispute - Retrieve a dispute for a specified charge. | <p>Retrieve a dispute for a specified charge.</p>  | /v1/charges/{charge}/dispute\_\_\_get |
| GetChargesChargeRefundsRefund - Retrieves the details of an existing refund. | <p>Retrieves the details of an existing refund.</p>  | /v1/charges/{charge}/refunds/{refund}\_\_\_get |
| GetCheckoutSessionsSession - Retrieves a Session object. | <p>Retrieves a Session object.</p>  | /v1/checkout/sessions/{session}\_\_\_get |
| GetCountrySpecsCountry - Returns a Country Spec for a given Country code. | <p>Returns a Country Spec for a given Country code.</p>  | /v1/country\_specs/{country}\_\_\_get |
| GetCouponsCoupon - Retrieves the coupon with the given ID. | <p>Retrieves the coupon with the given ID.</p>  | /v1/coupons/{coupon}\_\_\_get |
| GetCreditNotesId - Retrieves the credit note object with the given id... | <p>Retrieves the credit note object with the given identifier.</p>  | /v1/credit\_notes/{id}\_\_\_get |
| GetCreditNotesPreview - Get a preview of a credit note without creating it... | <p>Get a preview of a credit note without creating it.</p>  | /v1/credit\_notes/preview\_\_\_get |
| GetCustomersCustomer - Retrieves the details of an existing customer. You... | <p>Retrieves the details of an existing customer. You need only supply the unique customer identifier that was returned upon customer creation.</p>  | /v1/customers/{customer}\_\_\_get |
| GetCustomersCustomerBalanceTransactionsTransaction - Retrieves a specific transaction that updated the ... | <p>Retrieves a specific transaction that updated the customer’s <a href="/docs/api/customers/object#customer_object-balance"><code>balance</code></a>.</p>  | /v1/customers/{customer}/balance\_transactions/{transaction}\_\_\_get |
| GetCustomersCustomerBankAccountsId - By default, you can see the 10 most recent sources... | <p>By default, you can see the 10 most recent sources stored on a Customer directly on the object, but you can also retrieve details about a specific bank account stored on the Stripe account.</p>  | /v1/customers/{customer}/bank\_accounts/{id}\_\_\_get |
| GetCustomersCustomerCardsId - You can always see the 10 most recent cards direct... | <p>You can always see the 10 most recent cards directly on a customer; this method lets you retrieve details about a specific card stored on the customer.</p>  | /v1/customers/{customer}/cards/{id}\_\_\_get |
| GetCustomersCustomerDiscount -  |  **helpText NOT SET IN DESCRIPTOR FILE** | /v1/customers/{customer}/discount\_\_\_get |
| GetCustomersCustomerSourcesId - Retrieve a specified source for a given customer. | <p>Retrieve a specified source for a given customer.</p>  | /v1/customers/{customer}/sources/{id}\_\_\_get |
| GetCustomersCustomerSubscriptionsSubscriptionExposedId - Retrieves the subscription with the given ID. | <p>Retrieves the subscription with the given ID.</p>  | /v1/customers/{customer}/subscriptions/{subscription\_exposed\_id}\_\_\_get |
| GetCustomersCustomerSubscriptionsSubscriptionExposedIdDiscount -  |  **helpText NOT SET IN DESCRIPTOR FILE** | /v1/customers/{customer}/subscriptions/{subscription\_exposed\_id}/discount\_\_\_get |
| GetCustomersCustomerTaxIdsId - Retrieves the <code>TaxID</code> object with the g... | <p>Retrieves the <code>TaxID</code> object with the given identifier.</p>  | /v1/customers/{customer}/tax\_ids/{id}\_\_\_get |
| GetDisputesDispute - Retrieves the dispute with the given ID. | <p>Retrieves the dispute with the given ID.</p>  | /v1/disputes/{dispute}\_\_\_get |
| GetEventsId - Retrieves the details of an event. Supply the uniq... | <p>Retrieves the details of an event. Supply the unique identifier of the event, which you might have received in a webhook.</p>  | /v1/events/{id}\_\_\_get |
| GetExchangeRatesCurrency - Retrieves the exchange rates from the given curren... | <p>Retrieves the exchange rates from the given currency to every supported currency.</p>  | /v1/exchange\_rates/{currency}\_\_\_get |
| GetFileLinksLink - Retrieves the file link with the given ID. | <p>Retrieves the file link with the given ID.</p>  | /v1/file\_links/{link}\_\_\_get |
| GetFilesFile - Retrieves the details of an existing file object. ... | <p>Retrieves the details of an existing file object. Supply the unique file ID from a file, and Stripe will return the corresponding file object.</p>  | /v1/files/{file}\_\_\_get |
| GetInvoiceitemsInvoiceitem - Retrieves the invoice item with the given ID. | <p>Retrieves the invoice item with the given ID.</p>  | /v1/invoiceitems/{invoiceitem}\_\_\_get |
| GetInvoicesInvoice - Retrieves the invoice with the given ID. | <p>Retrieves the invoice with the given ID.</p>  | /v1/invoices/{invoice}\_\_\_get |
| GetInvoicesUpcoming - At any time, you can preview the upcoming invoice ... | <p>At any time, you can preview the upcoming invoice for a customer. This will show you all the charges that are pending, including subscription renewal charges, invoice item charges, etc. It will also show you any discount that is applicable to the customer.</p>  <p>Note that when you are viewing an upcoming invoice, you are simply viewing a preview – the invoice has not yet been created. As such, the upcoming invoice will not show up in invoice listing calls, and you cannot use the API to pay or edit the invoice. If you want to change the amount that your customer will be billed, you can add, remove, or update pending invoice items, or update the customer’s discount.</p>  <p>You can preview the effects of updating a subscription, including a preview of what proration will take place. To ensure that the actual proration is calculated exactly the same as the previewed proration, you should pass a <code>proration_date</code> parameter when doing the actual subscription update. The value passed in should be the same as the <code>subscription_proration_date</code> returned on the upcoming invoice resource. The recommended way to get only the prorations being previewed is to consider only proration line items where <code>period[start]</code> is equal to the <code>subscription_proration_date</code> on the upcoming invoice resource.</p>  | /v1/invoices/upcoming\_\_\_get |
| GetIssuerFraudRecordsIssuerFraudRecord - Retrieves the details of an issuer fraud record th... | <p>Retrieves the details of an issuer fraud record that has previously been created. </p>  <p>Please refer to the <a href="#issuer_fraud_record_object">issuer fraud record</a> object reference for more details.</p>  | /v1/issuer\_fraud\_records/{issuer\_fraud\_record}\_\_\_get |
| GetIssuingAuthorizationsAuthorization - Retrieves an Issuing <code>Authorization</code> ob... | <p>Retrieves an Issuing <code>Authorization</code> object.</p>  | /v1/issuing/authorizations/{authorization}\_\_\_get |
| GetIssuingCardholdersCardholder - Retrieves an Issuing <code>Cardholder</code> objec... | <p>Retrieves an Issuing <code>Cardholder</code> object.</p>  | /v1/issuing/cardholders/{cardholder}\_\_\_get |
| GetIssuingCardsCard - Retrieves an Issuing <code>Card</code> object. | <p>Retrieves an Issuing <code>Card</code> object.</p>  | /v1/issuing/cards/{card}\_\_\_get |
| GetIssuingCardsCardDetails - For virtual cards only. Retrieves an Issuing <code... | <p>For virtual cards only. Retrieves an Issuing <code>card_details</code> object that contains <a href="/docs/issuing/cards/management#virtual-card-info">the sensitive details</a> of a virtual card.</p>  | /v1/issuing/cards/{card}/details\_\_\_get |
| GetIssuingCardsCardPin - Retrieves the PIN for a card object, subject to ca... | <p>Retrieves the PIN for a card object, subject to cardholder verification, see <a href="/docs/issuing/pin_management">Retrieve and update cardholder PIN</a></p>  | /v1/issuing/cards/{card}/pin\_\_\_get |
| GetIssuingDisputesDispute - Retrieves an Issuing <code>Dispute</code> object. | <p>Retrieves an Issuing <code>Dispute</code> object.</p>  | /v1/issuing/disputes/{dispute}\_\_\_get |
| GetIssuingSettlementsSettlement - Retrieves an Issuing <code>Settlement</code> objec... | <p>Retrieves an Issuing <code>Settlement</code> object.</p>  | /v1/issuing/settlements/{settlement}\_\_\_get |
| GetIssuingTransactionsTransaction - Retrieves an Issuing <code>Transaction</code> obje... | <p>Retrieves an Issuing <code>Transaction</code> object.</p>  | /v1/issuing/transactions/{transaction}\_\_\_get |
| GetMandatesMandate - Retrieves a Mandate object. | <p>Retrieves a Mandate object.</p>  | /v1/mandates/{mandate}\_\_\_get |
| GetOrderReturnsId - Retrieves the details of an existing order return.... | <p>Retrieves the details of an existing order return. Supply the unique order ID from either an order return creation request or the order return list, and Stripe will return the corresponding order information.</p>  | /v1/order\_returns/{id}\_\_\_get |
| GetOrdersId - Retrieves the details of an existing order. Supply... | <p>Retrieves the details of an existing order. Supply the unique order ID from either an order creation request or the order list, and Stripe will return the corresponding order information.</p>  | /v1/orders/{id}\_\_\_get |
| GetPaymentIntentsIntent - Retrieves the details of a PaymentIntent that has ... | <p>Retrieves the details of a PaymentIntent that has previously been created. </p>  <p>Client-side retrieval using a publishable key is allowed when the <code>client_secret</code> is provided in the query string. </p>  <p>When retrieved with a publishable key, only a subset of properties will be returned. Please refer to the <a href="#payment_intent_object">payment intent</a> object reference for more details.</p>  | /v1/payment\_intents/{intent}\_\_\_get |
| GetPaymentMethodsPaymentMethod - Retrieves a PaymentMethod object. | <p>Retrieves a PaymentMethod object.</p>  | /v1/payment\_methods/{payment\_method}\_\_\_get |
| GetPayoutsPayout - Retrieves the details of an existing payout. Suppl... | <p>Retrieves the details of an existing payout. Supply the unique payout ID from either a payout creation request or the payout list, and Stripe will return the corresponding payout information.</p>  | /v1/payouts/{payout}\_\_\_get |
| GetPlansPlan - Retrieves the plan with the given ID. | <p>Retrieves the plan with the given ID.</p>  | /v1/plans/{plan}\_\_\_get |
| GetProductsId - Retrieves the details of an existing product. Supp... | <p>Retrieves the details of an existing product. Supply the unique product ID from either a product creation request or the product list, and Stripe will return the corresponding product information.</p>  | /v1/products/{id}\_\_\_get |
| GetRadarEarlyFraudWarningsEarlyFraudWarning - Retrieves the details of an early fraud warning th... | <p>Retrieves the details of an early fraud warning that has previously been created. </p>  <p>Please refer to the <a href="#early_fraud_warning_object">early fraud warning</a> object reference for more details.</p>  | /v1/radar/early\_fraud\_warnings/{early\_fraud\_warning}\_\_\_get |
| GetRadarValueListItemsItem - Retrieves a <code>ValueListItem</code> object. | <p>Retrieves a <code>ValueListItem</code> object.</p>  | /v1/radar/value\_list\_items/{item}\_\_\_get |
| GetRadarValueListsValueList - Retrieves a <code>ValueList</code> object. | <p>Retrieves a <code>ValueList</code> object.</p>  | /v1/radar/value\_lists/{value\_list}\_\_\_get |
| GetRecipientsId - Retrieves the details of an existing recipient. Yo... | <p>Retrieves the details of an existing recipient. You need only supply the unique recipient identifier that was returned upon recipient creation.</p>  | /v1/recipients/{id}\_\_\_get |
| GetRefundsRefund - Retrieves the details of an existing refund. | <p>Retrieves the details of an existing refund.</p>  | /v1/refunds/{refund}\_\_\_get |
| GetReportingReportRunsReportRun - Retrieves the details of an existing Report Run. (... | <p>Retrieves the details of an existing Report Run. (Requires a <a href="https://stripe.com/docs/keys#test-live-modes">live-mode API key</a>.)</p>  | /v1/reporting/report\_runs/{report\_run}\_\_\_get |
| GetReportingReportTypes - Returns a full list of Report Types. (Requires a <... | <p>Returns a full list of Report Types. (Requires a <a href="https://stripe.com/docs/keys#test-live-modes">live-mode API key</a>.)</p>  | /v1/reporting/report\_types\_\_\_get |
| GetReportingReportTypesReportType - Retrieves the details of a Report Type. (Requires ... | <p>Retrieves the details of a Report Type. (Requires a <a href="https://stripe.com/docs/keys#test-live-modes">live-mode API key</a>.)</p>  | /v1/reporting/report\_types/{report\_type}\_\_\_get |
| GetReviewsReview - Retrieves a <code>Review</code> object. | <p>Retrieves a <code>Review</code> object.</p>  | /v1/reviews/{review}\_\_\_get |
| GetSetupIntentsIntent - Retrieves the details of a SetupIntent that has pr... | <p>Retrieves the details of a SetupIntent that has previously been created. </p>  <p>Client-side retrieval using a publishable key is allowed when the <code>client_secret</code> is provided in the query string. </p>  <p>When retrieved with a publishable key, only a subset of properties will be returned. Please refer to the <a href="#setup_intent_object">SetupIntent</a> object reference for more details.</p>  | /v1/setup\_intents/{intent}\_\_\_get |
| GetSigmaScheduledQueryRunsScheduledQueryRun - Retrieves the details of an scheduled query run. | <p>Retrieves the details of an scheduled query run.</p>  | /v1/sigma/scheduled\_query\_runs/{scheduled\_query\_run}\_\_\_get |
| GetSkusId - Retrieves the details of an existing SKU. Supply t... | <p>Retrieves the details of an existing SKU. Supply the unique SKU identifier from either a SKU creation request or from the product, and Stripe will return the corresponding SKU information.</p>  | /v1/skus/{id}\_\_\_get |
| GetSourcesSource - Retrieves an existing source object. Supply the un... | <p>Retrieves an existing source object. Supply the unique source ID from a source creation request and Stripe will return the corresponding up-to-date source object information.</p>  | /v1/sources/{source}\_\_\_get |
| GetSourcesSourceMandateNotificationsMandateNotification - Retrieves a new Source MandateNotification. | <p>Retrieves a new Source MandateNotification.</p>  | /v1/sources/{source}/mandate\_notifications/{mandate\_notification}\_\_\_get |
| GetSourcesSourceSourceTransactionsSourceTransaction - Retrieve an existing source transaction object. Su... | <p>Retrieve an existing source transaction object. Supply the unique source ID from a source creation request and the source transaction ID and Stripe will return the corresponding up-to-date source object information.</p>  | /v1/sources/{source}/source\_transactions/{source\_transaction}\_\_\_get |
| GetSubscriptionItemsItem - Retrieves the invoice item with the given ID. | <p>Retrieves the invoice item with the given ID.</p>  | /v1/subscription\_items/{item}\_\_\_get |
| GetSubscriptionSchedulesSchedule - Retrieves the details of an existing subscription ... | <p>Retrieves the details of an existing subscription schedule. You only need to supply the unique subscription schedule identifier that was returned upon subscription schedule creation.</p>  | /v1/subscription\_schedules/{schedule}\_\_\_get |
| GetSubscriptionsSubscriptionExposedId - Retrieves the subscription with the given ID. | <p>Retrieves the subscription with the given ID.</p>  | /v1/subscriptions/{subscription\_exposed\_id}\_\_\_get |
| GetTaxRatesTaxRate - Retrieves a tax rate with the given ID | <p>Retrieves a tax rate with the given ID</p>  | /v1/tax\_rates/{tax\_rate}\_\_\_get |
| GetTerminalLocationsLocation - Retrieves a <code>Location</code> object. | <p>Retrieves a <code>Location</code> object.</p>  | /v1/terminal/locations/{location}\_\_\_get |
| GetTerminalReadersReader - Retrieves a <code>Reader</code> object. | <p>Retrieves a <code>Reader</code> object.</p>  | /v1/terminal/readers/{reader}\_\_\_get |
| GetTokensToken - Retrieves the token with the given ID. | <p>Retrieves the token with the given ID.</p>  | /v1/tokens/{token}\_\_\_get |
| GetTopupsTopup - Retrieves the details of a top-up that has previou... | <p>Retrieves the details of a top-up that has previously been created. Supply the unique top-up ID that was returned from your previous request, and Stripe will return the corresponding top-up information.</p>  | /v1/topups/{topup}\_\_\_get |
| GetTransfersTransfer - Retrieves the details of an existing transfer. Sup... | <p>Retrieves the details of an existing transfer. Supply the unique transfer ID from either a transfer creation request or the transfer list, and Stripe will return the corresponding transfer information.</p>  | /v1/transfers/{transfer}\_\_\_get |
| GetTransfersTransferReversalsId - By default, you can see the 10 most recent reversa... | <p>By default, you can see the 10 most recent reversals stored directly on the transfer object, but you can also retrieve details about a specific reversal stored on the transfer.</p>  | /v1/transfers/{transfer}/reversals/{id}\_\_\_get |
| GetWebhookEndpointsWebhookEndpoint - Retrieves the webhook endpoint with the given ID. | <p>Retrieves the webhook endpoint with the given ID.</p>  | /v1/webhook\_endpoints/{webhook\_endpoint}\_\_\_get |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Get3dSecureThreeDSecure - Retrieves a 3D Secure object. | Retrieves a 3D Secure object. | /v1/3d\_secure/{three\_d\_secure}\_\_\_get |
| GetAccountBankAccountsId - Retrieve a specified external account for a given ... | Retrieve a specified external account for a given ... | /v1/account/bank\_accounts/{id}\_\_\_get |
| GetAccountCapabilitiesCapability - Retrieves information about the specified Account ... | Retrieves information about the specified Account ... | /v1/account/capabilities/{capability}\_\_\_get |
| GetAccountExternalAccountsId - Retrieve a specified external account for a given ... | Retrieve a specified external account for a given ... | /v1/account/external\_accounts/{id}\_\_\_get |
| GetAccountPeoplePerson - Retrieves an existing person. | Retrieves an existing person. | /v1/account/people/{person}\_\_\_get |
| GetAccountPersonsPerson - Retrieves an existing person. | Retrieves an existing person. | /v1/account/persons/{person}\_\_\_get |
| GetAccountsAccount - Retrieves the details of an account. | Retrieves the details of an account. | /v1/accounts/{account}\_\_\_get |
| GetApplePayDomainsDomain - Retrieve an apple pay domain. | Retrieve an apple pay domain. | /v1/apple\_pay/domains/{domain}\_\_\_get |
| GetApplicationFeesId - Retrieves the details of an application fee that y... | Retrieves the details of an application fee that y... | /v1/application\_fees/{id}\_\_\_get |
| GetBalanceHistoryId - Retrieves the balance transaction with the given I... | Retrieves the balance transaction with the given I... | /v1/balance/history/{id}\_\_\_get |
| GetBalanceTransactionsId - Retrieves the balance transaction with the given I... | Retrieves the balance transaction with the given I... | /v1/balance\_transactions/{id}\_\_\_get |
| GetBitcoinReceiversId - Retrieves the Bitcoin receiver with the given ID. | Retrieves the Bitcoin receiver with the given ID. | /v1/bitcoin/receivers/{id}\_\_\_get |
| GetChargesCharge - Retrieves the details of a charge that has previou... | Retrieves the details of a charge that has previou... | /v1/charges/{charge}\_\_\_get |
| GetCheckoutSessionsSession - Retrieves a Session object. | Retrieves a Session object. | /v1/checkout/sessions/{session}\_\_\_get |
| GetCountrySpecsCountry - Returns a Country Spec for a given Country code. | Returns a Country Spec for a given Country code. | /v1/country\_specs/{country}\_\_\_get |
| GetCouponsCoupon - Retrieves the coupon with the given ID. | Retrieves the coupon with the given ID. | /v1/coupons/{coupon}\_\_\_get |
| GetCreditNotesId - Retrieves the credit note object with the given id... | Retrieves the credit note object with the given id... | /v1/credit\_notes/{id}\_\_\_get |
| GetCustomersCustomer - Retrieves the details of an existing customer. You... | Retrieves the details of an existing customer. You... | /v1/customers/{customer}\_\_\_get |
| GetDisputesDispute - Retrieves the dispute with the given ID. | Retrieves the dispute with the given ID. | /v1/disputes/{dispute}\_\_\_get |
| GetEventsId - Retrieves the details of an event. Supply the uniq... | Retrieves the details of an event. Supply the uniq... | /v1/events/{id}\_\_\_get |
| GetExchangeRatesCurrency - Retrieves the exchange rates from the given curren... | Retrieves the exchange rates from the given curren... | /v1/exchange\_rates/{currency}\_\_\_get |
| GetFileLinksLink - Retrieves the file link with the given ID. | Retrieves the file link with the given ID. | /v1/file\_links/{link}\_\_\_get |
| GetFilesFile - Retrieves the details of an existing file object. ... | Retrieves the details of an existing file object. ... | /v1/files/{file}\_\_\_get |
| GetInvoiceitemsInvoiceitem - Retrieves the invoice item with the given ID. | Retrieves the invoice item with the given ID. | /v1/invoiceitems/{invoiceitem}\_\_\_get |
| GetInvoicesInvoice - Retrieves the invoice with the given ID. | Retrieves the invoice with the given ID. | /v1/invoices/{invoice}\_\_\_get |
| GetIssuerFraudRecordsIssuerFraudRecord - Retrieves the details of an issuer fraud record th... | Retrieves the details of an issuer fraud record th... | /v1/issuer\_fraud\_records/{issuer\_fraud\_record}\_\_\_get |
| GetIssuingAuthorizationsAuthorization - Retrieves an Issuing <code>Authorization</code> ob... | Retrieves an Issuing <code>Authorization</code> ob... | /v1/issuing/authorizations/{authorization}\_\_\_get |
| GetIssuingCardholdersCardholder - Retrieves an Issuing <code>Cardholder</code> objec... | Retrieves an Issuing <code>Cardholder</code> objec... | /v1/issuing/cardholders/{cardholder}\_\_\_get |
| GetIssuingCardsCard - Retrieves an Issuing <code>Card</code> object. | Retrieves an Issuing <code>Card</code> object. | /v1/issuing/cards/{card}\_\_\_get |
| GetIssuingDisputesDispute - Retrieves an Issuing <code>Dispute</code> object. | Retrieves an Issuing <code>Dispute</code> object. | /v1/issuing/disputes/{dispute}\_\_\_get |
| GetIssuingSettlementsSettlement - Retrieves an Issuing <code>Settlement</code> objec... | Retrieves an Issuing <code>Settlement</code> objec... | /v1/issuing/settlements/{settlement}\_\_\_get |
| GetIssuingTransactionsTransaction - Retrieves an Issuing <code>Transaction</code> obje... | Retrieves an Issuing <code>Transaction</code> obje... | /v1/issuing/transactions/{transaction}\_\_\_get |
| GetMandatesMandate - Retrieves a Mandate object. | Retrieves a Mandate object. | /v1/mandates/{mandate}\_\_\_get |
| GetOrderReturnsId - Retrieves the details of an existing order return.... | Retrieves the details of an existing order return.... | /v1/order\_returns/{id}\_\_\_get |
| GetOrdersId - Retrieves the details of an existing order. Supply... | Retrieves the details of an existing order. Supply... | /v1/orders/{id}\_\_\_get |
| GetPaymentIntentsIntent - Retrieves the details of a PaymentIntent that has ... | Retrieves the details of a PaymentIntent that has ... | /v1/payment\_intents/{intent}\_\_\_get |
| GetPaymentMethodsPaymentMethod - Retrieves a PaymentMethod object. | Retrieves a PaymentMethod object. | /v1/payment\_methods/{payment\_method}\_\_\_get |
| GetPayoutsPayout - Retrieves the details of an existing payout. Suppl... | Retrieves the details of an existing payout. Suppl... | /v1/payouts/{payout}\_\_\_get |
| GetPlansPlan - Retrieves the plan with the given ID. | Retrieves the plan with the given ID. | /v1/plans/{plan}\_\_\_get |
| GetProductsId - Retrieves the details of an existing product. Supp... | Retrieves the details of an existing product. Supp... | /v1/products/{id}\_\_\_get |
| GetRadarEarlyFraudWarningsEarlyFraudWarning - Retrieves the details of an early fraud warning th... | Retrieves the details of an early fraud warning th... | /v1/radar/early\_fraud\_warnings/{early\_fraud\_warning}\_\_\_get |
| GetRadarValueListItemsItem - Retrieves a <code>ValueListItem</code> object. | Retrieves a <code>ValueListItem</code> object. | /v1/radar/value\_list\_items/{item}\_\_\_get |
| GetRadarValueListsValueList - Retrieves a <code>ValueList</code> object. | Retrieves a <code>ValueList</code> object. | /v1/radar/value\_lists/{value\_list}\_\_\_get |
| GetRecipientsId - Retrieves the details of an existing recipient. Yo... | Retrieves the details of an existing recipient. Yo... | /v1/recipients/{id}\_\_\_get |
| GetRefundsRefund - Retrieves the details of an existing refund. | Retrieves the details of an existing refund. | /v1/refunds/{refund}\_\_\_get |
| GetReportingReportRunsReportRun - Retrieves the details of an existing Report Run. (... | Retrieves the details of an existing Report Run. (... | /v1/reporting/report\_runs/{report\_run}\_\_\_get |
| GetReportingReportTypesReportType - Retrieves the details of a Report Type. (Requires ... | Retrieves the details of a Report Type. (Requires ... | /v1/reporting/report\_types/{report\_type}\_\_\_get |
| GetReviewsReview - Retrieves a <code>Review</code> object. | Retrieves a <code>Review</code> object. | /v1/reviews/{review}\_\_\_get |
| GetSetupIntentsIntent - Retrieves the details of a SetupIntent that has pr... | Retrieves the details of a SetupIntent that has pr... | /v1/setup\_intents/{intent}\_\_\_get |
| GetSigmaScheduledQueryRunsScheduledQueryRun - Retrieves the details of an scheduled query run. | Retrieves the details of an scheduled query run. | /v1/sigma/scheduled\_query\_runs/{scheduled\_query\_run}\_\_\_get |
| GetSkusId - Retrieves the details of an existing SKU. Supply t... | Retrieves the details of an existing SKU. Supply t... | /v1/skus/{id}\_\_\_get |
| GetSourcesSource - Retrieves an existing source object. Supply the un... | Retrieves an existing source object. Supply the un... | /v1/sources/{source}\_\_\_get |
| GetSubscriptionItemsItem - Retrieves the invoice item with the given ID. | Retrieves the invoice item with the given ID. | /v1/subscription\_items/{item}\_\_\_get |
| GetSubscriptionSchedulesSchedule - Retrieves the details of an existing subscription ... | Retrieves the details of an existing subscription ... | /v1/subscription\_schedules/{schedule}\_\_\_get |
| GetSubscriptionsSubscriptionExposedId - Retrieves the subscription with the given ID. | Retrieves the subscription with the given ID. | /v1/subscriptions/{subscription\_exposed\_id}\_\_\_get |
| GetTaxRatesTaxRate - Retrieves a tax rate with the given ID | Retrieves a tax rate with the given ID | /v1/tax\_rates/{tax\_rate}\_\_\_get |
| GetTerminalLocationsLocation - Retrieves a <code>Location</code> object. | Retrieves a <code>Location</code> object. | /v1/terminal/locations/{location}\_\_\_get |
| GetTerminalReadersReader - Retrieves a <code>Reader</code> object. | Retrieves a <code>Reader</code> object. | /v1/terminal/readers/{reader}\_\_\_get |
| GetTokensToken - Retrieves the token with the given ID. | Retrieves the token with the given ID. | /v1/tokens/{token}\_\_\_get |
| GetTopupsTopup - Retrieves the details of a top-up that has previou... | Retrieves the details of a top-up that has previou... | /v1/topups/{topup}\_\_\_get |
| GetTransfersTransfer - Retrieves the details of an existing transfer. Sup... | Retrieves the details of an existing transfer. Sup... | /v1/transfers/{transfer}\_\_\_get |
| GetWebhookEndpointsWebhookEndpoint - Retrieves the webhook endpoint with the given ID. | Retrieves the webhook endpoint with the given ID. | /v1/webhook\_endpoints/{webhook\_endpoint}\_\_\_get |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| GetAccountExternalAccounts - List external accounts for an account. | <p>List external accounts for an account.</p>  | /v1/account/external\_accounts\_\_\_get |
| GetAccountPeople - Returns a list of people associated with the accou... | <p>Returns a list of people associated with the account’s legal entity. The people are returned sorted by creation date, with the most recent people appearing first.</p>  | /v1/account/people\_\_\_get |
| GetAccountPersons - Returns a list of people associated with the accou... | <p>Returns a list of people associated with the account’s legal entity. The people are returned sorted by creation date, with the most recent people appearing first.</p>  | /v1/account/persons\_\_\_get |
| GetAccounts - Returns a list of accounts connected to your platf... | <p>Returns a list of accounts connected to your platform via <a href="/docs/connect">Connect</a>. If you’re not a platform, the list is empty.</p>  | /v1/accounts\_\_\_get |
| GetAccountsAccountExternalAccounts - List external accounts for an account. | <p>List external accounts for an account.</p>  | /v1/accounts/{account}/external\_accounts\_\_\_get |
| GetAccountsAccountPeople - Returns a list of people associated with the accou... | <p>Returns a list of people associated with the account’s legal entity. The people are returned sorted by creation date, with the most recent people appearing first.</p>  | /v1/accounts/{account}/people\_\_\_get |
| GetAccountsAccountPersons - Returns a list of people associated with the accou... | <p>Returns a list of people associated with the account’s legal entity. The people are returned sorted by creation date, with the most recent people appearing first.</p>  | /v1/accounts/{account}/persons\_\_\_get |
| GetApplePayDomains - List apple pay domains. | <p>List apple pay domains.</p>  | /v1/apple\_pay/domains\_\_\_get |
| GetApplicationFees - Returns a list of application fees you’ve previo... | <p>Returns a list of application fees you’ve previously collected. The application fees are returned in sorted order, with the most recent fees appearing first.</p>  | /v1/application\_fees\_\_\_get |
| GetApplicationFeesIdRefunds - You can see a list of the refunds belonging to a s... | <p>You can see a list of the refunds belonging to a specific application fee. Note that the 10 most recent refunds are always available by default on the application fee object. If you need more than those 10, you can use this API method and the <code>limit</code> and <code>starting_after</code> parameters to page through additional refunds.</p>  | /v1/application\_fees/{id}/refunds\_\_\_get |
| GetBalanceHistory - Returns a list of transactions that have contribut... | <p>Returns a list of transactions that have contributed to the Stripe account balance (e.g., charges, transfers, and so forth). The transactions are returned in sorted order, with the most recent transactions appearing first.</p>  <p>Note that this endpoint was previously called “Balance history�? and used the path <code>/v1/balance/history</code>.</p>  | /v1/balance/history\_\_\_get |
| GetBalanceTransactions - Returns a list of transactions that have contribut... | <p>Returns a list of transactions that have contributed to the Stripe account balance (e.g., charges, transfers, and so forth). The transactions are returned in sorted order, with the most recent transactions appearing first.</p>  <p>Note that this endpoint was previously called “Balance history�? and used the path <code>/v1/balance/history</code>.</p>  | /v1/balance\_transactions\_\_\_get |
| GetBitcoinReceivers - Returns a list of your receivers. Receivers are re... | <p>Returns a list of your receivers. Receivers are returned sorted by creation date, with the most recently created receivers appearing first.</p>  | /v1/bitcoin/receivers\_\_\_get |
| GetBitcoinReceiversReceiverTransactions - List bitcoin transacitons for a given receiver. | <p>List bitcoin transacitons for a given receiver.</p>  | /v1/bitcoin/receivers/{receiver}/transactions\_\_\_get |
| GetBitcoinTransactions - List bitcoin transacitons for a given receiver. | <p>List bitcoin transacitons for a given receiver.</p>  | /v1/bitcoin/transactions\_\_\_get |
| GetCharges - Returns a list of charges you’ve previously crea... | <p>Returns a list of charges you’ve previously created. The charges are returned in sorted order, with the most recent charges appearing first.</p>  | /v1/charges\_\_\_get |
| GetChargesChargeRefunds - You can see a list of the refunds belonging to a s... | <p>You can see a list of the refunds belonging to a specific charge. Note that the 10 most recent refunds are always available by default on the charge object. If you need more than those 10, you can use this API method and the <code>limit</code> and <code>starting_after</code> parameters to page through additional refunds.</p>  | /v1/charges/{charge}/refunds\_\_\_get |
| GetCountrySpecs - Lists all Country Spec objects available in the AP... | <p>Lists all Country Spec objects available in the API.</p>  | /v1/country\_specs\_\_\_get |
| GetCoupons - Returns a list of your coupons. | <p>Returns a list of your coupons.</p>  | /v1/coupons\_\_\_get |
| GetCreditNotes - Returns a list of credit notes. | <p>Returns a list of credit notes.</p>  | /v1/credit\_notes\_\_\_get |
| GetCustomers - Returns a list of your customers. The customers ar... | <p>Returns a list of your customers. The customers are returned sorted by creation date, with the most recent customers appearing first.</p>  | /v1/customers\_\_\_get |
| GetCustomersCustomerBalanceTransactions - Returns a list of transactions that updated the cu... | <p>Returns a list of transactions that updated the customer’s <a href="/docs/api/customers/object#customer_object-balance"><code>balance</code></a>.</p>  | /v1/customers/{customer}/balance\_transactions\_\_\_get |
| GetCustomersCustomerBankAccounts - You can see a list of the bank accounts belonging ... | <p>You can see a list of the bank accounts belonging to a Customer. Note that the 10 most recent sources are always available by default on the Customer. If you need more than those 10, you can use this API method and the <code>limit</code> and <code>starting_after</code> parameters to page through additional bank accounts.</p>  | /v1/customers/{customer}/bank\_accounts\_\_\_get |
| GetCustomersCustomerCards - You can see a list of the cards belonging to a cus... | <p>You can see a list of the cards belonging to a customer. Note that the 10 most recent sources are always available on the <code>Customer</code> object. If you need more than those 10, you can use this API method and the <code>limit</code> and <code>starting_after</code> parameters to page through additional cards.</p>  | /v1/customers/{customer}/cards\_\_\_get |
| GetCustomersCustomerSources - List sources for a specified customer. | <p>List sources for a specified customer.</p>  | /v1/customers/{customer}/sources\_\_\_get |
| GetCustomersCustomerSubscriptions - You can see a list of the customer’s active subs... | <p>You can see a list of the customer’s active subscriptions. Note that the 10 most recent active subscriptions are always available by default on the customer object. If you need more than those 10, you can use the limit and starting_after parameters to page through additional subscriptions.</p>  | /v1/customers/{customer}/subscriptions\_\_\_get |
| GetCustomersCustomerTaxIds - Returns a list of tax IDs for a customer. | <p>Returns a list of tax IDs for a customer.</p>  | /v1/customers/{customer}/tax\_ids\_\_\_get |
| GetDisputes - Returns a list of your disputes. | <p>Returns a list of your disputes.</p>  | /v1/disputes\_\_\_get |
| GetEvents - List events, going back up to 30 days. Each event ... | <p>List events, going back up to 30 days. Each event data is rendered according to Stripe API version at its creation time, specified in <a href="/docs/api/events/object">event object</a> <code>api_version</code> attribute (not according to your current Stripe API version or <code>Stripe-Version</code> header).</p>  | /v1/events\_\_\_get |
| GetExchangeRates - Returns a list of objects that contain the rates a... | <p>Returns a list of objects that contain the rates at which foreign currencies are converted to one another. Only shows the currencies for which Stripe supports.</p>  | /v1/exchange\_rates\_\_\_get |
| GetFileLinks - Returns a list of file links. | <p>Returns a list of file links.</p>  | /v1/file\_links\_\_\_get |
| GetFiles - Returns a list of the files that your account has ... | <p>Returns a list of the files that your account has access to. The files are returned sorted by creation date, with the most recently created files appearing first.</p>  | /v1/files\_\_\_get |
| GetInvoiceitems - Returns a list of your invoice items. Invoice item... | <p>Returns a list of your invoice items. Invoice items are returned sorted by creation date, with the most recently created invoice items appearing first.</p>  | /v1/invoiceitems\_\_\_get |
| GetInvoices - You can list all invoices, or list the invoices fo... | <p>You can list all invoices, or list the invoices for a specific customer. The invoices are returned sorted by creation date, with the most recently created invoices appearing first.</p>  | /v1/invoices\_\_\_get |
| GetInvoicesInvoiceLines - When retrieving an invoice, you’ll get a <strong... | <p>When retrieving an invoice, you’ll get a <strong>lines</strong> property containing the total count of line items and the first handful of those items. There is also a URL where you can retrieve the full (paginated) list of line items.</p>  | /v1/invoices/{invoice}/lines\_\_\_get |
| GetInvoicesUpcomingLines - When retrieving an upcoming invoice, you’ll get ... | <p>When retrieving an upcoming invoice, you’ll get a <strong>lines</strong> property containing the total count of line items and the first handful of those items. There is also a URL where you can retrieve the full (paginated) list of line items.</p>  | /v1/invoices/upcoming/lines\_\_\_get |
| GetIssuerFraudRecords - Returns a list of issuer fraud records. | <p>Returns a list of issuer fraud records.</p>  | /v1/issuer\_fraud\_records\_\_\_get |
| GetIssuingAuthorizations - Returns a list of Issuing <code>Authorization</cod... | <p>Returns a list of Issuing <code>Authorization</code> objects. The objects are sorted in descending order by creation date, with the most recently created object appearing first.</p>  | /v1/issuing/authorizations\_\_\_get |
| GetIssuingCardholders - Returns a list of Issuing <code>Cardholder</code> ... | <p>Returns a list of Issuing <code>Cardholder</code> objects. The objects are sorted in descending order by creation date, with the most recently created object appearing first.</p>  | /v1/issuing/cardholders\_\_\_get |
| GetIssuingCards - Returns a list of Issuing <code>Card</code> object... | <p>Returns a list of Issuing <code>Card</code> objects. The objects are sorted in descending order by creation date, with the most recently created object appearing first.</p>  | /v1/issuing/cards\_\_\_get |
| GetIssuingDisputes - Returns a list of Issuing <code>Dispute</code> obj... | <p>Returns a list of Issuing <code>Dispute</code> objects. The objects are sorted in descending order by creation date, with the most recently created object appearing first.</p>  | /v1/issuing/disputes\_\_\_get |
| GetIssuingSettlements - Returns a list of Issuing <code>Settlement</code> ... | <p>Returns a list of Issuing <code>Settlement</code> objects. The objects are sorted in descending order by creation date, with the most recently created object appearing first.</p>  | /v1/issuing/settlements\_\_\_get |
| GetIssuingTransactions - Returns a list of Issuing <code>Transaction</code>... | <p>Returns a list of Issuing <code>Transaction</code> objects. The objects are sorted in descending order by creation date, with the most recently created object appearing first.</p>  | /v1/issuing/transactions\_\_\_get |
| GetOrderReturns - Returns a list of your order returns. The returns ... | <p>Returns a list of your order returns. The returns are returned sorted by creation date, with the most recently created return appearing first.</p>  | /v1/order\_returns\_\_\_get |
| GetOrders - Returns a list of your orders. The orders are retu... | <p>Returns a list of your orders. The orders are returned sorted by creation date, with the most recently created orders appearing first.</p>  | /v1/orders\_\_\_get |
| GetPaymentIntents - Returns a list of PaymentIntents. | <p>Returns a list of PaymentIntents.</p>  | /v1/payment\_intents\_\_\_get |
| GetPaymentMethods - Returns a list of PaymentMethods for a given Custo... | <p>Returns a list of PaymentMethods for a given Customer</p>  | /v1/payment\_methods\_\_\_get |
| GetPayouts - Returns a list of existing payouts sent to third-p... | <p>Returns a list of existing payouts sent to third-party bank accounts or that Stripe has sent you. The payouts are returned in sorted order, with the most recently created payouts appearing first.</p>  | /v1/payouts\_\_\_get |
| GetPlans - Returns a list of your plans. | <p>Returns a list of your plans.</p>  | /v1/plans\_\_\_get |
| GetProducts - Returns a list of your products. The products are ... | <p>Returns a list of your products. The products are returned sorted by creation date, with the most recently created products appearing first.</p>  | /v1/products\_\_\_get |
| GetRadarEarlyFraudWarnings - Returns a list of early fraud warnings. | <p>Returns a list of early fraud warnings.</p>  | /v1/radar/early\_fraud\_warnings\_\_\_get |
| GetRadarValueListItems - Returns a list of <code>ValueListItem</code> objec... | <p>Returns a list of <code>ValueListItem</code> objects. The objects are sorted in descending order by creation date, with the most recently created object appearing first.</p>  | /v1/radar/value\_list\_items\_\_\_get |
| GetRadarValueLists - Returns a list of <code>ValueList</code> objects. ... | <p>Returns a list of <code>ValueList</code> objects. The objects are sorted in descending order by creation date, with the most recently created object appearing first.</p>  | /v1/radar/value\_lists\_\_\_get |
| GetRecipients - Returns a list of your recipients. The recipients ... | <p>Returns a list of your recipients. The recipients are returned sorted by creation date, with the most recently created recipients appearing first.</p>  | /v1/recipients\_\_\_get |
| GetRefunds - Returns a list of all refunds you’ve previously ... | <p>Returns a list of all refunds you’ve previously created. The refunds are returned in sorted order, with the most recent refunds appearing first. For convenience, the 10 most recent refunds are always available by default on the charge object.</p>  | /v1/refunds\_\_\_get |
| GetReportingReportRuns - Returns a list of Report Runs, with the most recen... | <p>Returns a list of Report Runs, with the most recent appearing first. (Requires a <a href="https://stripe.com/docs/keys#test-live-modes">live-mode API key</a>.)</p>  | /v1/reporting/report\_runs\_\_\_get |
| GetReviews - Returns a list of <code>Review</code> objects that... | <p>Returns a list of <code>Review</code> objects that have <code>open</code> set to <code>true</code>. The objects are sorted in descending order by creation date, with the most recently created object appearing first.</p>  | /v1/reviews\_\_\_get |
| GetSetupIntents - Returns a list of SetupIntents. | <p>Returns a list of SetupIntents.</p>  | /v1/setup\_intents\_\_\_get |
| GetSigmaScheduledQueryRuns - Returns a list of scheduled query runs. | <p>Returns a list of scheduled query runs.</p>  | /v1/sigma/scheduled\_query\_runs\_\_\_get |
| GetSkus - Returns a list of your SKUs. The SKUs are returned... | <p>Returns a list of your SKUs. The SKUs are returned sorted by creation date, with the most recently created SKUs appearing first.</p>  | /v1/skus\_\_\_get |
| GetSourcesSourceSourceTransactions - List source transactions for a given source. | <p>List source transactions for a given source.</p>  | /v1/sources/{source}/source\_transactions\_\_\_get |
| GetSubscriptionItems - Returns a list of your subscription items for a gi... | <p>Returns a list of your subscription items for a given subscription.</p>  | /v1/subscription\_items\_\_\_get |
| GetSubscriptionItemsSubscriptionItemUsageRecordSummaries - For the specified subscription item, returns a lis... | <p>For the specified subscription item, returns a list of summary objects. Each object in the list provides usage information that’s been summarized from multiple usage records and over a subscription billing period (e.g., 15 usage records in the billing plan’s month of September).</p>  <p>The list is sorted in reverse-chronological order (newest first). The first list item represents the most current usage period that hasn’t ended yet. Since new usage records can still be added, the returned summary information for the subscription item’s ID should be seen as unstable until the subscription billing period ends.</p>  | /v1/subscription\_items/{subscription\_item}/usage\_record\_summaries\_\_\_get |
| GetSubscriptions - By default, returns a list of subscriptions that h... | <p>By default, returns a list of subscriptions that have not been canceled. In order to list canceled subscriptions, specify <code>status=canceled</code>.</p>  | /v1/subscriptions\_\_\_get |
| GetSubscriptionSchedules - Retrieves the list of your subscription schedules. | <p>Retrieves the list of your subscription schedules.</p>  | /v1/subscription\_schedules\_\_\_get |
| GetTaxRates - Returns a list of your tax rates. Tax rates are re... | <p>Returns a list of your tax rates. Tax rates are returned sorted by creation date, with the most recently created tax rates appearing first.</p>  | /v1/tax\_rates\_\_\_get |
| GetTerminalLocations - Returns a list of <code>Location</code> objects. | <p>Returns a list of <code>Location</code> objects.</p>  | /v1/terminal/locations\_\_\_get |
| GetTerminalReaders - Returns a list of <code>Reader</code> objects. | <p>Returns a list of <code>Reader</code> objects.</p>  | /v1/terminal/readers\_\_\_get |
| GetTopups - Returns a list of top-ups. | <p>Returns a list of top-ups.</p>  | /v1/topups\_\_\_get |
| GetTransfers - Returns a list of existing transfers sent to conne... | <p>Returns a list of existing transfers sent to connected accounts. The transfers are returned in sorted order, with the most recently created transfers appearing first.</p>  | /v1/transfers\_\_\_get |
| GetTransfersIdReversals - You can see a list of the reversals belonging to a... | <p>You can see a list of the reversals belonging to a specific transfer. Note that the 10 most recent reversals are always available by default on the transfer object. If you need more than those 10, you can use this API method and the <code>limit</code> and <code>starting_after</code> parameters to page through additional reversals.</p>  | /v1/transfers/{id}/reversals\_\_\_get |
| GetWebhookEndpoints - Returns a list of your webhook endpoints. | <p>Returns a list of your webhook endpoints.</p>  | /v1/webhook\_endpoints\_\_\_get |


### UPDATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| PostAccountBankAccountsId - Updates the metadata, account holder name, and acc... (POST) | Updates the metadata, account holder name, and acc... | /v1/account/bank\_accounts/{id}\_\_\_post |
| PostAccountCapabilitiesCapability - Updates an existing Account Capability. (POST) | Updates an existing Account Capability. | /v1/account/capabilities/{capability}\_\_\_post |
| PostAccountExternalAccountsId - Updates the metadata, account holder name, and acc... (POST) | Updates the metadata, account holder name, and acc... | /v1/account/external\_accounts/{id}\_\_\_post |
| PostAccountPeoplePerson - Updates an existing person. (POST) | Updates an existing person. | /v1/account/people/{person}\_\_\_post |
| PostAccountPersonsPerson - Updates an existing person. (POST) | Updates an existing person. | /v1/account/persons/{person}\_\_\_post |
| PostAccountsAccount - Updates a connected <a href="/docs/connect/account... (POST) | Updates a connected <a href="/docs/connect/account... | /v1/accounts/{account}\_\_\_post |
| PostAccountsAccountBankAccountsId - Updates the metadata, account holder name, and acc... (POST) | Updates the metadata, account holder name, and acc... | /v1/accounts/{account}/bank\_accounts/{id}\_\_\_post |
| PostAccountsAccountCapabilitiesCapability - Updates an existing Account Capability. (POST) | Updates an existing Account Capability. | /v1/accounts/{account}/capabilities/{capability}\_\_\_post |
| PostAccountsAccountExternalAccountsId - Updates the metadata, account holder name, and acc... (POST) | Updates the metadata, account holder name, and acc... | /v1/accounts/{account}/external\_accounts/{id}\_\_\_post |
| PostAccountsAccountPeoplePerson - Updates an existing person. (POST) | Updates an existing person. | /v1/accounts/{account}/people/{person}\_\_\_post |
| PostAccountsAccountPersonsPerson - Updates an existing person. (POST) | Updates an existing person. | /v1/accounts/{account}/persons/{person}\_\_\_post |
| PostApplicationFeesFeeRefundsId - Updates the specified application fee refund by se... (POST) | Updates the specified application fee refund by se... | /v1/application\_fees/{fee}/refunds/{id}\_\_\_post |
| PostChargesCharge - Updates the specified charge by setting the values... (POST) | Updates the specified charge by setting the values... | /v1/charges/{charge}\_\_\_post |
| PostChargesChargeRefundsRefund - Update a specified refund. (POST) | Update a specified refund. | /v1/charges/{charge}/refunds/{refund}\_\_\_post |
| PostCouponsCoupon - Updates the metadata of a coupon. Other coupon det... (POST) | Updates the metadata of a coupon. Other coupon det... | /v1/coupons/{coupon}\_\_\_post |
| PostCreditNotesId - Updates an existing credit note. (POST) | Updates an existing credit note. | /v1/credit\_notes/{id}\_\_\_post |
| PostCustomersCustomer - Updates the specified customer by setting the valu... (POST) | Updates the specified customer by setting the valu... | /v1/customers/{customer}\_\_\_post |
| PostCustomersCustomerBalanceTransactionsTransaction - Most customer balance transaction fields are immut... (POST) | Most customer balance transaction fields are immut... | /v1/customers/{customer}/balance\_transactions/{transaction}\_\_\_post |
| PostCustomersCustomerBankAccountsId - Update a specified source for a given customer. (POST) | Update a specified source for a given customer. | /v1/customers/{customer}/bank\_accounts/{id}\_\_\_post |
| PostCustomersCustomerCardsId - Update a specified source for a given customer. (POST) | Update a specified source for a given customer. | /v1/customers/{customer}/cards/{id}\_\_\_post |
| PostCustomersCustomerSourcesId - Update a specified source for a given customer. (POST) | Update a specified source for a given customer. | /v1/customers/{customer}/sources/{id}\_\_\_post |
| PostCustomersCustomerSubscriptionsSubscriptionExposedId - Updates an existing subscription on a customer to ... (POST) | Updates an existing subscription on a customer to ... | /v1/customers/{customer}/subscriptions/{subscription\_exposed\_id}\_\_\_post |
| PostDisputesDispute - When you get a dispute, contacting your customer i... (POST) | When you get a dispute, contacting your customer i... | /v1/disputes/{dispute}\_\_\_post |
| PostFileLinksLink - Updates an existing file link object. Expired link... (POST) | Updates an existing file link object. Expired link... | /v1/file\_links/{link}\_\_\_post |
| PostInvoiceitemsInvoiceitem - Updates the amount or description of an invoice it... (POST) | Updates the amount or description of an invoice it... | /v1/invoiceitems/{invoiceitem}\_\_\_post |
| PostInvoicesInvoice - Draft invoices are fully editable. Once an invoice... (POST) | Draft invoices are fully editable. Once an invoice... | /v1/invoices/{invoice}\_\_\_post |
| PostIssuingAuthorizationsAuthorization - Updates the specified Issuing <code>Authorization<... (POST) | Updates the specified Issuing <code>Authorization<... | /v1/issuing/authorizations/{authorization}\_\_\_post |
| PostIssuingCardholdersCardholder - Updates the specified Issuing <code>Cardholder</co... (POST) | Updates the specified Issuing <code>Cardholder</co... | /v1/issuing/cardholders/{cardholder}\_\_\_post |
| PostIssuingCardsCard - Updates the specified Issuing <code>Card</code> ob... (POST) | Updates the specified Issuing <code>Card</code> ob... | /v1/issuing/cards/{card}\_\_\_post |
| PostIssuingDisputesDispute - Updates the specified Issuing <code>Dispute</code>... (POST) | Updates the specified Issuing <code>Dispute</code>... | /v1/issuing/disputes/{dispute}\_\_\_post |
| PostIssuingSettlementsSettlement - Updates the specified Issuing <code>Settlement</co... (POST) | Updates the specified Issuing <code>Settlement</co... | /v1/issuing/settlements/{settlement}\_\_\_post |
| PostIssuingTransactionsTransaction - Updates the specified Issuing <code>Transaction</c... (POST) | Updates the specified Issuing <code>Transaction</c... | /v1/issuing/transactions/{transaction}\_\_\_post |
| PostOrdersId - Updates the specific order by setting the values o... (POST) | Updates the specific order by setting the values o... | /v1/orders/{id}\_\_\_post |
| PostPaymentIntentsIntent - Updates properties on a PaymentIntent object witho... (POST) | Updates properties on a PaymentIntent object witho... | /v1/payment\_intents/{intent}\_\_\_post |
| PostPaymentMethodsPaymentMethod - Updates a PaymentMethod object. A PaymentMethod mu... (POST) | Updates a PaymentMethod object. A PaymentMethod mu... | /v1/payment\_methods/{payment\_method}\_\_\_post |
| PostPayoutsPayout - Updates the specified payout by setting the values... (POST) | Updates the specified payout by setting the values... | /v1/payouts/{payout}\_\_\_post |
| PostPlansPlan - Updates the specified plan by setting the values o... (POST) | Updates the specified plan by setting the values o... | /v1/plans/{plan}\_\_\_post |
| PostProductsId - Updates the specific product by setting the values... (POST) | Updates the specific product by setting the values... | /v1/products/{id}\_\_\_post |
| PostRadarValueListsValueList - Updates a <code>ValueList</code> object by setting... (POST) | Updates a <code>ValueList</code> object by setting... | /v1/radar/value\_lists/{value\_list}\_\_\_post |
| PostRecipientsId - Updates the specified recipient by setting the val... (POST) | Updates the specified recipient by setting the val... | /v1/recipients/{id}\_\_\_post |
| PostRefundsRefund - Updates the specified refund by setting the values... (POST) | Updates the specified refund by setting the values... | /v1/refunds/{refund}\_\_\_post |
| PostSetupIntentsIntent - Updates a SetupIntent object. (POST) | Updates a SetupIntent object. | /v1/setup\_intents/{intent}\_\_\_post |
| PostSkusId - Updates the specific SKU by setting the values of ... (POST) | Updates the specific SKU by setting the values of ... | /v1/skus/{id}\_\_\_post |
| PostSourcesSource - Updates the specified source by setting the values... (POST) | Updates the specified source by setting the values... | /v1/sources/{source}\_\_\_post |
| PostSubscriptionItemsItem - Updates the plan or quantity of an item on a curre... (POST) | Updates the plan or quantity of an item on a curre... | /v1/subscription\_items/{item}\_\_\_post |
| PostSubscriptionSchedulesSchedule - Updates an existing subscription schedule. (POST) | Updates an existing subscription schedule. | /v1/subscription\_schedules/{schedule}\_\_\_post |
| PostSubscriptionsSubscriptionExposedId - Updates an existing subscription on a customer to ... (POST) | Updates an existing subscription on a customer to ... | /v1/subscriptions/{subscription\_exposed\_id}\_\_\_post |
| PostTaxRatesTaxRate - Updates an existing tax rate. (POST) | Updates an existing tax rate. | /v1/tax\_rates/{tax\_rate}\_\_\_post |
| PostTerminalLocationsLocation - Updates a <code>Location</code> object by setting ... (POST) | Updates a <code>Location</code> object by setting ... | /v1/terminal/locations/{location}\_\_\_post |
| PostTerminalReadersReader - Updates a <code>Reader</code> object by setting th... (POST) | Updates a <code>Reader</code> object by setting th... | /v1/terminal/readers/{reader}\_\_\_post |
| PostTopupsTopup - Updates the metadata of a top-up. Other top-up det... (POST) | Updates the metadata of a top-up. Other top-up det... | /v1/topups/{topup}\_\_\_post |
| PostTransfersTransfer - Updates the specified transfer by setting the valu... (POST) | Updates the specified transfer by setting the valu... | /v1/transfers/{transfer}\_\_\_post |
| PostTransfersTransferReversalsId - Updates the specified reversal by setting the valu... (POST) | Updates the specified reversal by setting the valu... | /v1/transfers/{transfer}/reversals/{id}\_\_\_post |
| PostWebhookEndpointsWebhookEndpoint - Updates the webhook endpoint. You may edit the <co... (POST) | Updates the webhook endpoint. You may edit the <co... | /v1/webhook\_endpoints/{webhook\_endpoint}\_\_\_post |
| PutAccountLogout - Invalidates all sessions for a light account, for ... (PUT) | Invalidates all sessions for a light account, for ... | /v1/account/logout\_\_\_put |
| PutAccountsAccountLogout - Invalidates all sessions for a light account, for ... (PUT) | Invalidates all sessions for a light account, for ... | /v1/accounts/{account}/logout\_\_\_put |


