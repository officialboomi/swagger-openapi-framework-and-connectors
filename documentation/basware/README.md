# Basware API
REST interface to Basware

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string



#### Basware API REST API Service URL

The URL for the Basware API REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### AuthenticationType

Allows user to select between BASIC and OAUTH 2.0 Authentication

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE/POST
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE/PATCH
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE/PUT
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| AccountingDocuments - Deletes data from Basware API. It does not remove ... | AccountingDocuments Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| Accounts - Deletes data from Basware API. It does not remove ... | Accounts Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| AdvancedPermissions - Deletes data from Basware API. It does not remove ... | AdvancedPermissions Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| AdvancedValidations - Deletes data from Basware API. It does not remove ... | AdvancedValidations Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| Contracts - Deletes data from Basware API. It does not remove ... | Contracts Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| CostCenters - Deletes data from Basware API. It does not remove ... | CostCenters Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| ErrorFeedbacks - Deletes data from Basware API. It does not remove ... | ErrorFeedbacks Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| ExchangeRates - Deletes data from Basware API. It does not remove ... | ExchangeRates Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| ExportedPurchaseOrders - Deletes data from Basware API. It does not remove ... | ExportedPurchaseOrders Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| ExportedPurchaseRequisitions - Deletes data from Basware API. It does not remove ... | ExportedPurchaseRequisitions Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| GenericLists - Deletes data from Basware API. It does not remove ... | GenericLists Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| MatchingOrderLines - Deletes data from Basware API. It does not remove ... | MatchingOrderLines Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| MatchingOrders - Deletes data from Basware API. It does not remove ... | MatchingOrders Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| PaymentTerms - Deletes data from Basware API. It does not remove ... | PaymentTerms Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| Projects v1 (deprecated) - Deletes data from Basware API. It does not remove ... | Projects v1 (deprecated) Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| Projects v2 - Deletes data from Basware API. It does not remove ... | Projects v2 Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| PurchaseGoodsReceipts - Deletes data from Basware API. It does not remove ... | PurchaseGoodsReceipts Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| PurchaseOrders - Deletes data from Basware API. It does not remove ... | PurchaseOrders Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| PurchaseRequisitions - Deletes data from Basware API. It does not remove ... | PurchaseRequisitions Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| TaxCodes - Deletes data from Basware API. It does not remove ... | TaxCodes Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| Users - Deletes data from Basware API. It does not remove ... | Users Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |
| Vendors - Deletes data from Basware API. It does not remove ... | Vendors Deletes data from Basware API. It does not remove the deleted data from target systems (such as P2P, Vendor manager, etc.).  Deletion in these target systems needs to be done separately using the data deletion mechanisms available in each of the target systems  in addition to deleting the data in Basware API.  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| AccountingDocuments - Returns single approved invoice by invoiceId -iden... | AccountingDocuments Note: This GET operation returns a HTTP redirect, which the API client needs to follow.  * 'Authorization' header must not be included in the redirected request (the second request after receiving a redirect).  * 'Host' header needs to be included.  Please see section "[Usage scenario 3: Prebook and transfer invoice to Accounting](https://developer.basware.com/api/p2p/manual#usage3)" of Basware Purchase-to-Pay API manual for details on implementing invoice transfer process with Basware API. Returns single approved invoice by invoiceId -identifier.  |
| Accounts - Returns single account record by externalCode -ide... | Accounts  Returns single account record by externalCode -identifier.  |
| AdvancedPermissions - Returns single advanced user rights -record by ext... | AdvancedPermissions  Returns single advanced user rights -record by externalCode -identifier.  |
| AdvancedValidations - Returns single coding row validation rules by exte... | AdvancedValidations  Returns single coding row validation rules by externalCode -identifier.  |
| ApplicationGroups - Returns single Application Group by application gr... | ApplicationGroups Application groups give access to a specific set of applications for users imported through 'users' API. Each user needs to be assigned to one or more application group(s) by specifying 'applicationGroupCode' values in users API. Returns single Application Group by application group code - identifier.  |
| Companies - Returns single company record by companyCode - ide... | Companies  Returns single company record by companyCode - identifier.  |
| Contracts - Returns single contract entity by ID | Contracts  Returns single contract entity by ID  |
| CostCenters - Returns single cost center -record by externalCode... | CostCenters  Returns single cost center -record by externalCode -identifier.  |
| ErrorFeedbacks - Returns error messages from receiving end-system(s... | ErrorFeedbacks If errors occur after Basware API has validated the data schema, these will be returned from target systems asynchronously in this interface.  Please see section "[Usage scenario 5: Error handling and monitoring](https://developer.basware.com/api/p2p/manual#usage5)" in Basware Purchase-to-Pay API manual for details on implementing error handling with Basware API. Returns error messages from receiving end-system(s), based on webservice request id.  |
| ErrorFeedbacks - Returns error messages from receiving end-system(s... | ErrorFeedbacks If errors occur after Basware API has validated the data schema, these will be returned from target systems asynchronously in this interface.  Please see section "[Usage scenario 5: Error handling and monitoring](https://developer.basware.com/api/p2p/manual#usage5)" in Basware Purchase-to-Pay API manual for details on implementing error handling with Basware API. Returns error messages from receiving end-system(s), based on interface and exernalCode -identifier of record.  |
| ExchangeRates - Returns single exchange rate -record by externalCo... | ExchangeRates  Returns single exchange rate -record by externalCode -identifier.  |
| ExportedPurchaseOrders - Returns single exported order by externalCode - id... | ExportedPurchaseOrders Note: This GET operation returns a HTTP redirect, which the API client needs to follow.  * 'Authorization' header must not be included in the redirected request (the second request after receiving a redirect).  * 'Host' header needs to be included.  Please see section "[Usage scenario 4: Import and export procurement data](https://developer.basware.com/api/p2p/manual#usage4)" for details on implementing this API. Returns single exported order by externalCode - identifier.  |
| ExportedPurchaseRequisitions - Returns a single purchase requisition by external ... | ExportedPurchaseRequisitions Note: This GET operation returns a HTTP redirect, which the API client needs to follow.  * 'Authorization' header must not be included in the redirected request (the second request after receiving a redirect).  * 'Host' header needs to be included.  Please see section "[Usage scenario 4: Import and export procurement data](https://developer.basware.com/api/p2p/manual#usage4)" for details on implementing this API. Returns a single purchase requisition by external code - identifier.  |
| GenericLists - Returns list items posted to Basware API for speci... | GenericLists Returns list items posted to Basware API for specified list.  |
| GenericLists - Returns single list item, based on specified list ... | GenericLists  Returns single list item, based on specified list and externalCode -identifier of the record.  |
| MatchingOrderLines - Returns single matching order line by externalCode... | MatchingOrderLines  Returns single matching order line by externalCode -identifier.  |
| MatchingOrders - Returns single matching order header by externalCo... | MatchingOrders  Returns single matching order header by externalCode -identifier.  |
| PaymentTerms - Returns single payment term by externalCode -ident... | PaymentTerms  Returns single payment term by externalCode -identifier.  |
| Projects v1 (deprecated) - Returns single project by externalCode -identifier... | Projects v1 (deprecated)  Returns single project by externalCode -identifier.  |
| Projects v2 - Returns single project by externalCode -identifier... | Projects v2  Returns single project by externalCode -identifier.  |
| PurchaseGoodsReceipts - Returns single purchase goods receipts by orderId ... | PurchaseGoodsReceipts Returns single purchase goods receipts by orderId - identifier.  |
| PurchaseOrders - Returns single imported purchase order by orderId ... | PurchaseOrders Note: This GET operation returns a HTTP redirect, which the API client needs to follow.  * 'Authorization' header must not be included in the redirected request (the second request after receiving a redirect).  * 'Host' header needs to be included. Returns single imported purchase order by orderId - identifier.  |
| PurchaseRequisitions - Returns single purchase requisition by externalCod... | PurchaseRequisitions Note: Updating data for existing requisitions is not supported. Posting updates will result in errors in errorFeedbacks -interface and data will not be updated in Alusta Purchase. The data will still be accepted and updated in Basware API. Because of this updates to existing purchase requisition records will result in inconsistent data between GET operations to Basware API and Alusta Purchase. Returns single purchase requisition by externalCode -identifier.  |
| RequestStatus - Returns request status responses from receiving en... | RequestStatus Note: RequestStatus functionality is not yet available for 'users' API for following target systems:  1. Network consumer (application codes 'SupplierPortal' and 'SupplierManagement')  2. Basware Access   Please see section "[Usage scenario 5: Error handling and monitoring](https://developer.basware.com/api/p2p/manual#usage5)" in Basware Purchase-to-Pay API manual for details on implementing error handling with Basware API. Returns request status responses from receiving end-system(s), based on request id.  |
| TaxCodes - Returns single tax code by externalCode identifier... | TaxCodes  Returns single tax code by externalCode identifier.  |
| Users - Returns single user by externalCode -identifier. | Users  Returns single user by externalCode -identifier.  |
| Vendors - Returns single vendor by externalCode -identifier. | Vendors  Returns single vendor by externalCode -identifier.  |


### PATCH Operation Types
| Label | Help Text |
| --- | --- |
| Accounts - Updates fields on specified account. Preserves exi... (PATCH) | Accounts Note: Basware API considers 'null' value in field(s) equivalent to the field(s) not being sent. For this reason patch method does not support setting field values to 'null'. Updates fields on specified account. Preserves existing values in fields, which were not updated.  |
| AdvancedPermissions - Updates fields on specified advanced user rights -... (PATCH) | AdvancedPermissions Note: Basware API considers 'null' value in field(s) equivalent to the field(s) not being sent. For this reason patch method does not support setting field values to 'null'. Updates fields on specified advanced user rights -record. Preserves existing values in fields, which were not updated.  |
| AdvancedValidations - Updates fields on spefified coding row validation ... (PATCH) | AdvancedValidations Updates fields on spefified coding row validation rule. Preserves existing values in fields, which were not updated.  |
| Contracts - Updates  contract entity. To do a partial update c... (PATCH) | Contracts Updates  contract entity. To do a partial update consumer needs to provide changed properties.  |
| CostCenters - Updates fields on speficied cost center. Preserves... (PATCH) | CostCenters Updates fields on speficied cost center. Preserves existing values in fields, which were not updated.  |
| ExchangeRates - Updates fields on specified exchange rate. Preserv... (PATCH) | ExchangeRates Note: Basware API considers 'null' value in field(s) equivalent to the field(s) not being sent. For this reason patch method does not support setting field values to 'null'. Updates fields on specified exchange rate. Preserves existing values in non-updated fields.  |
| GenericLists - Updates fields on specified list item. Preserves e... (PATCH) | GenericLists In Basware P2P, the following lists are available. Please note the list-specific differences in available columns and column lengths.  1) ACC_LIST_1  - ACC_LIST_10: Text1 - Text5  (250 chars), Num1 - Num3 2) ACC_LIST_11 - ACC_LIST_20: Text1 - Text5  (50 chars), Num1 - Num3 3) ACC_LIST_21 - ACC_LIST_30: Text1 - Text10 (250 chars), Num1 - Num5,  Date1 - Date5 4) ACC_LIST_31 - ACC_LIST_35: Text1 - Text25 (250 chars), Num1 - Num10, Date1 - Date10 5) INV_LIST_1  - INV_LIST_10: Text1 - Text5  (250 chars) 6) INV_LIST_11 - INV_LIST_20: Text1 - Text10 (250 chars), Num1 - Num5,  Date1 - Date5 7) CUST_RESOLVER_1 - CUST_RESOLVER_5: Text1 - Text15 (250 chars), Num1 - Num15, Date1 - Date15  Note: Basware API considers 'null' value in field(s) equivalent to the field(s) not being sent. For this reason patch method does not support setting field values to 'null'. Updates fields on specified list item. Preserves existing values in omitted fields.  |
| MatchingOrderLines - Updates fields on specified matching order line. C... (PATCH) | MatchingOrderLines Notes:  1) Concurrent PATCH operations to the same matchingOrderline can fail with error HTTP 503 due to concurrency issues. In such a case the failed request should be retried after a few seconds delay.  2) Basware API considers 'null' value in field(s) equivalent to the field(s) not being sent. For this reason patch method does not support setting field values to 'null'.  Please see section "[Usage scenario 2: Import external purchase orders for Order Matching](https://developer.basware.com/api/p2p/manual#usage2)" for details on implementing this API. Check out also the [example JSONs using a minimal feasible set of fields](https://developer.basware.com/api/p2p/templates) from the developer site. Updates fields on specified matching order line. Can be used for adding new goods receipts to the order line. Preserves existing values in fields, which were not updated.  |
| MatchingOrders - Updates fields on specified order header. Preserve... (PATCH) | MatchingOrders Note: Basware API considers 'null' value in field(s) equivalent to the field(s) not being sent. For this reason patch method does not support setting field values to 'null'. Please see section "[Usage scenario 2: Import external purchase orders for Order Matching](https://developer.basware.com/api/p2p/manual#usage2)" for details on implementing this API. Check out also the [example JSONs using a minimal feasible set of fields](https://developer.basware.com/api/p2p/templates) from the developer site. Updates fields on specified order header. Preserves existing values in fields, which were not updated.  |
| PaymentTerms - Updates on specified payment term. Preserves exist... (PATCH) | PaymentTerms Updates on specified payment term. Preserves existing values in fields, which were not updated.  |
| Projects v1 (deprecated) - Updates project. Existing data is preserved in omi... (PATCH) | Projects v1 (deprecated) Updates project. Existing data is preserved in omitted records.  |
| Projects v2 - Updates project. Existing data is preserved in omi... (PATCH) | Projects v2 Updates project. Existing data is preserved in omitted records.  |
| PurchaseOrders - Updates fields on specified matching order line. C... (PATCH) | PurchaseOrders Notes:  1) Field 'processingStatus' requires a value from API user in POST and PATCH request. If you need to import an order with more than 200 lines, this can be done by setting 'processingStatus' to 'Uncompleted' when POSTing the purchaseOrder and then adding additional lines to it using the PATCH method. ProcessingStaus: 'Uncompleted' prevents the order from being set to P2P so that more lines can be added. When all lines have been added, set 'processingStaus' = 'ReadyForImport' using PATCH method and the order will be imported to P2P.  2) Basware API considers 'null' value in field(s) equivalent to the field(s) not being sent. For this reason patch method does not support setting field values to 'null'.   Please see section "[Usage scenario 4: Import and export procurement data](https://developer.basware.com/api/p2p/manual#usage4)" for details on implementing this API. Check out also the [example JSONs using a minimal feasible set of fields](https://developer.basware.com/api/p2p/templates) from the developer site. Updates fields on specified matching order line. Can be used for adding new goods receipts to the order line. Preserves existing values in fields, which were not updated.  |
| TaxCodes - Updates on specified taxCode. Preserves existing v... (PATCH) | TaxCodes Updates on specified taxCode. Preserves existing values in fields, which were not updated.  |
| Users - Updates fields on specified user. Preserves existi... (PATCH) | Users Basware applications accessible to a user are controlled through 'users' and 'applicationGroups' APIs. Applications in a group are defined through 'applicationGroups' API. Application groups are assigned to users through the 'applicationGroups' -block in users API.  Notes:  1. You need to have at least one applicationGroup for posting users to Basware API. The applicationGroups are available in applicationGroups API.   2. Application groups are assigned to users through the 'applicationGroups' -block in users API.  3. It is possible to assign users to default application group(s). Users with no applicationGroups are automatically assigned to application groups where 'default' = true. When using this approach, you will need to have an applicationGroup with default = true.  4. The company to which a user is imported needs to be set up as an administrative site in P2P.  5. User 'loginType' needs to be 4 (user authentication through Basware Access) when the user is imported to any system besides (or in addition to) P2P. 6. When loginType = 4 (Basware Access) the email is used as user’s login account and must be unique within a tenant. 7. User groups are overwritten in P2P only when P2P admin parameter to overwrite existing user groups them is enabled. (Note: user groups are different from application groups) 8. User recipient identifiers in P2P are overridden with values received through API. When no values are specified in API, the corresponding fields are emptied in P2P.  9. User's externalCode is used as unique identifier in P2P and in Basware Access. Change to external code means a new user. The externalCode is ignored by Network Portal, which uses loginAccount as unique identifier. 10. Basware API considers 'null' value in field(s) equivalent to the field(s) not being sent. For this reason patch method does not support setting field values to 'null'.  Please see section "[Usage scenario 4: Import users](https://developer.basware.com/api/p2p/manual#usage3)" ... |
| Vendors - Updates fields on specified vendor. Preserves exis... (PATCH) | Vendors Note: Basware API considers 'null' value in field(s) equivalent to the field(s) not being sent. For this reason patch method does not support setting field values to 'null'. Updates fields on specified vendor. Preserves existing values in fields, which were not updated.  |


### POST Operation Types
| Label | Help Text |
| --- | --- |
| AccountingDocuments - Acknowledged invoices are no longer offered for ne... | AccountingDocuments Notes:  1) Updates 'processingStatus' -field on the invoice to allow filtering out the invoice on the next GET operation.  2) For an invoice which is already acknowledged (processingStatus: 'TransferInProgress' or 'PrebookInProgress'), API will return 405 'Method not allowed' if acknowledge is attempted again on the invoice.        Please see section "[Usage scenario 3: Prebook and transfer invoice to Accounting](https://developer.basware.com/api/p2p/manual#usage3)" of Basware Purchase-to-Pay API manual for details on implementing invoice transfer process with Basware API. Acknowledged invoices are no longer offered for next GET operation. Done right after GET, before transferResponse.  |
| AccountingDocuments - Marks invoice prebooking as accepted / rejected by... | AccountingDocuments Notes:  1. Prebook responses for already prebooked invoices (where processingStatus = 'Prebooked') are not allowed. 2. Saving voucher numbers and payment block -information to the invoice in P2P requires a succesful prebook response from ERP ('success' = 'true'). The responseMessage gets saved also when success = false. 3. Please do not send acknowledge and prebookResponse at the same time for the same invoice. If acknowledge is used, please wait for acknowledge to complete before sending the transferResponse. 4. Prebook failed -invoices (success = false) are processed manually in P2P.  Please see section "[Usage scenario 3: Prebook and transfer invoice to Accounting](https://developer.basware.com/api/p2p/manual#usage3)" of Basware Purchase-to-Pay API manual for details on implementing invoice transfer process with Basware API. Marks invoice prebooking as accepted / rejected by receiving system.  |
| AccountingDocuments - Marks invoice transfer as accepted / rejected by r... | AccountingDocuments Notes:  1) Transfer responses for already transferred invoices (where processingStatus = 'Transferred') are not allowed.  2) Saving voucher numbers and payment block -information to the invoice in P2P requires a succesful transfer to ERP ('success' = 'true'). The responseMessage gets saved also when success = false.  3) Please do not send acknowledge and transferResponse for at the same time for the same invoice. If acknowledge is used, please wait for acknowledge to complete before sending the transferResponse.   Please see section "[Usage scenario 3: Prebook and transfer invoice to Accounting](https://developer.basware.com/api/p2p/manual#usage3)" of Basware Purchase-to-Pay API manual for details on implementing invoice transfer process with Basware API. Marks invoice transfer as accepted / rejected by receiving system.  |
| AccountingDocuments - Updates invoice payment information, including pay... | AccountingDocuments Notes:  1) Payment response can be sent multiple times for the same invoice (for example when updating partial payments). When sending multiple payment responses to the same invoice, externalCode needs to be different on each of the paymentResponse updates.  2) Please post a paymentResponse only when data related to it on the invoice has changed (do not post same contents repeatedly).  Please see section "[Usage scenario 3: Prebook and transfer invoice to Accounting](https://developer.basware.com/api/p2p/manual#usage3)" of Basware Purchase-to-Pay API manual for details on implementing invoice transfer process with Basware API. Updates invoice payment information, including payment date.  |
| Accounts - Creates new account(s), fully overwrites previous ... | Accounts Creates new account(s), fully overwrites previous record if exists.  |
| AdvancedPermissions - Creates new advanced user rights -record(s), fully... | AdvancedPermissions Creates new advanced user rights -record(s), fully overwrites previous record if exists.  |
| AdvancedValidations - Creates new coding row validation rule(s), fully o... | AdvancedValidations Creates new coding row validation rule(s), fully overwrites previous record if exists.  |
| ApplicationGroups - Creates new Application groups, fully overwrites p... | ApplicationGroups Application groups give access to a specific set of applications for users imported through 'users' API. Each user needs to be assigned to one or more application group(s) by specifying 'applicationGroupCode' values in users API.  Notes:  1. User 'loginType' (set on users API) needs to be '4' (user authentication through Basware Access) when the user is imported to any system besides (or in addition to) P2P. 2. Support for updating and removing user access to Network applications is currently not available (planned to be released after Q1/2021).  3. POSTing to applicationGroups API returns a link to 'redistribute users' task status. This is because changes to applicationGroups will trigger changes to users assigned to those groups. When there are many users, the changes to users may take a while to complete. Accessing the link requires providing API credentials.  Please see section "[Usage scenario 4: Import users](https://developer.basware.com/api/p2p/manual#usage3)" of Basware Purchase-to-Pay API manual for details on managing users with Basware API. Check out also the [example JSONs using a minimal feasible set of fields](https://developer.basware.com/api/p2p/templates) from the developer site. Creates new Application groups, fully overwrites previous record if exists.  |
| Companies - Creates new companies, fully overwrites previous r... | Companies Note: Only consuming target system at the moment is Open Network Portal (incl. Vendor manager). Companies cannot be imported to P2P through this API.   Please see section "[Usage scenario 6: Import companies](https://developer.basware.com/api/p2p/manual#usage6)" of Basware Purchase-to-Pay API manual for details on implementing this API. Creates new companies, fully overwrites previous record if exists.  |
| Contracts - Creates new contract entity, overwrites previous i... | Contracts Creates new contract entity, overwrites previous if exists.  |
| CostCenters - Creates new cost center(s), fully overwrites previ... | CostCenters Creates new cost center(s), fully overwrites previous record if exists.  |
| ExchangeRates - Creates new exchange rate(s), fully overwrites pre... | ExchangeRates Notes: 1) Please provide exchange rates to 1) the currency used by entire organization as well as 2) to currencies used by individual companies.  2) Exchange rates are inherited in P2P from top level organizations down if company-specific rates are not provided.   3) Multiplying 'from' currency amount by rate needs to give amount in 'to' -currency. Creates new exchange rate(s), fully overwrites previous record if exists.  |
| ExportedPurchaseOrders - Acknowledged orders are no longer returned for nex... | ExportedPurchaseOrders Notes: 1. Updates 'processingStatus' -field on the document to allow filtering out the document on the next GET operation 2. For a document which is already acknowledged (processingStatus: 'Exported'), API will return 405 'Method not allowed' if acknowledge is attempted again on the document.  Please see section "[Usage scenario 4: Import and export procurement data](https://developer.basware.com/api/p2p/manual#usage4)" for details on implementing this API. Acknowledged orders are no longer returned for next GET operation.  |
| ExportedPurchaseRequisitions - Acknowledged requisitions are no longer returned f... | ExportedPurchaseRequisitions Notes: 1. Updates 'processingStatus' -field on the document to allow filtering out the document on the next GET operation 2. For a document which is already acknowledged (processingStatus: 'Exported'), API will return 405 'Method not allowed' if acknowledge is attempted again on the document.  Please see section "[Usage scenario 4: Import and export procurement data](https://developer.basware.com/api/p2p/manual#usage4)" for details on implementing this API. Acknowledged requisitions are no longer returned for next GET operation.  |
| GenericLists - Creates new list item(s), fully overwrites previou... (POST) | GenericLists In Basware P2P, the following lists are available. Please note the list-specific differences in available columns and column lengths.  1) ACC_LIST_1  - ACC_LIST_10: Text1 - Text5  (250 chars), Num1 - Num3 2) ACC_LIST_11 - ACC_LIST_20: Text1 - Text5  (50 chars), Num1 - Num3 3) ACC_LIST_21 - ACC_LIST_30: Text1 - Text10 (250 chars), Num1 - Num5,  Date1 - Date5 4) ACC_LIST_31 - ACC_LIST_35: Text1 - Text25 (250 chars), Num1 - Num10, Date1 - Date10 5) INV_LIST_1  - INV_LIST_10: Text1 - Text5  (250 chars) 6) INV_LIST_11 - INV_LIST_20: Text1 - Text10 (250 chars), Num1 - Num5,  Date1 - Date5 7) CUST_RESOLVER_1 - CUST_RESOLVER_5: Text1 - Text15 (250 chars), Num1 - Num15, Date1 - Date15 Creates new list item(s), fully overwrites previous record if exists.  |
| MatchingOrderLines - Creates new matching order line(s), fully overwrit... | MatchingOrderLines Notes: 1) Please make sure you have POSTed the order header through matchingOrders API before posting lines to the same order to matchingOrderLines API. It is enough to POST the order header only once (unless updates are neede to the order header).  2) Up to 100 order lines can be posted in one request. Order lines for an order containing more than 100 lines need to be posted in several requets. 3) A single POST operation is saved to P2P in a single transaction. Multiple POST operations will be saved each in their own transactions. 4) Invoiced quantities and sums need to be imported before matching has been performed against the order in P2P.  5) Field 'isDeleted' needs to have a value other than null when bestfit matching is used on the order in P2P.   Please see section "[Usage scenario 2: Import external purchase orders for Order Matching](https://developer.basware.com/api/p2p/manual#usage2)" for details on implementing this API. Check out also the [example JSONs using a minimal feasible set of fields](https://developer.basware.com/api/p2p/templates) from the developer site. Creates new matching order line(s), fully overwrites previous record if exists.  |
| MatchingOrders - Creates new order header(s), overwrites previous r... | MatchingOrders Notes:  1) Please make sure you have POSTed the order header to matchingOrders API before posting lines to the same order through matchingOrderLines API. It is enough to POST the order header only once (unless updates are needed to the order header). The order becomes available in P2P after order lines lines have been added. 2) Vendor(s) used by the order need to be in P2P before importing the order.  3) The following fields uniquely identify an order in P2P: Order number, source system, company code and organization element code.  Please see section "[Usage scenario 2: Import external purchase orders for Order Matching](https://developer.basware.com/api/p2p/manual#usage2)" for details on implementing this API. Check out also the [example JSONs using a minimal feasible set of fields](https://developer.basware.com/api/p2p/templates) from the developer site. Creates new order header(s), overwrites previous record if exists.  |
| PaymentTerms - Creates new paymentTerm entity, fully overwrites p... | PaymentTerms Note: Please post payment term(s) before posting vendor(s) using the payment term(s). Payment term(s) should be in P2P before posting the vendor(s). Creates new paymentTerm entity, fully overwrites previous record if exists.  |
| Projects v1 (deprecated) - Creates new projects, fully overwrites previous re... | Projects v1 (deprecated) Notes:  1. This /v1/projects API is deprecated and should no longer be used in new implementations. Existing implementations will keep functioning until further notice. Basware will give at least 12 month notice before decommissioning this API. 2. Data from /v1/projects API is imported to P2P target table ADM_PROJECTS. Creates new projects, fully overwrites previous record if exists.  |
| Projects v2 - Creates new projects, fully overwrites previous re... | Projects v2 Notes:  1. This /v2/projects API replaces the deprecated /v1/projects API and should be used in new implementations.  2. Data from /v2/projects API is imported to P2P target table GDM_CUSTOMER_PROJECTS. Creates new projects, fully overwrites previous record if exists. Note: v2 api requires P2P 21.5  |
| PurchaseGoodsReceipts - Creates new purchase goods receipts, fully overwri... | PurchaseGoodsReceipts Specifies a single goods receipt or reverse goods receipt document concerning one purchase order. Goods receipt document can contain single receiving for some or all PO lines. So you can receive all order lines, some order lines or some order lines partly. Reversing has reference to GR document and line to reverse with negative quanties. For receiving or reversing to succeed all lines need to exist in order having sufficient quantity to receive. They cannot be closed, cancelled or matched. Several goods receipt documents can be sent for the same purchase order in one JSON. Suggested practice is to group lines received at one time in one location for the PO into a single goods receipt document. Notes: 1. All the lines received on single goods receipt document must be ordered to the same delivery location as stated in order. Each delivery location requires a separate goods receipt document. 2. A single goods receipt document is processed fully when imported to P2P. There is no partial processing: All lines are either accepted or the complete goods receipt document is rejected. 3. P2P supports reversing GR documents. When sending a GR reversal, all lines on the GR document must be reversal lines. Normal goods receipt lines and reversal lines cannot be mixed on the same goods reeipt document.   Please see section "[Usage scenario 4: Import and export procurement data](https://developer.basware.com/api/p2p/manual#usage4)" for details on implementing this API. Check out also the [example JSONs using a minimal feasible set of fields](https://developer.basware.com/api/p2p/templates) from the developer site. Creates new purchase goods receipts, fully overwrites previous record if exists.  |
| PurchaseOrders - Creates new purchase order(s), fully overwrites pr... | PurchaseOrders Notes:  1. Any referenced vendors, users, payment terms, purchasing categories, quantity units, etc. need to exist in P2P when the purchase order is posted. 2. Orders are technically handled as auto-approved requisitions resulting in a corresponding order being created. If you don't find the order in P2P UI, try looking for it as a requisition. It might have invalid data (see above) or the requisition approval process may be configured in a way to block automatic order creation. 3. Field 'processingStatus' requires a value from API user in POST and PATCH request. If you need to import an order with more than 200 lines, this can be done by setting 'processingStatus' to 'Uncompleted' when POSTing the purchaseOrder and then adding additional lines to it using the PATCH method. ProcessingStaus: 'Uncompleted' prevents the order from being set to P2P so that more lines can be added. When all lines have been added, set 'processingStaus' = 'ReadyForImport' using PATCH method and the order will be imported to P2P.    Please see section "[Usage scenario 4: Import and export procurement data](https://developer.basware.com/api/p2p/manual#usage4)" for details on implementing this API. Check out also the [example JSONs using a minimal feasible set of fields](https://developer.basware.com/api/p2p/templates) from the developer site. Creates new purchase order(s), fully overwrites previous record if exists. P2P Purchase does not allow updates to purchase orders after import.  |
| PurchaseRequisitions - Creates new purchase requisition(s). P2P Purchase ... | PurchaseRequisitions  Notes:   1. Any referenced vendors, users, etc. need to exist in P2P when the purchase requisition is imported.   2. Updating data for existing requisitions is not supported. Posting updates will result in errors in errorFeedbacks -interface and data will not be updated in Alusta Purchase. The data will still be accepted and updated in Basware API. Because of this updates to existing purchase requisition records will result in inconsistent data between GET operations to Basware API and Alusta Purchase.   Please see section "[Usage scenario 4: Import and export procurement data](https://developer.basware.com/api/p2p/manual#usage4)" for details on implementing this API. Check out also the [example JSONs using a minimal feasible set of fields](https://developer.basware.com/api/p2p/templates) from the developer site. Creates new purchase requisition(s). P2P Purchase does not allow updates to purchase requisitions after import.  |
| RequestStatus - Acknowledged request status responses are no longe... | RequestStatus Acknowledging will status from 'Success' or 'Error' to indicate the request has already been acknowledged. Please see section "[Usage scenario 5: Error handling and monitoring](https://developer.basware.com/api/p2p/manual#usage5)" in Basware Purchase-to-Pay API manual for details on implementing error handling with Basware API. Acknowledged request status responses are no longer offered for next GET operation.  |
| TaxCodes - Creates new tax code(s), fully overwrites previous... | TaxCodes Creates new tax code(s), fully overwrites previous record if exists.  |
| Users - Creates new user(s), fully overwrites previous rec... | Users Basware applications accessible to a user are controlled through 'users' and 'applicationGroups' APIs. Applications in a group are defined through 'applicationGroups' API. Application groups are assigned to users through the 'applicationGroups' -block in users API.  Notes:  1. You need to have at least one applicationGroup for posting users to Basware API. The applicationGroups are available in applicationGroups API.   2. Application groups are assigned to users through the 'applicationGroups' -block in users API.  3. It is possible to assign users to default application group(s). Users with no applicationGroups are automatically assigned to application groups where 'default' = true. When using this approach, you will need to have an applicationGroup with default = true.  4. The company to which a user is imported needs to be set up as an administrative site in P2P.  5. User 'loginType' needs to be 4 (user authentication through Basware Access) when the user is imported to any system besides (or in addition to) P2P. 6. When loginType = 4 (Basware Access) the email is used as user’s login account and must be unique within a tenant. 7. User groups are overwritten in P2P only when P2P admin parameter to overwrite existing user groups them is enabled. (Note: user groups are different from application groups) 8. User recipient identifiers in P2P are overridden with values received through API. When no values are specified in API, the corresponding fields are emptied in P2P.  9. User's externalCode is used as unique identifier in P2P and in Basware Access. Change to external code means a new user. The externalCode is ignored by Network Portal, which uses loginAccount as unique identifier. 10. If costCenterCode is empty, you may get error "No Cost Center information found for user" from P2P but user will still be imported to P2P. That message can be regarded as a warning. 11. Fields containing data on existing user records cannot be cleared in P2P through users API... |
| Vendors - Creates new vendor(s), overwrites previous record ... | Vendors Notes:  1) Payment terms used by the vendor(s) need to exist in P2P. Payment terms can be imported through paymentTerms API or maintained manually in P2P.   2) Currencies used by the vendor(s) need to be active in P2P. Active currencies are maintained manually in P2P.   3) Vendors used with P2P Purchase need to have 'orderingFormat' value different from 'none'. 'OrderingFormat' field can be specified through vendors API (in orderingDetails -block), or it can be manually maintained in P2P.  4) Posting vendors to Bsaware Portal requires company structure to be imported to portal. The company structure can also be backfilled to portal from P2P.  5) Up to 200 vendor records can be sent in one POST vendors request.  Check out the [example JSONs using a minimal feasible set of fields](https://developer.basware.com/api/p2p/templates) from the developer site. Creates new vendor(s), overwrites previous record if exists.  |


### PUT Operation Types
| Label | Help Text |
| --- | --- |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| AccountingDocuments - Returns invoices approved for transfer to accounti... | AccountingDocuments Note: This GET operation returns a HTTP redirect, which the API client needs to follow.  * 'Authorization' header must not be included in the redirected request (the second request after receiving a redirect).  * 'Host' header needs to be included.  Please see section "[Usage scenario 3: Prebook and transfer invoice to Accounting](https://developer.basware.com/api/p2p/manual#usage3)" of Basware Purchase-to-Pay API manual for details on implementing invoice transfer process with Basware API. Returns invoices approved for transfer to accounting.  |
| Accounts - Returns accounts posted to Basware API. | Accounts  Returns accounts posted to Basware API.  |
| AdvancedPermissions - Returns advanced user rights -records posted to Ba... | AdvancedPermissions  Returns advanced user rights -records posted to Basware API.  |
| AdvancedValidations - Returns coding row validation rules posted to Basw... | AdvancedValidations  Returns coding row validation rules posted to Basware API.  |
| ApplicationGroups - Returns application groups. | ApplicationGroups Application groups give access to a specific set of applications for users imported through 'users' API. Each user needs to be assigned to one or more application group(s) by specifying 'applicationGroupCode' values in users API. Returns application groups.  |
| Companies - Returns companies posted to Basware API. | Companies  Returns companies posted to Basware API.  |
| Contracts - Returns contract entities | Contracts  Returns contract entities  |
| CostCenters - Returns cost centers posted to Basware API. | CostCenters  Returns cost centers posted to Basware API.  |
| ErrorFeedbacks - Returns error messages from receiving end-system(s... | ErrorFeedbacks If errors occur after Basware API has validated the data schema, these will be returned from target systems asynchronously in this interface.  Please see section "[Usage scenario 5: Error handling and monitoring](https://developer.basware.com/api/p2p/manual#usage5)" in Basware Purchase-to-Pay API manual for details on implementing error handling with Basware API. Returns error messages from receiving end-system(s), based on error time stamp.  |
| ExchangeRates - Returns exchange rates posted to Basware API. | ExchangeRates  Returns exchange rates posted to Basware API.  |
| ExportedPurchaseOrders - Returns purchase orders exported from Basware P2P. | ExportedPurchaseOrders Note: This GET operation returns a HTTP redirect, which the API client needs to follow.  * 'Authorization' header must not be included in the redirected request (the second request after receiving a redirect).  * 'Host' header needs to be included.  Please see section "[Usage scenario 4: Import and export procurement data](https://developer.basware.com/api/p2p/manual#usage4)" for details on implementing this API. Returns purchase orders exported from Basware P2P.  |
| ExportedPurchaseRequisitions - Returns purchase requisitions exported fom Basware... | ExportedPurchaseRequisitions Notes: 1. A purchase requisition is exported from P2P when the requisition is approved, rejected or canceled. Requisitions are not exported while they are in approval process.  2. Note: This GET operation returns a HTTP redirect, which the API client needs to follow.  * 'Authorization' header must not be included in the redirected request (the second request after receiving a redirect).  * 'Host' header needs to be included.  Please see section "[Usage scenario 4: Import and export procurement data](https://developer.basware.com/api/p2p/manual#usage4)" for details on implementing this API. Returns purchase requisitions exported fom Basware P2P.  |
| MatchingOrderLines - Returns matching order lines posted to Basware API... | MatchingOrderLines  Returns matching order lines posted to Basware API.  |
| MatchingOrders - Returns matching order headers posted to Basware A... | MatchingOrders  Returns matching order headers posted to Basware API.  |
| PaymentTerms - Returns payment terms posted to Basware API. | PaymentTerms  Returns payment terms posted to Basware API.  |
| Projects v1 (deprecated) - Returns projects posted to Basware API. | Projects v1 (deprecated)  Returns projects posted to Basware API.  |
| Projects v2 - Returns projects posted to Basware API. | Projects v2  Returns projects posted to Basware API.  |
| PurchaseGoodsReceipts - Returns purchase goods receipts. | PurchaseGoodsReceipts  Returns purchase goods receipts.  |
| PurchaseOrders - Returns imported purchase orders. | PurchaseOrders Note: This GET operation returns a HTTP redirect, which the API client needs to follow.  * 'Authorization' header must not be included in the redirected request (the second request after receiving a redirect).  * 'Host' header needs to be included. Returns imported purchase orders.  |
| PurchaseRequisitions - Returns purchase requisitions posted to Basware AP... | PurchaseRequisitions Note: Updating data for existing requisitions is not supported. Posting updates will result in errors in errorFeedbacks -interface and data will not be updated in Alusta Purchase. The data will still be accepted and updated in Basware API. Because of this updates to existing purchase requisition records will result in inconsistent data between GET operations to Basware API and Alusta Purchase. Returns purchase requisitions posted to Basware API.  |
| RequestStatus - Returns request status responses from receiving en... | RequestStatus Note: RequestStatus functionality is not yet available for 'users' API for following target systems:  1. Network consumer (application codes 'SupplierPortal' and 'SupplierManagement')  2. Basware Access   Please see section "[Usage scenario 5: Error handling and monitoring](https://developer.basware.com/api/p2p/manual#usage5)" in Basware Purchase-to-Pay API manual for details on implementing error handling with Basware API. Returns request status responses from receiving end-system(s), based on status and time stamp.  |
| TaxCodes - Returns tax codes posted to Basware API. | TaxCodes  Returns tax codes posted to Basware API.  |
| Users - Returns users posted to Basware API. | Users  Returns users posted to Basware API.  |
| Vendors - Returns vendors posted to Basware API. | Vendors  Returns vendors posted to Basware API.  |


