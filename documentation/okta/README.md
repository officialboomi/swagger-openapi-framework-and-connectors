# Okta Connector
The Okta Connector provides access to the Okta REST API for managing Okta objects. 

Many Okta objects support pagination and those will be classified as Query operations. 

Some Get APIs return an array of objects but do not support pagination. Those will be treated as Get operations and the results can be split into separate documents using a Data Process shape if required.

For more information refer to: https://developer.okta.com/docs/reference/api/roles/ 
	

# Connection Tab
## Connection Fields


#### URL

The URL for the Service service

**Type** - string



#### AuthenticationType

Choose the type of Authentication to use. For more information refer to: https://developer.okta.com/docs/guides/create-an-api-token/-/overview/
		

**Type** - string

**Default Value** - CUSTOM

##### Allowed Values

 * Okta API Token (SSWS)
 * OAuth 2.0 Authorization Code


#### Okta API Token (SSWS)

Enter the Okta API Token in the form SSWS 00QCjAl4MlV-WPXM...0HmjFx-vbGua

**Type** - password



#### OAuth 2.0 Authorization Code

OAuth 2.0 Authorization Code authentication is available for select end points with specific scope values configured. For more information refer to: https://developer.okta.com/docs/guides/implement-oauth-for-okta/create-oauth-app/

**Type** - oauth

# Operation Tab


## CREATE/EXECUTE


## UPDATE


## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. This can be used for entering endpoint specific parameters such as 'expand=' or 'q='. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE


## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

The connector does not support field selection. All fields will be returned by default.


#### Filters

The query filter supports one level of nesting, where the top level groupings will be AND'ed together and the nested expression groups will be OR'ed together.

Example:
((foo lessThan 30) OR (baz lessThan 42)) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than or Equal  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than or Equal  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Has Value  **helpText NOT SET IN DESCRIPTOR FILE**

 * Starts With  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters (Get,Query)** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| Application: Activate Application | Activates an inactive application. |
| Application: Add Application | Adds a new application to your Okta organization. |
| Application: Assign User to Application for SSO & Provisioning | Assigns an user to an application with [credentials](#application-user-credentials-object) and an app-specific [profile](#application-user-profile-object). Profile mappings defined for the application are first applied before applying any profile properties specified in the request. |
| Application: Clone Application Key Credential | Clones a X.509 certificate for an application key credential from a source application to target application. |
| Application: Deactivate Application | Deactivates an active application. |
| Application: Generate Certificate Signing Request for Application | Generates a new key pair and returns the Certificate Signing Request for it. |
| Application: generateApplicationKey | Generates a new X.509 certificate for an application key credential |
| Application: grantConsentToScope | Grants consent for the application to request an OAuth 2.0 Okta scope |
| Application: null |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: activateAuthorizationServer |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: createAuthorizationServer |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: createAuthorizationServerPolicy |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: createAuthorizationServerPolicyRule | Creates a policy rule for the specified Custom Authorization Server and Policy. |
| AuthorizationServer: createOAuth2Claim |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: createOAuth2Scope |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: deactivateAuthorizationServer |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: rotateAuthorizationServerKeys |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EventHook: activateEventHook |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EventHook: createEventHook |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EventHook: deactivateEventHook |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EventHook: verifyEventHook |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Group: Activate a group Rule | Activates a specific group rule by id from your organization |
| Group: Add Group | Adds a new group with `OKTA_GROUP` type to your organization. |
| Group: assignRoleToGroup | Assigns a Role to a Group |
| Group: Create Group Rule | Creates a group rule to dynamically add users to the specified group if they match the condition |
| Group: Deactivate a group Rule | Deactivates a specific group rule by id from your organization |
| IdentityProvider: Activate Identity Provider | Activates an inactive IdP. |
| IdentityProvider: Add Identity Provider | Adds a new IdP to your organization. |
| IdentityProvider: Add X.509 Certificate Public Key | Adds a new X.509 certificate credential to the IdP key store. |
| IdentityProvider: Clone Signing Key Credential for IdP | Clones a X.509 certificate for an IdP signing key credential from a source IdP to target IdP |
| IdentityProvider: Deactivate Identity Provider | Deactivates an active IdP. |
| IdentityProvider: Generate Certificate Signing Request for IdP | Generates a new key pair and returns a Certificate Signing Request for it. |
| IdentityProvider: Generate New IdP Signing Key Credential | Generates a new X.509 certificate for an IdP signing key credential to be used for signing assertions sent to the IdP |
| IdentityProvider: null | Update the Certificate Signing Request with a signed X.509 certificate and add it into the signing key credentials for the IdP. |
| InlineHook: activateInlineHook | Activates the Inline Hook matching the provided id |
| InlineHook: createInlineHook |  **helpText NOT SET IN DESCRIPTOR FILE** |
| InlineHook: deactivateInlineHook | Deactivates the Inline Hook matching the provided id |
| InlineHook: executeInlineHook | Executes the Inline Hook matching the provided inlineHookId using the request body as the input. This will send the provided data through the Channel and return a response if it matches the correct data contract. This execution endpoint should only be used for testing purposes. |
| LinkedObject: addLinkedObjectDefinition |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Policy: activatePolicy | Activates a policy. |
| Policy: activatePolicyRule | Activates a policy rule. |
| Policy: createPolicy | Creates a policy. |
| Policy: createPolicyRule | Creates a policy rule. |
| Policy: deactivatePolicy | Deactivates a policy. |
| Policy: deactivatePolicyRule | Deactivates a policy rule. |
| Session: Create Session with Session Token | Creates a new session for a user with a valid session token. Use this API if, for example, you want to set the session cookie yourself instead of allowing Okta to set it, or want to hold the session ID in order to delete a session via the API instead of visiting the logout URL. |
| Session: Refresh Session |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Template: Add SMS Template | Adds a new custom SMS template to your organization. |
| TrustedOrigin: activateOrigin |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TrustedOrigin: createOrigin |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TrustedOrigin: deactivateOrigin |  **helpText NOT SET IN DESCRIPTOR FILE** |
| User: Activate User | Activates a user.  This operation can only be performed on users with a `STAGED` status.  Activation of a user is an asynchronous operation. The user will have the `transitioningToStatus` property with a value of `ACTIVE` during activation to indicate that the user hasn't completed the asynchronous operation.  The user will have a status of `ACTIVE` when the activation process is complete. |
| User: assignRoleToUser | Assigns a role to a user. |
| User: Change Password | Changes a user's password by validating the user's current password. This operation can only be performed on users in `STAGED`, `ACTIVE`, `PASSWORD_EXPIRED`, or `RECOVERY` status that have a valid password credential |
| User: Change Recovery Question | Changes a user's recovery question & answer credential by validating the user's current password.  This operation can only be performed on users in **STAGED**, **ACTIVE** or **RECOVERY** `status` that have a valid password credential |
| User: Create User | Creates a new user in your Okta organization with or without credentials. |
| User: Deactivate User | Deactivates a user.  This operation can only be performed on users that do not have a `DEPROVISIONED` status.  Deactivation of a user is an asynchronous operation.  The user will have the `transitioningToStatus` property with a value of `DEPROVISIONED` during deactivation to indicate that the user hasn't completed the asynchronous operation.  The user will have a status of `DEPROVISIONED` when the deactivation process is complete. |
| User: Expire Password | This operation transitions the user to the status of `PASSWORD_EXPIRED` and the user's password is reset to a temporary password that is returned. |
| User: Expire Password | This operation transitions the user to the status of `PASSWORD_EXPIRED` so that the user is required to change their password at their next login. |
| User: Forgot Password |  **helpText NOT SET IN DESCRIPTOR FILE** |
| User: Reactivate User | Reactivates a user.  This operation can only be performed on users with a `PROVISIONED` status.  This operation restarts the activation workflow if for some reason the user activation was not completed when using the activationToken from [Activate User](#activate-user). |
| User: Reset Factors | This operation resets all factors for the specified user. All MFA factor enrollments returned to the unenrolled state. The user's status remains ACTIVE. This link is present only if the user is currently enrolled in one or more MFA factors. |
| User: Reset Password | Generates a one-time token (OTT) that can be used to reset a user's password.  The OTT link can be automatically emailed to the user or returned to the API caller and distributed using a custom flow. |
| User: Suspend User | Suspends a user.  This operation can only be performed on users with an `ACTIVE` status.  The user will have a status of `SUSPENDED` when the process is complete. |
| User: Unlock User | Unlocks a user with a `LOCKED_OUT` status and returns them to `ACTIVE` status.  Users will be able to login with their current password. |
| User: Unsuspend User | Unsuspends a user and returns them to the `ACTIVE` state.  This operation can only be performed on users that have a `SUSPENDED` status. |
| UserFactor: Activate Factor | The `sms` and `token:software:totp` factor types require activation to complete the enrollment process. |
| UserFactor: Enroll Factor | Enrolls a user with a supported factor. |
| UserFactor: Verify MFA Factor | Verifies an OTP for a `token` or `token:hardware` factor |
| UserSchema: Partial updates on the User Profile properties of the Application User Schema. | Partial updates on the User Profile properties of the Application User Schema. |
| UserType: createUserType | Creates a new User Type. A default User Type is automatically created along with your org, and you may add another 9 User Types for a maximum of 10. |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Application: Delete Application | Removes an inactive application. |
| Application: Remove Group from Application | Removes a group assignment from an application. |
| Application: Remove User from Application | Removes an assignment for a user from an application. |
| Application: revokeCsrFromApplication |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Application: revokeOAuth2TokenForApplication | Revokes the specified token for the specified application |
| Application: revokeOAuth2TokensForApplication | Revokes all tokens for the specified application |
| Application: revokeScopeConsentGrant | Revokes permission for the application to request the given scope |
| AuthorizationServer: deleteAuthorizationServer |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: deleteAuthorizationServerPolicy |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: deleteAuthorizationServerPolicyRule | Deletes a Policy Rule defined in the specified Custom Authorization Server and Policy. |
| AuthorizationServer: deleteOAuth2Claim |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: deleteOAuth2Scope |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: revokeRefreshTokenForAuthorizationServerAndClient |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: revokeRefreshTokensForAuthorizationServerAndClient |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EventHook: deleteEventHook |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Group: Delete a group Rule | Removes a specific group rule by id from your organization |
| Group: Remove App Instance Target to App Administrator Role given to a Group | Remove App Instance Target to App Administrator Role given to a Group |
| Group: Remove Group | Removes a group with `OKTA_GROUP` type from your organization. |
| Group: Remove User from Group | Removes a user from a group with 'OKTA_GROUP' type. |
| Group: removeApplicationTargetFromApplicationAdministratorRoleGivenToGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Group: removeGroupTargetFromGroupAdministratorRoleGivenToGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Group: removeRoleFromGroup | Unassigns a Role from a Group |
| IdentityProvider: Delete Identity Provider | Removes an IdP from your organization. |
| IdentityProvider: Delete Key | Deletes a specific IdP Key Credential by `kid` if it is not currently being used by an Active or Inactive IdP. |
| IdentityProvider: revokeCsrForIdentityProvider | Revoke a Certificate Signing Request and delete the key pair from the IdP |
| IdentityProvider: Unlink User from IdP | Removes the link between the Okta user and the IdP user. |
| InlineHook: deleteInlineHook | Deletes the Inline Hook matching the provided id. Once deleted, the Inline Hook is unrecoverable. As a safety precaution, only Inline Hooks with a status of INACTIVE are eligible for deletion. |
| LinkedObject: deleteLinkedObjectDefinition |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Policy: deletePolicy | Removes a policy. |
| Policy: deletePolicyRule | Removes a policy rule. |
| Session: Close Session |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Template: Remove SMS Template | Removes an SMS template. |
| TrustedOrigin: deleteOrigin |  **helpText NOT SET IN DESCRIPTOR FILE** |
| User: clearUserSessions | Removes all active identity provider sessions. This forces the user to authenticate on the next operation. Optionally revokes OpenID Connect and OAuth refresh and access tokens issued to the user. |
| User: Delete User | Deletes a user permanently.  This operation can only be performed on users that have a `DEPROVISIONED` status.  **This action cannot be recovered!** |
| User: Remove App Instance Target to App Administrator Role given to a User | Remove App Instance Target to App Administrator Role given to a User |
| User: removeApplicationTargetFromApplicationAdministratorRoleForUser |  **helpText NOT SET IN DESCRIPTOR FILE** |
| User: removeGroupTargetFromRole |  **helpText NOT SET IN DESCRIPTOR FILE** |
| User: removeLinkedObjectForUser | Delete linked objects for a user, relationshipName can be ONLY a primary relationship name |
| User: removeRoleFromUser | Unassigns a role from a user. |
| User: revokeGrantsForUserAndClient | Revokes all grants for the specified user and client |
| User: revokeTokenForUserAndClient | Revokes the specified refresh token. |
| User: revokeTokensForUserAndClient | Revokes all refresh tokens issued for the specified User and Client. |
| User: revokeUserGrant | Revokes one grant for a specified user |
| User: revokeUserGrants | Revokes all grants for a specified user |
| UserFactor: deleteFactor | Unenrolls an existing factor for the specified user, allowing the user to enroll a new factor. |
| UserType: deleteUserType | Deletes a User Type permanently. This operation is not permitted for the default type, nor for any User Type that has existing users |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Application: Get Application | Fetches an application from your Okta organization by `id`. |
| Application: Get Assigned Group for Application | Fetches an application group assignment |
| Application: Get Assigned User for Application | Fetches a specific user assignment for application by `id`. |
| Application: Get Key Credential for Application | Gets a specific application key credential by kid |
| Application: getCsrForApplication |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Application: getOAuth2TokenForApplication | Gets a token for the specified application |
| Application: getScopeConsentGrant | Fetches a single scope consent grant for the application |
| AuthorizationServer: getAuthorizationServer |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: getAuthorizationServerPolicy |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: getAuthorizationServerPolicyRule | Returns a Policy Rule by ID that is defined in the specified Custom Authorization Server and Policy. |
| AuthorizationServer: getOAuth2Claim |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: getOAuth2Scope |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: getRefreshTokenForAuthorizationServerAndClient |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EventHook: getEventHook |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Feature: getFeature |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Group: Get Group Rule | Fetches a specific group rule by id from your organization |
| Group: getRole |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Group: List Group Rules | Lists all group rules for your organization. |
| IdentityProvider: Get Identity Provider | Fetches an IdP by `id`. |
| IdentityProvider: Get Key | Gets a specific IdP Key Credential by `kid` |
| IdentityProvider: Get Signing Key Credential for IdP | Gets a specific IdP Key Credential by `kid` |
| IdentityProvider: getCsrForIdentityProvider | Gets a specific Certificate Signing Request model by id |
| IdentityProvider: getIdentityProviderApplicationUser | Fetches a linked IdP user by ID |
| InlineHook: getInlineHook | Gets an inline hook by ID |
| LinkedObject: getLinkedObjectDefinition |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Policy: getPolicy | Gets a policy. |
| Policy: getPolicyRule | Gets a policy rule. |
| Session: getSession | Get details about a session. |
| Template: Get SMS Template | Fetches a specific template by `id` |
| TrustedOrigin: getOrigin |  **helpText NOT SET IN DESCRIPTOR FILE** |
| User: Get User | Fetches a user from your Okta organization. |
| User: getUserGrant | Gets a grant for the specified user |
| UserFactor: getFactor | Fetches a factor for the specified user |
| UserFactor: getFactorTransactionStatus | Polls factors verification transaction for status. |
| UserSchema: Fetches the schema for a Schema Id. | Fetches the schema for a Schema Id. |
| UserSchema: Fetches the Schema for an App User | Fetches the Schema for an App User |
| UserType: getUserType | Fetches a User Type by ID. The special identifier `default` may be used to fetch the default User Type. |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Application: List Applications | Enumerates apps added to your organization with pagination. A subset of apps can be returned that match a supported filter expression or query.  Filters: q= filter=   Supports expand |
| Application: List Certificate Signing Requests for Application (Not Paginated) | Enumerates Certificate Signing Requests for an application (Not Paginated) |
| Application: List Groups Assigned to Application | Enumerates group assignments for an application.  Filters: q=   Supports expand |
| Application: List Key Credentials for Application (Not Paginated) | Enumerates key credentials for an application (Not Paginated) |
| Application: List Users Assigned to Application | Enumerates all assigned [application users](#application-user-model) for an application.  Filters: q= filter=   Supports expand |
| Application: listOAuth2TokensForApplication | Lists all tokens for the application  Supports expand |
| Application: listScopeConsentGrants (Not Paginated) | Lists all scope consent grants for the application (Not Paginated)  Supports expand |
| AuthorizationServer: listAuthorizationServerKeys (Not Paginated) |  (Not Paginated) |
| AuthorizationServer: listAuthorizationServerPolicies (Not Paginated) |  (Not Paginated) |
| AuthorizationServer: listAuthorizationServerPolicyRules (Not Paginated) | Enumerates all policy rules for the specified Custom Authorization Server and Policy. (Not Paginated) |
| AuthorizationServer: listAuthorizationServers |   Filters: q=  |
| AuthorizationServer: listOAuth2Claims (Not Paginated) |  (Not Paginated) |
| AuthorizationServer: listOAuth2ClientsForAuthorizationServer (Not Paginated) |  (Not Paginated) |
| AuthorizationServer: listOAuth2Scopes (Not Paginated) |  (Not Paginated)  Filters: q= filter=  |
| AuthorizationServer: listRefreshTokensForAuthorizationServerAndClient |   Supports expand |
| EventHook: listEventHooks (Not Paginated) |  (Not Paginated) |
| Feature: listFeatureDependencies (Not Paginated) |  (Not Paginated) |
| Feature: listFeatureDependents (Not Paginated) |  (Not Paginated) |
| Feature: listFeatures (Not Paginated) |  (Not Paginated) |
| Group: List Assigned Applications | Enumerates all applications that are assigned to a group. |
| Group: List Group Members | Enumerates all users that are a member of a group. |
| Group: List Group Rules | Lists all group rules for your organization.  Filters: search=   Supports expand |
| Group: List Groups | Enumerates groups in your organization with pagination. A subset of groups can be returned that match a supported filter expression or query.  Filters: q= filter=   Supports expand |
| Group: listApplicationTargetsForApplicationAdministratorRoleForGroup | Lists all App targets for an `APP_ADMIN` Role assigned to a Group. This methods return list may include full Applications or Instances. The response for an instance will have an `ID` value, while Application will not have an ID. |
| Group: listGroupAssignedRoles (Not Paginated) |  (Not Paginated)  Supports expand |
| Group: listGroupTargetsForGroupRole |  **helpText NOT SET IN DESCRIPTOR FILE** |
| IdentityProvider: Find Users (Not Paginated) | Find all the users linked to an identity provider (Not Paginated) |
| IdentityProvider: List Certificate Signing Requests for IdP (Not Paginated) | Enumerates Certificate Signing Requests for an IdP (Not Paginated) |
| IdentityProvider: List Identity Providers | Enumerates IdPs in your organization with pagination. A subset of IdPs can be returned that match a supported filter expression or query.  Filters: q=  |
| IdentityProvider: List Keys | Enumerates IdP key credentials. |
| IdentityProvider: List Signing Key Credentials for IdP (Not Paginated) | Enumerates signing key credentials for an IdP (Not Paginated) |
| IdentityProvider: Social Authentication Token Operation (Not Paginated) | Fetches the tokens minted by the Social Authentication Provider when the user authenticates with Okta via Social Auth. (Not Paginated) |
| InlineHook: listInlineHooks (Not Paginated) |  (Not Paginated) |
| LinkedObject: listLinkedObjectDefinitions (Not Paginated) |  (Not Paginated) |
| Log: Fetch a list of events from your Okta organization system log. | The Okta System Log API provides read access to your organization’s system log. This API provides more functionality than the Events API  Filters: q= filter=  |
| Policy: listPolicies (Not Paginated) | Gets all policies with the specified type. (Not Paginated)  Supports expand |
| Policy: listPolicyRules (Not Paginated) | Enumerates all policy rules. (Not Paginated) |
| Template: List SMS Templates (Not Paginated) | Enumerates custom SMS templates in your organization. A subset of templates can be returned that match a template type. (Not Paginated) |
| TrustedOrigin: listOrigins |   Filters: q= filter=  |
| User: Get Assigned App Links (Not Paginated) | Fetches appLinks for all direct or indirect (via group membership) assigned applications. (Not Paginated) |
| User: Get Member Groups (Not Paginated) | Fetches the groups of which the user is a member. (Not Paginated) |
| User: getLinkedObjectsForUser | Get linked objects for a user, relationshipName can be a primary or associated relationship name |
| User: getRefreshTokenForUserAndClient | Gets a refresh token issued for the specified User and Client.  Supports expand |
| User: List Users | Lists users in your organization with pagination in most cases.  A subset of users can be returned that match a supported filter expression or search criteria.  Filters: q= search= filter=  |
| User: listApplicationTargetsForApplicationAdministratorRoleForUser | Lists all App targets for an `APP_ADMIN` Role assigned to a User. This methods return list may include full Applications or Instances. The response for an instance will have an `ID` value, while Application will not have an ID. |
| User: listAssignedRolesForUser (Not Paginated) | Lists all roles assigned to a user. (Not Paginated)  Supports expand |
| User: listGrantsForUserAndClient | Lists all grants for a specified user and client  Supports expand |
| User: listGroupTargetsForRole |  **helpText NOT SET IN DESCRIPTOR FILE** |
| User: Listing IdPs associated with a user (Not Paginated) | Lists the IdPs associated with the user. (Not Paginated) |
| User: listRefreshTokensForUserAndClient | Lists all refresh tokens issued for the specified User and Client.  Supports expand |
| User: listUserClients (Not Paginated) | Lists all client resources for which the specified user has grants or tokens. (Not Paginated) |
| User: listUserGrants | Lists all grants for the specified user  Supports expand |
| UserFactor: listFactors (Not Paginated) | Enumerates all the enrolled factors for the specified user (Not Paginated) |
| UserFactor: listSupportedFactors (Not Paginated) | Enumerates all the supported factors that can be enrolled for the specified user (Not Paginated) |
| UserFactor: listSupportedSecurityQuestions (Not Paginated) | Enumerates all available security questions for a user's `question` factor (Not Paginated) |
| UserType: listUserTypes (Not Paginated) | Fetches all User Types in your org (Not Paginated) |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Application: Assign Group to Application | Assigns a group to an application |
| Application: Update Application | Updates an application in your organization. |
| Application: Update Application Profile for Assigned User | Updates a user's profile for an application |
| AuthorizationServer: updateAuthorizationServer |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: updateAuthorizationServerPolicy |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: updateAuthorizationServerPolicyRule | Updates the configuration of the Policy Rule defined in the specified Custom Authorization Server and Policy. |
| AuthorizationServer: updateOAuth2Claim |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AuthorizationServer: updateOAuth2Scope |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EventHook: updateEventHook |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Feature: updateFeatureLifecycle |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Group: Add App Instance Target to App Administrator Role given to a Group | Add App Instance Target to App Administrator Role given to a Group |
| Group: Add User to Group | Adds a user to a group with 'OKTA_GROUP' type. |
| Group: addApplicationTargetToAdminRoleGivenToGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Group: addGroupTargetToGroupAdministratorRoleForGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Group: Update Group | Updates the profile for a group with `OKTA_GROUP` type from your organization. |
| Group: updateGroupRule | Updates a group rule. Only `INACTIVE` rules can be updated. |
| IdentityProvider: Link a user to a Social IdP without a transaction | Links an Okta user to an existing Social Identity Provider. This does not support the SAML2 Identity Provider Type |
| IdentityProvider: Update Identity Provider | Updates the configuration for an IdP. |
| InlineHook: updateInlineHook | Updates an inline hook by ID |
| Policy: updatePolicy | Updates a policy. |
| Policy: updatePolicyRule | Updates a policy rule. |
| Template: Partial SMS Template Update | Updates only some of the SMS template properties: |
| Template: Update SMS Template | Updates the SMS template. |
| TrustedOrigin: updateOrigin |  **helpText NOT SET IN DESCRIPTOR FILE** |
| User: Add App Instance Target to App Administrator Role given to a User | Add App Instance Target to App Administrator Role given to a User |
| User: addAllAppsAsTargetToRole |  **helpText NOT SET IN DESCRIPTOR FILE** |
| User: addApplicationTargetToAdminRoleForUser |  **helpText NOT SET IN DESCRIPTOR FILE** |
| User: addGroupTargetToRole |  **helpText NOT SET IN DESCRIPTOR FILE** |
| User: partialUpdateUser | Fetch a user by `id`, `login`, or `login shortname` if the short name is unambiguous. |
| User: setLinkedObjectForUser |  **helpText NOT SET IN DESCRIPTOR FILE** |
| User: Update User | Update a user's profile and/or credentials using strict-update semantics. |
| UserSchema: updateUserProfile | Partial updates on the User Profile properties of the user schema. |
| UserType: replaceUserType | Replace an existing User Type |
| UserType: updateUserType | Updates an existing User Type |


