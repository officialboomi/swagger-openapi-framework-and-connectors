# Paylocity API Connector
For general questions and support of the Paylocity API, contact: webservices@paylocity.com Overview: Paylocity Web Services API is an externally facing RESTful Internet protocol. The Paylocity API uses HTTP verbs and a RESTful endpoint structure. OAuth 2.0 is used as the API Authorization framework. Request and response payloads are formatted as JSON. Paylocity supports v1 and v2 versions of its API endpoints. v1, while supported, won't be enhanced with additional functionality. For direct link to v1 documentation, please click [here](https://docs.paylocity.com/weblink/guides/Paylocity_Web_Services_API/v1/Paylocity_Web_Services_API.htm). For additional resources regarding v1/v2 differences and conversion path, please contact webservices@paylocity.com. Setup: Paylocity will provide the secure client credentials and set up the scope (type of requests and allowed company numbers). You will receive the unique client id, secret, and Paylocity public key for the data encryption. The secret will expire in 365 days. Paylocity will send you an e-mail 10 days prior to the expiration date for the current secret. If not renewed, the second e-mail notification will be sent 5 days prior to secret's expiration. Each email will contain the code necessary to renew the client secret. You can obtain the new secret by calling API endpoint using your current not yet expired credentials and the code that was sent with the notification email. For details on API endpoint, please see Client Credentials section. Both the current secret value and the new secret value will be recognized during the transition period. After the current secret expires, you must use the new secret. If you were unable to renew the secret via API endpoint, you can still contact Service and they will email you new secret via secure email.rnrnrnWhen validating the request, Paylocity API will honor the defaults and required fields set up for the company default New Hire Template as defined in Web Pay. Authorization: Paylocity Web Services API uses OAuth2.0 Authentication with JSON Message Format. All requests of the Paylocity Web Services API require a bearer token which can be obtained by authenticating the client with the Paylocity Web Services API via OAuth 2.0.rnrnrnThe client must request a bearer token from the authorization endpoint:rnrnrnauth-server for production: https://api.paylocity.com/IdentityServer/connect/tokenrnrnrnauth-server for testing: https://apisandbox.paylocity.com/IdentityServer/connect/tokenrnrnPaylocity reserves the right to impose rate limits on the number of calls made to our APIs. Changes to API features/functionality may be made at anytime with or without prior notice.rnrn##### Authorization HeaderrnrnThe request is expected to be in the form of a basic authentication request, with the "Authorization" header containing the client-id and client-secret. This means the standard base-64 encoded user:password, prefixed with "Basic" as the value for the Authorization header, where user is the client-id and password is the client-secret.rnrn##### Content-Type HeaderrnrnThe "Content-Type" header is required to be "application/x-www-form-urlencoded".rnrn##### Additional ValuesrnrnThe request must post the following form encoded values within the request body:rnrn    grant_type = client_credentialsrn    scope = WebLinkAPIrnrn##### ResponsesrnrnSuccess will return HTTP 200 OK with JSON content:rnrn    {rn      "access_token": "xxx",rn      "expires_in": 3600,rn      "token_type": "Bearer"rn    }rnrn# EncryptionrnrnPaylocity uses a combination of RSA and AES cryptography. As part of the setup, each client is issued a public RSA key.rnrnPaylocity recommends the encryption of the incoming requests as additional protection of the sensitive data. Clients can opt-out of the encryption during the initial setup process. Opt-out will allow Paylocity to process unencrypted requests.rnrnThe Paylocity Public Key has the following properties:rnrn* 2048 bit key sizernrn* PKCS1 key formatrnrn* PEM encodingrnrn##### Propertiesrnrn* key (base 64 encoded): The AES symmetric key encrypted with the Paylocity Public Key. It is the key used to encrypt the content. Paylocity will decrypt the AES key using RSA decryption and use it to decrypt the content.rnrn* iv (base 64 encoded): The AES IV (Initialization Vector) used when encrypting the content.rnrn* content (base 64 encoded): The AES encrypted request. The key and iv provided in the secureContent request are used by Paylocity for decryption of the content.rnrnWe suggest using the following for the AES:rnrn* CBC cipher modernrn* PKCS7 paddingrnrn* 128 bit block sizernrn* 256 bit key sizernrn##### Encryption Flowrnrn* Generate the unencrypted JSON payload to POST/PUTrn* Encrypt this JSON payload using your _own key and IV_ (NOT with the Paylocity public key)rn* RSA encrypt the _key_ you used in step 2 with the Paylocity Public Key, then, base64 encode the resultrn* Base64 encode the IV used to encrypt the JSON payload in step 2rn* Put together a "securecontent" JSON object:rn rn{rn  'secureContent' : {rn    'key' : -- RSA-encrypted & base64 encoded key from step 3,rn    'iv' : -- base64 encoded iv from step 4rn    'content' -- content encrypted with your own key from step 2, base64 encodedrn  }rn}rnrnSupport: Questions about using the Paylocity API? Please contact webservices@paylocity.com Deductions (v1): Deductions API provides endpoints to retrieve, add, update and delete deductions for a company's employees. For schema details, click https://docs.paylocity.com/weblink/guides/Paylocity_Web_Services_API/v1/Paylocity_Web_Services_API.htm OnBoarding (v1): Onboarding API sends employee data into Paylocity Onboarding to help ensure an easy and accurate hiring process for subsequent completion into Web Pay. For schema details, click https://docs.paylocity.com/weblink/guides/Paylocity_Web_Services_API/v1/Paylocity_Web_Services_API.htm

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string



#### !!PRODUCTNAME!! REST API Service URL

The URL for the !!PRODUCTNAME!! REST API Service server.

**Type** - string



#### Paylocity API REST API Service URL

The URL for the Paylocity API REST API Service server.

**Type** - string

**Default Value** - https://api.paylocity.com/api/v2/openapi



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

The connector does not support field selection. All fields will be returned by default.


#### Filters

The query filter supports any number of non-nested expressions which will be AND'ed together.

Example:
((foo lessThan 30) AND (baz lessThan 42))

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts




# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| Add Client Secret - Obtain new client secret. | Client Credentials Obtain new client secret for Paylocity-issued client id. See Setup section for details. Obtain new client secret.  |
| Add Employee - Add new employee | Employee New Employee API sends new employee data directly to Web Pay. Companies who use the New Hire Template in Web Pay may require additional fields when hiring employees. New Employee API Requests will honor these required fields. Add new employee  |
| Add Local Tax - Add new local tax | Local Taxes Sends new employee local tax information directly to Web Pay. Add new local tax  |
| Add New Employee To Web Link - Add new employee to Web Link | Employee Staging Add new employee to Web Link will send partially completed or potentially erroneous new hire record to Web Link, where it can be corrected and competed by company administrator or authorized Paylocity Service Bureau employee. Add new employee to Web Link  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Delete Earning By Earning Code And Start Date - Delete Earning by Earning Code and Start Date | Earnings Delete Earning by Earning Code and Start Date Delete Earning by Earning Code and Start Date  |
| Delete Local Tax By Tax Code - Delete local tax by tax code | Local Taxes Delete local tax by tax code Delete local tax by tax code  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Get All Company Codes And Descriptions By Resource - Get All Company Codes | Company Codes Get All Company Codes for the selected company and resource Get All Company Codes  |
| Get All Custom Fields By Category - Get All Custom Fields | Custom Fields Get All Custom Fields for the selected company Get All Custom Fields  |
| Get All Direct Deposit - Get All Direct Deposit | Direct Deposit Get All Direct Deposit returns main direct deposit and all additional direct depositsfor the selected employee. Get All Direct Deposit  |
| Get All Earnings - Get All Earnings | Earnings Get All Earnings returns all earnings for the selected employee. Get All Earnings  |
| Get All Local Taxes - Get all local taxes | Local Taxes Returns all local taxes for the selected employee. Get all local taxes  |
| Get Company-specific Open API Documentation - Get Company-Specific Open API Documentation | Company-Specific Schema The company-specific Open API endpoint allows the client to GET an Open API document for the Paylocity API that is customized with company-specific resource schemas. These customized resource schemas define certain properties as enumerations of pre-defined values that correspond to the company's setup with Web Pay. The customized schemas also indicate which properties are required by the company within Web Pay.<br  />To learn more about Open API, click [here](https://www.openapis.org/) Get Company-Specific Open API Documentation  |
| Get Earning By Earning Code And Start Date - Get Earning by Earning Code and Start Date | Earnings Get Earnings returns the single earning with the provided earning code and start date for the selected employee. Get Earning by Earning Code and Start Date  |
| Get Earnings By Earning Code - Get Earnings by Earning Code | Earnings Get Earnings returns all earnings with the provided earning code for the selected employee. Get Earnings by Earning Code  |
| Get Employee - Get employee | Employee Get Employee API will return employee data currently available in Web Pay. Get employee  |
| Get Local Tax By Tax Code - Get local taxes by tax code | Local Taxes Returns all local taxes with the provided tax code for the selected employee. Get local taxes by tax code  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Get All Employees - Get all employees | Employee Get All Employees API will return employee data currently available in Web Pay. Get all employees  |
| Gets Employee Pay Statement Detail Data Based On The Specified Year - Get employee pay statement details data for the sp... | PayStatements Get pay statement details API will return employee pay statement details data currently available in Web Pay for the specified year. Get employee pay statement details data for the specified year.  |
| Gets Employee Pay Statement Detail Data Based On The Specified Year And Check Date - Get employee pay statement details data for the sp... | PayStatements Get pay statement details API will return employee pay statement detail data currently available in Web Pay for the specified year and check date. Get employee pay statement details data for the specified year and check date.  |
| Gets Employee Pay Statement Summary Data Based On The Specified Year - Get employee pay statement summary data for the sp... | PayStatements Get pay statement summary API will return employee pay statement summary data currently available in Web Pay for the specified year. Get employee pay statement summary data for the specified year.  |
| Gets Employee Pay Statement Summary Data Based On The Specified Year And Check Date - Get employee pay statement summary data for the sp... | PayStatements Get pay statement summary API will return employee pay statement summary data currently available in Web Pay for the specified year and check date. Get employee pay statement summary data for the specified year and check date.  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Add Or Update Additional Rates - Add/update additional rates (PUT) | Additional Rates Sends new or updated employee additional rates information directly to Web Pay. Add/update additional rates  |
| Add Or Update An Employee Earning - Add/Update Earning (PUT) | Earnings Add/Update Earning API sends new or updated employee earnings information directly to Web Pay. Add/Update Earning  |
| Add Or Update Emergency Contacts - Add/update emergency contacts (PUT) | Emergency Contacts Sends new or updated employee emergency contacts directly to Web Pay. Add/update emergency contacts  |
| Add Or Update Non-primary State Tax - Add/update non-primary state tax (PUT) | Non-Primary State Tax Sends new or updated employee non-primary state tax information directly to Web Pay. Add/update non-primary state tax  |
| Add Or Update Primary State Tax - Add/update primary state tax (PUT) | Primary State Tax Sends new or updated employee primary state tax information directly to Web Pay. Add/update primary state tax  |
| Update Employee - Update employee (PATCH) | Employee Update Employee API will update existing employee data in WebPay. Update employee  |
| Update Or Add Employee Benefit Setup - Add/update employee's benefit setup (PUT) | Employee Benefit Setup Sends new or updated employee benefit setup information directly to Web Pay. Add/update employee's benefit setup  |


