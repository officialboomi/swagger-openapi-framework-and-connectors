# Bullhorn REST API
This connector implements the Bullhorn REST API. For more information click here: https://bullhorn.github.io/rest-api-docs/

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string



#### Regional Data Center

The URL for the service server

**Type** - string

##### Allowed Values

 * US East (CLS40, CLS41, CLS42)
 * US West (CLS30, CLS31, CLS32, CLS33, CLS34, CLS35)
 * US West (CLS50)
 * Australia (CLS66)
 * Germany (CLS70)
 * Singapore (CLS60)
 * UK (CLS21, CLS22, CLS23)
 * Sandbox US West (CLS91,CLS99)
 * Sandbox UK (CLS29)


#### Other Region

Specify the subdomain suffix of any region not listed above.

**Type** - string



#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### Client ID

The OAUTH Client ID

**Type** - string



#### Client Secret

The OAUTH Client Secret

**Type** - password

# Operation Tab


## CREATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| ActivityGoal |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ActivityGoalConfiguration |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ActivityGoalTarget |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Appointment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AppointmentAttendee |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AppointmentEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AppointmentEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| BillableChargeEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| BillableChargeEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Branch |  **helpText NOT SET IN DESCRIPTOR FILE** |
| BusinessSector |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Candidate |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationRequirement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationRequirementEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationRequirementEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateEducation |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateReference |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateReferenceQuestion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateReferenceResponse |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateSource |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateTaxInfo |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateWorkHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Category |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Certification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationRequirement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationRequirementStatusLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContactFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContactHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationAppointment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCertification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject11EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject12EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject13EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject14EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject15EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject16EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject17EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject18EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject19EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject20EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject21EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject22EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject23EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject24EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject25EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject26EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject27EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject28EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject29EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject30EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject31EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject32EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject33EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject34EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject35EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance11 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance12 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance13 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance14 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance15 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance16 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance17 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance18 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance19 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance20 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance21 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance22 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance23 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance24 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance25 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance26 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance27 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance28 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance29 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance30 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance31 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance32 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance33 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance34 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance35 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationNote |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationTask |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CorporateUser |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CorporationDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Country |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CustomAction |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Department |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EntityFieldTypeLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EstaffMappableFlowback |  **helpText NOT SET IN DESCRIPTOR FILE** |
| GoalTarget |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplex |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexAmenity |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexFurnitureDelivery |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexUnit |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexUtilityAccount |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Issue |  **helpText NOT SET IN DESCRIPTOR FILE** |
| IssueItems |  **helpText NOT SET IN DESCRIPTOR FILE** |
| IssueResolutionStatusLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobBoardPost |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCard |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCardLine |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCardLineGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCardVersion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderTemplate |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmission |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionCertificationRequirement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionCertificationRequirementEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionCertificationRequirementEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Lead |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LeadHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Location |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationEffectiveDateChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationVersion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| MappedEntityConfigurationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| MappedEntityConfigurationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Note |  **helpText NOT SET IN DESCRIPTOR FILE** |
| NoteEntity |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayableChargeEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayableChargeEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayrollEmployeeTypeLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayrollSyncStatusLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Person |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCertification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCertificationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCertificationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementChangeRequest |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementChangeRequestEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementChangeRequestEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCommission |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCommissionEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCommissionEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCard |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardEffectiveDateChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardLine |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardLineGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardVersion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Report |  **helpText NOT SET IN DESCRIPTOR FILE** |
| SalesQuota |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Sendout |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Shift |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Skill |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Specialty |  **helpText NOT SET IN DESCRIPTOR FILE** |
| State |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Task |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TaskEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TaskEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Tearsheet |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TearsheetMember |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TearsheetRecipient |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TimeUnit |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserHousingComplexUnit |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserPulseCallLog |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserPulseCallLogContact |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserType |  **helpText NOT SET IN DESCRIPTOR FILE** |
| WorkersCompensation |  **helpText NOT SET IN DESCRIPTOR FILE** |
| WorkersCompensationRate |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ZipCodeGis |  **helpText NOT SET IN DESCRIPTOR FILE** |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| ActivityGoal |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ActivityGoalConfiguration |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ActivityGoalTarget |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Appointment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AppointmentAttendee |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AppointmentEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AppointmentEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| BillableChargeEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| BillableChargeEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Branch |  **helpText NOT SET IN DESCRIPTOR FILE** |
| BusinessSector |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Candidate |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationRequirement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationRequirementEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationRequirementEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateEducation |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateReference |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateReferenceQuestion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateReferenceResponse |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateSource |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateTaxInfo |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateWorkHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Category |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Certification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationRequirement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationRequirementStatusLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContactFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContactHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationAppointment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCertification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject11EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject12EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject13EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject14EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject15EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject16EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject17EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject18EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject19EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject20EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject21EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject22EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject23EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject24EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject25EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject26EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject27EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject28EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject29EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject30EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject31EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject32EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject33EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject34EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject35EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance11 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance12 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance13 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance14 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance15 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance16 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance17 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance18 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance19 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance20 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance21 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance22 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance23 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance24 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance25 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance26 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance27 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance28 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance29 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance30 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance31 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance32 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance33 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance34 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance35 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationNote |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationTask |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CorporateUser |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CorporationDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Country |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CustomAction |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Department |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EntityFieldTypeLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EstaffMappableFlowback |  **helpText NOT SET IN DESCRIPTOR FILE** |
| GoalTarget |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplex |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexAmenity |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexFurnitureDelivery |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexUnit |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexUtilityAccount |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Issue |  **helpText NOT SET IN DESCRIPTOR FILE** |
| IssueItems |  **helpText NOT SET IN DESCRIPTOR FILE** |
| IssueResolutionStatusLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobBoardPost |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCard |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCardLine |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCardLineGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCardVersion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderTemplate |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmission |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionCertificationRequirement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionCertificationRequirementEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionCertificationRequirementEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Lead |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LeadHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Location |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationEffectiveDateChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationVersion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| MappedEntityConfigurationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| MappedEntityConfigurationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Note |  **helpText NOT SET IN DESCRIPTOR FILE** |
| NoteEntity |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayableChargeEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayableChargeEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayrollEmployeeTypeLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayrollSyncStatusLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Person |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCertification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCertificationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCertificationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementChangeRequest |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementChangeRequestEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementChangeRequestEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCommission |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCommissionEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCommissionEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCard |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardEffectiveDateChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardLine |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardLineGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardVersion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Report |  **helpText NOT SET IN DESCRIPTOR FILE** |
| SalesQuota |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Sendout |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Shift |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Skill |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Specialty |  **helpText NOT SET IN DESCRIPTOR FILE** |
| State |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Task |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TaskEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TaskEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Tearsheet |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TearsheetMember |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TearsheetRecipient |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TimeUnit |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserHousingComplexUnit |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserPulseCallLog |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserPulseCallLogContact |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserType |  **helpText NOT SET IN DESCRIPTOR FILE** |
| WorkersCompensation |  **helpText NOT SET IN DESCRIPTOR FILE** |
| WorkersCompensationRate |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ZipCodeGis |  **helpText NOT SET IN DESCRIPTOR FILE** |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| ActivityGoal |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ActivityGoalConfiguration |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ActivityGoalTarget |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Appointment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AppointmentAttendee |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AppointmentEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AppointmentEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| BillableChargeEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| BillableChargeEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Branch |  **helpText NOT SET IN DESCRIPTOR FILE** |
| BusinessSector |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Candidate |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationRequirement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationRequirementEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationRequirementEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateEducation |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateReference |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateReferenceQuestion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateReferenceResponse |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateSource |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateTaxInfo |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateWorkHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Category |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Certification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationRequirement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationRequirementStatusLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContactFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContactHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationAppointment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCertification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject11EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject12EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject13EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject14EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject15EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject16EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject17EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject18EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject19EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject20EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject21EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject22EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject23EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject24EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject25EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject26EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject27EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject28EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject29EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject30EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject31EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject32EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject33EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject34EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject35EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance11 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance12 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance13 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance14 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance15 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance16 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance17 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance18 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance19 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance20 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance21 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance22 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance23 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance24 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance25 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance26 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance27 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance28 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance29 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance30 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance31 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance32 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance33 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance34 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance35 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationNote |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationTask |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CorporateUser |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CorporationDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Country |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CustomAction |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Department |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EntityFieldTypeLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EstaffMappableFlowback |  **helpText NOT SET IN DESCRIPTOR FILE** |
| GoalTarget |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplex |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexAmenity |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexFurnitureDelivery |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexUnit |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexUtilityAccount |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Issue |  **helpText NOT SET IN DESCRIPTOR FILE** |
| IssueItems |  **helpText NOT SET IN DESCRIPTOR FILE** |
| IssueResolutionStatusLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobBoardPost |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCard |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCardLine |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCardLineGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCardVersion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderTemplate |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmission |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionCertificationRequirement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionCertificationRequirementEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionCertificationRequirementEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Lead |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LeadHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Location |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationEffectiveDateChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationVersion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| MappedEntityConfigurationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| MappedEntityConfigurationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Note |  **helpText NOT SET IN DESCRIPTOR FILE** |
| NoteEntity |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayableChargeEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayableChargeEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayrollEmployeeTypeLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayrollSyncStatusLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Person |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCertification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCertificationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCertificationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementChangeRequest |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementChangeRequestEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementChangeRequestEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCommission |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCommissionEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCommissionEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCard |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardEffectiveDateChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardLine |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardLineGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardVersion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Report |  **helpText NOT SET IN DESCRIPTOR FILE** |
| SalesQuota |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Sendout |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Shift |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Skill |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Specialty |  **helpText NOT SET IN DESCRIPTOR FILE** |
| State |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Task |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TaskEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TaskEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Tearsheet |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TearsheetMember |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TearsheetRecipient |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TimeUnit |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserHousingComplexUnit |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserPulseCallLog |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserPulseCallLogContact |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserType |  **helpText NOT SET IN DESCRIPTOR FILE** |
| WorkersCompensation |  **helpText NOT SET IN DESCRIPTOR FILE** |
| WorkersCompensationRate |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ZipCodeGis |  **helpText NOT SET IN DESCRIPTOR FILE** |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| ActivityGoal |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ActivityGoalConfiguration |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ActivityGoalTarget |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Appointment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AppointmentAttendee |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AppointmentEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AppointmentEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| BillableChargeEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| BillableChargeEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Branch |  **helpText NOT SET IN DESCRIPTOR FILE** |
| BusinessSector |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Candidate |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationRequirement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationRequirementEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationRequirementEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateEducation |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateReference |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateReferenceQuestion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateReferenceResponse |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateSource |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateTaxInfo |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateWorkHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Category |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Certification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationRequirement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationRequirementStatusLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContactFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContactHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationAppointment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCertification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject11EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject12EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject13EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject14EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject15EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject16EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject17EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject18EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject19EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject20EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject21EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject22EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject23EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject24EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject25EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject26EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject27EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject28EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject29EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject30EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject31EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject32EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject33EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject34EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject35EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance11 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance12 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance13 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance14 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance15 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance16 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance17 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance18 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance19 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance20 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance21 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance22 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance23 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance24 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance25 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance26 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance27 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance28 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance29 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance30 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance31 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance32 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance33 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance34 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance35 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationNote |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationTask |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CorporateUser |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CorporationDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Country |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CustomAction |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Department |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EntityFieldTypeLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EstaffMappableFlowback |  **helpText NOT SET IN DESCRIPTOR FILE** |
| GoalTarget |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplex |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexAmenity |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexFurnitureDelivery |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexUnit |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexUtilityAccount |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Issue |  **helpText NOT SET IN DESCRIPTOR FILE** |
| IssueItems |  **helpText NOT SET IN DESCRIPTOR FILE** |
| IssueResolutionStatusLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobBoardPost |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCard |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCardLine |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCardLineGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCardVersion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderTemplate |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmission |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionCertificationRequirement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionCertificationRequirementEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionCertificationRequirementEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Lead |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LeadHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Location |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationEffectiveDateChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationVersion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| MappedEntityConfigurationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| MappedEntityConfigurationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Note |  **helpText NOT SET IN DESCRIPTOR FILE** |
| NoteEntity |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayableChargeEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayableChargeEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayrollEmployeeTypeLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayrollSyncStatusLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Person |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCertification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCertificationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCertificationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementChangeRequest |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementChangeRequestEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementChangeRequestEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCommission |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCommissionEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCommissionEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCard |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardEffectiveDateChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardLine |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardLineGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardVersion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Report |  **helpText NOT SET IN DESCRIPTOR FILE** |
| SalesQuota |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Sendout |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Shift |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Skill |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Specialty |  **helpText NOT SET IN DESCRIPTOR FILE** |
| State |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Task |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TaskEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TaskEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Tearsheet |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TearsheetMember |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TearsheetRecipient |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TimeUnit |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserHousingComplexUnit |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserPulseCallLog |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserPulseCallLogContact |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserType |  **helpText NOT SET IN DESCRIPTOR FILE** |
| WorkersCompensation |  **helpText NOT SET IN DESCRIPTOR FILE** |
| WorkersCompensationRate |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ZipCodeGis |  **helpText NOT SET IN DESCRIPTOR FILE** |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| ActivityGoal |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ActivityGoalConfiguration |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ActivityGoalTarget |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Appointment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AppointmentAttendee |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AppointmentEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| AppointmentEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| BillableChargeEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| BillableChargeEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Branch |  **helpText NOT SET IN DESCRIPTOR FILE** |
| BusinessSector |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Candidate |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationRequirement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationRequirementEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateCertificationRequirementEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateEducation |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateReference |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateReferenceQuestion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateReferenceResponse |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateSource |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateTaxInfo |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CandidateWorkHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Category |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Certification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationRequirement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CertificationRequirementStatusLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContact5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContactFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientContactHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporation5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationAppointment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCertification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject11EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject12EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject13EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject14EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject15EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject16EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject17EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject18EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject19EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject20EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject21EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject22EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject23EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject24EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject25EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject26EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject27EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject28EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject29EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject30EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject31EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject32EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject33EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject34EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject35EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance11 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance12 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance13 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance14 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance15 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance16 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance17 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance18 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance19 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance20 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance21 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance22 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance23 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance24 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance25 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance26 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance27 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance28 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance29 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance30 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance31 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance32 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance33 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance34 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance35 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationNote |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ClientCorporationTask |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CorporateUser |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CorporationDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Country |  **helpText NOT SET IN DESCRIPTOR FILE** |
| CustomAction |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Department |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EntityFieldTypeLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| EstaffMappableFlowback |  **helpText NOT SET IN DESCRIPTOR FILE** |
| GoalTarget |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplex |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexAmenity |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexFurnitureDelivery |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexUnit |  **helpText NOT SET IN DESCRIPTOR FILE** |
| HousingComplexUtilityAccount |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Issue |  **helpText NOT SET IN DESCRIPTOR FILE** |
| IssueItems |  **helpText NOT SET IN DESCRIPTOR FILE** |
| IssueResolutionStatusLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobBoardPost |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrder5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCard |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCardLine |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCardLineGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderRateCardVersion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobOrderTemplate |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmission |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionCertificationRequirement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionCertificationRequirementEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionCertificationRequirementEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| JobSubmissionHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Lead |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LeadHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Location |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationEffectiveDateChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| LocationVersion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| MappedEntityConfigurationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| MappedEntityConfigurationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Note |  **helpText NOT SET IN DESCRIPTOR FILE** |
| NoteEntity |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Opportunity5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| OpportunityHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayableChargeEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayableChargeEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayrollEmployeeTypeLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PayrollSyncStatusLookup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Person |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PersonCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Placement5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCertification |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCertificationEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCertificationEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementChangeRequest |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementChangeRequestEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementChangeRequestEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCommission |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCommissionEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCommissionEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject10EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject1EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject2EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject3EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject4EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject5EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject6EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject7EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject8EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObject9EditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance1 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance10 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance2 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance3 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance4 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance5 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance6 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance7 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance8 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementCustomObjectInstance9 |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementFileAttachment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCard |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardEffectiveDateChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardLine |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardLineGroup |  **helpText NOT SET IN DESCRIPTOR FILE** |
| PlacementRateCardVersion |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Report |  **helpText NOT SET IN DESCRIPTOR FILE** |
| SalesQuota |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Sendout |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Shift |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Skill |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Specialty |  **helpText NOT SET IN DESCRIPTOR FILE** |
| State |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Task |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TaskEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TaskEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| Tearsheet |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TearsheetMember |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TearsheetRecipient |  **helpText NOT SET IN DESCRIPTOR FILE** |
| TimeUnit |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserCustomObject |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserCustomObjectAttribute |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserCustomObjectDepartment |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserEditHistory |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserEditHistoryFieldChange |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserHousingComplexUnit |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserPulseCallLog |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserPulseCallLogContact |  **helpText NOT SET IN DESCRIPTOR FILE** |
| UserType |  **helpText NOT SET IN DESCRIPTOR FILE** |
| WorkersCompensation |  **helpText NOT SET IN DESCRIPTOR FILE** |
| WorkersCompensationRate |  **helpText NOT SET IN DESCRIPTOR FILE** |
| ZipCodeGis |  **helpText NOT SET IN DESCRIPTOR FILE** |


