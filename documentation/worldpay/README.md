# Worldpay Connector
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### Swagger URL

The URL for the Swagger descriptor file

**Type** - string

**Default Value** - resources/worldpay/swagger.json



#### URL

The URL for the service

**Type** - string

**Default Value** - https://petstore.swagger.io

# Operation Tab


## CREATE
# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| ACHCardBlock - Set or remove block from ACH Card. | ACH Set or remove block from ACH Card.  | /achcard/cardblock\_\_\_post |
| ACHCardClose - Close ACH Card. | ACH Close ACH Card.  | /achcard/cardclose\_\_\_post |
| ACHCardInfo - Inquiry for ACH Card data | ACH Inquiry for ACH Card data  | /achcard/cardinfo\_\_\_post |
| ACHCardOpen - Open ACH Card. | ACH Open ACH Card.  | /achard/cardopen\_\_\_post |
| ACHCardReorder - Replace or reorder ACH Card. | ACH Replace or reorder ACH Card.  | /achcard/cardreorder\_\_\_post |
| ACHCardUpdate - Update ACH Card data. | ACH Update ACH Card data.  | /achcard/cardupdate\_\_\_post |
| ACHChangePin - change pin on ACH Card | ACH change pin on ACH Card  | /achcard/changepin\_\_\_post |
| ACHDailyCardLimitInfo - Return limits for ACH Card. | ACH Return limits for ACH Card.  | /achcard/dailycardlimitinfo\_\_\_post |
| ACHDailyCardLimitUpdate - Update limits on ACH Card. | ACH Update limits on ACH Card.  | /achcard/dailycardlimitupdate\_\_\_post |
| ACHNewActivation - Activate a new ACH Card. | ACH Activate a new ACH Card.  | /achcard/newactivation\_\_\_post |
| ACHReissueActivation - Activate a re-issued ACH Card. | ACH Activate a re-issued ACH Card.  | /achcard/reissueactivation\_\_\_post |
| BatchuploadHeader - description | Batchupload description  | /batchupload/header\_\_\_post |
| BatchuploadTrailer - description | Batchupload description  | /batchupload/trailer\_\_\_post |
| CheckAuthorization - description | Check description  | /check/authorization\_\_\_post |
| CheckConversion - description | Check description  | /check/conversion\_\_\_post |
| CheckGuaranteewithconversion - description | Check description  | /check/guaranteewithconversion\_\_\_post |
| CheckVerificationwithconversion - description | Check description  | /check/verificationwithconversion\_\_\_post |
| CreditAuthorization - Use the Credit Card Authorization transaction to c... | Credit Use the Credit Card Authorization transaction to confirm that a customer has submitted a valid payment method with their order and has sufficient funds to purchase the goods or services they ordered. An approved Credit Card Authorization reduces the customer's credit limit (or bank balance, in the case of a debit card) by the amount of the approval by placing the amount on hold.  By default, a credit card authorization is a preauthorization (preauth), as there will always be a second piece to finalize the authorization:  an EMD record or a capture request.  | /credit/authorization\_\_\_post |
| CreditBalanceinquiry - description | Credit description  | /credit/balanceinquiry\_\_\_post |
| CreditBillpayment - description | Credit description  | /credit/billpayment\_\_\_post |
| CreditCashadvance - description | Credit description  | /credit/cashadvance\_\_\_post |
| CreditCompletion - Use this transaction to transfer previously author... | Credit Use this transaction to transfer previously authorized funds from the customer to you after order fulfillment.  | /credit/completion\_\_\_post |
| CreditDccratelookup - description | Credit description  | /credit/dccratelookup\_\_\_post |
| CreditFleetcompletion - description | Credit description  | /credit/fleetcompletion\_\_\_post |
| CreditFleetpreauth - description | Credit description  | /credit/fleetpreauth\_\_\_post |
| CreditFleetpurchase - description | Credit description  | /credit/fleetpurchase\_\_\_post |
| CreditFleetrefund - description | Credit description  | /credit/fleetrefund\_\_\_post |
| CreditHealthcarecompletion - description | Credit description  | /credit/healthcarecompletion\_\_\_post |
| CreditHealthcareinquiry - description | Credit description  | /credit/healthcareinquiry\_\_\_post |
| CreditHealthcarepreauth - description | Credit description  | /credit/healthcarepreauth\_\_\_post |
| CreditHealthcarepurchase - description | Credit description  | /credit/healthcarepurchase\_\_\_post |
| CreditHealthcarerefund - description | Credit description  | /credit/healthcarerefund\_\_\_post |
| CreditPurchase - Use this transaction to initiate a purchase using ... | Credit Use this transaction to initiate a purchase using credit account.  | /credit/purchase\_\_\_post |
| CreditRefund - Use this transaction to refund money to a customer... | Credit Use this transaction to refund money to a customer, when the original transaction was processed on the Worldpay platform.  | /credit/refund\_\_\_post |
| DebitBalanceinquiry - description | Debit description  | /debit/balanceinquiry\_\_\_post |
| DebitCashwithdrawal - description | Debit description  | /debit/cashwithdrawal\_\_\_post |
| DebitCompletion - description | Debit description  | /debit/completion\_\_\_post |
| DebitInquiry - description | Debit description  | /debit/inquiry\_\_\_post |
| DebitInterbanktransfercredit - description | Debit description  | /debit/interbanktransfercredit\_\_\_post |
| DebitInterbanktransferdebit - description | Debit description  | /debit/interbanktransferdebit\_\_\_post |
| DebitPayment - description | Debit description  | /debit/payment\_\_\_post |
| DebitPaymentcredit - description | Debit description  | /debit/paymentcredit\_\_\_post |
| DebitPintranslation - description | Debit description  | /debit/pintranslation\_\_\_post |
| DebitPreauth - description | Debit description  | /debit/preauth\_\_\_post |
| DebitPurchase - description | Debit description  | /debit/purchase\_\_\_post |
| DebitQuasicashpurchase - description | Debit description  | /debit/quasicashpurchase\_\_\_post |
| DebitRefund - description | Debit description  | /debit/refund\_\_\_post |
| DebitWicprevalidation - description | Debit description  | /debit/wicprevalidation\_\_\_post |
| GiftcardActivation - description | Giftcard description  | /giftcard/activation\_\_\_post |
| GiftcardClose - description | Giftcard description  | /giftcard/close\_\_\_post |
| GiftcardCompletion - description | Giftcard description  | /giftcard/completion\_\_\_post |
| GiftcardInquiry - description | Giftcard description  | /giftcard/inquiry\_\_\_post |
| GiftcardMassactivation - description | Giftcard description  | /giftcard/massactivation\_\_\_post |
| GiftcardMassclose - description | Giftcard description  | /giftcard/massclose\_\_\_post |
| GiftcardMassinquiry - description | Giftcard description  | /giftcard/massinquiry\_\_\_post |
| GiftcardMassreload - description | Giftcard description  | /giftcard/massreload\_\_\_post |
| GiftcardMassunload - description | Giftcard description  | /giftcard/massunload\_\_\_post |
| GiftcardMinistatement - description | Giftcard description  | /giftcard/ministatement\_\_\_post |
| GiftcardPreauth - description | Giftcard description  | /giftcard/preauth\_\_\_post |
| GiftcardPurchase - description | Giftcard description  | /giftcard/purchase\_\_\_post |
| GiftcardRefund - description | Giftcard description  | /giftcard/refund\_\_\_post |
| GiftcardReload - description | Giftcard description  | /giftcard/reload\_\_\_post |
| GiftcardStatuscard - description | Giftcard description  | /giftcard/statuscard\_\_\_post |
| GiftcardTransfer - description | Giftcard description  | /giftcard/transfer\_\_\_post |
| GiftcardUnload - description | Giftcard description  | /giftcard/unload\_\_\_post |
| GiftcardVirtualcard - description | Giftcard description  | /giftcard/virtualcard\_\_\_post |
| KeychangeConfirm - Master/Session - Confirm Key was applied. The cust... | Keychange Master/Session - Confirm Key was applied. The customer will confirm that they have applied the key and that Worldpay should begin using the new key. Both the key and check digits should be returned on the confirm key request.  | /keychange/confirm\_\_\_post |
| KeychangeMakekey - Master/Session - Request a Key. The customer will ... | Keychange Master/Session - Request a Key. The customer will request that Worlpday make a key for them.  If successful, Worldpay will return the key and check digits.  | /keychange/makekey\_\_\_post |
| StoreClose - description | Store description  | /store/close\_\_\_post |
| StoreTotals - description | Store description  | /store/totals\_\_\_post |
| TokenizationConversion - This service returns a low value token given a hig... | Tokenization This service returns a low value token given a high value token. In this case, the Low Value token is a temporary token that will expire after 24 hours and a High Value token is the permanent token associated with that card.  | /tokenization/conversion\_\_\_post |
| TokenizationDeregistrationlohi - This service returns a high value token for a give... | Tokenization This service returns a high value token for a given low value token.  Use this to translate a temporary, low value token into the permanent, high value token associated with the underlying card.  | /tokenization/deregistrationlohi\_\_\_post |
| TokenizationDeregistrationlopan - This service returns a high value token and card n... | Tokenization This service returns a high value token and card number for a given low value token.  Use this to translate a temporary, low value token into both the permanent, high value token and the underlying card number.  | /tokenization/deregistrationlopan\_\_\_post |
| TokenizationDetoken - Returns the card number related to a specific toke... | Tokenization Returns the card number related to a specific token.  | /tokenization/detoken\_\_\_post |
| TokenizationToken - You use the tokenization transaction to submit a c... | Tokenization You use the tokenization transaction to submit a credit card number to Worldpay and receive a token in return.  | /tokenization/token\_\_\_post |


