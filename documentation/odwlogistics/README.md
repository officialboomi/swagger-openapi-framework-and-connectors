# ODW Logistics API
ODW Logistics supplychain solutions API

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string



#### ODW Logistics API REST API Service URL

The URL for the ODW Logistics API REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### AuthenticationType

Allows user to select between BASIC and OAUTH 2.0 Authentication

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| Get-auth - Generates jwt tokens when a valid username and pas... | Generates jwt tokens when a valid username and password is passed in the request body. Generates jwt tokens when a valid username and password is passed in the request body.  |
| Post-accessorial-code - Implements entity insert endpoint for AccessorialC... | Implements entity insert endpoint for AccessorialCode. Implements entity insert endpoint for AccessorialCode.  |
| Post-action-definition - Implements entity insert endpoint for ActionDefini... | Implements entity insert endpoint for ActionDefinition. Implements entity insert endpoint for ActionDefinition.  |
| Post-allocated-charge - Implements entity insert endpoint for AllocatedCha... | Implements entity insert endpoint for AllocatedCharge. Implements entity insert endpoint for AllocatedCharge.  |
| Post-allocated-cost - Implements entity insert endpoint for AllocatedCos... | Implements entity insert endpoint for AllocatedCost. Implements entity insert endpoint for AllocatedCost.  |
| Post-authority - Implements entity insert endpoint for Authority. | Implements entity insert endpoint for Authority. Implements entity insert endpoint for Authority.  |
| Post-bill-to - Implements entity insert endpoint for BillTo. | Implements entity insert endpoint for BillTo. Implements entity insert endpoint for BillTo.  |
| Post-business-type - Implements entity insert endpoint for BusinessType... | Implements entity insert endpoint for BusinessType. Implements entity insert endpoint for BusinessType.  |
| Post-calendar - Implements entity insert endpoint for Calendar. | Implements entity insert endpoint for Calendar. Implements entity insert endpoint for Calendar.  |
| Post-carrier - Implements entity insert endpoint for Carrier. | Implements entity insert endpoint for Carrier. Implements entity insert endpoint for Carrier.  |
| Post-carrier-comment - Implements entity insert endpoint for CarrierComme... | Implements entity insert endpoint for CarrierComment. Implements entity insert endpoint for CarrierComment.  |
| Post-carrier-ref - Implements entity insert endpoint for CarrierRef. | Implements entity insert endpoint for CarrierRef. Implements entity insert endpoint for CarrierRef.  |
| Post-client-comment - Implements entity insert endpoint for ClientCommen... | Implements entity insert endpoint for ClientComment. Implements entity insert endpoint for ClientComment.  |
| Post-client-ref - Implements entity insert endpoint for ClientRef. | Implements entity insert endpoint for ClientRef. Implements entity insert endpoint for ClientRef.  |
| Post-commodity - Implements entity insert endpoint for Commodity. | Implements entity insert endpoint for Commodity. Implements entity insert endpoint for Commodity.  |
| Post-connection-definition - Implements entity insert endpoint for ConnectionDe... | Implements entity insert endpoint for ConnectionDefinition. Implements entity insert endpoint for ConnectionDefinition.  |
| Post-customer-comment - Implements entity insert endpoint for CustomerComm... | Implements entity insert endpoint for CustomerComment. Implements entity insert endpoint for CustomerComment.  |
| Post-customer-ref - Implements entity insert endpoint for CustomerRef. | Implements entity insert endpoint for CustomerRef. Implements entity insert endpoint for CustomerRef.  |
| Post-equipment - Implements entity insert endpoint for Equipment. | Implements entity insert endpoint for Equipment. Implements entity insert endpoint for Equipment.  |
| Post-equipment-group-buckets - Implements entity insert endpoint for EquipmentGro... | Implements entity insert endpoint for EquipmentGroupBuckets. Implements entity insert endpoint for EquipmentGroupBuckets.  |
| Post-error-queue - Implements entity insert endpoint for ErrorQueue. | Implements entity insert endpoint for ErrorQueue. Implements entity insert endpoint for ErrorQueue.  |
| Post-forgot-password - Validates a user's request and sends them a passwo... | Validates a user's request and sends them a password reset email. Validates a user's request and sends them a password reset email.  |
| Post-forgot-password-change - Updates a user's password if it passes validation ... | Updates a user's password if it passes validation checks. Updates a user's password if it passes validation checks.  |
| Post-freight-bill - Implements entity insert endpoint for FreightBill. | Implements entity insert endpoint for FreightBill. Implements entity insert endpoint for FreightBill.  |
| Post-freight-bill-comment - Implements entity insert endpoint for FreightBillC... | Implements entity insert endpoint for FreightBillComment. Implements entity insert endpoint for FreightBillComment.  |
| Post-freight-bill-cost-detail - Implements entity insert endpoint for FreightBillC... | Implements entity insert endpoint for FreightBillCostDetail. Implements entity insert endpoint for FreightBillCostDetail.  |
| Post-freight-bill-geo-info - Implements entity insert endpoint for FreightBillG... | Implements entity insert endpoint for FreightBillGeoInfo. Implements entity insert endpoint for FreightBillGeoInfo.  |
| Post-freight-bill-line-item - Implements entity insert endpoint for FreightBillL... | Implements entity insert endpoint for FreightBillLineItem. Implements entity insert endpoint for FreightBillLineItem.  |
| Post-freight-bill-ref - Implements entity insert endpoint for FreightBillR... | Implements entity insert endpoint for FreightBillRef. Implements entity insert endpoint for FreightBillRef.  |
| Post-group - Implements entity insert endpoint for Group. | Implements entity insert endpoint for Group. Implements entity insert endpoint for Group.  |
| Post-group-role - Implements entity insert endpoint for GroupRole. | Implements entity insert endpoint for GroupRole. Implements entity insert endpoint for GroupRole.  |
| Post-groups-roles - Handles retrieving all the roles associated to a g... | Handles retrieving all the roles associated to a given list of group ids. Handles retrieving all the roles associated to a given list of group ids.  |
| Post-handling-unit-type - Implements entity insert endpoint for HandlingUnit... | Implements entity insert endpoint for HandlingUnitType. Implements entity insert endpoint for HandlingUnitType.  |
| Post-invoice - Implements entity insert endpoint for Invoice. | Implements entity insert endpoint for Invoice. Implements entity insert endpoint for Invoice.  |
| Post-invoice-billing - Implements entity insert endpoint for InvoiceBilli... | Implements entity insert endpoint for InvoiceBilling. Implements entity insert endpoint for InvoiceBilling.  |
| Post-invoice-comment - Implements entity insert endpoint for InvoiceComme... | Implements entity insert endpoint for InvoiceComment. Implements entity insert endpoint for InvoiceComment.  |
| Post-invoice-ref - Implements entity insert endpoint for InvoiceRef. | Implements entity insert endpoint for InvoiceRef. Implements entity insert endpoint for InvoiceRef.  |
| Post-item-master - Implements entity insert endpoint for ItemMaster. | Implements entity insert endpoint for ItemMaster. Implements entity insert endpoint for ItemMaster.  |
| Post-label - Implements entity insert endpoint for Label. | Implements entity insert endpoint for Label. Implements entity insert endpoint for Label.  |
| Post-load - Implements entity insert endpoint for Load. | Implements entity insert endpoint for Load. Implements entity insert endpoint for Load.  |
| Post-load-charge - Implements entity insert endpoint for LoadCharge. | Implements entity insert endpoint for LoadCharge. Implements entity insert endpoint for LoadCharge.  |
| Post-load-comment - Implements entity insert endpoint for LoadComment. | Implements entity insert endpoint for LoadComment. Implements entity insert endpoint for LoadComment.  |
| Post-load-cost - Implements entity insert endpoint for LoadCost. | Implements entity insert endpoint for LoadCost. Implements entity insert endpoint for LoadCost.  |
| Post-load-cost-option - Implements entity insert endpoint for LoadCostOpti... | Implements entity insert endpoint for LoadCostOption. Implements entity insert endpoint for LoadCostOption.  |
| Post-load-order - Implements entity insert endpoint for LoadOrder. | Implements entity insert endpoint for LoadOrder. Implements entity insert endpoint for LoadOrder.  |
| Post-load-ref - Implements entity insert endpoint for LoadRef. | Implements entity insert endpoint for LoadRef. Implements entity insert endpoint for LoadRef.  |
| Post-load-stop - Implements entity insert endpoint for LoadStop. | Implements entity insert endpoint for LoadStop. Implements entity insert endpoint for LoadStop.  |
| Post-location - Implements entity insert endpoint for Location. | Implements entity insert endpoint for Location. Implements entity insert endpoint for Location.  |
| Post-master-location - Implements entity insert endpoint for MasterLocati... | Implements entity insert endpoint for MasterLocation. Implements entity insert endpoint for MasterLocation.  |
| Post-order - Implements entity insert endpoint for Order. | Implements entity insert endpoint for Order. Implements entity insert endpoint for Order.  |
| Post-order-charge - Implements entity insert endpoint for OrderCharge. | Implements entity insert endpoint for OrderCharge. Implements entity insert endpoint for OrderCharge.  |
| Post-order-comment - Implements entity insert endpoint for OrderComment... | Implements entity insert endpoint for OrderComment. Implements entity insert endpoint for OrderComment.  |
| Post-order-leg - Implements entity insert endpoint for OrderLeg. | Implements entity insert endpoint for OrderLeg. Implements entity insert endpoint for OrderLeg.  |
| Post-order-line - Implements entity insert endpoint for OrderLine. | Implements entity insert endpoint for OrderLine. Implements entity insert endpoint for OrderLine.  |
| Post-order-ref - Implements entity insert endpoint for OrderRef. | Implements entity insert endpoint for OrderRef. Implements entity insert endpoint for OrderRef.  |
| Post-organization - Implements entity insert endpoint for Organization... | Implements entity insert endpoint for Organization. Implements entity insert endpoint for Organization.  |
| Post-partner - Implements entity insert endpoint for Partner. | Implements entity insert endpoint for Partner. Implements entity insert endpoint for Partner.  |
| Post-partner-list-simple - Updates a user's password. | Updates a user's password. Updates a user's password.  |
| Post-payment - Implements entity insert endpoint for Payment. | Implements entity insert endpoint for Payment. Implements entity insert endpoint for Payment.  |
| Post-priority - Implements entity insert endpoint for Priority. | Implements entity insert endpoint for Priority. Implements entity insert endpoint for Priority.  |
| Post-quote - Implements entity insert endpoint for Quote. | Implements entity insert endpoint for Quote. Implements entity insert endpoint for Quote.  |
| Post-route - Implements entity insert endpoint for Route. | Implements entity insert endpoint for Route. Implements entity insert endpoint for Route.  |
| Post-service - Implements entity insert endpoint for Service. | Implements entity insert endpoint for Service. Implements entity insert endpoint for Service.  |
| Post-shipment - Implements entity insert endpoint for Shipment. | Implements entity insert endpoint for Shipment. Implements entity insert endpoint for Shipment.  |
| Post-shipment-charge - Implements entity insert endpoint for ShipmentChar... | Implements entity insert endpoint for ShipmentCharge. Implements entity insert endpoint for ShipmentCharge.  |
| Post-shipment-comment - Implements entity insert endpoint for ShipmentComm... | Implements entity insert endpoint for ShipmentComment. Implements entity insert endpoint for ShipmentComment.  |
| Post-shipment-ref - Implements entity insert endpoint for ShipmentRef. | Implements entity insert endpoint for ShipmentRef. Implements entity insert endpoint for ShipmentRef.  |
| Post-system-metric - Implements entity insert endpoint for SystemMetric... | Implements entity insert endpoint for SystemMetric. Implements entity insert endpoint for SystemMetric.  |
| Post-trigger-definition - Implements entity insert endpoint for TriggerDefin... | Implements entity insert endpoint for TriggerDefinition. Implements entity insert endpoint for TriggerDefinition.  |
| Post-user - Implements entity insert endpoint for User. | Implements entity insert endpoint for User. Implements entity insert endpoint for User.  |
| Post-user-role - Implements entity insert endpoint for UserRole. | Implements entity insert endpoint for UserRole. Implements entity insert endpoint for UserRole.  |
| Post-user-view - Implements view query endpoint for UserView. | Implements view query endpoint for UserView. Implements view query endpoint for UserView.  |
| Post-vendor - Implements entity insert endpoint for Vendor. | Implements entity insert endpoint for Vendor. Implements entity insert endpoint for Vendor.  |
| Post-vendor-configuration - Implements entity insert endpoint for VendorConfig... | Implements entity insert endpoint for VendorConfiguration. Implements entity insert endpoint for VendorConfiguration.  |
| Post-voucher - Implements entity insert endpoint for Voucher. | Implements entity insert endpoint for Voucher. Implements entity insert endpoint for Voucher.  |
| Post-webhook-definition - Implements entity insert endpoint for WebhookDefin... | Implements entity insert endpoint for WebhookDefinition. Implements entity insert endpoint for WebhookDefinition.  |
| Post-workflow-definition - Implements entity insert endpoint for WorkflowDefi... | Implements entity insert endpoint for WorkflowDefinition. Implements entity insert endpoint for WorkflowDefinition.  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Delete-accessorial-code - Implements entity delete endpoint for AccessorialC... | Implements entity delete endpoint for AccessorialCode. Implements entity delete endpoint for AccessorialCode.  |
| Delete-action-definition - Implements entity delete endpoint for ActionDefini... | Implements entity delete endpoint for ActionDefinition. Implements entity delete endpoint for ActionDefinition.  |
| Delete-allocated-charge - Implements entity delete endpoint for AllocatedCha... | Implements entity delete endpoint for AllocatedCharge. Implements entity delete endpoint for AllocatedCharge.  |
| Delete-allocated-cost - Implements entity delete endpoint for AllocatedCos... | Implements entity delete endpoint for AllocatedCost. Implements entity delete endpoint for AllocatedCost.  |
| Delete-authority - Implements entity delete endpoint for Authority. | Implements entity delete endpoint for Authority. Implements entity delete endpoint for Authority.  |
| Delete-authority-delete - Handles deleting an authority and all assocations ... | Handles deleting an authority and all assocations of it in the UserRole and GroupRole tables. Handles deleting an authority and all assocations of it in the UserRole and GroupRole tables.  |
| Delete-bill-to - Implements entity delete endpoint for BillTo. | Implements entity delete endpoint for BillTo. Implements entity delete endpoint for BillTo.  |
| Delete-business-type - Implements entity delete endpoint for BusinessType... | Implements entity delete endpoint for BusinessType. Implements entity delete endpoint for BusinessType.  |
| Delete-calendar - Implements entity delete endpoint for Calendar. | Implements entity delete endpoint for Calendar. Implements entity delete endpoint for Calendar.  |
| Delete-carrier - Implements entity delete endpoint for Carrier. | Implements entity delete endpoint for Carrier. Implements entity delete endpoint for Carrier.  |
| Delete-carrier-comment - Implements entity delete endpoint for CarrierComme... | Implements entity delete endpoint for CarrierComment. Implements entity delete endpoint for CarrierComment.  |
| Delete-carrier-ref - Implements entity delete endpoint for CarrierRef. | Implements entity delete endpoint for CarrierRef. Implements entity delete endpoint for CarrierRef.  |
| Delete-client-comment - Implements entity delete endpoint for ClientCommen... | Implements entity delete endpoint for ClientComment. Implements entity delete endpoint for ClientComment.  |
| Delete-client-ref - Implements entity delete endpoint for ClientRef. | Implements entity delete endpoint for ClientRef. Implements entity delete endpoint for ClientRef.  |
| Delete-commodity - Implements entity delete endpoint for Commodity. | Implements entity delete endpoint for Commodity. Implements entity delete endpoint for Commodity.  |
| Delete-connection-definition - Implements entity delete endpoint for ConnectionDe... | Implements entity delete endpoint for ConnectionDefinition. Implements entity delete endpoint for ConnectionDefinition.  |
| Delete-customer-comment - Implements entity delete endpoint for CustomerComm... | Implements entity delete endpoint for CustomerComment. Implements entity delete endpoint for CustomerComment.  |
| Delete-customer-ref - Implements entity delete endpoint for CustomerRef. | Implements entity delete endpoint for CustomerRef. Implements entity delete endpoint for CustomerRef.  |
| Delete-equipment - Implements entity delete endpoint for Equipment. | Implements entity delete endpoint for Equipment. Implements entity delete endpoint for Equipment.  |
| Delete-equipment-group-buckets - Implements entity delete endpoint for EquipmentGro... | Implements entity delete endpoint for EquipmentGroupBuckets. Implements entity delete endpoint for EquipmentGroupBuckets.  |
| Delete-error-queue - Implements entity delete endpoint for ErrorQueue. | Implements entity delete endpoint for ErrorQueue. Implements entity delete endpoint for ErrorQueue.  |
| Delete-freight-bill - Implements entity delete endpoint for FreightBill. | Implements entity delete endpoint for FreightBill. Implements entity delete endpoint for FreightBill.  |
| Delete-freight-bill-comment - Implements entity delete endpoint for FreightBillC... | Implements entity delete endpoint for FreightBillComment. Implements entity delete endpoint for FreightBillComment.  |
| Delete-freight-bill-cost-detail - Implements entity delete endpoint for FreightBillC... | Implements entity delete endpoint for FreightBillCostDetail. Implements entity delete endpoint for FreightBillCostDetail.  |
| Delete-freight-bill-geo-info - Implements entity delete endpoint for FreightBillG... | Implements entity delete endpoint for FreightBillGeoInfo. Implements entity delete endpoint for FreightBillGeoInfo.  |
| Delete-freight-bill-line-item - Implements entity delete endpoint for FreightBillL... | Implements entity delete endpoint for FreightBillLineItem. Implements entity delete endpoint for FreightBillLineItem.  |
| Delete-freight-bill-ref - Implements entity delete endpoint for FreightBillR... | Implements entity delete endpoint for FreightBillRef. Implements entity delete endpoint for FreightBillRef.  |
| Delete-group - Implements entity delete endpoint for Group. | Implements entity delete endpoint for Group. Implements entity delete endpoint for Group.  |
| Delete-group-delete - Handles deleting a group and all associations in t... | Handles deleting a group and all associations in the UserRole and GroupRole tables. Handles deleting a group and all associations in the UserRole and GroupRole tables.  |
| Delete-group-role - Implements entity delete endpoint for GroupRole. | Implements entity delete endpoint for GroupRole. Implements entity delete endpoint for GroupRole.  |
| Delete-handling-unit-type - Implements entity delete endpoint for HandlingUnit... | Implements entity delete endpoint for HandlingUnitType. Implements entity delete endpoint for HandlingUnitType.  |
| Delete-invoice - Implements entity delete endpoint for Invoice. | Implements entity delete endpoint for Invoice. Implements entity delete endpoint for Invoice.  |
| Delete-invoice-billing - Implements entity delete endpoint for InvoiceBilli... | Implements entity delete endpoint for InvoiceBilling. Implements entity delete endpoint for InvoiceBilling.  |
| Delete-invoice-comment - Implements entity delete endpoint for InvoiceComme... | Implements entity delete endpoint for InvoiceComment. Implements entity delete endpoint for InvoiceComment.  |
| Delete-invoice-ref - Implements entity delete endpoint for InvoiceRef. | Implements entity delete endpoint for InvoiceRef. Implements entity delete endpoint for InvoiceRef.  |
| Delete-item-master - Implements entity delete endpoint for ItemMaster. | Implements entity delete endpoint for ItemMaster. Implements entity delete endpoint for ItemMaster.  |
| Delete-label - Implements entity delete endpoint for Label. | Implements entity delete endpoint for Label. Implements entity delete endpoint for Label.  |
| Delete-load - Implements entity delete endpoint for Load. | Implements entity delete endpoint for Load. Implements entity delete endpoint for Load.  |
| Delete-load-charge - Implements entity delete endpoint for LoadCharge. | Implements entity delete endpoint for LoadCharge. Implements entity delete endpoint for LoadCharge.  |
| Delete-load-comment - Implements entity delete endpoint for LoadComment. | Implements entity delete endpoint for LoadComment. Implements entity delete endpoint for LoadComment.  |
| Delete-load-cost - Implements entity delete endpoint for LoadCost. | Implements entity delete endpoint for LoadCost. Implements entity delete endpoint for LoadCost.  |
| Delete-load-cost-option - Implements entity delete endpoint for LoadCostOpti... | Implements entity delete endpoint for LoadCostOption. Implements entity delete endpoint for LoadCostOption.  |
| Delete-load-order - Implements entity delete endpoint for LoadOrder. | Implements entity delete endpoint for LoadOrder. Implements entity delete endpoint for LoadOrder.  |
| Delete-load-ref - Implements entity delete endpoint for LoadRef. | Implements entity delete endpoint for LoadRef. Implements entity delete endpoint for LoadRef.  |
| Delete-load-stop - Implements entity delete endpoint for LoadStop. | Implements entity delete endpoint for LoadStop. Implements entity delete endpoint for LoadStop.  |
| Delete-location - Implements entity delete endpoint for Location. | Implements entity delete endpoint for Location. Implements entity delete endpoint for Location.  |
| Delete-master-location - Implements entity delete endpoint for MasterLocati... | Implements entity delete endpoint for MasterLocation. Implements entity delete endpoint for MasterLocation.  |
| Delete-order - Implements entity delete endpoint for Order. | Implements entity delete endpoint for Order. Implements entity delete endpoint for Order.  |
| Delete-order-charge - Implements entity delete endpoint for OrderCharge. | Implements entity delete endpoint for OrderCharge. Implements entity delete endpoint for OrderCharge.  |
| Delete-order-comment - Implements entity delete endpoint for OrderComment... | Implements entity delete endpoint for OrderComment. Implements entity delete endpoint for OrderComment.  |
| Delete-order-leg - Implements entity delete endpoint for OrderLeg. | Implements entity delete endpoint for OrderLeg. Implements entity delete endpoint for OrderLeg.  |
| Delete-order-line - Implements entity delete endpoint for OrderLine. | Implements entity delete endpoint for OrderLine. Implements entity delete endpoint for OrderLine.  |
| Delete-order-ref - Implements entity delete endpoint for OrderRef. | Implements entity delete endpoint for OrderRef. Implements entity delete endpoint for OrderRef.  |
| Delete-organization - Implements entity delete endpoint for Organization... | Implements entity delete endpoint for Organization. Implements entity delete endpoint for Organization.  |
| Delete-partner - Implements entity delete endpoint for Partner. | Implements entity delete endpoint for Partner. Implements entity delete endpoint for Partner.  |
| Delete-payment - Implements entity delete endpoint for Payment. | Implements entity delete endpoint for Payment. Implements entity delete endpoint for Payment.  |
| Delete-priority - Implements entity delete endpoint for Priority. | Implements entity delete endpoint for Priority. Implements entity delete endpoint for Priority.  |
| Delete-quote - Implements entity delete endpoint for Quote. | Implements entity delete endpoint for Quote. Implements entity delete endpoint for Quote.  |
| Delete-route - Implements entity delete endpoint for Route. | Implements entity delete endpoint for Route. Implements entity delete endpoint for Route.  |
| Delete-service - Implements entity delete endpoint for Service. | Implements entity delete endpoint for Service. Implements entity delete endpoint for Service.  |
| Delete-shipment - Implements entity delete endpoint for Shipment. | Implements entity delete endpoint for Shipment. Implements entity delete endpoint for Shipment.  |
| Delete-shipment-charge - Implements entity delete endpoint for ShipmentChar... | Implements entity delete endpoint for ShipmentCharge. Implements entity delete endpoint for ShipmentCharge.  |
| Delete-shipment-comment - Implements entity delete endpoint for ShipmentComm... | Implements entity delete endpoint for ShipmentComment. Implements entity delete endpoint for ShipmentComment.  |
| Delete-shipment-ref - Implements entity delete endpoint for ShipmentRef. | Implements entity delete endpoint for ShipmentRef. Implements entity delete endpoint for ShipmentRef.  |
| Delete-system-metric - Implements entity delete endpoint for SystemMetric... | Implements entity delete endpoint for SystemMetric. Implements entity delete endpoint for SystemMetric.  |
| Delete-trigger-definition - Implements entity delete endpoint for TriggerDefin... | Implements entity delete endpoint for TriggerDefinition. Implements entity delete endpoint for TriggerDefinition.  |
| Delete-user - Implements entity delete endpoint for User. | Implements entity delete endpoint for User. Implements entity delete endpoint for User.  |
| Delete-user-role - Implements entity delete endpoint for UserRole. | Implements entity delete endpoint for UserRole. Implements entity delete endpoint for UserRole.  |
| Delete-vendor - Implements entity delete endpoint for Vendor. | Implements entity delete endpoint for Vendor. Implements entity delete endpoint for Vendor.  |
| Delete-vendor-configuration - Implements entity delete endpoint for VendorConfig... | Implements entity delete endpoint for VendorConfiguration. Implements entity delete endpoint for VendorConfiguration.  |
| Delete-voucher - Implements entity delete endpoint for Voucher. | Implements entity delete endpoint for Voucher. Implements entity delete endpoint for Voucher.  |
| Delete-webhook-definition - Implements entity delete endpoint for WebhookDefin... | Implements entity delete endpoint for WebhookDefinition. Implements entity delete endpoint for WebhookDefinition.  |
| Delete-workflow-definition - Implements entity delete endpoint for WorkflowDefi... | Implements entity delete endpoint for WorkflowDefinition. Implements entity delete endpoint for WorkflowDefinition.  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Get-accessorial-code - Implements entity retrieval endpoint for Accessori... | Implements entity retrieval endpoint for AccessorialCode. Implements entity retrieval endpoint for AccessorialCode.  |
| Get-accessorial-code-full - Implements entity full retrieval endpoint for Acce... | Implements entity full retrieval endpoint for AccessorialCode. Implements entity full retrieval endpoint for AccessorialCode.  |
| Get-action-definition - Implements entity retrieval endpoint for ActionDef... | Implements entity retrieval endpoint for ActionDefinition. Implements entity retrieval endpoint for ActionDefinition.  |
| Get-action-definition-full - Implements entity full retrieval endpoint for Acti... | Implements entity full retrieval endpoint for ActionDefinition. Implements entity full retrieval endpoint for ActionDefinition.  |
| Get-allocated-charge - Implements entity retrieval endpoint for Allocated... | Implements entity retrieval endpoint for AllocatedCharge. Implements entity retrieval endpoint for AllocatedCharge.  |
| Get-allocated-charge-full - Implements entity full retrieval endpoint for Allo... | Implements entity full retrieval endpoint for AllocatedCharge. Implements entity full retrieval endpoint for AllocatedCharge.  |
| Get-allocated-cost - Implements entity retrieval endpoint for Allocated... | Implements entity retrieval endpoint for AllocatedCost. Implements entity retrieval endpoint for AllocatedCost.  |
| Get-allocated-cost-full - Implements entity full retrieval endpoint for Allo... | Implements entity full retrieval endpoint for AllocatedCost. Implements entity full retrieval endpoint for AllocatedCost.  |
| Get-authority - Implements entity retrieval endpoint for Authority... | Implements entity retrieval endpoint for Authority. Implements entity retrieval endpoint for Authority.  |
| Get-authority-full - Implements entity full retrieval endpoint for Auth... | Implements entity full retrieval endpoint for Authority. Implements entity full retrieval endpoint for Authority.  |
| Get-bill-to - Implements entity retrieval endpoint for BillTo. | Implements entity retrieval endpoint for BillTo. Implements entity retrieval endpoint for BillTo.  |
| Get-bill-to-full - Implements entity full retrieval endpoint for Bill... | Implements entity full retrieval endpoint for BillTo. Implements entity full retrieval endpoint for BillTo.  |
| Get-business-type - Implements entity retrieval endpoint for BusinessT... | Implements entity retrieval endpoint for BusinessType. Implements entity retrieval endpoint for BusinessType.  |
| Get-business-type-full - Implements entity full retrieval endpoint for Busi... | Implements entity full retrieval endpoint for BusinessType. Implements entity full retrieval endpoint for BusinessType.  |
| Get-calendar - Implements entity retrieval endpoint for Calendar. | Implements entity retrieval endpoint for Calendar. Implements entity retrieval endpoint for Calendar.  |
| Get-calendar-full - Implements entity full retrieval endpoint for Cale... | Implements entity full retrieval endpoint for Calendar. Implements entity full retrieval endpoint for Calendar.  |
| Get-carrier - Implements entity retrieval endpoint for Carrier. | Implements entity retrieval endpoint for Carrier. Implements entity retrieval endpoint for Carrier.  |
| Get-carrier-comment - Implements entity retrieval endpoint for CarrierCo... | Implements entity retrieval endpoint for CarrierComment. Implements entity retrieval endpoint for CarrierComment.  |
| Get-carrier-comment-full - Implements entity full retrieval endpoint for Carr... | Implements entity full retrieval endpoint for CarrierComment. Implements entity full retrieval endpoint for CarrierComment.  |
| Get-carrier-full - Implements entity full retrieval endpoint for Carr... | Implements entity full retrieval endpoint for Carrier. Implements entity full retrieval endpoint for Carrier.  |
| Get-carrier-ref - Implements entity retrieval endpoint for CarrierRe... | Implements entity retrieval endpoint for CarrierRef. Implements entity retrieval endpoint for CarrierRef.  |
| Get-carrier-ref-full - Implements entity full retrieval endpoint for Carr... | Implements entity full retrieval endpoint for CarrierRef. Implements entity full retrieval endpoint for CarrierRef.  |
| Get-client-comment - Implements entity retrieval endpoint for ClientCom... | Implements entity retrieval endpoint for ClientComment. Implements entity retrieval endpoint for ClientComment.  |
| Get-client-comment-full - Implements entity full retrieval endpoint for Clie... | Implements entity full retrieval endpoint for ClientComment. Implements entity full retrieval endpoint for ClientComment.  |
| Get-client-ref - Implements entity retrieval endpoint for ClientRef... | Implements entity retrieval endpoint for ClientRef. Implements entity retrieval endpoint for ClientRef.  |
| Get-client-ref-full - Implements entity full retrieval endpoint for Clie... | Implements entity full retrieval endpoint for ClientRef. Implements entity full retrieval endpoint for ClientRef.  |
| Get-commodity - Implements entity retrieval endpoint for Commodity... | Implements entity retrieval endpoint for Commodity. Implements entity retrieval endpoint for Commodity.  |
| Get-commodity-full - Implements entity full retrieval endpoint for Comm... | Implements entity full retrieval endpoint for Commodity. Implements entity full retrieval endpoint for Commodity.  |
| Get-connection-definition - Implements entity retrieval endpoint for Connectio... | Implements entity retrieval endpoint for ConnectionDefinition. Implements entity retrieval endpoint for ConnectionDefinition.  |
| Get-connection-definition-full - Implements entity full retrieval endpoint for Conn... | Implements entity full retrieval endpoint for ConnectionDefinition. Implements entity full retrieval endpoint for ConnectionDefinition.  |
| Get-customer-comment - Implements entity retrieval endpoint for CustomerC... | Implements entity retrieval endpoint for CustomerComment. Implements entity retrieval endpoint for CustomerComment.  |
| Get-customer-comment-full - Implements entity full retrieval endpoint for Cust... | Implements entity full retrieval endpoint for CustomerComment. Implements entity full retrieval endpoint for CustomerComment.  |
| Get-customer-ref - Implements entity retrieval endpoint for CustomerR... | Implements entity retrieval endpoint for CustomerRef. Implements entity retrieval endpoint for CustomerRef.  |
| Get-customer-ref-full - Implements entity full retrieval endpoint for Cust... | Implements entity full retrieval endpoint for CustomerRef. Implements entity full retrieval endpoint for CustomerRef.  |
| Get-equipment - Implements entity retrieval endpoint for Equipment... | Implements entity retrieval endpoint for Equipment. Implements entity retrieval endpoint for Equipment.  |
| Get-equipment-full - Implements entity full retrieval endpoint for Equi... | Implements entity full retrieval endpoint for Equipment. Implements entity full retrieval endpoint for Equipment.  |
| Get-equipment-group-buckets - Implements entity retrieval endpoint for Equipment... | Implements entity retrieval endpoint for EquipmentGroupBuckets. Implements entity retrieval endpoint for EquipmentGroupBuckets.  |
| Get-equipment-group-buckets-full - Implements entity full retrieval endpoint for Equi... | Implements entity full retrieval endpoint for EquipmentGroupBuckets. Implements entity full retrieval endpoint for EquipmentGroupBuckets.  |
| Get-error-queue - Implements entity retrieval endpoint for ErrorQueu... | Implements entity retrieval endpoint for ErrorQueue. Implements entity retrieval endpoint for ErrorQueue.  |
| Get-error-queue-full - Implements entity full retrieval endpoint for Erro... | Implements entity full retrieval endpoint for ErrorQueue. Implements entity full retrieval endpoint for ErrorQueue.  |
| Get-freight-bill - Implements entity retrieval endpoint for FreightBi... | Implements entity retrieval endpoint for FreightBill. Implements entity retrieval endpoint for FreightBill.  |
| Get-freight-bill-comment - Implements entity retrieval endpoint for FreightBi... | Implements entity retrieval endpoint for FreightBillComment. Implements entity retrieval endpoint for FreightBillComment.  |
| Get-freight-bill-comment-full - Implements entity full retrieval endpoint for Frei... | Implements entity full retrieval endpoint for FreightBillComment. Implements entity full retrieval endpoint for FreightBillComment.  |
| Get-freight-bill-cost-detail - Implements entity retrieval endpoint for FreightBi... | Implements entity retrieval endpoint for FreightBillCostDetail. Implements entity retrieval endpoint for FreightBillCostDetail.  |
| Get-freight-bill-cost-detail-full - Implements entity full retrieval endpoint for Frei... | Implements entity full retrieval endpoint for FreightBillCostDetail. Implements entity full retrieval endpoint for FreightBillCostDetail.  |
| Get-freight-bill-full - Implements entity full retrieval endpoint for Frei... | Implements entity full retrieval endpoint for FreightBill. Implements entity full retrieval endpoint for FreightBill.  |
| Get-freight-bill-geo-info - Implements entity retrieval endpoint for FreightBi... | Implements entity retrieval endpoint for FreightBillGeoInfo. Implements entity retrieval endpoint for FreightBillGeoInfo.  |
| Get-freight-bill-geo-info-full - Implements entity full retrieval endpoint for Frei... | Implements entity full retrieval endpoint for FreightBillGeoInfo. Implements entity full retrieval endpoint for FreightBillGeoInfo.  |
| Get-freight-bill-line-item - Implements entity retrieval endpoint for FreightBi... | Implements entity retrieval endpoint for FreightBillLineItem. Implements entity retrieval endpoint for FreightBillLineItem.  |
| Get-freight-bill-line-item-full - Implements entity full retrieval endpoint for Frei... | Implements entity full retrieval endpoint for FreightBillLineItem. Implements entity full retrieval endpoint for FreightBillLineItem.  |
| Get-freight-bill-ref - Implements entity retrieval endpoint for FreightBi... | Implements entity retrieval endpoint for FreightBillRef. Implements entity retrieval endpoint for FreightBillRef.  |
| Get-freight-bill-ref-full - Implements entity full retrieval endpoint for Frei... | Implements entity full retrieval endpoint for FreightBillRef. Implements entity full retrieval endpoint for FreightBillRef.  |
| Get-group - Implements entity retrieval endpoint for Group. | Implements entity retrieval endpoint for Group. Implements entity retrieval endpoint for Group.  |
| Get-group-full - Implements entity full retrieval endpoint for Grou... | Implements entity full retrieval endpoint for Group. Implements entity full retrieval endpoint for Group.  |
| Get-group-role - Implements entity retrieval endpoint for GroupRole... | Implements entity retrieval endpoint for GroupRole. Implements entity retrieval endpoint for GroupRole.  |
| Get-group-role-full - Implements entity full retrieval endpoint for Grou... | Implements entity full retrieval endpoint for GroupRole. Implements entity full retrieval endpoint for GroupRole.  |
| Get-group-roles - Handles retrieving all the roles associated to the... | Handles retrieving all the roles associated to the given group id. Handles retrieving all the roles associated to the given group id.  |
| Get-handling-unit-type - Implements entity retrieval endpoint for HandlingU... | Implements entity retrieval endpoint for HandlingUnitType. Implements entity retrieval endpoint for HandlingUnitType.  |
| Get-handling-unit-type-full - Implements entity full retrieval endpoint for Hand... | Implements entity full retrieval endpoint for HandlingUnitType. Implements entity full retrieval endpoint for HandlingUnitType.  |
| Get-invoice - Implements entity retrieval endpoint for Invoice. | Implements entity retrieval endpoint for Invoice. Implements entity retrieval endpoint for Invoice.  |
| Get-invoice-billing - Implements entity retrieval endpoint for InvoiceBi... | Implements entity retrieval endpoint for InvoiceBilling. Implements entity retrieval endpoint for InvoiceBilling.  |
| Get-invoice-billing-full - Implements entity full retrieval endpoint for Invo... | Implements entity full retrieval endpoint for InvoiceBilling. Implements entity full retrieval endpoint for InvoiceBilling.  |
| Get-invoice-comment - Implements entity retrieval endpoint for InvoiceCo... | Implements entity retrieval endpoint for InvoiceComment. Implements entity retrieval endpoint for InvoiceComment.  |
| Get-invoice-comment-full - Implements entity full retrieval endpoint for Invo... | Implements entity full retrieval endpoint for InvoiceComment. Implements entity full retrieval endpoint for InvoiceComment.  |
| Get-invoice-full - Implements entity full retrieval endpoint for Invo... | Implements entity full retrieval endpoint for Invoice. Implements entity full retrieval endpoint for Invoice.  |
| Get-invoice-ref - Implements entity retrieval endpoint for InvoiceRe... | Implements entity retrieval endpoint for InvoiceRef. Implements entity retrieval endpoint for InvoiceRef.  |
| Get-invoice-ref-full - Implements entity full retrieval endpoint for Invo... | Implements entity full retrieval endpoint for InvoiceRef. Implements entity full retrieval endpoint for InvoiceRef.  |
| Get-item-master - Implements entity retrieval endpoint for ItemMaste... | Implements entity retrieval endpoint for ItemMaster. Implements entity retrieval endpoint for ItemMaster.  |
| Get-item-master-full - Implements entity full retrieval endpoint for Item... | Implements entity full retrieval endpoint for ItemMaster. Implements entity full retrieval endpoint for ItemMaster.  |
| Get-label - Implements entity retrieval endpoint for Label. | Implements entity retrieval endpoint for Label. Implements entity retrieval endpoint for Label.  |
| Get-label-full - Implements entity full retrieval endpoint for Labe... | Implements entity full retrieval endpoint for Label. Implements entity full retrieval endpoint for Label.  |
| Get-load - Implements entity retrieval endpoint for Load. | Implements entity retrieval endpoint for Load. Implements entity retrieval endpoint for Load.  |
| Get-load-charge - Implements entity retrieval endpoint for LoadCharg... | Implements entity retrieval endpoint for LoadCharge. Implements entity retrieval endpoint for LoadCharge.  |
| Get-load-charge-full - Implements entity full retrieval endpoint for Load... | Implements entity full retrieval endpoint for LoadCharge. Implements entity full retrieval endpoint for LoadCharge.  |
| Get-load-comment - Implements entity retrieval endpoint for LoadComme... | Implements entity retrieval endpoint for LoadComment. Implements entity retrieval endpoint for LoadComment.  |
| Get-load-comment-full - Implements entity full retrieval endpoint for Load... | Implements entity full retrieval endpoint for LoadComment. Implements entity full retrieval endpoint for LoadComment.  |
| Get-load-cost - Implements entity retrieval endpoint for LoadCost. | Implements entity retrieval endpoint for LoadCost. Implements entity retrieval endpoint for LoadCost.  |
| Get-load-cost-full - Implements entity full retrieval endpoint for Load... | Implements entity full retrieval endpoint for LoadCost. Implements entity full retrieval endpoint for LoadCost.  |
| Get-load-cost-option - Implements entity retrieval endpoint for LoadCostO... | Implements entity retrieval endpoint for LoadCostOption. Implements entity retrieval endpoint for LoadCostOption.  |
| Get-load-cost-option-full - Implements entity full retrieval endpoint for Load... | Implements entity full retrieval endpoint for LoadCostOption. Implements entity full retrieval endpoint for LoadCostOption.  |
| Get-load-full - Implements entity full retrieval endpoint for Load... | Implements entity full retrieval endpoint for Load. Implements entity full retrieval endpoint for Load.  |
| Get-load-order - Implements entity retrieval endpoint for LoadOrder... | Implements entity retrieval endpoint for LoadOrder. Implements entity retrieval endpoint for LoadOrder.  |
| Get-load-order-full - Implements entity full retrieval endpoint for Load... | Implements entity full retrieval endpoint for LoadOrder. Implements entity full retrieval endpoint for LoadOrder.  |
| Get-load-ref - Implements entity retrieval endpoint for LoadRef. | Implements entity retrieval endpoint for LoadRef. Implements entity retrieval endpoint for LoadRef.  |
| Get-load-ref-full - Implements entity full retrieval endpoint for Load... | Implements entity full retrieval endpoint for LoadRef. Implements entity full retrieval endpoint for LoadRef.  |
| Get-load-stop - Implements entity retrieval endpoint for LoadStop. | Implements entity retrieval endpoint for LoadStop. Implements entity retrieval endpoint for LoadStop.  |
| Get-load-stop-full - Implements entity full retrieval endpoint for Load... | Implements entity full retrieval endpoint for LoadStop. Implements entity full retrieval endpoint for LoadStop.  |
| Get-location - Implements entity retrieval endpoint for Location. | Implements entity retrieval endpoint for Location. Implements entity retrieval endpoint for Location.  |
| Get-location-full - Implements entity full retrieval endpoint for Loca... | Implements entity full retrieval endpoint for Location. Implements entity full retrieval endpoint for Location.  |
| Get-master-location - Implements entity retrieval endpoint for MasterLoc... | Implements entity retrieval endpoint for MasterLocation. Implements entity retrieval endpoint for MasterLocation.  |
| Get-master-location-full - Implements entity full retrieval endpoint for Mast... | Implements entity full retrieval endpoint for MasterLocation. Implements entity full retrieval endpoint for MasterLocation.  |
| Get-order - Implements entity retrieval endpoint for Order. | Implements entity retrieval endpoint for Order. Implements entity retrieval endpoint for Order.  |
| Get-order-charge - Implements entity retrieval endpoint for OrderChar... | Implements entity retrieval endpoint for OrderCharge. Implements entity retrieval endpoint for OrderCharge.  |
| Get-order-charge-full - Implements entity full retrieval endpoint for Orde... | Implements entity full retrieval endpoint for OrderCharge. Implements entity full retrieval endpoint for OrderCharge.  |
| Get-order-comment - Implements entity retrieval endpoint for OrderComm... | Implements entity retrieval endpoint for OrderComment. Implements entity retrieval endpoint for OrderComment.  |
| Get-order-comment-full - Implements entity full retrieval endpoint for Orde... | Implements entity full retrieval endpoint for OrderComment. Implements entity full retrieval endpoint for OrderComment.  |
| Get-order-full - Implements entity full retrieval endpoint for Orde... | Implements entity full retrieval endpoint for Order. Implements entity full retrieval endpoint for Order.  |
| Get-order-leg - Implements entity retrieval endpoint for OrderLeg. | Implements entity retrieval endpoint for OrderLeg. Implements entity retrieval endpoint for OrderLeg.  |
| Get-order-leg-full - Implements entity full retrieval endpoint for Orde... | Implements entity full retrieval endpoint for OrderLeg. Implements entity full retrieval endpoint for OrderLeg.  |
| Get-order-line - Implements entity retrieval endpoint for OrderLine... | Implements entity retrieval endpoint for OrderLine. Implements entity retrieval endpoint for OrderLine.  |
| Get-order-line-full - Implements entity full retrieval endpoint for Orde... | Implements entity full retrieval endpoint for OrderLine. Implements entity full retrieval endpoint for OrderLine.  |
| Get-order-ref - Implements entity retrieval endpoint for OrderRef. | Implements entity retrieval endpoint for OrderRef. Implements entity retrieval endpoint for OrderRef.  |
| Get-order-ref-full - Implements entity full retrieval endpoint for Orde... | Implements entity full retrieval endpoint for OrderRef. Implements entity full retrieval endpoint for OrderRef.  |
| Get-organization - Implements entity retrieval endpoint for Organizat... | Implements entity retrieval endpoint for Organization. Implements entity retrieval endpoint for Organization.  |
| Get-organization-full - Implements entity full retrieval endpoint for Orga... | Implements entity full retrieval endpoint for Organization. Implements entity full retrieval endpoint for Organization.  |
| Get-partner - Implements entity retrieval endpoint for Partner. | Implements entity retrieval endpoint for Partner. Implements entity retrieval endpoint for Partner.  |
| Get-partner-full - Implements entity full retrieval endpoint for Part... | Implements entity full retrieval endpoint for Partner. Implements entity full retrieval endpoint for Partner.  |
| Get-payment - Implements entity retrieval endpoint for Payment. | Implements entity retrieval endpoint for Payment. Implements entity retrieval endpoint for Payment.  |
| Get-payment-full - Implements entity full retrieval endpoint for Paym... | Implements entity full retrieval endpoint for Payment. Implements entity full retrieval endpoint for Payment.  |
| Get-priority - Implements entity retrieval endpoint for Priority. | Implements entity retrieval endpoint for Priority. Implements entity retrieval endpoint for Priority.  |
| Get-priority-full - Implements entity full retrieval endpoint for Prio... | Implements entity full retrieval endpoint for Priority. Implements entity full retrieval endpoint for Priority.  |
| Get-quote - Implements entity retrieval endpoint for Quote. | Implements entity retrieval endpoint for Quote. Implements entity retrieval endpoint for Quote.  |
| Get-quote-full - Implements entity full retrieval endpoint for Quot... | Implements entity full retrieval endpoint for Quote. Implements entity full retrieval endpoint for Quote.  |
| Get-route - Implements entity retrieval endpoint for Route. | Implements entity retrieval endpoint for Route. Implements entity retrieval endpoint for Route.  |
| Get-route-full - Implements entity full retrieval endpoint for Rout... | Implements entity full retrieval endpoint for Route. Implements entity full retrieval endpoint for Route.  |
| Get-service - Implements entity retrieval endpoint for Service. | Implements entity retrieval endpoint for Service. Implements entity retrieval endpoint for Service.  |
| Get-service-full - Implements entity full retrieval endpoint for Serv... | Implements entity full retrieval endpoint for Service. Implements entity full retrieval endpoint for Service.  |
| Get-shipment - Implements entity retrieval endpoint for Shipment. | Implements entity retrieval endpoint for Shipment. Implements entity retrieval endpoint for Shipment.  |
| Get-shipment-charge - Implements entity retrieval endpoint for ShipmentC... | Implements entity retrieval endpoint for ShipmentCharge. Implements entity retrieval endpoint for ShipmentCharge.  |
| Get-shipment-charge-full - Implements entity full retrieval endpoint for Ship... | Implements entity full retrieval endpoint for ShipmentCharge. Implements entity full retrieval endpoint for ShipmentCharge.  |
| Get-shipment-comment - Implements entity retrieval endpoint for ShipmentC... | Implements entity retrieval endpoint for ShipmentComment. Implements entity retrieval endpoint for ShipmentComment.  |
| Get-shipment-comment-full - Implements entity full retrieval endpoint for Ship... | Implements entity full retrieval endpoint for ShipmentComment. Implements entity full retrieval endpoint for ShipmentComment.  |
| Get-shipment-full - Implements entity full retrieval endpoint for Ship... | Implements entity full retrieval endpoint for Shipment. Implements entity full retrieval endpoint for Shipment.  |
| Get-shipment-ref - Implements entity retrieval endpoint for ShipmentR... | Implements entity retrieval endpoint for ShipmentRef. Implements entity retrieval endpoint for ShipmentRef.  |
| Get-shipment-ref-full - Implements entity full retrieval endpoint for Ship... | Implements entity full retrieval endpoint for ShipmentRef. Implements entity full retrieval endpoint for ShipmentRef.  |
| Get-system-metric - Implements entity retrieval endpoint for SystemMet... | Implements entity retrieval endpoint for SystemMetric. Implements entity retrieval endpoint for SystemMetric.  |
| Get-system-metric-full - Implements entity full retrieval endpoint for Syst... | Implements entity full retrieval endpoint for SystemMetric. Implements entity full retrieval endpoint for SystemMetric.  |
| Get-trigger-definition - Implements entity retrieval endpoint for TriggerDe... | Implements entity retrieval endpoint for TriggerDefinition. Implements entity retrieval endpoint for TriggerDefinition.  |
| Get-trigger-definition-full - Implements entity full retrieval endpoint for Trig... | Implements entity full retrieval endpoint for TriggerDefinition. Implements entity full retrieval endpoint for TriggerDefinition.  |
| Get-user - Implements entity retrieval endpoint for User. | Implements entity retrieval endpoint for User. Implements entity retrieval endpoint for User.  |
| Get-user-full - Implements entity full retrieval endpoint for User... | Implements entity full retrieval endpoint for User. Implements entity full retrieval endpoint for User.  |
| Get-user-role - Implements entity retrieval endpoint for UserRole. | Implements entity retrieval endpoint for UserRole. Implements entity retrieval endpoint for UserRole.  |
| Get-user-role-full - Implements entity full retrieval endpoint for User... | Implements entity full retrieval endpoint for UserRole. Implements entity full retrieval endpoint for UserRole.  |
| Get-user-roles - Gets all the roles associated to the given userid. | Gets all the roles associated to the given userid. Gets all the roles associated to the given userid.  |
| Get-vendor - Implements entity retrieval endpoint for Vendor. | Implements entity retrieval endpoint for Vendor. Implements entity retrieval endpoint for Vendor.  |
| Get-vendor-configuration - Implements entity retrieval endpoint for VendorCon... | Implements entity retrieval endpoint for VendorConfiguration. Implements entity retrieval endpoint for VendorConfiguration.  |
| Get-vendor-configuration-full - Implements entity full retrieval endpoint for Vend... | Implements entity full retrieval endpoint for VendorConfiguration. Implements entity full retrieval endpoint for VendorConfiguration.  |
| Get-vendor-full - Implements entity full retrieval endpoint for Vend... | Implements entity full retrieval endpoint for Vendor. Implements entity full retrieval endpoint for Vendor.  |
| Get-voucher - Implements entity retrieval endpoint for Voucher. | Implements entity retrieval endpoint for Voucher. Implements entity retrieval endpoint for Voucher.  |
| Get-voucher-full - Implements entity full retrieval endpoint for Vouc... | Implements entity full retrieval endpoint for Voucher. Implements entity full retrieval endpoint for Voucher.  |
| Get-webhook-definition - Implements entity retrieval endpoint for WebhookDe... | Implements entity retrieval endpoint for WebhookDefinition. Implements entity retrieval endpoint for WebhookDefinition.  |
| Get-webhook-definition-full - Implements entity full retrieval endpoint for Webh... | Implements entity full retrieval endpoint for WebhookDefinition. Implements entity full retrieval endpoint for WebhookDefinition.  |
| Get-workflow-definition - Implements entity retrieval endpoint for WorkflowD... | Implements entity retrieval endpoint for WorkflowDefinition. Implements entity retrieval endpoint for WorkflowDefinition.  |
| Get-workflow-definition-full - Implements entity full retrieval endpoint for Work... | Implements entity full retrieval endpoint for WorkflowDefinition. Implements entity full retrieval endpoint for WorkflowDefinition.  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Get-carrier-list-simple - Implements simple entity retrieval endpoint for Ca... | Implements simple entity retrieval endpoint for Carrier. Implements simple entity retrieval endpoint for Carrier.  |
| Get-current-auth - Retrieves the current user entity object. | Retrieves the current user entity object. Retrieves the current user entity object.  |
| Get-execute-workflow - Kicks off a workflow based on the request. | Kicks off a workflow based on the request. Kicks off a workflow based on the request.  |
| Get-global-authorities - Gets all authorities for the global authorities en... | Gets all authorities for the global authorities endpoint. Gets all authorities for the global authorities endpoint.  |
| Get-global-groups - Gets all groups for the global groups endpoint. | Gets all groups for the global groups endpoint. Gets all groups for the global groups endpoint.  |
| Get-health - Implements a simple healthcheck to get the status ... | Implements a simple healthcheck to get the status of TMS services. Implements a simple healthcheck to get the status of TMS services.  |
| Get-organization-list-simple - Implements simple entity retrieval endpoint for Or... | Implements simple entity retrieval endpoint for Organization. Implements simple entity retrieval endpoint for Organization.  |
| Get-partner-list-simple - Implements simple entity retrieval endpoint for Pa... | Implements simple entity retrieval endpoint for Partner. Implements simple entity retrieval endpoint for Partner.  |
| Post-view-fields - Used to get a list of all views and their respecti... | Used to get a list of all views and their respective fields. Used to get a list of all views and their respective fields.  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Update-accessorial-code - Implements entity update endpoint for AccessorialC... (PUT) | Implements entity update endpoint for AccessorialCode. Implements entity update endpoint for AccessorialCode.  |
| Update-action-definition - Implements entity update endpoint for ActionDefini... (PUT) | Implements entity update endpoint for ActionDefinition. Implements entity update endpoint for ActionDefinition.  |
| Update-allocated-charge - Implements entity update endpoint for AllocatedCha... (PUT) | Implements entity update endpoint for AllocatedCharge. Implements entity update endpoint for AllocatedCharge.  |
| Update-allocated-cost - Implements entity update endpoint for AllocatedCos... (PUT) | Implements entity update endpoint for AllocatedCost. Implements entity update endpoint for AllocatedCost.  |
| Update-authority - Implements entity update endpoint for Authority. (PUT) | Implements entity update endpoint for Authority. Implements entity update endpoint for Authority.  |
| Update-bill-to - Implements entity update endpoint for BillTo. (PUT) | Implements entity update endpoint for BillTo. Implements entity update endpoint for BillTo.  |
| Update-business-type - Implements entity update endpoint for BusinessType... (PUT) | Implements entity update endpoint for BusinessType. Implements entity update endpoint for BusinessType.  |
| Update-calendar - Implements entity update endpoint for Calendar. (PUT) | Implements entity update endpoint for Calendar. Implements entity update endpoint for Calendar.  |
| Update-carrier - Implements entity update endpoint for Carrier. (PUT) | Implements entity update endpoint for Carrier. Implements entity update endpoint for Carrier.  |
| Update-carrier-comment - Implements entity update endpoint for CarrierComme... (PUT) | Implements entity update endpoint for CarrierComment. Implements entity update endpoint for CarrierComment.  |
| Update-carrier-ref - Implements entity update endpoint for CarrierRef. (PUT) | Implements entity update endpoint for CarrierRef. Implements entity update endpoint for CarrierRef.  |
| Update-client-comment - Implements entity update endpoint for ClientCommen... (PUT) | Implements entity update endpoint for ClientComment. Implements entity update endpoint for ClientComment.  |
| Update-client-ref - Implements entity update endpoint for ClientRef. (PUT) | Implements entity update endpoint for ClientRef. Implements entity update endpoint for ClientRef.  |
| Update-commodity - Implements entity update endpoint for Commodity. (PUT) | Implements entity update endpoint for Commodity. Implements entity update endpoint for Commodity.  |
| Update-connection-definition - Implements entity update endpoint for ConnectionDe... (PUT) | Implements entity update endpoint for ConnectionDefinition. Implements entity update endpoint for ConnectionDefinition.  |
| Update-customer-comment - Implements entity update endpoint for CustomerComm... (PUT) | Implements entity update endpoint for CustomerComment. Implements entity update endpoint for CustomerComment.  |
| Update-customer-ref - Implements entity update endpoint for CustomerRef. (PUT) | Implements entity update endpoint for CustomerRef. Implements entity update endpoint for CustomerRef.  |
| Update-equipment - Implements entity update endpoint for Equipment. (PUT) | Implements entity update endpoint for Equipment. Implements entity update endpoint for Equipment.  |
| Update-equipment-group-buckets - Implements entity update endpoint for EquipmentGro... (PUT) | Implements entity update endpoint for EquipmentGroupBuckets. Implements entity update endpoint for EquipmentGroupBuckets.  |
| Update-error-queue - Implements entity update endpoint for ErrorQueue. (PUT) | Implements entity update endpoint for ErrorQueue. Implements entity update endpoint for ErrorQueue.  |
| Update-freight-bill - Implements entity update endpoint for FreightBill. (PUT) | Implements entity update endpoint for FreightBill. Implements entity update endpoint for FreightBill.  |
| Update-freight-bill-comment - Implements entity update endpoint for FreightBillC... (PUT) | Implements entity update endpoint for FreightBillComment. Implements entity update endpoint for FreightBillComment.  |
| Update-freight-bill-cost-detail - Implements entity update endpoint for FreightBillC... (PUT) | Implements entity update endpoint for FreightBillCostDetail. Implements entity update endpoint for FreightBillCostDetail.  |
| Update-freight-bill-geo-info - Implements entity update endpoint for FreightBillG... (PUT) | Implements entity update endpoint for FreightBillGeoInfo. Implements entity update endpoint for FreightBillGeoInfo.  |
| Update-freight-bill-line-item - Implements entity update endpoint for FreightBillL... (PUT) | Implements entity update endpoint for FreightBillLineItem. Implements entity update endpoint for FreightBillLineItem.  |
| Update-freight-bill-ref - Implements entity update endpoint for FreightBillR... (PUT) | Implements entity update endpoint for FreightBillRef. Implements entity update endpoint for FreightBillRef.  |
| Update-group - Implements entity update endpoint for Group. (PUT) | Implements entity update endpoint for Group. Implements entity update endpoint for Group.  |
| Update-group-role - Implements entity update endpoint for GroupRole. (PUT) | Implements entity update endpoint for GroupRole. Implements entity update endpoint for GroupRole.  |
| Update-group-roles-modify - Handles modifying roles for a group. (PUT) | Handles modifying roles for a group. Handles modifying roles for a group.  |
| Update-handling-unit-type - Implements entity update endpoint for HandlingUnit... (PUT) | Implements entity update endpoint for HandlingUnitType. Implements entity update endpoint for HandlingUnitType.  |
| Update-invoice - Implements entity update endpoint for Invoice. (PUT) | Implements entity update endpoint for Invoice. Implements entity update endpoint for Invoice.  |
| Update-invoice-billing - Implements entity update endpoint for InvoiceBilli... (PUT) | Implements entity update endpoint for InvoiceBilling. Implements entity update endpoint for InvoiceBilling.  |
| Update-invoice-comment - Implements entity update endpoint for InvoiceComme... (PUT) | Implements entity update endpoint for InvoiceComment. Implements entity update endpoint for InvoiceComment.  |
| Update-invoice-ref - Implements entity update endpoint for InvoiceRef. (PUT) | Implements entity update endpoint for InvoiceRef. Implements entity update endpoint for InvoiceRef.  |
| Update-item-master - Implements entity update endpoint for ItemMaster. (PUT) | Implements entity update endpoint for ItemMaster. Implements entity update endpoint for ItemMaster.  |
| Update-label - Implements entity update endpoint for Label. (PUT) | Implements entity update endpoint for Label. Implements entity update endpoint for Label.  |
| Update-load - Implements entity update endpoint for Load. (PUT) | Implements entity update endpoint for Load. Implements entity update endpoint for Load.  |
| Update-load-charge - Implements entity update endpoint for LoadCharge. (PUT) | Implements entity update endpoint for LoadCharge. Implements entity update endpoint for LoadCharge.  |
| Update-load-comment - Implements entity update endpoint for LoadComment. (PUT) | Implements entity update endpoint for LoadComment. Implements entity update endpoint for LoadComment.  |
| Update-load-cost - Implements entity update endpoint for LoadCost. (PUT) | Implements entity update endpoint for LoadCost. Implements entity update endpoint for LoadCost.  |
| Update-load-cost-option - Implements entity update endpoint for LoadCostOpti... (PUT) | Implements entity update endpoint for LoadCostOption. Implements entity update endpoint for LoadCostOption.  |
| Update-load-order - Implements entity update endpoint for LoadOrder. (PUT) | Implements entity update endpoint for LoadOrder. Implements entity update endpoint for LoadOrder.  |
| Update-load-ref - Implements entity update endpoint for LoadRef. (PUT) | Implements entity update endpoint for LoadRef. Implements entity update endpoint for LoadRef.  |
| Update-load-stop - Implements entity update endpoint for LoadStop. (PUT) | Implements entity update endpoint for LoadStop. Implements entity update endpoint for LoadStop.  |
| Update-location - Implements entity update endpoint for Location. (PUT) | Implements entity update endpoint for Location. Implements entity update endpoint for Location.  |
| Update-master-location - Implements entity update endpoint for MasterLocati... (PUT) | Implements entity update endpoint for MasterLocation. Implements entity update endpoint for MasterLocation.  |
| Update-order - Implements entity update endpoint for Order. (PUT) | Implements entity update endpoint for Order. Implements entity update endpoint for Order.  |
| Update-order-charge - Implements entity update endpoint for OrderCharge. (PUT) | Implements entity update endpoint for OrderCharge. Implements entity update endpoint for OrderCharge.  |
| Update-order-comment - Implements entity update endpoint for OrderComment... (PUT) | Implements entity update endpoint for OrderComment. Implements entity update endpoint for OrderComment.  |
| Update-order-leg - Implements entity update endpoint for OrderLeg. (PUT) | Implements entity update endpoint for OrderLeg. Implements entity update endpoint for OrderLeg.  |
| Update-order-line - Implements entity update endpoint for OrderLine. (PUT) | Implements entity update endpoint for OrderLine. Implements entity update endpoint for OrderLine.  |
| Update-order-ref - Implements entity update endpoint for OrderRef. (PUT) | Implements entity update endpoint for OrderRef. Implements entity update endpoint for OrderRef.  |
| Update-organization - Implements entity update endpoint for Organization... (PUT) | Implements entity update endpoint for Organization. Implements entity update endpoint for Organization.  |
| Update-partner - Implements entity update endpoint for Partner. (PUT) | Implements entity update endpoint for Partner. Implements entity update endpoint for Partner.  |
| Update-payment - Implements entity update endpoint for Payment. (PUT) | Implements entity update endpoint for Payment. Implements entity update endpoint for Payment.  |
| Update-priority - Implements entity update endpoint for Priority. (PUT) | Implements entity update endpoint for Priority. Implements entity update endpoint for Priority.  |
| Update-quote - Implements entity update endpoint for Quote. (PUT) | Implements entity update endpoint for Quote. Implements entity update endpoint for Quote.  |
| Update-route - Implements entity update endpoint for Route. (PUT) | Implements entity update endpoint for Route. Implements entity update endpoint for Route.  |
| Update-service - Implements entity update endpoint for Service. (PUT) | Implements entity update endpoint for Service. Implements entity update endpoint for Service.  |
| Update-shipment - Implements entity update endpoint for Shipment. (PUT) | Implements entity update endpoint for Shipment. Implements entity update endpoint for Shipment.  |
| Update-shipment-charge - Implements entity update endpoint for ShipmentChar... (PUT) | Implements entity update endpoint for ShipmentCharge. Implements entity update endpoint for ShipmentCharge.  |
| Update-shipment-comment - Implements entity update endpoint for ShipmentComm... (PUT) | Implements entity update endpoint for ShipmentComment. Implements entity update endpoint for ShipmentComment.  |
| Update-shipment-ref - Implements entity update endpoint for ShipmentRef. (PUT) | Implements entity update endpoint for ShipmentRef. Implements entity update endpoint for ShipmentRef.  |
| Update-system-metric - Implements entity update endpoint for SystemMetric... (PUT) | Implements entity update endpoint for SystemMetric. Implements entity update endpoint for SystemMetric.  |
| Update-trigger-definition - Implements entity update endpoint for TriggerDefin... (PUT) | Implements entity update endpoint for TriggerDefinition. Implements entity update endpoint for TriggerDefinition.  |
| Update-user - Implements entity update endpoint for User. (PUT) | Implements entity update endpoint for User. Implements entity update endpoint for User.  |
| Update-user-role - Implements entity update endpoint for UserRole. (PUT) | Implements entity update endpoint for UserRole. Implements entity update endpoint for UserRole.  |
| Update-user-roles-modify - Handles modifying roles associated to a user. (PUT) | Handles modifying roles associated to a user. Handles modifying roles associated to a user.  |
| Update-vendor - Implements entity update endpoint for Vendor. (PUT) | Implements entity update endpoint for Vendor. Implements entity update endpoint for Vendor.  |
| Update-vendor-configuration - Implements entity update endpoint for VendorConfig... (PUT) | Implements entity update endpoint for VendorConfiguration. Implements entity update endpoint for VendorConfiguration.  |
| Update-voucher - Implements entity update endpoint for Voucher. (PUT) | Implements entity update endpoint for Voucher. Implements entity update endpoint for Voucher.  |
| Update-webhook-definition - Implements entity update endpoint for WebhookDefin... (PUT) | Implements entity update endpoint for WebhookDefinition. Implements entity update endpoint for WebhookDefinition.  |
| Update-workflow-definition - Implements entity update endpoint for WorkflowDefi... (PUT) | Implements entity update endpoint for WorkflowDefinition. Implements entity update endpoint for WorkflowDefinition.  |


