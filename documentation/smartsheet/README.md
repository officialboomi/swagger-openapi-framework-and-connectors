# Smartsheet 
API 2.0 allows you to programmatically access and manage your organization's Smartsheet data and account information.

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string



#### Smartsheet  REST API Service URL

The URL for the Smartsheet  REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### AuthenticationType

Allows user to select between BASIC and OAUTH 2.0 Authentication

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| Add-alternate-email - Add Alternate Emails | alternateEmailAddress Adds one or more alternate email addresses for the specified user.  **_This operation is only available to system administrators._**  Add Alternate Emails  |
| Add-crosssheet-reference - Create Cross-sheet References | crossSheetReferences Adds a cross-sheet reference between two sheets and defines the data range for formulas. Each distinct data range requires a new cross-sheet reference. Create Cross-sheet References  |
| Add-favorite - Add Favorites | favorites Adds one or more items to the user's list of favorite items. This operation supports both single-object and bulk semantics. For more information, see Optional Bulk Operations. If called with a single Favorite object, and that favorite already exists, error code 1129 is returned. If called with an array of Favorite objects, any objects specified in the array that are already marked as favorites are ignored and omitted from the response.  Add Favorites  |
| Add-group - Add Group | groups Creates a new group.  **_This operation is only available to group administrators and system administrators._**  Add Group  |
| Add-group-members - Add Group Members | groupMembers Adds one or more members to a group.  **_This operation supports both single-object and bulk semantics. For more information, see Optional Bulk Operations._**  If called with a single [GroupMember object](../../tag/groupMembersObjects), and that group member already exists, error code **1129** is returned. If called with an array of [GroupMember objects](../../tag/groupMembersObjects), any users specified in the array that are already group members are ignored and omitted from the response.  **_This operation is only available to group administrators and system administrators._**  Add Group Members  |
| Add-image-summaryField - Add Image to Sheet Summary | sheetSummary Adds an image to the summary field. Add Image to Sheet Summary  |
| Add-summary-fields - Add Summary Fields | sheetSummary Creates one or more summary fields for the specified sheet. Add Summary Fields  |
| Add-user - Add User | users Adds a user to the organization account.  * **_This operation is only available to system administrators_**  * **If successful, and user auto provisioning (UAP) is on, and user matches the auto provisioning rules, user is added to the org. If UAP is off, or user does not match UAP rules, user is invited to the org and must explicitly accept the invitation to join.**  * **In some specific scenarios, supplied attributes such as firstName and lastName may be ignored. For example, if you are inviting an existing Smartsheet user to join your organization account, and the invited user has not yet accepted your invitation, any supplied firstName and lastName are ignored.**  Add User  |
| AddImageToCell - Add Image to Cell | cellImages Uploads an image to the specified cell within a sheet. Add Image to Cell  |
| Attachments-attachToComment - Attach File or URL to Comment | attachments Attaches a file to the comment. The URL can be any of the following:  * Normal URL (attachmentType "LINK") * Box.com URL (attachmentType "BOX_COM") * Dropbox URL (attachmentType "DROPBOX") * Egnyte URL (attachmentType "EGNYTE") * Evernote URL (attachmentType "EVERNOTE") * Google Drive URL (attachmentType "GOOGLE_DRIVE") * OneDrive URL (attachmentType "ONEDRIVE")  This operation can be performed using a simple upload or a multipart upload.  Attach File or URL to Comment  |
| Attachments-attachToSheet - Attach File or URL to Sheet | attachments Attaches a file to the sheet. The URL can be any of the following:  * Normal URL (attachmentType "LINK") * Box.com URL (attachmentType "BOX_COM") * Dropbox URL (attachmentType "DROPBOX") * Egnyte URL (attachmentType "EGNYTE") * Evernote URL (attachmentType "EVERNOTE") * Google Drive URL (attachmentType "GOOGLE_DRIVE") * OneDrive URL (attachmentType "ONEDRIVE")  For multipart uploads please use "multipart/form-data" content type.  Attach File or URL to Sheet  |
| Attachments-versionUpload - Attach New version | attachments Uploads a new version of a file to a sheet or row. This operation can be performed using a simple upload or a multipart upload.  Attach New version  |
| Columns-addToSheet - Add Columns | columns Inserts one or more columns into the sheet specified in the URL.This operation can be performed using a [simple upload](../../tag/attachmentsDescription#section/Post-an-Attachment/Simple-Uploads) or a [multipart upload](../../tag/attachmentsDescription#section/Post-an-Attachment/Multipart-Uploads). For more information, see [Post an Attachment](../../tag/attachmentsDescription#section/Post-an-Attachment). Add Columns  |
| Comments-create - Create a comment | comments Adds a comment to a discussion. To create a comment with an attachment please use "multipart/form-data" content type.  Create a comment  |
| Copy-folder - Copy Folder | folders Copies a folder. Copy Folder  |
| Copy-rows - Copy Rows to Another Sheet | rows Copies rows from the sheet specified in the URL to (the bottom of) another sheet. Copy Rows to Another Sheet  |
| Copy-sheet - Copy Sheet | sheets Creates a copy of the specified sheet.  Copy Sheet  |
| Copy-sight - Copy Dashboard | dashboards Creates a copy of the specified dashboard. Copy Dashboard  |
| Copy-workspace - Copy Workspace | workspaces Copies a workspace. Copy Workspace  |
| Create-folder-folder - Create Folder | folders Creates a new folder.  Create Folder  |
| Create-home-folder - Create Folder | home Creates a new folder.  Create Folder  |
| Create-sheet-in-folder - Create Sheet in Folder | sheets Creates a sheet from scratch or from the specified template in the specified folder.  Create Sheet in Folder  |
| Create-sheet-in-sheets-folder - Create Sheet in "Sheets" Folder | sheets Creates a sheet from scratch or from the specified template in the user's Sheets folder (Home). For subfolders, use Create Sheet in Folder.  Create Sheet in "Sheets" Folder  |
| Create-sheet-in-workspace - Create Sheet in Workspace | sheets Creates a sheet from scratch or from the specified template at the top-level of the specified workspace. For subfolders, use Create Sheet in Folder.  Create Sheet in Workspace  |
| Create-workspace - Create Workspace | workspaces Creates a new workspace.  Create Workspace  |
| Create-workspace-folder - Create a Folder | workspaces Creates a new folder.  Create a Folder  |
| CreateWebhook - Create Webhook | webhooks Creates a new Webhook.  A webhook is not enabled by default when it is created. Once you've created a webhook, you can enable it by using the Update Webhook operation to set **enabled** to **true**.  When a row is deleted on a sheet, even if you are using a **subscope** to monitor columns only and the cell in that column for that row is empty, you will receive a "row.deleted" event.  Create Webhook  |
| Discussions-create - Create a Discussion | discussions Creates a new discussion on a sheet. To create a discussion with an attachment please use "multipart/form-data" content type.  Create a Discussion  |
| Import-sheet-into-folder - Import Sheet into Folder | import Imports CSV or XLSX data into a new sheet in the specified folder.  Note the following: * Both sheetName and the file name must use ASCII characters. * The source data must be basic text. To include rich formula data, import and create a sheet first, and then use Update Rows. To work with images, see Cell Images. * XLS is not supported. You must use XLSX. * Hierarchical relationships between rows in an external file won't import.  Import Sheet into Folder  |
| Import-sheet-into-sheets-folder - Import Sheet from CSV / XLSX | import Imports CSV or XLSX data into a new sheet in the top-level "Sheets" folder.  Note the following: * Both sheetName and the file name must use ASCII characters. * The source data must be basic text. To include rich formula data, import and create a sheet first, and then use Update Rows. To work with images, see Cell Images. * XLS is not supported. You must use XLSX. * Hierarchical relationships between rows in an external file won't import.  Import Sheet from CSV / XLSX  |
| Import-sheet-into-workspace - Import Sheet into Workspace | import Imports CSV or XLSX data into a new sheet in the specified workspace.  Note the following: * Both sheetName and the file name must use ASCII characters. * The source data must be basic text. To include rich formula data, import and create a sheet first, and then use Update Rows. To work with images, see Cell Images. * XLS is not supported. You must use XLSX. * Hierarchical relationships between rows in an external file won't import.  Import Sheet into Workspace  |
| ListImageUrls - List Image URLs | cellImages Gets a list of URLs that can be used to retrieve the specified cell images. List Image URLs  |
| Move-folder - Move Folder | folders Moves a folder. Move Folder  |
| Move-rows - Move Rows to Another Sheet | rows Moves rows from the sheet specified in the URL to (the bottom of) another sheet. Move Rows to Another Sheet  |
| Move-sheet - Move Sheet | sheets Moves the specified sheet to a new location. When a sheet that is shared to one or more users and/or groups is moved into or out of a workspace, those sheet-level shares are preserved.  Move Sheet  |
| Move-sight - Move Dashboard | dashboards Moves the specified dashboard to a new location. Move Dashboard  |
| Promote-alternate-email - Make Alternate Email Primary | alternateEmailAddress Makes the specified alternate email address to become the primary email address for the specified user.  * **_This operation is only available to system administrators_**  The alternate email address can only be made primary if both conditions are met:   * The primary email address domain is validated   * The alternate email address is confirmed or the alternate email address domain is validated  Make Alternate Email Primary  |
| Proofs-attachToProof - Attach File to Proof | proofs Attaches a file to the proof.  Attach File to Proof  |
| Proofs-create - Create Proof | proofs Creates a proof on a row.  Create Proof  |
| Proofs-createDiscussion - Create Proof Discussion | proofs Creates a discussion on a proof.  Create Proof Discussion  |
| Proofs-createProofRequests - Create Proof Request | proofs Creates a proof request.  Create Proof Request  |
| Proofs-createVersion - Create Proof Version | proofs Creates a proof version. Proof Id must be for the original proof.  Create Proof Version  |
| ResetSharedSecret - Reset Shared Secret | webhooks Resets the shared secret for the specified webhook. For more information about how a shared secret is used, see Authenticating Callbacks. This operation can be used to rotate an API client's webhooks' shared secrets at periodic intervals to provide additional security. Reset Shared Secret  |
| Row-attachments-attachFile - Attach File or URL to Row | attachments Attaches a file to the row. The URL can be any of the following:  * Normal URL (attachmentType "LINK") * Box.com URL (attachmentType "BOX_COM") * Dropbox URL (attachmentType "DROPBOX") * Egnyte URL (attachmentType "EGNYTE") * Evernote URL (attachmentType "EVERNOTE") * Google Drive URL (attachmentType "GOOGLE_DRIVE") * OneDrive URL (attachmentType "ONEDRIVE")  For multipart uploads please use "multipart/form-data" content type.  Attach File or URL to Row  |
| Row-discussions-create - Create a Discussion on a Row | discussions Creates a new discussion on a row. To create a discussion with an attachment please use "multipart/form-data" content type.  Create a Discussion on a Row  |
| Rows-addToSheet - Add Rows | rows Inserts one or more rows into the sheet specified in the URL. If you want to insert the rows in any position but the default, use [location-specifier]() attributes.       This operation supports both single-object and bulk semantics. For more information, see Optional Bulk Operations.  Add Rows  |
| Rows-send - Send Rows via Email | rows Sends one or more rows via email. Send Rows via Email  |
| Rows-sort - Sort Rows in Sheet | rows Sorts the rows of a sheet, either in ascending or descending order. Sort Rows in Sheet  |
| SendReportViaEmail - Send report via email | reports Sends the report as a PDF attachment via email to the designated recipients Send report via email  |
| Share-report - Share Report | sharing reports Shares a Report with the specified users and groups. Share Report  |
| Share-sheet - Share Sheet | sharing sheets Shares a sheet with the specified users and groups. Share Sheet  |
| Share-sight - Share Dashboard | dashboards sharing Shares a dashboard with the specified users and groups. Share Dashboard  |
| Share-workspace - Share Workspace | sharing workspaces Shares a Workspace with the specified users and groups. This operation supports both single-object and bulk semantics.  Share Workspace  |
| Sheet-send - Send Sheet via Email | sheets Sends the sheet as a PDF attachment via email to the designated recipients. Send Sheet via Email  |
| Tokens-getOrRefresh - Gets or Refreshes an Access Token | token Gets or refreshes an access token, as part of the OAuth process. Gets or Refreshes an Access Token  |
| Update-user-profile-image - Update User Profile Image | users Uploads an image to the user profile.  Uploading a profile image differs from Adding an Image to a Cell in the following ways:   * A **Content-Length** header is not required   * Allowable file types are limited to: gif, jpg, and png   * Maximum file size is determined by the following rules:       * If you have not defined a custom size and the image is larger than 1050 x 1050 pixels, Smartsheet scales the image down to 1050 x 1050       * If you have defined a custom size, Smartsheet uses that as the file size max   * If the image is not square, Smartsheet uses a solid color to pad the image  Update User Profile Image  |
| Updaterequests-create - Create an Update Request | updateRequests Creates an update request for the specified rows within the sheet. An email notification (containing a link to the update request) is sent to the specified recipients according to the specified schedule.  The recipients of an update request must be specified by using email addresses only. Sending an update request to a group is not supported.  The following attributes have the following values when not specified: * **ccMe:** false * **message:** Please update the following rows in my online sheet. * **subject:** Update Request: {Sheet Name}  When the Schedule object is not specified, the request is sent to the recipients immediately.  If an error occurs because the request specified one or more *alternate email addresses*, please retry using the primary email address.  Create an Update Request  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Attachments-delete - Delete Attachment | attachments Deletes the attachment specified in the URL.  Delete Attachment  |
| Attachments-versionsDelete - Delete All Versions | attachments Deletes all versions of the attachment corresponding to the specified attachmentId. For attachments with multiple versions, this effectively deletes the attachment from the object that it’s attached to.  Delete All Versions  |
| Automationrule-delete - Delete an Automation Rule | automationRules Deletes an automation rule.  Delete an Automation Rule  |
| Column-delete - Delete Column | columns Deletes the column specified in the URL. Delete Column  |
| Comment-delete - Delete a comment | comments Deletes the comment specified in the URL.  Delete a comment  |
| Delete-alternate-email - Delete Alternate Email | alternateEmailAddress Deletes the specified alternate email address for the specified user. Delete Alternate Email  |
| Delete-favorites-by-type - Remove Multiple Favorites | favorites Removes all favorites with the same type for the user. Remove Multiple Favorites  |
| Delete-favorites-by-type-and-id - Remove Favorite | favorites Removes a single favorite from the user's list of favorite items by type and ID. Remove Favorite  |
| Delete-folder - Delete Folder | folders Deletes a folder. Delete Folder  |
| Delete-group - Delete Group | groups Deletes the group specified in the URL.  **_This operation is only available to group administrators and system administrators._**  Delete Group  |
| Delete-group-members - Delete Group Members | groupMembers Removes a member from a group.  **_This operation is only available to group administrators and system administrators._**  Delete Group Members  |
| Delete-report-share - Delete Report Share | sharing reports Deletes the share specified in the URL. Delete Report Share  |
| Delete-rows - Delete Rows | rows Deletes one or more rows from the sheet specified in the URL. Delete Rows  |
| Delete-sheet-share - Delete Sheet Share | sharing sheets Deletes the share specified in the URL. Delete Sheet Share  |
| Delete-sight - Delete Dashboard | dashboards Deletes the dashboard specified in the URL. Delete Dashboard  |
| Delete-sight-share - Delete Dashboard Share | dashboards sharing Deletes the share specified in the URL. Delete Dashboard Share  |
| Delete-summary-fields - Delete Summary Fields | sheetSummary Deletes summary fields from the specified sheet. Delete Summary Fields  |
| Delete-workspace - Delete Workspace | workspaces Deletes a workspace. Delete Workspace  |
| Delete-workspace-share - Delete Workspace Share | sharing workspaces Deletes the share specified in the URL. Delete Workspace Share  |
| DeleteSheet - Delete Sheet | sheets Deletes the sheet specified in the URL. Delete Sheet  |
| DeleteWebhook - Delete Webhook | webhooks Deletes the specified Webhook.  Using this operation permanently deletes the specified webhook. To temporarily disable a webhook, use the Update Webhook operation to set **enabled** to **false**.  Delete Webhook  |
| Discussion-delete - Delete a Discussion | discussions Deletes the discussion specified in the URL.  Delete a Discussion  |
| Proofs-delete - Delete Proof | proofs Deletes the proof including all versions. The proofId must be for the original version.  Delete Proof  |
| Proofs-deleteProofRequests - Delete Proof Requests | proofs Deletes all proof requests in a proof.  Delete Proof Requests  |
| Proofs-deleteVersion - Delete Proof Version | proofs Deletes a proof version. Proof Id must be a current version proof Id.  Delete Proof Version  |
| Remove-user - Remove User | users Removes a user from an organization account. User is transitioned to a free collaborator with read-only access to owned reports, sheets, Sights, workspaces, and any shared templates (unless those are optionally transferred to another user).  * **_This operation is only available to system administrators_**  * If the **transferTo** parameter is specified and the removed user owns groups, the user specified via the **transferTo** parameter must have group admin rights.  * The **transferTo** and **transferSheets** parameters cannot be specified for a user who has not yet accepted an invitation to join the organization account (that is, if user **status=PENDING**).  Remove User  |
| Sentupdaterequest-delete - Delete Sent Update Request | updateRequests Deletes the specified sent update request.  **Delete operation is supported only when the specified sent update request is in the pending status. Deleting a sent update request that was already completed by recipient is not allowed.**  Delete Sent Update Request  |
| Tokens-delete - Revoke Access Token | token Revokes the access token used to make this request. The access token is no longer valid, and subsequent API calls made using the token fail. Revoke Access Token  |
| Updaterequests-delete - Delete an Update Request | updateRequests Terminates the future scheduled delivery of the update request specified in the URL.  Delete an Update Request  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Attachments-get - Get Attachment | attachments Fetches a temporary URL that allows you to download an attachment. The urlExpiresInMillis attribute tells you how long the URL is valid.  Get Attachment  |
| Automationrule-get - Get an Automation Rule | automationRules Returns the specified automation rule, including any action values.  Get an Automation Rule  |
| Column-get - Get Column | columns Gets the column specified in the URL. Get Column  |
| Comment-get - Get a comment | comments Gets the comment specified by commentId.  Get a comment  |
| Discussion-get - Get Discussion | discussions Gets the discussion specified by discussionId.  Get Discussion  |
| Get-alternate-email - Get Alternate Email | alternateEmailAddress Gets the specified alternate email. Get Alternate Email  |
| Get-contact - Get Contact | contacts Gets the specified contact. Get Contact  |
| Get-crosssheet-reference - Get Cross-sheet Reference | crossSheetReferences Gets the cross-sheet reference specified in the URL. Get Cross-sheet Reference  |
| Get-folder - Get Folder | folders Gets a Folder object. Get Folder  |
| Get-group - Get Group | groups Gets information about and an array of [Group Members](../../tag/groupMembersObjects#section/Group-Member-Object) for the group specified in the URL. Get Group  |
| Get-sight - Get Dashboard | dashboards Gets the specified dashboard. Get Dashboard  |
| Get-user - Get User | users Gets the user specified in the URL.  * NOTE: For system administrators, the following UserProfile attributes are included in the response):   * **admin**   * **customWelcomeScreenViewed** (only returned when an Enterprise user has viewed the [Custom Welcome Screen](https://help.smartsheet.com/articles/1392225-customizing-a-welcome-message-upgrade-screen-enterprise-only))   * **groupAdmin**   * **lastLogin** (only returned if the user has logged in)   * **licensedSheetCreator**   * **resourceViewer**   * **sheetCount** (only returned if the status attribute is ACTIVE)   * **status**  Get User  |
| Get-workspace - Get Workspace | workspaces Gets a Workspace object. Get Workspace  |
| GetReport - Get Report | reports Gets a report based on the specified ID Get Report  |
| GetSheet - Get Sheet | sheets Gets a sheet in the format specified, based on the sheet Id. Get Sheet  |
| GetWebhook - Get Webhook | webhooks Gets a Webhook based on the specified ID Get Webhook  |
| List-search-sheet - Search Sheet | search Gets a list of the user's search results in a sheet based on query. The list contains an abbreviated row object for each search result in a sheet. *Note* Newly created or recently updated data may not be immediately discoverable via search. Search Sheet  |
| Proofs-get - Get Proof | proofs Gets the proof specified in the URL. Returns the proof, which is optionally populated with discussion and attachment objects.  Get Proof  |
| Row-get - Get Row | rows Gets the row specified in the URL. Get Row  |
| Sentupdaterequest-get - Get Sent Update Request | updateRequests Gets the specified sent update request on the sheet.  Get Sent Update Request  |
| Share-report-get - Get Report Share. | sharing reports Gets the share specified in the URL. Get Report Share.  |
| Share-sheet-get - Get Sheet Share. | sharing sheets Gets the share specified in the URL. Get Sheet Share.  |
| Share-sight-get - Get Dashboard Share | dashboards sharing Gets a list of all users and groups to whom the specified dashboard is shared, and their access level. Get Dashboard Share  |
| Share-workspace-get - Get Workspace Share. | sharing workspaces Gets the share specified in the URL. Get Workspace Share.  |
| Updaterequests-get - Get an Update Request | updateRequests Gets the specified update request for the sheet that has a future schedule.  The rowIds and columnIds in the returned UpdateRequest object represent the list at the time the update request was created or last modified. The lists may contain Ids of rows or columns that are no longer valid (for example, they have been removed from the sheet).  Get an Update Request  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Attachments-listOnRow - List Row Attachments | attachments Gets a list of all attachments that are on the row, including row and discussion-level attachments.  List Row Attachments  |
| Attachments-listOnSheet - List Attachments | attachments Gets a list of all attachments that are on the sheet, including sheet, row, and discussion-level attachments.  List Attachments  |
| Attachments-versionList - List Versions | attachments Gets a list of all versions of the given attachmentId in order from newest to oldest.  List Versions  |
| Automationrules-list - List All Automation Rules | automationRules Returns all automation rules associated with the specified sheet.  Multistep workflows are not returned via the API. Instead, you'll see an error 400 - 1266: This rule is not accessible through the API. Only single-action notifications, approval requests, or update requests qualify.  For users of Smartsheet for Slack, note that Slack notifications are not returned.  List All Automation Rules  |
| CellHistory-get - List Cell History | cells Gets the cell modification history.  List Cell History  |
| Columns-listOnSheet - List Columns | columns Gets a list of all columns belonging to the sheet specified in the URL. List Columns  |
| Discussion-listAttachments - List Discussion Attachments | attachments Gets a list of all attachments that are in the discussion.  List Discussion Attachments  |
| Discussions-list - List Discussions | discussions Gets a list of all discussions associated with the specified sheet. Remember that discussions are containers for the conversation thread. To see the entire thread, use the include=comments parameter.  List Discussions  |
| Get-current-user - Get Current User | users Gets the current user  **NOTE:** For system administrators, the following UserProfile attributes are included in the response:   * **customWelcomeScreenViewed** (only returned when an Enterprise user has viewed the [Custom Welcome Screen](https://help.smartsheet.com/articles/1392225-customizing-a-welcome-message-upgrade-screen-enterprise-only))   * **lastLogin** (only returned if the user has logged in)   * **sheetCount** (only returned if the status attribute is ACTIVE)  Get Current User  |
| Get-favorites - List Favorites | favorites Gets a list of all of the user's favorite items. List Favorites  |
| Get-sheetPublish - Get Sheet Publish Status | sheets Gets the sheet's 'Publish' settings.  Get Sheet Publish Status  |
| Get-sheetVersion - Get Sheet Version | sheets Gets the sheet version without loading the entire sheet. The following actions increment sheet version: * add/modify cell value * add/modify discussion/comment * add/modify row * add/remove/update version attachment * cell updated via cell link * change formatting  Get Sheet Version  |
| Get-sight-publish-status - Get Dashboard Publish Status | sights Gets the dashboard 'publish' settings. Get Dashboard Publish Status  |
| Get-workspace-folders - Get Workspace folders | workspaces Gets a workspace's folders. Get Workspace folders  |
| GetReportPublish - Gets a Report's publish settings | reports Get a Report's publish settings based on the specified ID Gets a Report's publish settings  |
| GetReports - Get Reports | reports Gets a list of all Reports that the user has access to. Get Reports  |
| Home-list-folders - List Folders in Home | home Gets a list of folders in your Home tab. The list contains an abbreviated Folder object for each folder. List Folders in Home  |
| List-alternate-emails - List Alternate Emails | alternateEmailAddress Gets a list of the alternate emails for the specified user. List Alternate Emails  |
| List-contacts - List Contacts | contacts Gets a list of the user's Smartsheet contacts. List Contacts  |
| List-crosssheet-references - List Cross-sheet References | crossSheetReferences Lists all cross-sheet references for the sheet. List Cross-sheet References  |
| List-events - List Events | events Gets events that are occurring in your Smartsheet organization account. Examples of events are creation, update, load, and delete of sheets, reports, dashboards, attachments, users, etc.  Each event type has a distinct combination of objectType and action. Many event types have additional information returned under an additionalDetails object. See the Event Reporting reference documentation for a complete list of all currently supported events, including their respective objectType, action, and additionalDetails properties. List Events  |
| List-folders - List Folders | folders Gets a list of folders in a given folder. The list contains an abbreviated Folder object for each folder.  List Folders  |
| List-groups - List Org Groups | groups Gets a list of all groups in an organization account. To fetch the members of an individual group, use the [Get Group](../../tag/groups#operation/get-group) operation.  List Org Groups  |
| List-home-contents - List Contents | home Gets a nested list of all Home objects, including dashboards, folders, reports, sheets, templates, and workspaces, as shown on the "Home" tab. List Contents  |
| List-org-sheets - List Org Sheets | sheets Gets a summarized list of all sheets owned by the members of the organization account.  * **_This operation is only available to system administrators_**  * **_You may use the query string parameter numericDates with a value of true to enable strict parsing of dates in numeric format. See Dates and Times for more information._**  List Org Sheets  |
| List-report-shares - List Report Shares | sharing reports Gets a list of all users and groups to whom the specified Report is shared, and their access level. This operation supports query string parameters for pagination of results.  List Report Shares  |
| List-search - Search Everything | search Searches all sheets that the user can access, for the specified text. Search Everything  |
| List-sheet-shares - List Sheet Shares | sharing sheets Gets a list of all users and groups to whom the specified Sheet is shared, and their access level. This operation supports query string parameters for pagination of results. For more information, see Paging Query String Parameters.  List Sheet Shares  |
| List-sheets - List Sheets | sheets Gets a list of all sheets that the user has access to. The list contains an abbreviated Sheet object for each sheet.  List Sheets  |
| List-sight-shares - List Dashboard Shares | dashboards sharing Gets a list of all users and groups to whom the specified dashboard is shared, and their access level. List Dashboard Shares  |
| List-sights - List Dashboards | dashboards Gets a list of all dashboards that the user has access to. List Dashboards  |
| List-summary-fields - Get Sheet Summary | sheetSummary Returns object containing array of summary fields. Allows for pagination of results. Get Sheet Summary  |
| List-summary-fields-paginated - Get Summary Fields | sheetSummary Returns object containing array of summary fields. Allows for pagination of results. Get Summary Fields  |
| List-users - List Users | users Gets a list of users in the organization account. To filter by email, use the optional email query string parameter to specify a list of users' email addresses.  **NOTE:** If the API request is submitted by a system administrator, the following User object attributes are included in the response (else, they are omitted from the response):   * **admin**   * **groupAdmin**   * **licensedSheetCreator**   * **resourceViewer**   * **sheetCount (omitted if the status attribute is not ACTIVE)**   * **status**  **NOTE:** If the API request is submitted by a system administrator of an Enterprise account, and [Custom Welcome Screen](https://help.smartsheet.com/articles/1392225-customizing-a-welcome-message-upgrade-screen-enterprise-only) is enabled, the following [User object](../../tag/usersObjects#section/User-Object) attributes are included in the response (else, they are omitted from the response):   * **customWelcomeScreenViewed** (omitted if the user has never viewed the [Custom Welcome Screen](https://help.smartsheet.com/articles/1392225-customizing-a-welcome-message-upgrade-screen-enterprise-only))  List Users  |
| List-webhooks - List Webhooks | webhooks Gets the list of all *webhooks* that the user owns (if a user-generated token was used to make the request) or the list of all webhooks associated with the third-party app (if a third-party app made the request). Items in the response are ordered by API cient name > webhook name > creation date. List Webhooks  |
| List-workspace-shares - List Workspace Shares | sharing workspaces Gets a list of all users and groups to whom the specified Workspace is shared, and their access level. List Workspace Shares  |
| List-workspaces - List Workspaces | workspaces Gets a list of workspaces that the user has access to. The list contains an abbreviated Workspace object for each workspace.  List Workspaces  |
| Proofs-getAllProofs - List Proofs | proofs Gets a list of all proofs for a given sheet.  List Proofs  |
| Proofs-getVersions - List Proof Versions | proofs Gets a list of all versions of the given proofId in order from newest to oldest.  List Proof Versions  |
| Proofs-listAttachments - List Proof Attachments | proofs Gets a list of all attachments that are in the proof, excluding discussion-level attachments in the proof.  List Proof Attachments  |
| Proofs-listDiscussions - List Proof Discussions | proofs Gets a list of all discussions that are in the proof.  List Proof Discussions  |
| Proofs-listRequestActions - List Proof Request Actions | proofs Gets a summarized list of all request actions associated with the specified proof.  List Proof Request Actions  |
| Row-discussions-list - List Discussions with a Row | discussions Gets a list of all discussions associated with the specified row. Remember that discussions are containers for the conversation thread. To see the entire thread, use the include=comments parameter.  List Discussions with a Row  |
| Sentupdaterequests-list - List Sent Update Requests | updateRequests Gets a summarized list of all sent update requests on the sheet. Only the following fields are returned in the response:   * **id**   * **message**   * **sendTo**   * **sentAt**   * **sentBy**   * **status**   * **subject**   * **updateRequestId**  List Sent Update Requests  |
| Serverinfo-get - Gets application constants. | serverInfo Gets application constants. Gets application constants.  |
| Templates-list - List User-Created Templates | templates Gets a list of user-created templates that the user has access to. List User-Created Templates  |
| Templates-listPublic - List Public Templates | templates Gets a list of public templates that the user has access to. List Public Templates  |
| Updaterequests-list - List Update Requests | updateRequests Gets a summarized list of all update requests that have future schedules associated with the specified sheet. Only the following fields are returned in the response:   * **id**   * **ccMe**   * **createdAt**   * **message**   * **modifiedAt**   * **schedule**   * **sendTo**   * **sentBy**   * **subject**  List Update Requests  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Automationrule-update - Update an Automation Rule (PUT) | automationRules Updates an existing automation rule.  When sending an AutomationRule, you must always specify **action.type** and it must match the existing rule type.  Update an Automation Rule  |
| Column-updateColumn - Update Column (PUT) | columns Updates properties of the column, moves the column, or renames the column.  **NOTE:** * You cannot change the type of a Primary column. * While dependencies are enabled on a sheet, you can't change the type of any special calendar/Gantt columns. * If the column type is changed, all cells in the column are converted to the new column type and column validation is cleared. * Type is optional when moving or renaming, but required when changing type or dropdown values.  Update Column  |
| Comment-edit - Edit a comment (PUT) | comments Updates the text of a comment. NOTE: Only the user that created the comment is permitted to update it.  Edit a comment  |
| Proofs-update - Update Proof Status (PUT) | proofs Sets the proof status as either complete or incomplete.  Update Proof Status  |
| Set-sheetPublish - Set Sheet Publish Status (PUT) | sheets Sets the publish status of the sheet and returns the new status, including the URLs of any enabled publishings.  Set Sheet Publish Status  |
| Set-sight-publish-status - Set Dashboard Publish Status (PUT) | dashboards Publishes or unpublishes a dashboard. Set Dashboard Publish Status  |
| SetReportPublish - Set a Report's publish status (PUT) | reports Sets the publish status of the report and returns the new status, including the URL of any enabled publishing. Set a Report's publish status  |
| Update-folder - Update Folder (PUT) | folders Updates a folder. Update Folder  |
| Update-group - Update Group (PUT) | groups Updates the Group specified in the URL.  **_This operation is only available to group administrators and system administrators._**  Update Group  |
| Update-report-share - Update Report Share. (PUT) | sharing reports Updates the access level of a user or group for the specified report. Update Report Share.  |
| Update-rows - Update Rows (PUT) | rows Updates cell values in the specified rows, expands/collapses the specified rows, or modifies the position of specified rows (including indenting/outdenting). For detailed information about changing row positions, see [location-specifier attributes](). Update Rows  |
| Update-sheet-share - Update Sheet Share. (PUT) | sharing sheets Updates the access level of a user or group for the specified sheet. Update Sheet Share.  |
| Update-sight - Update Dashboard (PUT) | dashboards Updates (renames) the specified dashboard. Update Dashboard  |
| Update-sight-share - Update Dashboard Share (PUT) | dashboards sharing Updates the access level of a user or group for the specified dashboard. Update Dashboard Share  |
| Update-summary-fields - Update Summary Fields (PUT) | sheetSummary Updates the summary fields for the given sheet. Update Summary Fields  |
| Update-user - Update User (PUT) | users Updates the user specified in the URL. Update User  |
| Update-workspace - Update Workspace (PUT) | workspaces Updates a workspace. Update Workspace  |
| Update-workspace-share - Update Workspace Share. (PUT) | sharing workspaces Updates the access level of a user or group for the specified workspace. Update Workspace Share.  |
| Updaterequests-update - Update an Update Request (PUT) | updateRequests Changes the specified update request for the sheet.  **Making changes to update requests that do not have future scheduled delivery is not allowed.**  The UpdateRequest object in the request body must specify one or more of the following attributes:  * **ccMe:** Boolean * **columnIds:** number[] * **includeAttachments:** Boolean * **includeDiscussions:** Boolean * **message:** string * **schedule:** Schedule object * **sendTo:** Recipient[] * **subject:** string  If an error occurs because the request specified one or more *alternate email addresses*, please retry using the primary email address.  Update an Update Request  |
| UpdateSheet - Update Sheet (PUT) | sheets Updates the sheet specified in the URL. To modify sheet contents, see [Add Rows](../../tag/rows#operation/rows-addToSheet), [Update Rows](../../tag/rows#operation/update-rows), [Add Columns](../../tag/columns#operation/columns-addToSheet), and [Update Column](../../tag/columns#operation/column-updateColumn). This operation can be used to update an individual user's sheet settings. If the request body contains only the **userSettings** attribute, this operation may be performed even if the user only has read-only access to the sheet (for example, the user has viewer permissions or the sheet is read-only). Update Sheet  |
| UpdateWebhook - Update Webhook (PUT) | webhooks Updates the specified Webhook. The following properties can be updated: * callbackUrl (optional) * enabled (optional) * events (optional) * name (optional) * version (optional)  When setting a webhook's **enabled** to **true** using this operation, the behavior and result depend on the webhook's **status** and may result in a webhook verification being triggered, or in some cases, an error being returned. See Webhook Status for more details.  Update Webhook  |


