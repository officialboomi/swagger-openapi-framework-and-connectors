# Coupa REST API
REST interface to Coupa

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string



#### Coupa REST API REST API Service URL

The URL for the Coupa REST API REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE/POST
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE/PATCH
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE/PUT
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

The connector does not support field selection. All fields will be returned by default.


#### Filters

The query filter supports any number of non-nested expressions which will be AND'ed together.

Example:
((foo lessThan 30) AND (baz lessThan 42))

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is blank (true|false)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Contains (Not used with date fields)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Starts With (Not used with date fields)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Ends With (Not used with date fields)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

 * **Authorization** - Authorization Header to provide dynamically.

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| Attachment - Delete attachment | Attachment Delete attachment Delete attachment  |
| CommodityTranslation - Delete commodity translation | CommodityTranslation Delete commodity translation Delete commodity translation  |
| ObjectTranslation - /uoms/{uom_id}/translations/{id} | ObjectTranslation  /uoms/{uom_id}/translations/{id}  |
| ProjectMembership - Delete a project membership | ProjectMembership Delete a project membership Delete a project membership  |
| Supplier - Log out (CSP)iframe session | Supplier Log out (CSP)iframe session Log out (CSP)iframe session  |
| SupplierInformationSite - /supplier_information/{supplier_information_id}/su... | SupplierInformationSite  /supplier_information/{supplier_information_id}/supplier_information_sites/{id}  |
| SupplierInformationSite - /supplier_information_sites/{id} | SupplierInformationSite  /supplier_information_sites/{id}  |
| SupplierInformationTaxRegistration - /supplier_information_tax_registrations/{id} | SupplierInformationTaxRegistration  /supplier_information_tax_registrations/{id}  |
| SupplierSharingSetting - Delete supplier sharing setting | SupplierSharingSetting Delete supplier sharing setting Delete supplier sharing setting  |
| Task - /projects/{project_id}/tasks/{id} | Task  /projects/{project_id}/tasks/{id}  |
| Task - /tasks/{id} | Task  /tasks/{id}  |
| User - /users/{id}/avatar | User  /users/{id}/avatar  |
| UserGroupMembership - Delete an user group membership | UserGroupMembership Delete an user group membership Delete an user group membership  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Account - Show account | Account Show account Show account  |
| AccountType - Show chart of accounts | AccountType Show chart of accounts Show chart of accounts  |
| AccountValidationRule - Show account validation | AccountValidationRule Show account validation Show account validation  |
| Address - Show address | Address Show address Show address  |
| Address - Show user's association to an address | Address Show user's association to an address Show user's association to an address  |
| Announcement - Show announcement | Announcement Show announcement Show announcement  |
| Approval - Show approval | Approval Show approval Show approval  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| Attachment - Show attachment | Attachment Show attachment Show attachment  |
| BudgetLine - Show budget line | BudgetLine Show budget line Show budget line  |
| BudgetLineAdjustment - Show budget line adjustment | BudgetLineAdjustment Show budget line adjustment Show budget line adjustment  |
| BudgetLineAdjustment - Show budget line adjustment | BudgetLineAdjustment Show budget line adjustment Show budget line adjustment  |
| BusinessEntity - Show business entity | BusinessEntity Show business entity Show business entity  |
| BusinessGroup - Show business group | BusinessGroup Show business group Show business group  |
| BusinessGroup - Show contract's associations to a business groups | BusinessGroup Show contract's associations to a business groups Show contract's associations to a business groups  |
| BusinessGroup - Show supplier's associations to a business groups | BusinessGroup Show supplier's associations to a business groups Show supplier's associations to a business groups  |
| Comment - Show comment | Comment Show comment Show comment  |
| Comment - Show comment | Comment Show comment Show comment  |
| Comment - Show comment | Comment Show comment Show comment  |
| Comment - Show comment | Comment Show comment Show comment  |
| Comment - Show comment | Comment Show comment Show comment  |
| Commodity - Show commodity | Commodity Show commodity Show commodity  |
| CommodityTranslation - Show commodity translation | CommodityTranslation Show commodity translation Show commodity translation  |
| CoupaPay-Invoice - Show Invoice | CoupaPay-Invoice Show Invoice Show Invoice  |
| Currency - Show currency | Currency Show currency Show currency  |
| DataFileSource - Show data file source | DataFileSource Show data file source Show data file source  |
| Department - Show department | Department Show department Show department  |
| EasyFormResponse - Show an easy form response | EasyFormResponse Show an easy form response Show an easy form response  |
| EasyFormResponse - Show an easy form response | EasyFormResponse Show an easy form response Show an easy form response  |
| EasyFormResponse - Show an easy form response | EasyFormResponse Show an easy form response Show an easy form response  |
| EasyFormResponse - Show an easy form response | EasyFormResponse Show an easy form response Show an easy form response  |
| ExchangeRate - Show exchange rate | ExchangeRate Show exchange rate Show exchange rate  |
| ExpenseArtifact - Show expense artifact | ExpenseArtifact Show expense artifact Show expense artifact  |
| ExpenseArtifact - Show expense artifact | ExpenseArtifact Show expense artifact Show expense artifact  |
| ExpenseArtifact - Show expense artifact | ExpenseArtifact Show expense artifact Show expense artifact  |
| ExpenseArtifact - Show expense artifact | ExpenseArtifact Show expense artifact Show expense artifact  |
| Integration - Show integration | Integration Show integration Show integration  |
| IntegrationContact - Show integration contact | IntegrationContact Show integration contact Show integration contact  |
| IntegrationError - Show integration error | IntegrationError Show integration error Show integration error  |
| IntegrationHistoryRecord - Show integration history record | IntegrationHistoryRecord Show integration history record Show integration history record  |
| IntegrationRun - Show integration run | IntegrationRun Show integration run Show integration run  |
| InventoryTransaction - /inventory_adjustments/{id} | InventoryTransaction  /inventory_adjustments/{id}  |
| InventoryTransaction - /inventory_consumptions/{id} | InventoryTransaction  /inventory_consumptions/{id}  |
| InventoryTransaction - /inventory_transfers/{id} | InventoryTransaction  /inventory_transfers/{id}  |
| InventoryTransaction - /receiving_transactions/{id} | InventoryTransaction  /receiving_transactions/{id}  |
| Item - Show item | Item Show item Show item  |
| Lookup - Show lookup | Lookup Show lookup Show lookup  |
| LookupValue - Show lookup value | LookupValue Show lookup value Show lookup value  |
| ObjectInstance - Show custom object instance | ObjectInstance Show custom object instance Show custom object instance  |
| ObjectTranslation - /uoms/{uom_id}/translations/{id} | ObjectTranslation  /uoms/{uom_id}/translations/{id}  |
| OrderLine - /receivable_purchase_order_lines/{id} | OrderLine  /receivable_purchase_order_lines/{id}  |
| Payables-External-Payable - Show External Payable | Payables-External-Payable Show External Payable Show External Payable  |
| Payables-Invoice - Show Invoice Payable | Payables-Invoice Show Invoice Payable Show Invoice Payable  |
| PaymentTerm - Show payment term | PaymentTerm Show payment term Show payment term  |
| Project - Show Task | Project Show Task Show Task  |
| ProjectMembership - Show a project membership | ProjectMembership Show a project membership Show a project membership  |
| QuoteAwardApprovable - /quote_award_approvables/{id} | QuoteAwardApprovable  /quote_award_approvables/{id}  |
| QuoteRequestApprovable - /quote_request_approvables/{id} | QuoteRequestApprovable  /quote_request_approvables/{id}  |
| ReceiptRequest - Show receipt request | ReceiptRequest Show receipt request Show receipt request  |
| RemitToAddress - Show remit to address | RemitToAddress Show remit to address Show remit to address  |
| RemitToAddress - Show remit to address | RemitToAddress Show remit to address Show remit to address  |
| ShippingTerm - Show shipping term | ShippingTerm Show shipping term Show shipping term  |
| Supplier - Show background job status | Supplier Show background job status Show background job status  |
| Supplier - Show supplier | Supplier Show supplier Show supplier  |
| SupplierInformation - Show | SupplierInformation Show Show  |
| SupplierInformationSite - Show supplier information site | SupplierInformationSite Show supplier information site Show supplier information site  |
| SupplierInformationSite - Show supplier information site | SupplierInformationSite Show supplier information site Show supplier information site  |
| SupplierInformationTaxRegistration - Show supplier information tax registration | SupplierInformationTaxRegistration Show supplier information tax registration Show supplier information tax registration  |
| SupplierSharingSetting - Show supplier sharing setting | SupplierSharingSetting Show supplier sharing setting Show supplier sharing setting  |
| SupplierSite - Show supplier site | SupplierSite Show supplier site Show supplier site  |
| SupplierSite - Show supplier site | SupplierSite Show supplier site Show supplier site  |
| Task - /projects/{project_id}/tasks/{id} | Task  /projects/{project_id}/tasks/{id}  |
| Task - /tasks/{id} | Task  /tasks/{id}  |
| Task - /user_groups/{user_group_id}/tasks/{id} | Task  /user_groups/{user_group_id}/tasks/{id}  |
| TaxRegistration - Show tax registration | TaxRegistration Show tax registration Show tax registration  |
| Uom - Show role | Uom Show role Show role  |
| User - Show user | User Show user Show user  |
| UserGroup - Show User Group | UserGroup Show User Group Show User Group  |
| UserGroupMembership - Show an user group membership | UserGroupMembership Show an user group membership Show an user group membership  |


### PATCH Operation Types
| Label | Help Text |
| --- | --- |
| Account - Update account (PATCH) | Account Update account Update account  |
| AccountValidationRule - Update account validation (PATCH) | AccountValidationRule Update account validation Update account validation  |
| Address - Update address (PATCH) | Address Update address Update address  |
| Address - Update user's association to an address (PATCH) | Address Update user's association to an address Update user's association to an address  |
| Approval - Perform Approve action on an approval (PATCH) | Approval Perform Approve action on an approval Perform Approve action on an approval  |
| Approval - Perform Hold action on an approval (PATCH) | Approval Perform Hold action on an approval Perform Hold action on an approval  |
| Approval - Perform Reject action on an approval (PATCH) | Approval Perform Reject action on an approval Perform Reject action on an approval  |
| Approval - Update approval (PATCH) | Approval Update approval Update approval  |
| BudgetLine - Update budget line (PATCH) | BudgetLine Update budget line Update budget line  |
| BusinessEntity - Update business entity (PATCH) | BusinessEntity Update business entity Update business entity  |
| BusinessGroup - Update business group (PATCH) | BusinessGroup Update business group Update business group  |
| Commodity - Update commodity (PATCH) | Commodity Update commodity Update commodity  |
| CommodityTranslation - Update commodity translation (PATCH) | CommodityTranslation Update commodity translation Update commodity translation  |
| Department - Update department (PATCH) | Department Update department Update department  |
| EasyFormResponse - Update easy form response (PATCH) | EasyFormResponse Update easy form response Update easy form response  |
| EasyFormResponse - Update easy form response (PATCH) | EasyFormResponse Update easy form response Update easy form response  |
| ExchangeRate - Update exchange rate (PATCH) | ExchangeRate Update exchange rate Update exchange rate  |
| ExpenseArtifact - Update expense artifact (PATCH) | ExpenseArtifact Update expense artifact Update expense artifact  |
| ExpenseArtifact - Update expense artifact (PATCH) | ExpenseArtifact Update expense artifact Update expense artifact  |
| ExpenseArtifact - Update expense artifact (PATCH) | ExpenseArtifact Update expense artifact Update expense artifact  |
| ExpenseArtifact - Update expense artifact (PATCH) | ExpenseArtifact Update expense artifact Update expense artifact  |
| Integration - Update integration (PATCH) | Integration Update integration Update integration  |
| IntegrationContact - Update integration contact (PATCH) | IntegrationContact Update integration contact Update integration contact  |
| IntegrationError - Update integration error (PATCH) | IntegrationError Update integration error Update integration error  |
| IntegrationHistoryRecord - Update integration history record (PATCH) | IntegrationHistoryRecord Update integration history record Update integration history record  |
| IntegrationRun - Update integration run (PATCH) | IntegrationRun Update integration run Update integration run  |
| InventoryTransaction - /inventory_adjustments/{id} (PATCH) | InventoryTransaction  /inventory_adjustments/{id}  |
| InventoryTransaction - /inventory_consumptions/{id} (PATCH) | InventoryTransaction  /inventory_consumptions/{id}  |
| InventoryTransaction - /inventory_transfers/{id} (PATCH) | InventoryTransaction  /inventory_transfers/{id}  |
| InventoryTransaction - /receiving_transactions/{id} (PATCH) | InventoryTransaction  /receiving_transactions/{id}  |
| Item - Update item (PATCH) | Item Update item Update item  |
| Lookup - Update lookup (PATCH) | Lookup Update lookup Update lookup  |
| LookupValue - Update lookup value (PATCH) | LookupValue Update lookup value Update lookup value  |
| ObjectInstance - Update custom object instance (PATCH) | ObjectInstance Update custom object instance Update custom object instance  |
| ObjectTranslation - /uoms/{uom_id}/translations/{id} (PATCH) | ObjectTranslation  /uoms/{uom_id}/translations/{id}  |
| Payables-External-Payable - Update External Payable (PATCH) | Payables-External-Payable Update External Payable Update External Payable  |
| Payables-External-Payable - Void External Payable (PATCH) | Payables-External-Payable Void External Payable Void External Payable  |
| Payables-Invoice - /payables/invoices/{id}/track_in_coupa (PATCH) | Payables-Invoice  /payables/invoices/{id}/track_in_coupa  |
| Payables-Invoice - Mark Invoice Payable as Exported (PATCH) | Payables-Invoice Mark Invoice Payable as Exported Mark Invoice Payable as Exported  |
| Payables-Invoice - Mark Invoice Payable as Paid and stop tracking in ... (PATCH) | Payables-Invoice Mark Invoice Payable as Paid and stop tracking in Coupa Mark Invoice Payable as Paid and stop tracking in Coupa  |
| Payables-Invoice - Stop Tracking Invoice Payable in Coupa (PATCH) | Payables-Invoice Stop Tracking Invoice Payable in Coupa Stop Tracking Invoice Payable in Coupa  |
| PaymentTerm - Update payment term (PATCH) | PaymentTerm Update payment term Update payment term  |
| Project - Update Task (PATCH) | Project Update Task Update Task  |
| ProjectMembership - Update a project membership (PATCH) | ProjectMembership Update a project membership Update a project membership  |
| RemitToAddress - Update remit to address (PATCH) | RemitToAddress Update remit to address Update remit to address  |
| RemitToAddress - Update remit to address (PATCH) | RemitToAddress Update remit to address Update remit to address  |
| ShippingTerm - Update shipping term (PATCH) | ShippingTerm Update shipping term Update shipping term  |
| Supplier - Update supplier (PATCH) | Supplier Update supplier Update supplier  |
| SupplierInformation - Update (PATCH) | SupplierInformation Update Update  |
| SupplierInformationSite - Update supplier information site (PATCH) | SupplierInformationSite Update supplier information site Update supplier information site  |
| SupplierInformationSite - Update supplier information site (PATCH) | SupplierInformationSite Update supplier information site Update supplier information site  |
| SupplierInformationTaxRegistration - Update supplier information tax registration (PATCH) | SupplierInformationTaxRegistration Update supplier information tax registration Update supplier information tax registration  |
| SupplierSharingSetting - Update supplier sharing setting (PATCH) | SupplierSharingSetting Update supplier sharing setting Update supplier sharing setting  |
| SupplierSite - Update supplier site (PATCH) | SupplierSite Update supplier site Update supplier site  |
| SupplierSite - Update supplier site (PATCH) | SupplierSite Update supplier site Update supplier site  |
| Task - /projects/{project_id}/tasks/{id} (PATCH) | Task  /projects/{project_id}/tasks/{id}  |
| Task - /tasks/{id} (PATCH) | Task  /tasks/{id}  |
| Task - /user_groups/{user_group_id}/tasks/{id} (PATCH) | Task  /user_groups/{user_group_id}/tasks/{id}  |
| TaxRegistration - Update tax registration (PATCH) | TaxRegistration Update tax registration Update tax registration  |
| User - Update user (PATCH) | User Update user Update user  |
| UserGroup - Update User Group (PATCH) | UserGroup Update User Group Update User Group  |
| UserGroupMembership - Update an user group membership (PATCH) | UserGroupMembership Update an user group membership Update an user group membership  |


### POST Operation Types
| Label | Help Text |
| --- | --- |
| Account - /accounts/validate_account | Account  /accounts/validate_account  |
| Account - Create account | Account Create account Create account  |
| AccountType - Copy/Clone existing chart of accounts | AccountType Copy/Clone existing chart of accounts Copy/Clone existing chart of accounts  |
| AccountType - Create chart of accounts | AccountType Create chart of accounts Create chart of accounts  |
| AccountValidationRule - Create account validation | AccountValidationRule Create account validation Create account validation  |
| Address - Create address | Address Create address Create address  |
| Address - Create user association to an address | Address Create user association to an address Create user association to an address  |
| Approval - Create approval | Approval Create approval Create approval  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| Attachment - Create attachment | Attachment Create attachment Create attachment  |
| BudgetLine - Create budget line | BudgetLine Create budget line Create budget line  |
| BudgetLineAdjustment - You cannot create budget line adjustments via this... | BudgetLineAdjustment You cannot create budget line adjustments via this API (See budget_lines/:id/adjust) You cannot create budget line adjustments via this API (See budget_lines/:id/adjust)  |
| BudgetLineAdjustment - You cannot create budget line adjustments via this... | BudgetLineAdjustment You cannot create budget line adjustments via this API (See budget_lines/:id/adjust) You cannot create budget line adjustments via this API (See budget_lines/:id/adjust)  |
| BusinessEntity - Create business entity | BusinessEntity Create business entity Create business entity  |
| BusinessGroup - Create business group | BusinessGroup Create business group Create business group  |
| Comment - Create comment | Comment Create comment Create comment  |
| Comment - Create comment | Comment Create comment Create comment  |
| Comment - Create comment | Comment Create comment Create comment  |
| Comment - Create comment | Comment Create comment Create comment  |
| Comment - Create comment | Comment Create comment Create comment  |
| Commodity - Create commodity | Commodity Create commodity Create commodity  |
| CommodityTranslation - Create commodity translation | CommodityTranslation Create commodity translation Create commodity translation  |
| DataFileSource - /data_file_sources/load_file | DataFileSource  /data_file_sources/load_file  |
| DataFileSource - Upload flat file and process via data file source | DataFileSource Upload flat file and process via data file source Upload flat file and process via data file source  |
| Department - Create department | Department Create department Create department  |
| ExchangeRate - Create exchange rate | ExchangeRate Create exchange rate Create exchange rate  |
| Integration - Create integration | Integration Create integration Create integration  |
| IntegrationContact - Create integration contact | IntegrationContact Create integration contact Create integration contact  |
| IntegrationError - /integration_errors/create_alert | IntegrationError  /integration_errors/create_alert  |
| IntegrationError - Create integration error | IntegrationError Create integration error Create integration error  |
| IntegrationHistoryRecord - Create integration history record | IntegrationHistoryRecord Create integration history record Create integration history record  |
| IntegrationHistoryRecord - The acknowledge action is not documented ... yet | IntegrationHistoryRecord The acknowledge action is not documented ... yet The acknowledge action is not documented ... yet  |
| IntegrationHistoryRecord - The create_alert action is not documented ... yet | IntegrationHistoryRecord The create_alert action is not documented ... yet The create_alert action is not documented ... yet  |
| IntegrationHistoryRecord - The create_alert_and_mark_exported action is not d... | IntegrationHistoryRecord The create_alert_and_mark_exported action is not documented ... yet The create_alert_and_mark_exported action is not documented ... yet  |
| IntegrationHistoryRecord - The mark_exported action is not documented ... yet | IntegrationHistoryRecord The mark_exported action is not documented ... yet The mark_exported action is not documented ... yet  |
| IntegrationRun - Create integration run with status of pending | IntegrationRun Create integration run with status of pending Create integration run with status of pending  |
| InventoryTransaction - /inventory_adjustments | InventoryTransaction  /inventory_adjustments  |
| InventoryTransaction - /inventory_consumptions | InventoryTransaction  /inventory_consumptions  |
| InventoryTransaction - /inventory_transfers | InventoryTransaction  /inventory_transfers  |
| InventoryTransaction - /receiving_transactions | InventoryTransaction  /receiving_transactions  |
| Item - Create item | Item Create item Create item  |
| Lookup - Create lookup | Lookup Create lookup Create lookup  |
| LookupValue - Create lookup value | LookupValue Create lookup value Create lookup value  |
| ObjectInstance - Create custom object instance | ObjectInstance Create custom object instance Create custom object instance  |
| ObjectTranslation - /uoms/{uom_id}/translations | ObjectTranslation  /uoms/{uom_id}/translations  |
| Payables-External-Payable - Create External Payable | Payables-External-Payable Create External Payable Create External Payable  |
| PaymentTerm - Create payment term | PaymentTerm Create payment term Create payment term  |
| Project - /projects/{id}/cancelled | Project  /projects/{id}/cancelled  |
| Project - /projects/{id}/complete | Project  /projects/{id}/complete  |
| Project - /projects/{id}/draft | Project  /projects/{id}/draft  |
| Project - /projects/{id}/in_progress | Project  /projects/{id}/in_progress  |
| Project - /projects/{id}/planned | Project  /projects/{id}/planned  |
| Project - Create Task | Project Create Task Create Task  |
| ProjectMembership - Create a project membership | ProjectMembership Create a project membership Create a project membership  |
| PurchaseOrderLineReceiptRequest - /purchase_order_line_receipt_requests | PurchaseOrderLineReceiptRequest  /purchase_order_line_receipt_requests  |
| QuoteSupplier - /quote_requests/{quote_request_id}/pending_quote_s... | QuoteSupplier  /quote_requests/{quote_request_id}/pending_quote_suppliers  |
| ReceiptRequest - Submit receipt request for approval | ReceiptRequest Submit receipt request for approval Submit receipt request for approval  |
| ReceiptRequest - Widthdraw receipt request | ReceiptRequest Widthdraw receipt request Widthdraw receipt request  |
| RemitToAddress - Create remit to address | RemitToAddress Create remit to address Create remit to address  |
| RemitToAddress - Create remit to address | RemitToAddress Create remit to address Create remit to address  |
| ShippingTerm - Create shipping term | ShippingTerm Create shipping term Create shipping term  |
| Supplier - Create background job to invite many suppliers | Supplier Create background job to invite many suppliers Create background job to invite many suppliers  |
| Supplier - Create supplier | Supplier Create supplier Create supplier  |
| Supplier - Create supplier user preferences | Supplier Create supplier user preferences Create supplier user preferences  |
| Supplier - Sync supplier user locale | Supplier Sync supplier user locale Sync supplier user locale  |
| SupplierInformation - Create | SupplierInformation Create Create  |
| SupplierInformationSite - Create supplier information site | SupplierInformationSite Create supplier information site Create supplier information site  |
| SupplierInformationSite - Create supplier information site | SupplierInformationSite Create supplier information site Create supplier information site  |
| SupplierInformationTaxRegistration - Create supplier information tax registration | SupplierInformationTaxRegistration Create supplier information tax registration Create supplier information tax registration  |
| SupplierSharingSetting - Create supplier sharing setting | SupplierSharingSetting Create supplier sharing setting Create supplier sharing setting  |
| SupplierSite - Create supplier site | SupplierSite Create supplier site Create supplier site  |
| SupplierSite - Create supplier site | SupplierSite Create supplier site Create supplier site  |
| Task - /projects/{project_id}/tasks | Task  /projects/{project_id}/tasks  |
| Task - /tasks | Task  /tasks  |
| Task - /user_groups/{user_group_id}/tasks | Task  /user_groups/{user_group_id}/tasks  |
| TaxRegistration - Create tax registration | TaxRegistration Create tax registration Create tax registration  |
| User - Create user | User Create user Create user  |
| User - Update avatar | User Update avatar Update avatar  |
| UserGroup - Create User Group | UserGroup Create User Group Create User Group  |
| UserGroupMembership - Create an user group membership | UserGroupMembership Create an user group membership Create an user group membership  |


### PUT Operation Types
| Label | Help Text |
| --- | --- |
| Account - Update account (PUT) | Account Update account Update account  |
| AccountValidationRule - Update account validation (PUT) | AccountValidationRule Update account validation Update account validation  |
| Address - Update address (PUT) | Address Update address Update address  |
| Address - Update user's association to an address (PUT) | Address Update user's association to an address Update user's association to an address  |
| Approval - Perform Approve action on an approval (PUT) | Approval Perform Approve action on an approval Perform Approve action on an approval  |
| Approval - Perform Hold action on an approval (PUT) | Approval Perform Hold action on an approval Perform Hold action on an approval  |
| Approval - Perform Reject action on an approval (PUT) | Approval Perform Reject action on an approval Perform Reject action on an approval  |
| Approval - Update approval (PUT) | Approval Update approval Update approval  |
| BudgetLine - Creat budget line adjustment for budget line (PUT) | BudgetLine Creat budget line adjustment for budget line Creat budget line adjustment for budget line  |
| BudgetLine - Update budget line (PUT) | BudgetLine Update budget line Update budget line  |
| BusinessEntity - Update business entity (PUT) | BusinessEntity Update business entity Update business entity  |
| BusinessGroup - Add contract to business group (PUT) | BusinessGroup Add contract to business group Add contract to business group  |
| BusinessGroup - Add contract to business group (PUT) | BusinessGroup Add contract to business group Add contract to business group  |
| BusinessGroup - Add supplier to business group (PUT) | BusinessGroup Add supplier to business group Add supplier to business group  |
| BusinessGroup - Add supplier to business group (PUT) | BusinessGroup Add supplier to business group Add supplier to business group  |
| BusinessGroup - Add user to business group (PUT) | BusinessGroup Add user to business group Add user to business group  |
| BusinessGroup - Add user to business group (PUT) | BusinessGroup Add user to business group Add user to business group  |
| BusinessGroup - Remove contract from all business groups (PUT) | BusinessGroup Remove contract from all business groups Remove contract from all business groups  |
| BusinessGroup - Remove contract from business group (PUT) | BusinessGroup Remove contract from business group Remove contract from business group  |
| BusinessGroup - Remove contract from business group (PUT) | BusinessGroup Remove contract from business group Remove contract from business group  |
| BusinessGroup - Remove supplier from all business groups (PUT) | BusinessGroup Remove supplier from all business groups Remove supplier from all business groups  |
| BusinessGroup - Remove supplier from business group (PUT) | BusinessGroup Remove supplier from business group Remove supplier from business group  |
| BusinessGroup - Remove supplier from business group (PUT) | BusinessGroup Remove supplier from business group Remove supplier from business group  |
| BusinessGroup - Remove user from business group (PUT) | BusinessGroup Remove user from business group Remove user from business group  |
| BusinessGroup - Remove user from business group (PUT) | BusinessGroup Remove user from business group Remove user from business group  |
| BusinessGroup - Update business group (PUT) | BusinessGroup Update business group Update business group  |
| Commodity - Update commodity (PUT) | Commodity Update commodity Update commodity  |
| CommodityTranslation - Update commodity translation (PUT) | CommodityTranslation Update commodity translation Update commodity translation  |
| Department - Update department (PUT) | Department Update department Update department  |
| EasyFormResponse - Approve an easy form response (PUT) | EasyFormResponse Approve an easy form response Approve an easy form response  |
| EasyFormResponse - Approve an easy form response (PUT) | EasyFormResponse Approve an easy form response Approve an easy form response  |
| EasyFormResponse - Manually add an approver for an easy form response (PUT) | EasyFormResponse Manually add an approver for an easy form response Manually add an approver for an easy form response  |
| EasyFormResponse - Manually add an approver for an easy form response (PUT) | EasyFormResponse Manually add an approver for an easy form response Manually add an approver for an easy form response  |
| EasyFormResponse - Remove an apporver who was manually added (PUT) | EasyFormResponse Remove an apporver who was manually added Remove an apporver who was manually added  |
| EasyFormResponse - Remove an apporver who was manually added (PUT) | EasyFormResponse Remove an apporver who was manually added Remove an apporver who was manually added  |
| EasyFormResponse - Review an easy form response (PUT) | EasyFormResponse Review an easy form response Review an easy form response  |
| EasyFormResponse - Review an easy form response (PUT) | EasyFormResponse Review an easy form response Review an easy form response  |
| EasyFormResponse - Update easy form response (PUT) | EasyFormResponse Update easy form response Update easy form response  |
| EasyFormResponse - Update easy form response (PUT) | EasyFormResponse Update easy form response Update easy form response  |
| ExchangeRate - Update exchange rate (PUT) | ExchangeRate Update exchange rate Update exchange rate  |
| ExpenseArtifact - Update expense artifact (PUT) | ExpenseArtifact Update expense artifact Update expense artifact  |
| ExpenseArtifact - Update expense artifact (PUT) | ExpenseArtifact Update expense artifact Update expense artifact  |
| ExpenseArtifact - Update expense artifact (PUT) | ExpenseArtifact Update expense artifact Update expense artifact  |
| ExpenseArtifact - Update expense artifact (PUT) | ExpenseArtifact Update expense artifact Update expense artifact  |
| Integration - Update integration (PUT) | Integration Update integration Update integration  |
| IntegrationContact - Add Supplier User Integration Contact (PUT) | IntegrationContact Add Supplier User Integration Contact Add Supplier User Integration Contact  |
| IntegrationContact - Remove Supplier User Integration Contact (PUT) | IntegrationContact Remove Supplier User Integration Contact Remove Supplier User Integration Contact  |
| IntegrationContact - Update integration contact (PUT) | IntegrationContact Update integration contact Update integration contact  |
| IntegrationError - Resolve integration error (PUT) | IntegrationError Resolve integration error Resolve integration error  |
| IntegrationError - Unresolve resolved integration error (PUT) | IntegrationError Unresolve resolved integration error Unresolve resolved integration error  |
| IntegrationError - Update integration error (PUT) | IntegrationError Update integration error Update integration error  |
| IntegrationHistoryRecord - Resolve integration history record (PUT) | IntegrationHistoryRecord Resolve integration history record Resolve integration history record  |
| IntegrationHistoryRecord - Unresolve integration history record (PUT) | IntegrationHistoryRecord Unresolve integration history record Unresolve integration history record  |
| IntegrationHistoryRecord - Update integration history record (PUT) | IntegrationHistoryRecord Update integration history record Update integration history record  |
| IntegrationRun - Set integration run status as failed (PUT) | IntegrationRun Set integration run status as failed Set integration run status as failed  |
| IntegrationRun - Set integration run status as paused (PUT) | IntegrationRun Set integration run status as paused Set integration run status as paused  |
| IntegrationRun - Set integration run status as pending (PUT) | IntegrationRun Set integration run status as pending Set integration run status as pending  |
| IntegrationRun - Set integration run status as running (PUT) | IntegrationRun Set integration run status as running Set integration run status as running  |
| IntegrationRun - Set integration run status as successful if no err... (PUT) | IntegrationRun Set integration run status as successful if no errors exist Set integration run status as successful if no errors exist  |
| IntegrationRun - Set integration run status as successful if no err... (PUT) | IntegrationRun Set integration run status as successful if no errors exist Set integration run status as successful if no errors exist  |
| IntegrationRun - Update integration run (PUT) | IntegrationRun Update integration run Update integration run  |
| InventoryTransaction - /inventory_adjustments/{id} (PUT) | InventoryTransaction  /inventory_adjustments/{id}  |
| InventoryTransaction - /inventory_consumptions/{id} (PUT) | InventoryTransaction  /inventory_consumptions/{id}  |
| InventoryTransaction - /inventory_transfers/{id} (PUT) | InventoryTransaction  /inventory_transfers/{id}  |
| InventoryTransaction - /receiving_transactions/{id} (PUT) | InventoryTransaction  /receiving_transactions/{id}  |
| Item - Update item (PUT) | Item Update item Update item  |
| Lookup - Update lookup (PUT) | Lookup Update lookup Update lookup  |
| LookupValue - Update lookup value (PUT) | LookupValue Update lookup value Update lookup value  |
| ObjectInstance - Update custom object instance (PUT) | ObjectInstance Update custom object instance Update custom object instance  |
| ObjectTranslation - /uoms/{uom_id}/translations/{id} (PUT) | ObjectTranslation  /uoms/{uom_id}/translations/{id}  |
| Payables-External-Payable - Update External Payable (PUT) | Payables-External-Payable Update External Payable Update External Payable  |
| Payables-External-Payable - Void External Payable (PUT) | Payables-External-Payable Void External Payable Void External Payable  |
| PaymentTerm - Update payment term (PUT) | PaymentTerm Update payment term Update payment term  |
| Project - Update Task (PUT) | Project Update Task Update Task  |
| ProjectMembership - Update a project membership (PUT) | ProjectMembership Update a project membership Update a project membership  |
| RemitToAddress - Update remit to address (PUT) | RemitToAddress Update remit to address Update remit to address  |
| RemitToAddress - Update remit to address (PUT) | RemitToAddress Update remit to address Update remit to address  |
| RequisitionHeader - /requisitions/{id}/update_without_validation (PUT) | RequisitionHeader  /requisitions/{id}/update_without_validation  |
| ShippingTerm - Update shipping term (PUT) | ShippingTerm Update shipping term Update shipping term  |
| Supplier - Update supplier (PUT) | Supplier Update supplier Update supplier  |
| SupplierInformation - Update (PUT) | SupplierInformation Update Update  |
| SupplierInformationSite - Update supplier information site (PUT) | SupplierInformationSite Update supplier information site Update supplier information site  |
| SupplierInformationSite - Update supplier information site (PUT) | SupplierInformationSite Update supplier information site Update supplier information site  |
| SupplierInformationTaxRegistration - Update supplier information tax registration (PUT) | SupplierInformationTaxRegistration Update supplier information tax registration Update supplier information tax registration  |
| SupplierSharingSetting - Update supplier sharing setting (PUT) | SupplierSharingSetting Update supplier sharing setting Update supplier sharing setting  |
| SupplierSite - Update supplier site (PUT) | SupplierSite Update supplier site Update supplier site  |
| SupplierSite - Update supplier site (PUT) | SupplierSite Update supplier site Update supplier site  |
| Task - /projects/{project_id}/tasks/{id} (PUT) | Task  /projects/{project_id}/tasks/{id}  |
| Task - /tasks/{id} (PUT) | Task  /tasks/{id}  |
| Task - /user_groups/{user_group_id}/tasks/{id} (PUT) | Task  /user_groups/{user_group_id}/tasks/{id}  |
| TaxRegistration - Update tax registration (PUT) | TaxRegistration Update tax registration Update tax registration  |
| User - Update avatar (PUT) | User Update avatar Update avatar  |
| User - Update user (PUT) | User Update user Update user  |
| UserGroup - Update User Group (PUT) | UserGroup Update User Group Update User Group  |
| UserGroupMembership - Update an user group membership (PUT) | UserGroupMembership Update an user group membership Update an user group membership  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Account - /accounts/user_accounts | Account  /accounts/user_accounts  |
| Account - Favorites | Account Favorites Favorites  |
| Account - Query accounts | Account Query accounts Query accounts  |
| Account - Recent | Account Recent Recent  |
| AccountType - Query chart of accounts | AccountType Query chart of accounts Query chart of accounts  |
| AccountValidationRule - Query account validations | AccountValidationRule Query account validations Query account validations  |
| Address - Query addresses | Address Query addresses Query addresses  |
| Address - Query user's associations to addresses | Address Query user's associations to addresses Query user's associations to addresses  |
| Announcement - Query announcements | Announcement Query announcements Query announcements  |
| Approval - Autocomplete approver/watcher search | Approval Autocomplete approver/watcher search Autocomplete approver/watcher search  |
| Approval - Query approvals | Approval Query approvals Query approvals  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| Attachment - Query attachments | Attachment Query attachments Query attachments  |
| BudgetLine - Query budget lines | BudgetLine Query budget lines Query budget lines  |
| BudgetLineAdjustment - Query budget line adjustments | BudgetLineAdjustment Query budget line adjustments Query budget line adjustments  |
| BudgetLineAdjustment - Query budget line adjustments | BudgetLineAdjustment Query budget line adjustments Query budget line adjustments  |
| BusinessEntity - Query business entities | BusinessEntity Query business entities Query business entities  |
| BusinessGroup - Query business groups | BusinessGroup Query business groups Query business groups  |
| BusinessGroup - Query contract's associations to business groups | BusinessGroup Query contract's associations to business groups Query contract's associations to business groups  |
| BusinessGroup - Query supplier's associations to business groups | BusinessGroup Query supplier's associations to business groups Query supplier's associations to business groups  |
| BusinessGroup - Query user's associations to business groups | BusinessGroup Query user's associations to business groups Query user's associations to business groups  |
| Comment - Query comments | Comment Query comments Query comments  |
| Comment - Query comments | Comment Query comments Query comments  |
| Comment - Query comments | Comment Query comments Query comments  |
| Comment - Query comments | Comment Query comments Query comments  |
| Comment - Query comments | Comment Query comments Query comments  |
| Comment - Query comments | Comment Query comments Query comments  |
| Commodity - Query commodities | Commodity Query commodities Query commodities  |
| CommodityTranslation - Query commodity translations | CommodityTranslation Query commodity translations Query commodity translations  |
| CoupaPay-Invoice - Query Invoices | CoupaPay-Invoice Query Invoices Query Invoices  |
| Currency - Query currencies | Currency Query currencies Query currencies  |
| DataFileSource - Query data file sources | DataFileSource Query data file sources Query data file sources  |
| Department - Query departments | Department Query departments Query departments  |
| EasyFormResponse - Query easy form responses | EasyFormResponse Query easy form responses Query easy form responses  |
| EasyFormResponse - Query easy form responses | EasyFormResponse Query easy form responses Query easy form responses  |
| ExchangeRate - Query exchange rates | ExchangeRate Query exchange rates Query exchange rates  |
| ExpenseArtifact - Query expense artifactes | ExpenseArtifact Query expense artifactes Query expense artifactes  |
| ExpenseArtifact - Query expense artifactes | ExpenseArtifact Query expense artifactes Query expense artifactes  |
| ExpenseArtifact - Query expense artifactes | ExpenseArtifact Query expense artifactes Query expense artifactes  |
| ExpenseArtifact - Query expense artifactes | ExpenseArtifact Query expense artifactes Query expense artifactes  |
| InspectionCode - /inspection_codes | InspectionCode  /inspection_codes  |
| Integration - Query integrations | Integration Query integrations Query integrations  |
| IntegrationContact - Query integration contacts | IntegrationContact Query integration contacts Query integration contacts  |
| IntegrationError - Query integration errors | IntegrationError Query integration errors Query integration errors  |
| IntegrationHistoryRecord - Query integration history records | IntegrationHistoryRecord Query integration history records Query integration history records  |
| IntegrationRun - Query integration runs | IntegrationRun Query integration runs Query integration runs  |
| InventoryTransaction - /inventory_adjustments | InventoryTransaction  /inventory_adjustments  |
| InventoryTransaction - /inventory_consumptions | InventoryTransaction  /inventory_consumptions  |
| InventoryTransaction - /inventory_transfers | InventoryTransaction  /inventory_transfers  |
| InventoryTransaction - /receiving_transactions | InventoryTransaction  /receiving_transactions  |
| Item - /items/{id}/image | Item  /items/{id}/image  |
| Item - Query items | Item Query items Query items  |
| Lookup - Query lookups | Lookup Query lookups Query lookups  |
| LookupValue - /lookup_values/user_lookup_values | LookupValue  /lookup_values/user_lookup_values  |
| LookupValue - Query lookup values | LookupValue Query lookup values Query lookup values  |
| ObjectInstance - Query custom object instance | ObjectInstance Query custom object instance Query custom object instance  |
| ObjectTranslation - /uoms/{uom_id}/translations | ObjectTranslation  /uoms/{uom_id}/translations  |
| OrderLine - /receivable_purchase_order_lines | OrderLine  /receivable_purchase_order_lines  |
| Payables-External-Payable - Query External Payables | Payables-External-Payable Query External Payables Query External Payables  |
| Payables-Invoice - Query Invoice Payables | Payables-Invoice Query Invoice Payables Query Invoice Payables  |
| PaymentTerm - Query payment terms | PaymentTerm Query payment terms Query payment terms  |
| Project - Query Tasks | Project Query Tasks Query Tasks  |
| ProjectMembership - Query Project memberships | ProjectMembership Query Project memberships Query Project memberships  |
| QuoteSupplier - /quote_requests/{quote_request_id}/pending_quote_s... | QuoteSupplier  /quote_requests/{quote_request_id}/pending_quote_suppliers  |
| ReceiptRequest - List receipt requests | ReceiptRequest List receipt requests List receipt requests  |
| RemitToAddress - Query remit to addresss | RemitToAddress Query remit to addresss Query remit to addresss  |
| RemitToAddress - Query remit to addresss | RemitToAddress Query remit to addresss Query remit to addresss  |
| ShippingTerm - Query shipping terms | ShippingTerm Query shipping terms Query shipping terms  |
| Supplier - Query suppliers | Supplier Query suppliers Query suppliers  |
| SupplierInformation - Index | SupplierInformation Index Index  |
| SupplierInformationSite - Query supplier information sites | SupplierInformationSite Query supplier information sites Query supplier information sites  |
| SupplierInformationSite - Query supplier information sites | SupplierInformationSite Query supplier information sites Query supplier information sites  |
| SupplierSharingSetting - Query supplier sharing settings | SupplierSharingSetting Query supplier sharing settings Query supplier sharing settings  |
| SupplierSite - Query supplier sites | SupplierSite Query supplier sites Query supplier sites  |
| SupplierSite - Query supplier sites | SupplierSite Query supplier sites Query supplier sites  |
| Task - /projects/{project_id}/tasks | Task  /projects/{project_id}/tasks  |
| Task - /tasks | Task  /tasks  |
| Task - /user_groups/{user_group_id}/tasks | Task  /user_groups/{user_group_id}/tasks  |
| TaxRegistration - Query tax registrations | TaxRegistration Query tax registrations Query tax registrations  |
| Uom - Query UOMs | Uom Query UOMs Query UOMs  |
| User - Download avatar | User Download avatar Download avatar  |
| User - Download avatar | User Download avatar Download avatar  |
| User - Query users | User Query users Query users  |
| UserGroup - Query User Groups | UserGroup Query User Groups Query User Groups  |
| UserGroupMembership - Query User group memberships | UserGroupMembership Query User group memberships Query User group memberships  |


