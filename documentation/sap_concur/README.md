# SAP Concur Connector
The Boomi Connnector for SAP Concur provides easy access to the Concur REST API. For more information on the API refer to: https://developer.concur.com/api-reference/

# Connection Tab
## Connection Fields


#### Other SAP Concur REST API Swagger URL

This will override selections so you can provide a url to any swagger file other than those presented in an operation.

**Type** - string



#### URL

The URL for the Service service

**Type** - string



#### OAuth 2.0

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - oauth

# Operation Tab


## CREATE/EXECUTE
### Operation Fields


##### SAP Concur OpenAPI Service

The URL for the SAP Concur OpenAPI descriptor file

**Type** - string

###### Allowed Values

 * Allocations v3.0
 * Attendees v3.0
 * AttendeeTypes v3.0
 * Budget v4
 * ConcurRequest v4
 * ConnectionRequests v3.0
 * ConnectionRequests v3.2
 * DigitalTaxInvoices v3.0
 * Entries v3.0
 * EntryAttendeeAssociations v3.0
 * ExpenseGroupConfigurations v3.0
 * InvoicePay v4
 * Itemizations v3.0
 * LatestBookings v3.0
 * List v4
 * ListItem v4
 * ListItems v3.0
 * Lists v3.0
 * LocalizedData v3.0
 * Locations v3.0
 * Opportunities v3.0
 * PaymentRequest v3.0
 * PaymentRequestDigest v3.0
 * PurchaseOrderReceipts v3.0
 * PurchaseOrders v3.0
 * PurchaseRequest v4
 * QuickExpenses v4.0.3
 * ReceiptImages v3.0
 * Receipts v3.0
 * Reports v3.0
 * RequestGroupConfigurations v3.2
 * SalesTaxValidationRequest v3.0
 * Suppliers v3.0
 * Users v3.0
 * VendorBank v3.0
 * VendorGroup v3.0
 * Vendors v3.0


## UPDATE
### Operation Fields


##### SAP Concur OpenAPI Service

The URL for the SAP Concur OpenAPI descriptor file

**Type** - string

###### Allowed Values

 * Allocations v3.0
 * Attendees v3.0
 * AttendeeTypes v3.0
 * Budget v4
 * ConcurRequest v4
 * ConnectionRequests v3.0
 * ConnectionRequests v3.2
 * DigitalTaxInvoices v3.0
 * Entries v3.0
 * EntryAttendeeAssociations v3.0
 * ExpenseGroupConfigurations v3.0
 * InvoicePay v4
 * Itemizations v3.0
 * LatestBookings v3.0
 * List v4
 * ListItem v4
 * ListItems v3.0
 * Lists v3.0
 * LocalizedData v3.0
 * Locations v3.0
 * Opportunities v3.0
 * PaymentRequest v3.0
 * PaymentRequestDigest v3.0
 * PurchaseOrderReceipts v3.0
 * PurchaseOrders v3.0
 * PurchaseRequest v4
 * QuickExpenses v4.0.3
 * ReceiptImages v3.0
 * Receipts v3.0
 * Reports v3.0
 * RequestGroupConfigurations v3.2
 * SalesTaxValidationRequest v3.0
 * Suppliers v3.0
 * Users v3.0
 * VendorBank v3.0
 * VendorGroup v3.0
 * Vendors v3.0


## GET
### Operation Fields


##### SAP Concur OpenAPI Service

The URL for the SAP Concur OpenAPI descriptor file

**Type** - string

###### Allowed Values

 * Allocations v3.0
 * Attendees v3.0
 * AttendeeTypes v3.0
 * Budget v4
 * ConcurRequest v4
 * ConnectionRequests v3.0
 * ConnectionRequests v3.2
 * DigitalTaxInvoices v3.0
 * Entries v3.0
 * EntryAttendeeAssociations v3.0
 * ExpenseGroupConfigurations v3.0
 * InvoicePay v4
 * Itemizations v3.0
 * LatestBookings v3.0
 * List v4
 * ListItem v4
 * ListItems v3.0
 * Lists v3.0
 * LocalizedData v3.0
 * Locations v3.0
 * Opportunities v3.0
 * PaymentRequest v3.0
 * PaymentRequestDigest v3.0
 * PurchaseOrderReceipts v3.0
 * PurchaseOrders v3.0
 * PurchaseRequest v4
 * QuickExpenses v4.0.3
 * ReceiptImages v3.0
 * Receipts v3.0
 * Reports v3.0
 * RequestGroupConfigurations v3.2
 * SalesTaxValidationRequest v3.0
 * Suppliers v3.0
 * Users v3.0
 * VendorBank v3.0
 * VendorGroup v3.0
 * Vendors v3.0


## DELETE
### Operation Fields


##### SAP Concur OpenAPI Service

The URL for the SAP Concur OpenAPI descriptor file

**Type** - string

###### Allowed Values

 * Allocations v3.0
 * Attendees v3.0
 * AttendeeTypes v3.0
 * Budget v4
 * ConcurRequest v4
 * ConnectionRequests v3.0
 * ConnectionRequests v3.2
 * DigitalTaxInvoices v3.0
 * Entries v3.0
 * EntryAttendeeAssociations v3.0
 * ExpenseGroupConfigurations v3.0
 * InvoicePay v4
 * Itemizations v3.0
 * LatestBookings v3.0
 * List v4
 * ListItem v4
 * ListItems v3.0
 * Lists v3.0
 * LocalizedData v3.0
 * Locations v3.0
 * Opportunities v3.0
 * PaymentRequest v3.0
 * PaymentRequestDigest v3.0
 * PurchaseOrderReceipts v3.0
 * PurchaseOrders v3.0
 * PurchaseRequest v4
 * QuickExpenses v4.0.3
 * ReceiptImages v3.0
 * Receipts v3.0
 * Reports v3.0
 * RequestGroupConfigurations v3.2
 * SalesTaxValidationRequest v3.0
 * Suppliers v3.0
 * Users v3.0
 * VendorBank v3.0
 * VendorGroup v3.0
 * Vendors v3.0


## QUERY
### Operation Fields


##### SAP Concur OpenAPI Service

The URL for the SAP Concur OpenAPI descriptor file

**Type** - string

###### Allowed Values

 * Allocations v3.0
 * Attendees v3.0
 * AttendeeTypes v3.0
 * Budget v4
 * ConcurRequest v4
 * ConnectionRequests v3.0
 * ConnectionRequests v3.2
 * DigitalTaxInvoices v3.0
 * Entries v3.0
 * EntryAttendeeAssociations v3.0
 * ExpenseGroupConfigurations v3.0
 * InvoicePay v4
 * Itemizations v3.0
 * LatestBookings v3.0
 * List v4
 * ListItem v4
 * ListItems v3.0
 * Lists v3.0
 * LocalizedData v3.0
 * Locations v3.0
 * Opportunities v3.0
 * PaymentRequest v3.0
 * PaymentRequestDigest v3.0
 * PurchaseOrderReceipts v3.0
 * PurchaseOrders v3.0
 * PurchaseRequest v4
 * QuickExpenses v4.0.3
 * ReceiptImages v3.0
 * Receipts v3.0
 * Reports v3.0
 * RequestGroupConfigurations v3.2
 * SalesTaxValidationRequest v3.0
 * Suppliers v3.0
 * Users v3.0
 * VendorBank v3.0
 * VendorGroup v3.0
 * Vendors v3.0


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1


### Query Options


#### Fields

The connector does not support field selection. All fields will be returned by default.


#### Filters

The query filter supports any number of non-nested expressions which will be AND'ed together.

Example:
((foo lessThan 30) AND (baz lessThan 42))

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts




# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.
 * Additional URI Query Parameters
 * URL Suffix
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

## Object Operations Provided

| Object | Query | Get | Create | Update | Delete 
| --- |:---:|:---:|:---:|:---:|:---:|
| Allocations v3 | X | X |  |  |  |
| Attendees v3 | X | X | X | X | X |
| AttendeeTypes v3 | X | X | X | X | X |
| Budget v4 | X | X | X |  | X |
| ConcurRequest v4 | X | X | X | X | X |
| ConnectionRequests v3 | X | X | X | X | X |
| ConnectionRequests v3 | X | X | X | X |  |
| DigitalTaxInvoices v3 | X | X |  | X |  |
| Entries v3 | X | X | X | X | X |
| EntryAttendeeAssociations v3 | X | X | X | X | X |
| ExpenseGroupConfigurations v3 | X | X |  |  |  |
| InvoicePay v4 | X |  |  | X |  |
| Itemizations v3 | X | X | X | X | X |
| LatestBookings v3 | X |  |  |  |  |
| List v4 | X | X | X | X | X |
| ListItem v4 | X | X | X | X | X |
| ListItems v3 | X | X | X | X | X |
| Lists v3 | X | X | X | X |  |
| LocalizedData v3 | X |  |  |  |  |
| Locations v3 | X | X |  |  |  |
| Opportunities v3 | X |  |  |  |  |
| PaymentRequest v3 |  | X | X | X |  |
| PaymentRequestDigest v3 | X | X |  |  |  |
| PurchaseOrderReceipts v3 | X |  | X | X | X |
| PurchaseOrders v3 |  | X | X | X |  |
| PurchaseRequest v4 | X |  | X |  |  |
| QuickExpenses v4 |  |  | X |  |  |
| ReceiptImages v3 | X | X | X | X | X |
| Receipts v3 |  |  | X |  |  |
| Reports v3 | X | X | X | X |  |
| RequestGroupConfigurations v3 | X |  |  |  |  |
| SalesTaxValidationRequest v3 | X |  |  | X |  |
| Suppliers v3 | X | X |  |  |  |
| Users v3 | X |  |  |  |  |
| VendorBank v3 |  |  |  | X |  |
| VendorGroup v3 |  |  |  | X | X |
| Vendors v3 | X |  | X | X | X |
