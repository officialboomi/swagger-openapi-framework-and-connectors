
# b2c-service


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| Post /services/rest/connect/v1.4/accounts - Create an account | Accounts Create an account  |
| Post /services/rest/connect/v1.4/analyticsReportResults - Create an analytics report result | Analytics Report Results Create an analytics report result  |
| Post /services/rest/connect/v1.4/answers - Create an answer | Answers Create an answer  |
| Post /services/rest/connect/v1.4/assets - Create an asset | Assets Create an asset  |
| Post /services/rest/connect/v1.4/bulkExtracts - Create a bulk extract | Bulk Extracts Create a bulk extract  |
| Post /services/rest/connect/v1.4/contactMarketingRosters - Create a contact marketing roster | Contact Marketing Rosters Create a contact marketing roster  |
| Post /services/rest/connect/v1.4/contacts - Create a contact | Contacts Create a contact  |
| Post /services/rest/connect/v1.4/contacts/{id}/executeMarketingFlow - Send a contact through a campaign flow starting at... | Contacts Send a contact through a campaign flow starting at the specified entry point.  |
| Post /services/rest/connect/v1.4/contacts/{id}/resetPassword - Reset password for a contact | Contacts Reset password for a contact  |
| Post /services/rest/connect/v1.4/contacts/{id}/sendMailing - Send a transactional mailing to a contact | Contacts Send a transactional mailing to a contact  |
| Post /services/rest/connect/v1.4/countries - Create a country | Countries Create a country  |
| Post /services/rest/connect/v1.4/eventSubscriptions - Create an event subscription | Event Subscriptions Create an event subscription  |
| Post /services/rest/connect/v1.4/holidays - Create a holiday | Holidays Create a holiday  |
| Post /services/rest/connect/v1.4/incidentResponse - Create an incident response | Incident Response Create an incident response  |
| Post /services/rest/connect/v1.4/incidents - Create an incident | Incidents Create an incident  |
| Post /services/rest/connect/v1.4/opportunities - Create an opportunity | Opportunities Create an opportunity  |
| Post /services/rest/connect/v1.4/organizations - Create an organization | Organizations Create an organization  |
| Post /services/rest/connect/v1.4/purchasedProducts - Create a purchased product | Purchased Products Create a purchased product  |
| Post /services/rest/connect/v1.4/salesProducts - Create a sales product | Sales Products Create a sales product  |
| Post /services/rest/connect/v1.4/salesTerritories - Create a sales territory | Sales Territories Create a sales territory  |
| Post /services/rest/connect/v1.4/serviceCategories - Create a service category | Service Categories Create a service category  |
| Post /services/rest/connect/v1.4/serviceDispositions - Create a service disposition | Service Dispositions Create a service disposition  |
| Post /services/rest/connect/v1.4/serviceProducts - Create a service product | Service Products Create a service product  |
| Post /services/rest/connect/v1.4/standardContents - Create a standard content | Standard Contents Create a standard content  |
| Post /services/rest/connect/v1.4/tasks - Create a task | Tasks Create a task  |
| Post /services/rest/connect/v1.4/variables - Create a variable | Variables Create a variable  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Delete /services/rest/connect/v1.4/accounts/{id} - Delete an account | Accounts Delete an account  |
| Delete /services/rest/connect/v1.4/answers/{id} - Delete an answer | Answers Delete an answer  |
| Delete /services/rest/connect/v1.4/answerVersions/{id} - Delete an answer version | Answer Versions Delete an answer version  |
| Delete /services/rest/connect/v1.4/assets/{id} - Delete an asset | Assets Delete an asset  |
| Delete /services/rest/connect/v1.4/bulkExtractResults/{id} - Delete a bulk extract result | Bulk Extract Results Delete a bulk extract result  |
| Delete /services/rest/connect/v1.4/bulkExtracts/{id} - Delete a bulk extract | Bulk Extracts Delete a bulk extract  |
| Delete /services/rest/connect/v1.4/contactMarketingRosters/{id} - Delete a contact marketing roster | Contact Marketing Rosters Delete a contact marketing roster  |
| Delete /services/rest/connect/v1.4/contacts/{id} - Delete a contact | Contacts Delete a contact  |
| Delete /services/rest/connect/v1.4/countries/{id} - Delete a country | Countries Delete a country  |
| Delete /services/rest/connect/v1.4/eventSubscriptions/{id} - Delete an event subscription | Event Subscriptions Delete an event subscription  |
| Delete /services/rest/connect/v1.4/holidays/{id} - Delete a holiday | Holidays Delete a holiday  |
| Delete /services/rest/connect/v1.4/incidents/{id} - Delete an incident | Incidents Delete an incident  |
| Delete /services/rest/connect/v1.4/opportunities/{id} - Delete an opportunity | Opportunities Delete an opportunity  |
| Delete /services/rest/connect/v1.4/organizations/{id} - Delete an organization | Organizations Delete an organization  |
| Delete /services/rest/connect/v1.4/purchasedProducts/{id} - Delete a purchased product | Purchased Products Delete a purchased product  |
| Delete /services/rest/connect/v1.4/salesProducts/{id} - Delete a sales product | Sales Products Delete a sales product  |
| Delete /services/rest/connect/v1.4/salesTerritories/{id} - Delete a sales territory | Sales Territories Delete a sales territory  |
| Delete /services/rest/connect/v1.4/serviceCategories/{id} - Delete a service category | Service Categories Delete a service category  |
| Delete /services/rest/connect/v1.4/serviceDispositions/{id} - Delete a service disposition | Service Dispositions Delete a service disposition  |
| Delete /services/rest/connect/v1.4/serviceProducts/{id} - Delete a service product | Service Products Delete a service product  |
| Delete /services/rest/connect/v1.4/standardContents/{id} - Delete a standard content | Standard Contents Delete a standard content  |
| Delete /services/rest/connect/v1.4/tasks/{id} - Delete a task | Tasks Delete a task  |
| Delete /services/rest/connect/v1.4/variables/{id} - Delete a variable | Variables Delete a variable  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Get /services/rest/connect/v1.4/accounts/{id} - Get an account | Accounts Get an account  |
| Get /services/rest/connect/v1.4/analyticsReports/{id} - Get an analytics report | Analytics Reports Get an analytics report  |
| Get /services/rest/connect/v1.4/answers/{id} - Get an answer | Answers Get an answer  |
| Get /services/rest/connect/v1.4/answerVersions/{id} - Get an answer version | Answer Versions Get an answer version  |
| Get /services/rest/connect/v1.4/assets/{id} - Get an asset | Assets Get an asset  |
| Get /services/rest/connect/v1.4/assetStatuses/{id} - Get an asset status | Asset Statuses Get an asset status  |
| Get /services/rest/connect/v1.4/bulkExtractResults/{id} - Get a bulk extract result | Bulk Extract Results Get a bulk extract result  |
| Get /services/rest/connect/v1.4/bulkExtracts/{id} - Get a bulk extract | Bulk Extracts Get a bulk extract  |
| Get /services/rest/connect/v1.4/campaigns/{id} - Get a campaign | Campaigns Get a campaign  |
| Get /services/rest/connect/v1.4/channelTypes/{id} - Get a channel type | Channel Types Get a channel type  |
| Get /services/rest/connect/v1.4/chats/{id} - Get a chat | Chats Get a chat  |
| Get /services/rest/connect/v1.4/configurations/{id} - Get a configuration | Configurations Get a configuration  |
| Get /services/rest/connect/v1.4/contactMarketingRosters/{id} - Get a contact marketing roster | Contact Marketing Rosters Get a contact marketing roster  |
| Get /services/rest/connect/v1.4/contacts/{id} - Get a contact | Contacts Get a contact  |
| Get /services/rest/connect/v1.4/countries/{id} - Get a country | Countries Get a country  |
| Get /services/rest/connect/v1.4/eventSubscriptions/{id} - Get an event subscription | Event Subscriptions Get an event subscription  |
| Get /services/rest/connect/v1.4/holidays/{id} - Get a holiday | Holidays Get a holiday  |
| Get /services/rest/connect/v1.4/incidents/{id} - Get an incident | Incidents Get an incident  |
| Get /services/rest/connect/v1.4/mailboxes/{id} - Get a mailbox | Mailboxes Get a mailbox  |
| Get /services/rest/connect/v1.4/marketingMailboxes/{id} - Get a marketing mailbox | Marketing Mailboxes Get a marketing mailbox  |
| Get /services/rest/connect/v1.4/messageBases/{id} - Get a message base | Message Bases Get a message base  |
| Get /services/rest/connect/v1.4/namedIDHierarchies/{fullyQualifiedPath} - Get all named ID hierarchy values | Named ID Hierarchies Get all named ID hierarchy values  |
| Get /services/rest/connect/v1.4/namedIDHierarchies/{fullyQualifiedPath}/{id} - Get a named ID hierarchy | Named ID Hierarchies Get a named ID hierarchy  |
| Get /services/rest/connect/v1.4/namedIDs/{fullyQualifiedPath} - Get all named ID values | Named IDs Get all named ID values  |
| Get /services/rest/connect/v1.4/namedIDs/{fullyQualifiedPath}/{id} - Get a named ID | Named IDs Get a named ID  |
| Get /services/rest/connect/v1.4/opportunities/{id} - Get an opportunity | Opportunities Get an opportunity  |
| Get /services/rest/connect/v1.4/opportunityStatuses/{id} - Get an opportunity status | Opportunity Statuses Get an opportunity status  |
| Get /services/rest/connect/v1.4/organizations/{id} - Get an organization | Organizations Get an organization  |
| Get /services/rest/connect/v1.4/purchasedProducts/{id} - Get a purchased product | Purchased Products Get a purchased product  |
| Get /services/rest/connect/v1.4/salesProducts/{id} - Get a sales product | Sales Products Get a sales product  |
| Get /services/rest/connect/v1.4/salesTerritories/{id} - Get a sales territory | Sales Territories Get a sales territory  |
| Get /services/rest/connect/v1.4/serviceCategories/{id} - Get a service category | Service Categories Get a service category  |
| Get /services/rest/connect/v1.4/serviceDispositions/{id} - Get a service disposition | Service Dispositions Get a service disposition  |
| Get /services/rest/connect/v1.4/serviceMailboxes/{id} - Get a service mailbox | Service Mailboxes Get a service mailbox  |
| Get /services/rest/connect/v1.4/serviceProducts/{id} - Get a service product | Service Products Get a service product  |
| Get /services/rest/connect/v1.4/siteInterfaces/{id} - Get a site interface | Site Interfaces Get a site interface  |
| Get /services/rest/connect/v1.4/sSOTokenReferences/{id} - Get an SSO token reference | SSO Token References Get an SSO token reference  |
| Get /services/rest/connect/v1.4/standardContents/{id} - Get a standard content | Standard Contents Get a standard content  |
| Get /services/rest/connect/v1.4/tasks/{id} - Get a task | Tasks Get a task  |
| Get /services/rest/connect/v1.4/variables/{id} - Get a variable | Variables Get a variable  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Get /services/rest/connect/v1.4/accounts - Get multiple accounts | Accounts Get multiple accounts  |
| Get /services/rest/connect/v1.4/analyticsReports - Get multiple analytics reports | Analytics Reports Get multiple analytics reports  |
| Get /services/rest/connect/v1.4/answers - Get multiple answers | Answers Get multiple answers  |
| Get /services/rest/connect/v1.4/answerVersions - Get multiple answer versions | Answer Versions Get multiple answer versions  |
| Get /services/rest/connect/v1.4/assets - Get multiple assets | Assets Get multiple assets  |
| Get /services/rest/connect/v1.4/assetStatuses - Get multiple asset statuses | Asset Statuses Get multiple asset statuses  |
| Get /services/rest/connect/v1.4/bulkExtractResults - Get multiple bulk extract results | Bulk Extract Results Get multiple bulk extract results  |
| Get /services/rest/connect/v1.4/bulkExtracts - Get multiple bulk extracts | Bulk Extracts Get multiple bulk extracts  |
| Get /services/rest/connect/v1.4/campaigns - Get multiple campaigns | Campaigns Get multiple campaigns  |
| Get /services/rest/connect/v1.4/channelTypes - Get multiple channel types | Channel Types Get multiple channel types  |
| Get /services/rest/connect/v1.4/chats - Get multiple chats | Chats Get multiple chats  |
| Get /services/rest/connect/v1.4/configurations - Get multiple configurations | Configurations Get multiple configurations  |
| Get /services/rest/connect/v1.4/contactMarketingRosters - Get multiple contact marketing rosters | Contact Marketing Rosters Get multiple contact marketing rosters  |
| Get /services/rest/connect/v1.4/contacts - Get multiple contacts | Contacts Get multiple contacts  |
| Get /services/rest/connect/v1.4/countries - Get multiple countries | Countries Get multiple countries  |
| Get /services/rest/connect/v1.4/eventSubscriptions - Get multiple event subscriptions | Event Subscriptions Get multiple event subscriptions  |
| Get /services/rest/connect/v1.4/holidays - Get multiple holidays | Holidays Get multiple holidays  |
| Get /services/rest/connect/v1.4/incidents - Get multiple incidents | Incidents Get multiple incidents  |
| Get /services/rest/connect/v1.4/mailboxes - Get multiple mailboxes | Mailboxes Get multiple mailboxes  |
| Get /services/rest/connect/v1.4/marketingMailboxes - Get multiple marketing mailboxes | Marketing Mailboxes Get multiple marketing mailboxes  |
| Get /services/rest/connect/v1.4/messageBases - Get multiple message bases | Message Bases Get multiple message bases  |
| Get /services/rest/connect/v1.4/namedIDHierarchies/ - Get all resources having named ID hierarchies | Named ID Hierarchies Get all resources having named ID hierarchies  |
| Get /services/rest/connect/v1.4/namedIDs/ - Get all resources having named IDs | Named IDs Get all resources having named IDs  |
| Get /services/rest/connect/v1.4/opportunities - Get multiple opportunities | Opportunities Get multiple opportunities  |
| Get /services/rest/connect/v1.4/opportunityStatuses - Get multiple opportunity statuses | Opportunity Statuses Get multiple opportunity statuses  |
| Get /services/rest/connect/v1.4/organizations - Get multiple organizations | Organizations Get multiple organizations  |
| Get /services/rest/connect/v1.4/purchasedProducts - Get multiple purchased products | Purchased Products Get multiple purchased products  |
| Get /services/rest/connect/v1.4/queryResults - Get an array of ROQL results | Query Results Get an array of ROQL results  |
| Get /services/rest/connect/v1.4/salesProducts - Get multiple sales products | Sales Products Get multiple sales products  |
| Get /services/rest/connect/v1.4/salesTerritories - Get multiple sales territories | Sales Territories Get multiple sales territories  |
| Get /services/rest/connect/v1.4/serviceCategories - Get multiple service categories | Service Categories Get multiple service categories  |
| Get /services/rest/connect/v1.4/serviceDispositions - Get multiple service dispositions | Service Dispositions Get multiple service dispositions  |
| Get /services/rest/connect/v1.4/serviceMailboxes - Get multiple service mailboxes | Service Mailboxes Get multiple service mailboxes  |
| Get /services/rest/connect/v1.4/serviceProducts - Get multiple service products | Service Products Get multiple service products  |
| Get /services/rest/connect/v1.4/siteInterfaces - Get multiple site interfaces | Site Interfaces Get multiple site interfaces  |
| Get /services/rest/connect/v1.4/sSOTokenReferences - Get multiple SSO token references | SSO Token References Get multiple SSO token references  |
| Get /services/rest/connect/v1.4/standardContents - Get multiple standard contents | Standard Contents Get multiple standard contents  |
| Get /services/rest/connect/v1.4/tasks - Get multiple tasks | Tasks Get multiple tasks  |
| Get /services/rest/connect/v1.4/variables - Get multiple variables | Variables Get multiple variables  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Patch /services/rest/connect/v1.4/accounts/{id} - Update an account (PATCH) | Accounts Update an account  |
| Patch /services/rest/connect/v1.4/answers/{id} - Update an answer (PATCH) | Answers Update an answer  |
| Patch /services/rest/connect/v1.4/answerVersions/{id} - Update an answer version (PATCH) | Answer Versions Update an answer version  |
| Patch /services/rest/connect/v1.4/assets/{id} - Update an asset (PATCH) | Assets Update an asset  |
| Patch /services/rest/connect/v1.4/bulkExtracts/{id} - Update a bulk extract (PATCH) | Bulk Extracts Update a bulk extract  |
| Patch /services/rest/connect/v1.4/contactMarketingRosters/{id} - Update a contact marketing roster (PATCH) | Contact Marketing Rosters Update a contact marketing roster  |
| Patch /services/rest/connect/v1.4/contacts/{id} - Update a contact (PATCH) | Contacts Update a contact  |
| Patch /services/rest/connect/v1.4/countries/{id} - Update a country (PATCH) | Countries Update a country  |
| Patch /services/rest/connect/v1.4/eventSubscriptions/{id} - Update an event subscription (PATCH) | Event Subscriptions Update an event subscription  |
| Patch /services/rest/connect/v1.4/holidays/{id} - Update a holiday (PATCH) | Holidays Update a holiday  |
| Patch /services/rest/connect/v1.4/incidents/{id} - Update an incident (PATCH) | Incidents Update an incident  |
| Patch /services/rest/connect/v1.4/opportunities/{id} - Update an opportunity (PATCH) | Opportunities Update an opportunity  |
| Patch /services/rest/connect/v1.4/organizations/{id} - Update an organization (PATCH) | Organizations Update an organization  |
| Patch /services/rest/connect/v1.4/purchasedProducts/{id} - Update a purchased product (PATCH) | Purchased Products Update a purchased product  |
| Patch /services/rest/connect/v1.4/salesProducts/{id} - Update a sales product (PATCH) | Sales Products Update a sales product  |
| Patch /services/rest/connect/v1.4/salesTerritories/{id} - Update a sales territory (PATCH) | Sales Territories Update a sales territory  |
| Patch /services/rest/connect/v1.4/serviceCategories/{id} - Update a service category (PATCH) | Service Categories Update a service category  |
| Patch /services/rest/connect/v1.4/serviceDispositions/{id} - Update a service disposition (PATCH) | Service Dispositions Update a service disposition  |
| Patch /services/rest/connect/v1.4/serviceProducts/{id} - Update a service product (PATCH) | Service Products Update a service product  |
| Patch /services/rest/connect/v1.4/sSOTokenReferences/{id} - Update an SSO token reference (PATCH) | SSO Token References Update an SSO token reference  |
| Patch /services/rest/connect/v1.4/standardContents/{id} - Update a standard content (PATCH) | Standard Contents Update a standard content  |
| Patch /services/rest/connect/v1.4/tasks/{id} - Update a task (PATCH) | Tasks Update a task  |
| Patch /services/rest/connect/v1.4/variables/{id} - Update a variable (PATCH) | Variables Update a variable  |


