
# financials


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| Bank Account User Rules - Create a country-specific rule | Bank Account User Rules Create a country-specific rule  |
| Bank Accounts - Create a bank account | Bank Accounts Create a bank account  |
| Bank Accounts/Bank Account Descriptive Flexfields - Create a descriptive flexfield for a bank account | Bank Accounts/Bank Account Descriptive Flexfields Create a descriptive flexfield for a bank account  |
| Bank Accounts/Bank Account Global Descriptive Flexfields - Create a global descriptive flexfield for a bank a... | Bank Accounts/Bank Account Global Descriptive Flexfields Create a global descriptive flexfield for a bank account  |
| Bank Accounts/Bank Account Grants - Create a grant for a bank account | Bank Accounts/Bank Account Grants Create a grant for a bank account  |
| Bank Accounts/Bank Account Payment Documents - Create a payment document for a bank account | Bank Accounts/Bank Account Payment Documents Create a payment document for a bank account  |
| Bank Accounts/Bank Account Payment Documents/Bank Account Checkbooks - Create a checkbook for a bank account | Bank Accounts/Bank Account Payment Documents/Bank Account Checkbooks Create a checkbook for a bank account  |
| Bank Accounts/Bank Account Transaction Creation Rules - Create a bank statement transaction creation rule ... | Bank Accounts/Bank Account Transaction Creation Rules Create a bank statement transaction creation rule for a bank account  |
| Bank Accounts/Bank Account Uses - Create a bank account use for a bank account | Bank Accounts/Bank Account Uses Create a bank account use for a bank account  |
| Bank Accounts/Bank Account Uses/Bank Account Payment Document Categories - Create a payment document category for a business ... | Bank Accounts/Bank Account Uses/Bank Account Payment Document Categories Create a payment document category for a business unit  |
| Bank Branches - Create a bank branch | Bank Branches Create a bank branch  |
| Banks - Create a bank | Banks Create a bank  |
| Bill Management Users - Create a bill management user | Bill Management Users Create a bill management user  |
| Budgetary Control for Enterprise Performance Management Budget Transactions - Create and perform budgetary control validation on... | Budgetary Control for Enterprise Performance Management Budget Transactions Create and perform budgetary control validation on a budget entry transaction  |
| Cash Pools - Create a cash pool | Cash Pools Create a cash pool  |
| Cash Pools/Cash Pool Members - Create a cash pool member | Cash Pools/Cash Pool Members Create a cash pool member  |
| Collection Promises - Create a collection promise | Collection Promises Create a collection promise  |
| Collection Promises/Collection Promises Descriptive Flexfields - Create a collection promise descriptive flexfield | Collection Promises/Collection Promises Descriptive Flexfields Create a collection promise descriptive flexfield  |
| Collections Strategies - Create a strategy assignment | Collections Strategies Create a strategy assignment  |
| Collections Strategies/Tasks - Create a task assignment | Collections Strategies/Tasks Create a task assignment  |
| Collections Strategies/User Tasks - Create a user task assignment | Collections Strategies/User Tasks Create a user task assignment  |
| Credit and Collections Data Points - Create a credit and collections data point | Credit and Collections Data Points Create a credit and collections data point  |
| Data Point Values - Create a data point value | Data Point Values Create a data point value  |
| Data Security for Users - Create a data assignment for a user | Data Security for Users Create a data assignment for a user  |
| Debit Authorizations - Create a debit authorization | Debit Authorizations Create a debit authorization  |
| Document Sequence Derivations - Create multiple document sequence derivation recor... | Document Sequence Derivations Create multiple document sequence derivation records  |
| Early Payment Offers - Create a standing early payment offer | Early Payment Offers Create a standing early payment offer  |
| Early Payment Offers/Early Payment Offer Assignments - Create a standing offer assignment | Early Payment Offers/Early Payment Offer Assignments Create a standing offer assignment  |
| Early Payment Offers/Early Payment Offer Assignments/Early Payment Offers Descriptive Flexfields - Create descriptive flexfields for a standing offer... | Early Payment Offers/Early Payment Offer Assignments/Early Payment Offers Descriptive Flexfields Create descriptive flexfields for a standing offer assignment  |
| ERP Integrations - Create an inbound or outbound set of data | ERP Integrations Create an inbound or outbound set of data  |
| Expense Cash Advances - Create a cash advance record | Expense Cash Advances Create a cash advance record  |
| Expense Cash Advances/Cash Advance Descriptive Flexfields - Create a cash advance descriptive flexfield record... | Expense Cash Advances/Cash Advance Descriptive Flexfields Create a cash advance descriptive flexfield record for a cash advance  |
| Expense Delegations - Create an expense delegation | Expense Delegations Create an expense delegation  |
| Expense Distributions - Create a distribution record for the related expen... | Expense Distributions Create a distribution record for the related expense  |
| Expense Distributions/Project Descriptive Flexfields - Create a project-related descriptive flexfield rec... | Expense Distributions/Project Descriptive Flexfields Create a project-related descriptive flexfield record for a specific expense distribution identifier  |
| Expense Preferences - Create an expense preferences record for a person | Expense Preferences Create an expense preferences record for a person  |
| Expense Reports - Create an expense report record | Expense Reports Create an expense report record  |
| Expense Reports/Attachments - Create an attachment record for a specific attachm... | Expense Reports/Attachments Create an attachment record for a specific attachment identifier  |
| Expense Reports/Expense Report Descriptive Flexfields - Create an expense report descriptive flexfield rec... | Expense Reports/Expense Report Descriptive Flexfields Create an expense report descriptive flexfield record  |
| Expense Reports/Expenses - Create an expense record | Expense Reports/Expenses Create an expense record  |
| Expense Reports/Expenses/Attachments - Create an attachment record for a specific attachm... | Expense Reports/Expenses/Attachments Create an attachment record for a specific attachment identifier  |
| Expense Reports/Expenses/Expense Attendees - Create an expense attendee record for an expense i... | Expense Reports/Expenses/Expense Attendees Create an expense attendee record for an expense identifier  |
| Expense Reports/Expenses/Expense Descriptive Flexfields - Create an expense descriptive flexfield record for... | Expense Reports/Expenses/Expense Descriptive Flexfields Create an expense descriptive flexfield record for a specific expense identifier  |
| Expense Reports/Expenses/Expense Distributions - Create a distribution record for the related expen... | Expense Reports/Expenses/Expense Distributions Create a distribution record for the related expense  |
| Expense Reports/Expenses/Expense Distributions/Projects Descriptive Flexfields - Create a project-related descriptive flexfield rec... | Expense Reports/Expenses/Expense Distributions/Projects Descriptive Flexfields Create a project-related descriptive flexfield record for a specific expense distribution identifier  |
| Expense Reports/Expenses/Expense Itemizations - Create an itemization record for a specific expens... | Expense Reports/Expenses/Expense Itemizations Create an itemization record for a specific expense identifier  |
| Expense Scanned Images - Upload an image to the image recognition applicati... | Expense Scanned Images Upload an image to the image recognition application  |
| Expense Travel Itineraries - Create a travel itinerary | Expense Travel Itineraries Create a travel itinerary  |
| Expense Travel Itineraries/Expense Travel Itinerary Reservations - Create a travel itinerary reservation | Expense Travel Itineraries/Expense Travel Itinerary Reservations Create a travel itinerary reservation  |
| Expense Travel Itineraries/Expense Travel Itinerary Reservations/Expense Travel Itinerary Reservation Items - Create a travel itinerary reservation item | Expense Travel Itineraries/Expense Travel Itinerary Reservations/Expense Travel Itinerary Reservation Items Create a travel itinerary reservation item  |
| Expenses - Create an expense record | Expenses Create an expense record  |
| Expenses/Attachments - Create an attachment record for an attachment iden... | Expenses/Attachments Create an attachment record for an attachment identifier  |
| Expenses/Expense Attendees - Create an expense attendee record for an expense i... | Expenses/Expense Attendees Create an expense attendee record for an expense identifier  |
| Expenses/Expense Descriptive Flexfields - Create an expense descriptive flexfield record for... | Expenses/Expense Descriptive Flexfields Create an expense descriptive flexfield record for an expense identifier  |
| Expenses/Expense Distributions - Create a distribution record for the related expen... | Expenses/Expense Distributions Create a distribution record for the related expense  |
| Expenses/Expense Distributions/Project Descriptive Flexfields - Create a project-related descriptive flexfield rec... | Expenses/Expense Distributions/Project Descriptive Flexfields Create a project-related descriptive flexfield record  |
| Expenses/Expense Itemizations - Create an itemization record for an expense identi... | Expenses/Expense Itemizations Create an itemization record for an expense identifier  |
| External Bank Accounts - Create an external bank account | External Bank Accounts Create an external bank account  |
| External Bank Accounts/Account Owners - Create an account owner for an external bank accou... | External Bank Accounts/Account Owners Create an account owner for an external bank account  |
| External Cash Transactions - Create an external transaction | External Cash Transactions Create an external transaction  |
| External Cash Transactions/Attachments - Create an attachment for an external transaction | External Cash Transactions/Attachments Create an attachment for an external transaction  |
| External Cash Transactions/Descriptive Flexfields - Create a descriptive flexfield for an external tra... | External Cash Transactions/Descriptive Flexfields Create a descriptive flexfield for an external transaction  |
| External Payees - Create an external payee | External Payees Create an external payee  |
| External Payees/External Party Payment Methods - Create a payment method assignment to a payee | External Payees/External Party Payment Methods Create a payment method assignment to a payee  |
| Federal Account Symbols - Create a federal account symbol | Federal Account Symbols Create a federal account symbol  |
| Federal Account Symbols/Federal Account Symbols Descriptive Flexfields - Create a federal account symbol descriptive flexfi... | Federal Account Symbols/Federal Account Symbols Descriptive Flexfields Create a federal account symbol descriptive flexfield  |
| Federal Agency Location Codes - Create an agency location code | Federal Agency Location Codes Create an agency location code  |
| Federal Agency Location Codes/Federal Agency Location Codes Descriptive Flexfields - Create an agency location code descriptive flexfie... | Federal Agency Location Codes/Federal Agency Location Codes Descriptive Flexfields Create an agency location code descriptive flexfield  |
| Federal Business Event Type Codes - Create business event type codes | Federal Business Event Type Codes Create business event type codes  |
| Federal Business Event Type Codes/Federal Business Event Type Codes Descriptive Flexfields - Create a business event type code descriptive flex... | Federal Business Event Type Codes/Federal Business Event Type Codes Descriptive Flexfields Create a business event type code descriptive flexfield  |
| Federal Fund Attributes - Create fund attributes | Federal Fund Attributes Create fund attributes  |
| Federal Fund Attributes/Federal Fund Attributes Descriptive Flexfields - Create a fund attribute descriptive flexfield | Federal Fund Attributes/Federal Fund Attributes Descriptive Flexfields Create a fund attribute descriptive flexfield  |
| Federal Payment Format Mappings - Create payment format mappings | Federal Payment Format Mappings Create payment format mappings  |
| Federal Payment Format Mappings/Federal Payment Format Mappings Descriptive Flexfields - Create a payment format mapping descriptive flexfi... | Federal Payment Format Mappings/Federal Payment Format Mappings Descriptive Flexfields Create a payment format mapping descriptive flexfield  |
| Federal Setup Options - Create setup options | Federal Setup Options Create setup options  |
| Federal Setup Options/Federal Setup Options Descriptive Flexfields - Create a setup option descriptive flexfield | Federal Setup Options/Federal Setup Options Descriptive Flexfields Create a setup option descriptive flexfield  |
| Federal Treasury Account Symbols - Create treasury account symbols | Federal Treasury Account Symbols Create treasury account symbols  |
| Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes - Create treasury account symbols business event typ... | Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes Create treasury account symbols business event type codes  |
| Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes/Federal Treasury Account Symbols Business Event Type Codes Descriptive Flexfields - Create a TAS BETC descriptive flexfield | Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes/Federal Treasury Account Symbols Business Event Type Codes Descriptive Flexfields Create a TAS BETC descriptive flexfield  |
| Federal Treasury Account Symbols/Federal Treasury Account Symbols Descriptive Flexfields - Create a treasury account symbol descriptive flexf... | Federal Treasury Account Symbols/Federal Treasury Account Symbols Descriptive Flexfields Create a treasury account symbol descriptive flexfield  |
| Federal USSGL Accounts - Create USSGL accounts | Federal USSGL Accounts Create USSGL accounts  |
| Federal USSGL Accounts/Federal USSGL Accounts Descriptive Flexfields - Create a federal USSGL account descriptive flexfie... | Federal USSGL Accounts/Federal USSGL Accounts Descriptive Flexfields Create a federal USSGL account descriptive flexfield  |
| Instrument Assignments - Create a payment instrument assignment | Instrument Assignments Create a payment instrument assignment  |
| Interim Payables Documents - Create an interim payables document | Interim Payables Documents Create an interim payables document  |
| Invoice Holds - Create an invoice hold | Invoice Holds Create an invoice hold  |
| Invoice Holds/Invoice Holds Descriptive Flexfields - Create a descriptive flexfield for an invoice hold | Invoice Holds/Invoice Holds Descriptive Flexfields Create a descriptive flexfield for an invoice hold  |
| Invoices - Apply prepayments to an invoice | Invoices Applies prepayments to an invoice. When you apply a prepayment, the invoice is updated to reflect the amount paid and the prepayment amount available for application is reduced. A prepayment application line with corresponding distributions is added to the invoice. Apply prepayments to an invoice  |
| Invoices - Calculate tax for the invoice | Invoices Calculate taxes for the invoice based on the invoice details and the relevant tax configuration. Calculate tax for the invoice  |
| Invoices - Cancel an invoice | Invoices Cancels an invoice. When you cancel an invoice, all invoice amounts are set to zero, and the invoice can't be edited or submitted for processing. Cancel an invoice  |
| Invoices - Cancel an invoice line | Invoices Cancels an invoice line. When you cancel an item line, the line amount is set to zero, and the invoice line can't be edited. Billed quantity on the purchase order is adjusted with the canceled line quantity if the item line is a matched to a purchase order. Cancel an invoice line  |
| Invoices - Create an invoice | Invoices Create an invoice Create an invoice  |
| Invoices - Generate distributions for the invoice | Invoices Generate distributions for the invoice based on the line information such as distribution combinations, distribution sets, and freight or miscellaneous charge allocations. Distributions are used to create accounting entries and include information, such as accounting date, distribution combinations, and projects information. Generate distributions for the invoice  |
| Invoices - Unapply a prepayment from an invoice | Invoices Unapplies a prepayment from an invoice. When you unapply a prepayment, the prepayment is then available for application to another invoice. Unapply a prepayment from an invoice  |
| Invoices - Validate an invoice | Invoices Validates an invoice. When you validate an invoice, the validation process performs actions, such as calculating tax, checking that matching variances fall within specified tolerance limits, and placing holds for exception conditions. Validate an invoice  |
| Invoices/Attachments - Create an attachment for an invoice | Invoices/Attachments Create an attachment for an invoice  |
| Invoices/Invoice Installments - Create an invoice installment | Invoices/Invoice Installments Create an invoice installment  |
| Invoices/Invoice Installments/Invoice Installments Descriptive Flexfields - Create a descriptive flexfield for an invoice inst... | Invoices/Invoice Installments/Invoice Installments Descriptive Flexfields Create a descriptive flexfield for an invoice installment  |
| Invoices/Invoice Installments/Invoice Installments Global Descriptive Flexfields - Create a global descriptive flexfield for an invoi... | Invoices/Invoice Installments/Invoice Installments Global Descriptive Flexfields Create a global descriptive flexfield for an invoice installment  |
| Invoices/Invoice Lines - Create an invoice line | Invoices/Invoice Lines Create an invoice line  |
| Invoices/Invoice Lines/Invoice Distributions - Create an invoice distribution | Invoices/Invoice Lines/Invoice Distributions Create an invoice distribution  |
| Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Descriptive Flexfields - Create a descriptive flexfield for an invoice dist... | Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Descriptive Flexfields Create a descriptive flexfield for an invoice distribution  |
| Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Global Descriptive Flexfields - Create a global descriptive flexfield for an invoi... | Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Global Descriptive Flexfields Create a global descriptive flexfield for an invoice distribution  |
| Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Project Descriptive Flexfields - Create a project-related descriptive flexfield for... | Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Project Descriptive Flexfields Create a project-related descriptive flexfield for an invoice distribution  |
| Invoices/Invoice Lines/Invoice Lines Descriptive Flexfields - Create a descriptive flexfield for an invoice line | Invoices/Invoice Lines/Invoice Lines Descriptive Flexfields Create a descriptive flexfield for an invoice line  |
| Invoices/Invoice Lines/Invoice Lines Global Descriptive Flexfields - Create a global descriptive flexfield for an invoi... | Invoices/Invoice Lines/Invoice Lines Global Descriptive Flexfields Create a global descriptive flexfield for an invoice line  |
| Invoices/Invoice Lines/Invoice Lines Project Descriptive Flexfields - Create a project-related descriptive flexfield for... | Invoices/Invoice Lines/Invoice Lines Project Descriptive Flexfields Create a project-related descriptive flexfield for an invoice line  |
| Invoices/Invoices Descriptive Flexfields - Create a descriptive flexfield for an invoice | Invoices/Invoices Descriptive Flexfields Create a descriptive flexfield for an invoice  |
| Invoices/Invoices Global Descriptive Flexfields - Create a global descriptive flexfield for an invoi... | Invoices/Invoices Global Descriptive Flexfields Create a global descriptive flexfield for an invoice  |
| Joint Venture General Ledger Transactions/Joint Venture Transaction Descriptive Flexfields - Create the set of descriptive flexfields for a joi... | Joint Venture General Ledger Transactions/Joint Venture Transaction Descriptive Flexfields Create the set of descriptive flexfields for a joint venture transaction  |
| Joint Venture Invoicing Partners - Create a joint venture invoicing partner | Joint Venture Invoicing Partners Create a joint venture invoicing partner  |
| Joint Venture Invoicing Partners/Invoicing Partner Descriptive Flexfields - Create a set of descriptive flexfields for a joint... | Joint Venture Invoicing Partners/Invoicing Partner Descriptive Flexfields Create a set of descriptive flexfields for a joint venture  |
| Joint Venture Options - Create joint venture options | Joint Venture Options Create joint venture options  |
| Joint Venture Options/Joint Venture Options Descriptive Flexfields - Create the set of descriptive flexfields for joint... | Joint Venture Options/Joint Venture Options Descriptive Flexfields Create the set of descriptive flexfields for joint venture options  |
| Joint Venture Partner Contributions - Create a joint venture partner contribution | Joint Venture Partner Contributions Create a joint venture partner contribution  |
| Joint Venture Partner Contributions/Partner Contribution Descriptive Flexfields - Create a set of descriptive flexfields for a partn... | Joint Venture Partner Contributions/Partner Contribution Descriptive Flexfields Create a set of descriptive flexfields for a partner contribution  |
| Joint Venture Subledger Transactions/Joint Venture Transaction Descriptive Flexfields - Create the set of descriptive flexfields for a joi... | Joint Venture Subledger Transactions/Joint Venture Transaction Descriptive Flexfields Create the set of descriptive flexfields for a joint venture transaction  |
| Joint Ventures - Create a joint venture | Joint Ventures Create a joint venture  |
| Joint Ventures/Cost Centers - Create a joint venture cost center | Joint Ventures/Cost Centers Create a joint venture cost center  |
| Joint Ventures/Cost Centers/Cost Center Descriptive Flexfields - Create a descriptive flexfield for a cost center a... | Joint Ventures/Cost Centers/Cost Center Descriptive Flexfields Create a descriptive flexfield for a cost center associated with a joint venture  |
| Joint Ventures/Cost Centers/Distributable Value Sets - Create a distributable value set | Joint Ventures/Cost Centers/Distributable Value Sets Create a distributable value set  |
| Joint Ventures/Distributable Project Rules - Create a distributable project rule | Joint Ventures/Distributable Project Rules Create a distributable project rule  |
| Joint Ventures/Joint Venture Descriptive Flexfieds - Create a set of descriptive flexfields for a joint... | Joint Ventures/Joint Venture Descriptive Flexfieds Create a set of descriptive flexfields for a joint venture  |
| Joint Ventures/Ownership Definitions - Create a set of ownership definitions for a joint ... | Joint Ventures/Ownership Definitions Create a set of ownership definitions for a joint venture  |
| Joint Ventures/Ownership Definitions/Ownership Definition Descriptive Flexfields - Create a descriptive flexfield for an ownership de... | Joint Ventures/Ownership Definitions/Ownership Definition Descriptive Flexfields Create a descriptive flexfield for an ownership definition associated with a joint venture  |
| Joint Ventures/Ownership Definitions/Ownership Percentages - Create a set of ownership percentage for an owners... | Joint Ventures/Ownership Definitions/Ownership Percentages Create a set of ownership percentage for an ownership definition  |
| Joint Ventures/Ownership Definitions/Ownership Percentages/Ownership Percentage Descriptive Flexfields - Create a descriptive flexfield for an ownership pe... | Joint Ventures/Ownership Definitions/Ownership Percentages/Ownership Percentage Descriptive Flexfields Create a descriptive flexfield for an ownership percentage associated with an ownership definition  |
| Joint Ventures/Stakeholders - Create a joint venture stakeholder | Joint Ventures/Stakeholders Create a joint venture stakeholder  |
| Joint Ventures/Stakeholders/Stakeholder Descriptive Flexfields - Create a set of descriptive flexfields for a joint... | Joint Ventures/Stakeholders/Stakeholder Descriptive Flexfields Create a set of descriptive flexfields for a joint venture stakeholder  |
| Party Fiscal Classifications - Create a party fiscal classification | Party Fiscal Classifications Create a party fiscal classification  |
| Party Fiscal Classifications/Tax Regime Associations - Create a tax regime for a party fiscal classificat... | Party Fiscal Classifications/Tax Regime Associations Create a tax regime for a party fiscal classification  |
| Party Tax Profiles - Create a party tax profile | Party Tax Profiles Create a party tax profile  |
| Payables Payments - Create a payment | Payables Payments Create a payment  |
| Payables Payments/Payments Descriptive Flexfields - Create a descriptive flexfield for a payment | Payables Payments/Payments Descriptive Flexfields Create a descriptive flexfield for a payment  |
| Payables Payments/Payments Global Descriptive Flexfields - Create a global descriptive flexfield for a paymen... | Payables Payments/Payments Global Descriptive Flexfields Create a global descriptive flexfield for a payment  |
| Payables Payments/Related Invoices - Invoices to be paid | Payables Payments/Related Invoices Invoices to be paid  |
| Payables Payments/Related Invoices/Related Invoice Global Descriptive Flexfields - Create a global descriptive flexfield for an invoi... | Payables Payments/Related Invoices/Related Invoice Global Descriptive Flexfields Create a global descriptive flexfield for an invoice payment  |
| Receipt Method Assignments - Create a receipt method assignment at customer acc... | Receipt Method Assignments Create a receipt method assignment at customer account or site  |
| Receivables Invoices - Create a receivables invoice | Receivables Invoices Create a receivables invoice  |
| Receivables Invoices/Receivables Invoice Attachments - Create a receivables invoice attachment | Receivables Invoices/Receivables Invoice Attachments Create a receivables invoice attachment  |
| Receivables Invoices/Receivables Invoice Descriptive Flexfields - Create a receivables invoice descriptive flexfield | Receivables Invoices/Receivables Invoice Descriptive Flexfields Create a receivables invoice descriptive flexfield  |
| Receivables Invoices/Receivables Invoice Distributions - Create a receivables invoice distribution | Receivables Invoices/Receivables Invoice Distributions Create a receivables invoice distribution  |
| Receivables Invoices/Receivables Invoice Distributions/Receivables Invoice Distribution Descriptive Flexfields - Create a receivables invoice distribution descript... | Receivables Invoices/Receivables Invoice Distributions/Receivables Invoice Distribution Descriptive Flexfields Create a receivables invoice distribution descriptive flexfield  |
| Receivables Invoices/Receivables Invoice Global Descriptive Flexfields - Create a receivables invoice global descriptive fl... | Receivables Invoices/Receivables Invoice Global Descriptive Flexfields Create a receivables invoice global descriptive flexfield  |
| Receivables Invoices/Receivables Invoice Installments/Receivables Invoice Installment Global Descriptive Flexfields - Create a receivables invoice installment global de... | Receivables Invoices/Receivables Invoice Installments/Receivables Invoice Installment Global Descriptive Flexfields Create a receivables invoice installment global descriptive flexfield  |
| Receivables Invoices/Receivables Invoice Installments/Receivables Invoice Installment Notes - Create a receivables invoice installment note | Receivables Invoices/Receivables Invoice Installments/Receivables Invoice Installment Notes Create a receivables invoice installment note  |
| Receivables Invoices/Receivables Invoice Lines - Create a receivables invoice line | Receivables Invoices/Receivables Invoice Lines Create a receivables invoice line  |
| Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Attachments - Create a receivables invoice attachment | Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Attachments Create a receivables invoice attachment  |
| Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Descriptive Flexfields - Create a receivables invoice line descriptive flex... | Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Descriptive Flexfields Create a receivables invoice line descriptive flexfield  |
| Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Global Descriptive Flexfields - Create a receivables invoice line global descripti... | Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Global Descriptive Flexfields Create a receivables invoice line global descriptive flexfield  |
| Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Tax Lines - Create a receivables invoice line tax line | Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Tax Lines Create a receivables invoice line tax line  |
| Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Transaction Descriptive Flexfields - Create a receivables invoice line transaction desc... | Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Transaction Descriptive Flexfields Create a receivables invoice line transaction descriptive flexfield  |
| Receivables Invoices/Receivables Invoice Notes - Create a receivables invoice note | Receivables Invoices/Receivables Invoice Notes Create a receivables invoice note  |
| Receivables Invoices/Receivables Invoice Transaction Descriptive Flexfields - Create a receivables invoice transaction descripti... | Receivables Invoices/Receivables Invoice Transaction Descriptive Flexfields Create a receivables invoice transaction descriptive flexfield  |
| Salesperson Reference Accounts - Create a salesperson reference account | Salesperson Reference Accounts Create a salesperson reference account  |
| Standard Receipts - Create a standard receipt | Standard Receipts Create a standard receipt  |
| Standard Receipts/Attachments - Create a standard receipt attachment | Standard Receipts/Attachments Create a standard receipt attachment  |
| Standard Receipts/Remittance References - Create a remittance reference | Standard Receipts/Remittance References Create a remittance reference  |
| Standard Receipts/Standard Receipt Descriptive Flexfields - Create a standard receipt descriptive flexfield | Standard Receipts/Standard Receipt Descriptive Flexfields Create a standard receipt descriptive flexfield  |
| Standard Receipts/Standard Receipt Global Descriptive Flexfields - Create a standard receipt global descriptive flexf... | Standard Receipts/Standard Receipt Global Descriptive Flexfields Create a standard receipt global descriptive flexfield  |
| Tax Exemptions - Create a tax exemption | Tax Exemptions Create a tax exemption  |
| Tax Partner Registrations - Create a tax partner for tax calculation integrati... | Tax Partner Registrations Create a tax partner for tax calculation integration  |
| Tax Registrations - Create a tax registration | Tax Registrations Create a tax registration  |
| Taxpayer Identifiers - Create a taxpayer identifier | Taxpayer Identifiers Create a taxpayer identifier  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Bank Accounts/Bank Account Transaction Creation Rules - Delete a bank statement transaction creation rule ... | Bank Accounts/Bank Account Transaction Creation Rules Delete a bank statement transaction creation rule for a bank account  |
| Bank Accounts/Bank Account Uses/Bank Account Payment Document Categories - Delete a payment document category for a business ... | Bank Accounts/Bank Account Uses/Bank Account Payment Document Categories Delete a payment document category for a business unit  |
| Bank Branches - Delete a bank branch | Bank Branches Delete a bank branch  |
| Banks - Delete a bank | Banks Delete a bank  |
| Budgetary Control for Enterprise Performance Management Budget Transactions - Delete a budgetary control budget entry transactio... | Budgetary Control for Enterprise Performance Management Budget Transactions Delete a budgetary control budget entry transaction interface row  |
| Cash Pools - Delete a cash pool | Cash Pools Delete a cash pool  |
| Cash Pools/Cash Pool Members - Delete a cash pool member | Cash Pools/Cash Pool Members Delete a cash pool member  |
| Document Sequence Derivations - Delete a document sequence derivation record | Document Sequence Derivations Delete a document sequence derivation record  |
| Expense Cash Advances - Delete a cash advance | Expense Cash Advances Delete a cash advance  |
| Expense Preferences - Delete an expense preferences record for a person | Expense Preferences Delete an expense preferences record for a person  |
| Expense Reports/Attachments - DELETE action not supported | Expense Reports/Attachments DELETE action not supported  |
| Expense Reports/Expenses/Attachments - DELETE action not supported | Expense Reports/Expenses/Attachments DELETE action not supported  |
| Expenses - DELETE action not supported | Expenses DELETE action not supported  |
| Expenses/Attachments - DELETE action not supported | Expenses/Attachments DELETE action not supported  |
| External Cash Transactions/Attachments - Delete an attachment for an external transaction | External Cash Transactions/Attachments Delete an attachment for an external transaction  |
| Federal Account Symbols - Delete a federal account symbol | Federal Account Symbols Delete a federal account symbol  |
| Federal Account Symbols/Federal Account Symbols Descriptive Flexfields - Delete a federal account symbol descriptive flexfi... | Federal Account Symbols/Federal Account Symbols Descriptive Flexfields Delete a federal account symbol descriptive flexfield  |
| Federal Agency Location Codes - Delete an agency location code | Federal Agency Location Codes Delete an agency location code  |
| Federal Agency Location Codes/Federal Agency Location Codes Descriptive Flexfields - Delete an agency location code descriptive flexfie... | Federal Agency Location Codes/Federal Agency Location Codes Descriptive Flexfields Delete an agency location code descriptive flexfield  |
| Federal Business Event Type Codes - Delete a business event type code | Federal Business Event Type Codes Delete a business event type code  |
| Federal Business Event Type Codes/Federal Business Event Type Codes Descriptive Flexfields - Delete a business event type code descriptive flex... | Federal Business Event Type Codes/Federal Business Event Type Codes Descriptive Flexfields Delete a business event type code descriptive flexfield  |
| Federal Fund Attributes - Delete a fund attribute | Federal Fund Attributes Delete a fund attribute  |
| Federal Fund Attributes/Federal Fund Attributes Descriptive Flexfields - Delete a fund attribute descriptive flexfield | Federal Fund Attributes/Federal Fund Attributes Descriptive Flexfields Delete a fund attribute descriptive flexfield  |
| Federal Payment Format Mappings - Delete a payment format mapping | Federal Payment Format Mappings Delete a payment format mapping  |
| Federal Payment Format Mappings/Federal Payment Format Mappings Descriptive Flexfields - Delete descriptive flexfield for a payment format ... | Federal Payment Format Mappings/Federal Payment Format Mappings Descriptive Flexfields Delete descriptive flexfield for a payment format mapping  |
| Federal Setup Options - Delete a setup option | Federal Setup Options Delete a setup option  |
| Federal Setup Options/Federal Setup Options Descriptive Flexfields - Delete a setup option descriptive flexfield | Federal Setup Options/Federal Setup Options Descriptive Flexfields Delete a setup option descriptive flexfield  |
| Federal Treasury Account Symbols - Delete a treasury account symbol | Federal Treasury Account Symbols Delete a treasury account symbol  |
| Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes - Delete a treasury account symbol business event ty... | Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes Delete a treasury account symbol business event type code  |
| Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes/Federal Treasury Account Symbols Business Event Type Codes Descriptive Flexfields - Delete a TAS BETC descriptive flexfield | Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes/Federal Treasury Account Symbols Business Event Type Codes Descriptive Flexfields Delete a TAS BETC descriptive flexfield  |
| Federal Treasury Account Symbols/Federal Treasury Account Symbols Descriptive Flexfields - Delete a treasury account symbol descriptive flexf... | Federal Treasury Account Symbols/Federal Treasury Account Symbols Descriptive Flexfields Delete a treasury account symbol descriptive flexfield  |
| Federal USSGL Accounts/Federal USSGL Accounts Descriptive Flexfields - Delete a USSGL descriptive flexfield | Federal USSGL Accounts/Federal USSGL Accounts Descriptive Flexfields Delete a USSGL descriptive flexfield  |
| Interim Payables Documents - Delete an interim payables document | Interim Payables Documents Delete an interim payables document  |
| Invoices - Delete an invoice | Invoices Delete an invoice  |
| Invoices/Attachments - Delete an attachment for an invoice | Invoices/Attachments Delete an attachment for an invoice  |
| Invoices/Invoice Installments - Delete an invoice installment | Invoices/Invoice Installments Delete an invoice installment  |
| Invoices/Invoice Lines - Delete an invoice line | Invoices/Invoice Lines Delete an invoice line  |
| Joint Venture General Ledger Distributions - Delete a joint venture general ledger distribution | Joint Venture General Ledger Distributions Delete a joint venture general ledger distribution  |
| Joint Venture General Ledger Transactions - Delete a joint venture general ledger transaction | Joint Venture General Ledger Transactions Delete a joint venture general ledger transaction  |
| Joint Venture Invoicing Partners - Delete a joint venture invoicing partner | Joint Venture Invoicing Partners Delete a joint venture invoicing partner  |
| Joint Venture Partner Contributions - Delete a joint venture partner contribution | Joint Venture Partner Contributions Delete a joint venture partner contribution  |
| Joint Venture Partner Contributions/Partner Contribution References - Delete a transaction reference for a partner contr... | Joint Venture Partner Contributions/Partner Contribution References Delete a transaction reference for a partner contribution  |
| Joint Venture Subledger Distributions - Delete a joint venture subledger distribution | Joint Venture Subledger Distributions Delete a joint venture subledger distribution  |
| Joint Venture Subledger Transactions - Delete a joint venture subledger transaction | Joint Venture Subledger Transactions Delete a joint venture subledger transaction  |
| Joint Ventures - Delete a joint venture | Joint Ventures Delete a joint venture  |
| Joint Ventures/Cost Centers - Delete a joint venture cost center | Joint Ventures/Cost Centers Delete a joint venture cost center  |
| Joint Ventures/Cost Centers/Distributable Value Sets - Delete a distributable value set | Joint Ventures/Cost Centers/Distributable Value Sets Delete a distributable value set  |
| Joint Ventures/Distributable Project Rules - Delete a distributable project rule | Joint Ventures/Distributable Project Rules Delete a distributable project rule  |
| Joint Ventures/Ownership Definitions - Delete an ownership definition | Joint Ventures/Ownership Definitions Delete an ownership definition  |
| Joint Ventures/Ownership Definitions/Ownership Percentages - Delete an ownership percentage for a stakeholder | Joint Ventures/Ownership Definitions/Ownership Percentages Delete an ownership percentage for a stakeholder  |
| Joint Ventures/Stakeholders - Delete a joint venture stakeholder | Joint Ventures/Stakeholders Delete a joint venture stakeholder  |
| Journal Batches - Delete a batch | Journal Batches Delete a batch  |
| Receipt Method Assignments - Delete a receipt method assigned to a customer acc... | Receipt Method Assignments Delete a receipt method assigned to a customer account or site  |
| Receivables Invoices - Delete a receivables invoice | Receivables Invoices Delete a receivables invoice  |
| Salesperson Reference Accounts - Delete a salesperson reference account | Salesperson Reference Accounts Delete a salesperson reference account  |
| Standard Receipts - Delete a standard receipt | Standard Receipts Delete a standard receipt  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Account Combinations List of Values - Get an account combination | Account Combinations List of Values Get an account combination  |
| Accounting Calendars List of Values - Get a calendar | Accounting Calendars List of Values Get a calendar  |
| Accounting Period Status List of Values - Get a period status | Accounting Period Status List of Values Get a period status  |
| Accounting Periods List of Values - Get a period | Accounting Periods List of Values Get a period  |
| Asset Books List of Values - Get an asset book | Asset Books List of Values Get an asset book  |
| Bank Account User Rules - Get a country-specific rule | Bank Account User Rules Get a country-specific rule  |
| Bank Accounts - Get a bank account | Bank Accounts Get a bank account  |
| Bank Accounts List of Values - Get a bank account | Bank Accounts List of Values Get a bank account  |
| Bank Accounts/Bank Account Descriptive Flexfields - Get a descriptive flexfield for a bank account | Bank Accounts/Bank Account Descriptive Flexfields Get a descriptive flexfield for a bank account  |
| Bank Accounts/Bank Account Global Descriptive Flexfields - Get a global descriptive flexfield for a bank acco... | Bank Accounts/Bank Account Global Descriptive Flexfields Get a global descriptive flexfield for a bank account  |
| Bank Accounts/Bank Account Grants - Get a grant for a bank account | Bank Accounts/Bank Account Grants Get a grant for a bank account  |
| Bank Accounts/Bank Account Payment Documents - Get a payment document for a bank account | Bank Accounts/Bank Account Payment Documents Get a payment document for a bank account  |
| Bank Accounts/Bank Account Payment Documents/Bank Account Checkbooks - Get a checkbook for a bank account | Bank Accounts/Bank Account Payment Documents/Bank Account Checkbooks Get a checkbook for a bank account  |
| Bank Accounts/Bank Account Transaction Creation Rules - Get a bank statement transaction creation rule for... | Bank Accounts/Bank Account Transaction Creation Rules Get a bank statement transaction creation rule for a bank account  |
| Bank Accounts/Bank Account Uses - Get a bank account use for a bank account | Bank Accounts/Bank Account Uses Get a bank account use for a bank account  |
| Bank Accounts/Bank Account Uses/Bank Account Payment Document Categories - Get a payment document category for a business uni... | Bank Accounts/Bank Account Uses/Bank Account Payment Document Categories Get a payment document category for a business unit  |
| Bank Branches - Get a bank branch | Bank Branches Get a bank branch  |
| Bank Branches List of Values - Get a bank branch | Bank Branches List of Values Get a bank branch  |
| Bank Collection Business Units List of Values - Get a bank collection business unit | Bank Collection Business Units List of Values Get a bank collection business unit  |
| Banks - Get a bank | Banks Get a bank  |
| Banks List of Values - Get a bank | Banks List of Values Get a bank  |
| Bill Management Users - Get a bill management user | Bill Management Users Get a bill management user  |
| Brazilian Fiscal Documents - Get a fiscal document | Brazilian Fiscal Documents Get a fiscal document  |
| Budgetary Control for Enterprise Performance Management Budget Transactions - Get a budgetary control budget entry transaction i... | Budgetary Control for Enterprise Performance Management Budget Transactions Get a budgetary control budget entry transaction interface row  |
| Budgetary Control Results Budget Impacts for Labor Cost Management - Get the budget impacts of a budgetary control resu... | Budgetary Control Results Budget Impacts for Labor Cost Management Get the budget impacts of a budgetary control result  |
| Budgetary Control Results for Enterprise Performance Management Budget Transactions - Get a budget entry transaction budgetary control r... | Budgetary Control Results for Enterprise Performance Management Budget Transactions Get a budget entry transaction budgetary control result  |
| Budgetary Control Results for Labor Cost Management - Get a budgetary control result | Budgetary Control Results for Labor Cost Management Get a budgetary control result  |
| Calendar Types List of Values - Get a payables calendar | Calendar Types List of Values Get a payables calendar  |
| Cash Pools - Get a cash pool | Cash Pools Get a cash pool  |
| Cash Pools/Cash Pool Members - Get a cash pool member | Cash Pools/Cash Pool Members Get a cash pool member  |
| Chart of Accounts List of Values - Get a chart of account | Chart of Accounts List of Values Get a chart of account  |
| Collection Promises - Get a collection promise | Collection Promises Get a collection promise  |
| Collection Promises/Collection Promises Descriptive Flexfields - Get a collection promise descriptive flexfield | Collection Promises/Collection Promises Descriptive Flexfields Get a collection promise descriptive flexfield  |
| Collections Strategies - Get a strategy assignment | Collections Strategies Get a strategy assignment  |
| Collections Strategies/Strategy Template Tasks - Get a strategy template task | Collections Strategies/Strategy Template Tasks Get a strategy template task  |
| Collections Strategies/Tasks - Get a task assignment | Collections Strategies/Tasks Get a task assignment  |
| Collections Strategies/User Tasks - Get a user task assignment | Collections Strategies/User Tasks Get a user task assignment  |
| Control Budget Periods List of Values - Get a control budget period | Control Budget Periods List of Values Get a control budget period  |
| Control Budgets List of Values - Get a control budget | Control Budgets List of Values Get a control budget  |
| Credit and Collections Data Points - Get a credit and collections data point | Credit and Collections Data Points Get a credit and collections data point  |
| Currency Conversion Types List of Values - Get a currency conversion type | Currency Conversion Types List of Values Get a currency conversion type  |
| Currency Rates - GET action not supported | Currency Rates GET action not supported  |
| Customer Account Sites List of Values - Get a customer account site details | Customer Account Sites List of Values Get a customer account site details  |
| Data Access Set Ledgers List of Values - Get a ledger or ledger set | Data Access Set Ledgers List of Values Get a ledger or ledger set  |
| Data Access Sets List of Values - Get a data access set | Data Access Sets List of Values Get a data access set  |
| Data Point Values - Get a data point value | Data Point Values Get a data point value  |
| Data Security for Users - Get a data assignment by user and role | Data Security for Users Get a data assignment by user and role  |
| Debit Authorizations - Get a debit authorization | Debit Authorizations Get a debit authorization  |
| Debit Authorizations/Debit Authorization Versions - Get a debit authorization version | Debit Authorizations/Debit Authorization Versions Get a debit authorization version  |
| Distribution Sets List of Values - Get a distribution set | Distribution Sets List of Values Get a distribution set  |
| Document Fiscal Classifications List of Values - Get a document fiscal classification | Document Fiscal Classifications List of Values Get a document fiscal classification  |
| Document Sequence Derivations - Get a document sequence derivation record | Document Sequence Derivations Get a document sequence derivation record  |
| Early Payment Offers - Get a standing early payment offer | Early Payment Offers Get a standing early payment offer  |
| Early Payment Offers/Early Payment Offer Assignments - Get a standing offer assignment | Early Payment Offers/Early Payment Offer Assignments Get a standing offer assignment  |
| Early Payment Offers/Early Payment Offer Assignments/Early Payment Offers Descriptive Flexfields - Get a descriptive flexfield for a standing offer a... | Early Payment Offers/Early Payment Offer Assignments/Early Payment Offers Descriptive Flexfields Get a descriptive flexfield for a standing offer assignment  |
| Encumbrance Types List of Values - Get a journal encumbrance type | Encumbrance Types List of Values Get a journal encumbrance type  |
| ERP Business Events - Get a business event record | ERP Business Events Get a business event record  |
| ERP Integrations - Get job details | ERP Integrations Get job details  |
| Expense Accommodations Polices - Get a single accommodations expense policy record ... | Expense Accommodations Polices Get a single accommodations expense policy record for an accommodations expense policy identifier  |
| Expense Accommodations Polices/Expense Accommodations Policy Lines - Get a single accommodations expense policy line re... | Expense Accommodations Polices/Expense Accommodations Policy Lines Get a single accommodations expense policy line record using an accommodations expense policy rate line identifier  |
| Expense Airfare Policies - Get an expense airfare policy | Expense Airfare Policies Get an expense airfare policy  |
| Expense Airfare Policies/Expense Airfare Policy Lines - Get an expense airfare policy line | Expense Airfare Policies/Expense Airfare Policy Lines Get an expense airfare policy line  |
| Expense Business Unit Settings - Get a business unit setting record | Expense Business Unit Settings Get a business unit setting record  |
| Expense Cash Advances - Get a cash advance record for the requested cash a... | Expense Cash Advances Get a cash advance record for the requested cash advance identifier  |
| Expense Cash Advances/Cash Advance Descriptive Flexfields - Get a descriptive flexfield record for the cash ad... | Expense Cash Advances/Cash Advance Descriptive Flexfields Get a descriptive flexfield record for the cash advance  |
| Expense Conversion Rate Options - Get a single conversion rate option record for a g... | Expense Conversion Rate Options Get a single conversion rate option record for a given conversion rate option row identifier  |
| Expense Conversion Rate Options/Conversion Rate Tolerances - Get a single conversion rate tolerance record for ... | Expense Conversion Rate Options/Conversion Rate Tolerances Get a single conversion rate tolerance record for a given identifier of a conversion rate exception for a specific currency within a business unit  |
| Expense Credit Card Transactions - Get a corporate card transaction | Expense Credit Card Transactions Get a corporate card transaction  |
| Expense Delegations - Get an expense delegation | Expense Delegations Get an expense delegation  |
| Expense Descriptive Flexfield Contexts - Get a descriptive flexfield context for expenses a... | Expense Descriptive Flexfield Contexts Get a descriptive flexfield context for expenses and expense reports  |
| Expense Descriptive Flexfield Segments - Get a descriptive flexfield segment for expenses a... | Expense Descriptive Flexfield Segments Get a descriptive flexfield segment for expenses and expense reports  |
| Expense Distributions - Get an expense distribution record for a specific ... | Expense Distributions Get an expense distribution record for a specific expense distribution identifier  |
| Expense Distributions/Project Descriptive Flexfields - Get a project-related descriptive flexfield record... | Expense Distributions/Project Descriptive Flexfields Get a project-related descriptive flexfield record for a specific expense distribution identifier  |
| Expense Locations - Get a single expense location record using the exp... | Expense Locations Get a single expense location record using the expense location identifier  |
| Expense Meals Policies - Get a single expense meals policy record for a mea... | Expense Meals Policies Get a single expense meals policy record for a meals policy identifier  |
| Expense Meals Policies/Expense Meals Policy Lines - Get a single expense meals policy line record for ... | Expense Meals Policies/Expense Meals Policy Lines Get a single expense meals policy line record for a meals policy rate line identifier  |
| Expense Mileage Policies - Get a single expense mileage policy record for a g... | Expense Mileage Policies Get a single expense mileage policy record for a given mileage policy identifier  |
| Expense Mileage Policies/Expense Mileage Policy Lines - Get a single expense mileage policy line record fo... | Expense Mileage Policies/Expense Mileage Policy Lines Get a single expense mileage policy line record for a given mileage policy rate line identifier  |
| Expense Persons - Get an expense person record | Expense Persons Get an expense person record  |
| Expense Preferences - Get an expense preferences record for a person | Expense Preferences Get an expense preferences record for a person  |
| Expense Preferred Types - Get a single preferred expense type | Expense Preferred Types Get a single preferred expense type  |
| Expense Profile Attributes - Get the record of a person's expense profile attri... | Expense Profile Attributes Get the record of a person's expense profile attributes  |
| Expense Reports - Get an expense report record | Expense Reports Get an expense report record  |
| Expense Reports/Attachments - Get an attachment record related to an expense rep... | Expense Reports/Attachments Get an attachment record related to an expense report  |
| Expense Reports/Expense Notes - Get a single expense note record | Expense Reports/Expense Notes Get a single expense note record  |
| Expense Reports/Expense Processing Details - Get a single processing detail record of an expens... | Expense Reports/Expense Processing Details Get a single processing detail record of an expense report  |
| Expense Reports/Expense Report Descriptive Flexfields - Get an expense report header descriptive flexfield... | Expense Reports/Expense Report Descriptive Flexfields Get an expense report header descriptive flexfield record for a specific expense report identifier  |
| Expense Reports/Expense Report Payments - Get an expense report payment record | Expense Reports/Expense Report Payments Get an expense report payment record  |
| Expense Reports/Expenses - Get an expense record for a specific expense ident... | Expense Reports/Expenses Get an expense record for a specific expense identifier  |
| Expense Reports/Expenses/Attachments - Get an attachment record related to an expense rep... | Expense Reports/Expenses/Attachments Get an attachment record related to an expense report  |
| Expense Reports/Expenses/Duplicate Expenses - Get a duplicate expense record | Expense Reports/Expenses/Duplicate Expenses Get a duplicate expense record  |
| Expense Reports/Expenses/Expense Attendees - Get an expense attendee record for a specific expe... | Expense Reports/Expenses/Expense Attendees Get an expense attendee record for a specific expense attendee identifier  |
| Expense Reports/Expenses/Expense Descriptive Flexfields - Get a descriptive flexfield record for the related... | Expense Reports/Expenses/Expense Descriptive Flexfields Get a descriptive flexfield record for the related expense  |
| Expense Reports/Expenses/Expense Distributions - Get an expense distribution record for a specific ... | Expense Reports/Expenses/Expense Distributions Get an expense distribution record for a specific expense distribution identifier  |
| Expense Reports/Expenses/Expense Distributions/Projects Descriptive Flexfields - Get a project-related descriptive flexfield record... | Expense Reports/Expenses/Expense Distributions/Projects Descriptive Flexfields Get a project-related descriptive flexfield record for a specific expense distribution identifier  |
| Expense Reports/Expenses/Expense Errors - Get an expense error record | Expense Reports/Expenses/Expense Errors Get an expense error record  |
| Expense Reports/Expenses/Expense Itemizations - Get an expense itemization record for a specific e... | Expense Reports/Expenses/Expense Itemizations Get an expense itemization record for a specific expense  |
| Expense Reports/Expenses/Matched Expenses - Get a matched expense record | Expense Reports/Expenses/Matched Expenses Get a matched expense record  |
| Expense Scanned Images - GET action not supported | Expense Scanned Images GET action not supported  |
| Expense System Options - Get a setup option record | Expense System Options Get a setup option record  |
| Expense Templates - Get a single expense template | Expense Templates Get a single expense template  |
| Expense Templates/Expense Types - Get a single expense type in an expense template | Expense Templates/Expense Types Get a single expense type in an expense template  |
| Expense Templates/Expense Types/Itemized Expense Types - Get a single itemized expense type record | Expense Templates/Expense Types/Itemized Expense Types Get a single itemized expense type record  |
| Expense Templates/Expense Types/Preferred Agency Lists - Get a single preferred agency list record | Expense Templates/Expense Types/Preferred Agency Lists Get a single preferred agency list record  |
| Expense Templates/Expense Types/Preferred Merchant Lists - Get a single preferred merchant list record | Expense Templates/Expense Types/Preferred Merchant Lists Get a single preferred merchant list record  |
| Expense Templates/Mobile Expense Types - Get a single mobile expense type record | Expense Templates/Mobile Expense Types Get a single mobile expense type record  |
| Expense Travel Itineraries - Get a travel itinerary | Expense Travel Itineraries Get a travel itinerary  |
| Expense Travel Itineraries/Expense Travel Itinerary Reservations - Get a travel itinerary reservation | Expense Travel Itineraries/Expense Travel Itinerary Reservations Get a travel itinerary reservation  |
| Expense Travel Itineraries/Expense Travel Itinerary Reservations/Expense Travel Itinerary Reservation Items - Get a travel itinerary reservation item | Expense Travel Itineraries/Expense Travel Itinerary Reservations/Expense Travel Itinerary Reservation Items Get a travel itinerary reservation item  |
| Expense Types - Get a single expense type record for the requested... | Expense Types Get a single expense type record for the requested expense type  |
| Expense Types/Expenses Type Itemize - Get a single itemized expense type record for the ... | Expense Types/Expenses Type Itemize Get a single itemized expense type record for the requested itemized expense type  |
| Expense Types/Preferred Agency Lists - Get a single preferred agency list record | Expense Types/Preferred Agency Lists Get a single preferred agency list record  |
| Expense Types/Preferred Merchant Lists - Get a single preferred merchant list record | Expense Types/Preferred Merchant Lists Get a single preferred merchant list record  |
| Expenses - Get an expense record of an expense identifier | Expenses Get an expense record of an expense identifier  |
| Expenses/Attachments - Get the attachment record related to the expense r... | Expenses/Attachments Get the attachment record related to the expense report  |
| Expenses/Duplicate Expenses - Get a duplicate expense record | Expenses/Duplicate Expenses Get a duplicate expense record  |
| Expenses/Expense Attendees - Get an expense attendee record for an expense atte... | Expenses/Expense Attendees Get an expense attendee record for an expense attendee identifier  |
| Expenses/Expense Descriptive Flexfields - Get the descriptive flexfield record for the relat... | Expenses/Expense Descriptive Flexfields Get the descriptive flexfield record for the related expense  |
| Expenses/Expense Distributions - Get an expense distribution record for an expense ... | Expenses/Expense Distributions Get an expense distribution record for an expense distribution identifier  |
| Expenses/Expense Distributions/Project Descriptive Flexfields - Get the project-related descriptive flexfield reco... | Expenses/Expense Distributions/Project Descriptive Flexfields Get the project-related descriptive flexfield record for an expense distribution identifier  |
| Expenses/Expense Errors - Get an expense error record | Expenses/Expense Errors Get an expense error record  |
| Expenses/Expense Itemizations - Get an expense itemization record for an expense | Expenses/Expense Itemizations Get an expense itemization record for an expense  |
| Expenses/Matched Expenses - Get a matched expense record | Expenses/Matched Expenses Get a matched expense record  |
| External Bank Accounts - Get an external bank account | External Bank Accounts Get an external bank account  |
| External Bank Accounts/Account Owners - Get an account owner for an external bank account | External Bank Accounts/Account Owners Get an account owner for an external bank account  |
| External Cash Transactions - Get an external transaction | External Cash Transactions Get an external transaction  |
| External Cash Transactions/Attachments - Get an attachment for an external transaction | External Cash Transactions/Attachments Get an attachment for an external transaction  |
| External Cash Transactions/Descriptive Flexfields - Get a descriptive flexfield for an external transa... | External Cash Transactions/Descriptive Flexfields Get a descriptive flexfield for an external transaction  |
| External Payees - Get an external payee | External Payees Get an external payee  |
| External Payees/External Party Payment Methods - Get a payment method assigned to a payee | External Payees/External Party Payment Methods Get a payment method assigned to a payee  |
| Federal Account Symbols - Get a federal account symbol | Federal Account Symbols Get a federal account symbol  |
| Federal Account Symbols List of Values - Get a federal account symbol | Federal Account Symbols List of Values Get a federal account symbol  |
| Federal Account Symbols/Federal Account Symbols Descriptive Flexfields - Get a federal account symbol descriptive flexfield | Federal Account Symbols/Federal Account Symbols Descriptive Flexfields Get a federal account symbol descriptive flexfield  |
| Federal Adjustment Business Event Type Codes List of Values - Get an Adjustment BETC | Federal Adjustment Business Event Type Codes List of Values Get an Adjustment BETC  |
| Federal Agency Location Codes - Get an agency location code | Federal Agency Location Codes Get an agency location code  |
| Federal Agency Location Codes/Federal Agency Location Codes Descriptive Flexfields - Get an agency location code descriptive flexfield | Federal Agency Location Codes/Federal Agency Location Codes Descriptive Flexfields Get an agency location code descriptive flexfield  |
| Federal Business Event Type Codes - Get a business event type code | Federal Business Event Type Codes Get a business event type code  |
| Federal Business Event Type Codes List of Values - Get a BETC | Federal Business Event Type Codes List of Values Get a BETC  |
| Federal Business Event Type Codes/Federal Business Event Type Codes Descriptive Flexfields - Get a business event type code descriptive flexfie... | Federal Business Event Type Codes/Federal Business Event Type Codes Descriptive Flexfields Get a business event type code descriptive flexfield  |
| Federal Fund Attributes - Get a fund attribute | Federal Fund Attributes Get a fund attribute  |
| Federal Fund Attributes/Federal Fund Attributes Descriptive Flexfields - Get a fund attribute descriptive flexfield | Federal Fund Attributes/Federal Fund Attributes Descriptive Flexfields Get a fund attribute descriptive flexfield  |
| Federal Partial Segments List of Values - Get a partial segment | Federal Partial Segments List of Values Get a partial segment  |
| Federal Payment Format Mappings - Get a payment format mapping | Federal Payment Format Mappings Get a payment format mapping  |
| Federal Payment Format Mappings/Federal Payment Format Mappings Descriptive Flexfields - Get descriptive flexfield for a payment format map... | Federal Payment Format Mappings/Federal Payment Format Mappings Descriptive Flexfields Get descriptive flexfield for a payment format mapping  |
| Federal Setup Options - Get a setup option | Federal Setup Options Get a setup option  |
| Federal Setup Options/Federal Setup Options Descriptive Flexfields - Get a setup option descriptive flexfield | Federal Setup Options/Federal Setup Options Descriptive Flexfields Get a setup option descriptive flexfield  |
| Federal Treasury Account Symbol Business Event Type Codes List of Values - Get a treasury account symbol business event type ... | Federal Treasury Account Symbol Business Event Type Codes List of Values Get a treasury account symbol business event type code  |
| Federal Treasury Account Symbols - Get a treasury account symbol | Federal Treasury Account Symbols Get a treasury account symbol  |
| Federal Treasury Account Symbols List of Values - Get a treasury account symbol | Federal Treasury Account Symbols List of Values Get a treasury account symbol  |
| Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes - Get a treasury account symbol business event type ... | Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes Get a treasury account symbol business event type code  |
| Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes/Federal Treasury Account Symbols Business Event Type Codes Descriptive Flexfields - Get a TAS BETC descriptive flexfield | Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes/Federal Treasury Account Symbols Business Event Type Codes Descriptive Flexfields Get a TAS BETC descriptive flexfield  |
| Federal Treasury Account Symbols/Federal Treasury Account Symbols Descriptive Flexfields - Get a treasury account symbol descriptive flexfiel... | Federal Treasury Account Symbols/Federal Treasury Account Symbols Descriptive Flexfields Get a treasury account symbol descriptive flexfield  |
| Federal USSGL Accounts - Get a USSGL account | Federal USSGL Accounts Get a USSGL account  |
| Federal USSGL Accounts/Federal USSGL Accounts Descriptive Flexfields - Get a federal USSGL account descriptive flexfield | Federal USSGL Accounts/Federal USSGL Accounts Descriptive Flexfields Get a federal USSGL account descriptive flexfield  |
| Financials Business Units List of Values - Get a business unit | Financials Business Units List of Values Get a business unit  |
| Fiscal Document Business Units List of Values - Get a fiscal document business unit | Fiscal Document Business Units List of Values Get a fiscal document business unit  |
| Income Tax Regions List of Values - Get an income tax region | Income Tax Regions List of Values Get an income tax region  |
| Income Tax Types - Get an income tax type | Income Tax Types Get an income tax type  |
| Income Tax Types List of Values - Get an income tax type | Income Tax Types List of Values Get an income tax type  |
| Input Tax Classifications List of Values - Get an input tax classification | Input Tax Classifications List of Values Get an input tax classification  |
| Instrument Assignments - Get a payment instrument assignment | Instrument Assignments Get a payment instrument assignment  |
| Intercompany Organizations List of Values - Get an intercompany organization | Intercompany Organizations List of Values Get an intercompany organization  |
| Intercompany Transaction Types List of Values - Get an intercompany transaction type | Intercompany Transaction Types List of Values Get an intercompany transaction type  |
| Interim Payables Documents - Get an interim payables document | Interim Payables Documents Get an interim payables document  |
| Invoice Approvals and Notifications History - Get an invoice approvals and notifications history | Invoice Approvals and Notifications History Get an invoice approvals and notifications history  |
| Invoice Holds - Get an invoice hold | Invoice Holds Get an invoice hold  |
| Invoice Holds List of Values - Get an invoice hold | Invoice Holds List of Values Get an invoice hold  |
| Invoice Holds/Invoice Holds Descriptive Flexfields - Get descriptive flexfields for an invoice hold | Invoice Holds/Invoice Holds Descriptive Flexfields Get descriptive flexfields for an invoice hold  |
| Invoice Tolerances - Get a payables invoice tolerance | Invoice Tolerances Get a payables invoice tolerance  |
| Invoice Tolerances List of Values - Get an invoice tolerance | Invoice Tolerances List of Values Get an invoice tolerance  |
| Invoices - Get an invoice | Invoices Get an invoice  |
| Invoices/Applied Prepayments - Get an applied prepayment | Invoices/Applied Prepayments Get an applied prepayment  |
| Invoices/Attachments - Get an attachment for an invoice | Invoices/Attachments Get an attachment for an invoice  |
| Invoices/Available Prepayments - Get an available prepayment | Invoices/Available Prepayments Get an available prepayment  |
| Invoices/Invoice Installments - Get an invoice installment | Invoices/Invoice Installments Get an invoice installment  |
| Invoices/Invoice Installments/Invoice Installments Descriptive Flexfields - Get descriptive flexfields for an invoice installm... | Invoices/Invoice Installments/Invoice Installments Descriptive Flexfields Get descriptive flexfields for an invoice installment  |
| Invoices/Invoice Installments/Invoice Installments Global Descriptive Flexfields - Get global descriptive flexfields for an invoice i... | Invoices/Invoice Installments/Invoice Installments Global Descriptive Flexfields Get global descriptive flexfields for an invoice installment  |
| Invoices/Invoice Lines - Get an invoice line | Invoices/Invoice Lines Get an invoice line  |
| Invoices/Invoice Lines/Invoice Distributions - Get an invoice distribution | Invoices/Invoice Lines/Invoice Distributions Get an invoice distribution  |
| Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Descriptive Flexfields - Get descriptive flexfields for an invoice distribu... | Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Descriptive Flexfields Get descriptive flexfields for an invoice distribution  |
| Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Global Descriptive Flexfields - Get global descriptive flexfields for an invoice d... | Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Global Descriptive Flexfields Get global descriptive flexfields for an invoice distribution  |
| Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Project Descriptive Flexfields - Get a project-related descriptive flexfield for an... | Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Project Descriptive Flexfields Get a project-related descriptive flexfield for an invoice distribution  |
| Invoices/Invoice Lines/Invoice Lines Descriptive Flexfields - Get descriptive flexfields for an invoice line | Invoices/Invoice Lines/Invoice Lines Descriptive Flexfields Get descriptive flexfields for an invoice line  |
| Invoices/Invoice Lines/Invoice Lines Global Descriptive Flexfields - Get global descriptive flexfields for an invoice l... | Invoices/Invoice Lines/Invoice Lines Global Descriptive Flexfields Get global descriptive flexfields for an invoice line  |
| Invoices/Invoice Lines/Invoice Lines Project Descriptive Flexfields - Get a project-related descriptive flexfield for an... | Invoices/Invoice Lines/Invoice Lines Project Descriptive Flexfields Get a project-related descriptive flexfield for an invoice line  |
| Invoices/Invoices Descriptive Flexfields - Get descriptive flexfields for an invoice | Invoices/Invoices Descriptive Flexfields Get descriptive flexfields for an invoice  |
| Invoices/Invoices Global Descriptive Flexfields - Get global descriptive flexfields for an invoice | Invoices/Invoices Global Descriptive Flexfields Get global descriptive flexfields for an invoice  |
| Joint Venture Approval Counts - Get a count of joint ventures with the specified a... | Joint Venture Approval Counts Get a count of joint ventures with the specified approved date  |
| Joint Venture General Ledger Distribution Totals - Get distribution totals for a joint venture with t... | Joint Venture General Ledger Distribution Totals Get distribution totals for a joint venture with the specified distribution status  |
| Joint Venture General Ledger Distributions - Get a joint venture general ledger distribution | Joint Venture General Ledger Distributions Get a joint venture general ledger distribution  |
| Joint Venture General Ledger Distributions/Account Key Flexfields - Get account key flexfields for a journal line asso... | Joint Venture General Ledger Distributions/Account Key Flexfields Get account key flexfields for a journal line associated with a joint venture transaction  |
| Joint Venture General Ledger Distributions/Offset Account Key Flexfields - Get account key flexfields for a journal line asso... | Joint Venture General Ledger Distributions/Offset Account Key Flexfields Get account key flexfields for a journal line associated with a joint venture transaction  |
| Joint Venture General Ledger Transaction Totals - Get transaction totals for a joint venture with th... | Joint Venture General Ledger Transaction Totals Get transaction totals for a joint venture with the specified transaction status  |
| Joint Venture General Ledger Transactions - Get a joint venture general ledger transaction | Joint Venture General Ledger Transactions Get a joint venture general ledger transaction  |
| Joint Venture General Ledger Transactions/Account Key Flexfields - Get account key flexfields for a journal line asso... | Joint Venture General Ledger Transactions/Account Key Flexfields Get account key flexfields for a journal line associated with a joint venture transaction  |
| Joint Venture General Ledger Transactions/Joint Venture Transaction Descriptive Flexfields - Get descriptive flexfields for a joint venture tra... | Joint Venture General Ledger Transactions/Joint Venture Transaction Descriptive Flexfields Get descriptive flexfields for a joint venture transaction  |
| Joint Venture Invoicing Partners - Get a joint venture invoicing partner | Joint Venture Invoicing Partners Get a joint venture invoicing partner  |
| Joint Venture Invoicing Partners List of Values - Get a joint venture invoicing partner | Joint Venture Invoicing Partners List of Values Get a joint venture invoicing partner  |
| Joint Venture Invoicing Partners/Invoicing Partner Descriptive Flexfields - Get descriptive flexfields for a joint venture | Joint Venture Invoicing Partners/Invoicing Partner Descriptive Flexfields Get descriptive flexfields for a joint venture  |
| Joint Venture Options - Get joint venture options | Joint Venture Options Get joint venture options  |
| Joint Venture Options/Joint Venture Options Descriptive Flexfields - Get descriptive flexfields for joint venture optio... | Joint Venture Options/Joint Venture Options Descriptive Flexfields Get descriptive flexfields for joint venture options  |
| Joint Venture Partner Contributions - Get a joint venture partner contribution | Joint Venture Partner Contributions Get a joint venture partner contribution  |
| Joint Venture Partner Contributions/Partner Contribution Descriptive Flexfields - Get descriptive flexfields for a partner contribut... | Joint Venture Partner Contributions/Partner Contribution Descriptive Flexfields Get descriptive flexfields for a partner contribution  |
| Joint Venture Partner Contributions/Partner Contribution References - Get a transaction reference for a joint venture pa... | Joint Venture Partner Contributions/Partner Contribution References Get a transaction reference for a joint venture partner contribution  |
| Joint Venture SLA Date References - Get a supporting reference that derives a date val... | Joint Venture SLA Date References Get a supporting reference that derives a date value  |
| Joint Venture SLA Supporting References List of Values - Get a supporting reference | Joint Venture SLA Supporting References List of Values Get a supporting reference  |
| Joint Venture Stakeholders List of Values - Get a stakeholder associated with a joint venture | Joint Venture Stakeholders List of Values Get a stakeholder associated with a joint venture  |
| Joint Venture Status Counts - Get a count of joint ventures with the specified s... | Joint Venture Status Counts Get a count of joint ventures with the specified status  |
| Joint Venture Subledger Accounting Distribution Totals - Get distribution totals for a joint venture with t... | Joint Venture Subledger Accounting Distribution Totals Get distribution totals for a joint venture with the specified distribution status  |
| Joint Venture Subledger Accounting Transaction Totals - Get transaction totals for a joint venture with th... | Joint Venture Subledger Accounting Transaction Totals Get transaction totals for a joint venture with the specified transaction status  |
| Joint Venture Subledger Distributions - Get a joint venture subledger distribution | Joint Venture Subledger Distributions Get a joint venture subledger distribution  |
| Joint Venture Subledger Distributions/Account Key Flexfields - Get account key flexfields for a journal line asso... | Joint Venture Subledger Distributions/Account Key Flexfields Get account key flexfields for a journal line associated with a joint venture transaction  |
| Joint Venture Subledger Distributions/Offset Account Key Flexfields - Get account key flexfields for a journal line asso... | Joint Venture Subledger Distributions/Offset Account Key Flexfields Get account key flexfields for a journal line associated with a joint venture transaction  |
| Joint Venture Subledger Transactions - Get a joint venture subledger transaction | Joint Venture Subledger Transactions Get a joint venture subledger transaction  |
| Joint Venture Subledger Transactions/Account Key Flexfields - Get account key flexfields for a journal line asso... | Joint Venture Subledger Transactions/Account Key Flexfields Get account key flexfields for a journal line associated with a joint venture transaction  |
| Joint Venture Subledger Transactions/Joint Venture Transaction Descriptive Flexfields - Get descriptive flexfields for a joint venture tra... | Joint Venture Subledger Transactions/Joint Venture Transaction Descriptive Flexfields Get descriptive flexfields for a joint venture transaction  |
| Joint Ventures - Get a joint venture | Joint Ventures Get a joint venture  |
| Joint Ventures/Cost Centers - Get a joint venture cost center | Joint Ventures/Cost Centers Get a joint venture cost center  |
| Joint Ventures/Cost Centers/Cost Center Descriptive Flexfields - Get descriptive flexfields for a cost center assoc... | Joint Ventures/Cost Centers/Cost Center Descriptive Flexfields Get descriptive flexfields for a cost center associated with a joint venture  |
| Joint Ventures/Cost Centers/Distributable Value Sets - Get a distributable value set | Joint Ventures/Cost Centers/Distributable Value Sets Get a distributable value set  |
| Joint Ventures/Distributable Project Rules - Get a distributable project rule | Joint Ventures/Distributable Project Rules Get a distributable project rule  |
| Joint Ventures/Joint Venture Descriptive Flexfieds - Get descriptive flexfields for a joint venture | Joint Ventures/Joint Venture Descriptive Flexfieds Get descriptive flexfields for a joint venture  |
| Joint Ventures/Joint Venture Messages - Get a joint venture message | Joint Ventures/Joint Venture Messages Get a joint venture message  |
| Joint Ventures/Ownership Definitions - Get an ownership definition | Joint Ventures/Ownership Definitions Get an ownership definition  |
| Joint Ventures/Ownership Definitions/Ownership Definition Descriptive Flexfields - Get descriptive flexfields for an ownership defini... | Joint Ventures/Ownership Definitions/Ownership Definition Descriptive Flexfields Get descriptive flexfields for an ownership definition associated with a joint venture  |
| Joint Ventures/Ownership Definitions/Ownership Percentages - Get an ownership percentage for a stakeholder | Joint Ventures/Ownership Definitions/Ownership Percentages Get an ownership percentage for a stakeholder  |
| Joint Ventures/Ownership Definitions/Ownership Percentages/Ownership Percentage Descriptive Flexfields - Get descriptive flexfields for an ownership percen... | Joint Ventures/Ownership Definitions/Ownership Percentages/Ownership Percentage Descriptive Flexfields Get descriptive flexfields for an ownership percentage associated with an ownership definition  |
| Joint Ventures/Stakeholders - Get a joint venture stakeholder | Joint Ventures/Stakeholders Get a joint venture stakeholder  |
| Joint Ventures/Stakeholders/Stakeholder Descriptive Flexfields - Get descriptive flexfields for a joint venture sta... | Joint Ventures/Stakeholders/Stakeholder Descriptive Flexfields Get descriptive flexfields for a joint venture stakeholder  |
| Journal Batches - Get a batch | Journal Batches Get a batch  |
| Journal Batches/Action Logs - Get a log entry | Journal Batches/Action Logs Get a log entry  |
| Journal Batches/Attachments - Get an attachment | Journal Batches/Attachments Get an attachment  |
| Journal Batches/Descriptive Flexfields - Get a descriptive flexfield | Journal Batches/Descriptive Flexfields Get a descriptive flexfield  |
| Journal Batches/Errors - Get an error | Journal Batches/Errors Get an error  |
| Journal Batches/Journal Headers - Get a journal | Journal Batches/Journal Headers Get a journal  |
| Journal Batches/Journal Headers/Attachments - Get an attachment | Journal Batches/Journal Headers/Attachments Get an attachment  |
| Journal Batches/Journal Headers/Descriptive Flexfields - Get a descriptive flexfield | Journal Batches/Journal Headers/Descriptive Flexfields Get a descriptive flexfield  |
| Journal Batches/Journal Headers/Global Descriptive Flexfields - Get a global descriptive flexfield | Journal Batches/Journal Headers/Global Descriptive Flexfields Get a global descriptive flexfield  |
| Journal Batches/Journal Headers/Journal Lines - Get a line | Journal Batches/Journal Headers/Journal Lines Get a line  |
| Journal Batches/Journal Headers/Journal Lines/Captured Information Descriptive Flexfields - Get captured information descriptive flexfield | Journal Batches/Journal Headers/Journal Lines/Captured Information Descriptive Flexfields Get captured information descriptive flexfield  |
| Journal Batches/Journal Headers/Journal Lines/Descriptive Flexfields - Get a descriptive flexfield | Journal Batches/Journal Headers/Journal Lines/Descriptive Flexfields Get a descriptive flexfield  |
| Journal Batches/Journal Headers/Journal Lines/Global Descriptive Flexfields - Get a global descriptive flexfield | Journal Batches/Journal Headers/Journal Lines/Global Descriptive Flexfields Get a global descriptive flexfield  |
| Journal Categories List of Values - Get a journal category | Journal Categories List of Values Get a journal category  |
| Journal Sources List of Values - Get a journal source | Journal Sources List of Values Get a journal source  |
| Ledger Balances - GET action not supported | Ledger Balances GET action not supported  |
| Ledgers List of Values - Get a ledger | Ledgers List of Values Get a ledger  |
| Legal Entities List of Values - Get a legal entity | Legal Entities List of Values Get a legal entity  |
| Legal Jurisdictions List of Values - Get a legal jurisdiction | Legal Jurisdictions List of Values Get a legal jurisdiction  |
| Legal Reporting Units List of Values - Get a legal reporting unit | Legal Reporting Units List of Values Get a legal reporting unit  |
| Netting Legal Entities List of Values - Get a netting legal entity | Netting Legal Entities List of Values Get a netting legal entity  |
| Output Tax Classifications List of Values - Get an output tax classification | Output Tax Classifications List of Values Get an output tax classification  |
| Ownership Definition Names List of Values - Get an ownership definition name assigned to a joi... | Ownership Definition Names List of Values Get an ownership definition name assigned to a joint venture  |
| Party Fiscal Classifications - Get a party fiscal classification | Party Fiscal Classifications Get a party fiscal classification  |
| Party Fiscal Classifications/Tax Regime Associations - Get a tax regime for a party fiscal classification | Party Fiscal Classifications/Tax Regime Associations Get a tax regime for a party fiscal classification  |
| Party Tax Profiles - Get a party tax profile | Party Tax Profiles Get a party tax profile  |
| Payables and Procurement Options - Get payables and procurement options for a busines... | Payables and Procurement Options Get payables and procurement options for a business unit  |
| Payables Calendar Types - Get one calendar type | Payables Calendar Types Get one calendar type  |
| Payables Calendar Types/Payables Calendar Periods - Get calendar periods for one calendar type | Payables Calendar Types/Payables Calendar Periods Get calendar periods for one calendar type  |
| Payables Distribution Sets - Get a distribution set header for a distribution s... | Payables Distribution Sets Get a distribution set header for a distribution set  |
| Payables Distribution Sets/Payables Distribution Set Lines - Get a distribution set line for a distribution set | Payables Distribution Sets/Payables Distribution Set Lines Get a distribution set line for a distribution set  |
| Payables Income Tax Regions - Get an income tax region | Payables Income Tax Regions Get an income tax region  |
| Payables Invoice Holds - Get a hold details | Payables Invoice Holds Get a hold details  |
| Payables Options - Get payables options for a business unit | Payables Options Get payables options for a business unit  |
| Payables Payments - Get a payment | Payables Payments Get a payment  |
| Payables Payments/Payments Descriptive Flexfields - Get descriptive flexfields for a payment | Payables Payments/Payments Descriptive Flexfields Get descriptive flexfields for a payment  |
| Payables Payments/Payments Global Descriptive Flexfields - Get global descriptive flexfields for a payment | Payables Payments/Payments Global Descriptive Flexfields Get global descriptive flexfields for a payment  |
| Payables Payments/Related Invoices - Get a paid invoice for a payment | Payables Payments/Related Invoices Get a paid invoice for a payment  |
| Payables Payments/Related Invoices/Related Invoice Global Descriptive Flexfields - Get global descriptive flexfields for an invoice p... | Payables Payments/Related Invoices/Related Invoice Global Descriptive Flexfields Get global descriptive flexfields for an invoice payment  |
| Payee Bank Accounts List of Values - Get a payee bank account | Payee Bank Accounts List of Values Get a payee bank account  |
| Payment Codes List of Values - Get a payment code | Payment Codes List of Values Get a payment code  |
| Payment Methods List of Values - Get a payment method | Payment Methods List of Values Get a payment method  |
| Payment Process Profiles List of Values - Get a payment process profile | Payment Process Profiles List of Values Get a payment process profile  |
| Payment Process Requests - Get a payment process request | Payment Process Requests Get a payment process request  |
| Payment Terms - Get a payment term header for a terms ID | Payment Terms Get a payment term header for a terms ID  |
| Payment Terms List of Values - Get a payables payment term | Payment Terms List of Values Get a payables payment term  |
| Payment Terms/Payment Terms Lines - Get a payment term line for a terms ID | Payment Terms/Payment Terms Lines Get a payment term line for a terms ID  |
| Payment Terms/Payment Terms Sets - Get all set names associated to the payment terms | Payment Terms/Payment Terms Sets Get all set names associated to the payment terms  |
| Payment Terms/Payment Terms Translations - Get a payment term details for a term ID in a spec... | Payment Terms/Payment Terms Translations Get a payment term details for a term ID in a specific language  |
| Preferred Currencies List of Values - Get a currency | Preferred Currencies List of Values Get a currency  |
| Product Fiscal Classifications List of Values - Get a product fiscal classification | Product Fiscal Classifications List of Values Get a product fiscal classification  |
| Product Types List of Values - Get a product type | Product Types List of Values Get a product type  |
| Purchase to Pay Legal Entities List of Values - Get a purchase to pay legal entity | Purchase to Pay Legal Entities List of Values Get a purchase to pay legal entity  |
| Receipt Method Assignments - Get a receipt method assigned to a customer accoun... | Receipt Method Assignments Get a receipt method assigned to a customer account or site  |
| Receivables Adjustments - Get a receivables adjustment | Receivables Adjustments Get a receivables adjustment  |
| Receivables Adjustments/Receivables Adjustments Descriptive Flexfields - Get a receivables adjustment descriptive flexfield | Receivables Adjustments/Receivables Adjustments Descriptive Flexfields Get a receivables adjustment descriptive flexfield  |
| Receivables Business Units List of Values - Get a business unit | Receivables Business Units List of Values Get a business unit  |
| Receivables Credit Memos - Get a receivables credit memo | Receivables Credit Memos Get a receivables credit memo  |
| Receivables Credit Memos/Receivables Credit Memo Lines - Get a receivables credit memo line | Receivables Credit Memos/Receivables Credit Memo Lines Get a receivables credit memo line  |
| Receivables Customer Account Activities - Get a customer account activity | Receivables Customer Account Activities Get a customer account activity  |
| Receivables Customer Account Activities/Credit Memo Applications - Get a credit memo application | Receivables Customer Account Activities/Credit Memo Applications Get a credit memo application  |
| Receivables Customer Account Activities/Credit Memo Applications/Credit Memo Applications Descriptive Flexfields - Get a credit memo application descriptive flexfiel... | Receivables Customer Account Activities/Credit Memo Applications/Credit Memo Applications Descriptive Flexfields Get a credit memo application descriptive flexfield  |
| Receivables Customer Account Activities/Credit Memos - Get a credit memo | Receivables Customer Account Activities/Credit Memos Get a credit memo  |
| Receivables Customer Account Activities/Standard Receipt Applications - Get a standard receipt application | Receivables Customer Account Activities/Standard Receipt Applications Get a standard receipt application  |
| Receivables Customer Account Activities/Standard Receipt Applications/Credit Memo Applications Descriptive Flexfields - Get a credit memo application descriptive flexfiel... | Receivables Customer Account Activities/Standard Receipt Applications/Credit Memo Applications Descriptive Flexfields Get a credit memo application descriptive flexfield  |
| Receivables Customer Account Activities/Standard Receipts - Get a standard receipt | Receivables Customer Account Activities/Standard Receipts Get a standard receipt  |
| Receivables Customer Account Activities/Transaction Adjustments - Get a transaction adjustment | Receivables Customer Account Activities/Transaction Adjustments Get a transaction adjustment  |
| Receivables Customer Account Activities/Transaction Payment Schedules - Get a transaction payment schedule | Receivables Customer Account Activities/Transaction Payment Schedules Get a transaction payment schedule  |
| Receivables Customer Account Site Activities - Get a customer account activity | Receivables Customer Account Site Activities Get a customer account activity  |
| Receivables Customer Account Site Activities/Credit Memo Applications - Get a credit memo application | Receivables Customer Account Site Activities/Credit Memo Applications Get a credit memo application  |
| Receivables Customer Account Site Activities/Credit Memo Applications/Credit Memo Applications Descriptive Flexfields - Get a credit memo application descriptive flexfiel... | Receivables Customer Account Site Activities/Credit Memo Applications/Credit Memo Applications Descriptive Flexfields Get a credit memo application descriptive flexfield  |
| Receivables Customer Account Site Activities/Credit Memos - Get a credit memo | Receivables Customer Account Site Activities/Credit Memos Get a credit memo  |
| Receivables Customer Account Site Activities/Standard Receipt Applications - Get a standard receipt application | Receivables Customer Account Site Activities/Standard Receipt Applications Get a standard receipt application  |
| Receivables Customer Account Site Activities/Standard Receipt Applications/Credit Memo Applications Descriptive Flexfields - Get a credit memo application descriptive flexfiel... | Receivables Customer Account Site Activities/Standard Receipt Applications/Credit Memo Applications Descriptive Flexfields Get a credit memo application descriptive flexfield  |
| Receivables Customer Account Site Activities/Standard Receipts - Get a standard receipt | Receivables Customer Account Site Activities/Standard Receipts Get a standard receipt  |
| Receivables Customer Account Site Activities/Transaction Adjustments - Get a transaction adjustment | Receivables Customer Account Site Activities/Transaction Adjustments Get a transaction adjustment  |
| Receivables Customer Account Site Activities/Transaction Payment Schedules - Get a transaction payment schedule | Receivables Customer Account Site Activities/Transaction Payment Schedules Get a transaction payment schedule  |
| Receivables Invoices - Get a receivables invoice | Receivables Invoices Get a receivables invoice  |
| Receivables Invoices/Receivables Invoice Attachments - Get a receivables invoice attachment | Receivables Invoices/Receivables Invoice Attachments Get a receivables invoice attachment  |
| Receivables Invoices/Receivables Invoice Descriptive Flexfields - Get a receivables invoice descriptive flexfield | Receivables Invoices/Receivables Invoice Descriptive Flexfields Get a receivables invoice descriptive flexfield  |
| Receivables Invoices/Receivables Invoice Distributions - Get a receivables invoice distribution | Receivables Invoices/Receivables Invoice Distributions Get a receivables invoice distribution  |
| Receivables Invoices/Receivables Invoice Distributions/Receivables Invoice Distribution Descriptive Flexfields - Get a receivables invoice distribution descriptive... | Receivables Invoices/Receivables Invoice Distributions/Receivables Invoice Distribution Descriptive Flexfields Get a receivables invoice distribution descriptive flexfield  |
| Receivables Invoices/Receivables Invoice Global Descriptive Flexfields - Get a receivables invoice global descriptive flexf... | Receivables Invoices/Receivables Invoice Global Descriptive Flexfields Get a receivables invoice global descriptive flexfield  |
| Receivables Invoices/Receivables Invoice Installments - Get a receivables invoice installment | Receivables Invoices/Receivables Invoice Installments Get a receivables invoice installment  |
| Receivables Invoices/Receivables Invoice Installments/Receivables Invoice Installment Global Descriptive Flexfields - Get a receivables invoice installment global descr... | Receivables Invoices/Receivables Invoice Installments/Receivables Invoice Installment Global Descriptive Flexfields Get a receivables invoice installment global descriptive flexfield  |
| Receivables Invoices/Receivables Invoice Installments/Receivables Invoice Installment Notes - Get a receivables invoice installment note | Receivables Invoices/Receivables Invoice Installments/Receivables Invoice Installment Notes Get a receivables invoice installment note  |
| Receivables Invoices/Receivables Invoice Lines - Get a receivables invoice line | Receivables Invoices/Receivables Invoice Lines Get a receivables invoice line  |
| Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Attachments - Get a receivables invoice attachment | Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Attachments Get a receivables invoice attachment  |
| Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Descriptive Flexfields - Get a receivables invoice line descriptive flexfie... | Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Descriptive Flexfields Get a receivables invoice line descriptive flexfield  |
| Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Global Descriptive Flexfields - Get a receivables invoice line global descriptive ... | Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Global Descriptive Flexfields Get a receivables invoice line global descriptive flexfield  |
| Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Tax Lines - Get a receivables invoice line tax line | Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Tax Lines Get a receivables invoice line tax line  |
| Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Transaction Descriptive Flexfields - Get a receivables invoice line transaction descrip... | Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Transaction Descriptive Flexfields Get a receivables invoice line transaction descriptive flexfield  |
| Receivables Invoices/Receivables Invoice Notes - Get a receivables invoice note | Receivables Invoices/Receivables Invoice Notes Get a receivables invoice note  |
| Receivables Invoices/Receivables Invoice Transaction Descriptive Flexfields - Get a receivables invoice transaction descriptive ... | Receivables Invoices/Receivables Invoice Transaction Descriptive Flexfields Get a receivables invoice transaction descriptive flexfield  |
| Receivables Memo Lines List of Values - Get a memo line | Receivables Memo Lines List of Values Get a memo line  |
| Receivables Payment Terms List of Values - Get payment terms | Receivables Payment Terms List of Values Get payment terms  |
| Receivables Transaction Sources List of Values - Get a transaction source | Receivables Transaction Sources List of Values Get a transaction source  |
| Receivables Transaction Types List of Values - Get a transaction type | Receivables Transaction Types List of Values Get a transaction type  |
| Revenue Contracts - Get a revenue contract | Revenue Contracts Get a revenue contract  |
| Revenue Contracts/Performance Obligations - Get a performance obligation | Revenue Contracts/Performance Obligations Get a performance obligation  |
| Revenue Contracts/Performance Obligations/Performance Obligation Series - Get a performance obligation series | Revenue Contracts/Performance Obligations/Performance Obligation Series Get a performance obligation series  |
| Revenue Contracts/Performance Obligations/Promised Details - Get a promised detail | Revenue Contracts/Performance Obligations/Promised Details Get a promised detail  |
| Revenue Contracts/Performance Obligations/Promised Details/Satisfaction Events - Get a satisfaction event | Revenue Contracts/Performance Obligations/Promised Details/Satisfaction Events Get a satisfaction event  |
| Revenue Contracts/Performance Obligations/Promised Details/Source Documents - Get a source document | Revenue Contracts/Performance Obligations/Promised Details/Source Documents Get a source document  |
| Revenue Contracts/Performance Obligations/Promised Details/Source Documents/Source Document Lines - Get a source document line | Revenue Contracts/Performance Obligations/Promised Details/Source Documents/Source Document Lines Get a source document line  |
| Revenue Contracts/Performance Obligations/Promised Details/Source Documents/Source Document Lines/Source Document Sublines - Get a source document subline | Revenue Contracts/Performance Obligations/Promised Details/Source Documents/Source Document Lines/Source Document Sublines Get a source document subline  |
| Salesperson Reference Accounts - Get a salesperson reference account | Salesperson Reference Accounts Get a salesperson reference account  |
| Standard Receipts - Get a standard receipt | Standard Receipts Get a standard receipt  |
| Standard Receipts/Attachments - Get a standard receipt attachment | Standard Receipts/Attachments Get a standard receipt attachment  |
| Standard Receipts/Remittance References - Get a remittance reference | Standard Receipts/Remittance References Get a remittance reference  |
| Standard Receipts/Standard Receipt Descriptive Flexfields - Get a standard receipt descriptive flexfield | Standard Receipts/Standard Receipt Descriptive Flexfields Get a standard receipt descriptive flexfield  |
| Standard Receipts/Standard Receipt Global Descriptive Flexfields - Get a standard receipt global descriptive flexfiel... | Standard Receipts/Standard Receipt Global Descriptive Flexfields Get a standard receipt global descriptive flexfield  |
| Tax Authority Profiles - Get a tax authority profile | Tax Authority Profiles Get a tax authority profile  |
| Tax Classifications - Get a tax classification | Tax Classifications Get a tax classification  |
| Tax Exemptions - Get a tax exemption | Tax Exemptions Get a tax exemption  |
| Tax Intended Uses - Get an intended use fiscal classification record | Tax Intended Uses Get an intended use fiscal classification record  |
| Tax Intended Uses List of Values - Get an intended use fiscal classification | Tax Intended Uses List of Values Get an intended use fiscal classification  |
| Tax Partner Registrations - GET action not supported | Tax Partner Registrations GET action not supported  |
| Tax Product Categories - Get a product category | Tax Product Categories Get a product category  |
| Tax Rates List of Values - Get a tax rate | Tax Rates List of Values Get a tax rate  |
| Tax Regimes List of Values - Get a tax regime | Tax Regimes List of Values Get a tax regime  |
| Tax Registrations - Get a tax registration | Tax Registrations Get a tax registration  |
| Tax Reporting Entities - Get a single tax reporting entity | Tax Reporting Entities Get a single tax reporting entity  |
| Tax Reporting Entities List of Values - Get a tax reporting entity | Tax Reporting Entities List of Values Get a tax reporting entity  |
| Tax Reporting Entities/Tax Reporting Entity Lines - Get tax reporting entity lines for a tax reporting... | Tax Reporting Entities/Tax Reporting Entity Lines Get tax reporting entity lines for a tax reporting entity  |
| Tax Transaction Business Categories - Get a transaction business category | Tax Transaction Business Categories Get a transaction business category  |
| Taxpayer Identifiers - Get a taxpayer identifier | Taxpayer Identifiers Get a taxpayer identifier  |
| Transaction Tax Lines - Get a transaction tax line | Transaction Tax Lines Get a transaction tax line  |
| Withholding Tax Groups List of Values - Get a withholding tax group | Withholding Tax Groups List of Values Get a withholding tax group  |
| Withholding Tax Lines - Get a withholding tax line | Withholding Tax Lines Get a withholding tax line  |
| Workflow Users - Get a workflow user | Workflow Users Get a workflow user  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Account Combinations List of Values - Get all account combinations | Account Combinations List of Values Get all account combinations  |
| Accounting Calendars List of Values - Get all calendars | Accounting Calendars List of Values Get all calendars  |
| Accounting Period Status List of Values - Get all period statuses | Accounting Period Status List of Values Get all period statuses  |
| Accounting Periods List of Values - Get all periods | Accounting Periods List of Values Get all periods  |
| Asset Books List of Values - Get all asset books | Asset Books List of Values Get all asset books  |
| Bank Account User Rules - Get all country-specific rules | Bank Account User Rules Get all country-specific rules  |
| Bank Accounts - Get all bank accounts | Bank Accounts Get all bank accounts  |
| Bank Accounts List of Values - Get all bank accounts | Bank Accounts List of Values Get all bank accounts  |
| Bank Accounts/Bank Account Descriptive Flexfields - Get all descriptive flexfields for a bank account | Bank Accounts/Bank Account Descriptive Flexfields Get all descriptive flexfields for a bank account  |
| Bank Accounts/Bank Account Global Descriptive Flexfields - Get all global descriptive flexfields for a bank a... | Bank Accounts/Bank Account Global Descriptive Flexfields Get all global descriptive flexfields for a bank account  |
| Bank Accounts/Bank Account Grants - Get all grants for a bank account | Bank Accounts/Bank Account Grants Get all grants for a bank account  |
| Bank Accounts/Bank Account Payment Documents - Get all payment documents for a bank account | Bank Accounts/Bank Account Payment Documents Get all payment documents for a bank account  |
| Bank Accounts/Bank Account Payment Documents/Bank Account Checkbooks - Get all checkbooks for a bank account | Bank Accounts/Bank Account Payment Documents/Bank Account Checkbooks Get all checkbooks for a bank account  |
| Bank Accounts/Bank Account Transaction Creation Rules - Get all bank statement transaction creation rules ... | Bank Accounts/Bank Account Transaction Creation Rules Get all bank statement transaction creation rules for a bank account  |
| Bank Accounts/Bank Account Uses - Get all bank account uses for a bank account | Bank Accounts/Bank Account Uses Get all bank account uses for a bank account  |
| Bank Accounts/Bank Account Uses/Bank Account Payment Document Categories - Get all payment document categories for a business... | Bank Accounts/Bank Account Uses/Bank Account Payment Document Categories Get all payment document categories for a business unit  |
| Bank Branches - Get all bank branches | Bank Branches Get all bank branches  |
| Bank Branches List of Values - Get all bank branches | Bank Branches List of Values Get all bank branches  |
| Bank Collection Business Units List of Values - Get all bank collection business units | Bank Collection Business Units List of Values Get all bank collection business units  |
| Banks - Get all banks | Banks Get all banks  |
| Banks List of Values - Get all banks | Banks List of Values Get all banks  |
| Bill Management Users - Get all bill management users | Bill Management Users Get all bill management users  |
| Brazilian Fiscal Documents - Get all fiscal documents | Brazilian Fiscal Documents Get all fiscal documents  |
| Budgetary Control for Enterprise Performance Management Budget Transactions - Get all budgetary control budget entry transaction... | Budgetary Control for Enterprise Performance Management Budget Transactions Get all budgetary control budget entry transaction interface rows  |
| Budgetary Control Results Budget Impacts for Labor Cost Management - Get the budget impacts of all budgetary control re... | Budgetary Control Results Budget Impacts for Labor Cost Management Get the budget impacts of all budgetary control results  |
| Budgetary Control Results for Enterprise Performance Management Budget Transactions - Get all latest budget entry transaction budgetary ... | Budgetary Control Results for Enterprise Performance Management Budget Transactions Get all latest budget entry transaction budgetary control results  |
| Budgetary Control Results for Labor Cost Management - Get all latest budgetary control results | Budgetary Control Results for Labor Cost Management Get all latest budgetary control results  |
| Calendar Types List of Values - Get all payables calendars | Calendar Types List of Values Get all payables calendars  |
| Cash Pools - Get all cash pools | Cash Pools Get all cash pools  |
| Cash Pools/Cash Pool Members - Get all cash pool members | Cash Pools/Cash Pool Members Get all cash pool members  |
| Chart of Accounts List of Values - Get all chart of accounts | Chart of Accounts List of Values Get all chart of accounts  |
| Collection Promises - Get all collection promises | Collection Promises Get all collection promises  |
| Collection Promises/Collection Promises Descriptive Flexfields - Get all descriptive flexfields for a collection pr... | Collection Promises/Collection Promises Descriptive Flexfields Get all descriptive flexfields for a collection promise group  |
| Collections Strategies - Get all strategy assignments | Collections Strategies Get all strategy assignments  |
| Collections Strategies/Strategy Template Tasks - Get all strategy template tasks | Collections Strategies/Strategy Template Tasks Get all strategy template tasks  |
| Collections Strategies/Tasks - Get all task assignments | Collections Strategies/Tasks Get all task assignments  |
| Collections Strategies/User Tasks - Get all user task assignments | Collections Strategies/User Tasks Get all user task assignments  |
| Control Budget Periods List of Values - Get all control budget periods | Control Budget Periods List of Values Get all control budget periods  |
| Control Budgets List of Values - Get all control budgets | Control Budgets List of Values Get all control budgets  |
| Credit and Collections Data Points - Get all credit and collections data points | Credit and Collections Data Points Get all credit and collections data points  |
| Currency Conversion Types List of Values - Get all currency conversion types | Currency Conversion Types List of Values Get all currency conversion types  |
| Currency Rates - Get all currency rates | Currency Rates Get all currency rates  |
| Customer Account Sites List of Values - Get all customer account sites details | Customer Account Sites List of Values Get all customer account sites details  |
| Data Access Set Ledgers List of Values - Get all ledgers or ledger sets | Data Access Set Ledgers List of Values Get all ledgers or ledger sets  |
| Data Access Sets List of Values - Get all data access sets | Data Access Sets List of Values Get all data access sets  |
| Data Point Values - Get all data point values | Data Point Values Get all data point values  |
| Data Security for Users - Get all data assignments by user and role | Data Security for Users Get all data assignments by user and role  |
| Debit Authorizations - Get all debit authorizations | Debit Authorizations Get all debit authorizations  |
| Debit Authorizations/Debit Authorization Versions - Get all debit authorization versions | Debit Authorizations/Debit Authorization Versions Get all debit authorization versions  |
| Distribution Sets List of Values - Get all distribution sets | Distribution Sets List of Values Get all distribution sets  |
| Document Fiscal Classifications List of Values - Get all document fiscal classifications | Document Fiscal Classifications List of Values Get all document fiscal classifications  |
| Document Sequence Derivations - Get all document sequence derivation records | Document Sequence Derivations Get all document sequence derivation records  |
| Early Payment Offers - Get all standing early payment offer | Early Payment Offers Get all standing early payment offer  |
| Early Payment Offers/Early Payment Offer Assignments - Get all standing offer assignments | Early Payment Offers/Early Payment Offer Assignments Get all standing offer assignments  |
| Early Payment Offers/Early Payment Offer Assignments/Early Payment Offers Descriptive Flexfields - Get all descriptive flexfields for a standing offe... | Early Payment Offers/Early Payment Offer Assignments/Early Payment Offers Descriptive Flexfields Get all descriptive flexfields for a standing offer assignment  |
| Encumbrance Types List of Values - Get all journal encumbrance types | Encumbrance Types List of Values Get all journal encumbrance types  |
| ERP Business Events - Get all business event records | ERP Business Events Get all business event records  |
| ERP Integrations - Get all job details | ERP Integrations Get all job details  |
| Expense Accommodations Polices - Get all accommodations expense policy records | Expense Accommodations Polices Get all accommodations expense policy records  |
| Expense Accommodations Polices/Expense Accommodations Policy Lines - Get all accommodations expense policy line records | Expense Accommodations Polices/Expense Accommodations Policy Lines Get all accommodations expense policy line records  |
| Expense Airfare Policies - Get all expense airfare policies | Expense Airfare Policies Get all expense airfare policies  |
| Expense Airfare Policies/Expense Airfare Policy Lines - Get all expense airfare policy lines | Expense Airfare Policies/Expense Airfare Policy Lines Get all expense airfare policy lines  |
| Expense Business Unit Settings - Get all business unit setting records | Expense Business Unit Settings Get all business unit setting records  |
| Expense Cash Advances - Get all cash advance records | Expense Cash Advances Get all cash advance records  |
| Expense Cash Advances/Cash Advance Descriptive Flexfields - Get all descriptive flexfield records for the rela... | Expense Cash Advances/Cash Advance Descriptive Flexfields Get all descriptive flexfield records for the related cash advance  |
| Expense Conversion Rate Options - Get all conversion rate option records | Expense Conversion Rate Options Get all conversion rate option records  |
| Expense Conversion Rate Options/Conversion Rate Tolerances - Get all conversion rate tolerance records | Expense Conversion Rate Options/Conversion Rate Tolerances Get all conversion rate tolerance records  |
| Expense Credit Card Transactions - Get all corporate card transactions | Expense Credit Card Transactions Get all corporate card transactions  |
| Expense Delegations - Get all expense delegations | Expense Delegations Get all expense delegations  |
| Expense Descriptive Flexfield Contexts - Get all descriptive flexfield contexts for expense... | Expense Descriptive Flexfield Contexts Get all descriptive flexfield contexts for expenses and expense reports  |
| Expense Descriptive Flexfield Segments - Get all descriptive flexfield segments for expense... | Expense Descriptive Flexfield Segments Get all descriptive flexfield segments for expenses and expense reports  |
| Expense Distributions - Get all distribution records for the related expen... | Expense Distributions Get all distribution records for the related expense  |
| Expense Distributions/Project Descriptive Flexfields - Get all project-related descriptive flexfield reco... | Expense Distributions/Project Descriptive Flexfields Get all project-related descriptive flexfield records for a specific expense distribution identifier  |
| Expense Locations - Get all expense location records | Expense Locations Get all expense location records  |
| Expense Meals Policies - Get all expense meals policy records | Expense Meals Policies Get all expense meals policy records  |
| Expense Meals Policies/Expense Meals Policy Lines - Get all expense meals policy line records | Expense Meals Policies/Expense Meals Policy Lines Get all expense meals policy line records  |
| Expense Mileage Policies - Get all expense mileage policy line records | Expense Mileage Policies Get all expense mileage policy line records  |
| Expense Mileage Policies/Expense Mileage Policy Lines - Get all expense mileage policy records | Expense Mileage Policies/Expense Mileage Policy Lines Get all expense mileage policy records  |
| Expense Persons - Get all expense person records | Expense Persons Get all expense person records  |
| Expense Preferences - Get all expense preferences records for a person | Expense Preferences Get all expense preferences records for a person  |
| Expense Preferred Types - Get all preferred expense types | Expense Preferred Types Get all preferred expense types  |
| Expense Profile Attributes - Get all records of a person's expense profile attr... | Expense Profile Attributes Get all records of a person's expense profile attributes  |
| Expense Reports - Get all expense report records | Expense Reports Get all expense report records  |
| Expense Reports/Attachments - Get attachment records | Expense Reports/Attachments Get attachment records  |
| Expense Reports/Expense Notes - Get all expense note records | Expense Reports/Expense Notes Get all expense note records  |
| Expense Reports/Expense Processing Details - Get all processing detail records of an expense re... | Expense Reports/Expense Processing Details Get all processing detail records of an expense report  |
| Expense Reports/Expense Report Descriptive Flexfields - Get all expense report header descriptive flexfiel... | Expense Reports/Expense Report Descriptive Flexfields Get all expense report header descriptive flexfield records  |
| Expense Reports/Expense Report Payments - Get all expense report payments | Expense Reports/Expense Report Payments Get all expense report payments  |
| Expense Reports/Expenses - Get all expense records | Expense Reports/Expenses Get all expense records  |
| Expense Reports/Expenses/Attachments - Get attachment records | Expense Reports/Expenses/Attachments Get attachment records  |
| Expense Reports/Expenses/Duplicate Expenses - Get all duplicate expense records | Expense Reports/Expenses/Duplicate Expenses Get all duplicate expense records  |
| Expense Reports/Expenses/Expense Attendees - Get all expense attendee records for an expense id... | Expense Reports/Expenses/Expense Attendees Get all expense attendee records for an expense identifier  |
| Expense Reports/Expenses/Expense Descriptive Flexfields - Get all descriptive flexfield records for the rela... | Expense Reports/Expenses/Expense Descriptive Flexfields Get all descriptive flexfield records for the related expense  |
| Expense Reports/Expenses/Expense Distributions - Get all distribution records for the related expen... | Expense Reports/Expenses/Expense Distributions Get all distribution records for the related expense  |
| Expense Reports/Expenses/Expense Distributions/Projects Descriptive Flexfields - Get all project-related descriptive flexfield reco... | Expense Reports/Expenses/Expense Distributions/Projects Descriptive Flexfields Get all project-related descriptive flexfield records for a specific expense distribution identifier  |
| Expense Reports/Expenses/Expense Errors - Get all expense error records | Expense Reports/Expenses/Expense Errors Get all expense error records  |
| Expense Reports/Expenses/Expense Itemizations - Get all expense itemization records | Expense Reports/Expenses/Expense Itemizations Get all expense itemization records  |
| Expense Reports/Expenses/Matched Expenses - Get all matched expense records | Expense Reports/Expenses/Matched Expenses Get all matched expense records  |
| Expense Scanned Images - GET action for multiple images not supported | Expense Scanned Images GET action for multiple images not supported  |
| Expense System Options - Get all setup option records | Expense System Options Get all setup option records  |
| Expense Templates - Get all expense templates | Expense Templates Get all expense templates  |
| Expense Templates/Expense Types - Get all expense types in an expense template recor... | Expense Templates/Expense Types Get all expense types in an expense template record  |
| Expense Templates/Expense Types/Itemized Expense Types - Get all itemized expense type records | Expense Templates/Expense Types/Itemized Expense Types Get all itemized expense type records  |
| Expense Templates/Expense Types/Preferred Agency Lists - Get all preferred agency list records | Expense Templates/Expense Types/Preferred Agency Lists Get all preferred agency list records  |
| Expense Templates/Expense Types/Preferred Merchant Lists - Get all preferred merchant list records | Expense Templates/Expense Types/Preferred Merchant Lists Get all preferred merchant list records  |
| Expense Templates/Mobile Expense Types - Get all mobile expense type records | Expense Templates/Mobile Expense Types Get all mobile expense type records  |
| Expense Travel Itineraries - Get all travel itineraries | Expense Travel Itineraries Get all travel itineraries  |
| Expense Travel Itineraries/Expense Travel Itinerary Reservations - Get all travel itinerary reservations | Expense Travel Itineraries/Expense Travel Itinerary Reservations Get all travel itinerary reservations  |
| Expense Travel Itineraries/Expense Travel Itinerary Reservations/Expense Travel Itinerary Reservation Items - Get all travel itinerary reservation items | Expense Travel Itineraries/Expense Travel Itinerary Reservations/Expense Travel Itinerary Reservation Items Get all travel itinerary reservation items  |
| Expense Types - Get all expense type records | Expense Types Get all expense type records  |
| Expense Types/Expenses Type Itemize - Get all itemized expense type records | Expense Types/Expenses Type Itemize Get all itemized expense type records  |
| Expense Types/Preferred Agency Lists - Get all preferred agency list records | Expense Types/Preferred Agency Lists Get all preferred agency list records  |
| Expense Types/Preferred Merchant Lists - Get all preferred merchant list records | Expense Types/Preferred Merchant Lists Get all preferred merchant list records  |
| Expenses - Get all expense records | Expenses Get all expense records  |
| Expenses/Attachments - Get all attachment records | Expenses/Attachments Get all attachment records  |
| Expenses/Duplicate Expenses - Get all duplicate expense records | Expenses/Duplicate Expenses Get all duplicate expense records  |
| Expenses/Expense Attendees - Get all expense attendee records for an expense id... | Expenses/Expense Attendees Get all expense attendee records for an expense identifier  |
| Expenses/Expense Descriptive Flexfields - Get all descriptive flexfield records for the rela... | Expenses/Expense Descriptive Flexfields Get all descriptive flexfield records for the related expense  |
| Expenses/Expense Distributions - Get all distribution records for the related expen... | Expenses/Expense Distributions Get all distribution records for the related expense  |
| Expenses/Expense Distributions/Project Descriptive Flexfields - Get all project-related descriptive flexfield reco... | Expenses/Expense Distributions/Project Descriptive Flexfields Get all project-related descriptive flexfield records  |
| Expenses/Expense Errors - Get all expense error records | Expenses/Expense Errors Get all expense error records  |
| Expenses/Expense Itemizations - Get all expense itemization records | Expenses/Expense Itemizations Get all expense itemization records  |
| Expenses/Matched Expenses - Get all matched expense records | Expenses/Matched Expenses Get all matched expense records  |
| External Bank Accounts - Get all external bank accounts | External Bank Accounts Get all external bank accounts  |
| External Bank Accounts/Account Owners - Get all account owners for an external bank accoun... | External Bank Accounts/Account Owners Get all account owners for an external bank account  |
| External Cash Transactions - Get all external transactions | External Cash Transactions Get all external transactions  |
| External Cash Transactions/Attachments - Get all attachments for an external transaction | External Cash Transactions/Attachments Get all attachments for an external transaction  |
| External Cash Transactions/Descriptive Flexfields - Get all descriptive flexfields for an external tra... | External Cash Transactions/Descriptive Flexfields Get all descriptive flexfields for an external transaction  |
| External Payees - Get all external payees | External Payees Get all external payees  |
| External Payees/External Party Payment Methods - Get all payment methods assigned to a payee | External Payees/External Party Payment Methods Get all payment methods assigned to a payee  |
| Federal Account Symbols - Get all federal account symbols | Federal Account Symbols Get all federal account symbols  |
| Federal Account Symbols List of Values - Get all federal account symbols | Federal Account Symbols List of Values Get all federal account symbols  |
| Federal Account Symbols/Federal Account Symbols Descriptive Flexfields - Get all federal account symbols descriptive flexfi... | Federal Account Symbols/Federal Account Symbols Descriptive Flexfields Get all federal account symbols descriptive flexfields  |
| Federal Adjustment Business Event Type Codes List of Values - Get all Adjustment BETC | Federal Adjustment Business Event Type Codes List of Values Get all Adjustment BETC  |
| Federal Agency Location Codes - Get all agency location codes | Federal Agency Location Codes Get all agency location codes  |
| Federal Agency Location Codes/Federal Agency Location Codes Descriptive Flexfields - Get all agency location code descriptive flexfield... | Federal Agency Location Codes/Federal Agency Location Codes Descriptive Flexfields Get all agency location code descriptive flexfields  |
| Federal Business Event Type Codes - Get all business event type codes | Federal Business Event Type Codes Get all business event type codes  |
| Federal Business Event Type Codes List of Values - Get all BETC | Federal Business Event Type Codes List of Values Get all BETC  |
| Federal Business Event Type Codes/Federal Business Event Type Codes Descriptive Flexfields - Get all business event type codes descriptive flex... | Federal Business Event Type Codes/Federal Business Event Type Codes Descriptive Flexfields Get all business event type codes descriptive flexfields  |
| Federal Fund Attributes - Get all fund attributes | Federal Fund Attributes Get all fund attributes  |
| Federal Fund Attributes/Federal Fund Attributes Descriptive Flexfields - Get all fund attributes descriptive flexfields | Federal Fund Attributes/Federal Fund Attributes Descriptive Flexfields Get all fund attributes descriptive flexfields  |
| Federal Partial Segments List of Values - Get all partial segments | Federal Partial Segments List of Values Get all partial segments  |
| Federal Payment Format Mappings - Get all payment format mappings | Federal Payment Format Mappings Get all payment format mappings  |
| Federal Payment Format Mappings/Federal Payment Format Mappings Descriptive Flexfields - Get all descriptive flexfields for a payment forma... | Federal Payment Format Mappings/Federal Payment Format Mappings Descriptive Flexfields Get all descriptive flexfields for a payment format mapping  |
| Federal Setup Options - Get all setup options | Federal Setup Options Get all setup options  |
| Federal Setup Options/Federal Setup Options Descriptive Flexfields - Get all setup options descriptive flexfields | Federal Setup Options/Federal Setup Options Descriptive Flexfields Get all setup options descriptive flexfields  |
| Federal Treasury Account Symbol Business Event Type Codes List of Values - Get all treasury account symbols business event ty... | Federal Treasury Account Symbol Business Event Type Codes List of Values Get all treasury account symbols business event type codes  |
| Federal Treasury Account Symbols - Get all treasury account symbols | Federal Treasury Account Symbols Get all treasury account symbols  |
| Federal Treasury Account Symbols List of Values - Get all treasury account symbols | Federal Treasury Account Symbols List of Values Get all treasury account symbols  |
| Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes - Get all treasury account symbols business event ty... | Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes Get all treasury account symbols business event type codes  |
| Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes/Federal Treasury Account Symbols Business Event Type Codes Descriptive Flexfields - Get all TAS BETC descriptive flexfields | Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes/Federal Treasury Account Symbols Business Event Type Codes Descriptive Flexfields Get all TAS BETC descriptive flexfields  |
| Federal Treasury Account Symbols/Federal Treasury Account Symbols Descriptive Flexfields - Get all treasury account symbols descriptive flexf... | Federal Treasury Account Symbols/Federal Treasury Account Symbols Descriptive Flexfields Get all treasury account symbols descriptive flexfields  |
| Federal USSGL Accounts - Get all USSGL accounts | Federal USSGL Accounts Get all USSGL accounts  |
| Federal USSGL Accounts/Federal USSGL Accounts Descriptive Flexfields - Get all federal USSGL account descriptive flexfiel... | Federal USSGL Accounts/Federal USSGL Accounts Descriptive Flexfields Get all federal USSGL account descriptive flexfields  |
| Financials Business Units List of Values - Get all business units | Financials Business Units List of Values Get all business units  |
| Fiscal Document Business Units List of Values - Get all fiscal document business units | Fiscal Document Business Units List of Values Get all fiscal document business units  |
| Income Tax Regions List of Values - Get all income tax regions | Income Tax Regions List of Values Get all income tax regions  |
| Income Tax Types - Get all income tax types | Income Tax Types Get all income tax types  |
| Income Tax Types List of Values - Get all income tax types | Income Tax Types List of Values Get all income tax types  |
| Input Tax Classifications List of Values - Get all input tax classifications | Input Tax Classifications List of Values Get all input tax classifications  |
| Instrument Assignments - Get all payment instrument assignments | Instrument Assignments Get all payment instrument assignments  |
| Intercompany Organizations List of Values - Get all intercompany organizations | Intercompany Organizations List of Values Get all intercompany organizations  |
| Intercompany Transaction Types List of Values - Get all intercompany transaction types | Intercompany Transaction Types List of Values Get all intercompany transaction types  |
| Interim Payables Documents - Get all interim payables documents | Interim Payables Documents Get all interim payables documents  |
| Invoice Approvals and Notifications History - Get all invoice approvals and notifications histor... | Invoice Approvals and Notifications History Get all invoice approvals and notifications history  |
| Invoice Holds - Get all holds | Invoice Holds Get all holds  |
| Invoice Holds List of Values - Get all invoice holds | Invoice Holds List of Values Get all invoice holds  |
| Invoice Holds/Invoice Holds Descriptive Flexfields - Get descriptive flexfields for an invoice hold | Invoice Holds/Invoice Holds Descriptive Flexfields Get descriptive flexfields for an invoice hold  |
| Invoice Tolerances - Get all payables invoice tolerances | Invoice Tolerances Get all payables invoice tolerances  |
| Invoice Tolerances List of Values - Get all invoice tolerances | Invoice Tolerances List of Values Get all invoice tolerances  |
| Invoices - Get all invoices | Invoices Get all invoices  |
| Invoices/Applied Prepayments - Get all applied prepayments | Invoices/Applied Prepayments Get all applied prepayments  |
| Invoices/Attachments - Get all attachments for an invoice | Invoices/Attachments Get all attachments for an invoice  |
| Invoices/Available Prepayments - Get all available prepayments | Invoices/Available Prepayments Get all available prepayments  |
| Invoices/Invoice Installments - Get all installments for an invoice | Invoices/Invoice Installments Get all installments for an invoice  |
| Invoices/Invoice Installments/Invoice Installments Descriptive Flexfields - Get all descriptive flexfields for an invoice inst... | Invoices/Invoice Installments/Invoice Installments Descriptive Flexfields Get all descriptive flexfields for an invoice installment  |
| Invoices/Invoice Installments/Invoice Installments Global Descriptive Flexfields - Get all global descriptive flexfields for an invoi... | Invoices/Invoice Installments/Invoice Installments Global Descriptive Flexfields Get all global descriptive flexfields for an invoice installment  |
| Invoices/Invoice Lines - Get all lines for an invoice | Invoices/Invoice Lines Get all lines for an invoice  |
| Invoices/Invoice Lines/Invoice Distributions - Get all distributions for an invoice | Invoices/Invoice Lines/Invoice Distributions Get all distributions for an invoice  |
| Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Descriptive Flexfields - Get all descriptive flexfields for an invoice dist... | Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Descriptive Flexfields Get all descriptive flexfields for an invoice distribution  |
| Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Global Descriptive Flexfields - Get all global descriptive flexfields for an invoi... | Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Global Descriptive Flexfields Get all global descriptive flexfields for an invoice distribution  |
| Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Project Descriptive Flexfields - Get all project-related descriptive flexfields for... | Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Project Descriptive Flexfields Get all project-related descriptive flexfields for an invoice distribution  |
| Invoices/Invoice Lines/Invoice Lines Descriptive Flexfields - Get all descriptive flexfields for an invoice line | Invoices/Invoice Lines/Invoice Lines Descriptive Flexfields Get all descriptive flexfields for an invoice line  |
| Invoices/Invoice Lines/Invoice Lines Global Descriptive Flexfields - Get all global descriptive flexfields for an invoi... | Invoices/Invoice Lines/Invoice Lines Global Descriptive Flexfields Get all global descriptive flexfields for an invoice line  |
| Invoices/Invoice Lines/Invoice Lines Project Descriptive Flexfields - Get all project-related descriptive flexfields for... | Invoices/Invoice Lines/Invoice Lines Project Descriptive Flexfields Get all project-related descriptive flexfields for an invoice line  |
| Invoices/Invoices Descriptive Flexfields - Get all descriptive flexfields for an invoice | Invoices/Invoices Descriptive Flexfields Get all descriptive flexfields for an invoice  |
| Invoices/Invoices Global Descriptive Flexfields - Get all global descriptive flexfields for an invoi... | Invoices/Invoices Global Descriptive Flexfields Get all global descriptive flexfields for an invoice  |
| Joint Venture Approval Counts - Get all counts for joint ventures with any approve... | Joint Venture Approval Counts Get all counts for joint ventures with any approved dates  |
| Joint Venture General Ledger Distribution Totals - Get all distribution totals for a joint venture | Joint Venture General Ledger Distribution Totals Get all distribution totals for a joint venture  |
| Joint Venture General Ledger Distributions - Get all joint venture general ledger distributions | Joint Venture General Ledger Distributions Get all joint venture general ledger distributions  |
| Joint Venture General Ledger Distributions/Account Key Flexfields - Get all account key flexfields for a journal line ... | Joint Venture General Ledger Distributions/Account Key Flexfields Get all account key flexfields for a journal line associated with a joint venture transaction  |
| Joint Venture General Ledger Distributions/Offset Account Key Flexfields - Get all account key flexfields for a journal line ... | Joint Venture General Ledger Distributions/Offset Account Key Flexfields Get all account key flexfields for a journal line associated with a joint venture transaction  |
| Joint Venture General Ledger Transaction Totals - Get all transaction totals for a joint venture | Joint Venture General Ledger Transaction Totals Get all transaction totals for a joint venture  |
| Joint Venture General Ledger Transactions - Get all joint venture general ledger transactions | Joint Venture General Ledger Transactions Get all joint venture general ledger transactions  |
| Joint Venture General Ledger Transactions/Account Key Flexfields - Get all account key flexfields for a journal line ... | Joint Venture General Ledger Transactions/Account Key Flexfields Get all account key flexfields for a journal line associated with a joint venture transaction  |
| Joint Venture General Ledger Transactions/Joint Venture Transaction Descriptive Flexfields - Get all descriptive flexfields for a joint venture... | Joint Venture General Ledger Transactions/Joint Venture Transaction Descriptive Flexfields Get all descriptive flexfields for a joint venture transaction  |
| Joint Venture Invoicing Partners - Get all joint venture invoicing partners | Joint Venture Invoicing Partners Get all joint venture invoicing partners  |
| Joint Venture Invoicing Partners List of Values - Get all joint venture invoicing partners | Joint Venture Invoicing Partners List of Values Get all joint venture invoicing partners  |
| Joint Venture Invoicing Partners/Invoicing Partner Descriptive Flexfields - Get all descriptive flexfields for a joint venture | Joint Venture Invoicing Partners/Invoicing Partner Descriptive Flexfields Get all descriptive flexfields for a joint venture  |
| Joint Venture Options - Get joint venture options | Joint Venture Options Get joint venture options  |
| Joint Venture Options/Joint Venture Options Descriptive Flexfields - Get all descriptive flexfields for joint venture o... | Joint Venture Options/Joint Venture Options Descriptive Flexfields Get all descriptive flexfields for joint venture options  |
| Joint Venture Partner Contributions - Get all joint venture partner contributions | Joint Venture Partner Contributions Get all joint venture partner contributions  |
| Joint Venture Partner Contributions/Partner Contribution Descriptive Flexfields - Get all descriptive flexfields for a partner contr... | Joint Venture Partner Contributions/Partner Contribution Descriptive Flexfields Get all descriptive flexfields for a partner contribution  |
| Joint Venture Partner Contributions/Partner Contribution References - Get all transaction references for a joint venture... | Joint Venture Partner Contributions/Partner Contribution References Get all transaction references for a joint venture partner contribution  |
| Joint Venture SLA Date References - Get all supporting references that derive a date v... | Joint Venture SLA Date References Get all supporting references that derive a date value  |
| Joint Venture SLA Supporting References List of Values - Get all supporting references | Joint Venture SLA Supporting References List of Values Get all supporting references  |
| Joint Venture Stakeholders List of Values - Get all stakeholders associated with a joint ventu... | Joint Venture Stakeholders List of Values Get all stakeholders associated with a joint venture  |
| Joint Venture Status Counts - Get all counts for joint ventures at every status | Joint Venture Status Counts Get all counts for joint ventures at every status  |
| Joint Venture Subledger Accounting Distribution Totals - Get all distribution totals for a joint venture | Joint Venture Subledger Accounting Distribution Totals Get all distribution totals for a joint venture  |
| Joint Venture Subledger Accounting Transaction Totals - Get all transaction totals for a joint venture | Joint Venture Subledger Accounting Transaction Totals Get all transaction totals for a joint venture  |
| Joint Venture Subledger Distributions - Get all joint venture subledger distributions | Joint Venture Subledger Distributions Get all joint venture subledger distributions  |
| Joint Venture Subledger Distributions/Account Key Flexfields - Get all account key flexfields for a journal line ... | Joint Venture Subledger Distributions/Account Key Flexfields Get all account key flexfields for a journal line associated with a joint venture transaction  |
| Joint Venture Subledger Distributions/Offset Account Key Flexfields - Get all account key flexfields for a journal line ... | Joint Venture Subledger Distributions/Offset Account Key Flexfields Get all account key flexfields for a journal line associated with a joint venture transaction  |
| Joint Venture Subledger Transactions - Get all joint venture subledger transactions | Joint Venture Subledger Transactions Get all joint venture subledger transactions  |
| Joint Venture Subledger Transactions/Account Key Flexfields - Get all account key flexfields for a journal line ... | Joint Venture Subledger Transactions/Account Key Flexfields Get all account key flexfields for a journal line associated with a joint venture transaction  |
| Joint Venture Subledger Transactions/Joint Venture Transaction Descriptive Flexfields - Get all descriptive flexfields for a joint venture... | Joint Venture Subledger Transactions/Joint Venture Transaction Descriptive Flexfields Get all descriptive flexfields for a joint venture transaction  |
| Joint Ventures - Get all joint ventures | Joint Ventures Get all joint ventures  |
| Joint Ventures/Cost Centers - Get all cost centers associated with the joint ven... | Joint Ventures/Cost Centers Get all cost centers associated with the joint venture  |
| Joint Ventures/Cost Centers/Cost Center Descriptive Flexfields - Get all descriptive flexfields for a cost center a... | Joint Ventures/Cost Centers/Cost Center Descriptive Flexfields Get all descriptive flexfields for a cost center associated with a joint venture  |
| Joint Ventures/Cost Centers/Distributable Value Sets - Get all distributable value sets associated with a... | Joint Ventures/Cost Centers/Distributable Value Sets Get all distributable value sets associated with a joint venture cost center  |
| Joint Ventures/Distributable Project Rules - Get all distributable project rules associated wit... | Joint Ventures/Distributable Project Rules Get all distributable project rules associated with a joint venture  |
| Joint Ventures/Joint Venture Descriptive Flexfieds - Get all descriptive flexfields for a joint venture | Joint Ventures/Joint Venture Descriptive Flexfieds Get all descriptive flexfields for a joint venture  |
| Joint Ventures/Joint Venture Messages - Get all joint venture messages | Joint Ventures/Joint Venture Messages Get all joint venture messages  |
| Joint Ventures/Ownership Definitions - Get all ownership definitions associated with a jo... | Joint Ventures/Ownership Definitions Get all ownership definitions associated with a joint venture  |
| Joint Ventures/Ownership Definitions/Ownership Definition Descriptive Flexfields - Get all descriptive flexfields for an ownership de... | Joint Ventures/Ownership Definitions/Ownership Definition Descriptive Flexfields Get all descriptive flexfields for an ownership definition associated with a joint venture  |
| Joint Ventures/Ownership Definitions/Ownership Percentages - Get all ownership percentages associated with an o... | Joint Ventures/Ownership Definitions/Ownership Percentages Get all ownership percentages associated with an ownership definition  |
| Joint Ventures/Ownership Definitions/Ownership Percentages/Ownership Percentage Descriptive Flexfields - Get all descriptive flexfields for an ownership pe... | Joint Ventures/Ownership Definitions/Ownership Percentages/Ownership Percentage Descriptive Flexfields Get all descriptive flexfields for an ownership percentage associated with an ownership definition  |
| Joint Ventures/Stakeholders - Get all joint venture stakeholders | Joint Ventures/Stakeholders Get all joint venture stakeholders  |
| Joint Ventures/Stakeholders/Stakeholder Descriptive Flexfields - Get all descriptive flexfields for a joint venture... | Joint Ventures/Stakeholders/Stakeholder Descriptive Flexfields Get all descriptive flexfields for a joint venture stakeholder  |
| Journal Batches - Get all batches | Journal Batches Get all batches  |
| Journal Batches/Action Logs - Get all log entries | Journal Batches/Action Logs Get all log entries  |
| Journal Batches/Attachments - Get all attachments | Journal Batches/Attachments Get all attachments  |
| Journal Batches/Descriptive Flexfields - Get a descriptive flexfield | Journal Batches/Descriptive Flexfields Get a descriptive flexfield  |
| Journal Batches/Errors - Get all errors | Journal Batches/Errors Get all errors  |
| Journal Batches/Journal Headers - Get all journals | Journal Batches/Journal Headers Get all journals  |
| Journal Batches/Journal Headers/Attachments - Get all attachments | Journal Batches/Journal Headers/Attachments Get all attachments  |
| Journal Batches/Journal Headers/Descriptive Flexfields - Get a descriptive flexfield | Journal Batches/Journal Headers/Descriptive Flexfields Get a descriptive flexfield  |
| Journal Batches/Journal Headers/Global Descriptive Flexfields - Get a global descriptive flexfield | Journal Batches/Journal Headers/Global Descriptive Flexfields Get a global descriptive flexfield  |
| Journal Batches/Journal Headers/Journal Lines - Get all lines | Journal Batches/Journal Headers/Journal Lines Get all lines  |
| Journal Batches/Journal Headers/Journal Lines/Captured Information Descriptive Flexfields - Get captured information descriptive flexfield | Journal Batches/Journal Headers/Journal Lines/Captured Information Descriptive Flexfields Get captured information descriptive flexfield  |
| Journal Batches/Journal Headers/Journal Lines/Descriptive Flexfields - Get a descriptive flexfield | Journal Batches/Journal Headers/Journal Lines/Descriptive Flexfields Get a descriptive flexfield  |
| Journal Batches/Journal Headers/Journal Lines/Global Descriptive Flexfields - Get a global descriptive flexfield | Journal Batches/Journal Headers/Journal Lines/Global Descriptive Flexfields Get a global descriptive flexfield  |
| Journal Categories List of Values - Get all journal categories | Journal Categories List of Values Get all journal categories  |
| Journal Sources List of Values - Get all journal sources | Journal Sources List of Values Get all journal sources  |
| Ledger Balances - Get all account balances | Ledger Balances Get all account balances  |
| Ledgers List of Values - Get all ledgers | Ledgers List of Values Get all ledgers  |
| Legal Entities List of Values - Get all legal entities | Legal Entities List of Values Get all legal entities  |
| Legal Jurisdictions List of Values - Get all legal jurisdictions | Legal Jurisdictions List of Values Get all legal jurisdictions  |
| Legal Reporting Units List of Values - Get all legal reporting units | Legal Reporting Units List of Values Get all legal reporting units  |
| Netting Legal Entities List of Values - Get all netting legal entities | Netting Legal Entities List of Values Get all netting legal entities  |
| Output Tax Classifications List of Values - Get all output tax classifications | Output Tax Classifications List of Values Get all output tax classifications  |
| Ownership Definition Names List of Values - Get all ownership definition names assigned to a j... | Ownership Definition Names List of Values Get all ownership definition names assigned to a joint venture  |
| Party Fiscal Classifications - Get all party fiscal classifications | Party Fiscal Classifications Get all party fiscal classifications  |
| Party Fiscal Classifications/Tax Regime Associations - Get all tax regimes for a party fiscal classificat... | Party Fiscal Classifications/Tax Regime Associations Get all tax regimes for a party fiscal classification  |
| Party Tax Profiles - Get all party tax profiles | Party Tax Profiles Get all party tax profiles  |
| Payables and Procurement Options - Get payables and procurement options for all busin... | Payables and Procurement Options Get payables and procurement options for all business units  |
| Payables Calendar Types - Get all calendar types | Payables Calendar Types Get all calendar types  |
| Payables Calendar Types/Payables Calendar Periods - Get all calendar periods for one calendar type | Payables Calendar Types/Payables Calendar Periods Get all calendar periods for one calendar type  |
| Payables Distribution Sets - Get all distribution sets | Payables Distribution Sets Get all distribution sets  |
| Payables Distribution Sets/Payables Distribution Set Lines - Get all distribution set lines for a distribution ... | Payables Distribution Sets/Payables Distribution Set Lines Get all distribution set lines for a distribution set  |
| Payables Income Tax Regions - Get all income tax regions | Payables Income Tax Regions Get all income tax regions  |
| Payables Invoice Holds - Get all holds details | Payables Invoice Holds Get all holds details  |
| Payables Options - Get payables options for all business units | Payables Options Get payables options for all business units  |
| Payables Payments - Get all payments | Payables Payments Get all payments  |
| Payables Payments/Payments Descriptive Flexfields - Get all descriptive flexfields for a payment | Payables Payments/Payments Descriptive Flexfields Get all descriptive flexfields for a payment  |
| Payables Payments/Payments Global Descriptive Flexfields - Get all global descriptive flexfields for a paymen... | Payables Payments/Payments Global Descriptive Flexfields Get all global descriptive flexfields for a payment  |
| Payables Payments/Related Invoices - Get all paid invoices for a payment | Payables Payments/Related Invoices Get all paid invoices for a payment  |
| Payables Payments/Related Invoices/Related Invoice Global Descriptive Flexfields - Get all global descriptive flexfields for an invoi... | Payables Payments/Related Invoices/Related Invoice Global Descriptive Flexfields Get all global descriptive flexfields for an invoice payment  |
| Payee Bank Accounts List of Values - Get all payee bank accounts | Payee Bank Accounts List of Values Get all payee bank accounts  |
| Payment Codes List of Values - Get all payment codes | Payment Codes List of Values Get all payment codes  |
| Payment Methods List of Values - Get all payment methods | Payment Methods List of Values Get all payment methods  |
| Payment Process Profiles List of Values - Get all payment process profiles | Payment Process Profiles List of Values Get all payment process profiles  |
| Payment Process Requests - Get all payment process requests | Payment Process Requests Get all payment process requests  |
| Payment Terms - Get all payment terms headers | Payment Terms Get all payment terms headers  |
| Payment Terms List of Values - Get all payables payment terms | Payment Terms List of Values Get all payables payment terms  |
| Payment Terms/Payment Terms Lines - Get all payment terms lines for a terms ID | Payment Terms/Payment Terms Lines Get all payment terms lines for a terms ID  |
| Payment Terms/Payment Terms Sets - Get a set name for the payment term | Payment Terms/Payment Terms Sets Get a set name for the payment term  |
| Payment Terms/Payment Terms Translations - Get all payment terms details for a term ID in all... | Payment Terms/Payment Terms Translations Get all payment terms details for a term ID in all supported languages  |
| Preferred Currencies List of Values - Get all currencies | Preferred Currencies List of Values Get all currencies  |
| Product Fiscal Classifications List of Values - Get all product fiscal classifications | Product Fiscal Classifications List of Values Get all product fiscal classifications  |
| Product Types List of Values - Get all product types | Product Types List of Values Get all product types  |
| Purchase to Pay Legal Entities List of Values - GET action not supported | Purchase to Pay Legal Entities List of Values GET action not supported  |
| Receipt Method Assignments - Get all receipt methods assigned to a customer acc... | Receipt Method Assignments Get all receipt methods assigned to a customer account or site  |
| Receivables Adjustments - Get all receivables adjustments | Receivables Adjustments Get all receivables adjustments  |
| Receivables Adjustments/Receivables Adjustments Descriptive Flexfields - Get all receivables adjustment descriptive flexfie... | Receivables Adjustments/Receivables Adjustments Descriptive Flexfields Get all receivables adjustment descriptive flexfield  |
| Receivables Business Units List of Values - Get all business units | Receivables Business Units List of Values Get all business units  |
| Receivables Credit Memos - Get all receivables credit memos | Receivables Credit Memos Get all receivables credit memos  |
| Receivables Credit Memos/Receivables Credit Memo Lines - Get all receivables credit memo lines | Receivables Credit Memos/Receivables Credit Memo Lines Get all receivables credit memo lines  |
| Receivables Customer Account Activities - Get all customer account activities | Receivables Customer Account Activities Get all customer account activities  |
| Receivables Customer Account Activities/Credit Memo Applications - Get all credit memo applications | Receivables Customer Account Activities/Credit Memo Applications Get all credit memo applications  |
| Receivables Customer Account Activities/Credit Memo Applications/Credit Memo Applications Descriptive Flexfields - Get all credit memo applications descriptive flexf... | Receivables Customer Account Activities/Credit Memo Applications/Credit Memo Applications Descriptive Flexfields Get all credit memo applications descriptive flexfields  |
| Receivables Customer Account Activities/Credit Memos - Get all credit memos | Receivables Customer Account Activities/Credit Memos Get all credit memos  |
| Receivables Customer Account Activities/Standard Receipt Applications - Get all standard receipt applications | Receivables Customer Account Activities/Standard Receipt Applications Get all standard receipt applications  |
| Receivables Customer Account Activities/Standard Receipt Applications/Credit Memo Applications Descriptive Flexfields - Get all credit memo applications descriptive flexf... | Receivables Customer Account Activities/Standard Receipt Applications/Credit Memo Applications Descriptive Flexfields Get all credit memo applications descriptive flexfields  |
| Receivables Customer Account Activities/Standard Receipts - Get all standard receipts | Receivables Customer Account Activities/Standard Receipts Get all standard receipts  |
| Receivables Customer Account Activities/Transaction Adjustments - Get all transaction adjustments | Receivables Customer Account Activities/Transaction Adjustments Get all transaction adjustments  |
| Receivables Customer Account Activities/Transaction Payment Schedules - Get all transaction payment schedules | Receivables Customer Account Activities/Transaction Payment Schedules Get all transaction payment schedules  |
| Receivables Customer Account Site Activities - Get all customer account activities | Receivables Customer Account Site Activities Get all customer account activities  |
| Receivables Customer Account Site Activities/Credit Memo Applications - Get all credit memo applications | Receivables Customer Account Site Activities/Credit Memo Applications Get all credit memo applications  |
| Receivables Customer Account Site Activities/Credit Memo Applications/Credit Memo Applications Descriptive Flexfields - Get all credit memo applications descriptive flexf... | Receivables Customer Account Site Activities/Credit Memo Applications/Credit Memo Applications Descriptive Flexfields Get all credit memo applications descriptive flexfields  |
| Receivables Customer Account Site Activities/Credit Memos - Get all credit memos | Receivables Customer Account Site Activities/Credit Memos Get all credit memos  |
| Receivables Customer Account Site Activities/Standard Receipt Applications - Get all standard receipt applications | Receivables Customer Account Site Activities/Standard Receipt Applications Get all standard receipt applications  |
| Receivables Customer Account Site Activities/Standard Receipt Applications/Credit Memo Applications Descriptive Flexfields - Get all credit memo applications descriptive flexf... | Receivables Customer Account Site Activities/Standard Receipt Applications/Credit Memo Applications Descriptive Flexfields Get all credit memo applications descriptive flexfields  |
| Receivables Customer Account Site Activities/Standard Receipts - Get all standard receipts | Receivables Customer Account Site Activities/Standard Receipts Get all standard receipts  |
| Receivables Customer Account Site Activities/Transaction Adjustments - Get all transaction adjustments | Receivables Customer Account Site Activities/Transaction Adjustments Get all transaction adjustments  |
| Receivables Customer Account Site Activities/Transaction Payment Schedules - Get all transaction payment schedules | Receivables Customer Account Site Activities/Transaction Payment Schedules Get all transaction payment schedules  |
| Receivables Invoices - Get all receivables invoices | Receivables Invoices Get all receivables invoices  |
| Receivables Invoices/Receivables Invoice Attachments - Get all attachments for a group of receivables inv... | Receivables Invoices/Receivables Invoice Attachments Get all attachments for a group of receivables invoices  |
| Receivables Invoices/Receivables Invoice Descriptive Flexfields - Get all descriptive flexfields for a group of rece... | Receivables Invoices/Receivables Invoice Descriptive Flexfields Get all descriptive flexfields for a group of receivables invoices  |
| Receivables Invoices/Receivables Invoice Distributions - Get all invoice distributions for a receivables in... | Receivables Invoices/Receivables Invoice Distributions Get all invoice distributions for a receivables invoice  |
| Receivables Invoices/Receivables Invoice Distributions/Receivables Invoice Distribution Descriptive Flexfields - Get all descriptive flexfields for a group of rece... | Receivables Invoices/Receivables Invoice Distributions/Receivables Invoice Distribution Descriptive Flexfields Get all descriptive flexfields for a group of receivables invoice distributions  |
| Receivables Invoices/Receivables Invoice Global Descriptive Flexfields - Get all global descriptive flexfields for a group ... | Receivables Invoices/Receivables Invoice Global Descriptive Flexfields Get all global descriptive flexfields for a group of receivables invoices  |
| Receivables Invoices/Receivables Invoice Installments - Get all installments for a group of receivables in... | Receivables Invoices/Receivables Invoice Installments Get all installments for a group of receivables invoices  |
| Receivables Invoices/Receivables Invoice Installments/Receivables Invoice Installment Global Descriptive Flexfields - Get all global descriptive flexfields for a group ... | Receivables Invoices/Receivables Invoice Installments/Receivables Invoice Installment Global Descriptive Flexfields Get all global descriptive flexfields for a group of receivables invoice installments  |
| Receivables Invoices/Receivables Invoice Installments/Receivables Invoice Installment Notes - Get all notes for a group of receivables invoice i... | Receivables Invoices/Receivables Invoice Installments/Receivables Invoice Installment Notes Get all notes for a group of receivables invoice installments  |
| Receivables Invoices/Receivables Invoice Lines - Get all receivables invoice lines | Receivables Invoices/Receivables Invoice Lines Get all receivables invoice lines  |
| Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Attachments - Get all attachments for a group of receivables inv... | Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Attachments Get all attachments for a group of receivables invoices  |
| Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Descriptive Flexfields - Get all descriptive flexfields for a group of rece... | Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Descriptive Flexfields Get all descriptive flexfields for a group of receivables invoice lines  |
| Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Global Descriptive Flexfields - Get all global descriptive flexfields for a group ... | Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Global Descriptive Flexfields Get all global descriptive flexfields for a group of receivables invoice lines  |
| Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Tax Lines - Get all receivables invoice line tax lines | Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Tax Lines Get all receivables invoice line tax lines  |
| Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Transaction Descriptive Flexfields - Get all transaction descriptive flexfields for a g... | Receivables Invoices/Receivables Invoice Lines/Receivables Invoice Line Transaction Descriptive Flexfields Get all transaction descriptive flexfields for a group of receivables invoice lines  |
| Receivables Invoices/Receivables Invoice Notes - Get all notes for a group of receivables invoices | Receivables Invoices/Receivables Invoice Notes Get all notes for a group of receivables invoices  |
| Receivables Invoices/Receivables Invoice Transaction Descriptive Flexfields - Get all transaction descriptive flexfields for a g... | Receivables Invoices/Receivables Invoice Transaction Descriptive Flexfields Get all transaction descriptive flexfields for a group of receivables invoices  |
| Receivables Memo Lines List of Values - Get all memo lines | Receivables Memo Lines List of Values Get all memo lines  |
| Receivables Payment Terms List of Values - Get all payment terms | Receivables Payment Terms List of Values Get all payment terms  |
| Receivables Transaction Sources List of Values - Get all transaction sources | Receivables Transaction Sources List of Values Get all transaction sources  |
| Receivables Transaction Types List of Values - Get all transaction types | Receivables Transaction Types List of Values Get all transaction types  |
| Revenue Contracts - Get all revenue contracts | Revenue Contracts Get all revenue contracts  |
| Revenue Contracts/Performance Obligations - Get all performance obligations | Revenue Contracts/Performance Obligations Get all performance obligations  |
| Revenue Contracts/Performance Obligations/Performance Obligation Series - Get all performance obligation series | Revenue Contracts/Performance Obligations/Performance Obligation Series Get all performance obligation series  |
| Revenue Contracts/Performance Obligations/Promised Details - Get all promised details | Revenue Contracts/Performance Obligations/Promised Details Get all promised details  |
| Revenue Contracts/Performance Obligations/Promised Details/Satisfaction Events - Get all satisfaction events | Revenue Contracts/Performance Obligations/Promised Details/Satisfaction Events Get all satisfaction events  |
| Revenue Contracts/Performance Obligations/Promised Details/Source Documents - Get all source documents | Revenue Contracts/Performance Obligations/Promised Details/Source Documents Get all source documents  |
| Revenue Contracts/Performance Obligations/Promised Details/Source Documents/Source Document Lines - Get all source document lines | Revenue Contracts/Performance Obligations/Promised Details/Source Documents/Source Document Lines Get all source document lines  |
| Revenue Contracts/Performance Obligations/Promised Details/Source Documents/Source Document Lines/Source Document Sublines - Get all source document sublines | Revenue Contracts/Performance Obligations/Promised Details/Source Documents/Source Document Lines/Source Document Sublines Get all source document sublines  |
| Salesperson Reference Accounts - Get all salesperson reference accounts | Salesperson Reference Accounts Get all salesperson reference accounts  |
| Standard Receipts - Get all standard receipts | Standard Receipts Get all standard receipts  |
| Standard Receipts/Attachments - Get all standard receipt attachments | Standard Receipts/Attachments Get all standard receipt attachments  |
| Standard Receipts/Remittance References - Get all remittance references | Standard Receipts/Remittance References Get all remittance references  |
| Standard Receipts/Standard Receipt Descriptive Flexfields - Get all standard receipt descriptive flexfields | Standard Receipts/Standard Receipt Descriptive Flexfields Get all standard receipt descriptive flexfields  |
| Standard Receipts/Standard Receipt Global Descriptive Flexfields - Get all standard receipt global descriptive flexfi... | Standard Receipts/Standard Receipt Global Descriptive Flexfields Get all standard receipt global descriptive flexfields  |
| Tax Authority Profiles - Get all tax authority profiles | Tax Authority Profiles Get all tax authority profiles  |
| Tax Classifications - Get all tax classifications | Tax Classifications Get all tax classifications  |
| Tax Exemptions - Get all tax exemptions | Tax Exemptions Get all tax exemptions  |
| Tax Intended Uses - Get all intended use fiscal classification records | Tax Intended Uses Get all intended use fiscal classification records  |
| Tax Intended Uses List of Values - Get all intended use fiscal classifications | Tax Intended Uses List of Values Get all intended use fiscal classifications  |
| Tax Partner Registrations - GET action not supported | Tax Partner Registrations GET action not supported  |
| Tax Product Categories - Get all product categories | Tax Product Categories Get all product categories  |
| Tax Rates List of Values - Get all tax rates | Tax Rates List of Values Get all tax rates  |
| Tax Regimes List of Values - Get all tax regimes | Tax Regimes List of Values Get all tax regimes  |
| Tax Registrations - Get all tax registrations | Tax Registrations Get all tax registrations  |
| Tax Reporting Entities - Get all tax reporting entities | Tax Reporting Entities Get all tax reporting entities  |
| Tax Reporting Entities List of Values - Get all tax reporting entities | Tax Reporting Entities List of Values Get all tax reporting entities  |
| Tax Reporting Entities/Tax Reporting Entity Lines - Get all tax reporting entity lines for a tax repor... | Tax Reporting Entities/Tax Reporting Entity Lines Get all tax reporting entity lines for a tax reporting entity  |
| Tax Transaction Business Categories - Get all transaction business categories | Tax Transaction Business Categories Get all transaction business categories  |
| Taxpayer Identifiers - Get all taxpayer identifiers | Taxpayer Identifiers Get all taxpayer identifiers  |
| Transaction Tax Lines - Get all transaction tax lines | Transaction Tax Lines Get all transaction tax lines  |
| Withholding Tax Groups List of Values - Get all withholding tax groups | Withholding Tax Groups List of Values Get all withholding tax groups  |
| Withholding Tax Lines - Get all withholding tax lines | Withholding Tax Lines Get all withholding tax lines  |
| Workflow Users - Get all workflow users | Workflow Users Get all workflow users  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Bank Account User Rules - Update an existing country-specific rule (PATCH) | Bank Account User Rules Update an existing country-specific rule  |
| Bank Accounts - Update a bank account (PATCH) | Bank Accounts Update a bank account  |
| Bank Accounts/Bank Account Descriptive Flexfields - Update a descriptive flexfield for a bank account (PATCH) | Bank Accounts/Bank Account Descriptive Flexfields Update a descriptive flexfield for a bank account  |
| Bank Accounts/Bank Account Global Descriptive Flexfields - Update a global descriptive flexfield for a bank a... (PATCH) | Bank Accounts/Bank Account Global Descriptive Flexfields Update a global descriptive flexfield for a bank account  |
| Bank Accounts/Bank Account Grants - Update a grant for a bank account (PATCH) | Bank Accounts/Bank Account Grants Update a grant for a bank account  |
| Bank Accounts/Bank Account Payment Documents - Update a payment document for a bank account (PATCH) | Bank Accounts/Bank Account Payment Documents Update a payment document for a bank account  |
| Bank Accounts/Bank Account Payment Documents/Bank Account Checkbooks - Update a checkbook for a bank account (PATCH) | Bank Accounts/Bank Account Payment Documents/Bank Account Checkbooks Update a checkbook for a bank account  |
| Bank Accounts/Bank Account Transaction Creation Rules - Update a bank statement transaction creation rule ... (PATCH) | Bank Accounts/Bank Account Transaction Creation Rules Update a bank statement transaction creation rule for a bank account  |
| Bank Accounts/Bank Account Uses - Update a bank account use for a bank account (PATCH) | Bank Accounts/Bank Account Uses Update a bank account use for a bank account  |
| Bank Accounts/Bank Account Uses/Bank Account Payment Document Categories - Update a payment document category for a business ... (PATCH) | Bank Accounts/Bank Account Uses/Bank Account Payment Document Categories Update a payment document category for a business unit  |
| Bank Branches - Update a bank branch (PATCH) | Bank Branches Update a bank branch  |
| Banks - Update a bank (PATCH) | Banks Update a bank  |
| Budgetary Control for Enterprise Performance Management Budget Transactions - Update a budgetary control budget entry transactio... (PATCH) | Budgetary Control for Enterprise Performance Management Budget Transactions Update a budgetary control budget entry transaction interface row  |
| Cash Pools - Update a cash pool (PATCH) | Cash Pools Update a cash pool  |
| Cash Pools/Cash Pool Members - Update a cash pool member (PATCH) | Cash Pools/Cash Pool Members Update a cash pool member  |
| Collections Strategies - Update a strategy assignment (PATCH) | Collections Strategies Update a strategy assignment  |
| Collections Strategies/Strategy Template Tasks - Update a strategy template task status (PATCH) | Collections Strategies/Strategy Template Tasks Update a strategy template task status  |
| Collections Strategies/Tasks - Update a task assignment (PATCH) | Collections Strategies/Tasks Update a task assignment  |
| Collections Strategies/User Tasks - Update a user task assignment (PATCH) | Collections Strategies/User Tasks Update a user task assignment  |
| Credit and Collections Data Points - Update a credit and collections data point (PATCH) | Credit and Collections Data Points Update a credit and collections data point  |
| Data Point Values - Update a data point value (PATCH) | Data Point Values Update a data point value  |
| Data Security for Users - Update a data assignment for a user (PATCH) | Data Security for Users Update a data assignment for a user  |
| Debit Authorizations - Update a debit authorization (PATCH) | Debit Authorizations Update a debit authorization  |
| Document Sequence Derivations - Update a document sequence derivation record (PATCH) | Document Sequence Derivations Update a document sequence derivation record  |
| Early Payment Offers - Update a standing early payment offer (PATCH) | Early Payment Offers Update a standing early payment offer  |
| Early Payment Offers/Early Payment Offer Assignments - Update a standing offer assignment (PATCH) | Early Payment Offers/Early Payment Offer Assignments Update a standing offer assignment  |
| Early Payment Offers/Early Payment Offer Assignments/Early Payment Offers Descriptive Flexfields - Update a descriptive flexfield for a standing offe... (PATCH) | Early Payment Offers/Early Payment Offer Assignments/Early Payment Offers Descriptive Flexfields Update a descriptive flexfield for a standing offer assignment  |
| ERP Business Events - Update the enabled indicator for a business event (PATCH) | ERP Business Events Update the enabled indicator for a business event  |
| Expense Cash Advances - Update a cash advance (PATCH) | Expense Cash Advances Update a cash advance  |
| Expense Cash Advances/Cash Advance Descriptive Flexfields - Update the descriptive flexfield record for a cash... (PATCH) | Expense Cash Advances/Cash Advance Descriptive Flexfields Update the descriptive flexfield record for a cash advance  |
| Expense Delegations - Update an expense delegation (PATCH) | Expense Delegations Update an expense delegation  |
| Expense Distributions - Update a distribution record for a specific expens... (PATCH) | Expense Distributions Update a distribution record for a specific expense distribution identifier  |
| Expense Distributions/Project Descriptive Flexfields - Update a project-related descriptive flexfield rec... (PATCH) | Expense Distributions/Project Descriptive Flexfields Update a project-related descriptive flexfield record for a specific expense distribution identifier  |
| Expense Preferences - Update an expense preferences record for a person (PATCH) | Expense Preferences Update an expense preferences record for a person  |
| Expense Reports - Apply a cash advance (POST) | Expense Reports Applies the cash advance to the expense report. Apply a cash advance  |
| Expense Reports - Process a workflow action (POST) | Expense Reports Processes a workflow action on the expense report. Process a workflow action  |
| Expense Reports - Remove a cash advance (POST) | Expense Reports Removes the cash advance on the expense report. Remove a cash advance  |
| Expense Reports - Submit an expense report (POST) | Expense Reports Submits the expense report after validation. Submit an expense report  |
| Expense Reports - Update an expense report record for a specific exp... (PATCH) | Expense Reports Update an expense report record for a specific expense report identifier  |
| Expense Reports/Attachments - Update an attachment record for a specific attachm... (PATCH) | Expense Reports/Attachments Update an attachment record for a specific attachment identifier  |
| Expense Reports/Expense Report Descriptive Flexfields - Update an expense report header descriptive flexfi... (PATCH) | Expense Reports/Expense Report Descriptive Flexfields Update an expense report header descriptive flexfields for a specific expense report identifier  |
| Expense Reports/Expenses - Update an expense record for a specific expense id... (PATCH) | Expense Reports/Expenses Update an expense record for a specific expense identifier  |
| Expense Reports/Expenses/Attachments - Update an attachment record for a specific attachm... (PATCH) | Expense Reports/Expenses/Attachments Update an attachment record for a specific attachment identifier  |
| Expense Reports/Expenses/Duplicate Expenses - Update a duplicate expense record (PATCH) | Expense Reports/Expenses/Duplicate Expenses Update a duplicate expense record  |
| Expense Reports/Expenses/Expense Attendees - Update an expense attendee record for a specific e... (PATCH) | Expense Reports/Expenses/Expense Attendees Update an expense attendee record for a specific expense identifier  |
| Expense Reports/Expenses/Expense Descriptive Flexfields - Update a descriptive flexfield record for a specif... (PATCH) | Expense Reports/Expenses/Expense Descriptive Flexfields Update a descriptive flexfield record for a specific expense identifier  |
| Expense Reports/Expenses/Expense Distributions - Update a distribution record for a specific expens... (PATCH) | Expense Reports/Expenses/Expense Distributions Update a distribution record for a specific expense distribution identifier  |
| Expense Reports/Expenses/Expense Distributions/Projects Descriptive Flexfields - Update a project-related descriptive flexfield rec... (PATCH) | Expense Reports/Expenses/Expense Distributions/Projects Descriptive Flexfields Update a project-related descriptive flexfield record for a specific expense distribution identifier  |
| Expense Reports/Expenses/Expense Itemizations - Update an expense itemization record for a specifi... (PATCH) | Expense Reports/Expenses/Expense Itemizations Update an expense itemization record for a specific expense identifier  |
| Expense Reports/Expenses/Matched Expenses - PATCH action not supported (PATCH) | Expense Reports/Expenses/Matched Expenses PATCH action not supported  |
| Expense Travel Itineraries - Update a travel itinerary (PATCH) | Expense Travel Itineraries Update a travel itinerary  |
| Expense Travel Itineraries/Expense Travel Itinerary Reservations - Update a travel itinerary reservation (PATCH) | Expense Travel Itineraries/Expense Travel Itinerary Reservations Update a travel itinerary reservation  |
| Expense Travel Itineraries/Expense Travel Itinerary Reservations/Expense Travel Itinerary Reservation Items - Update a travel itinerary reservation item (PATCH) | Expense Travel Itineraries/Expense Travel Itinerary Reservations/Expense Travel Itinerary Reservation Items Update a travel itinerary reservation item  |
| Expenses - Create a duplicate of an expense. (POST) | Expenses Creates a copy of the cash item to a new line. Create a duplicate of an expense.  |
| Expenses - Update an expense record (PATCH) | Expenses Update an expense record  |
| Expenses/Attachments - Update an attachment record (PATCH) | Expenses/Attachments Update an attachment record  |
| Expenses/Duplicate Expenses - Update a duplicate expense record (PATCH) | Expenses/Duplicate Expenses Update a duplicate expense record  |
| Expenses/Expense Attendees - Update an expense attendee record for an expense i... (PATCH) | Expenses/Expense Attendees Update an expense attendee record for an expense identifier  |
| Expenses/Expense Descriptive Flexfields - Update the descriptive flexfield record for an exp... (PATCH) | Expenses/Expense Descriptive Flexfields Update the descriptive flexfield record for an expense identifier  |
| Expenses/Expense Distributions - Update the distribution record for an expense dist... (PATCH) | Expenses/Expense Distributions Update the distribution record for an expense distribution identifier  |
| Expenses/Expense Distributions/Project Descriptive Flexfields - Update the project-related descriptive flexfield r... (PATCH) | Expenses/Expense Distributions/Project Descriptive Flexfields Update the project-related descriptive flexfield record for an expense distribution identifier  |
| Expenses/Expense Itemizations - Update the expense itemization record for an expen... (PATCH) | Expenses/Expense Itemizations Update the expense itemization record for an expense identifier  |
| Expenses/Matched Expenses - PATCH action not supported (PATCH) | Expenses/Matched Expenses PATCH action not supported  |
| External Bank Accounts - Update an external bank account (PATCH) | External Bank Accounts Update an external bank account  |
| External Bank Accounts/Account Owners - Update an account owner for an external bank accou... (PATCH) | External Bank Accounts/Account Owners Update an account owner for an external bank account  |
| External Cash Transactions - Update an external transaction (PATCH) | External Cash Transactions Update an external transaction  |
| External Cash Transactions/Attachments - Update an attachment for an external transaction (PATCH) | External Cash Transactions/Attachments Update an attachment for an external transaction  |
| External Cash Transactions/Descriptive Flexfields - Update a descriptive flexfield for an external tra... (PATCH) | External Cash Transactions/Descriptive Flexfields Update a descriptive flexfield for an external transaction  |
| External Payees - Update an external payee (PATCH) | External Payees Update an external payee  |
| External Payees/External Party Payment Methods - Update a payment method assigned to a payee (PATCH) | External Payees/External Party Payment Methods Update a payment method assigned to a payee  |
| Federal Account Symbols - Update a federal account symbol (PATCH) | Federal Account Symbols Update a federal account symbol  |
| Federal Account Symbols/Federal Account Symbols Descriptive Flexfields - Update a federal account symbol descriptive flexfi... (PATCH) | Federal Account Symbols/Federal Account Symbols Descriptive Flexfields Update a federal account symbol descriptive flexfield  |
| Federal Agency Location Codes - Update an agency location code (PATCH) | Federal Agency Location Codes Update an agency location code  |
| Federal Agency Location Codes/Federal Agency Location Codes Descriptive Flexfields - Update an agency location code descriptive flexfie... (PATCH) | Federal Agency Location Codes/Federal Agency Location Codes Descriptive Flexfields Update an agency location code descriptive flexfield  |
| Federal Business Event Type Codes - Update a business event type code (PATCH) | Federal Business Event Type Codes Update a business event type code  |
| Federal Business Event Type Codes/Federal Business Event Type Codes Descriptive Flexfields - Update a business event type code descriptive flex... (PATCH) | Federal Business Event Type Codes/Federal Business Event Type Codes Descriptive Flexfields Update a business event type code descriptive flexfield  |
| Federal Fund Attributes - Update a fund attribute (PATCH) | Federal Fund Attributes Update a fund attribute  |
| Federal Fund Attributes/Federal Fund Attributes Descriptive Flexfields - Update a fund attribute descriptive flexfield (PATCH) | Federal Fund Attributes/Federal Fund Attributes Descriptive Flexfields Update a fund attribute descriptive flexfield  |
| Federal Payment Format Mappings - Update a payment format mapping (PATCH) | Federal Payment Format Mappings Update a payment format mapping  |
| Federal Payment Format Mappings/Federal Payment Format Mappings Descriptive Flexfields - Update descriptive flexfield for a payment format ... (PATCH) | Federal Payment Format Mappings/Federal Payment Format Mappings Descriptive Flexfields Update descriptive flexfield for a payment format mapping  |
| Federal Setup Options - Update a setup option (PATCH) | Federal Setup Options Update a setup option  |
| Federal Setup Options/Federal Setup Options Descriptive Flexfields - Update a setup option descriptive flexfield (PATCH) | Federal Setup Options/Federal Setup Options Descriptive Flexfields Update a setup option descriptive flexfield  |
| Federal Treasury Account Symbols - Update a treasury account symbol (PATCH) | Federal Treasury Account Symbols Update a treasury account symbol  |
| Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes - Update a treasury account symbol business event ty... (PATCH) | Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes Update a treasury account symbol business event type code  |
| Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes/Federal Treasury Account Symbols Business Event Type Codes Descriptive Flexfields - Update a TAS BETC descriptive flexfield (PATCH) | Federal Treasury Account Symbols/Federal Treasury Account Symbols Business Event Type Codes/Federal Treasury Account Symbols Business Event Type Codes Descriptive Flexfields Update a TAS BETC descriptive flexfield  |
| Federal Treasury Account Symbols/Federal Treasury Account Symbols Descriptive Flexfields - Update a treasury account symbol descriptive flexf... (PATCH) | Federal Treasury Account Symbols/Federal Treasury Account Symbols Descriptive Flexfields Update a treasury account symbol descriptive flexfield  |
| Federal USSGL Accounts - Update a USSGL account (PATCH) | Federal USSGL Accounts Update a USSGL account  |
| Federal USSGL Accounts/Federal USSGL Accounts Descriptive Flexfields - Update a federal USSGL account descriptive flexfie... (PATCH) | Federal USSGL Accounts/Federal USSGL Accounts Descriptive Flexfields Update a federal USSGL account descriptive flexfield  |
| Instrument Assignments - Update a payment instrument assignment (PATCH) | Instrument Assignments Update a payment instrument assignment  |
| Interim Payables Documents - Update an interim payables document (PATCH) | Interim Payables Documents Update an interim payables document  |
| Invoice Holds - Update an invoice hold (PATCH) | Invoice Holds Update an invoice hold  |
| Invoice Holds/Invoice Holds Descriptive Flexfields - Update descriptive flexfields for an invoice hold (PATCH) | Invoice Holds/Invoice Holds Descriptive Flexfields Update descriptive flexfields for an invoice hold  |
| Invoices - Update an invoice (PATCH) | Invoices Update an invoice  |
| Invoices/Invoice Installments - Update an invoice installment (PATCH) | Invoices/Invoice Installments Update an invoice installment  |
| Invoices/Invoice Installments/Invoice Installments Descriptive Flexfields - Update descriptive flexfields for an invoice insta... (PATCH) | Invoices/Invoice Installments/Invoice Installments Descriptive Flexfields Update descriptive flexfields for an invoice installment  |
| Invoices/Invoice Installments/Invoice Installments Global Descriptive Flexfields - Update global descriptive flexfields for an invoic... (PATCH) | Invoices/Invoice Installments/Invoice Installments Global Descriptive Flexfields Update global descriptive flexfields for an invoice installment  |
| Invoices/Invoice Lines - Update an invoice line (PATCH) | Invoices/Invoice Lines Update an invoice line  |
| Invoices/Invoice Lines/Invoice Distributions - Update an invoice distribution (PATCH) | Invoices/Invoice Lines/Invoice Distributions Update an invoice distribution  |
| Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Descriptive Flexfields - Update descriptive flexfields for an invoice distr... (PATCH) | Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Descriptive Flexfields Update descriptive flexfields for an invoice distribution  |
| Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Global Descriptive Flexfields - Update global descriptive flexfields for an invoic... (PATCH) | Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Global Descriptive Flexfields Update global descriptive flexfields for an invoice distribution  |
| Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Project Descriptive Flexfields - Update a project-related descriptive flexfield for... (PATCH) | Invoices/Invoice Lines/Invoice Distributions/Invoice Distributions Project Descriptive Flexfields Update a project-related descriptive flexfield for an invoice distribution  |
| Invoices/Invoice Lines/Invoice Lines Descriptive Flexfields - Update descriptive flexfields for an invoice line (PATCH) | Invoices/Invoice Lines/Invoice Lines Descriptive Flexfields Update descriptive flexfields for an invoice line  |
| Invoices/Invoice Lines/Invoice Lines Global Descriptive Flexfields - Update global descriptive flexfields for an invoic... (PATCH) | Invoices/Invoice Lines/Invoice Lines Global Descriptive Flexfields Update global descriptive flexfields for an invoice line  |
| Invoices/Invoice Lines/Invoice Lines Project Descriptive Flexfields - Update a project-related descriptive flexfield for... (PATCH) | Invoices/Invoice Lines/Invoice Lines Project Descriptive Flexfields Update a project-related descriptive flexfield for an invoice line  |
| Invoices/Invoices Descriptive Flexfields - Update descriptive flexfields for an invoice (PATCH) | Invoices/Invoices Descriptive Flexfields Update descriptive flexfields for an invoice  |
| Invoices/Invoices Global Descriptive Flexfields - Update global descriptive flexfields for an invoic... (PATCH) | Invoices/Invoices Global Descriptive Flexfields Update global descriptive flexfields for an invoice  |
| Joint Venture General Ledger Distributions - Update a joint venture general ledger distribution (PATCH) | Joint Venture General Ledger Distributions Update a joint venture general ledger distribution  |
| Joint Venture General Ledger Transactions - Update a joint venture general ledger transaction (PATCH) | Joint Venture General Ledger Transactions Update a joint venture general ledger transaction  |
| Joint Venture General Ledger Transactions/Joint Venture Transaction Descriptive Flexfields - Update descriptive flexfields for a joint venture ... (PATCH) | Joint Venture General Ledger Transactions/Joint Venture Transaction Descriptive Flexfields Update descriptive flexfields for a joint venture transaction  |
| Joint Venture Invoicing Partners - Update a joint venture invoicing partner (PATCH) | Joint Venture Invoicing Partners Update a joint venture invoicing partner  |
| Joint Venture Invoicing Partners/Invoicing Partner Descriptive Flexfields - Update a descriptive flexfield for a joint venture (PATCH) | Joint Venture Invoicing Partners/Invoicing Partner Descriptive Flexfields Update a descriptive flexfield for a joint venture  |
| Joint Venture Options - Update joint venture options (PATCH) | Joint Venture Options Update joint venture options  |
| Joint Venture Options/Joint Venture Options Descriptive Flexfields - Update descriptive flexfields for joint venture op... (PATCH) | Joint Venture Options/Joint Venture Options Descriptive Flexfields Update descriptive flexfields for joint venture options  |
| Joint Venture Partner Contributions - Update a joint venture partner contribution (PATCH) | Joint Venture Partner Contributions Update a joint venture partner contribution  |
| Joint Venture Partner Contributions/Partner Contribution Descriptive Flexfields - Update a descriptive flexfield for a partner contr... (PATCH) | Joint Venture Partner Contributions/Partner Contribution Descriptive Flexfields Update a descriptive flexfield for a partner contribution  |
| Joint Venture Partner Contributions/Partner Contribution References - Update a transaction reference for a partner contr... (PATCH) | Joint Venture Partner Contributions/Partner Contribution References Update a transaction reference for a partner contribution  |
| Joint Venture Subledger Distributions - Update a joint venture subledger distribution (PATCH) | Joint Venture Subledger Distributions Update a joint venture subledger distribution  |
| Joint Venture Subledger Transactions - Update a joint venture subledger transaction (PATCH) | Joint Venture Subledger Transactions Update a joint venture subledger transaction  |
| Joint Venture Subledger Transactions/Joint Venture Transaction Descriptive Flexfields - Update descriptive flexfields for a joint venture ... (PATCH) | Joint Venture Subledger Transactions/Joint Venture Transaction Descriptive Flexfields Update descriptive flexfields for a joint venture transaction  |
| Joint Ventures - Update a joint venture (PATCH) | Joint Ventures Update a joint venture  |
| Joint Ventures/Cost Centers/Cost Center Descriptive Flexfields - Update a descriptive flexfield for a cost center a... (PATCH) | Joint Ventures/Cost Centers/Cost Center Descriptive Flexfields Update a descriptive flexfield for a cost center associated with a joint venture  |
| Joint Ventures/Cost Centers/Distributable Value Sets - Update a distributable value set (PATCH) | Joint Ventures/Cost Centers/Distributable Value Sets Update a distributable value set  |
| Joint Ventures/Distributable Project Rules - Update a distributable project rule (PATCH) | Joint Ventures/Distributable Project Rules Update a distributable project rule  |
| Joint Ventures/Joint Venture Descriptive Flexfieds - Update a descriptive flexfield for a joint venture (PATCH) | Joint Ventures/Joint Venture Descriptive Flexfieds Update a descriptive flexfield for a joint venture  |
| Joint Ventures/Ownership Definitions - Update an ownership definition (PATCH) | Joint Ventures/Ownership Definitions Update an ownership definition  |
| Joint Ventures/Ownership Definitions/Ownership Definition Descriptive Flexfields - Update a descriptive flexfield for an ownership de... (PATCH) | Joint Ventures/Ownership Definitions/Ownership Definition Descriptive Flexfields Update a descriptive flexfield for an ownership definition associated with a joint venture  |
| Joint Ventures/Ownership Definitions/Ownership Percentages - Update an ownership percentage for a stakeholder (PATCH) | Joint Ventures/Ownership Definitions/Ownership Percentages Update an ownership percentage for a stakeholder  |
| Joint Ventures/Ownership Definitions/Ownership Percentages/Ownership Percentage Descriptive Flexfields - Update a descriptive flexfield for an ownership pe... (PATCH) | Joint Ventures/Ownership Definitions/Ownership Percentages/Ownership Percentage Descriptive Flexfields Update a descriptive flexfield for an ownership percentage associated with an ownership definition  |
| Joint Ventures/Stakeholders - Update a joint venture stakeholder (PATCH) | Joint Ventures/Stakeholders Update a joint venture stakeholder  |
| Joint Ventures/Stakeholders/Stakeholder Descriptive Flexfields - Update a descriptive flexfield for a joint venture... (PATCH) | Joint Ventures/Stakeholders/Stakeholder Descriptive Flexfields Update a descriptive flexfield for a joint venture stakeholder  |
| Journal Batches - Update a batch (PATCH) | Journal Batches Update a batch  |
| Party Fiscal Classifications - Update a party fiscal classification (PATCH) | Party Fiscal Classifications Update a party fiscal classification  |
| Party Fiscal Classifications/Tax Regime Associations - Update a tax regime for a party fiscal classificat... (PATCH) | Party Fiscal Classifications/Tax Regime Associations Update a tax regime for a party fiscal classification  |
| Party Tax Profiles - Update a party tax profile (PATCH) | Party Tax Profiles Update a party tax profile  |
| Payables Payments - Update a payment (PATCH) | Payables Payments Update a payment  |
| Payables Payments/Payments Descriptive Flexfields - Update descriptive flexfields for a payment (PATCH) | Payables Payments/Payments Descriptive Flexfields Update descriptive flexfields for a payment  |
| Payables Payments/Payments Global Descriptive Flexfields - Update global descriptive flexfields for a payment (PATCH) | Payables Payments/Payments Global Descriptive Flexfields Update global descriptive flexfields for a payment  |
| Payables Payments/Related Invoices/Related Invoice Global Descriptive Flexfields - Update global descriptive flexfields for an invoic... (PATCH) | Payables Payments/Related Invoices/Related Invoice Global Descriptive Flexfields Update global descriptive flexfields for an invoice payment  |
| Payment Process Requests - Update a payment process request (PATCH) | Payment Process Requests Update a payment process request  |
| Receipt Method Assignments - Update a receipt method assigned to a customer acc... (PATCH) | Receipt Method Assignments Update a receipt method assigned to a customer account or site  |
| Receivables Invoices - Update a receivables invoice (PATCH) | Receivables Invoices Update a receivables invoice  |
| Receivables Invoices/Receivables Invoice Installments - Update a receivables invoice installment (PATCH) | Receivables Invoices/Receivables Invoice Installments Update a receivables invoice installment  |
| Tax Exemptions - Update a tax exemption (PATCH) | Tax Exemptions Update a tax exemption  |
| Tax Registrations - Update a tax registration (PATCH) | Tax Registrations Update a tax registration  |
| Taxpayer Identifiers - Update a taxpayer identifier (PATCH) | Taxpayer Identifiers Update a taxpayer identifier  |


