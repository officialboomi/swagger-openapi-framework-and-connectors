
# procurement


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| Draft Purchase Orders - Create a header | Draft Purchase Orders Create a header  |
| Draft Purchase Orders/Attachments for Draft Purchase Orders - Create an attachment | Draft Purchase Orders/Attachments for Draft Purchase Orders Create an attachment  |
| Draft Purchase Orders/Flexfields for Draft Purchase Orders - Create a descriptive flexfield | Draft Purchase Orders/Flexfields for Draft Purchase Orders Create a descriptive flexfield  |
| Draft Purchase Orders/Lines - Create a line | Draft Purchase Orders/Lines Create a line  |
| Draft Purchase Orders/Lines/Attachments for Draft Purchase Orders - Create an attachment | Draft Purchase Orders/Lines/Attachments for Draft Purchase Orders Create an attachment  |
| Draft Purchase Orders/Lines/Flexfields for Draft Purchase Orders - Create a descriptive flexfield | Draft Purchase Orders/Lines/Flexfields for Draft Purchase Orders Create a descriptive flexfield  |
| Draft Purchase Orders/Lines/Schedules - Create a schedule | Draft Purchase Orders/Lines/Schedules Create a schedule  |
| Draft Purchase Orders/Lines/Schedules/Attachments for Draft Purchase Orders - Create an attachment | Draft Purchase Orders/Lines/Schedules/Attachments for Draft Purchase Orders Create an attachment  |
| Draft Purchase Orders/Lines/Schedules/Distributions - Create a distribution | Draft Purchase Orders/Lines/Schedules/Distributions Create a distribution  |
| Draft Purchase Orders/Lines/Schedules/Distributions/Flexfields for Draft Purchase Orders - Create a descriptive flexfield | Draft Purchase Orders/Lines/Schedules/Distributions/Flexfields for Draft Purchase Orders Create a descriptive flexfield  |
| Draft Purchase Orders/Lines/Schedules/Distributions/Project Costing Flexfields for Distributions - Create a project costing flexfield | Draft Purchase Orders/Lines/Schedules/Distributions/Project Costing Flexfields for Distributions Create a project costing flexfield  |
| Draft Purchase Orders/Lines/Schedules/Flexfields for Draft Purchase Orders - Create a descriptive flexfield | Draft Purchase Orders/Lines/Schedules/Flexfields for Draft Purchase Orders Create a descriptive flexfield  |
| Procurement Agents - Create procurement agents | Procurement Agents Create procurement agents  |
| Public Shopping List Lines - Create lines | Public Shopping List Lines Create lines  |
| Purchase Requisitions - Create a header | Purchase Requisitions Create a header  |
| Purchase Requisitions/Flexfields for Purchase Requisitions - Create descriptive flexfields | Purchase Requisitions/Flexfields for Purchase Requisitions Create descriptive flexfields  |
| Purchase Requisitions/Lines - Create a line | Purchase Requisitions/Lines Create a line  |
| Purchase Requisitions/Lines/Distributions - Create a distribution | Purchase Requisitions/Lines/Distributions Create a distribution  |
| Purchase Requisitions/Lines/Distributions/Flexfields for Purchase Requisitions - Create descriptive flexfields | Purchase Requisitions/Lines/Distributions/Flexfields for Purchase Requisitions Create descriptive flexfields  |
| Purchase Requisitions/Lines/Distributions/Project Costing Flexfields for Purchase Requisition Distributions - Create project costing flexfields | Purchase Requisitions/Lines/Distributions/Project Costing Flexfields for Purchase Requisition Distributions Create project costing flexfields  |
| Purchase Requisitions/Lines/Flexfields for Purchase Requisitions - Create descriptive flexfields | Purchase Requisitions/Lines/Flexfields for Purchase Requisitions Create descriptive flexfields  |
| Requisition One-Time Locations - Create locations | Requisition One-Time Locations Create locations  |
| Shopping Search - Create one search phrase | Shopping Search Create one search phrase  |
| Supplier Eligibility - Create supplier eligibility records | Supplier Eligibility Create supplier eligibility records  |
| Supplier Initiatives - Create one initiative | Supplier Initiatives Create one initiative  |
| Supplier Initiatives/Attachments - Upload an attachment | Supplier Initiatives/Attachments Upload an attachment  |
| Supplier Initiatives/Evaluation Team Members - Create one team member | Supplier Initiatives/Evaluation Team Members Create one team member  |
| Supplier Initiatives/Flexfields for Initiatives - Create one flexfield | Supplier Initiatives/Flexfields for Initiatives Create one flexfield  |
| Supplier Initiatives/Qualification Areas - Create one qualification area | Supplier Initiatives/Qualification Areas Create one qualification area  |
| Supplier Initiatives/Suppliers - Create one supplier | Supplier Initiatives/Suppliers Create one supplier  |
| Supplier Negotiations - Create a negotiation | Supplier Negotiations Create a negotiation Create a negotiation  |
| Supplier Negotiations - Create a negotiation from template | Supplier Negotiations Creates a draft negotiation using a negotiation template. Create a negotiation from template  |
| Supplier Negotiations/Attachments - Create an attachment | Supplier Negotiations/Attachments Create an attachment  |
| Supplier Negotiations/Collaboration Team Members - Create a team member | Supplier Negotiations/Collaboration Team Members Create a team member  |
| Supplier Negotiations/Flexfields for Abstracts - Create flexfields | Supplier Negotiations/Flexfields for Abstracts Create flexfields  |
| Supplier Negotiations/Lines - Create a negotiation line | Supplier Negotiations/Lines Create a negotiation line  |
| Supplier Negotiations/Lines/Attachments - Create an attachment | Supplier Negotiations/Lines/Attachments Create an attachment  |
| Supplier Negotiations/Lines/Attribute Groups - Create a group | Supplier Negotiations/Lines/Attribute Groups Create a group  |
| Supplier Negotiations/Lines/Attribute Groups/Line Attributes - Create an attribute | Supplier Negotiations/Lines/Attribute Groups/Line Attributes Create an attribute  |
| Supplier Negotiations/Lines/Attribute Groups/Line Attributes/Response Values - Create a value | Supplier Negotiations/Lines/Attribute Groups/Line Attributes/Response Values Create a value  |
| Supplier Negotiations/Lines/External Cost Factors - Create a cost factor | Supplier Negotiations/Lines/External Cost Factors Create a cost factor  |
| Supplier Negotiations/Lines/Flexfields for Abstracts - Create flexfields | Supplier Negotiations/Lines/Flexfields for Abstracts Create flexfields  |
| Supplier Negotiations/Lines/Price Breaks - Create a price break | Supplier Negotiations/Lines/Price Breaks Create a price break  |
| Supplier Negotiations/Lines/Price Tiers - Create a price tier | Supplier Negotiations/Lines/Price Tiers Create a price tier  |
| Supplier Negotiations/Sections/Requirements/Attachments - Create an attachment | Supplier Negotiations/Sections/Requirements/Attachments Create an attachment  |
| Supplier Negotiations/Supplier Flexfields - Create supplier flexfields | Supplier Negotiations/Supplier Flexfields Create supplier flexfields  |
| Supplier Negotiations/Suppliers - Create a supplier | Supplier Negotiations/Suppliers Create a supplier  |
| Supplier Negotiations/Suppliers/Access Restrictions - Create a restriction | Supplier Negotiations/Suppliers/Access Restrictions Create a restriction  |
| Supplier Qualification Question Responses - Create a question response | Supplier Qualification Question Responses Create a question response  |
| Supplier Qualification Question Responses/Values - POST action not supported | Supplier Qualification Question Responses/Values POST action not supported  |
| Supplier Qualification Question Responses/Values/Attachments - POST action not supported | Supplier Qualification Question Responses/Values/Attachments POST action not supported  |
| Supplier Qualification Questions - Create a question | Supplier Qualification Questions Create a question Create a question  |
| Supplier Qualification Questions - Create one draft revision | Supplier Qualification Questions Creates the draft revision of a question. Create one draft revision  |
| Supplier Qualification Questions/Acceptable Responses - Create an acceptable response | Supplier Qualification Questions/Acceptable Responses Create an acceptable response  |
| Supplier Qualification Questions/Acceptable Responses/Branches - Create a branching question | Supplier Qualification Questions/Acceptable Responses/Branches Create a branching question  |
| Supplier Qualification Questions/Attachments - Upload a question attachment | Supplier Qualification Questions/Attachments Upload a question attachment  |
| Supplier Qualification Questions/Scores - Create a question score | Supplier Qualification Questions/Scores Create a question score  |
| Suppliers - Create a supplier | Suppliers Create a supplier  |
| Suppliers/Addresses - Create an address | Suppliers/Addresses Create an address  |
| Suppliers/Attachments - Create an attachment | Suppliers/Attachments Create an attachment  |
| Suppliers/Business Classifications - Create a business classification | Suppliers/Business Classifications Create a business classification  |
| Suppliers/Business Classifications/Attachments - Create an attachment | Suppliers/Business Classifications/Attachments Create an attachment  |
| Suppliers/Contacts - Create a contact | Suppliers/Contacts Create a contact  |
| Suppliers/Contacts/Addresses - Create an address | Suppliers/Contacts/Addresses Create an address  |
| Suppliers/Flexfields for Suppliers - Create a flexfield | Suppliers/Flexfields for Suppliers Create a flexfield  |
| Suppliers/Global Flexfields for Suppliers - Create a flexfield | Suppliers/Global Flexfields for Suppliers Create a flexfield  |
| Suppliers/Products and Services - Create a product and service | Suppliers/Products and Services Create a product and service  |
| Suppliers/Sites - Create a site | Suppliers/Sites Create a site  |
| Suppliers/Sites/Attachments - Create an attachment | Suppliers/Sites/Attachments Create an attachment  |
| Suppliers/Sites/Flexfields for Suppliers - Create a flexfield | Suppliers/Sites/Flexfields for Suppliers Create a flexfield  |
| Suppliers/Sites/Global Flexfields for Suppliers - Create a flexfield | Suppliers/Sites/Global Flexfields for Suppliers Create a flexfield  |
| Suppliers/Sites/Site Assignments - Create an assignment | Suppliers/Sites/Site Assignments Create an assignment  |
| Suppliers/Sites/Third-Party Payment Relationships - Create a relationship | Suppliers/Sites/Third-Party Payment Relationships Create a relationship  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Draft Purchase Orders/Attachments for Draft Purchase Orders - Delete an attachment | Draft Purchase Orders/Attachments for Draft Purchase Orders Delete an attachment  |
| Draft Purchase Orders/Lines/Attachments for Draft Purchase Orders - Delete an attachment | Draft Purchase Orders/Lines/Attachments for Draft Purchase Orders Delete an attachment  |
| Draft Purchase Orders/Lines/Schedules/Attachments for Draft Purchase Orders - Delete an attachment | Draft Purchase Orders/Lines/Schedules/Attachments for Draft Purchase Orders Delete an attachment  |
| Public Shopping List Lines - Delete a line | Public Shopping List Lines Delete a line  |
| Purchase Requisitions - Delete a header | Purchase Requisitions Delete a header  |
| Purchase Requisitions/Lines - Delete a line | Purchase Requisitions/Lines Delete a line  |
| Purchase Requisitions/Lines/Distributions - Delete a distribution | Purchase Requisitions/Lines/Distributions Delete a distribution  |
| Requisition One-Time Locations - Delete a location | Requisition One-Time Locations Delete a location  |
| Supplier Eligibility - Delete one supplier eligibility record | Supplier Eligibility Delete one supplier eligibility record  |
| Supplier Negotiations/Attachments - Delete an attachment | Supplier Negotiations/Attachments Delete an attachment  |
| Supplier Negotiations/Lines/Attachments - Delete an attachment | Supplier Negotiations/Lines/Attachments Delete an attachment  |
| Supplier Negotiations/Lines/External Cost Factors - Delete a cost factor | Supplier Negotiations/Lines/External Cost Factors Delete a cost factor  |
| Supplier Negotiations/Sections/Requirements/Attachments - Delete an attachment | Supplier Negotiations/Sections/Requirements/Attachments Delete an attachment  |
| Supplier Negotiations/Suppliers/Access Restrictions - Delete a restriction | Supplier Negotiations/Suppliers/Access Restrictions Delete a restriction  |
| Supplier Qualification Questions - Delete a question | Supplier Qualification Questions Delete a question  |
| Supplier Qualification Questions/Acceptable Responses - Delete an acceptable response | Supplier Qualification Questions/Acceptable Responses Delete an acceptable response  |
| Supplier Qualification Questions/Acceptable Responses/Branches - Delete a branching question | Supplier Qualification Questions/Acceptable Responses/Branches Delete a branching question  |
| Supplier Qualification Questions/Attachments - Delete an attachment | Supplier Qualification Questions/Attachments Delete an attachment  |
| Supplier Qualification Questions/Scores - Delete a question score | Supplier Qualification Questions/Scores Delete a question score  |
| Suppliers/Attachments - Delete an attachment | Suppliers/Attachments Delete an attachment  |
| Suppliers/Business Classifications - Delete a business classification | Suppliers/Business Classifications Delete a business classification  |
| Suppliers/Business Classifications/Attachments - Delete an attachment | Suppliers/Business Classifications/Attachments Delete an attachment  |
| Suppliers/Contacts/Addresses - Delete a contact address | Suppliers/Contacts/Addresses Delete a contact address  |
| Suppliers/Products and Services - Delete a product and service | Suppliers/Products and Services Delete a product and service  |
| Suppliers/Sites/Attachments - Delete an attachment | Suppliers/Sites/Attachments Delete an attachment  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Catalog Category Hierarchy Nodes - Get one node | Catalog Category Hierarchy Nodes Get one node  |
| Draft Purchase Orders - Get one draft purchase order | Draft Purchase Orders Get one draft purchase order  |
| Draft Purchase Orders/Attachments for Draft Purchase Orders - GET one attachment not supported | Draft Purchase Orders/Attachments for Draft Purchase Orders GET one attachment not supported  |
| Draft Purchase Orders/Flexfields for Draft Purchase Orders - GET one descriptive flexfield not supported | Draft Purchase Orders/Flexfields for Draft Purchase Orders GET one descriptive flexfield not supported  |
| Draft Purchase Orders/Lines - Get one line | Draft Purchase Orders/Lines Get one line  |
| Draft Purchase Orders/Lines/Attachments for Draft Purchase Orders - GET one attachment not supported | Draft Purchase Orders/Lines/Attachments for Draft Purchase Orders GET one attachment not supported  |
| Draft Purchase Orders/Lines/Flexfields for Draft Purchase Orders - GET one descriptive flexfield not supported | Draft Purchase Orders/Lines/Flexfields for Draft Purchase Orders GET one descriptive flexfield not supported  |
| Draft Purchase Orders/Lines/Schedules - Get one schedule | Draft Purchase Orders/Lines/Schedules Get one schedule  |
| Draft Purchase Orders/Lines/Schedules/Attachments for Draft Purchase Orders - GET one attachment not supported | Draft Purchase Orders/Lines/Schedules/Attachments for Draft Purchase Orders GET one attachment not supported  |
| Draft Purchase Orders/Lines/Schedules/Distributions - Get one distribution | Draft Purchase Orders/Lines/Schedules/Distributions Get one distribution  |
| Draft Purchase Orders/Lines/Schedules/Distributions/Flexfields for Draft Purchase Orders - GET one descriptive flexfield not supported | Draft Purchase Orders/Lines/Schedules/Distributions/Flexfields for Draft Purchase Orders GET one descriptive flexfield not supported  |
| Draft Purchase Orders/Lines/Schedules/Distributions/Project Costing Flexfields for Distributions - GET one project costing flexfield not supported | Draft Purchase Orders/Lines/Schedules/Distributions/Project Costing Flexfields for Distributions GET one project costing flexfield not supported  |
| Draft Purchase Orders/Lines/Schedules/Flexfields for Draft Purchase Orders - GET one descriptive flexfield not supported | Draft Purchase Orders/Lines/Schedules/Flexfields for Draft Purchase Orders GET one descriptive flexfield not supported  |
| Negotiation Statuses List of Values - Get one negotiation status | Negotiation Statuses List of Values Get one negotiation status  |
| Procurement Agents - Get one procurement agent | Procurement Agents Get one procurement agent  |
| Project Task Completion Events List Of Values - Get one event | Project Task Completion Events List Of Values Get one event  |
| Public Shopping List Lines - Get one line | Public Shopping List Lines Get one line  |
| Punchout Connections - Get one redirection URL | Punchout Connections Get one redirection URL  |
| Purchase Order Schedules - Get one schedule | Purchase Order Schedules Get one schedule  |
| Purchase Orders - Get one purchase order | Purchase Orders Get one purchase order  |
| Purchase Orders List of Values - Get one purchase order | Purchase Orders List of Values Get one purchase order  |
| Purchase Orders/Attachments for Purchase Orders - GET one attachment not supported | Purchase Orders/Attachments for Purchase Orders GET one attachment not supported  |
| Purchase Orders/Flexfields for Purchase Orders - GET one descriptive flexfield not supported | Purchase Orders/Flexfields for Purchase Orders GET one descriptive flexfield not supported  |
| Purchase Orders/Lines - Get one line | Purchase Orders/Lines Get one line  |
| Purchase Orders/Lines/Attachments for Purchase Orders - GET one attachment not supported | Purchase Orders/Lines/Attachments for Purchase Orders GET one attachment not supported  |
| Purchase Orders/Lines/Flexfields for Purchase Orders - GET one descriptive flexfield not supported | Purchase Orders/Lines/Flexfields for Purchase Orders GET one descriptive flexfield not supported  |
| Purchase Orders/Lines/Schedules - Get one schedule | Purchase Orders/Lines/Schedules Get one schedule  |
| Purchase Orders/Lines/Schedules/Attachments for Purchase Orders - GET one attachment not supported | Purchase Orders/Lines/Schedules/Attachments for Purchase Orders GET one attachment not supported  |
| Purchase Orders/Lines/Schedules/Distributions - Get one distribution | Purchase Orders/Lines/Schedules/Distributions Get one distribution  |
| Purchase Orders/Lines/Schedules/Distributions/Flexfields for Purchase Orders - GET one descriptive flexfield not supported | Purchase Orders/Lines/Schedules/Distributions/Flexfields for Purchase Orders GET one descriptive flexfield not supported  |
| Purchase Orders/Lines/Schedules/Distributions/Project Costing Flexfields for Distributions - GET one project costing flexfield not supported | Purchase Orders/Lines/Schedules/Distributions/Project Costing Flexfields for Distributions GET one project costing flexfield not supported  |
| Purchase Orders/Lines/Schedules/Flexfields for Purchase Orders - GET one descriptive flexfield not supported | Purchase Orders/Lines/Schedules/Flexfields for Purchase Orders GET one descriptive flexfield not supported  |
| Purchase Requisitions - Get one header | Purchase Requisitions Get one header  |
| Purchase Requisitions/Flexfields for Purchase Requisitions - GET one descriptive flexfield not supported | Purchase Requisitions/Flexfields for Purchase Requisitions GET one descriptive flexfield not supported  |
| Purchase Requisitions/Lines - Get one line | Purchase Requisitions/Lines Get one line  |
| Purchase Requisitions/Lines/Distributions - Get one distribution | Purchase Requisitions/Lines/Distributions Get one distribution  |
| Purchase Requisitions/Lines/Distributions/Flexfields for Purchase Requisitions - GET one descriptive flexfield not supported | Purchase Requisitions/Lines/Distributions/Flexfields for Purchase Requisitions GET one descriptive flexfield not supported  |
| Purchase Requisitions/Lines/Distributions/Project Costing Flexfields for Purchase Requisition Distributions - GET one project costing flexfield not supported | Purchase Requisitions/Lines/Distributions/Project Costing Flexfields for Purchase Requisition Distributions GET one project costing flexfield not supported  |
| Purchase Requisitions/Lines/Flexfields for Purchase Requisitions - GET one descriptive flexfield not supported | Purchase Requisitions/Lines/Flexfields for Purchase Requisitions GET one descriptive flexfield not supported  |
| Purchase Requisitions/Summary Attributes for Purchase Requisitions - GET one action not supported | Purchase Requisitions/Summary Attributes for Purchase Requisitions GET one action not supported  |
| Purchasing News - Get one purchasing news item | Purchasing News Get one purchasing news item  |
| Recent Requisitions - Get one recent requisition for the user | Recent Requisitions Get one recent requisition for the user  |
| Requisition One-Time Locations - Get one location | Requisition One-Time Locations Get one location  |
| Requisition Preferences - Get one preference | Requisition Preferences Get one preference  |
| Requisition Preferences/Favorite Charge Accounts - Get one favorite charge account | Requisition Preferences/Favorite Charge Accounts Get one favorite charge account  |
| Requisition Preferences/Project Costing Flexfields for Requisition Preferences - GET action not supported | Requisition Preferences/Project Costing Flexfields for Requisition Preferences GET action not supported  |
| Requisition Preferences/Requisitioning Options - Get one requisitioning option | Requisition Preferences/Requisitioning Options Get one requisitioning option  |
| Requisition Product Details - Get one item detail | Requisition Product Details Get one item detail  |
| Requisition Product Details/Price Breaks - Get one item price break | Requisition Product Details/Price Breaks Get one item price break  |
| Shopping Catalog Item Details - Get one item detail | Shopping Catalog Item Details Get one item detail  |
| Shopping Catalog Item Details/Buying Options - Get one buying option | Shopping Catalog Item Details/Buying Options Get one buying option  |
| Shopping Catalog Item Details/Item GTIN Relationships - Get one item GTIN relationship | Shopping Catalog Item Details/Item GTIN Relationships Get one item GTIN relationship  |
| Shopping Catalog Item Details/Item Images - Get one item image | Shopping Catalog Item Details/Item Images Get one item image  |
| Shopping Catalog Item Details/Item Manufacturers - Get one item manufacturer relationship | Shopping Catalog Item Details/Item Manufacturers Get one item manufacturer relationship  |
| Shopping Catalog Item Details/Item Suppliers - Get one item supplier relationship | Shopping Catalog Item Details/Item Suppliers Get one item supplier relationship  |
| Shopping Search - Get one searched item | Shopping Search Get one searched item  |
| Shopping Search/Brand Filters - Get one brand | Shopping Search/Brand Filters Get one brand  |
| Shopping Search/Result Items - Get one catalog item | Shopping Search/Result Items Get one catalog item  |
| Shopping Search/Result Punchout Catalogs - Get one punchout catalog | Shopping Search/Result Punchout Catalogs Get one punchout catalog  |
| Supplier Eligibility - Get one supplier eligibility record | Supplier Eligibility Get one supplier eligibility record  |
| Supplier Initiatives - Get one initiative | Supplier Initiatives Get one initiative  |
| Supplier Initiatives/Attachments - GET action not supported | Supplier Initiatives/Attachments GET action not supported  |
| Supplier Initiatives/Evaluation Team Members - Get one team member | Supplier Initiatives/Evaluation Team Members Get one team member  |
| Supplier Initiatives/Flexfields for Initiatives - GET action not supported | Supplier Initiatives/Flexfields for Initiatives GET action not supported  |
| Supplier Initiatives/Qualification Areas - Get one qualification area | Supplier Initiatives/Qualification Areas Get one qualification area  |
| Supplier Initiatives/Suppliers - Get one supplier | Supplier Initiatives/Suppliers Get one supplier  |
| Supplier Negotiations - Get one negotiation | Supplier Negotiations Get one negotiation  |
| Supplier Negotiations List of Values - Get one negotiation | Supplier Negotiations List of Values Get one negotiation  |
| Supplier Negotiations/Abstracts - GET action not supported | Supplier Negotiations/Abstracts GET action not supported  |
| Supplier Negotiations/Abstracts/Flexfields for Abstracts - GET action not supported | Supplier Negotiations/Abstracts/Flexfields for Abstracts GET action not supported  |
| Supplier Negotiations/Attachments - GET action not supported | Supplier Negotiations/Attachments GET action not supported  |
| Supplier Negotiations/Collaboration Team Members - Get one team member | Supplier Negotiations/Collaboration Team Members Get one team member  |
| Supplier Negotiations/Flexfields for Abstracts - GET action not supported | Supplier Negotiations/Flexfields for Abstracts GET action not supported  |
| Supplier Negotiations/Lines - Get one negotiation line | Supplier Negotiations/Lines Get one negotiation line  |
| Supplier Negotiations/Lines/Attachments - GET action not supported | Supplier Negotiations/Lines/Attachments GET action not supported  |
| Supplier Negotiations/Lines/Attribute Groups - Get one group | Supplier Negotiations/Lines/Attribute Groups Get one group  |
| Supplier Negotiations/Lines/Attribute Groups/Line Attributes - Get one attribute | Supplier Negotiations/Lines/Attribute Groups/Line Attributes Get one attribute  |
| Supplier Negotiations/Lines/Attribute Groups/Line Attributes/Response Values - Get one value | Supplier Negotiations/Lines/Attribute Groups/Line Attributes/Response Values Get one value  |
| Supplier Negotiations/Lines/External Cost Factors - Get one cost factor | Supplier Negotiations/Lines/External Cost Factors Get one cost factor  |
| Supplier Negotiations/Lines/Flexfields for Abstracts - GET action not supported | Supplier Negotiations/Lines/Flexfields for Abstracts GET action not supported  |
| Supplier Negotiations/Lines/Price Breaks - Get one price break | Supplier Negotiations/Lines/Price Breaks Get one price break  |
| Supplier Negotiations/Lines/Price Tiers - Get one price tier | Supplier Negotiations/Lines/Price Tiers Get one price tier  |
| Supplier Negotiations/Response Currencies - Get one currency | Supplier Negotiations/Response Currencies Get one currency  |
| Supplier Negotiations/Scoring Teams - Get one team | Supplier Negotiations/Scoring Teams Get one team  |
| Supplier Negotiations/Scoring Teams/Scoring Team Members - Get one team member | Supplier Negotiations/Scoring Teams/Scoring Team Members Get one team member  |
| Supplier Negotiations/Sections - Get one section | Supplier Negotiations/Sections Get one section  |
| Supplier Negotiations/Sections/Requirements - Get one requirement | Supplier Negotiations/Sections/Requirements Get one requirement  |
| Supplier Negotiations/Sections/Requirements/Attachments - GET action not supported | Supplier Negotiations/Sections/Requirements/Attachments GET action not supported  |
| Supplier Negotiations/Sections/Requirements/Response Values - Get one value | Supplier Negotiations/Sections/Requirements/Response Values Get one value  |
| Supplier Negotiations/Supplier Flexfields - GET action not supported | Supplier Negotiations/Supplier Flexfields GET action not supported  |
| Supplier Negotiations/Suppliers - Get one supplier | Supplier Negotiations/Suppliers Get one supplier  |
| Supplier Negotiations/Suppliers/Access Restrictions - Get one restriction | Supplier Negotiations/Suppliers/Access Restrictions Get one restriction  |
| Supplier Qualification Areas - Get one area | Supplier Qualification Areas Get one area  |
| Supplier Qualification Areas/Attachments - Get one attachment | Supplier Qualification Areas/Attachments Get one attachment  |
| Supplier Qualification Areas/Business Units - Get one business unit | Supplier Qualification Areas/Business Units Get one business unit  |
| Supplier Qualification Areas/Outcomes - Get one outcome | Supplier Qualification Areas/Outcomes Get one outcome  |
| Supplier Qualification Areas/Questions - Get one question | Supplier Qualification Areas/Questions Get one question  |
| Supplier Qualification Question Responses - Get one response | Supplier Qualification Question Responses Get one response  |
| Supplier Qualification Question Responses/Values - Get one response value | Supplier Qualification Question Responses/Values Get one response value  |
| Supplier Qualification Question Responses/Values/Attachments - GET action not supported | Supplier Qualification Question Responses/Values/Attachments GET action not supported  |
| Supplier Qualification Questions - Get one question | Supplier Qualification Questions Get one question  |
| Supplier Qualification Questions/Acceptable Responses - Get one acceptable response | Supplier Qualification Questions/Acceptable Responses Get one acceptable response  |
| Supplier Qualification Questions/Acceptable Responses/Branches - Get one question branch | Supplier Qualification Questions/Acceptable Responses/Branches Get one question branch  |
| Supplier Qualification Questions/Attachments - Get one attachment | Supplier Qualification Questions/Attachments Get one attachment  |
| Supplier Qualification Questions/Scores - Get one question score | Supplier Qualification Questions/Scores Get one question score  |
| Suppliers - Get one supplier | Suppliers Get one supplier  |
| Suppliers List of Values - Get one supplier | Suppliers List of Values Get one supplier  |
| Suppliers List of Values/Supplier Sites List of Values - Get one site | Suppliers List of Values/Supplier Sites List of Values Get one site  |
| Suppliers/Addresses - Get one address | Suppliers/Addresses Get one address  |
| Suppliers/Addresses/Flexfields for Suppliers - GET action not supported | Suppliers/Addresses/Flexfields for Suppliers GET action not supported  |
| Suppliers/Attachments - GET action not supported | Suppliers/Attachments GET action not supported  |
| Suppliers/Business Classifications - Get one classification | Suppliers/Business Classifications Get one classification  |
| Suppliers/Business Classifications/Attachments - GET action not supported | Suppliers/Business Classifications/Attachments GET action not supported  |
| Suppliers/Contacts - Get one contact | Suppliers/Contacts Get one contact  |
| Suppliers/Contacts/Addresses - Get one address | Suppliers/Contacts/Addresses Get one address  |
| Suppliers/Contacts/Flexfields for Suppliers - GET action not supported | Suppliers/Contacts/Flexfields for Suppliers GET action not supported  |
| Suppliers/Flexfields for Suppliers - GET action not supported | Suppliers/Flexfields for Suppliers GET action not supported  |
| Suppliers/Global Flexfields for Suppliers - GET action not supported | Suppliers/Global Flexfields for Suppliers GET action not supported  |
| Suppliers/Products and Services - Get one product and service | Suppliers/Products and Services Get one product and service  |
| Suppliers/Sites - Get one site | Suppliers/Sites Get one site  |
| Suppliers/Sites/Attachments - GET action not supported | Suppliers/Sites/Attachments GET action not supported  |
| Suppliers/Sites/Flexfields for Suppliers - GET action not supported | Suppliers/Sites/Flexfields for Suppliers GET action not supported  |
| Suppliers/Sites/Global Flexfields for Suppliers - GET action not supported | Suppliers/Sites/Global Flexfields for Suppliers GET action not supported  |
| Suppliers/Sites/Site Assignments - Get one assignment | Suppliers/Sites/Site Assignments Get one assignment  |
| Suppliers/Sites/Third-Party Payment Relationships - Get one relationship | Suppliers/Sites/Third-Party Payment Relationships Get one relationship  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Catalog Category Hierarchy Nodes - Get all nodes | Catalog Category Hierarchy Nodes Get all nodes  |
| Draft Purchase Orders - Get all draft purchase orders | Draft Purchase Orders Get all draft purchase orders  |
| Draft Purchase Orders/Attachments for Draft Purchase Orders - Get all attachments | Draft Purchase Orders/Attachments for Draft Purchase Orders Get all attachments  |
| Draft Purchase Orders/Flexfields for Draft Purchase Orders - Get all descriptive flexfields | Draft Purchase Orders/Flexfields for Draft Purchase Orders Get all descriptive flexfields  |
| Draft Purchase Orders/Lines - Get all lines | Draft Purchase Orders/Lines Get all lines  |
| Draft Purchase Orders/Lines/Attachments for Draft Purchase Orders - Get all attachments | Draft Purchase Orders/Lines/Attachments for Draft Purchase Orders Get all attachments  |
| Draft Purchase Orders/Lines/Flexfields for Draft Purchase Orders - Get all descriptive flexfields | Draft Purchase Orders/Lines/Flexfields for Draft Purchase Orders Get all descriptive flexfields  |
| Draft Purchase Orders/Lines/Schedules - Get all schedules | Draft Purchase Orders/Lines/Schedules Get all schedules  |
| Draft Purchase Orders/Lines/Schedules/Attachments for Draft Purchase Orders - Get all attachments | Draft Purchase Orders/Lines/Schedules/Attachments for Draft Purchase Orders Get all attachments  |
| Draft Purchase Orders/Lines/Schedules/Distributions - Get all distributions | Draft Purchase Orders/Lines/Schedules/Distributions Get all distributions  |
| Draft Purchase Orders/Lines/Schedules/Distributions/Flexfields for Draft Purchase Orders - Get all descriptive flexfields | Draft Purchase Orders/Lines/Schedules/Distributions/Flexfields for Draft Purchase Orders Get all descriptive flexfields  |
| Draft Purchase Orders/Lines/Schedules/Distributions/Project Costing Flexfields for Distributions - Get all project costing flexfields | Draft Purchase Orders/Lines/Schedules/Distributions/Project Costing Flexfields for Distributions Get all project costing flexfields  |
| Draft Purchase Orders/Lines/Schedules/Flexfields for Draft Purchase Orders - Get all descriptive flexfields | Draft Purchase Orders/Lines/Schedules/Flexfields for Draft Purchase Orders Get all descriptive flexfields  |
| Negotiation Statuses List of Values - Get all negotiation statuses | Negotiation Statuses List of Values Get all negotiation statuses  |
| Procurement Agents - Get all procurement agents | Procurement Agents Get all procurement agents  |
| Project Task Completion Events List Of Values - Get all events | Project Task Completion Events List Of Values Get all events  |
| Public Shopping List Lines - Get all lines | Public Shopping List Lines Get all lines  |
| Punchout Connections - Get all redirection URLs | Punchout Connections Get all redirection URLs  |
| Purchase Order Schedules - Get all schedules | Purchase Order Schedules Get all schedules  |
| Purchase Orders - Get all purchase orders | Purchase Orders Get all purchase orders  |
| Purchase Orders List of Values - Get all purchase orders | Purchase Orders List of Values Get all purchase orders  |
| Purchase Orders/Attachments for Purchase Orders - Get all attachments | Purchase Orders/Attachments for Purchase Orders Get all attachments  |
| Purchase Orders/Flexfields for Purchase Orders - Get all descriptive flexfields | Purchase Orders/Flexfields for Purchase Orders Get all descriptive flexfields  |
| Purchase Orders/Lines - Get all lines | Purchase Orders/Lines Get all lines  |
| Purchase Orders/Lines/Attachments for Purchase Orders - Get all attachments | Purchase Orders/Lines/Attachments for Purchase Orders Get all attachments  |
| Purchase Orders/Lines/Flexfields for Purchase Orders - Get all descriptive flexfields | Purchase Orders/Lines/Flexfields for Purchase Orders Get all descriptive flexfields  |
| Purchase Orders/Lines/Schedules - Get all schedules | Purchase Orders/Lines/Schedules Get all schedules  |
| Purchase Orders/Lines/Schedules/Attachments for Purchase Orders - Get all attachments | Purchase Orders/Lines/Schedules/Attachments for Purchase Orders Get all attachments  |
| Purchase Orders/Lines/Schedules/Distributions - Get all distributions | Purchase Orders/Lines/Schedules/Distributions Get all distributions  |
| Purchase Orders/Lines/Schedules/Distributions/Flexfields for Purchase Orders - Get all descriptive flexfields | Purchase Orders/Lines/Schedules/Distributions/Flexfields for Purchase Orders Get all descriptive flexfields  |
| Purchase Orders/Lines/Schedules/Distributions/Project Costing Flexfields for Distributions - Get all project costing flexfields | Purchase Orders/Lines/Schedules/Distributions/Project Costing Flexfields for Distributions Get all project costing flexfields  |
| Purchase Orders/Lines/Schedules/Flexfields for Purchase Orders - Get all descriptive flexfields | Purchase Orders/Lines/Schedules/Flexfields for Purchase Orders Get all descriptive flexfields  |
| Purchase Requisitions - Get all headers | Purchase Requisitions Get all headers  |
| Purchase Requisitions/Flexfields for Purchase Requisitions - Get all descriptive flexfields | Purchase Requisitions/Flexfields for Purchase Requisitions Get all descriptive flexfields  |
| Purchase Requisitions/Lines - Get all lines | Purchase Requisitions/Lines Get all lines  |
| Purchase Requisitions/Lines/Distributions - Get all distributions | Purchase Requisitions/Lines/Distributions Get all distributions  |
| Purchase Requisitions/Lines/Distributions/Flexfields for Purchase Requisitions - Get all descriptive flexfields | Purchase Requisitions/Lines/Distributions/Flexfields for Purchase Requisitions Get all descriptive flexfields  |
| Purchase Requisitions/Lines/Distributions/Project Costing Flexfields for Purchase Requisition Distributions - Get all project costing flexfields | Purchase Requisitions/Lines/Distributions/Project Costing Flexfields for Purchase Requisition Distributions Get all project costing flexfields  |
| Purchase Requisitions/Lines/Flexfields for Purchase Requisitions - Get all descriptive flexfields | Purchase Requisitions/Lines/Flexfields for Purchase Requisitions Get all descriptive flexfields  |
| Purchase Requisitions/Summary Attributes for Purchase Requisitions - Get all summary attributes | Purchase Requisitions/Summary Attributes for Purchase Requisitions Get all summary attributes  |
| Purchasing News - Get all purchasing news items | Purchasing News Get all purchasing news items  |
| Recent Requisitions - Get all recent requisitions for the user | Recent Requisitions Get all recent requisitions for the user  |
| Requisition One-Time Locations - Get all locations | Requisition One-Time Locations Get all locations  |
| Requisition Preferences - Get all preferences | Requisition Preferences Get all preferences  |
| Requisition Preferences/Favorite Charge Accounts - Get all favorite charge accounts | Requisition Preferences/Favorite Charge Accounts Get all favorite charge accounts  |
| Requisition Preferences/Project Costing Flexfields for Requisition Preferences - Get all project costing flexfields | Requisition Preferences/Project Costing Flexfields for Requisition Preferences Get all project costing flexfields  |
| Requisition Preferences/Requisitioning Options - Get all requisitioning options | Requisition Preferences/Requisitioning Options Get all requisitioning options  |
| Requisition Product Details - Get all item details | Requisition Product Details Get all item details  |
| Requisition Product Details/Price Breaks - Get all item price breaks | Requisition Product Details/Price Breaks Get all item price breaks  |
| Shopping Catalog Item Details - Get all item details | Shopping Catalog Item Details Get all item details  |
| Shopping Catalog Item Details/Buying Options - Get all buying options | Shopping Catalog Item Details/Buying Options Get all buying options  |
| Shopping Catalog Item Details/Item GTIN Relationships - Get all item GTIN relationships | Shopping Catalog Item Details/Item GTIN Relationships Get all item GTIN relationships  |
| Shopping Catalog Item Details/Item Images - Get all item images | Shopping Catalog Item Details/Item Images Get all item images  |
| Shopping Catalog Item Details/Item Manufacturers - Get all item manufacturer relationships | Shopping Catalog Item Details/Item Manufacturers Get all item manufacturer relationships  |
| Shopping Catalog Item Details/Item Suppliers - Get all item supplier relationships | Shopping Catalog Item Details/Item Suppliers Get all item supplier relationships  |
| Shopping Search - Get all search items | Shopping Search Get all search items  |
| Shopping Search/Brand Filters - Get all brands | Shopping Search/Brand Filters Get all brands  |
| Shopping Search/Result Items - Get all catalog items | Shopping Search/Result Items Get all catalog items  |
| Shopping Search/Result Punchout Catalogs - Get all punchout catalogs | Shopping Search/Result Punchout Catalogs Get all punchout catalogs  |
| Supplier Eligibility - Get all supplier eligibility records | Supplier Eligibility Get all supplier eligibility records  |
| Supplier Initiatives - Get all initiatives | Supplier Initiatives Get all initiatives  |
| Supplier Initiatives/Attachments - Get all attachments | Supplier Initiatives/Attachments Get all attachments  |
| Supplier Initiatives/Evaluation Team Members - Get all team members | Supplier Initiatives/Evaluation Team Members Get all team members  |
| Supplier Initiatives/Flexfields for Initiatives - Get all flexfields | Supplier Initiatives/Flexfields for Initiatives Get all flexfields  |
| Supplier Initiatives/Qualification Areas - Get all qualification areas | Supplier Initiatives/Qualification Areas Get all qualification areas  |
| Supplier Initiatives/Suppliers - Get all suppliers | Supplier Initiatives/Suppliers Get all suppliers  |
| Supplier Negotiations - Get all negotiations | Supplier Negotiations Get all negotiations  |
| Supplier Negotiations List of Values - Get all negotiations | Supplier Negotiations List of Values Get all negotiations  |
| Supplier Negotiations/Abstracts - Get abstract | Supplier Negotiations/Abstracts Get abstract  |
| Supplier Negotiations/Abstracts/Flexfields for Abstracts - Get all flexfields | Supplier Negotiations/Abstracts/Flexfields for Abstracts Get all flexfields  |
| Supplier Negotiations/Attachments - Get all attachments | Supplier Negotiations/Attachments Get all attachments  |
| Supplier Negotiations/Collaboration Team Members - Get all team members | Supplier Negotiations/Collaboration Team Members Get all team members  |
| Supplier Negotiations/Flexfields for Abstracts - Get all flexfields | Supplier Negotiations/Flexfields for Abstracts Get all flexfields  |
| Supplier Negotiations/Lines - Get all negotiation lines | Supplier Negotiations/Lines Get all negotiation lines  |
| Supplier Negotiations/Lines/Attachments - Get all attachments | Supplier Negotiations/Lines/Attachments Get all attachments  |
| Supplier Negotiations/Lines/Attribute Groups - Get all groups | Supplier Negotiations/Lines/Attribute Groups Get all groups  |
| Supplier Negotiations/Lines/Attribute Groups/Line Attributes - Get all attributes | Supplier Negotiations/Lines/Attribute Groups/Line Attributes Get all attributes  |
| Supplier Negotiations/Lines/Attribute Groups/Line Attributes/Response Values - Get all values | Supplier Negotiations/Lines/Attribute Groups/Line Attributes/Response Values Get all values  |
| Supplier Negotiations/Lines/External Cost Factors - Get all cost factors | Supplier Negotiations/Lines/External Cost Factors Get all cost factors  |
| Supplier Negotiations/Lines/Flexfields for Abstracts - Get all flexfields | Supplier Negotiations/Lines/Flexfields for Abstracts Get all flexfields  |
| Supplier Negotiations/Lines/Price Breaks - Get all price breaks | Supplier Negotiations/Lines/Price Breaks Get all price breaks  |
| Supplier Negotiations/Lines/Price Tiers - Get all price tiers | Supplier Negotiations/Lines/Price Tiers Get all price tiers  |
| Supplier Negotiations/Response Currencies - Get all currencies | Supplier Negotiations/Response Currencies Get all currencies  |
| Supplier Negotiations/Scoring Teams - Get all teams | Supplier Negotiations/Scoring Teams Get all teams  |
| Supplier Negotiations/Scoring Teams/Scoring Team Members - Get all team members | Supplier Negotiations/Scoring Teams/Scoring Team Members Get all team members  |
| Supplier Negotiations/Sections - Get all sections | Supplier Negotiations/Sections Get all sections  |
| Supplier Negotiations/Sections/Requirements - Get all requirements | Supplier Negotiations/Sections/Requirements Get all requirements  |
| Supplier Negotiations/Sections/Requirements/Attachments - Get all attachments | Supplier Negotiations/Sections/Requirements/Attachments Get all attachments  |
| Supplier Negotiations/Sections/Requirements/Response Values - Get all values | Supplier Negotiations/Sections/Requirements/Response Values Get all values  |
| Supplier Negotiations/Supplier Flexfields - Get all supplier flexfields | Supplier Negotiations/Supplier Flexfields Get all supplier flexfields  |
| Supplier Negotiations/Suppliers - Get all suppliers | Supplier Negotiations/Suppliers Get all suppliers  |
| Supplier Negotiations/Suppliers/Access Restrictions - Get all restrictions | Supplier Negotiations/Suppliers/Access Restrictions Get all restrictions  |
| Supplier Qualification Areas - Get all areas | Supplier Qualification Areas Get all areas  |
| Supplier Qualification Areas/Attachments - Get all attachments | Supplier Qualification Areas/Attachments Get all attachments  |
| Supplier Qualification Areas/Business Units - Get all business units | Supplier Qualification Areas/Business Units Get all business units  |
| Supplier Qualification Areas/Outcomes - Get all outcomes | Supplier Qualification Areas/Outcomes Get all outcomes  |
| Supplier Qualification Areas/Questions - Get all questions | Supplier Qualification Areas/Questions Get all questions  |
| Supplier Qualification Question Responses - Get all responses | Supplier Qualification Question Responses Get all responses  |
| Supplier Qualification Question Responses/Values - Get all response values | Supplier Qualification Question Responses/Values Get all response values  |
| Supplier Qualification Question Responses/Values/Attachments - Get all response attachments | Supplier Qualification Question Responses/Values/Attachments Get all response attachments  |
| Supplier Qualification Questions - Get all questions | Supplier Qualification Questions Get all questions  |
| Supplier Qualification Questions/Acceptable Responses - Get all acceptable responses | Supplier Qualification Questions/Acceptable Responses Get all acceptable responses  |
| Supplier Qualification Questions/Acceptable Responses/Branches - Get all question branches | Supplier Qualification Questions/Acceptable Responses/Branches Get all question branches  |
| Supplier Qualification Questions/Attachments - Get all question attachments | Supplier Qualification Questions/Attachments Get all question attachments  |
| Supplier Qualification Questions/Scores - Get all question scores | Supplier Qualification Questions/Scores Get all question scores  |
| Suppliers - Get all suppliers | Suppliers Get all suppliers  |
| Suppliers List of Values - Get all suppliers | Suppliers List of Values Get all suppliers  |
| Suppliers List of Values/Supplier Sites List of Values - Get all sites | Suppliers List of Values/Supplier Sites List of Values Get all sites  |
| Suppliers/Addresses - Get all addresses | Suppliers/Addresses Get all addresses  |
| Suppliers/Addresses/Flexfields for Suppliers - Get all flexfields | Suppliers/Addresses/Flexfields for Suppliers Get all flexfields  |
| Suppliers/Attachments - Get all attachments | Suppliers/Attachments Get all attachments  |
| Suppliers/Business Classifications - Get all classifications | Suppliers/Business Classifications Get all classifications  |
| Suppliers/Business Classifications/Attachments - Get all attachments | Suppliers/Business Classifications/Attachments Get all attachments  |
| Suppliers/Contacts - Get all contacts | Suppliers/Contacts Get all contacts  |
| Suppliers/Contacts/Addresses - Get all addresses | Suppliers/Contacts/Addresses Get all addresses  |
| Suppliers/Contacts/Flexfields for Suppliers - Get all flexfields | Suppliers/Contacts/Flexfields for Suppliers Get all flexfields  |
| Suppliers/Flexfields for Suppliers - Get all flexfields | Suppliers/Flexfields for Suppliers Get all flexfields  |
| Suppliers/Global Flexfields for Suppliers - Get all flexfields | Suppliers/Global Flexfields for Suppliers Get all flexfields  |
| Suppliers/Products and Services - Get all products and services | Suppliers/Products and Services Get all products and services  |
| Suppliers/Sites - Get all sites | Suppliers/Sites Get all sites  |
| Suppliers/Sites/Attachments - Get all attachments | Suppliers/Sites/Attachments Get all attachments  |
| Suppliers/Sites/Flexfields for Suppliers - Get all flexfields | Suppliers/Sites/Flexfields for Suppliers Get all flexfields  |
| Suppliers/Sites/Global Flexfields for Suppliers - Get all flexfields | Suppliers/Sites/Global Flexfields for Suppliers Get all flexfields  |
| Suppliers/Sites/Site Assignments - Get all assignments | Suppliers/Sites/Site Assignments Get all assignments  |
| Suppliers/Sites/Third-Party Payment Relationships - Get all relationships | Suppliers/Sites/Third-Party Payment Relationships Get all relationships  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Draft Purchase Orders - Cancel a change order (POST) | Draft Purchase Orders Cancels the change order that is no longer required. This action is supported for all change order creation methods. This action will roll down to child levels. If the cancel action is performed on a change order, it will cancel the change done on a purchase order line, schedule, and distribution. Cancel a change order  |
| Draft Purchase Orders - Submit a draft purchase order (POST) | Draft Purchase Orders Submits the draft purchase order document for approval. The purchase order may be created by any purchase order creation method. Submit a draft purchase order  |
| Draft Purchase Orders - Update a header (PATCH) | Draft Purchase Orders Update a header  |
| Draft Purchase Orders/Attachments for Draft Purchase Orders - Update an attachment (PATCH) | Draft Purchase Orders/Attachments for Draft Purchase Orders Update an attachment  |
| Draft Purchase Orders/Flexfields for Draft Purchase Orders - Update a descriptive flexfield (PATCH) | Draft Purchase Orders/Flexfields for Draft Purchase Orders Update a descriptive flexfield  |
| Draft Purchase Orders/Lines - Update a line (PATCH) | Draft Purchase Orders/Lines Update a line  |
| Draft Purchase Orders/Lines/Attachments for Draft Purchase Orders - Update an attachment (PATCH) | Draft Purchase Orders/Lines/Attachments for Draft Purchase Orders Update an attachment  |
| Draft Purchase Orders/Lines/Flexfields for Draft Purchase Orders - Update a descriptive flexfield (PATCH) | Draft Purchase Orders/Lines/Flexfields for Draft Purchase Orders Update a descriptive flexfield  |
| Draft Purchase Orders/Lines/Schedules - Update a schedule (PATCH) | Draft Purchase Orders/Lines/Schedules Update a schedule  |
| Draft Purchase Orders/Lines/Schedules/Attachments for Draft Purchase Orders - Update an attachment (PATCH) | Draft Purchase Orders/Lines/Schedules/Attachments for Draft Purchase Orders Update an attachment  |
| Draft Purchase Orders/Lines/Schedules/Distributions - Update a distribution (PATCH) | Draft Purchase Orders/Lines/Schedules/Distributions Update a distribution  |
| Draft Purchase Orders/Lines/Schedules/Distributions/Flexfields for Draft Purchase Orders - Update a descriptive flexfield (PATCH) | Draft Purchase Orders/Lines/Schedules/Distributions/Flexfields for Draft Purchase Orders Update a descriptive flexfield  |
| Draft Purchase Orders/Lines/Schedules/Distributions/Project Costing Flexfields for Distributions - Update a project costing flexfield (PATCH) | Draft Purchase Orders/Lines/Schedules/Distributions/Project Costing Flexfields for Distributions Update a project costing flexfield  |
| Draft Purchase Orders/Lines/Schedules/Flexfields for Draft Purchase Orders - Update a descriptive flexfield (PATCH) | Draft Purchase Orders/Lines/Schedules/Flexfields for Draft Purchase Orders Update a descriptive flexfield  |
| Procurement Agents - Update one procurement agent (PATCH) | Procurement Agents Update one procurement agent  |
| Public Shopping List Lines - Update a line (PATCH) | Public Shopping List Lines Update a line  |
| Purchase Order Schedules - Close a schedule (POST) | Purchase Order Schedules Closes a purchase order schedule. You can manually close for receiving, close for invoicing, close, or finally close a schedule. Close a schedule  |
| Purchase Orders - Cancel a purchase order (POST) | Purchase Orders Cancels the purchase order that is no longer required. This action is supported for all purchase order creation methods. This action will roll down to child levels. If the cancel action is performed on a purchase order, it will cancel the purchase order lines and schedules. Cancel a purchase order  |
| Purchase Orders - Close a purchase order (POST) | Purchase Orders Closes the purchase order according to the close action. Close actions include close for receiving, close for invoicing, close, and finally close.<br>When manually closing, a check for allowable close statuses will be performed at the purchase order level. Close actions will roll down to child levels. If a manual close action is taken at the purchase order level, it will set the status at the purchase order line and schedule level. Close actions will also roll up to higher levels. Close a purchase order  |
| Purchase Orders - Recommunicate a purchase order (POST) | Purchase Orders Recommunicates a purchase order through the Oracle Business-to-Business channel. <br><br>You can send purchase orders electronically to suppliers using business-to-business messages. Sometimes, these messages are not received or cannot be processed by the supplier. In such a situation, you can use REST API to recommunicate your orders. Recommunicate a purchase order  |
| Purchase Orders - Renumber a purchase order (POST) | Purchase Orders Renumbers an existing purchase order that was created with incorrect data. This action is supported for all purchase order creation methods. Renumber a purchase order  |
| Purchase Orders - Resubmit a purchase order for approval (POST) | Purchase Orders Resubmits a purchase order or change order for approval. <br><br>Typically, your purchase orders go through an approval process and are implemented without any issues. If you encounter unexpected errors during order processing (for example, during submission check), you need to resubmit the order. You can use REST API to resubmit purchase orders in the Pending Approval status. Orders in the Pending Approval status will be automatically withdrawn before being resubmitted. Set the bypassApprovalsFlag attribute to True to bypass the approval workflow. Resubmit a purchase order for approval  |
| Purchase Orders/Lines - Close a line (POST) | Purchase Orders/Lines Closes the purchase order line according to the close action. Close actions include close for receiving, close for invoicing, close, and finally close.<br>When manually closing, a check for allowable close statuses will be performed at the line level. Close actions will roll down to child levels. If a manual close action is taken at the purchase order line level, it will set the status at the purchase order schedule level. Close actions will also roll up to higher levels. When all lines for a given purchase order are closed, the purchase order is closed. Close a line  |
| Purchase Orders/Lines/Schedules - Close a schedule (POST) | Purchase Orders/Lines/Schedules Closes the purchase order schedule according to the close action. Close actions include close for receiving, close for invoicing, close, and finally close. <br>When manually closing, a check for allowable close statuses will be performed at the schedule level. Close actions will roll up to higher levels. Once all schedules for a given line are closed, the line is closed. Close a schedule  |
| Purchase Requisitions - Calculate tax and derive charge account (POST) | Purchase Requisitions Calculates the recoverable and non-recoverable tax and derives the charge account for a requisition. Calculate tax and derive charge account  |
| Purchase Requisitions - Cancel a requisition (POST) | Purchase Requisitions Cancels a purchase requisition, which is not associated with a purchase order, a transfer order, or a negotiation reference, or is not being modified by the buyer. Cancel a requisition  |
| Purchase Requisitions - Create default distributions (POST) | Purchase Requisitions Creates a basic distribution comprising of the distribution number, quantity, or amount attributes. A distribution is created for a requisition line if it is not already created. Create default distributions  |
| Purchase Requisitions - Submit a requisition for approval (POST) | Purchase Requisitions Submits a purchase requisition for approval. The approvals workflow is bypassed if the requisition is externally managed or if the requisition contains approver details. Submit a requisition for approval  |
| Purchase Requisitions - Update a header (PATCH) | Purchase Requisitions Update a header  |
| Purchase Requisitions - Update header level attributes (POST) | Purchase Requisitions Updates the header level attributes of a requisition. The updated values are displayed for all the requisition lines. Update header level attributes  |
| Purchase Requisitions/Flexfields for Purchase Requisitions - Update a descriptive flexfield (PATCH) | Purchase Requisitions/Flexfields for Purchase Requisitions Update a descriptive flexfield  |
| Purchase Requisitions/Lines - Calculate tax using existing line data (POST) | Purchase Requisitions/Lines Calculates the tax for the line based on the tax setup. The updated tax values are displayed for the distributions present for the line. Calculate tax using existing line data  |
| Purchase Requisitions/Lines - Cancel a requisition line (POST) | Purchase Requisitions/Lines Cancels a requisition line, which is not associated with a purchase order, a transfer order, or a negotiation reference, or is not being modified by the buyer. Cancel a requisition line  |
| Purchase Requisitions/Lines - Derive a charge account (POST) | Purchase Requisitions/Lines Derives the charge account according to the line and distribution level attributes. Derive a charge account  |
| Purchase Requisitions/Lines - Update a line (PATCH) | Purchase Requisitions/Lines Update a line  |
| Purchase Requisitions/Lines/Distributions - Update a distribution (PATCH) | Purchase Requisitions/Lines/Distributions Update a distribution  |
| Purchase Requisitions/Lines/Distributions/Flexfields for Purchase Requisitions - Update a descriptive flexfield (PATCH) | Purchase Requisitions/Lines/Distributions/Flexfields for Purchase Requisitions Update a descriptive flexfield  |
| Purchase Requisitions/Lines/Distributions/Project Costing Flexfields for Purchase Requisition Distributions - Update a project costing flexfield (PATCH) | Purchase Requisitions/Lines/Distributions/Project Costing Flexfields for Purchase Requisition Distributions Update a project costing flexfield  |
| Purchase Requisitions/Lines/Flexfields for Purchase Requisitions - Update a descriptive flexfield (PATCH) | Purchase Requisitions/Lines/Flexfields for Purchase Requisitions Update a descriptive flexfield  |
| Requisition One-Time Locations - Update a location (PATCH) | Requisition One-Time Locations Update a location  |
| Supplier Eligibility - Update one supplier eligibility record (PATCH) | Supplier Eligibility Update one supplier eligibility record  |
| Supplier Initiatives - Launch an initiative (POST) | Supplier Initiatives Launches an initiative and sends questionnaires to suppliers and internal responders for their responses. Launch an initiative  |
| Supplier Negotiations - Update a negotiation (PATCH) | Supplier Negotiations Update a negotiation  |
| Supplier Negotiations/Attachments - Update an attachment (PATCH) | Supplier Negotiations/Attachments Update an attachment  |
| Supplier Negotiations/Collaboration Team Members - Update a team member (PATCH) | Supplier Negotiations/Collaboration Team Members Update a team member  |
| Supplier Negotiations/Flexfields for Abstracts - Update a flexfield (PATCH) | Supplier Negotiations/Flexfields for Abstracts Update a flexfield  |
| Supplier Negotiations/Lines - Update a negotiation line (PATCH) | Supplier Negotiations/Lines Update a negotiation line  |
| Supplier Negotiations/Lines/Attachments - Update an attachment (PATCH) | Supplier Negotiations/Lines/Attachments Update an attachment  |
| Supplier Negotiations/Lines/Attribute Groups - Update a group (PATCH) | Supplier Negotiations/Lines/Attribute Groups Update a group  |
| Supplier Negotiations/Lines/Attribute Groups/Line Attributes - Update an attribute (PATCH) | Supplier Negotiations/Lines/Attribute Groups/Line Attributes Update an attribute  |
| Supplier Negotiations/Lines/Attribute Groups/Line Attributes/Response Values - Update a value (PATCH) | Supplier Negotiations/Lines/Attribute Groups/Line Attributes/Response Values Update a value  |
| Supplier Negotiations/Lines/External Cost Factors - Update a cost factor (PATCH) | Supplier Negotiations/Lines/External Cost Factors Update a cost factor  |
| Supplier Negotiations/Lines/Flexfields for Abstracts - Update a flexfield (PATCH) | Supplier Negotiations/Lines/Flexfields for Abstracts Update a flexfield  |
| Supplier Negotiations/Lines/Price Breaks - Update a price break (PATCH) | Supplier Negotiations/Lines/Price Breaks Update a price break  |
| Supplier Negotiations/Lines/Price Tiers - Update a price tier (PATCH) | Supplier Negotiations/Lines/Price Tiers Update a price tier  |
| Supplier Negotiations/Sections/Requirements/Attachments - Update an attachment (PATCH) | Supplier Negotiations/Sections/Requirements/Attachments Update an attachment  |
| Supplier Negotiations/Supplier Flexfields - Update a supplier flexfield (PATCH) | Supplier Negotiations/Supplier Flexfields Update a supplier flexfield  |
| Supplier Negotiations/Suppliers - Update a supplier (PATCH) | Supplier Negotiations/Suppliers Update a supplier  |
| Supplier Qualification Question Responses - Update a question response (PATCH) | Supplier Qualification Question Responses Update a question response  |
| Supplier Qualification Questions - Update a question (PATCH) | Supplier Qualification Questions Update a question  |
| Supplier Qualification Questions/Acceptable Responses - Update an acceptable response (PATCH) | Supplier Qualification Questions/Acceptable Responses Update an acceptable response  |
| Supplier Qualification Questions/Acceptable Responses/Branches - PATCH action not supported (PATCH) | Supplier Qualification Questions/Acceptable Responses/Branches PATCH action not supported  |
| Supplier Qualification Questions/Scores - Update a question score (PATCH) | Supplier Qualification Questions/Scores Update a question score  |
| Suppliers - Retrieve scores (POST) | Suppliers Retrieves a score associated with a supplier linked to a company tracked by Oracle DataFox. Score is defined within and calculated by Oracle DataFox, which helps organizations rank suppliers, evaluate supplier performances, highlight potential risks, and in general improve supplier profile maintenance and monitoring processes. This action is not currently supported. Retrieve scores  |
| Suppliers - Retrieve signals (POST) | Suppliers Retrieves signals associated with a supplier linked to a company tracked by Oracle DataFox. Signals are made available by Oracle DataFox using an intelligence news feed and provide actionable information through updates on leading indicators such as negative news, financial news, corporate news, and more. This action is not currently supported. Retrieve signals  |
| Suppliers - Update a supplier (PATCH) | Suppliers Update a supplier  |
| Suppliers/Addresses - Update an address (PATCH) | Suppliers/Addresses Update an address  |
| Suppliers/Attachments - Update an attachment (PATCH) | Suppliers/Attachments Update an attachment  |
| Suppliers/Business Classifications - Update a business classification (PATCH) | Suppliers/Business Classifications Update a business classification  |
| Suppliers/Business Classifications/Attachments - Update an attachment (PATCH) | Suppliers/Business Classifications/Attachments Update an attachment  |
| Suppliers/Contacts - Update a contact (PATCH) | Suppliers/Contacts Update a contact  |
| Suppliers/Flexfields for Suppliers - Update a flexfield (PATCH) | Suppliers/Flexfields for Suppliers Update a flexfield  |
| Suppliers/Global Flexfields for Suppliers - Update a flexfield (PATCH) | Suppliers/Global Flexfields for Suppliers Update a flexfield  |
| Suppliers/Sites - Update a site (PATCH) | Suppliers/Sites Update a site  |
| Suppliers/Sites/Attachments - Update an attachment (PATCH) | Suppliers/Sites/Attachments Update an attachment  |
| Suppliers/Sites/Flexfields for Suppliers - Update a flexfield (PATCH) | Suppliers/Sites/Flexfields for Suppliers Update a flexfield  |
| Suppliers/Sites/Global Flexfields for Suppliers - Update a flexfield (PATCH) | Suppliers/Sites/Global Flexfields for Suppliers Update a flexfield  |
| Suppliers/Sites/Site Assignments - Update an assignment (PATCH) | Suppliers/Sites/Site Assignments Update an assignment  |
| Suppliers/Sites/Third-Party Payment Relationships - Update a relationship (PATCH) | Suppliers/Sites/Third-Party Payment Relationships Update a relationship  |


