# AutomationAnywhere API
REST interface to AutomationAnywhere

# Connection Tab
## Connection Fields


#### Control Room URL

The URL for the service server

**Type** - string



#### AutomationAnywhere API REST API Service URL

The URL for the AutomationAnywhere API REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### AuthenticationType

Allows user to select between Password and API Key JWT authentication

**Type** - string

**Default Value** - PASSWORD

##### Allowed Values

 * Password
 * API Key


#### User Name

User name for JWT Authentication. Depending on how your Control Room is configured, a domain might be required with the username. For example: your-domain\\jdoe

**Type** - string



#### Password

Password for JWT password authentication

**Type** - password



#### API Key

API Key for API Key JWT authentication.

**Type** - password

# Operation Tab


## CREATE/POST
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE/PATCH
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE/PUT
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| automations - Deletes automation by id | automations Deletes automation by id Deletes automation by id  |
| queues - Deletes queue by id | queues Deletes queue by id Deletes queue by id  |
| queues - Remove queue consumer. | queues Make the role no longer able to use queue. Remove queue consumer.  |
| queues - Remove queue member. | queues Make the user no longer able to add/remove credentials to the queue or edit the queue settings. Remove queue member.  |
| queues - Remove queue participant. | queues Make the role no longer able to add workItems in the queue. Remove queue participant.  |
| workItemModels - Deletes a workitem model by id | workItemModels Deletes a workitem model by id Deletes a workitem model by id  |
| workitems - Delete workItem by id | workitems Delete specific workItem by id Delete workItem by id  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| automations - Get specific automation in a tenant by id | automations Return specific automation Get specific automation in a tenant by id  |
| queues - All queue's consumers. | queues Return all roles that can use the queue. All queue's consumers.  |
| queues - All queue's members. | queues Return all users that can add/remove credentials to the queue or edit the queue settings. All queue's members.  |
| queues - All queue's participants. | queues Return all roles that can add workItems to the queue. All queue's participants.  |
| queues - Get queue by id | queues Return specific queue Get queue by id  |
| workItemModels - Get specific workitem model by id. | workItemModels Returns workitem model for given id Get specific workitem model by id.  |
| workitems - Get workItem by id | workitems Get specific workItem by id Get workItem by id  |


### PATCH Operation Types
| Label | Help Text |
| --- | --- |


### POST Operation Types
| Label | Help Text |
| --- | --- |
| automations - Create automation | automations Creates an automation Create automation  |
| queues - Add queue consumer. | queues Make the role able to use the queue. Add queue consumer.  |
| queues - Add queue participant. | queues Make the role able to add workItems to the queue. Add queue participant.  |
| queues - Create queue | queues Create new queue Create queue  |
| queues - Delete multiple queues | queues Deletes multiple queues Delete multiple queues  |
| workItemModels - Create workitem model | workItemModels Creates a workitem model Create workitem model  |
| workitems - Creates new work items | workitems Creates new work items Creates new work items  |
| workitems - Creates new work items | workitems Creates new work items Creates new work items  |
| workitems - Delete work items by ids in the body | workitems Delete work item by ids in the body Delete work items by ids in the body  |


### PUT Operation Types
| Label | Help Text |
| --- | --- |
| automations - Updates automation by id (PUT) | automations Updates automation by id Updates automation by id  |
| queues - Add or update a queue member. (PUT) | queues Make the user able to add/remove credentials to the queue or edit the queue settings. Add or update a queue member.  |
| queues - Update queue (PUT) | queues Update existing queue Update queue  |
| workitems - Update workitem data, result and status (PUT) | workitems Update workitem data, result and status Update workitem data, result and status  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| automations - List of automations | automations Return all automations List of automations  |
| queues - list all queues | queues list all queues list all queues  |
| workItemModels - Get all workitem models | workItemModels Returns all workitem models Get all workitem models  |
| workitems - List work items | workitems List work items for a given queue List work items  |


