# The PowerStore Management REST API is a set of resources (objects), operations, and attributes that provide interactive, scripted, and programmatic management control of the PowerStore cluster.
The PowerStore Management REST API is a set of resources (objects), operations, and attributes that provide interactive, scripted, and programmatic management control of the PowerStore cluster.

# Connection Tab
## Connection Fields


#### Dell PowerStore REST API Service URL

The URL for the Dell PowerStore REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default openapi.json file so you can provide a url to any swagger file.

**Type** - string



#### URL

The URL for the Service service

**Type** - string



#### AuthenticationType

Allows user to select between BASIC and OAUTH 2.0 Authentication

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE/EXECUTE


## UPDATE


## GET


## DELETE


## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  (Supported Types:  date, number, string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| appliance - Forecast capacity usage for an appliance. | appliance Forecast capacity usage for an appliance.  | /appliance/{id}/forecast\_\_\_post |
| appliance - Returns information about when an appliance is for... | appliance Returns information about when an appliance is forecast to reach 100% capacity usage.  | /appliance/{id}/time\_to\_full\_\_\_post |
| cluster - Forecast capacity usage for the cluster. | cluster Forecast capacity usage for the cluster.  | /cluster/{id}/forecast\_\_\_post |
| cluster - Returns information about when the cluster is fore... | cluster Returns information about when the cluster is forecast to reach 100% capacity usage.  | /cluster/{id}/time\_to\_full\_\_\_post |
| email_notify_destination - Create | email_notify_destination Add an email address to receive notifications. Create  | /email\_notify\_destination\_\_\_post |
| email_notify_destination - Test | email_notify_destination Send a test email to an email address. Test  | /email\_notify\_destination/{id}/test\_\_\_post |
| file_dns - Create | file_dns Create a new DNS Server configuration for a NAS Server. Only one object can be created per NAS Server. Create  | /file\_dns\_\_\_post |
| file_ftp - Create | file_ftp Create an FTP/SFTP server. Create  | /file\_ftp\_\_\_post |
| file_interface - Create | file_interface Create a file interface. Create  | /file\_interface\_\_\_post |
| file_interface_route - Create | file_interface_route Create and configure a new file interface route. There are 3 route types Subnet, Default, and Host. * The default route establishes a static route to a default gateway. To create a default route, provide only the default gateway IP address. * The host route establishes a static route to a specific host. To create a host route, provide the IP address of the specific host in the destination field, and the gateway. * The subnet route establishes a static route to a particular subnet. To create a subnet route, provide the IP address of the target subnet in the destination, the prefix length for that subnet, and the gateway.  Create  | /file\_interface\_route\_\_\_post |
| file_kerberos - Create | file_kerberos Create a Kerberos configuration. The operation will fail if a Kerberos configuration already exists. Create  | /file\_kerberos\_\_\_post |
| file_kerberos - Upload Keytab File | file_kerberos A keytab file is required for secure NFS service with a Linux or Unix Kerberos Key Distribution Center (KDC). The keytab file can be generated using the KDC server. Upload Keytab File  | /file\_kerberos/{id}/upload\_keytab\_\_\_post |
| file_ldap - Create | file_ldap Create an LDAP service on a NAS Server. Only one LDAP Service object can be created per NAS Server. Create  | /file\_ldap\_\_\_post |
| file_ldap - Upload Certificate | file_ldap Upload Certificate  | /file\_ldap/{id}/upload\_certificate\_\_\_post |
| file_ldap - Upload Config | file_ldap Upload Config  | /file\_ldap/{id}/upload\_config\_\_\_post |
| file_ndmp - Create | file_ndmp Add an NDMP service configuration to a NAS server. Only one NDMP service object can be configured per NAS server. Create  | /file\_ndmp\_\_\_post |
| file_nis - Create | file_nis Create a new NIS Service on a NAS Server. Only one NIS Setting object can be created per NAS Server. Create  | /file\_nis\_\_\_post |
| file_system - Clone | file_system Create a clone of a file system. Clone  | /file\_system/{id}/clone\_\_\_post |
| file_system - Create | file_system Create a file system. Create  | /file\_system\_\_\_post |
| file_system - Refresh | file_system Refresh a snapshot of a file system. The content of the snapshot is replaced with the current content of the parent file system. Refresh  | /file\_system/{id}/refresh\_\_\_post |
| file_system - Refresh Quota | file_system Refresh the actual content of tree and user quotas objects. Refresh Quota  | /file\_system/{id}/refresh\_quota\_\_\_post |
| file_system - Restore | file_system Restore from a snapshot of a file system. Restore  | /file\_system/{id}/restore\_\_\_post |
| file_system - Snapshot | file_system Create a snapshot of a file system. Snapshot  | /file\_system/{id}/snapshot\_\_\_post |
| file_tree_quota - Create | file_tree_quota Create a tree quota instance. Create  | /file\_tree\_quota\_\_\_post |
| file_tree_quota - Refresh | file_tree_quota Refresh the cache with the actual value of the tree quota. Refresh  | /file\_tree\_quota/{id}/refresh\_\_\_post |
| file_user_quota - Create | file_user_quota Create a user quota instance. Create  | /file\_user\_quota\_\_\_post |
| file_user_quota - Refresh | file_user_quota Refresh the cache with the actual value of the user quota. Refresh  | /file\_user\_quota/{id}/refresh\_\_\_post |
| file_virus_checker - Create | file_virus_checker Add a new virus checker setting to a NAS Server. Only one instance can be created per NAS Server. Workflow to enable the virus checker settings on the NAS Server is as follows: \n 1. Create a virus checker instance on NAS Server. 2. Download template virus checker configuration file. 3. Edit the configuration file with virus checker configuration details. 4. Upload the configuration file. 5. Enable the virus checker on the NAS Server.  Create  | /file\_virus\_checker\_\_\_post |
| file_virus_checker - Upload Config File | file_virus_checker Upload a virus checker configuration file containing the virus checker configuration settings. Upload Config File  | /file\_virus\_checker/{id}/upload\_config\_\_\_post |
| hardware - Repurpose a drive | hardware A drive that has been used in a different appliance will be locked for use only in that appliance. This operation will allow a locked drive to be used in the current appliance. All data on the drive will become unrecoverable. It will fail if the drive is not locked to a different appliance. Repurpose a drive  | /hardware/{id}/drive\_repurpose\_\_\_post |
| host - Attach | host Attach host to volume. Attach  | /host/{id}/attach\_\_\_post |
| host - Create | host Add a host. Create  | /host\_\_\_post |
| host - Detach | host Detach host from volume. Detach  | /host/{id}/detach\_\_\_post |
| host_group - Attach | host_group Attach host group to volume. Attach  | /host\_group/{id}/attach\_\_\_post |
| host_group - Create | host_group Create a host group. Create  | /host\_group\_\_\_post |
| host_group - Detach | host_group Detach host group from volume. Detach  | /host\_group/{id}/detach\_\_\_post |
| import_host_system - Create | import_host_system Add an import host system so that it can be mapped to a volume. Before mapping an import host system, ensure that a host agent is installed. Host agents can be installed on Linux, Windows, and ESXi host systems only.  While adding import_host_system if Host is not present a new Host shall be created. If Host is already present, the same Host will be updated with the import_host_system details. Create  | /import\_host\_system\_\_\_post |
| import_host_system - Refresh | import_host_system Refresh the details of a specific import host system. Use this operation when there is a change to the import host or import host volumes. Refresh  | /import\_host\_system/{id}/refresh\_\_\_post |
| import_psgroup - Discover importable storage resources | import_psgroup Discover the importable volumes and snapshot schedules in the PS Group. Discover importable storage resources  | /import\_psgroup/{id}/discover\_\_\_post |
| import_psgroup_volume - Return snapshot schedules | import_psgroup_volume Return the snapshot schedules for a PS Group volume. Return snapshot schedules  | /import\_psgroup\_volume/{id}/import\_snapshot\_schedules\_\_\_post |
| import_session - Cancel | import_session Cancel an active import session. Cancel is allowed when the import is in a Scheduled, Queued, Copy_In_Progress, or Ready_For_Cutover state. After a successful cancellation, the host is mapped to original source volume, all paths are cleaned up, and the import state is Cancelled. The import can be attempted again in the future. In most cases, the Cancel operation gracefully rolls back the import based on the source and host error responses. Use the force option to stop the import job irrespective of whether the storage system or hosts have issues. When the force option is true, the import process tries to reach out to the source and host to gracefully terminate the import. If either are not reachable or if the request fails, the import is terminated without rolling back. Cancel  | /import\_session/{id}/cancel\_\_\_post |
| import_session - Cleanup | import_session Clean up an import session that is in Cleanup_Required state and requires user intervention to revert the source volume to its pre-import state as part of the recovery procedure to restore host IO operations. Cleanup  | /import\_session/{id}/cleanup\_\_\_post |
| import_session - Create | import_session Create a new import session. The source storage system and hosts that access the volumes or consistency groups must be added prior to creating an import session. The volumes or consistency groups must be in a migration-ready state. Create  | /import\_session\_\_\_post |
| import_session - Cutover | import_session Commit an import session that is in a Ready_For_Cutover state. When the import session is created with the automatic_cutover attribute set to false, you must use the Cutover operation to complete the import. Until the cutover is complete, PowerStore forwards IO to the source volume to keep it in sync with all host IOs. You can cancel the import during this state if you want to continue using the source volume. Cutover  | /import\_session/{id}/cutover\_\_\_post |
| import_session - Enable import destination volume | import_session Enable the destination volume of an import session. This action can only be used on an agentless import session that is in the 'Mirror_Enabled' state after the host application using the source volume is brought offline. The host application can be reconfigured to use the destination volume of the import session after enabling the destination volume. To prevent accidental writes to the source volume through the source storage system path due to the incorrect reconfiguration, you can specify the removal of all host mappings of the source volume in the source storage system. Enable import destination volume  | /import\_session/{id}/enable\_destination\_volume\_\_\_post |
| import_session - Pause | import_session Pauses an ongoing import session. When this occurs, the background data copy stops, but IO to the source still occurs. Pause is only supported when the import job is in a in Copy_In_Progress state. You can resume or cancel the paused import. Pause  | /import\_session/{id}/pause\_\_\_post |
| import_session - Resume | import_session Resumes the paused import session. The background data copy continues from where it was stopped. Resume is only applicable when the import in a Paused state. Resume  | /import\_session/{id}/resume\_\_\_post |
| import_session - Start Copy | import_session Start the background data copy operation to import data from the source volume. This action can only be used on an agentless import session that is in the 'Ready_To_Start_Copy' state after the host application is reconfigured and brought online to use the destination volume of the import session. Start Copy  | /import\_session/{id}/start\_copy\_\_\_post |
| import_storage_center - Discover importable resources | import_storage_center Discover the importable volumes and snapshot profiles in the SC array. Discover importable resources  | /import\_storage\_center/{id}/discover\_\_\_post |
| import_storage_center_consistency_group - Return snapshot profiles | import_storage_center_consistency_group Return the snapshot profiles of an SC consistency group. Return snapshot profiles  | /import\_storage\_center\_consistency\_group/{id}/import\_snapshot\_profiles\_\_\_post |
| import_storage_center_volume - Return snapshot profiles | import_storage_center_volume Return the snapshot profiles of an SC volume. Return snapshot profiles  | /import\_storage\_center\_volume/{id}/import\_snapshot\_profiles\_\_\_post |
| import_unity - Discover the importable volumes and consistency gr... | import_unity Discover the importable volumes and consistency groups in the Unity storage system. Discover the importable volumes and consistency groups  | /import\_unity/{id}/discover\_\_\_post |
| import_unity_consistency_group - Return snapshot schedules | import_unity_consistency_group Return the snapshot schedules associated with the specified Unity consistency group. Return snapshot schedules  | /import\_unity\_consistency\_group/{id}/import\_snapshot\_schedules\_\_\_post |
| import_unity_volume - Return snapshot schedules | import_unity_volume Return the snapshot schedules associated with the specified Unity volume. Return snapshot schedules  | /import\_unity\_volume/{id}/import\_snapshot\_schedules\_\_\_post |
| import_vnx_array - Discover Importable Resources | import_vnx_array Discover the importable volumes and consistency groups in a VNX storage system. Discover Importable Resources  | /import\_vnx\_array/{id}/discover\_\_\_post |
| import_xtremio - Discover the importable volumes and consistency gr... | import_xtremio Discover the importable volumes and consistency groups in the XtremIO storage system. Discover the importable volumes and consistency groups  | /import\_xtremio/{id}/discover\_\_\_post |
| import_xtremio_consistency_group - Return snapshot schedules | import_xtremio_consistency_group Return the snapshot schedules associated with the specified XtremIO consistency group. Return snapshot schedules  | /import\_xtremio\_consistency\_group/{id}/import\_snapshot\_schedules\_\_\_post |
| import_xtremio_volume - Return snapshot schedules | import_xtremio_volume Return the snapshot schedules associated with the specified XtremIO volume. Return snapshot schedules  | /import\_xtremio\_volume/{id}/import\_snapshot\_schedules\_\_\_post |
| keystore_archive - Regenerate archive file | keystore_archive Creates a new encryption keystore archive file to replace the existing archive file, which includes the individual keystore backup files from each appliance in the cluster. Once complete, the command response includes a Uniform Resource Identifier, which can be used in a subsequent GET request to download the keystore backup archive file. This request is valid only on systems where Data at Rest Encryption is enabled and is applicable only on systems that support Data at Rest Encryption. Regenerate archive file  | /keystore\_archive/regenerate\_\_\_post |
| ldap_account - Create | ldap_account Create a new LDAP account. Create  | /ldap\_account\_\_\_post |
| ldap_domain - Create | ldap_domain Create a new ldap domain. Create  | /ldap\_domain\_\_\_post |
| ldap_domain - Verify | ldap_domain verifying the connectivity to the LDAP domain server. Verify  | /ldap\_domain/{id}/verify\_\_\_post |
| license - License File Upload | license Upload a software license to install the license on the cluster. License File Upload  | /license/upload\_\_\_post |
| license - Retrieve License | license Retrieve the license directly from the DellEMC Software Licensing Central. This runs automatically when the cluster is configured, and if it fails, once per day during the trial period. This allows a manual attempt, normally after attempting to correct the network connectivity issue preventing the automatic retrieval. Retrieve License  | /license/retrieve\_\_\_post |
| local_user - Create | local_user Create a new local user account. Any existing local user with either an administrator or a security administrator role can create a new local user account. By default, a new local_user account is NOT locked. Create  | /local\_user\_\_\_post |
| logout - Logout | logout Log out the current user. Logout  | /logout\_\_\_post |
| metrics - Metrics | metrics Retrieves metrics for specified type. Metrics  | /metrics/generate\_\_\_post |
| migration_recommendation - Create | migration_recommendation Generate a recommendation for redistributing storage utilization between appliances. Create  | /migration\_recommendation\_\_\_post |
| migration_recommendation - Create migrations | migration_recommendation Create the migration sessions to implement a migration recommendation. If the response contains a list of hosts to rescan, those hosts must be rescanned before starting the sessions or the host(s) may lose access to the data when the migration completes.  Create migrations  | /migration\_recommendation/{id}/create\_migration\_sessions\_\_\_post |
| migration_recommendation - Start migrations | migration_recommendation Start previously created migration sessions for recommendation. Ensure that any rescans specified in the create_migration_sessions response have been done before using this to start the sessions. Failure to do may result in data unavailability and/or data loss.  Start migrations  | /migration\_recommendation/{id}/start\_migration\_sessions\_\_\_post |
| migration_session - Create | migration_session Create a new migration session. For virtual volumes (vVols), the background copy is completed during this phase and the ownership of the vVol is transferred to the new appliance. For volumes and application groups, a migration session is created in this phase and no background copy is performed until either the sync or cutover operation is invoked. There are no interruptions to any services during this phase. Create  | /migration\_session\_\_\_post |
| migration_session - Cutover | migration_session Final phase of the migration, when ownership of the volume, vVol, or volume group is transferred to the new appliance. Cutover  | /migration\_session/{id}/cutover\_\_\_post |
| migration_session - Pause | migration_session Pause a migration session. Only migration sessions in the synchronizing state can be paused. Pause  | /migration\_session/{id}/pause\_\_\_post |
| migration_session - Resume | migration_session Resume a paused migration session. You cannot resume a migration session in the failed state. Resume  | /migration\_session/{id}/resume\_\_\_post |
| migration_session - Sync | migration_session Synchronize a migration session. During this phase, the majority of the background copy is completed and there are no interruptions to any services. Sync can be run multiple times to reduce the amount of data that must be copied during the cutover. Sync  | /migration\_session/{id}/sync\_\_\_post |
| nas_server - Create | nas_server Create a NAS server. Create  | /nas\_server\_\_\_post |
| nas_server - Ping an IP address | nas_server Ping destination from NAS server. Ping an IP address  | /nas\_server/{id}/ping\_\_\_post |
| nas_server - Update User Mappings | nas_server Fix the user mappings for all file systems associated with the NAS server. This process updates file ownership on the NAS server's file systems to reflect changes to users' SIDs. A new UID/GID will be obtained from a Unix Directory Service for the user name of the object owner. A user mapping report is also generated. This operation can take a significant amount of time, depending of the size of the file systems. Update User Mappings  | /nas\_server/{id}/update\_user\_mappings\_\_\_post |
| nas_server - Upload File | nas_server Upload the NAS server nsswitch file. Upload File  | /nas\_server/{id}/upload/nsswitch\_\_\_post |
| nas_server - Upload File | nas_server Upload NAS server passwd file. Upload File  | /nas\_server/{id}/upload/passwd\_\_\_post |
| nas_server - Upload File | nas_server Upload the NAS server netgroup file. Upload File  | /nas\_server/{id}/upload/netgroup\_\_\_post |
| nas_server - Upload File | nas_server Upload File  | /nas\_server/{id}/upload/ntxmap\_\_\_post |
| nas_server - Upload File | nas_server Upload NAS server host file. Upload File  | /nas\_server/{id}/upload/hosts\_\_\_post |
| nas_server - Upload File | nas_server Upload NAS server group file. Upload File  | /nas\_server/{id}/upload/group\_\_\_post |
| nas_server - Upload File | nas_server Upload the NAS server homedir file. Upload File  | /nas\_server/{id}/upload/homedir\_\_\_post |
| network - Add or remove IP ports | network Add IP ports for use by the storage network, or remove IP ports so they can no longer be used. At least one IP port must be configured for use by the storage network.  Add or remove IP ports  | /network/{id}/scale\_\_\_post |
| nfs_export - Create | nfs_export Create an NFS Export for a Snapshot. Create  | /nfs\_export\_\_\_post |
| nfs_server - Create | nfs_server Create an NFS server. Create  | /nfs\_server\_\_\_post |
| nfs_server - Join Active Directory (AD) Domain | nfs_server Join the secure NFS server to the NAS server's AD domain, which is necessary for Secure NFS. Join Active Directory (AD) Domain  | /nfs\_server/{id}/join\_\_\_post |
| nfs_server - Unjoin Active Directory (AD) Domain | nfs_server Unjoin the secure NFS server from the NAS server's Active Directory domain. If you unjoin with secure NFS exports active, exports will be unavailable to the clients. Unjoin Active Directory (AD) Domain  | /nfs\_server/{id}/unjoin\_\_\_post |
| physical_switch - Create | physical_switch Create a physical switch settings. Create  | /physical\_switch\_\_\_post |
| policy - Create | policy Create a new protection policy. Protection policies can be assigned to volumes or volume groups. When a protection policy is assigned to a volume or volume group: * If the policy is associated with one or more snapshot rules, scheduled snapshots are created based on the schedule specified in each snapshot rule. * If the policy is associated with a replication rule, a replication session is created and synchronized based on the schedule specified in the replication rule.  Create  | /policy\_\_\_post |
| remote_system - Create | remote_system Create a new remote system relationship. The type of remote system being connected requires different parameter sets.  For PowerStore remote system relationships, include the following parameters: * Management address - Either an IPv4 or IPv6 address. FQDN is not supported. * Type of remote system  * Data network latency type     For PowerStore remote system relationships, the relationship is created in both directions. Remote protection policies can be configured using the PowerStore remote system instance on either of the systems. This enables remote replication for storage resources in either direction. The data connections take into account whether Challenge Handshake Authentication Protocol (CHAP) is enabled on local and remote PowerStore systems.     For non-PowerStore remote system relationships, include the following parameters: * Management address - Either an IPv4 or IPv6 address. FQDN is not supported. * Type of remote system * Name * Description * Remote administrator credentials * iSCSI address - IPv4 address * CHAP mode for discovery or session  * CHAP secrets details     After the remote system relationship is created, the local system can communicate with the remote system, and open data connections for data transfer.  Create  | /remote\_system\_\_\_post |
| remote_system - Verify | remote_system Verify and update the remote system instance.      Detects changes in the local and remote systems and reestablishes data connections, also taking the Challenge Handshake Authentication Protocol (CHAP) settings into account.  Verify  | /remote\_system/{id}/verify\_\_\_post |
| Replace - Reconfigure cluster management network settings fr... | network Reconfigure cluster management network settings from IPv4 to IPv6 or vice versa. Reconfigure cluster management network settings from IPv4 to IPv6 or vice versa.  | /network/{id}/replace\_\_\_post |
| replication_rule - Create | replication_rule Create a new replication rule.  Create  | /replication\_rule\_\_\_post |
| replication_session - Failover | replication_session Fail over a replication session instance. Failing over the replication session changes the role of the destination system. After a failover, the original destination system becomes the source system, and production access is enabled for hosts and applications for recovery. Failovers can be planned or unplanned.      Planned failovers are issued from the source system and are indicated by setting the is_planned parameter to true. When you fail over a replication session from the source system, the destination system is fully synchronized with the source to ensure that there is no data loss. During a planned failover, stop I/O operations for any applications and hosts. If a synchronization error occurs during a planned failover, the replication session enters the System_Paused state. You cannot pause a replication session during a planned failover. The following operations can be performed during planned failover: * Unplanned failover * Delete the replication session by removing the protection policy on the storage resource     After a planned failover, the replication session is in an inactive state. You can use the reprotect action to synchronize the destination storage resource, and then resume the replication session. The auto-reprotect feature can also be used after a planned failover by using the reverse parameter, which activates the session in the reverse direction.      Unplanned failures are events such as source system failure or an event on the source system that leads to downtime for production access.     Unplanned failovers are issued from the destination system, and are indicated by setting the is_planned parameter to false. Unplanned failovers provide production access to the original destination resource from a preview synchronized point-in-time snapshot referred to as replication common-base. After an unplanned failover, you can restore the system from any point-in-time snapshots on the new source resource. Unplanned failovers pl... | /replication\_session/{id}/failover\_\_\_post |
| replication_session - Pause | replication_session Pause a replication session instance. You can pause a replication session when you need to modify the source or destination system. For example, you can pause the replication session to take the source or destination system down for maintenance.      The session can be paused when it is in the following states: * OK - Remembers the replication session state before pausing, and resumes to OK state * Synchronizing - Remembers the restart address before pausing, and resumes from the restart address * System_Paused - Remembers the restart address before pausing, and resumes from the restart address as recorded when the system entered the System_Paused state     In case of loss of network connectivity between two sites, the replication session is paused only on the local system where it is issued. Pause the replication session again to pause both sites. The following operations are not allowed while only the replication session on the local system is paused: * Resume * Sync * Planned Failover     The following operations are allowed while only the replication session on the local system is paused: * Pause - Use to place both sites into the **Paused** state * Failover - Use to get production access from the disaster recovery site * Delete the replication session by removing the protection policy on the storage resource     The following system operations may also pause, and subsequently resume, a replication session: * Non-disruptive upgrade * Intra-cluster migration      Leaving replication session in a paused state results in change accumulations on the source system, and consume more storage on the source system. Resuming a replication session that has been paused for a long time can result in long synchronization times.   Pause  | /replication\_session/{id}/pause\_\_\_post |
| replication_session - Reprotect | replication_session Reprotect a replication session instance. Activates the replication session and starts synchronization. This can only be used when the session is in the has been failed over.  Reprotect  | /replication\_session/{id}/reprotect\_\_\_post |
| replication_session - Resume | replication_session Resume a replication session instance that is paused. Resuming the replication session schedules a synchronization cycle if the session was in the following states when the session was paused: * Synchronizing * System_Paused      When only the replication session on the local system is paused, resuming the session pauses both sites.     You cannot resume replication sessions paused by the system. The following system operations may also pause, and subsequently resume, a replication session.  * Paused_for_NDU * Paused_for_Migration    Resume  | /replication\_session/{id}/resume\_\_\_post |
| replication_session - Synchronize | replication_session Synchronize the destination resource with changes on source resource from the previous synchronization cycle. Synchronization happens either automatically according to a set schedule, or manually. User and scheduler-created snapshots are synchronized from the source system to the destination system while maintaining block sharing efficiency.        Also synchronizes any size changes, membership changes, or both, on the source resource. At the end of the synchronization cycle, the destination resource reflects the state as it was when synchronization began. Any size changes, membership changes, or both, to source resource done during the synchronization cycle are replicated in next synchronization cycle.     Synchronization is allowed when the replication session is in the following states: * OK * System_Paused     During synchronization, you can take the following actions: * Planned failover from the source system * Failover from the destination system * Pause replication sessions from the source or destination system * Delete a replication session by removing a protection policy     Synchronization failure places the replication session in a System_Paused state. When the system recovers, the replication session continues from the same point as when the system paused, using the restart address.  Synchronize  | /replication\_session/{id}/sync\_\_\_post |
| smb_server - Create | smb_server Create an SMB server. Create  | /smb\_server\_\_\_post |
| smb_server - Domain Join | smb_server Join the SMB server to an Active Directory domain. Domain Join  | /smb\_server/{id}/join\_\_\_post |
| smb_server - Domain Unjoin | smb_server Unjoin the SMB server from an Active Directory domain. Domain Unjoin  | /smb\_server/{id}/unjoin\_\_\_post |
| smb_share - Create | smb_share Create an SMB share. Create  | /smb\_share\_\_\_post |
| smtp_config - Test | smtp_config Test the SMTP configuration. Test  | /smtp\_config/{id}/test\_\_\_post |
| snapshot_rule - Create | snapshot_rule Create a new snapshot rule.  Create  | /snapshot\_rule\_\_\_post |
| software_package - Pre-upgrade Health Check | software_package Run the pre-upgrade health check for a software package. This operation may take some time to respond. Pre-upgrade Health Check  | /software\_package/{id}/puhc\_\_\_post |
| software_package - Start Upgrade | software_package Start a software upgrade background job for the specified appliance within the cluster. If an  appliance is not specified, the upgrade is performed on all appliances in the cluster.    Only specify a subset of appliances to upgrade if the time required to upgrade the entire cluster does not fit within a desired maintenance window. When upgrading a subset of appliances, you must adhere to the following ordering rules:    * The primary appliance must always be upgraded first. * The secondary appliance, which is used as the cluster management database fail-over target, must be upgraded second. * After the primary and secondary appliances are upgraded, any remaining appliances in the cluster may be upgraded. By default, the process upgrades the appliances in the order they were added to the cluster if possible.    Because this operation takes a long time to complete, using the "is_async flag" is recommended. If the "is_reboot_required" flag is set to true, the primary appliance reboots before the install completes and the operation cannot return synchronously.  Start Upgrade  | /software\_package/{id}/install\_\_\_post |
| software_package - Upload | software_package Push a software package file from the client to the cluster. When successfully uploaded and verified, the result is a software_package in the downloaded state, ready to install. Upload  | /software\_package\_\_\_post |
| storage_container - Create | storage_container Create a virtual volume (vVol) storage container. Create  | /storage\_container\_\_\_post |
| storage_container - Mount | storage_container Mount a storage container as a vVol datastore in vCenter. Mount  | /storage\_container/{id}/mount\_\_\_post |
| storage_container - Unmount | storage_container Unmount a storage container, which removes the vVol datastore from vCenter. Unmount  | /storage\_container/{id}/unmount\_\_\_post |
| vcenter - Create | vcenter Add a vCenter. Not allowed in Unified+ deployments. Create  | /vcenter\_\_\_post |
| virtual_machine - Snapshot | virtual_machine Create a snapshot of a virtual machine. This operation cannot be used on a virtual machine snapshot or template. Snapshot  | /virtual\_machine/{id}/snapshot\_\_\_post |
| volume - Attach | volume Attach a volume to a host or host group. Attach  | /volume/{id}/attach\_\_\_post |
| volume - Clone | volume Create a clone of a volume or snapshot. Clone  | /volume/{id}/clone\_\_\_post |
| volume - Create | volume Create a volume on the appliance. Create  | /volume\_\_\_post |
| volume - Detach | volume Detach a volume from a host or host group. Detach  | /volume/{id}/detach\_\_\_post |
| volume - Refresh | volume Refresh the contents of the target volume from another volume in the same family. By default, a backup snapshot of the target volume is not created before the refresh is attempted. To create a snapshot before refreshing, set __create_backup_snap__ to true. If a snapshot is taken, the response includes the resulting snapshot id; otherwise it is empty. If a custom profile is not specified, the profile for the backup snapshot is automatically generated. The automatically generated profile only provides the name as an automatically generated, unique value. Other optional parameters are not specified. When a volume is refreshed, the source_time is the source_time of the volume from which it is refreshed. Refresh  | /volume/{id}/refresh\_\_\_post |
| volume - Restore | volume Restore a volume from a snapshot. A primary or clone volume can only be restored from one of its immediate snapshots.  By default, a backup snapshot of the target snapshot is created before the restore is attempted. To skip creating a snapshot before restoring, set create_backup_snap to false. If a snapshot is taken, the response includes the resulting snapshot id; otherwise it is empty. If a custom profile is not specified, the profile for the backup snapshot is automatically generated. The automatically generated profile only provides the name as an automatically generated, unique value. Other optional parameters are not specified. When a volume is restored, the source_time is the source_time of the snapshot from which it is restored. Restore  | /volume/{id}/restore\_\_\_post |
| volume - Snapshot | volume Create a snapshot of a volume or a clone. The source id of the snapshot is the id of source volume or clone. The source time is the time when the snapshot is created. Snapshot  | /volume/{id}/snapshot\_\_\_post |
| volume_group - Add Members | volume_group Add member volumes to an existing primary or clone volume group. This cannot be used to add members to a snapshot set. Members cannot be added to a volume group that is acting as the destination in a replication session.  Add Members  | /volume\_group/{id}/add\_members\_\_\_post |
| volume_group - Clone | volume_group Clone a volume group. The clone volume group will be created on the same appliance as the source volume group. A clone of a volume group will result in a new volume group of __Clone__ type. The clone will belong to the same family as the source volume group. When the source of a clone operation is a either primary or clone volume group,  * __source_id__ will be set to the identifier of the source volume group.  * __source_time__ will be set to the time at which the clone will be created.  When the source of a clone operation is a snapshot set,  * __source_id__ will be set to the source_id of the source snapshot set.  * __source_time__ will be set to the source_time of the source snapshot set.  The clone volume group will inherit the value of the __is_write_order_consistent__ property from the source volume group. A clone of a snapshot set is modeled as a clone of the snapshot set's source, created at the same time instant as when the source snapshot set was created.  Clone  | /volume\_group/{id}/clone\_\_\_post |
| volume_group - Create | volume_group Create a new volume group. The resulting volume group will have a type of Primary.  Create  | /volume\_group\_\_\_post |
| volume_group - Refresh | volume_group  Refresh the contents of a volume group (the target volume group) from another volume group in the same family. A backup snapshot set of the target volume group will be created before refresh is attempted. This behavior can be overridden by setting the __create_backup_snap__ property to false. The profile for the backup snapshot set will be auto-generated, unless a custom profile is specified. The auto-generated profile only initializes the name to an auto-generated, unique value. Other optional parameters are not specified. The table below outlines supported modes of operation and resulting updates to __source_id__ and __source_time__ attributes of __protection_data__.  Target volume group Source volume group New source_id New source_time   - - - -   Primary (P1)  Clone (C1) id of clone (C1) Current time   Primary (P1)  snapshot set (C1S1) of clone (C1) id of source snapshot set (C1S1) source_time of source snapshot set (C1S1)   Clone (C1)  Primary (P1) id of primary (P1) Current time   Clone (C1)  snapshot set (S1) of primary (P1) id of source snapshot set (S1) source_time of source snapshot set (S1)   Clone (C1)  Clone (C2) id of source clone(C2) Current time   Clone (C1)  snapshot set (C2S1) of clone (C2) id of source snapshot set (C2S1) source_time of source snapshot set (C2S1)  Refresh operation is only supported if there are no membership changes between the source and target volume groups of the refresh operation. You can refresh a volume group even when the sizes of the volumes in the target volume group have changed. This represents a case where the source volumes have been modified over time and you want to refresh the target to the new state of the source volume group. A volume group that is acting as the destination in a replication session cannot be refreshed.  Refresh  | /volume\_group/{id}/refresh\_\_\_post |
| volume_group - Remove Members | volume_group Remove members from an existing primary or clone volume group. This cannot be used to remove members from a snapshot set. Members cannot be removed from a volume group that is a acting as the destination in a replication session.  Remove Members  | /volume\_group/{id}/remove\_members\_\_\_post |
| volume_group - Restore | volume_group Restore a volume group from a snapshot set. A primary or a clone volume group can only be restored from one of its immediate snapshot sets. A backup snapshot set of the target volume group will be created before restore is attempted. This behavior can be overridden by setting the __create_backup_snap__ property to false.  The profile for the backup snapshot set will be auto-generated unless a custom profile is specified. The auto-generated profile only initializes the name to an auto-generated, unique value. Other optional parameters are not specified. Restore operation is only supported if there are no membership changes between the target volume group and source snapshot set. You can restore a volume group even when the sizes of the volumes in the target volume group have changed. This represents a case where the target volumes have been modified over time, but you want to revert them back to their old state captured in the source snapshot set. When a volume group is restored,  * __source_time__ is set to the __source_time__ of the snapshot set it is being restored from.  A volume group that is acting as the destination in a replication session cannot be restored.  Restore  | /volume\_group/{id}/restore\_\_\_post |
| volume_group - Snapshot | volume_group Create a new snapshot set for a volume group. When a snapshot of a volume group is created, the resultant snapshot volume group is referred to as a "snapshot set" and it represents a point-in-time copy of the members in the volume group. The snapshot set will be created on the same appliance as the source volume group. A snapshot of a volume group will result in a new volume group of __Snapshot__ type. The snapshot set will belong to the same family as the source volume group. When the source of a snapshot operation is a primary or clone volume group,  * __source_id__ of the snapshot set will be set to the identifier of the source volume group.  * __source_time__ of the snapshot set will be set to the time at which the snapshot set will be created.  The __is_write_order_consistent__ property of the source volume group determines whether the snapshot set will be write-order consistent.  Snapshot  | /volume\_group/{id}/snapshot\_\_\_post |
| x509_certificate - Exchange Certificates | x509_certificate Exchange certificates between two clusters. Add CA certificates to the trust store of each cluster and assign roles to the client certificates. After this process, certificate-based authentication can be used for communication between clusters. This exchange REST API can only be triggered with service Replication_HTTP. Exchange Certificates  | /x509\_certificate/exchange\_\_\_post |


### DELETE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| email_notify_destination - Delete | email_notify_destination Delete an email notification destination. Delete  | /email\_notify\_destination/{id}\_\_\_delete |
| file_dns - Delete | file_dns Delete DNS settings of a NAS Server. Delete  | /file\_dns/{id}\_\_\_delete |
| file_ftp - Delete | file_ftp Delete an FTP/SFTP Server. Delete  | /file\_ftp/{id}\_\_\_delete |
| file_interface - Delete | file_interface Delete a file interface. Delete  | /file\_interface/{id}\_\_\_delete |
| file_interface_route - Delete | file_interface_route Delete file interface route. Delete  | /file\_interface\_route/{id}\_\_\_delete |
| file_kerberos - Delete | file_kerberos Delete Kerberos configuration of a NAS Server. Delete  | /file\_kerberos/{id}\_\_\_delete |
| file_ldap - Delete | file_ldap Delete a NAS Server's LDAP settings. Delete  | /file\_ldap/{id}\_\_\_delete |
| file_ndmp - Delete | file_ndmp Delete an NDMP service configuration instance of a NAS Server. Delete  | /file\_ndmp/{id}\_\_\_delete |
| file_nis - Delete | file_nis Delete NIS settings of a NAS Server. Delete  | /file\_nis/{id}\_\_\_delete |
| file_system - Delete | file_system Delete a file system. Delete  | /file\_system/{id}\_\_\_delete |
| file_tree_quota - Delete | file_tree_quota Delete a tree quota instance. Delete  | /file\_tree\_quota/{id}\_\_\_delete |
| file_virus_checker - Delete | file_virus_checker Delete virus checker settings of a NAS Server. Delete  | /file\_virus\_checker/{id}\_\_\_delete |
| host - Delete | host Delete a host. Delete fails if host is attached to a volume or consistency group. Delete  | /host/{id}\_\_\_delete |
| host_group - Delete | host_group Delete a host group. Delete fails if host group is attached to a volume. Delete  | /host\_group/{id}\_\_\_delete |
| import_host_system - Delete | import_host_system Delete an import host system. You cannot delete an import host system if there are import sessions active in the system referencing the import host system instance. Delete  | /import\_host\_system/{id}\_\_\_delete |
| import_session - Delete | import_session Delete an import session that is in a Completed, Failed, or Cancelled state. Delete removes the historical record of the import. To stop active import sessions, use the Cancel operation. You can delete the import session after cancelling it. Delete  | /import\_session/{id}\_\_\_delete |
| ldap_account - Delete | ldap_account Delete an LDAP account. Delete  | /ldap\_account/{id}\_\_\_delete |
| ldap_domain - Delete | ldap_domain Delete an LDAP domain. Delete  | /ldap\_domain/{id}\_\_\_delete |
| local_user - Delete | local_user Delete a local user account instance using the unique identifier. You cannot delete the default "admin" account or the account you are currently logged into. Any local user account with Administrator or Security Administrator role can delete any other local user account except the default "admin" account. Delete  | /local\_user/{id}\_\_\_delete |
| migration_recommendation - Delete | migration_recommendation Delete a migration recommendation. Delete  | /migration\_recommendation/{id}\_\_\_delete |
| migration_session - Delete | migration_session Delete a migration session. With the force option, a migration session can be deleted regardless of its state. All background activity is canceled before deleting the session. Delete  | /migration\_session/{id}\_\_\_delete |
| nas_server - Delete | nas_server Delete a NAS server. Delete  | /nas\_server/{id}\_\_\_delete |
| nfs_export - Delete | nfs_export Delete NFS Export. Delete  | /nfs\_export/{id}\_\_\_delete |
| nfs_server - Delete | nfs_server Delete an NFS server. Delete  | /nfs\_server/{id}\_\_\_delete |
| physical_switch - Delete | physical_switch Delete the physical switch settings. Delete  | /physical\_switch/{id}\_\_\_delete |
| policy - Delete | policy Delete a protection policy.  Protection policies that are used by any storage resources can not be deleted.  Delete  | /policy/{id}\_\_\_delete |
| remote_system - Delete | remote_system Delete a remote system. Deleting the remote system deletes the management and data connections established with the remote system. You cannot delete a remote system if there are active import sessions, or if there are remote protection policies active in the system referencing the remote system instance.     For PowerStore remote systems, the relationship is deleted in both directions if the remote system is up and connectable. You cannot delete a PowerStore remote system if there is no management connectivity between the local and remore systems. Only the local end of the relationship is deleted. Manually log in to the remote PowerStore system and remove the relationship.  Delete  | /remote\_system/{id}\_\_\_delete |
| replication_rule - Delete | replication_rule Delete a replication rule.  Deleting a rule is not permitted, if the rule is associated with a protection policy that is currently applied to a storage resource.  Delete  | /replication\_rule/{id}\_\_\_delete |
| smb_server - Delete | smb_server Delete a SMB server. The SMB server must not be joined to a domain to be deleted.  Delete  | /smb\_server/{id}\_\_\_delete |
| smb_share - Delete | smb_share Delete an SMB Share. Delete  | /smb\_share/{id}\_\_\_delete |
| snapshot_rule - Delete | snapshot_rule Delete a snapshot rule  Delete  | /snapshot\_rule/{id}\_\_\_delete |
| software_package - Delete | software_package Delete the specified software package from the cluster. This operation may take some time to complete. Delete  | /software\_package/{id}\_\_\_delete |
| storage_container - Delete | storage_container Delete a storage container. Delete  | /storage\_container/{id}\_\_\_delete |
| vcenter - Delete | vcenter Delete a registered vCenter. Deletion of vCenter disables functionality that requires communication with vCenter. Not allowed in Unified+ deployments. Delete  | /vcenter/{id}\_\_\_delete |
| virtual_machine - Delete | virtual_machine Delete a virtual machine snapshot. This operation cannot be used on a base virtual machine or virtual machine template. Delete  | /virtual\_machine/{id}\_\_\_delete |
| virtual_volume - Delete | virtual_volume Delete a virtual volume. Delete  | /virtual\_volume/{id}\_\_\_delete |
| volume - Delete | volume Delete a volume.   * A volume which is attached to a host or host group or is a member of a volume group cannot be deleted. * A volume which has protection policies attached to it cannot be deleted. * A volume which has snapshots that are part of a snapset cannot be deleted. * Clones of a deleted production volume or a clone are not deleted. * Snapshots of the volume are deleted along with the volume being deleted. Delete  | /volume/{id}\_\_\_delete |
| volume_group - Delete | volume_group Delete a volume group, snapshot set, or clone. Before you try deleting a volume group, snapshot set, or clone, ensure that you first detach it from all hosts. Note the following: * When a volume group or clone is deleted, all related snapshot sets will also be deleted. * When a snapshot set is deleted, all of its constituent snapshots will also be deleted.  Delete  | /volume\_group/{id}\_\_\_delete |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| alert - Collection Query | alert Query all alerts. Collection Query  | /alert\_\_\_get |
| alert - Instance Query | alert Query a specific alert. Instance Query  | /alert/{id}\_\_\_get |
| appliance - Collection Query | appliance Query the appliances in a cluster. Collection Query  | /appliance\_\_\_get |
| appliance - Instance Query | appliance Query a specific appliance in a cluster. Instance Query  | /appliance/{id}\_\_\_get |
| bond - Collection Query | bond Query bond configurations. Collection Query  | /bond\_\_\_get |
| bond - Instance Query | bond Query a specific bond configuration. Instance Query  | /bond/{id}\_\_\_get |
| chap_config - Collection Query | chap_config Query the list of (one) CHAP configuration settings objects.  This resource type collection query does not support filtering, sorting or pagination Collection Query  | /chap\_config\_\_\_get |
| chap_config - Instance Query | chap_config Query the CHAP configuration settings object. Instance Query  | /chap\_config/{id}\_\_\_get |
| cluster - Collection Query | cluster Get details about the cluster.  This resource type collection query does not support filtering, sorting or pagination Collection Query  | /cluster\_\_\_get |
| cluster - Instance Query | cluster Get details about the cluster. This does not support the following standard query functionality: property selection, and nested query embedding. Instance Query  | /cluster/{id}\_\_\_get |
| discovered_initiator - Collection Query | discovered_initiator Returns connected initiators that are not associated with a host.  This resource type collection query does not support filtering, sorting or pagination Collection Query  | /discovered\_initiator\_\_\_get |
| dns - Collection Query | dns Query DNS settings for a cluster. Collection Query  | /dns\_\_\_get |
| dns - Instance Query | dns Query a specific DNS setting. Instance Query  | /dns/{id}\_\_\_get |
| email_notify_destination - Collection Query | email_notify_destination Query all email notification destinations. Collection Query  | /email\_notify\_destination\_\_\_get |
| email_notify_destination - Instance Query | email_notify_destination Query a specific email notification destination. Instance Query  | /email\_notify\_destination/{id}\_\_\_get |
| eth_port - Collection Query | eth_port Get Ethernet front-end port configuration for all cluster nodes. Collection Query  | /eth\_port\_\_\_get |
| eth_port - Instance Query | eth_port Get Ethernet front-end port configuration by instance identifier. Instance Query  | /eth\_port/{id}\_\_\_get |
| EventInstanceGet - Event summary | event Get event by Event Id. Event summary  | /event/{id}\_\_\_get |
| EventInstancesGet - Get events | event Returns all events in the database. Get events  | /event\_\_\_get |
| fc_port - Collection Query | fc_port Query the FC front-end port configurations for all cluster nodes. Collection Query  | /fc\_port\_\_\_get |
| fc_port - Instance Query | fc_port Query a specific FC front-end port configuration. Instance Query  | /fc\_port/{id}\_\_\_get |
| file_dns - Collection Query | file_dns Query of the DNS settings of NAS Servers. Collection Query  | /file\_dns\_\_\_get |
| file_dns - Instance Query | file_dns Query a specific DNS settings object of a NAS Server. Instance Query  | /file\_dns/{id}\_\_\_get |
| file_ftp - Collection Query | file_ftp Query FTP/SFTP instances. Collection Query  | /file\_ftp\_\_\_get |
| file_ftp - Instance Query | file_ftp Query a specific FTP/SFTP server for its settings. Instance Query  | /file\_ftp/{id}\_\_\_get |
| file_interface - Collection Query | file_interface Query file interfaces. Collection Query  | /file\_interface\_\_\_get |
| file_interface - Instance Query | file_interface Instance Query  | /file\_interface/{id}\_\_\_get |
| file_interface_route - Collection Query | file_interface_route Query file interface routes. Collection Query  | /file\_interface\_route\_\_\_get |
| file_interface_route - Instance Query | file_interface_route Query a specific file interface route for details. Instance Query  | /file\_interface\_route/{id}\_\_\_get |
| file_kerberos - Collection Query | file_kerberos Query of the Kerberos service settings of NAS Servers. Collection Query  | /file\_kerberos\_\_\_get |
| file_kerberos - Download Keytab File | file_kerberos Download previously uploaded keytab file for secure NFS service. Download Keytab File  | /file\_kerberos/{id}/download\_keytab\_\_\_get |
| file_kerberos - Instance Query | file_kerberos Query a specific Kerberos service settings of a NAS Server. Instance Query  | /file\_kerberos/{id}\_\_\_get |
| file_ldap - Collection Query | file_ldap List LDAP Service instances. Collection Query  | /file\_ldap\_\_\_get |
| file_ldap - Download Certificate | file_ldap Download Certificate  | /file\_ldap/{id}/download\_certificate\_\_\_get |
| file_ldap - Download Config | file_ldap Download Config  | /file\_ldap/{id}/download\_config\_\_\_get |
| file_ldap - Instance Query | file_ldap Query a specific NAS Server's LDAP settings object. Instance Query  | /file\_ldap/{id}\_\_\_get |
| file_ndmp - Collection Query | file_ndmp List configured NDMP service instances. Collection Query  | /file\_ndmp\_\_\_get |
| file_ndmp - Instance Query | file_ndmp Query an NDMP service configuration instance. Instance Query  | /file\_ndmp/{id}\_\_\_get |
| file_nis - Collection Query | file_nis Query the NIS settings of NAS Servers. Collection Query  | /file\_nis\_\_\_get |
| file_nis - Instance Query | file_nis Query a specific NIS settings object of a NAS Server. Instance Query  | /file\_nis/{id}\_\_\_get |
| file_system - Collection Query | file_system List file systems. Collection Query  | /file\_system\_\_\_get |
| file_system - Instance Query | file_system Query a specific file system. Instance Query  | /file\_system/{id}\_\_\_get |
| file_tree_quota - Collection Query | file_tree_quota List tree quota instances. Collection Query  | /file\_tree\_quota\_\_\_get |
| file_tree_quota - Instance Query | file_tree_quota Query a tree quota instance. Instance Query  | /file\_tree\_quota/{id}\_\_\_get |
| file_user_quota - Collection Query | file_user_quota List user quota instances. Collection Query  | /file\_user\_quota\_\_\_get |
| file_user_quota - Instance Query | file_user_quota Query a user quota instance. Instance Query  | /file\_user\_quota/{id}\_\_\_get |
| file_virus_checker - Collection Query | file_virus_checker Query all virus checker settings of the NAS Servers. Collection Query  | /file\_virus\_checker\_\_\_get |
| file_virus_checker - Download Config File | file_virus_checker Download a virus checker configuration file containing the template or the actual (if already uploaded) virus checker configuration settings. Download Config File  | /file\_virus\_checker/{id}/download\_config\_\_\_get |
| file_virus_checker - Instance Query | file_virus_checker Query a specific virus checker setting of a NAS Server. Instance Query  | /file\_virus\_checker/{id}\_\_\_get |
| GetAuditLogs - Collection Query | audit_event Query audit log entries. Collection Query  | /audit\_event\_\_\_get |
| hardware - Collection Query | hardware List hardware components. Collection Query  | /hardware\_\_\_get |
| hardware - Instance Query | hardware Get a specific hardware component instance. Instance Query  | /hardware/{id}\_\_\_get |
| host - Collection Query | host List host information. Collection Query  | /host\_\_\_get |
| host - Instance Query | host Get details about a specific host by id. Instance Query  | /host/{id}\_\_\_get |
| host_group - Collection Query | host_group List host groups. Collection Query  | /host\_group\_\_\_get |
| host_group - Instance Query | host_group Get details about a specific host group. Instance Query  | /host\_group/{id}\_\_\_get |
| host_virtual_volume_mapping - Collection Query | host_virtual_volume_mapping Query associations between a virtual volume and the host(s) it is attached to. Collection Query  | /host\_virtual\_volume\_mapping\_\_\_get |
| host_virtual_volume_mapping - Instance Query | host_virtual_volume_mapping Query a specific virtual volume mapping. Instance Query  | /host\_virtual\_volume\_mapping/{id}\_\_\_get |
| host_volume_mapping - Collection Query | host_volume_mapping Query associations between a volume and the host or host group it is attached to. Collection Query  | /host\_volume\_mapping\_\_\_get |
| host_volume_mapping - Instance Query | host_volume_mapping Query a specific host volume mapping. Instance Query  | /host\_volume\_mapping/{id}\_\_\_get |
| import_host_initiator - Collection Query | import_host_initiator Query import host initiators. Collection Query  | /import\_host\_initiator\_\_\_get |
| import_host_initiator - Instance Query | import_host_initiator Query a specific import host initiator instance. Instance Query  | /import\_host\_initiator/{id}\_\_\_get |
| import_host_system - Collection Query | import_host_system Query import host systems that are attached to volumes. Collection Query  | /import\_host\_system\_\_\_get |
| import_host_system - Instance Query | import_host_system Query a specific import host system instance. Instance Query  | /import\_host\_system/{id}\_\_\_get |
| import_host_volume - Collection Query | import_host_volume Query import host volumes. Collection Query  | /import\_host\_volume\_\_\_get |
| import_host_volume - Instance Query | import_host_volume Query a specific import host volume instance. Instance Query  | /import\_host\_volume/{id}\_\_\_get |
| import_psgroup - Collection Query | import_psgroup Query PS Group storage arrays. Collection Query  | /import\_psgroup\_\_\_get |
| import_psgroup - Instance Query | import_psgroup Query a specific PS Group storage array. Instance Query  | /import\_psgroup/{id}\_\_\_get |
| import_psgroup_volume - Collection Query | import_psgroup_volume Query PS Group volumes. Collection Query  | /import\_psgroup\_volume\_\_\_get |
| import_psgroup_volume - Instance Query | import_psgroup_volume Query a specific PS Group volume. Instance Query  | /import\_psgroup\_volume/{id}\_\_\_get |
| import_session - Collection Query | import_session Query import sessions. Collection Query  | /import\_session\_\_\_get |
| import_session - Instance Query | import_session Query a specific session. Instance Query  | /import\_session/{id}\_\_\_get |
| import_storage_center - Collection Query | import_storage_center Query SC arrays. Collection Query  | /import\_storage\_center\_\_\_get |
| import_storage_center - Instance Query | import_storage_center Query a specific SC array. Instance Query  | /import\_storage\_center/{id}\_\_\_get |
| import_storage_center_consistency_group - Collection Query | import_storage_center_consistency_group Query SC consistency groups. Collection Query  | /import\_storage\_center\_consistency\_group\_\_\_get |
| import_storage_center_consistency_group - Instance Query | import_storage_center_consistency_group Query a specific SC consistency group. Instance Query  | /import\_storage\_center\_consistency\_group/{id}\_\_\_get |
| import_storage_center_volume - Collection Query | import_storage_center_volume Query SC volumes. Collection Query  | /import\_storage\_center\_volume\_\_\_get |
| import_storage_center_volume - Instance Query | import_storage_center_volume Query a specific SC volume. Instance Query  | /import\_storage\_center\_volume/{id}\_\_\_get |
| import_unity - Collection Query | import_unity Query Unity storage systems. Collection Query  | /import\_unity\_\_\_get |
| import_unity - Instance Query | import_unity Query a specific Unity storage system. Instance Query  | /import\_unity/{id}\_\_\_get |
| import_unity_consistency_group - Collection Query | import_unity_consistency_group Query Unity consistency groups. Collection Query  | /import\_unity\_consistency\_group\_\_\_get |
| import_unity_consistency_group - Instance Query | import_unity_consistency_group Query a specific Unity consistency group. Instance Query  | /import\_unity\_consistency\_group/{id}\_\_\_get |
| import_unity_volume - Collection Query | import_unity_volume Query Unity volumes. Collection Query  | /import\_unity\_volume\_\_\_get |
| import_unity_volume - Instance Query | import_unity_volume Query a specific Unity volume. Instance Query  | /import\_unity\_volume/{id}\_\_\_get |
| import_vnx_array - Collection Query | import_vnx_array Query VNX storage systems. Collection Query  | /import\_vnx\_array\_\_\_get |
| import_vnx_array - Instance Query | import_vnx_array Query a specific VNX storage system. Instance Query  | /import\_vnx\_array/{id}\_\_\_get |
| import_vnx_consistency_group - Collection Query | import_vnx_consistency_group Query VNX consistency groups. Collection Query  | /import\_vnx\_consistency\_group\_\_\_get |
| import_vnx_consistency_group - Instance Query | import_vnx_consistency_group Query a specific VNX consistency group. Instance Query  | /import\_vnx\_consistency\_group/{id}\_\_\_get |
| import_vnx_volume - Collection Query | import_vnx_volume Query VNX volumes. Collection Query  | /import\_vnx\_volume\_\_\_get |
| import_vnx_volume - Instance Query | import_vnx_volume Query a specific VNX volume. Instance Query  | /import\_vnx\_volume/{id}\_\_\_get |
| import_xtremio - Collection Query | import_xtremio Query XtremIO storage systems X1 and X2. Collection Query  | /import\_xtremio\_\_\_get |
| import_xtremio - Instance Query | import_xtremio Query a specific XtremIO storage system. Instance Query  | /import\_xtremio/{id}\_\_\_get |
| import_xtremio_consistency_group - Collection Query | import_xtremio_consistency_group Query XtremIO consistency groups. Collection Query  | /import\_xtremio\_consistency\_group\_\_\_get |
| import_xtremio_consistency_group - Instance Query | import_xtremio_consistency_group Query a specific XtremIO consistency group. Instance Query  | /import\_xtremio\_consistency\_group/{id}\_\_\_get |
| import_xtremio_volume - Collection Query | import_xtremio_volume Query XtremIO volumes. Collection Query  | /import\_xtremio\_volume\_\_\_get |
| import_xtremio_volume - Instance Query | import_xtremio_volume Query a specific XtremIO volume. Instance Query  | /import\_xtremio\_volume/{id}\_\_\_get |
| ip_pool_address - Collection Query | ip_pool_address Query configured IP addresses. Collection Query  | /ip\_pool\_address\_\_\_get |
| ip_pool_address - Instance Query | ip_pool_address Query a specific IP address. Instance Query  | /ip\_pool\_address/{id}\_\_\_get |
| ip_port - Collection Query | ip_port Query IP port configurations. Collection Query  | /ip\_port\_\_\_get |
| ip_port - Instance Query | ip_port Query a specific IP port configuration. Instance Query  | /ip\_port/{id}\_\_\_get |
| job - Collection query | job Query jobs. Collection query  | /job\_\_\_get |
| job - Instance query | job Query a specific job. Instance query  | /job/{id}\_\_\_get |
| keystore_archive - Download a keystore backup archive file | keystore_archive Download a keystore backup archive file that was previously generated by a successful /api/rest/keystore_archive/regenerate POST command.  This resource type collection query does not support filtering, sorting or pagination Download a keystore backup archive file  | /keystore\_archive/{filename}\_\_\_get |
| ldap_account - Collection Query | ldap_account List LDAP account instances.  This resource type collection query does not support filtering, sorting or pagination Collection Query  | /ldap\_account\_\_\_get |
| ldap_account - Instance Query | ldap_account Query a specific LDAP account instance. Instance Query  | /ldap\_account/{id}\_\_\_get |
| ldap_domain - Collection Query | ldap_domain Query list of ldap domain.  This resource type collection query does not support filtering, sorting or pagination Collection Query  | /ldap\_domain\_\_\_get |
| ldap_domain - Instance Query | ldap_domain Query a specific LDAP domain. Instance Query  | /ldap\_domain/{id}\_\_\_get |
| license - Collection Query | license Query license information for the cluster. There is always one license instance. Collection Query  | /license\_\_\_get |
| license - Instance Query | license Query the specific license information for the cluster. Instance Query  | /license/{id}\_\_\_get |
| local_user - Collection Query | local_user Query all local user account instances.  This resource type collection query does not support filtering, sorting or pagination Collection Query  | /local\_user\_\_\_get |
| local_user - Instance Query | local_user Query a specific local user account instance using an unique identifier. Instance Query  | /local\_user/{id}\_\_\_get |
| login_session - Collection Query | login_session Obtain the login session for the current user.  This resource type collection query does not support filtering, sorting or pagination Collection Query  | /login\_session\_\_\_get |
| maintenance_window - Collection Query | maintenance_window Query the maintenance window configurations. Collection Query  | /maintenance\_window\_\_\_get |
| maintenance_window - Instance Query | maintenance_window Query one appliance maintenance window configuration. Instance Query  | /maintenance\_window/{id}\_\_\_get |
| migration_recommendation - Collection query | migration_recommendation Get migration recommendations. Collection query  | /migration\_recommendation\_\_\_get |
| migration_recommendation - Instance query | migration_recommendation Get a single migration recommendation. Instance query  | /migration\_recommendation/{id}\_\_\_get |
| migration_session - Collection Query | migration_session Query migration sessions. Collection Query  | /migration\_session\_\_\_get |
| migration_session - Instance Query | migration_session Query a specific migration session. Instance Query  | /migration\_session/{id}\_\_\_get |
| nas_server - Collection Query | nas_server Query all NAS servers. Collection Query  | /nas\_server\_\_\_get |
| nas_server - Download File | nas_server Download a NAS server passwd file containing template or the actual (if already uploaded) passwd details. Download File  | /nas\_server/{id}/download/passwd\_\_\_get |
| nas_server - Download File | nas_server Download a NAS server group file containing the template or the actual (if already uploaded) group details. Download File  | /nas\_server/{id}/download/group\_\_\_get |
| nas_server - Download File | nas_server Download a NAS server homedir file containing the template or the actual (if already uploaded) homedir configuration settings. Download File  | /nas\_server/{id}/download/homedir\_\_\_get |
| nas_server - Download File | nas_server Download an NAS server host file containing template/actual(if already uploaded) host details. Download File  | /nas\_server/{id}/download/hosts\_\_\_get |
| nas_server - Download File | nas_server Download a NAS server nsswitch file containing the template or the actual (if already uploaded) nsswitch configuration settings. Download File  | /nas\_server/{id}/download/nsswitch\_\_\_get |
| nas_server - Download File | nas_server Download an NAS server ntxmap file containing the template or the actual (if already uploaded) ntxmap configuration settings. Download File  | /nas\_server/{id}/download/ntxmap\_\_\_get |
| nas_server - Download File | nas_server Download an NAS server netgroup file containing the template or the actual (if already uploaded) netgroup details. Download File  | /nas\_server/{id}/download/netgroup\_\_\_get |
| nas_server - Download User Mapping Report | nas_server Download the report generated by the update_user_mappings action.  Download User Mapping Report  | /nas\_server/{id}/download/user\_mapping\_report\_\_\_get |
| nas_server - Instance Query | nas_server Query a specific NAS server. Instance Query  | /nas\_server/{id}\_\_\_get |
| network - Collection Query | network Query the IP network configurations of the cluster. Collection Query  | /network\_\_\_get |
| network - Instance Query | network Query a specific IP network configuration. Instance Query  | /network/{id}\_\_\_get |
| nfs_export - Collection Query | nfs_export List NFS Exports. Collection Query  | /nfs\_export\_\_\_get |
| nfs_export - Instance Query | nfs_export Get NFS Export properties. Instance Query  | /nfs\_export/{id}\_\_\_get |
| nfs_server - Collection Query | nfs_server Query all NFS Servers. Collection Query  | /nfs\_server\_\_\_get |
| nfs_server - Instance Query | nfs_server Query settings of an NFS server. Instance Query  | /nfs\_server/{id}\_\_\_get |
| node - Collection Query | node Query the nodes in a cluster. Collection Query  | /node\_\_\_get |
| node - Instance Query | node Query a specific node in a cluster. Instance Query  | /node/{id}\_\_\_get |
| ntp - Collection Query | ntp Query NTP settings for a cluster. Collection Query  | /ntp\_\_\_get |
| ntp - Instance Query | ntp Query a specific NTP setting. Instance Query  | /ntp/{id}\_\_\_get |
| performance_rule - Collection query | performance_rule Get performance rules. Collection query  | /performance\_rule\_\_\_get |
| performance_rule - Instance query | performance_rule Get a performance rule by id. Instance query  | /performance\_rule/{id}\_\_\_get |
| physical_switch - Collection Query | physical_switch Query physical switches settings for a cluster. Collection Query  | /physical\_switch\_\_\_get |
| physical_switch - Instance Query | physical_switch Query a specific physical switch settings. Instance Query  | /physical\_switch/{id}\_\_\_get |
| policy - Collection Query | policy Query protection and performance policies.  The following REST query is an example of how to retrieve protection policies along with their rules and associated resources:  https://{{cluster_ip}}/api/rest/policy?select=name,id,type,replication_rules(id,name,rpo,remote_system(id,name,management_address)),snapshot_rules(id,name,interval,time_of_day,days_of_week),volume(id,name),volume_group(id,name)&type=eq.Protection  The following REST query is an example of how to retrieve performance policies along with their associated resources:     https://{{cluster_ip}}/api/rest/policy?select=name,id,type,volume(id,name),volume_group(id,name)&type=eq.Performance  Collection Query  | /policy\_\_\_get |
| policy - Instance Query | policy Query a specific policy. Instance Query  | /policy/{id}\_\_\_get |
| remote_system - Collection Query | remote_system Query remote systems.  Collection Query  | /remote\_system\_\_\_get |
| remote_system - Instance Query | remote_system Query a remote system instance.  Instance Query  | /remote\_system/{id}\_\_\_get |
| replication_rule - Collection Query | replication_rule Query all replication rules. Collection Query  | /replication\_rule\_\_\_get |
| replication_rule - Instance Query | replication_rule Query a specific replication rule. Instance Query  | /replication\_rule/{id}\_\_\_get |
| replication_session - Collection Query | replication_session Query replication sessions.  Collection Query  | /replication\_session\_\_\_get |
| replication_session - Instance Query | replication_session Query a replication session instance.  Instance Query  | /replication\_session/{id}\_\_\_get |
| role - Collection Query | role Query roles.  This resource type collection query does not support filtering, sorting or pagination Collection Query  | /role\_\_\_get |
| role - Instance Query | role Query a specific role. Instance Query  | /role/{id}\_\_\_get |
| sas_port - Collection Query | sas_port Query the SAS port configuration for all cluster nodes. Collection Query  | /sas\_port\_\_\_get |
| sas_port - Instance query | sas_port Query a specific SAS port configuration. Instance query  | /sas\_port/{id}\_\_\_get |
| security_config - Collection Query | security_config Query system security configurations.  This resource type collection query does not support filtering, sorting or pagination Collection Query  | /security\_config\_\_\_get |
| security_config - Instance Query | security_config Query a specific system security configuration. Instance Query  | /security\_config/{id}\_\_\_get |
| service_config - Collection Query | service_config Query the service configuration instances for the cluster.  This resource type collection query does not support filtering, sorting or pagination Collection Query  | /service\_config\_\_\_get |
| service_config - Instance Query | service_config Query the service configuration instances for an appliance. Instance Query  | /service\_config/{id}\_\_\_get |
| service_user - Collection Query | service_user Query the service user account instance.  This resource type collection query does not support filtering, sorting or pagination Collection Query  | /service\_user\_\_\_get |
| service_user - Instance Query | service_user Query the service user account using the unique identifier. Instance Query  | /service\_user/{id}\_\_\_get |
| smb_server - Collection Query | smb_server Query all SMB servers. Collection Query  | /smb\_server\_\_\_get |
| smb_server - Instance Query | smb_server Query settings of a specific SMB server. Instance Query  | /smb\_server/{id}\_\_\_get |
| smb_share - Collection Query | smb_share List SMB shares. Collection Query  | /smb\_share\_\_\_get |
| smb_share - Instance Query | smb_share Get an SMB Share. Instance Query  | /smb\_share/{id}\_\_\_get |
| smtp_config - Collection Query | smtp_config Query the SMTP configuration. There is always exactly one smtp_config instance. Collection Query  | /smtp\_config\_\_\_get |
| smtp_config - Instance Query | smtp_config Query the specific SMTP configuration. Instance Query  | /smtp\_config/{id}\_\_\_get |
| snapshot_rule - Collection Query | snapshot_rule Query all snapshot rules. Collection Query  | /snapshot\_rule\_\_\_get |
| snapshot_rule - Instance Query | snapshot_rule Query a specific snapshot rule. Instance Query  | /snapshot\_rule/{id}\_\_\_get |
| software_installed - Collection Query | software_installed Query the software that is installed on each appliance. The output returns a list of JSON objects representing the software that is installed on each appliance and one entry representing the common software installed version that is supported for all appliances in the cluster. Collection Query  | /software\_installed\_\_\_get |
| software_installed - Instance Query | software_installed Query a specific item from the list of installed software. Instance Query  | /software\_installed/{id}\_\_\_get |
| software_package - Collection Query | software_package Query the software packages that are known by the cluster. The output returns a list of JSON objects representing the packages. Collection Query  | /software\_package\_\_\_get |
| software_package - Instance Query | software_package Query a specific software package. Instance Query  | /software\_package/{id}\_\_\_get |
| storage_container - Collection Query | storage_container List storage containers. Collection Query  | /storage\_container\_\_\_get |
| storage_container - Instance Query | storage_container Query a specific instance of storage container. Instance Query  | /storage\_container/{id}\_\_\_get |
| vcenter - Collection Query | vcenter Query registered vCenters. Collection Query  | /vcenter\_\_\_get |
| vcenter - Instance Query | vcenter Query a specific vCenter instance. Instance Query  | /vcenter/{id}\_\_\_get |
| veth_port - Collection Query | veth_port Query virtual Ethernet port configurations. Collection Query  | /veth\_port\_\_\_get |
| veth_port - Instance Query | veth_port Query a specific virtual Ethernet port configuration. Instance Query  | /veth\_port/{id}\_\_\_get |
| virtual_machine - Collection Query | virtual_machine Query virtual machines that use storage from the cluster. Collection Query  | /virtual\_machine\_\_\_get |
| virtual_machine - Instance Query | virtual_machine Query a specific virtual machine instance. Instance Query  | /virtual\_machine/{id}\_\_\_get |
| virtual_volume - Collection Query | virtual_volume Get virtual volumes. Collection Query  | /virtual\_volume\_\_\_get |
| virtual_volume - Instance Query | virtual_volume Get a specific virtual volume. Instance Query  | /virtual\_volume/{id}\_\_\_get |
| volume - Collection Query | volume Query volumes that are provisioned on the appliance. Collection Query  | /volume\_\_\_get |
| volume - Instance Query | volume Query a specific volume instance. Instance Query  | /volume/{id}\_\_\_get |
| volume_group - Collection Query | volume_group Query all volume groups, including snapshot sets and clones of volume groups.  Collection Query  | /volume\_group\_\_\_get |
| volume_group - Instance Query | volume_group Query a specific volume group, snapshot set, or clone. Instance Query  | /volume\_group/{id}\_\_\_get |
| x509_certificate - Collection Query | x509_certificate Query to list X509 Certificates instances.  This resource type collection query does not support filtering, sorting or pagination Collection Query  | /x509\_certificate\_\_\_get |
| x509_certificate - Instance Query | x509_certificate Query a specific X509 Certificate instance. Instance Query  | /x509\_certificate/{id}\_\_\_get |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| alert - Instance Query | alert Query a specific alert. Instance Query  | /alert/{id}\_\_\_get |
| appliance - Instance Query | appliance Query a specific appliance in a cluster. Instance Query  | /appliance/{id}\_\_\_get |
| bond - Instance Query | bond Query a specific bond configuration. Instance Query  | /bond/{id}\_\_\_get |
| chap_config - Instance Query | chap_config Query the CHAP configuration settings object. Instance Query  | /chap\_config/{id}\_\_\_get |
| cluster - Instance Query | cluster Get details about the cluster. This does not support the following standard query functionality: property selection, and nested query embedding. Instance Query  | /cluster/{id}\_\_\_get |
| dns - Instance Query | dns Query a specific DNS setting. Instance Query  | /dns/{id}\_\_\_get |
| email_notify_destination - Instance Query | email_notify_destination Query a specific email notification destination. Instance Query  | /email\_notify\_destination/{id}\_\_\_get |
| eth_port - Instance Query | eth_port Get Ethernet front-end port configuration by instance identifier. Instance Query  | /eth\_port/{id}\_\_\_get |
| EventInstanceGet - Event summary | event Get event by Event Id. Event summary  | /event/{id}\_\_\_get |
| fc_port - Instance Query | fc_port Query a specific FC front-end port configuration. Instance Query  | /fc\_port/{id}\_\_\_get |
| file_dns - Instance Query | file_dns Query a specific DNS settings object of a NAS Server. Instance Query  | /file\_dns/{id}\_\_\_get |
| file_ftp - Instance Query | file_ftp Query a specific FTP/SFTP server for its settings. Instance Query  | /file\_ftp/{id}\_\_\_get |
| file_interface - Instance Query | file_interface Instance Query  | /file\_interface/{id}\_\_\_get |
| file_interface_route - Instance Query | file_interface_route Query a specific file interface route for details. Instance Query  | /file\_interface\_route/{id}\_\_\_get |
| file_kerberos - Instance Query | file_kerberos Query a specific Kerberos service settings of a NAS Server. Instance Query  | /file\_kerberos/{id}\_\_\_get |
| file_ldap - Instance Query | file_ldap Query a specific NAS Server's LDAP settings object. Instance Query  | /file\_ldap/{id}\_\_\_get |
| file_ndmp - Instance Query | file_ndmp Query an NDMP service configuration instance. Instance Query  | /file\_ndmp/{id}\_\_\_get |
| file_nis - Instance Query | file_nis Query a specific NIS settings object of a NAS Server. Instance Query  | /file\_nis/{id}\_\_\_get |
| file_system - Instance Query | file_system Query a specific file system. Instance Query  | /file\_system/{id}\_\_\_get |
| file_tree_quota - Instance Query | file_tree_quota Query a tree quota instance. Instance Query  | /file\_tree\_quota/{id}\_\_\_get |
| file_user_quota - Instance Query | file_user_quota Query a user quota instance. Instance Query  | /file\_user\_quota/{id}\_\_\_get |
| file_virus_checker - Instance Query | file_virus_checker Query a specific virus checker setting of a NAS Server. Instance Query  | /file\_virus\_checker/{id}\_\_\_get |
| hardware - Instance Query | hardware Get a specific hardware component instance. Instance Query  | /hardware/{id}\_\_\_get |
| host - Instance Query | host Get details about a specific host by id. Instance Query  | /host/{id}\_\_\_get |
| host_group - Instance Query | host_group Get details about a specific host group. Instance Query  | /host\_group/{id}\_\_\_get |
| host_virtual_volume_mapping - Instance Query | host_virtual_volume_mapping Query a specific virtual volume mapping. Instance Query  | /host\_virtual\_volume\_mapping/{id}\_\_\_get |
| host_volume_mapping - Instance Query | host_volume_mapping Query a specific host volume mapping. Instance Query  | /host\_volume\_mapping/{id}\_\_\_get |
| import_host_initiator - Instance Query | import_host_initiator Query a specific import host initiator instance. Instance Query  | /import\_host\_initiator/{id}\_\_\_get |
| import_host_system - Instance Query | import_host_system Query a specific import host system instance. Instance Query  | /import\_host\_system/{id}\_\_\_get |
| import_host_volume - Instance Query | import_host_volume Query a specific import host volume instance. Instance Query  | /import\_host\_volume/{id}\_\_\_get |
| import_psgroup - Instance Query | import_psgroup Query a specific PS Group storage array. Instance Query  | /import\_psgroup/{id}\_\_\_get |
| import_psgroup_volume - Instance Query | import_psgroup_volume Query a specific PS Group volume. Instance Query  | /import\_psgroup\_volume/{id}\_\_\_get |
| import_session - Instance Query | import_session Query a specific session. Instance Query  | /import\_session/{id}\_\_\_get |
| import_storage_center - Instance Query | import_storage_center Query a specific SC array. Instance Query  | /import\_storage\_center/{id}\_\_\_get |
| import_storage_center_consistency_group - Instance Query | import_storage_center_consistency_group Query a specific SC consistency group. Instance Query  | /import\_storage\_center\_consistency\_group/{id}\_\_\_get |
| import_storage_center_volume - Instance Query | import_storage_center_volume Query a specific SC volume. Instance Query  | /import\_storage\_center\_volume/{id}\_\_\_get |
| import_unity - Instance Query | import_unity Query a specific Unity storage system. Instance Query  | /import\_unity/{id}\_\_\_get |
| import_unity_consistency_group - Instance Query | import_unity_consistency_group Query a specific Unity consistency group. Instance Query  | /import\_unity\_consistency\_group/{id}\_\_\_get |
| import_unity_volume - Instance Query | import_unity_volume Query a specific Unity volume. Instance Query  | /import\_unity\_volume/{id}\_\_\_get |
| import_vnx_array - Instance Query | import_vnx_array Query a specific VNX storage system. Instance Query  | /import\_vnx\_array/{id}\_\_\_get |
| import_vnx_consistency_group - Instance Query | import_vnx_consistency_group Query a specific VNX consistency group. Instance Query  | /import\_vnx\_consistency\_group/{id}\_\_\_get |
| import_vnx_volume - Instance Query | import_vnx_volume Query a specific VNX volume. Instance Query  | /import\_vnx\_volume/{id}\_\_\_get |
| import_xtremio - Instance Query | import_xtremio Query a specific XtremIO storage system. Instance Query  | /import\_xtremio/{id}\_\_\_get |
| import_xtremio_consistency_group - Instance Query | import_xtremio_consistency_group Query a specific XtremIO consistency group. Instance Query  | /import\_xtremio\_consistency\_group/{id}\_\_\_get |
| import_xtremio_volume - Instance Query | import_xtremio_volume Query a specific XtremIO volume. Instance Query  | /import\_xtremio\_volume/{id}\_\_\_get |
| ip_pool_address - Instance Query | ip_pool_address Query a specific IP address. Instance Query  | /ip\_pool\_address/{id}\_\_\_get |
| ip_port - Instance Query | ip_port Query a specific IP port configuration. Instance Query  | /ip\_port/{id}\_\_\_get |
| job - Instance query | job Query a specific job. Instance query  | /job/{id}\_\_\_get |
| keystore_archive - Download a keystore backup archive file | keystore_archive Download a keystore backup archive file that was previously generated by a successful /api/rest/keystore_archive/regenerate POST command.  This resource type collection query does not support filtering, sorting or pagination Download a keystore backup archive file  | /keystore\_archive/{filename}\_\_\_get |
| ldap_account - Instance Query | ldap_account Query a specific LDAP account instance. Instance Query  | /ldap\_account/{id}\_\_\_get |
| ldap_domain - Instance Query | ldap_domain Query a specific LDAP domain. Instance Query  | /ldap\_domain/{id}\_\_\_get |
| license - Instance Query | license Query the specific license information for the cluster. Instance Query  | /license/{id}\_\_\_get |
| local_user - Instance Query | local_user Query a specific local user account instance using an unique identifier. Instance Query  | /local\_user/{id}\_\_\_get |
| maintenance_window - Instance Query | maintenance_window Query one appliance maintenance window configuration. Instance Query  | /maintenance\_window/{id}\_\_\_get |
| migration_recommendation - Instance query | migration_recommendation Get a single migration recommendation. Instance query  | /migration\_recommendation/{id}\_\_\_get |
| migration_session - Instance Query | migration_session Query a specific migration session. Instance Query  | /migration\_session/{id}\_\_\_get |
| nas_server - Instance Query | nas_server Query a specific NAS server. Instance Query  | /nas\_server/{id}\_\_\_get |
| network - Instance Query | network Query a specific IP network configuration. Instance Query  | /network/{id}\_\_\_get |
| nfs_export - Instance Query | nfs_export Get NFS Export properties. Instance Query  | /nfs\_export/{id}\_\_\_get |
| nfs_server - Instance Query | nfs_server Query settings of an NFS server. Instance Query  | /nfs\_server/{id}\_\_\_get |
| node - Instance Query | node Query a specific node in a cluster. Instance Query  | /node/{id}\_\_\_get |
| ntp - Instance Query | ntp Query a specific NTP setting. Instance Query  | /ntp/{id}\_\_\_get |
| performance_rule - Instance query | performance_rule Get a performance rule by id. Instance query  | /performance\_rule/{id}\_\_\_get |
| physical_switch - Instance Query | physical_switch Query a specific physical switch settings. Instance Query  | /physical\_switch/{id}\_\_\_get |
| policy - Instance Query | policy Query a specific policy. Instance Query  | /policy/{id}\_\_\_get |
| remote_system - Instance Query | remote_system Query a remote system instance.  Instance Query  | /remote\_system/{id}\_\_\_get |
| replication_rule - Instance Query | replication_rule Query a specific replication rule. Instance Query  | /replication\_rule/{id}\_\_\_get |
| replication_session - Instance Query | replication_session Query a replication session instance.  Instance Query  | /replication\_session/{id}\_\_\_get |
| role - Instance Query | role Query a specific role. Instance Query  | /role/{id}\_\_\_get |
| sas_port - Instance query | sas_port Query a specific SAS port configuration. Instance query  | /sas\_port/{id}\_\_\_get |
| security_config - Instance Query | security_config Query a specific system security configuration. Instance Query  | /security\_config/{id}\_\_\_get |
| service_config - Instance Query | service_config Query the service configuration instances for an appliance. Instance Query  | /service\_config/{id}\_\_\_get |
| service_user - Instance Query | service_user Query the service user account using the unique identifier. Instance Query  | /service\_user/{id}\_\_\_get |
| smb_server - Instance Query | smb_server Query settings of a specific SMB server. Instance Query  | /smb\_server/{id}\_\_\_get |
| smb_share - Instance Query | smb_share Get an SMB Share. Instance Query  | /smb\_share/{id}\_\_\_get |
| smtp_config - Instance Query | smtp_config Query the specific SMTP configuration. Instance Query  | /smtp\_config/{id}\_\_\_get |
| snapshot_rule - Instance Query | snapshot_rule Query a specific snapshot rule. Instance Query  | /snapshot\_rule/{id}\_\_\_get |
| software_installed - Instance Query | software_installed Query a specific item from the list of installed software. Instance Query  | /software\_installed/{id}\_\_\_get |
| software_package - Instance Query | software_package Query a specific software package. Instance Query  | /software\_package/{id}\_\_\_get |
| storage_container - Instance Query | storage_container Query a specific instance of storage container. Instance Query  | /storage\_container/{id}\_\_\_get |
| vcenter - Instance Query | vcenter Query a specific vCenter instance. Instance Query  | /vcenter/{id}\_\_\_get |
| veth_port - Instance Query | veth_port Query a specific virtual Ethernet port configuration. Instance Query  | /veth\_port/{id}\_\_\_get |
| virtual_machine - Instance Query | virtual_machine Query a specific virtual machine instance. Instance Query  | /virtual\_machine/{id}\_\_\_get |
| virtual_volume - Instance Query | virtual_volume Get a specific virtual volume. Instance Query  | /virtual\_volume/{id}\_\_\_get |
| volume - Instance Query | volume Query a specific volume instance. Instance Query  | /volume/{id}\_\_\_get |
| volume_group - Instance Query | volume_group Query a specific volume group, snapshot set, or clone. Instance Query  | /volume\_group/{id}\_\_\_get |
| x509_certificate - Instance Query | x509_certificate Query a specific X509 Certificate instance. Instance Query  | /x509\_certificate/{id}\_\_\_get |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |


### UPDATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| alert - Modify (PATCH) | alert Modify an alert. acknowledged_severity parameter, if included, will cause the request to fail when the alert's severity is higher than the acknowledged_severity parameter value. acknowledged_severity  is ignored when is_acknowledged is set to false. Modify  | /alert/{id}\_\_\_patch |
| appliance - Modify (PATCH) | appliance Modify an appliance's name. Modify  | /appliance/{id}\_\_\_patch |
| chap_config - Modify (PATCH) | chap_config Modify the CHAP configuration settings object. To enable either Single or Mutual CHAP modes, the username and password must already be set, or included in the same request as the new mode. Modify  | /chap\_config/{id}\_\_\_patch |
| cluster - Modify cluster properties (PATCH) | cluster Modify cluster properties, such as physical MTU.  Modify cluster properties  | /cluster/{id}\_\_\_patch |
| dns - Modify (PATCH) | dns Modify a DNS setting. Modify  | /dns/{id}\_\_\_patch |
| email_notify_destination - Modify (PATCH) | email_notify_destination Modify an email notification destination. Modify  | /email\_notify\_destination/{id}\_\_\_patch |
| eth_port - Modify (PATCH) | eth_port Change the properties of the front-end port. Note that setting the port's requested speed may not cause the port speed to change immediately. In cases where the SFP is not inserted or the port is down the requested speed will be set but the current_speed will still show the old value until the SFP is able to change speed. By default, the partner port speed on the other node in the appliance is set to the same requested speed. If the requested speed is not supported by the partner port it is left unchanged. Modify  | /eth\_port/{id}\_\_\_patch |
| fc_port - Modify (PATCH) | fc_port Modify an FC front-end port's speed. Setting the port's requested speed might not cause the port speed to change immediately. In cases where the Small Form-Factor Pluggable (SFP) is not inserted or the port is down, the requested speed is set, but the current_speed attribute shows the old value until the SFP is able to change speed. By default, the partner port speed on the other node in the appliance is set to the same requested speed. If the requested speed is not supported by the partner port, it is left unchanged. Modify  | /fc\_port/{id}\_\_\_patch |
| file_dns - Modify (PATCH) | file_dns Modify the DNS settings of a NAS Server. Modify  | /file\_dns/{id}\_\_\_patch |
| file_ftp - Modify (PATCH) | file_ftp Modify an FTP/SFTP server settings. Modify  | /file\_ftp/{id}\_\_\_patch |
| file_interface - Modify (PATCH) | file_interface Modify the settings of a file interface. Modify  | /file\_interface/{id}\_\_\_patch |
| file_interface_route - Modify (PATCH) | file_interface_route Modify file interface route settings. Modify  | /file\_interface\_route/{id}\_\_\_patch |
| file_kerberos - Modify (PATCH) | file_kerberos Modify the Kerberos service settings of a NAS Server. Modify  | /file\_kerberos/{id}\_\_\_patch |
| file_ldap - Modify (PATCH) | file_ldap Modify a NAS Server's LDAP settings object. Modify  | /file\_ldap/{id}\_\_\_patch |
| file_ndmp - Modify (PATCH) | file_ndmp Modify an NDMP service configuration instance. Modify  | /file\_ndmp/{id}\_\_\_patch |
| file_nis - Modify (PATCH) | file_nis Modify the NIS settings of a NAS Server. Modify  | /file\_nis/{id}\_\_\_patch |
| file_system - Modify (PATCH) | file_system Modify a file system. Modify  | /file\_system/{id}\_\_\_patch |
| file_tree_quota - Modify (PATCH) | file_tree_quota Modify a tree quota instance. Modify  | /file\_tree\_quota/{id}\_\_\_patch |
| file_user_quota - Modify (PATCH) | file_user_quota Modify a user quota instance. Modify  | /file\_user\_quota/{id}\_\_\_patch |
| file_virus_checker - Modify (PATCH) | file_virus_checker Modify the virus checker settings of a NAS Server. Modify  | /file\_virus\_checker/{id}\_\_\_patch |
| hardware - Modify (PATCH) | hardware Modify a hardware instance. Modify  | /hardware/{id}\_\_\_patch |
| host - Modify (PATCH) | host Operation that can be performed are modify name, modify description, remove initiator(s) from host, add initiator(s) to host, update existing initiator(s) with chap username/password. This will only support one of add, remove and update initiator operations in a single request. Modify  | /host/{id}\_\_\_patch |
| host_group - Modify (PATCH) | host_group Operations that can be performed are modify name, remove host(s) from host group, add host(s) to host group. Modify request will only support either a add_host(s) or remove_host(s) at a time along with modifying host name Modify  | /host\_group/{id}\_\_\_patch |
| import_session - Modify (PATCH) | import_session Modify the scheduled date and time of the specified import session. Modify  | /import\_session/{id}\_\_\_patch |
| ip_port - Modify (PATCH) | ip_port Modify IP port parameters. Modify  | /ip\_port/{id}\_\_\_patch |
| ldap_account - Modify (PATCH) | ldap_account Modify the properties of an LDAP account. Modify  | /ldap\_account/{id}\_\_\_patch |
| ldap_domain - Modify (PATCH) | ldap_domain Modify the properties of an LDAP domain. Modify  | /ldap\_domain/{id}\_\_\_patch |
| local_user - Modify (PATCH) | local_user Modify a property of a local user account using the unique identifier. You cannot modify the default "admin" user account. Modify  | /local\_user/{id}\_\_\_patch |
| maintenance_window - Modify (PATCH) | maintenance_window Configure maintenance window. Modify  | /maintenance\_window/{id}\_\_\_patch |
| nas_server - Modify (PATCH) | nas_server Modify the settings of a NAS server. Modify  | /nas\_server/{id}\_\_\_patch |
| network - Modify (PATCH) | network Modify IP network parameters, such as gateways, netmasks, VLAN identifiers, and IP addresses.  Modify  | /network/{id}\_\_\_patch |
| nfs_export - Modify (PATCH) | nfs_export Modify NFS Export Properties. Modify  | /nfs\_export/{id}\_\_\_patch |
| nfs_server - Modify (PATCH) | nfs_server Modify NFS server settings. Modify  | /nfs\_server/{id}\_\_\_patch |
| ntp - Modify (PATCH) | ntp Modify NTP settings. Modify  | /ntp/{id}\_\_\_patch |
| physical_switch - Modify (PATCH) | physical_switch Modify a physical switch settings. Modify  | /physical\_switch/{id}\_\_\_patch |
| policy - Modify (PATCH) | policy Modify a protection policy.  Modify  | /policy/{id}\_\_\_patch |
| remote_system - Modify (PATCH) | remote_system Modify a remote system instance. The list of valid parameters depends on the type of remote system.     For PowerStore remote system relationships:  * Description * Management address - An IPv4 or IPv6 address. FQDN is not supported.     For non-PowerStore remote system relationships:  * Name * Description * Management address - An IPv4 address. FQDN is not supported. * Remote administrator credentials * iSCSI address - An IPv4 address.     After modifying the remote session instance, the system reestablishes the data connections as needed.  Modify  | /remote\_system/{id}\_\_\_patch |
| replication_rule - Modify (PATCH) | replication_rule Modify a replication rule.  If the rule is associated with a policy that is currently applied to a storage resource, the modified rule is immediately applied to the associated storage resource.  Changing the remote_system_id is not permitted, if the rule is part of a policy that is currently applied to a storage resource. To change the remote system associated with a replication rule, do either of the following:      Remove the protection policy association from the relevant storage resources, modify the replication rule, and then associate the storage resources with the relevant protection policies.         Remove the replication rule from the protection policies that use it, modify the replication rule,         and then associate it back with the relevant protection policies.                Modify  | /replication\_rule/{id}\_\_\_patch |
| service_config - Modify (PATCH) | service_config Modify the service configuration for an appliance. Modify  | /service\_config/{id}\_\_\_patch |
| service_user - Modify (PATCH) | service_user Modify the properties of the service user account. Modify  | /service\_user/{id}\_\_\_patch |
| smb_server - Modify (PATCH) | smb_server Modify an SMB server's settings. Modify  | /smb\_server/{id}\_\_\_patch |
| smb_share - Modify (PATCH) | smb_share Modify SMB share properties. Modify  | /smb\_share/{id}\_\_\_patch |
| smtp_config - Modify (PATCH) | smtp_config Configure the outgoing SMTP information. Modify  | /smtp\_config/{id}\_\_\_patch |
| snapshot_rule - Modify (PATCH) | snapshot_rule Modify a snapshot rule.  If the rule is associated with a policy that is currently applied to a storage resource, the modified rule is immediately applied to that associated storage resource.  Modify  | /snapshot\_rule/{id}\_\_\_patch |
| storage_container - Modify (PATCH) | storage_container Modify a storage container. Modify  | /storage\_container/{id}\_\_\_patch |
| vcenter - Modify (PATCH) | vcenter Modify a vCenter settings. Modify  | /vcenter/{id}\_\_\_patch |
| virtual_machine - Modify (PATCH) | virtual_machine Modify a virtual machine. This operation cannot be used on virtual machine snapshots or templates. Modify  | /virtual\_machine/{id}\_\_\_patch |
| volume - Modify (PATCH) | volume Modify the parameters of a volume. Modify  | /volume/{id}\_\_\_patch |
| volume_group - Modify (PATCH) | volume_group Modify a volume group, snapshot set, or clone. Modify  | /volume\_group/{id}\_\_\_patch |


