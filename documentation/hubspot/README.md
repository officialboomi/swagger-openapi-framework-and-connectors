# HubSport CRM Platform
HubSpot?s developer platform is a core part of our mission to empower organizations to grow better. Our APIs are designed to enable teams of any shape or size to build robust integrations that help them customize and get the most value out of HubSpot.

All HubSpot APIs are built using REST conventions and designed to have a predictable URL structure. They use many standard HTTP features, including methods (POST, GET, PUT, DELETE) and error response codes. All HubSpot API calls are made under https://api.hubapi.com and all responses return standard JSON. For more information please click https://developers.hubspot.com/docs/api/overview

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string

**Default Value** - https://api.hubapi.com



#### HubSpot CRM Platform REST API Service URL

The URL for the HubSpot CRM Platform REST API Service server.

**Type** - string

##### Allowed Values

 * WEBHOOKS -- Webhooks
 * EVENTSEVENTS -- Events
 * COMMUNICATION-PREFERENCES -- Communications-status
 * ANALYTICS -- Custom Behavioral Events
 * CMS -- Domains
 * CMS -- Source Code
 * CMS -- Blog-posts
 * CMS -- Authors
 * CMS -- Url-redirects
 * CMS -- Performance
 * CMS -- Hubdb
 * CMS -- Tags
 * CMS -- Audit-logs
 * CMS -- Site-search
 * MARKETING -- Marketing-events-beta
 * MARKETING -- Transactional
 * AUTOMATION -- Actions
 * CONVERSATIONS -- Visitor Identification
 * CRM -- Crm Extensions
 * CRM -- Products
 * CRM -- Company Properties
 * CRM -- Pipelines
 * CRM -- Accounting
 * CRM -- Companies
 * CRM -- Calling
 * CRM -- Quotes
 * CRM -- Deals
 * CRM -- Contact Properties
 * CRM -- Imports
 * CRM -- Schemas
 * CRM -- Properties
 * CRM -- Associations
 * CRM -- Owners
 * CRM -- Timeline
 * CRM -- Deal Properties
 * CRM -- Contacts
 * CRM -- Feedback Submissions
 * CRM -- Objects
 * CRM -- Videoconferencing
 * CRM -- Tickets
 * CRM -- Line Items


#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Delete-/crm/v3/objects/companies/{companyId} Archive - Archive | Basic Move an Object identified by `{companyId}` to the recycling bin. Archive  |
| Delete-/crm/v3/objects/companies/{companyId}/associations/{toObjectType}/{toObjectId}/{associationType} Archive - Remove an association between two companies | Associations Remove an association between two companies  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Get-/crm/v3/objects/companies/{companyId} GetById - Read | Basic Read an Object identified by `{companyId}`. `{companyId}` refers to the internal object ID by default, or optionally any unique property value as specified by the `idProperty` query param.  Control what is returned via the `properties` query param. Read  |


### PATCH Operation Types
| Label | Help Text |
| --- | --- |
| Patch-/crm/v3/objects/companies/{companyId} Update - Update (PATCH) | Basic Perform a partial update of an Object identified by `{companyId}`. `{companyId}` refers to the internal object ID by default, or optionally any unique property value as specified by the `idProperty` query param. Provided property values will be overwritten. Read-only and non-existent properties will be ignored. Properties values can be cleared by passing an empty string. Update  |


### POST Operation Types
| Label | Help Text |
| --- | --- |
| Post-/crm/v3/objects/companies Create - Create | Basic Create a company with the given properties and return a copy of the object, including the ID. Documentation and examples for creating standard companies is provided. Create  |
| Post-/crm/v3/objects/companies/batch/archive Archive - Archive a batch of companies by ID | Batch Archive a batch of companies by ID  |
| Post-/crm/v3/objects/companies/batch/create Create - Create a batch of companies | Batch Create a batch of companies  |
| Post-/crm/v3/objects/companies/batch/read Read - Read a batch of companies by internal ID, or uniqu... | Batch Read a batch of companies by internal ID, or unique property values  |
| Post-/crm/v3/objects/companies/batch/update Update - Update a batch of companies | Batch Update a batch of companies  |
| Post-/crm/v3/objects/companies/search DoSearch | Search  |


### PUT Operation Types
| Label | Help Text |
| --- | --- |
| Put-/crm/v3/objects/companies/{companyId}/associations/{toObjectType}/{toObjectId}/{associationType} Create - Associate a company with another object (PUT) | Associations Associate a company with another object  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Get-/crm/v3/objects/companies GetPage - List | Basic Read a page of companies. Control what is returned via the `properties` query param. List  |
| Get-/crm/v3/objects/companies/{companyId}/associations/{toObjectType} GetAll - List associations of a company by type | Associations List associations of a company by type  |


