# Insperity OSS  
## REST interface to Insperity OSS  
**Spec:** resources/spec.json  
**Total Endpoints:** 28  
**Unsupported Endpoints:** 0  
### Errors  
## Operation EXECUTE-GET  
**Employees.query.v1.get - Public API Get all Employees for a Company**  
/company/{companyId}/employees/v1  
**Employees.query.v2.get - Public API Get all Employees for a Company**  
/company/{companyId}/employees/v2  
**Employeescommunication.query.v1.get - Public API Get all Employees for a Company includi...**  
/company/{companyId}/employeescommunication/v1  
**Employeescompany.query.v1.get - Public API Get all Employees for a Company includi...**  
/company/{companyId}/employeescompany/v1  
**Employeescompanymanageddata.query.v1.get - Public API Get all Employees for a Company includi...**  
/company/{companyId}/employeescompanymanageddata/v1  
**Employeescompensation.query.v1.get - Public API Get all Employees for a Company includi...**  
/company/{companyId}/employeescompensation/v1  
**Employeescompensation.query.v2.get - Public API Get all Employees for a Company includi...**  
/company/{companyId}/employeescompensation/v2  
**Employeesemployment.query.v1.get - Public API Get all Employees for a Company includi...**  
/company/{companyId}/employeesemployment/v1  
**Employeesposition.query.v1.get - Public API Get all Employees for a Company includi...**  
/company/{companyId}/employeesposition/v1  
**Employeespto.query.v1.get - Public API Get all Employees for a Company includi...**  
/company/{companyId}/employeespto/v1  
**Employeespto.query.v1.getsingle - Public API Get all Employees for a Company includi...**  
/company/{companyId}/employeespto/{resourceId}/v1  
## Operation EXECUTE-POST  
**Employee.addresschange.v1.post - Public API Employee Address Change**  
/employee/addresschange/v1  
**Employee.bulkclientemployeenumberchange.v1.post - Public API for Bulk Employee Client Employee Numbe...**  
/employee/bulkclientemployeenumberchange/v1  
**Employee.change.v1.post - Composite Public API for Employee Changes**  
/employee/change/v1  
**Employee.clientemployeenumberchange.v1.post - Public API Employee Client Employee Number Change**  
/employee/clientemployeenumberchange/v1  
**Employee.companymanageddatachange.v1.post - Public API Employee Company Managed Data Change.**  
/employee/companymanageddatachange/v1  
**Employee.deliverylocationchange.v1.post - Public API Employee Delivery Location Change**  
/employee/deliverylocationchange/v1  
**Employee.departmentchange.v1.post - Public API Employee Department Change**  
/employee/departmentchange/v1  
**Employee.emailchange.v1.post - Public API Employee Email Change**  
/employee/emailchange/v1  
**Employee.employmentstatuschange.v1.post - Public API Employee Employment Status Change**  
/employee/employmentstatuschange/v1  
**Employee.exemptionstatuschange.v1.post - Public API Employee Exemption Status Change**  
/employee/exemptionstatuschange/v1  
**Employee.jobtitlechange.v1.post - Public API Employee Job Title Change**  
/employee/jobtitlechange/v1  
**Employee.locationchange.v1.post - Public API Employee Location Change**  
/employee/locationchange/v1  
**Employee.phonechange.v1.post - Public API Employee Phone Change**  
/employee/phonechange/v1  
**Employee.reportinglocationchange.v1.post - Public API Employee Reporting Location Change**  
/employee/reportinglocationchange/v1  
**Employee.supervisesothersstatuschange.v1.post - Public API Employee Supervises Others Status Chang...**  
/employee/supervisesothersstatuschange/v1  
**Employee.supervisorchange.v1.post - Public API Employee Supervisor Change**  
/employee/supervisorchange/v1  
**Employee.worksitelocationchange.v1.post - Public API Employee Worksite Location Change**  
/employee/worksitelocationchange/v1  
## Operation EXECUTE-PUT  
## Operation EXECUTE-DELETE  
## Operation EXECUTE-PATCH  
## Operation QUERY-  
