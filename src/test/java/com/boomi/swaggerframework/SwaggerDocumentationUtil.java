package com.boomi.swaggerframework;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.DefaultNodeMatcher;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.Difference;
import org.xmlunit.diff.ElementSelectors;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.util.StringUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;

public class SwaggerDocumentationUtil {
    protected static Logger logger = Logger.getLogger("SwaggerDocumentationUtil");
	public static String toPrettyXML(Node document) {
		if (document==null)
			return null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
//			Document document = DocumentHelper.parseText("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+node.asXML());
			OutputFormat format = new OutputFormat();
			format.setEncoding(StandardCharsets.UTF_8.name());
			format.setIndent(true);
			format.setIndentSize(2);
	        format.setNewlines(true);
			XMLWriter writer = new XMLWriter(baos, format);
			writer.write(document);
			writer.close();
			return baos.toString(StandardCharsets.UTF_8.name()).trim();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static String getSingleNode(Node node, String path)
	{
		node = node.selectSingleNode(path);
		if (node != null)
			return node.getText();
		return null;
	}
	
	public static void documentAPI(JSONObject swagger, FileWriter writer) throws IOException
	{
		JSONObject paths = swagger.getJSONObject("paths");
		Iterator<String> pathIter = paths.keys();
		
		String hasQuery="|  ";
		String hasGet="|  ";
		String hasCreate="|  ";
		String hasUpdate="|  ";
		String hasDelete="|  ";
		
		while(pathIter.hasNext())
		{
			String pathString=pathIter.next();
			JSONObject path = paths.getJSONObject(pathString);
			Iterator<String> methodIter = path.keys();
			
			while(methodIter.hasNext())
			{
				String methodString = methodIter.next().toLowerCase();	
				if (SwaggerBrowseUtil.isQuery(methodString, pathString))
					hasQuery="| X ";
				else if ("get".contentEquals(methodString.toLowerCase()))
					hasGet="| X ";
				
				if (SwaggerBrowseUtil.isCreate(methodString, pathString))
					hasCreate="| X ";
				if (SwaggerBrowseUtil.isUpdate(methodString, pathString))
					hasUpdate="| X ";
				if ("delete".contentEquals(methodString.toLowerCase()))
					hasDelete="| X ";
			}
		}
		writer.write(hasQuery);
		writer.write(hasGet);
		writer.write(hasCreate);
		writer.write(hasUpdate);
		writer.write(hasDelete);	
	}
	
	//Report on the following conditions that require non-standard implementations
	//1 .GET methods that have paths with parameters that do not end in id, aka candidate query operations that can not be supported by SDK because Query Operations can not accept parameters
	//2. GET and DELETE methods with multiple parameter paths which can not be implemented because platform supports only one id, EXECUTE must be used and ids must be put in the input profile 
	//3. Illegal DELETE, PATCH or PUT with no parameter
	//4. POST methods that have path parameter at the end of the path
	//5. PATCH and PUT methods that do not have an parameter at the end of the path
	public static void analyzeOpenAPI(JSONObject swagger)
	{
		JSONObject paths = swagger.getJSONObject("paths");
		Iterator<String> pathIter = paths.keys();
		
		boolean hasQuery=false;
		boolean hasGet=false;
		boolean hasCreate=false;
		boolean hasUpdate=false;
		boolean hasDelete=false;
		
		boolean hasUpdatePatch=false;
		boolean hasUpdatePut=false;
		boolean hasUpdatePost=false;
		
		while(pathIter.hasNext())
		{
			String pathString=pathIter.next();
			JSONObject path = paths.getJSONObject(pathString);
			Iterator<String> methodIter = path.keys();
			
			while(methodIter.hasNext())
			{
				String methodString = methodIter.next().toLowerCase();	
				if (SwaggerBrowseUtil.isQuery(methodString, pathString))
					hasQuery=true;
				if (SwaggerBrowseUtil.isGet(methodString, pathString))
					hasGet=true;
				if (SwaggerBrowseUtil.isDelete(methodString, pathString))
					hasDelete=true;
				if (SwaggerBrowseUtil.isCreate(methodString, pathString))
					hasCreate=true;
				if (SwaggerBrowseUtil.isUpdate(methodString, pathString))
				{
					hasUpdate=true;
					switch (methodString) {
					case "patch":
						hasUpdatePatch=true;
//						System.out.println(String.format("PATCH update - Path: %s, Method: %s", pathString, methodString));
						break;
					case "put":
						hasUpdatePut=true;
//						System.out.println(String.format("PUT update - Path: %s, Method: %s", pathString, methodString));
						break;
					case "post":
//						hasUpdatePost=true;
						System.out.println(String.format("POST update - Path: %s, Method: %s", pathString, methodString));
						break;
					}
				}
				if (false && !methodString.contentEquals("parameters"))
				{ 
					//Case 1
//					if (SwaggerBrowseUtil.isCustomQuery(methodString, pathString))
//						System.out.println(String.format("QUERY operation with url parameters is not supported - Path: %s, Method: %s", pathString, methodString));
					//Case 2
					if (SwaggerBrowseUtil.isDelete(methodString, pathString))
						System.out.println(String.format("DELETE operations with multiple parameters are not supported by the standard DELETE operation, custom EXECUTE required - Path: %s, Method: %s", pathString, methodString));
					if (SwaggerBrowseUtil.isGet(methodString, pathString))
						System.out.println(String.format("GET operations with multiple parameters are not supported by the standard GET operation, custom EXECUTE required - Path: %s, Method: %s", pathString, methodString));
					//Case 3
					if (methodString.toLowerCase().contentEquals("delete") && SwaggerBrowseUtil.getNumberOfPathParameters(pathString)==0)
						System.out.println(String.format("Warning: DELETE method has no parameters - Path: %s, Method: %s", pathString, methodString));
					if (methodString.toLowerCase().contentEquals("patch") && SwaggerBrowseUtil.getNumberOfPathParameters(pathString)==0)
						System.out.println(String.format("Warning: PATCH method has no parameters - Path: %s, Method: %s", pathString, methodString));
					if (methodString.toLowerCase().contentEquals("put") && SwaggerBrowseUtil.getNumberOfPathParameters(pathString)==0)
						System.out.println(String.format("Warning: PUT method has no parameters - Path: %s, Method: %s", pathString, methodString));
					//Case 4
					if (methodString.toLowerCase().contentEquals("post") && pathString.endsWith("}"))
						System.out.println(String.format("Warning POST/CREATE Operation without parameter at end of path - Path: %s, Method: %s", pathString, methodString));
					//Case 5
					if (methodString.toLowerCase().contentEquals("PATCH") && !pathString.endsWith("}"))
						System.out.println(String.format("Warning PATCH Operation with parameter at end of path - Path: %s, Method: %s", pathString, methodString));
					if (methodString.toLowerCase().contentEquals("PUT") && !pathString.endsWith("}"))
						System.out.println(String.format("Warning PUT Operation with parameter at end of path - Path: %s, Method: %s", pathString, methodString));
				}
			}
		}
		if (hasQuery)
			System.out.println("hasQuery: "+hasQuery);
		if (hasGet)
			System.out.println("hasGet: "+hasGet);
		if (hasCreate)
			System.out.println("hasCreate: "+hasCreate);
		if (hasUpdate)
			System.out.println("hasUpdate: "+hasUpdate);
		if (hasDelete)
			System.out.println("hasDelete: "+hasDelete);
		if (hasUpdatePatch)
			System.out.println("hasUpdatePatch: "+hasUpdatePatch);
		if (hasUpdatePut)
			System.out.println("hasUpdatePut: "+hasUpdatePut);
		if (hasUpdatePost)
			System.out.println("hasUpdatePost: "+hasUpdatePost);
	}
	
	public static void analyzeSwagger(String connectorName) throws JSONException, FileNotFoundException {
		System.out.println("");
		System.out.println(connectorName.toUpperCase());
		
		File folder = new File("src/main/java/resources/"+connectorName);
		File[] listOfFiles = folder.listFiles();

		for (File file : listOfFiles) {
		    if (file.isFile() && file.getName().endsWith(".json")) {
		        System.out.println(file.getName());
				JSONObject fullSwaggerSchema = new JSONObject(new JSONTokener(new FileInputStream(file)));
				SwaggerDocumentationUtil.analyzeOpenAPI(fullSwaggerSchema);
		    }
		}
	}	    
	
	private static void createDocumentationDirectory(String connectorName)
	{
		String dirName = "documentation/"+connectorName;
        File dir = new File(dirName);
        if (!dir.exists())
        	dir.mkdirs();
	}
	
	public static void generateMultiSwaggerWikiDoc(String connectorName) throws JSONException, IOException {
		
		createDocumentationDirectory(connectorName);
		FileWriter writer = new FileWriter("documentation/"+connectorName+"/README.md", true);

		writer.write("\n## Object Operations Provided\n\n");
		writer.write("| Object | Query | Get | Create | Update | Delete \n");
		writer.write("| --- |:---:|:---:|:---:|:---:|:---:|\n");
		
		File folder = new File("src/main/java/resources/"+connectorName);
		File[] listOfFiles = folder.listFiles();

		for (File file : listOfFiles) {
		    if (file.isFile() && file.getName().endsWith(".json")) {
		    	String name = file.getName().substring(0,file.getName().indexOf("."));
		    	writer.write("| " + name + " ");
				JSONObject fullSwaggerSchema = new JSONObject(new JSONTokener(new FileInputStream(file)));
				SwaggerDocumentationUtil.documentAPI(fullSwaggerSchema, writer);
				writer.write("|\n");
		    }
		}
		writer.flush();
		writer.close();
	}	   
	
    public static void prettyPrintTypes(String operationName, Document typesDocument, FileWriter writer) throws DocumentException, IOException
    {
    	writer.write("### " + operationName.toUpperCase() + " Operation Types\n");
//    	writer.write("| Label | Help Text | ID |\n");
//    	writer.write("| --- | --- | --- |\n");
    	writer.write("| Label | Help Text |\n");
    	writer.write("| --- | --- |\n");
        List<Node> nodes = typesDocument.selectNodes("/ObjectTypes/*");
        
        for (Node node : nodes)
        {
        	Element element = (Element)node;
        	String id = element.attributeValue("id").replace("_", "\\_");
        	String helpText = getNodeText(node,"helpText").replace("\r","").replace("\n", " ").replace("\t", " ").replace("|", " ");
        	String label = element.attributeValue("label").replace("\r","").replace("\n", " ").replace("\t", " ").replace("|", " ");

//        	writer.write(String.format("| %s | %s | %s |\n", label, helpText, id));
        	writer.write(String.format("| %s | %s |\n", label, helpText, id));
        }
        writer.write("\n\n");
//    	<ObjectTypes>
//    	  <type id="/api/v1/users/{userId}/sessions" label="ClearUserSessions - Removes all active identity provider sessions. Thi...">
//    	    <helpText>Removes all active identity provider sessions. Thi...</helpText>
//    	  </type>   	
 //   	 getSingleNode(actualDoc,"/ObjectDefinitions/definition[@inputType='json']/jsonSchema");
    }
	public static void generateOperationTypeDocs(String connectorName) throws JSONException, IOException, DocumentException {
		generateOperationTypeDocs(connectorName, "");
	}
	
//	public static void generateOperationTypeDocsMultiSwagger(Connector connector, String connectorName, OperationType operationType, String customOperationType,  Map<String, Object> connProps,  Map<String, Object> opProps, String swaggerPath, String module)
//	{
//		ConnectorTester tester = new ConnectorTester(connector);
//		FileWriter writer = new FileWriter("documentation/"+connectorName+"/"+module+"README.md", true);
//
//        String actual;
//        if (StringUtil.isNotBlank(swaggerPath))
//        {
//        	connProps = new HashMap<String,Object>();           
//        	connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
//        }
//        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, operationType, customOperationType, connProps, opProps);
//        tester.setBrowseContext(sbc);
////        tester.setBrowseContext(operationType, connProps, null);
//        actual = tester.browseTypes();
//        
//        SAXReader reader = new SAXReader();
//		Document document = reader.read(new ByteArrayInputStream(actual.getBytes()));
//    	prettyPrintTypes(operation, document, writer);
//	}
	public static void generateOperationTypeDocs(String connectorName, String module) throws JSONException, IOException, DocumentException {
		
		createDocumentationDirectory(connectorName);
		FileWriter writer = new FileWriter("documentation/"+connectorName+"/"+module+"README.md", true);

		writer.write("\n# "+module + "\n\n");
		writer.write("\n# Operations and Object Types Provided\n\n");

		File folder = new File("src/test/java/resources/"+connectorName+"/expected");
		File[] listOfFiles = folder.listFiles();

		for (File file : listOfFiles) {
		    if (file.isFile()) {
		    	logger.info(file.getName());
		    	String operation=null;
		    	if(file.getName().endsWith(module+"_POST.xml"))
		    		operation="POST";
		    	else if(file.getName().endsWith(module+"_PUT.xml"))
		    		operation="PUT";
		    	else if(file.getName().endsWith(module+"_PATCH.xml"))
		    		operation="PATCH";
		    	else if(file.getName().endsWith(module+"_DELETE.xml"))
		    		operation="DELETE";
		    	else if(file.getName().endsWith(module+"_GET.xml"))
		    		operation="GET";
		    	else if(file.getName().endsWith(module+"_QUERY.xml"))
		    		operation="QUERY";
		    	if (operation!=null)
		    	{
			        SAXReader reader = new SAXReader();
					Document document = reader.read(file);
			    	prettyPrintTypes(operation, document, writer);
		    	}
		    }
		}
		writer.flush();
		writer.close();
	}	   
	
	private static String getNodeText(Node field, String elementName)
	{
		if (field.selectSingleNode(elementName)!=null)
		{
			String text = field.selectSingleNode(elementName).getText();
			if (text!=null && text.trim().length()>0)
				return text;
		}		
		return " **" + elementName + " NOT SET IN DESCRIPTOR FILE**";
	}
	
	private static void writeDescriptorFields(List<Node> fields, StringBuilder sb, String indent)
	{
        for (Node field : fields)
        {
        	Element fieldElement = (Element)field;
        	sb.append("\n\n#" + indent +" "+fieldElement.attributeValue("label")+"\n\n");
        	sb.append(getNodeText(field,"helpText")+"\n\n");
        	if (fieldElement.attributeValue("type")!=null)
        		sb.append("**Type** - " + fieldElement.attributeValue("type")+"\n\n");
        	if (fieldElement.attributeValue("formatRegex")!=null)
        		sb.append("**Format** - " + fieldElement.attributeValue("formatRegex")+"\n\n");
        	if (field.selectSingleNode("defaultValue")!=null && field.selectSingleNode("defaultValue").getText().length()>0)
        		sb.append("**Default Value** - " + field.selectSingleNode("defaultValue").getText()+"\n\n");
        	List<Node> allowedValues = field.selectNodes("allowedValue");
        	if (allowedValues.size()>0)
            	sb.append("##"+indent + " Allowed Values\n\n");
        	for (Node allowedValue : allowedValues)
        	{
            	String label =  ((Element)allowedValue).attributeValue("label");
            	if (label!=null && label.trim().length()>0)
            		sb.append(" * " + label +"\n");
           	}
        }
	}
	
	public static void generateDescriptorDoc(String productName, String connectorName) throws DocumentException, IOException
	{
		File descriptorFile = new File("src/main/java/resources/"+connectorName+"/descriptor-"+connectorName+".xml");
		StringBuilder sb=new StringBuilder();
        SAXReader reader = new SAXReader();
		Document document = reader.read(descriptorFile);
        
		createDocumentationDirectory(connectorName);
        sb.append("# "+productName+"\n");
    	sb.append(getNodeText(document,"/GenericConnectorDescriptor/description")+"\n\n");
        
        sb.append("# Connection Tab\n");
        List<Node> fields = document.selectNodes("/GenericConnectorDescriptor/field");
        if (fields.size()>0)
        {
            sb.append("## Connection Fields\n");
            writeDescriptorFields(fields, sb, "###");
        }
        sb.append("# Operation Tab\n");
        List<Node> operations = document.selectNodes("/GenericConnectorDescriptor/operation");
        for (Node operation : operations)
        {
        	Element operationElement = (Element)operation;
        	String operationName=operationElement.attributeValue("types");
        	String label=operationName;
        	if (operationElement.attributeValue("customTypeLabel")!=null)
        		label=operationElement.attributeValue("customTypeLabel");
        	sb.append("\n\n## "+label+"\n");
            List<Node> operationFields = operation.selectNodes("field");
            if (operationFields.size()>0)
            {
                sb.append("### Operation Fields\n");
                writeDescriptorFields(operationFields, sb, "####");
            }
            if (operationName.contentEquals("QUERY"))
    		{
            	sb.append("\n### Query Options\n\n");

            	sb.append("\n#### Fields\n\n");
            	// <operation types="QUERY" allowFieldSelection="false" fieldSelectionLevels="0" fieldSelectionNone="false">
            	if ("true".contentEquals(operationElement.attributeValue("allowFieldSelection")))
            	{
            		sb.append("Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.");
            	} else {
            		sb.append("The connector does not support field selection. All fields will be returned by default.");
            	}
            	sb.append("\n");
            	sb.append("\n");
//        		<query filter grouping="none" sorting="none">
//    			<operator id="eq" label="Equal To">  
//    			</operator>
//    			</query filter>
            	
            	Node filter = operation.selectSingleNode("queryFilter");
            	if (filter!=null)
            	{
                	sb.append("\n#### Filters\n\n");
            		Element filterElement = (Element)filter;
            		String grouping = filterElement.attributeValue("grouping");
            		if (grouping==null)
            			grouping="none";
                	switch (grouping)
                	{
                	case "none":
                		sb.append("The query filter supports no groupings (only one expression allowed).\r\n" + 
                				"\r\n" + 
                				"Example:\r\n" + 
                				"(foo lessThan 30)");
                		break;
                	case "any":
                		sb.append(" The query filter supports any arbitrary grouping and nesting of AND's and OR's.\r\n" + 
                				"\r\n" + 
                				"Example:\r\n" + 
                				"((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)");
                		break;
                	case "noNestingImplicitOr":
                		sb.append(" The query filter supports any number of non-nested expressions which will be OR'ed together.\r\n" + 
                				"\r\n" + 
                				"Example:\r\n" + 
                				"((foo lessThan 30) OR (baz lessThan 42))");
                		break;
                	case "noNestingImplicitAnd":
                		sb.append("The query filter supports any number of non-nested expressions which will be AND'ed together.\r\n" + 
                				"\r\n" + 
                				"Example:\r\n" + 
                				"((foo lessThan 30) AND (baz lessThan 42))");
                		break;
                	case "singleNestingImplicitOr":
                		sb.append("The query filter supports one level of nesting, where the top level groupings will be OR'ed together and the nested expression groups will be AND'ed together.\r\n" + 
                				"\r\n" + 
                				"Example:\r\n" + 
                				"((foo lessThan 30) AND (baz lessThan 42)) OR (buzz greaterThan 55)");
                		break;
                	case "singleNestingImplicitAnd":
                		sb.append("The query filter supports one level of nesting, where the top level groupings will be AND'ed together and the nested expression groups will be OR'ed together.\r\n" + 
                				"\r\n" + 
                				"Example:\r\n" + 
                				"((foo lessThan 30) OR (baz lessThan 42)) AND (buzz greaterThan 55)");
                		break;
                	}
                	sb.append("\n");
                	sb.append("\n");
                	sb.append("#### Filter Operators\n\n");
                    List<Node> operators = filter.selectNodes("operator");
                    for (Node operator : operators)
                    {
                    	Element operatElement = (Element)operator;
                    	sb.append(" * " + ((Element)operator).attributeValue("label")+" ");
                    	List<Node> types = operator.selectNodes("supportedType");
                    	if (types!=null && types.size()>0)
                    	{
                        	boolean firstType=true;
                        	sb.append(" (Supported Types: ");
                            for (Node type : types)
                            {
                               	if (!firstType)
                            		sb.append(",");
                        		firstType=false;
                        		sb.append(" " + ((Element)type).attributeValue("type"));
                            }
                        	sb.append(") ");
                    	}
                    	sb.append(getNodeText(operator,"helpText")+"\n\n");
                    }
                    
//        			<operator id="greaterOrEqual" label="Greater Than Or Equal">
//                	<helpText>
//    				<supportedType type="date" />
//    			</operator>
                	sb.append("\n");
                	sb.append("\n");
                	sb.append("#### Sorts\n\n");
            		String sorting = filterElement.attributeValue("sorting");
            		if (sorting==null)
            			sorting="none";
                	switch (grouping)
                	{
                	case "none":
                		sb.append("The query operation does not support sorting.");
                		break;
                	case "one":
                		sb.append("The query operation supports one sorting statement. ");
                		break;
                	case "unbounded":
                		sb.append("The query operation supports any number of sorting levels. For example you can specify to sort by Last Name, First Name. ");
                		break;
                	}
                	sb.append("\n");
                	sb.append("\n");
                    if (!"none".contentEquals(sorting))
                    {
                        List<Node> sortOrderFields = filter.selectNodes("sortOrder");
                        if (sortOrderFields!=null && sortOrderFields.size()>1)
                        {
                        	sb.append("The sort order can be set to either ascending and descinding.");
                        } else {
                        	sb.append("Only one direction of sorting is supported.");
                        }
                    }
//    			<sortOrder id="asc" label="Ascending"/>
//    			<sortOrder id="desc" label="Descending"/>
            	}
            	else
            	{
                	sb.append("\n#### Filters\n\n");
            		sb.append("The connector does not support filtering documents\n");
                	sb.append("\n#### Sorts\n\n");
            		sb.append("The connector does not support sorting documents\n");
            	}
            	sb.append("\n");
    		}
        }
        sb.append("# Inbound Document Properties\n");
        
        List<Node> inputFields = document.selectNodes("/GenericConnectorDescriptor/dynamicProperty");
        if (inputFields!=null && inputFields.size()>0)
        {
        	sb.append("Inbound document properties can set by a process before a connector shape to control options supported by the connector.\n\n");
            for (Node field : inputFields)
            {
            	Element fieldElement = (Element)field;
            	sb.append(" * **"+fieldElement.attributeValue("label")+"** - ");
            	sb.append(getNodeText(field,"helpText").replace("\r","").replace("\n", " ").replace("\t", " ").replace("|", " ")+"\n\n");
            }
        } else {
        	sb.append("The connector does not support inbound document properties that can be set by a process before an connector shape.\n");
        }
        	
        sb.append("# Outbound Document Properties\n");
        
        List<Node> outputFields = document.selectNodes("/GenericConnectorDescriptor/trackedProperty");
        if (outputFields!=null && outputFields.size()>0)
        {
        	sb.append("Outbound document properties can used by a process after a connector shape to access information set by the connector.\n\n");
            for (Node field : outputFields)
            {
            	Element fieldElement = (Element)field;
            	sb.append(" * **"+fieldElement.attributeValue("label")+"** - ");
            	sb.append(getNodeText(field,"helpText").replace("\r","").replace("\n", " ").replace("\t", " ").replace("|", " ")+"\n\n");
            }
        } else {
        	sb.append("The connector does not support outbound document properties that can be read by a process after a connector shape.\n\n");
        }
        	
		FileWriter writer = new FileWriter("documentation/"+connectorName+"/README.md");
		writer.write(sb.toString());
		writer.flush();
		writer.close();
	}
	
	
}
