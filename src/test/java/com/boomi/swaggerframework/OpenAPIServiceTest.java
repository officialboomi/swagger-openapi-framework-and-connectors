package com.boomi.swaggerframework;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;


class OpenAPIServiceTest {
	
	
	@Test
	void testGetRequestBodyType() throws JSONException, Exception {
		JSONObject fullSwaggerSchema = new JSONObject(new JSONTokener(this.getClass().getClassLoader().getResourceAsStream("resources/mpesa/MPESASwagger.json")));
		
		SwaggerAPIService swaggerService = new SwaggerAPIService("/stkpush/v1/processrequest", "POST", fullSwaggerSchema, 30);
		//get the request json schema
		JSONObject type = swaggerService.getRequestBodySchema();
//		System.out.println(type.toString());
//		assertTrue(type.toString().length()>10);
	}

//	@Test
//	void testCookieCompression() throws JSONException, Exception {
//		JSONObject fullSwaggerSchema = new JSONObject(new JSONTokener(this.getClass().getClassLoader().getResourceAsStream("resources/mpesa/MPESASwagger.json")));
//		SwaggerAPIService swaggerService = new SwaggerAPIService("/stkpush/v1/processrequest", fullSwaggerSchema, "");
//		//Recreate from a compressed path
//		swaggerService = new SwaggerAPIService("/stkpush/v1/processrequest", swaggerService.getPathCookie());
//		assertTrue(swaggerService.getMethodObject().toString().length()>100);
//	}
}
