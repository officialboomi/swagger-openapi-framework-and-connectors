package com.boomi.swaggerframework;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.DefaultNodeMatcher;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.Difference;
import org.xmlunit.diff.ElementSelectors;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestUtil
{
//	public static Map<ObjectDefinitionRole, String> getCookieList(ConnectorCookie cookie, ObjectDefinitionRole role) throws JsonProcessingException
//	{
//		HashMap<ObjectDefinitionRole, String> map = new HashMap<ObjectDefinitionRole, String>();
//		ObjectMapper mapper = new ObjectMapper();
//		map.put(role, mapper.writeValueAsString(cookie));
//		return map;
//	}
	
    static String inputStreamToString(InputStream is) throws IOException
    {
    	try (Scanner scanner = new Scanner(is, "UTF-8")) {
    		return scanner.useDelimiter("\\A").next();
    	}
    }

	public static String readResource(String resourcePath, Class theClass) throws Exception
	{
		String resource = null;
		try {
			InputStream is = theClass.getClassLoader().getResourceAsStream(resourcePath);
			resource = inputStreamToString(is);
			
		} catch (Exception e)
		{
			throw new Exception("Error loading resource: "+resourcePath + " " + e.getMessage());
		}

		return resource;
	}

//	public boolean equals(Object o)
//	{
//	    if (o == this) return true;
//	    if (o == null) return false;
//	    if (o.getClass() != getClass()) {
//	        return false;
//	    }
//	    ObjectNode other = (ObjectNode) o;
//	    if (other.size() != size()) {
//	        return false;
//	    }
//	    if (_children != null) {
//	        for (Map.Entry<String, JsonNode> en : _children.entrySet()) {
//	            String key = en.getKey();
//	            JsonNode value = en.getValue();
//
//	            JsonNode otherValue = other.get(key);
//
//	            if (otherValue == null || !otherValue.equals(value)) {
//	                return false;
//	            }
//	        }
//	    }
//	    return true;
//	}
}