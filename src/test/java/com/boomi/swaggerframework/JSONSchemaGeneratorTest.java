package com.boomi.swaggerframework;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.swaggerutil.JSONSchemaGenerator;

public class JSONSchemaGeneratorTest {
    @Test
    public void testJSONSchemaGeneorator() throws IOException
    {
    	String json = "{\"id\":\"6bcf6a58-bb97-437a-9823-f72d8a8c02bb\",\"email\":\"Adrian.james@traffiglove.com\",\"displayName\":\"AdrianJ\",\"isActive\":true,\"roles\":[\"CompanyAdmin\",\"CompanyReportViewer\"]}"; 
    	String schema = JSONSchemaGenerator.outputAsString("TEST", "", json);
    	System.out.println(schema);
    }
    

}
