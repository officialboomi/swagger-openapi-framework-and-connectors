// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.dropbox.DropboxConnector;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.blackboard.BlackboardConnector;
import com.boomi.connector.oraclesaas.OracleSaaSConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.ConnectionProperties;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;

/**
 * @author Dave Hock
 */
public class DropboxBrowseTest {
    static final String connectorName = "dropbox";
    private static final boolean captureExpected = false;
//	String swaggerPath = "resources/"+connectorName+"/user_v2.json";
	String swaggerPath = "resources/"+connectorName+"/team_v2.json";
    
    @Test
    public void testBrowseTypesDropbox() throws JSONException, Exception
    {
        DropboxConnector connector = new DropboxConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, "", getClass(), captureExpected);
    }   
    
    @Test
    public void testBrowseDefinitionsCreate() throws JSONException, Exception
    {
    	DropboxConnector connector = new DropboxConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 20L);
       
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "POST", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/team/team_folder/create");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreate", getClass(), connectorName, captureExpected);
    } 
    
  //  @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("Dropbox REST API Connector", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}
