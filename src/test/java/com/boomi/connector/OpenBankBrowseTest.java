// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.openbank.OpenBankConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class OpenBankBrowseTest {
    static final String connectorName = "openbank";
    private static final String swaggerPath = "resources/openbank/openbank-account-info-swagger.json";
    private static final boolean captureExpected = false;
    
    @Test
    public void testBrowseTypes() throws JSONException, Exception
    {
    	OpenBankConnector connector = new OpenBankConnector();
 
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
    
    //TODO In both request and response, Risk is an Object with no properties
    //Response has pagination links????
    @Test
    public void testBrowseDefinitionsCreateAccountAccessConsents() throws JSONException, Exception
    {
    	OpenBankConnector connector = new OpenBankConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "POST", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/account-access-consents");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateAccountAccessConsents", getClass(), connectorName, captureExpected);
    }	
    
    //TODO pagination path has account name so can't resolve the pagination item
    //properties/Data/properties/Account/items/*"
    @Test
    public void testBrowseDefinitionsQueryAccounts() throws JSONException, Exception
    {
    	OpenBankConnector connector = new OpenBankConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/accounts");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryAccounts", getClass(), connectorName, captureExpected);
    }	
    
 //   @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("Openbank Connector", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}
