// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.cybersource.CybersourceConnection;
import com.boomi.connector.cybersource.CybersourceConnector;
import com.boomi.swaggerframework.MockOperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class CybersourceOperationTest 
{
    private static final String URL = "https://apitest.cybersource.com";
    private static final String MERCHANTID = "djwocky";
    private static final String KEYID = "f2eda493-1fbf-4927-9405-65b948f306e0";
    private static final String KEYVALUE = "cLIRnLsLsK9x3Ym6dnR2R/uy/RkoBd1uecLiHKblWfc=";

    //TODO we need the swagger for Transaction Search
    @Test
    public void testGetPaymentOperation() throws Exception
    {
    	String paymentsCookie = "{\"basePath\" : \"\"}";
    	String objectTypeId = "/tss/v2/transactions/{id}";
        CybersourceConnector connector = new CybersourceConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.name(), URL);
        connProps.put(CybersourceConnection.CyberSourceConnectionProperties.MERCHANTID.toString(), MERCHANTID);
        connProps.put(CybersourceConnection.CyberSourceConnectionProperties.KEYID.toString(), KEYID);
        connProps.put(CybersourceConnection.CyberSourceConnectionProperties.KEYVALUE.toString(), KEYVALUE);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.OUTPUT, paymentsCookie);
			
        opProps.put("path_id","6354352107666673104002");

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, objectTypeId, cookieMap, null);
        context.setCustomOperationType("GET");	
        tester.setOperationContext(context);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream("".getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
    }

    @Test   
    public void testCreatePaymentOperation() throws Exception
    {
    	String objectTypeID = "/v2/payments/";
    	String paymentsCookie = "{\"basePath\" : \"/pts\"}";
        CybersourceConnector connector = new CybersourceConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.name(), URL);
        connProps.put(CybersourceConnection.CyberSourceConnectionProperties.MERCHANTID.toString(), MERCHANTID);
        connProps.put(CybersourceConnection.CyberSourceConnectionProperties.KEYID.toString(), KEYID);
        connProps.put(CybersourceConnection.CyberSourceConnectionProperties.KEYVALUE.toString(), KEYVALUE);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.INPUT, paymentsCookie);
				
		String payload = "{\n" + 
				"  \"clientReferenceInformation\": {\n" + 
				"    \"code\": \"TC50171_3\"\n" + 
				"  },\n" + 
				"  \"processingInformation\": {\n" + 
				"    \"commerceIndicator\": \"internet\"\n" + 
				"  },\n" + 
				"  \"paymentInformation\": {\n" + 
				"    \"card\": {\n" + 
				"      \"number\": \"4111111111111111\",\n" + 
				"      \"expirationMonth\": \"12\",\n" + 
				"      \"expirationYear\": \"2031\",\n" + 
				"      \"securityCode\": \"123\"\n" + 
				"    }\n" + 
				"  },\n" + 
				"  \"orderInformation\": {\n" + 
				"    \"amountDetails\": {\n" + 
				"      \"totalAmount\": \"102.21\",\n" + 
				"      \"currency\": \"USD\"\n" + 
				"    },\n" + 
				"    \"billTo\": {\n" + 
				"      \"firstName\": \"John\",\n" + 
				"      \"lastName\": \"Doe\",\n" + 
				"      \"address1\": \"1 Market St\",\n" + 
				"      \"address2\": \"Address 2\",\n" + 
				"      \"locality\": \"san francisco\",\n" + 
				"      \"administrativeArea\": \"CA\",\n" + 
				"      \"postalCode\": \"94105\",\n" + 
				"      \"country\": \"US\",\n" + 
				"      \"email\": \"test@cybs.com\",\n" + 
				"      \"phoneNumber\": \"4158880000\"\n" + 
				"    }\n" + 
				"  }\n" + 
				"}";

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, objectTypeID, cookieMap, null);
        context.setCustomOperationType("POST");	
        tester.setOperationContext(context);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream(payload.getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        assertEquals("Created", actual.get(0).getMessage());
        assertEquals("201",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        String responseString = new String(actual.get(0).getPayloads().get(0));
        System.out.println(responseString);
    }
}
