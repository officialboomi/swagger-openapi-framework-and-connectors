// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.oraclesaas.OracleSaaSConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;

/**
 * @author Dave Hock
 */
public class MPESABrowseTest {
    static final String connectorName = "mpesa";
    private static final boolean captureExpected = false;
	String swaggerPath = "resources/"+connectorName+"/MPESASwagger.json";
    
    @Test
    public void testBrowseTypesMPESA() throws JSONException, Exception
    {
    	String swaggerPath = "resources/"+connectorName+"/MPESASwagger.json";
        OracleSaaSConnector connector = new OracleSaaSConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, "", getClass(), captureExpected);
    }
        
    @Test
    public void testBrowseDefinitionsCreateMPESA() throws JSONException, Exception
    {
        OracleSaaSConnector connector = new OracleSaaSConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "POST", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/stkpush/v1/processrequest");
        System.out.println(actual);
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateMPESA", getClass(), connectorName, captureExpected);
    }
    
//    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("MPESA Connector", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}
