// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.automationanywhere.AutomationAnywhereConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class AutomationAnywhereBrowseTest {
    static final String connectorName = "automationanywhere";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "resources/"+connectorName+"/wlm_openapi.json";
    
    @Test
    public void testBrowseTypesPOSTAutomationAnywhere() throws JSONException, Exception
    {
    	AutomationAnywhereConnector connector = new AutomationAnywhereConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
              
    @Test
    public void testBrowseTypesQUERYAutomationAnywhere() throws JSONException, Exception
    {
    	AutomationAnywhereConnector connector = new AutomationAnywhereConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesPUTAutomationAnywhere() throws JSONException, Exception
    {
    	AutomationAnywhereConnector connector = new AutomationAnywhereConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "PUT", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesPATCHAutomationAnywhere() throws JSONException, Exception
    {
    	AutomationAnywhereConnector connector = new AutomationAnywhereConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "PATCH", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEGETAutomationAnywhere() throws JSONException, Exception
    {
    	AutomationAnywhereConnector connector = new AutomationAnywhereConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEDELETEAutomationAnywhere() throws JSONException, Exception
    {
    	AutomationAnywhereConnector connector = new AutomationAnywhereConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseDefinitionsQueryAutomationAnywhere() throws JSONException, Exception
    {
        AutomationAnywhereConnector connector = new AutomationAnywhereConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        Map<String, Object> opProps = new HashMap<String,Object>();
        
//        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        opProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, opProps);
        actual = tester.browseProfiles("/automations/list");
//        actual = tester.browseProfiles("/users/list");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryAutomationAnywhere", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsExecuteGetAutomationAnywhere() throws JSONException, Exception
    {
        AutomationAnywhereConnector connector = new AutomationAnywhereConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/automations/{id}");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsExecuteGetAutomationAnywhere", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsCreateAutomationAnywhere() throws JSONException, Exception
    {
        AutomationAnywhereConnector connector = new AutomationAnywhereConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "POST", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/automations");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateAutomationAnywhere", getClass(), connectorName, captureExpected);
    }    
    
    //@Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
   		SwaggerDocumentationUtil.analyzeSwagger(connectorName);
    	SwaggerDocumentationUtil.generateDescriptorDoc("AutomationAnywhere API", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}