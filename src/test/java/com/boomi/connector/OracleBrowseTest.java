// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.oraclesaas.OracleSaaSConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.ConnectionProperties;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;

/**
 * @author Dave Hock
 */
public class OracleBrowseTest {
    static final String connectorName = "oraclesaas";
    private static final boolean captureExpected = false;
	
	String swaggers[] = {
			"https://docs.oracle.com/en/cloud/saas/address-verify-cloud/rrads/openapi.json",
			"https://docs.oracle.com/en/cloud/saas/b2c-service/21a/cxsvc/swagger.json",
			"https://docs.oracle.com/en/cloud/saas/applications-common/21b/farca/swagger.json",
			"https://docs.oracle.com/en/cloud/saas/sales/21b/faaps/swagger.json",
			"https://docs.oracle.com/en/industries/utilities/digital-self-service/restapi/swagger.json",
			"https://docs.oracle.com/en/cloud/saas/field-service/21a/cxfsc/swagger.json",
			"https://docs.oracle.com/en/cloud/saas/financials/21b/farfa/swagger.json",
			"https://docs.oracle.com/en/cloud/saas/human-resources/21b/farws/swagger.json",
			"https://docs.oracle.com/en/cloud/saas/procurement/21b/fapra/swagger.json",
			"https://docs.oracle.com/en/cloud/saas/project-management/21b/fapap/swagger.json",
			"https://docs.oracle.com/en/cloud/saas/risk-management/21b/farkm/swagger.json",
			"https://docs.oracle.com/en/cloud/saas/supply-chain-management/21b/fasrp/swagger.json"
	};

	String swaggerFiles[] = {
			"address-verification swagger.json",
			"b2c-service swagger.json",
			"common-features swagger.json",
			"cxsales-b2b-service swagger.json",
			"digital-self-service-energy-management swagger.json",
			"fieldservice swagger.json",
			"financials swagger.json",
			"human-resouces swagger.json",
			"procurement swagger.json",
			"project-portfolio-management swagger.json",
			"risk-management swagger.json",
			"supply-chain-management swagger.json"
	};

	String swaggerPathSupplyChain = swaggers[11];
	String swaggerPathHCM = swaggers[7];
	String swaggerPathFinance = swaggers[6];

	@Test
    public void testBrowseTypesOracleSaaS() throws JSONException, Exception
    {
    	Connector connector = new OracleSaaSConnector();
    	int i=0;
    	for (String swaggerHttp : swaggers)
    	{
    		String swagger=swaggerFiles[i];
    		String nameSuffix = swagger.substring(0, swagger.length()-" swagger.json".length());
    		String swaggerPath = swaggerHttp;
        	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, "", swaggerPath, connectorName, nameSuffix, getClass(), captureExpected);
        	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, nameSuffix, this.getClass(), captureExpected);
        	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, nameSuffix, this.getClass(), captureExpected);
        	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, nameSuffix, getClass(), captureExpected);
        	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "PATCH", swaggerPath, connectorName, nameSuffix, getClass(), captureExpected);
        	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "PUT", swaggerPath, connectorName, nameSuffix, getClass(), captureExpected);
        	if (i>2)
        		break;
        	i++;
    	}
    }
    
    @Test
    public void testBrowseDefinitionsQueryEmployees() throws JSONException, Exception
    {
        OracleSaaSConnector connector = new OracleSaaSConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), this.swaggerPathHCM);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 20L);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, opProps);
        actual = tester.browseProfiles("/emps");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryEmployees", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsQueryExpenseAttachments() throws JSONException, Exception
    {
        OracleSaaSConnector connector = new OracleSaaSConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), this.swaggerPathFinance);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 20L);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, opProps);
        actual = tester.browseProfiles("/expenseReports/{expenseReportsUniqID}/child/Attachments");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryExpenseAttachments", getClass(), connectorName, captureExpected);
    }
    
//Ignore 
    public void testBrowseDefinitionsQueryinventoryStagedTransactions() throws JSONException, Exception
    {
        OracleSaaSConnector connector = new OracleSaaSConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), this.swaggerPathHCM);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/inventoryStagedTransactions/{TransactionInterfaceId}/child/lots/{lotsUniqID}/child/lotAttributeDFFs");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryinventoryStagedTransactions", getClass(), connectorName, captureExpected);
    }
    
    //  /subinventories/{subinventoriesUniqID}/child/itemSubinventories/{itemSubinventoriesUniqID}/child/itemSubinventoriesDFF
    @Test
    public void testBrowseDefinitionsQuerycollaborationOrderForecasts() throws JSONException, Exception
    {
        OracleSaaSConnector connector = new OracleSaaSConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPathSupplyChain);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/fscmRestApi/resources/11.13.18.05/collaborationOrderForecasts");
        assertTrue(actual.length()>1000); //zipped cookie has dynamic timestamp data so compares will break;
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQuerycollaborationOrderForecasts", getClass(), connectorName, captureExpected);
    }
    //  /subinventories/{subinventoriesUniqID}/child/itemSubinventories/{itemSubinventoriesUniqID}/child/itemSubinventoriesDFF
    @Test
    public void testBrowseDefinitionsGetOracle() throws JSONException, Exception
    {
        OracleSaaSConnector connector = new OracleSaaSConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPathSupplyChain);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/fscmRestApi/resources/11.13.18.05/inventoryStagedTransactions/{TransactionInterfaceId}/child/lots/{lotsUniqID}/child/lotAttributeDFFs/{lotAttributeDFFsUniqID}");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetOracle", getClass(), connectorName, captureExpected);
    }

    @Test
    public void testBrowseDefinitionsGetOracleHCMEmployee() throws JSONException, Exception
    {
        OracleSaaSConnector connector = new OracleSaaSConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPathHCM);        
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 20L);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, opProps);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/emps/{empsUniqID}");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetOracleHCMEmployee", getClass(), connectorName, captureExpected);
    }

    
    @Test
    public void testBrowseDefinitionsCreateOracle() throws JSONException, Exception
    {
        OracleSaaSConnector connector = new OracleSaaSConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPathSupplyChain);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "POST", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/fscmRestApi/resources/11.13.18.05/inventoryStagedTransactions/{TransactionInterfaceId}/child/lots/{lotsUniqID}/child/lotAttributeDFFs");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateOracle", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsCreateXOperationOracle() throws JSONException, Exception
    {
        OracleSaaSConnector connector = new OracleSaaSConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPathFinance);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "POST", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/invoices___1");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateXOperationOracle", getClass(), connectorName, captureExpected);
    }    
    
//    @Test
	void AnalyzeSwagger() {
		System.out.println("");
		System.out.println(connectorName.toUpperCase());
		JSONObject fullSwaggerSchema = new JSONObject(new JSONTokener(this.getClass().getClassLoader().getResourceAsStream(swaggerPathSupplyChain)));
		SwaggerDocumentationUtil.analyzeOpenAPI(fullSwaggerSchema);
	}	
    
//    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("Oracle Cloud Applications Connector", connectorName);
    	for (String swagger : swaggers)
    	{
    		String nameSuffix = swagger.substring(0, swagger.length()-" swagger.json".length());
        	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName, nameSuffix);
    	}
    }    
}
