// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.api.Sort;
import com.boomi.connector.coupa.CoupaConnection;
import com.boomi.connector.coupa.CoupaConnector;

import com.boomi.swaggerframework.MockOperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.swaggerframework.swaggerutil.JSONUtil;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.QueryFilterBuilder;
import com.boomi.connector.testutil.QueryGroupingBuilder;
import com.boomi.connector.testutil.QuerySimpleBuilder;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class CoupaOperationTest 
{
    private static final String SERVICE_URL = "https://INSTANCENAME.coupahost.com";
    Map<String, Object> _connProps = new HashMap<String,Object>();
    Logger logger = Logger.getLogger(CoupaOperationTest.class.getName());
    
    @BeforeEach
    void init()
    {
        _connProps.put(SwaggerConnection.ConnectionProperties.URL.toString(), SERVICE_URL);
        _connProps.put(SwaggerConnection.ConnectionProperties.USERNAME.toString(), "harveym.api");
    }

    @Test   
    public void testQuery() throws Exception
    {
    	//TODO this returns paginated results!!!!!!!!!!!!
    	//We need another way to detect queries...maybe existence of RowBasedPagingParams.* parameter? 
    	String objectTypeId = "/accounts";
        CoupaConnector connector = new CoupaConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerQueryOperation.OperationProperties.MAXDOCUMENTS.name(), 28L);
        opProps.put(SwaggerQueryOperation.OperationProperties.PAGESIZE.name(), 1L);
 		
        List<String> selectedFields = new ArrayList<String>();
		selectedFields.add("username");
		selectedFields.add("id");
		MockOperationContext context = new MockOperationContext(null, connector, OperationType.QUERY, _connProps, opProps, objectTypeId, null, selectedFields);	
		tester.setOperationContext(context);

		List <SimpleOperationResult> actual;
		
        QueryFilter qf =  null;
        
        qf = new QueryFilter();
        actual = tester.executeQueryOperation(qf);
        logger.info(actual.toString());
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(5, actual.get(0).getPayloads().size());
        
        qf = new QueryFilterBuilder(new QuerySimpleBuilder("lastName", "substring", "a")).toFilter();
        actual = tester.executeQueryOperation(qf);
        logger.info(actual.toString());
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(2, actual.get(0).getPayloads().size());
        
        qf = new QueryFilterBuilder(QueryGroupingBuilder.and(
                new QuerySimpleBuilder("lastName", "substring", "a"),
                new QuerySimpleBuilder("createdOn", "gt", "2021-03-08T00:00:00.001Z"))).toFilter();
        Sort sort1 = new Sort();
        sort1.setProperty("lastName");
        qf.withSort(sort1); 
        sort1.setSortOrder("desc");
        actual = tester.executeQueryOperation(qf);
        logger.info(actual.toString());
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(2, actual.get(0).getPayloads().size());
        
        qf=null;
        actual = tester.executeQueryOperation(qf);
        logger.info(actual.toString());
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(5, actual.get(0).getPayloads().size());
    }
    
    @Test   
    public void testGet() throws Exception
    {
    	String objectTypeId = "/v1/usermanagement/users/{uid}";
    	CoupaConnector connector = new CoupaConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        
		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		
        opProps.put("path_uid", 41);
//        opProps.put(SwaggerExecuteOperation.OperationProperties.URL_PATH.name(), "https://dev-267789.okta.com/api/v1/users/00u2prx0ujsKc6qQg4x7");

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, _connProps, opProps, objectTypeId, cookieMap, null);
        context.setCustomOperationType("GET");	
		tester.setOperationContext(context);
        
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream("".getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
    }
}
