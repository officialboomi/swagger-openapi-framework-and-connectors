
// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Node;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import com.boomi.connector.insperityoss.OASConnector;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser.OperationProperties;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.ConnectionProperties;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class InsperityBrowseTest {
    Logger logger = Logger.getLogger("OASBrowseTest");
    OASConnector connector = new OASConnector();
    ConnectorTester tester = new ConnectorTester(connector);
    Boolean hasErrors=false;
    int numEndpoints=0;
    int numBadEndpoints=0;
    StringBuilder readme = new StringBuilder();
    ArrayList<String> errors = new ArrayList<String>();

    @Test
    public void testBrowseTypes() throws JSONException, Exception
    {
    	browseTypes(OperationType.EXECUTE,"GET");
    	browseTypes(OperationType.EXECUTE,"POST");
    	browseTypes(OperationType.EXECUTE,"PUT");
    	browseTypes(OperationType.EXECUTE,"DELETE");
    	browseTypes(OperationType.EXECUTE,"PATCH");
    	browseTypes(OperationType.QUERY,"");
    	for (String errorString : errors)
    	{
    		readme.insert(0,errorString+"  \n");
    	}
    	readme.insert(0,"### Errors  \n");
    	readme.insert(0,"**Unsupported Endpoints:** " + numBadEndpoints+"  \n");
    	readme.insert(0,"**Total Endpoints:** " + numEndpoints +"  \n");
    	readme.insert(0,"**Spec:** resources/spec.json  \n");
    	readme.insert(0,"## REST interface to Insperity OSS  \n");
    	readme.insert(0,"# Insperity OSS  \n");
    	this.writeReadme(readme);
    }      
    
    
    void browseTypes(OperationType operationType, String method) throws DocumentException
    {
        String actual="";
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, operationType, method, null, null);
        tester.setBrowseContext(sbc);
    	readme.append("## Operation " + operationType.toString() +"-"+ method + "  \n");
    	logger.info("*OPERATION* " + operationType.toString() +"-"+ method + "  \n");
        try {
        	actual = tester.browseTypes();
        } catch (Exception e) {
        	logger.info(e.toString());
        	readme.append("Not used  \n");
        	return;
        }
//        logger.info(actual);
        Node actualDoc = DocumentHelper.parseText(actual);        
        List<Node> types = actualDoc.selectNodes("/ObjectTypes/type");
        for (Node typeNode:types)
        {
        	String objectId = typeNode.selectSingleNode("@id").getText();
        	String label = typeNode.selectSingleNode("@label").getText();
        	logger.info("**ATTEMPTING ENDPOINT** " + objectId);
        	numEndpoints++;
        	boolean badEndpoint=false;
        	try {
            	actual = tester.browseProfiles(objectId);
                logger.info(actual);        		
        	} catch(Exception e)
        	{
        		numBadEndpoints++;
        		logger.severe("***ERROR****: " + method + " " + objectId + " " + e.toString());
        		StackTraceElement[] trace = e.getStackTrace();
        		String traceList = "";
        		int number=0;
        		for (StackTraceElement traceLine : trace)
        		{
        			traceList+=traceLine.toString()+"  \n";
        			number++;
        			if (number>5)
        				break;
        		}
        		hasErrors=true;
        		badEndpoint = true;
        		String errorString = e.toString()+"  \n"+traceList;
        		if (!errors.contains(errorString))
        			errors.add(errorString);
        	}
        	if (badEndpoint)
        		readme.append("~~");
        	readme.append("**"+label+"**");
        	if (badEndpoint)
        		readme.append("~~");
        	readme.append("  \n");
        	if (badEndpoint)
        		readme.append("~~");
        	readme.append(objectId);
        	if (badEndpoint)
        		readme.append("~~");
        	readme.append("  \n");
        }
    }
    
    @Test
    void browseEmployeeV2TypesTest() throws DocumentException
    {
        String actual="";
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "get", null, null);
        tester.setBrowseContext(sbc);
    	String objectId = "/company/{companyId}/employees/v2";
    	logger.info("**ATTEMPTING ENDPOINT** " + objectId);
    	numEndpoints++;
    	actual = tester.browseProfiles(objectId);
        logger.info(actual);        		
    }

    void writeReadme(StringBuilder content) throws IOException
    {
	    BufferedWriter writer = new BufferedWriter(new FileWriter("README.md", false));
	    writer.append(content);
	    writer.close();
    }
}




