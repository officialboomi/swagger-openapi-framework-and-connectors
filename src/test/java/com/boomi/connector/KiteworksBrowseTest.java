// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.kiteworks.KiteworksConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;

/**
 * @author Dave Hock
 */
public class KiteworksBrowseTest {
    static final String connectorName = "kiteworks";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = null;
    
    @Test
    public void testBrowseTypesCREATEKiteworks() throws JSONException, Exception
    {
    	KiteworksConnector connector = new KiteworksConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, "", getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesQUERYKiteworks() throws JSONException, Exception
    {
    	KiteworksConnector connector = new KiteworksConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesUPDATEKiteworks() throws JSONException, Exception
    {
    	KiteworksConnector connector = new KiteworksConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "PUT", swaggerPath, connectorName, "", getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesUPDATEPatchKiteworks() throws JSONException, Exception
    {
    	KiteworksConnector connector = new KiteworksConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "PATCH", swaggerPath, connectorName, "", getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEGETKiteworks() throws JSONException, Exception
    {
    	KiteworksConnector connector = new KiteworksConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEDELETEKiteworks() throws JSONException, Exception
    {
    	KiteworksConnector connector = new KiteworksConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseDefinitionsQueryKiteworks() throws JSONException, Exception
    {
        KiteworksConnector connector = new KiteworksConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/rest/folders/shared");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryKiteworks", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsGetKiteworks() throws JSONException, Exception
    {
        KiteworksConnector connector = new KiteworksConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/rest/folders/{id}");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetKiteworks", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsCreateKiteworks() throws JSONException, Exception
    {
        KiteworksConnector connector = new KiteworksConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "POST", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/rest/folders/{parent_id}/folders");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateKiteworks", getClass(), connectorName, captureExpected);
    }    
    
//    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("This connector provides access to the REST API for the The Kiteworks content firewall helps IT executives lock down the exchange of confidential enterprise information with customers, suppliers, and partners by unifying visibility and security across siloed third-party communication channels, including email, file sharing, mobile, web forms, managed file transfer, and SFTP. For more information, click here: https://developer.accellion.com/content/api-guides-landing-page.htm", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
   		SwaggerDocumentationUtil.analyzeSwagger(connectorName);
    }    
}