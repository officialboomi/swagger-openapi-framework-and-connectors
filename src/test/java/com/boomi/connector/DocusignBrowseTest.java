// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.blackboard.BlackboardConnector;
import com.boomi.connector.docusign.DocusignConnector;
import com.boomi.connector.oraclesaas.OracleSaaSConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.ConnectionProperties;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;

/**
 * @author Dave Hock
 */
public class DocusignBrowseTest {
    static final String connectorName = "docusign";
    private static final boolean captureExpected = false;
	String swaggerPath = "resources/"+connectorName+"/esignature.rest.swagger-v2.1.json";
    
    @Test
    public void testBrowseTypesDocusign() throws JSONException, Exception
    {
        DocusignConnector connector = new DocusignConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "PUT", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }   
    
    @Test
    public void testBrowseDefinitionsQueryUsers() throws JSONException, Exception
    {
    	DocusignConnector connector = new DocusignConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        Map<String, Object> opProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 20L);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, opProps);
        actual = tester.browseProfiles("/v2.1/accounts/{accountId}/users");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryUsers", getClass(), connectorName, captureExpected);
    } 
    
    @Test
    public void testBrowseDefinitionsGetUser() throws JSONException, Exception
    {
    	DocusignConnector connector = new DocusignConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/v2.1/accounts/{accountId}/users/{userId}");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetUser", getClass(), connectorName, captureExpected);
    } 
    
//    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("Docusign REST Connector", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}
