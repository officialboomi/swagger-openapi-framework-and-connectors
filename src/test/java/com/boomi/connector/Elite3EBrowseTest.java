// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.elite3e.Elite3EConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class Elite3EBrowseTest {
    static final String connectorName = "elite3e";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "resources/"+connectorName+"/openapi.json";
    
    @Test
    public void testBrowseTypesCREATEElite3E() throws JSONException, Exception
    {
    	Elite3EConnector connector = new Elite3EConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
              
    @Test
    public void testBrowseTypesQUERYElite3E() throws JSONException, Exception
    {
    	Elite3EConnector connector = new Elite3EConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesUPDATEElite3E() throws JSONException, Exception
    {
    	Elite3EConnector connector = new Elite3EConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "PUT", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEGETElite3E() throws JSONException, Exception
    {
    	Elite3EConnector connector = new Elite3EConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEDELETEElite3E() throws JSONException, Exception
    {
    	Elite3EConnector connector = new Elite3EConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseDefinitionsQueryElite3E() throws JSONException, Exception
    {
        Elite3EConnector connector = new Elite3EConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/api/v1/time/posted");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryElite3E", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsExecuteGetElite3E() throws JSONException, Exception
    {
        Elite3EConnector connector = new Elite3EConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/api/v1/dashboard/{dashboardId}");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsExecuteGetElite3E", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsCreateElite3E() throws JSONException, Exception
    {
        Elite3EConnector connector = new Elite3EConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "POST", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/api/v1/time/posted");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateElite3E", getClass(), connectorName, captureExpected);
    }    
    
//    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
   		SwaggerDocumentationUtil.analyzeSwagger(connectorName);
    	SwaggerDocumentationUtil.generateDescriptorDoc("Elite 3E API", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}