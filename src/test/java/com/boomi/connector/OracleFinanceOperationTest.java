// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.api.Sort;
import com.boomi.connector.oraclesaas.OracleSaaSConnector;
import com.boomi.swaggerframework.MockOperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerExecuteOperation;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.QueryFilterBuilder;
import com.boomi.connector.testutil.QueryGroupingBuilder;
import com.boomi.connector.testutil.QuerySimpleBuilder;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.connector.testutil.SimpleOperationContext;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class OracleFinanceOperationTest 
{
    private static final String URL = "https://ucf5-ztuy-fa-ext.oracledemos.com";
    private static final String USERNAME = "fin_impl";
    private static final String PASSWORD = "XXXXXXX";
	private static final String cookieString = "{\"basePath\" : \"/fscmRestApi/resources/11.13.18.05\"}";

    @Test
    public void testExecuteGetInvoiceOperation() throws Exception
    {
    	String objectTypeId = "/invoices/{InvoiceId}";
        OracleSaaSConnector connector = new OracleSaaSConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.name(), URL);
        connProps.put(SwaggerConnection.ConnectionProperties.USERNAME.name(), USERNAME);
        connProps.put(SwaggerConnection.ConnectionProperties.PASSWORD.name(), PASSWORD);
        
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream("".getBytes()));
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put("path_InvoiceId", "1");
		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.INPUT, cookieString);

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, objectTypeId, cookieMap, null);
        context.setCustomOperationType("GET");	
		tester.setOperationContext(context);
        
        List<SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
    }


    @Test
    public void testQueryInvoicesOperation() throws Exception
    {
    	String objectTypeId = "/invoices";
        OracleSaaSConnector connector = new OracleSaaSConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.name(), URL);
        connProps.put(SwaggerConnection.ConnectionProperties.USERNAME.name(), USERNAME);
        connProps.put(SwaggerConnection.ConnectionProperties.PASSWORD.name(), PASSWORD);
        connProps.put(SwaggerConnection.ConnectionProperties.AUTHTYPE.name(), SwaggerConnection.AuthType.BASIC.name());
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerQueryOperation.OperationProperties.MAXDOCUMENTS.name(), 28L);
        opProps.put(SwaggerQueryOperation.OperationProperties.PAGESIZE.name(), 10L);
        QueryFilter qf =  new QueryFilterBuilder(QueryGroupingBuilder.and(
//                new QuerySimpleBuilder("InvoiceAmount", "gt", 1111111111)
                
//                new QuerySimpleBuilder("given", "eq", "Niel"),              
//                ,new QuerySimpleBuilder("CreationDate", "lt", "2010-09-01")
                )).toFilter();
        Sort sort1 = new Sort();
        sort1.setProperty("InvoiceAmount");
        sort1.setSortOrder("asc");
//        Sort sort2 = new Sort();
//        sort2.setSortOrder("asc");
//        sort2.setProperty("gender");
//        qf.withSort(sort1, sort2);//2 sorts throwing 500, 
        qf.withSort(sort1); 

		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.OUTPUT, cookieString);

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, objectTypeId, cookieMap, null);
        context.setCustomOperationType("GET");	
		tester.setOperationContext(context);

		List <SimpleOperationResult> actual = tester.executeQueryOperation(qf);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(28, actual.get(0).getPayloads().size());
        String actualString = new String(actual.get(0).getPayloads().get(0));
      	System.out.println(actualString);
    }
}
