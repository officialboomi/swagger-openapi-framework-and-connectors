// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.deltekvantagepoint.DeltekVantagepointConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 * 
 * Swagger Issues 
 * 1. All paths had // prefix so removed     
 *     "//metadata/activity": {
 *      "get": {
 * 2. basePath was "/" so made blank
 * 3. query response schema paths had url encoded ()= Contacts(withlimit=5) so replaced with "#/definitions/Contact"
 * 4. bad utf8 char broke documentation generation and had to be converted to ansi with np++
 * 5. Create contact had an form input schema, edited it to accept json and added
 * 					{
						"name": "Contact",
						"in": "body",
						"required": true,
						"schema": {
							"type": "array",
							"items": {
								"$ref": "#/definitions/Contact"
							}
						}
					}
 * 6. Update had no input profile so added
 * 					{
						"name": "Contact",
						"in": "body",
						"required": true,
						"schema": {
							"$ref": "#/definitions/Contact"
						}
					}
 */
public class DeltekVantagepointBrowseTest {
    static final String connectorName = "deltekvantagepoint";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "resources/"+connectorName+"/swagger.json";
    
    @Test
    public void testBrowseTypesCREATEDeltekVantagepoint() throws JSONException, Exception
    {
    	DeltekVantagepointConnector connector = new DeltekVantagepointConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
              
    @Test
    public void testBrowseTypesQUERYDeltekVantagepoint() throws JSONException, Exception
    {
    	DeltekVantagepointConnector connector = new DeltekVantagepointConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesUPDATEDeltekVantagepoint() throws JSONException, Exception
    {
    	DeltekVantagepointConnector connector = new DeltekVantagepointConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "PUT", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEGETDeltekVantagepoint() throws JSONException, Exception
    {
    	DeltekVantagepointConnector connector = new DeltekVantagepointConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEDELETEDeltekVantagepoint() throws JSONException, Exception
    {
    	DeltekVantagepointConnector connector = new DeltekVantagepointConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseDefinitionsQueryDeltekVantagepoint() throws JSONException, Exception
    {
        DeltekVantagepointConnector connector = new DeltekVantagepointConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/contact");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryDeltekVantagepoint", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsExecuteGetDeltekVantagepoint() throws JSONException, Exception
    {
        DeltekVantagepointConnector connector = new DeltekVantagepointConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/contact/{ContactID}");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsExecuteGetDeltekVantagepoint", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsCreateDeltekVantagepoint() throws JSONException, Exception
    {
        DeltekVantagepointConnector connector = new DeltekVantagepointConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "POST", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/contact");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateDeltekVantagepoint", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsUpdateDeltekVantagepoint() throws JSONException, Exception
    {
        DeltekVantagepointConnector connector = new DeltekVantagepointConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "PUT", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/contact/{ContactID}");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsUpdateDeltekVantagepoint", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsDeleteDeltekVantagepoint() throws JSONException, Exception
    {
        DeltekVantagepointConnector connector = new DeltekVantagepointConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "DELETE", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/contact/{ContactID}");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsDeleteDeltekVantagepoint", getClass(), connectorName, captureExpected);
    }    
    
//    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
   		SwaggerDocumentationUtil.analyzeSwagger(connectorName);
    	SwaggerDocumentationUtil.generateDescriptorDoc("Deltek Vantagepoint REST API", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}