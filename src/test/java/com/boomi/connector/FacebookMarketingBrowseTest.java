// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.facebook_marketing.FacebookMarketingConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class FacebookMarketingBrowseTest {
    static final String connectorName = "facebook_marketing";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "resources/"+connectorName+"/openapi.json";
    
    @Test
    public void testBrowseTypesCREATEFacebookMarketing() throws JSONException, Exception
    {
    	FacebookMarketingConnector connector = new FacebookMarketingConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesGETFacebookMarketing() throws JSONException, Exception
    {
    	FacebookMarketingConnector connector = new FacebookMarketingConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesQUERYFacebookMarketing() throws JSONException, Exception
    {
    	FacebookMarketingConnector connector = new FacebookMarketingConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesUPDATEFacebookMarketing() throws JSONException, Exception
    {
    	FacebookMarketingConnector connector = new FacebookMarketingConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "PUT", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEGETFacebookMarketing() throws JSONException, Exception
    {
    	FacebookMarketingConnector connector = new FacebookMarketingConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEDELETEFacebookMarketing() throws JSONException, Exception
    {
    	FacebookMarketingConnector connector = new FacebookMarketingConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
//    @Test
    public void testBrowseDefinitionsQueryFacebookMarketing() throws JSONException, Exception
    {
        FacebookMarketingConnector connector = new FacebookMarketingConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/learn/api/public/v1/users");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryFacebookMarketing", getClass(), connectorName, captureExpected);
    }
    
//    @Test
    public void testBrowseDefinitionsExecuteGetFacebookMarketing() throws JSONException, Exception
    {
        FacebookMarketingConnector connector = new FacebookMarketingConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/learn/api/public/v1/users/{userId}");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsExecuteGetFacebookMarketing", getClass(), connectorName, captureExpected);
    }    
    
//    @Test
    public void testBrowseDefinitionsCreateFacebookMarketing() throws JSONException, Exception
    {
        FacebookMarketingConnector connector = new FacebookMarketingConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.CREATE, null, connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/learn/api/public/v1/users");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateFacebookMarketing", getClass(), connectorName, captureExpected);
    }    
    
 //   @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("Facebook Marketing", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}