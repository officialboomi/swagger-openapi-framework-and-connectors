// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.connector.worldpay.WorldpayConnector;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class WorldpayBrowseTest {
    static final String connectorName = "worldpay";
    private static final String swaggerPath = "resources/worldpay/swagger.json";
    private static final boolean captureExpected = false;
    
    @Test
    public void testBrowseTypes() throws JSONException, Exception
    {
    	WorldpayConnector connector = new WorldpayConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, "", getClass(), captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsCreate() throws JSONException, Exception
    {
    	WorldpayConnector connector = new WorldpayConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "POST", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/check/authorization");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreate", getClass(), connectorName, captureExpected);
    }
}
