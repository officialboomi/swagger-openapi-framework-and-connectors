// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.api.Sort;
import com.boomi.connector.kefron.KefronConnector;
import com.boomi.swaggerframework.MockOperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerExecuteOperation;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.QueryFilterBuilder;
import com.boomi.connector.testutil.QueryGroupingBuilder;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
//Create/Get/Delete in that order
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class KefronOperationTest 
{
    private static final String SERVICE_URL = "https://kefronapi.fileweb.ie/v1";
    private static String _userID="00u2prx0ujsKc6qQg4x7";
    private static final String cookieString = null;//"{\"basePath\" : \"/api/v1\", \"queryPaginationSplitPath\" : \"\"}";
    private static final String token="a5a455f5-5dba-4b52-a03c-99a852da930c";
    private static final String password = "B00m1$Trial";
    Map<String, Object> _connProps;
	private static JSONObject _userPayload;

    
    @BeforeEach
    void init()
    {
        _connProps = new HashMap<String,Object>();
        _connProps.put(SwaggerConnection.ConnectionProperties.URL.toString(), SERVICE_URL);
        _connProps.put(SwaggerConnection.ConnectionProperties.USERNAME.toString(), token);
        _connProps.put(SwaggerConnection.ConnectionProperties.PASSWORD.toString(), password);    	
    }
    
//    @Test   
//    @Order(1)
    public void testCreateOperation() throws Exception
    {
    	String objectTypeId = "/users";
        KefronConnector connector = new KefronConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        
        Map<String, Object> opProps = new HashMap<String,Object>();

		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.INPUT, cookieString);

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, _connProps, opProps, objectTypeId, cookieMap, null);
        context.setCustomOperationType("POST");	
		tester.setOperationContext(context);

        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream(_userPayload.toString().getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        String responseString = new String(actual.get(0).getPayloads().get(0));
        JSONObject responseObject = new JSONObject(responseString);
        _userID = responseObject.getString("id");
        System.out.println(responseString);
//testUpdateOperation();
    }    
    
 //   @Test   
//    @Order(2)
    public void testUpdateOperation() throws Exception
    {
    	String objectTypeId = "/users/{userId}";
        KefronConnector connector = new KefronConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        Map<String, Object> opProps = new HashMap<String,Object>();

		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.INPUT, cookieString);
		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, _connProps, opProps, objectTypeId, cookieMap, null);
        context.setCustomOperationType("PUT");	
		tester.setOperationContext(context);
		
        List<InputStream> inputs = new ArrayList<InputStream>();
//        _userPayload.put(SwaggerAPIService.URLPATHPARAMETERSROOTNAME, pathParams);
        opProps.put("path_userId", _userID);
        inputs.add(new ByteArrayInputStream(_userPayload.toString().getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        String responseString = new String(actual.get(0).getPayloads().get(0));
        JSONObject responseObject = new JSONObject(responseString);
        System.out.println(responseString);
    }    
    
    @Test   
//    @Order(3)
    public void testGetOperation() throws Exception
    {
    	String objectTypeId = "/users/{id}";
        KefronConnector connector = new KefronConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        
		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.INPUT, cookieString);
		
        opProps.put("path_id", "6bcf6a58-bb97-437a-9823-f72d8a8c02bb");
//        opProps.put(SwaggerExecuteOperation.OperationProperties.URL_PATH.name(), "https://dev-267789.Kefron.com/api/v1/users/00u2prx0ujsKc6qQg4x7");

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, _connProps, opProps, objectTypeId, cookieMap, null);
        context.setCustomOperationType("GET");	
		tester.setOperationContext(context);
        
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream("".getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        String responseString = new String(actual.get(0).getPayloads().get(0));
//        JSONObject responseObject = new JSONObject(responseString);
        System.out.println(responseString);

    }
    
    
    @Test
//    @Order(4)
    public void testQueryOperation() throws Exception
    {
    	String objectTypeId = "/users";
        KefronConnector connector = new KefronConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerQueryOperation.OperationProperties.MAXDOCUMENTS.name(), 28L);
        opProps.put(SwaggerQueryOperation.OperationProperties.PAGESIZE.name(), 2L);
        QueryFilter qf =  new QueryFilterBuilder(QueryGroupingBuilder.and(
//                new QuerySimpleBuilder("InvoiceAmount", "gt", 1111111111)
                
//                new QuerySimpleBuilder("given", "eq", "Niel"),              
//                ,new QuerySimpleBuilder("CreationDate", "lt", "2010-09-01")
                )).toFilter();
        Sort sort1 = new Sort();
        sort1.setProperty("InvoiceAmount");
        sort1.setSortOrder("asc");
//        Sort sort2 = new Sort();
//        sort2.setSortOrder("asc");
//        sort2.setProperty("gender");
//        qf.withSort(sort1, sort2);//2 sorts throwing 500, 
//        qf.withSort(sort1); 

		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.OUTPUT, cookieString);
        tester.setOperationContext(OperationType.QUERY, _connProps, opProps, objectTypeId, cookieMap);
        List <SimpleOperationResult> actual = tester.executeQueryOperation(null);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertTrue(actual.get(0).getPayloads().size()>10);
        String actualString = new String(actual.get(0).getPayloads().get(0));
      	System.out.println(actualString);
    }

//    @Test 
    //TODO can't test execute/delete until we figure out how to pass the customtype
//    @Order(6)
    public void testDeleteOperation() throws Exception
    {
    	String objectTypeId = "/users/{id}";
        KefronConnector connector = new KefronConnector();
        ConnectorTester tester = new ConnectorTester(connector);

//       _userID="200u2okqlz49Nsm6EJ4x7";
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put("path_id", _userID);
        
		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.INPUT, cookieString);

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, _connProps, opProps, objectTypeId, cookieMap, null);
        context.setCustomOperationType("DELETE");	
		tester.setOperationContext(context);
        
        List<InputStream> inputs = new ArrayList<InputStream>();
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        assertEquals("No Content", actual.get(0).getMessage());
        assertEquals("204",actual.get(0).getStatusCode());
        assertEquals("SUCCESS",actual.get(0).getStatus().name());
    }
}
