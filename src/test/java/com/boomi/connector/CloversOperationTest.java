// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.clovers.CloversConnection;
import com.boomi.connector.clovers.CloversConnector;
import com.boomi.swaggerframework.MockOperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class CloversOperationTest 
{
	String teamPayload = "{\r\n"
			+ " \"context\": {\r\n"
			+ " \"org_id\": \"d4faf782-81ee-4264-ba0c-9f45b38f451b\",\r\n"
			+ " \"as_of\": \"2021-05-15T17:00:00.000-07:00\",\r\n"
			+ " \"source_app\": \"GHATS-01\"\r\n"
			+ " },\r\n"
			+ " \"id\": \"EE889900\",\r\n"
			+ " \"first_name\": \"Boss\",\r\n"
			+ " \"last_name\": \"Hoag\",\r\n"
			+ " \"email\": \"bhoag@somecompany.net\",\r\n"
			+ " \"title\": \"Director of Cold Storage\",\r\n"
			+ " \"role\": \"manager\",\r\n"
			+ " \"department\": \"ORGjklm\"\r\n"
			+ "}";
    static final String connectorName = "clovers";
    private static final boolean captureExpected = false;
	private static JSONObject _credentials = new JSONObject(new JSONTokener(CloversBrowseTest.class.getClassLoader().getResourceAsStream("resources/clovers/testCredentials.json")));
	private static Map<String, Object> _connProps;
	Logger logger = Logger.getLogger(this.getClass().getName());

	@BeforeAll
    static void initAll() {
		_connProps = new HashMap<String,Object>();
        _connProps.put("URL", "https://ats.webhook.clovers-development.com");

        _connProps.put(CloversConnection.CustomConnectionProperties.SECRET.name(), _credentials.get("secret"));
        _connProps.put(CloversConnection.CustomConnectionProperties.CLIENT_ID.name(), _credentials.get("clientId"));
    }	

    @Test
    public void testCreateCandidateOperation() throws Exception
    {
//    	String paymentsCookie = "{\"basePath\" : \"\"}";
    	String objectTypeId = "/hiring-teams";
    	CloversConnector connector = new CloversConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        Map<String, Object> opProps = new HashMap<String,Object>();

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, _connProps, opProps, objectTypeId, null, null);
        context.setCustomOperationType("POST");	
        tester.setOperationContext(context);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream(teamPayload.getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        logger.info("Response payload: " + actual);
        String responseString = new String(actual.get(0).getPayloads().get(0));
        logger.info(responseString);
    }
}
