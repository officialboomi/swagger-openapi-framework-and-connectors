// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.amazonsellingpartner.AmazonSellingPartnerConnection;
import com.boomi.connector.amazonsellingpartner.AmazonSellingPartnerConnector;
import com.boomi.swaggerframework.MockOperationContext;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class AmazonSellingPartnerOperationTest 
{
    static final String connectorName = "aws";
//	private static JSONObject _credentials = new JSONObject(new JSONTokener(BullhornBrowseTest.class.getClassLoader().getResourceAsStream("resources/bullhorn/testCredentials.json")));
	private static Map<String, Object> _connProps;
	Logger logger = Logger.getLogger(this.getClass().getName());

	@BeforeAll
    static void initAll() {
		_connProps = new HashMap<String,Object>();

        _connProps.put(AmazonSellingPartnerConnection.CustomConnectionProperties.clientId.name(), "amzn1.application-oa2-client.69963712492f4e1e8e4a2599365da7ea");
        _connProps.put(AmazonSellingPartnerConnection.CustomConnectionProperties.accessKey.name(), "");
        _connProps.put(AmazonSellingPartnerConnection.CustomConnectionProperties.secretKey.name(), "");
        _connProps.put(AmazonSellingPartnerConnection.CustomConnectionProperties.refreshToken.name(), "");
        _connProps.put(AmazonSellingPartnerConnection.CustomConnectionProperties.clientSecret.name(), "");

        _connProps.put(AmazonSellingPartnerConnection.ConnectionProperties.URL.name(), "https://sandbox.sellingpartnerapi-na.amazon.com");
//        _connProps.put(AmazonSellingPartnerConnection.ConnectionProperties.URL.name(), "https://sellingpartnerapi-na.amazon.com");
        _connProps.put(AmazonSellingPartnerConnection.CustomConnectionProperties.scope.name(), "sellingpartnerapi::migration");
        _connProps.put(AmazonSellingPartnerConnection.CustomConnectionProperties.region.name(), "us-east-1");
//        _connProps.put(AmazonSellingPartnerConnection.CustomConnectionProperties.stsRegion.name(), "us-east-1");
        _connProps.put(AmazonSellingPartnerConnection.CustomConnectionProperties.iamUser.name(), "382029670213");
        _connProps.put(AmazonSellingPartnerConnection.CustomConnectionProperties.apiRole.name(), "SellingPartnerAPIRole");
        _connProps.put(AmazonSellingPartnerConnection.CustomConnectionProperties.useTempCredentials.name(), true);
    }	

    //TODO we need the swagger for Transaction Search
    @Test
    public void testGetOrderOperation() throws Exception
    {
    	String objectTypeId = "/orders/v0/orders/{orderId}";
    	AmazonSellingPartnerConnector connector = new AmazonSellingPartnerConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
//		opProps.put("useRestrictedDataToken", true);
        opProps.put("restrictedDataElements","buyerInfo,shippingAddress"); 
        opProps.put("path_orderId", "TEST_CASE_200");

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, _connProps, opProps, objectTypeId, null, null);
        context.setCustomOperationType("GET");	
        tester.setOperationContext(context);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream("".getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        logger.info("Response payload: " + actual);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());

    }
    
    @Test
    public void testCreateRestrictedDataTokenOperation() throws Exception
    {
    	String objectTypeId = "/tokens/2021-03-01/restrictedDataToken";
    	AmazonSellingPartnerConnector connector = new AmazonSellingPartnerConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
		opProps.put("useRestrictedDataToken", false);

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, _connProps, opProps, objectTypeId, null, null);
        context.setCustomOperationType("POST");	
        tester.setOperationContext(context);
        List<InputStream> inputs = new ArrayList<InputStream>();
        String payload = "{\r\n"
        		+ "  \"restrictedResources\": [\r\n"
        		+ "    {\r\n"
        		+ "      \"method\": \"GET\",\r\n"
        		+ "      \"path\": \"/orders/v0/orders/TEST_CASE_200\",\r\n"
        		+ "      \"dataElements\": [\"buyerInfo\",\"shippingAddress\"]\r\n"
        		+ "    }\r\n"
        		+ "  ]\r\n"
        		+ "}";
        inputs.add(new ByteArrayInputStream(payload.getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        logger.info("Response payload: " + actual);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());

    }

}
