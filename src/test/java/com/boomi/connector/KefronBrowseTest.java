// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.kefron.KefronBrowser;
import com.boomi.connector.kefron.KefronConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.JSONSchemaGenerator;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;

/**
 * @author Dave Hock
 */
public class KefronBrowseTest {
    static final String connectorName = "kefron";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "resources/"+connectorName+"/openapi.json";
    private static final String SERVICE_URL = "https://kefronapi.fileweb.ie/v1";
    private static String _userID="00u2prx0ujsKc6qQg4x7";
    private static final String cookieString = null;//"{\"basePath\" : \"/api/v1\", \"queryPaginationSplitPath\" : \"\"}";
    private static final String token="a5a455f5-5dba-4b52-a03c-99a852da930c";
    private static final String password = "B00m1$Trial";
    Map<String, Object> _connProps;
	private static JSONObject _userPayload;
    
    @BeforeEach
    void init()
    {
        _connProps = new HashMap<String,Object>();
        _connProps.put(SwaggerConnection.ConnectionProperties.URL.toString(), SERVICE_URL);
        _connProps.put(SwaggerConnection.ConnectionProperties.USERNAME.toString(), token);
        _connProps.put(SwaggerConnection.ConnectionProperties.PASSWORD.toString(), password);    	
    }
   
    @Test
    public void testBrowseTypesCREATEKefron() throws JSONException, Exception
    {
    	KefronConnector connector = new KefronConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
              
    @Test
    public void testBrowseTypesQUERYKefron() throws JSONException, Exception
    {
    	KefronConnector connector = new KefronConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesUPDATEKefron() throws JSONException, Exception
    {
    	KefronConnector connector = new KefronConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "PUT", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEGETKefron() throws JSONException, Exception
    {
    	KefronConnector connector = new KefronConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEDELETEKefron() throws JSONException, Exception
    {
    	KefronConnector connector = new KefronConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseDefinitionsQueryKefron() throws JSONException, Exception
    {
        KefronConnector connector = new KefronConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        Map<String, Object> opProps = new HashMap<String,Object>();
        
         connProps.put(KefronBrowser.OperationProperties.OBJECT_SAMPLE_PATH.name(), "/users/6bcf6a58-bb97-437a-9823-f72d8a8c02bb");
        
        tester.setBrowseContext(OperationType.QUERY, _connProps, opProps);
        actual = tester.browseProfiles("/v1/users");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryKefron", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsExecuteGetKefron() throws JSONException, Exception
    {
        KefronConnector connector = new KefronConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/v1/documents/{id}");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsExecuteGetKefron", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsCreateKefron() throws JSONException, Exception
    {
        KefronConnector connector = new KefronConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "POST", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/v1/document-attachments");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateKefron", getClass(), connectorName, captureExpected);
    }    
    
    //@Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
   		SwaggerDocumentationUtil.analyzeSwagger(connectorName);
    	SwaggerDocumentationUtil.generateDescriptorDoc("Kefron API", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}