// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.blackboard.BlackboardConnector;
import com.boomi.connector.openbank.OpenBankConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.JSONUtil;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class OpenbankOperationTest 
{
    private static final String SERVICE_URL = "https://apisandbox.openbankproject.com";
    private static final String SWAGGER_URL = "resources/openbank/openbank-account-info-swagger.json";
    //portal uid dave.hock
    //portal pwd Xxxxxxxxxx!## use the schema and double the king
    //redirect https://platform.boomi.com/account/boomi_davehock-T9DOG4/oauth2/callback
//    Consumer ID644942ea-cc5f-4fef-be98-1a7f3a721929
//    Application TypeWeb
//    Application NameBoomi_ekipnlmltr
//    User redirect URLhttps://platform.boomi.com/account/boomi_davehock-T9DOG4/oauth2/callback
//    Developer Emaildave.hock@dell.com
//    App Descriptionboomi ipaas
//    Client certificateNone
//    Consumer Keyawln00et1wjdbqvzcz5zhmrtbgw1ngqa3impuh2b
//    Consumer Secretxaxpnszj0kkjru3eciftba3div1xcnp0r4mboj5f
//    OAuth 1.0a Endpointhttps://apisandbox.openbankproject.com/oauth/initiate
//    OAuth 1.0a DocumentationHow to use OAuth for OpenBankProject
//    Direct Login Endpointhttps://apisandbox.openbankproject.com/my/logins/direct

//    @Test   
    public void testQueryAccountOperation() throws Exception
    {
    	//TODO this returns paginated results!!!!!!!!!!!!
    	//We need another way to detect queries...maybe existence of RowBasedPagingParams.* parameter? 
    	String objectTypeId = "/accounts";
    	OpenBankConnector connector = new OpenBankConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.toString(), SERVICE_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();

        tester.setOperationContext(OperationType.QUERY, connProps, opProps, objectTypeId, null);
        List <SimpleOperationResult> actual = tester.executeQueryOperation(null);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
    }
}
