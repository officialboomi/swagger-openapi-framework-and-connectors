// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.personio.PersonioConnection;
import com.boomi.connector.personio.PersonioConnector;
import com.boomi.swaggerframework.MockOperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class PersonioOperationTest 
{
    static final String connectorName = "personio";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "resources/"+connectorName+"/openapi.json";
	private static JSONObject _credentials = new JSONObject(new JSONTokener(PersonioBrowseTest.class.getClassLoader().getResourceAsStream("resources/personio/testCredentials.json")));
	private static Map<String, Object> _connProps;
	Logger logger = Logger.getLogger(this.getClass().getName());

	@BeforeAll
    static void initAll() {
		_connProps = new HashMap<String,Object>();
        _connProps.put(PersonioConnection.ConnectionProperties.URL.name(), "https://api.personio.de/v1");
        _connProps.put(PersonioConnection.CustomConnectionProperties.CLIENT_SECRET.name(), _credentials.get("clientSecret"));
        _connProps.put(PersonioConnection.CustomConnectionProperties.CLIENT_ID.name(), _credentials.get("clientId"));
    }	

    //TODO we need the swagger for Transaction Search
    @Test
    public void testGetEmployeeOperation() throws Exception
    {
//    	String paymentsCookie = "{\"basePath\" : \"\"}";
    	String objectTypeId = "/company/employees/{employee_id}";
    	PersonioConnector connector = new PersonioConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
		opProps.put(SwaggerQueryOperation.OperationProperties.PAGESIZE.name(), 3);	
        opProps.put("path_employee_id","5715417");

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, _connProps, opProps, objectTypeId, null, null);
        context.setCustomOperationType("GET");	
        tester.setOperationContext(context);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream("".getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        logger.info("Response payload: " + actual);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
    }
}
