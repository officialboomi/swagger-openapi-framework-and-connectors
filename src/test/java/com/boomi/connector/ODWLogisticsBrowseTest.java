// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.odwlogistics.ODWLogisticsConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class ODWLogisticsBrowseTest {
    static final String connectorName = "odwlogistics";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "resources/"+connectorName+"/openapi.json";
    
    @Test
    public void testBrowseTypesCREATEODWLogistics() throws JSONException, Exception
    {
    	ODWLogisticsConnector connector = new ODWLogisticsConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
              
    @Test
    public void testBrowseTypesQUERYODWLogistics() throws JSONException, Exception
    {
    	ODWLogisticsConnector connector = new ODWLogisticsConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesUPDATEODWLogistics() throws JSONException, Exception
    {
    	ODWLogisticsConnector connector = new ODWLogisticsConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "PUT", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEGETODWLogistics() throws JSONException, Exception
    {
    	ODWLogisticsConnector connector = new ODWLogisticsConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEDELETEODWLogistics() throws JSONException, Exception
    {
    	ODWLogisticsConnector connector = new ODWLogisticsConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseDefinitionsQueryODWLogistics() throws JSONException, Exception
    {
        ODWLogisticsConnector connector = new ODWLogisticsConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/entity/carrier/list/simple");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryODWLogistics", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsExecuteGetODWLogistics() throws JSONException, Exception
    {
        ODWLogisticsConnector connector = new ODWLogisticsConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/entity/accessorialCode/{id}");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsExecuteGetODWLogistics", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsCreateODWLogistics() throws JSONException, Exception
    {
        ODWLogisticsConnector connector = new ODWLogisticsConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "POST", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/entity/allocatedCharge/");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateODWLogistics", getClass(), connectorName, captureExpected);
    }    
    @Test
    void analyzeSwagger() throws JSONException, IOException, DocumentException
    {
   		SwaggerDocumentationUtil.analyzeSwagger(connectorName);
    }
   	//    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
//   		SwaggerDocumentationUtil.analyzeSwagger(connectorName);
    	SwaggerDocumentationUtil.generateDescriptorDoc("ODW Logistics API", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}