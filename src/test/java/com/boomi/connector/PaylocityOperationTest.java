// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.paylocity.PaylocityConnector;
import com.boomi.swaggerframework.MockOperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.JSONUtil;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class PaylocityOperationTest 
{
    private static final String SERVICE_URL = "https://prod.cdn.paylocity.com/api";
    private static final String SWAGGER_URL = "https://prod.cdn.paylocity.com/developer/specs/weblink/latest.json";
    private static final String OAUTH_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlFDbUk2UGFjQlJUeU9hNlpzc3ozYnJ2UWI0MCIsImtpZCI6IlFDbUk2UGFjQlJUeU9hNlpzc3ozYnJ2UWI0MCJ9.eyJpc3MiOiJodHRwczovL3RpbmFwaS5xYS5wYXlsb2NpdHkuY29tL0lkZW50aXR5U2VydmVyIiwiYXVkIjoiaHR0cHM6Ly90aW5hcGkucWEucGF5bG9jaXR5LmNvbS9JZGVudGl0eVNlcnZlci9yZXNvdXJjZXMiLCJleHAiOjE2MzM2NDMwNzYsIm5iZiI6MTYzMzYzOTQ3NiwiY2xpZW50X2lkIjoiTXdCbDQ1eGNZRTZzckZtZ0N4ZzlGUzA0TlRnMk5EZzFPVFE1TURJeE9UZzFPRFUxIiwic2NvcGUiOiJXZWJMaW5rQVBJIn0.Ym2aaHmkj2u2D4-BnM_JHkOCXG7CBwB2v4uDlxUHVRakW3plhCJlteGVe4gGlpdzBqcJnyCF-6IFFnib97TMdW2VRxSM1VRH1GboJIQRlOyVSjfntNAHOLqMjMxB4FuciASK2ck-RrGm8rpGt1ft1dOpcCbzZD_il7-hMCsk0pdwOP8w_4vtEd5PUaFhUHs3l-o--ulXYQU4Wpf52tZOnZywagKcVcRFsYfnSUVzULsZKz6I0W-mp15fmHoATtQ3Y9164YUtls9S1x3gQjGKbfgapqQm9I8WFf2EaAuN9EsYsIAwDLqpV_8kQZJxPT718_9xOCJ5bP69IeSmtq85xA";
    //portal uid dave.hock
    //portal pwd Xxxxxxxxxx!## use the schema and double the king
    //redirect https://platform.boomi.com/account/boomi_davehock-T9DOG4/oauth2/callback
//    Consumer ID644942ea-cc5f-4fef-be98-1a7f3a721929
//    Application TypeWeb
//    Application NameBoomi_ekipnlmltr
//    User redirect URLhttps://platform.boomi.com/account/boomi_davehock-T9DOG4/oauth2/callback
//    Developer Emaildave.hock@dell.com
//    App Descriptionboomi ipaas
//    Client certificateNone
//    Consumer Keyawln00et1wjdbqvzcz5zhmrtbgw1ngqa3impuh2b
//    Consumer Secretxaxpnszj0kkjru3eciftba3div1xcnp0r4mboj5f
//    OAuth 1.0a Endpointhttps://apisandbox.openbankproject.com/oauth/initiate
//    OAuth 1.0a DocumentationHow to use OAuth for OpenBankProject
//    Direct Login Endpointhttps://apisandbox.openbankproject.com/my/logins/direct
    
    String payload = "{    \"benefitSetup\":{       \"benefitClass\":\"EXCLUDE\",       \"benefitClassEffectiveDate\":\"2018-10-01T00:00:00\",       \"benefitSalary\":21736.000000,       \"benefitSalaryEffectiveDate\":\"2018-10-01T00:00:00\"    },    \"birthDate\":\"1984-10-01\",    \"employeeId\":\"\",    \"ethnicity\":\"B\",    \"firstName\":\"firstName\",    \"lastName\":\"lastName\",    \"maritalStatus\":\"M\",    \"middleName\":\"middleName\",    \"preferredName\":\"\",    \"priorLastName\":\"priorLastName\",    \"salutation\":\"Mr.\",    \"gender\":\"M\",    \"isSmoker\":true,    \"ssn\":\"999999999\",    \"suffix\":\"III\",    \"taxSetup\":{       \"taxForm\":\"W2\",       \"suiState\":\"IL\"    },    \"departmentPosition\":{       \"clockBadgeNumber\":\"2345\",       \"costCenter1\":\"100\",       \"costCenter2\":\"1\",       \"costCenter3\":\"1\",       \"effectiveDate\":\"2016-08-05\",       \"employeeType\":\"RFT\",       \"equalEmploymentOpportunityClass\":\"1.1\",       \"isSupervisorReviewer\":false,       \"jobTitle\":\"jobTitle\",       \"isMinimumWageExempt\":false,       \"isOvertimeExempt\":false,       \"payGroup\":\"CORP\",       \"unionAffiliationDate\":\"2016-08-06\",       \"unionCode\":\"1\",       \"isUnionDuesCollected\":false,       \"isUnionInitiationCollected\":false,       \"unionPosition\":\"1\",       \"workersCompensation\":\"8810\",       \"positionCode\":\"PSYCH\",       \"unionCode\":null,       \"unionPosition\":null    }}";

    @Test   
    public void testCreateEmployeeOperation() throws Exception
    {
    	//TODO this returns paginated results!!!!!!!!!!!!
    	//We need another way to detect queries...maybe existence of RowBasedPagingParams.* parameter? 
    	String objectTypeId = "/v2/companies/{companyId}/employees";
    	PaylocityConnector connector = new PaylocityConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.toString(), SERVICE_URL);
        connProps.put(SwaggerConnection.ConnectionProperties.OAUTH_TOKEN.name(), OAUTH_TOKEN);

        
        Map<String, Object> opProps = new HashMap<String,Object>();

        opProps.put("path_companyId", "39393");

 		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();

 		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, objectTypeId, cookieMap, null);
         context.setCustomOperationType("POST");	
 		tester.setOperationContext(context);

         List<InputStream> inputs = new ArrayList<InputStream>();
         inputs.add(new ByteArrayInputStream(payload.getBytes()));
         List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
         assertEquals("Bad Request", actual.get(0).getMessage());
         assertEquals("400",actual.get(0).getStatusCode());
         assertEquals(1, actual.get(0).getPayloads().size());
         String responseString = new String(actual.get(0).getPayloads().get(0));
         System.out.println(responseString);
         JSONObject responseObject = new JSONObject(responseString);
    }
}
