// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.blackboard.BlackboardConnector;
import com.boomi.connector.cybersource.CybersourceConnector;
import com.boomi.connector.oraclesaas.OracleSaaSConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;

/**
 * @author Dave Hock
 */
public class CybersourceBrowseTest {
    static final String connectorName = "cybersource";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "resources/cybersource/cybersourcepayments-swagger.json";
    private static final String URL = "URL";
    
    @Test
    public void testBrowseTypesCyberSource() throws JSONException, Exception
    {
        CybersourceConnector connector = new CybersourceConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }   
    
    @Test
    public void testBrowseDefinitionsGetCybersource() throws JSONException, Exception
    {
        CybersourceConnector connector = new CybersourceConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        connProps.put("URL", URL);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/v2/captures/{id}");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetCybersource", getClass(), connectorName, captureExpected);
    } 
    
    @Test
    public void testBrowseDefinitionsCreateCybersource() throws JSONException, Exception
    {
        CybersourceConnector connector = new CybersourceConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        connProps.put("URL", URL);

        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "POST", connProps, null);
        tester.setBrowseContext(sbc);

       actual = tester.browseProfiles("/v2/payments/{id}/captures");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateCybersource", getClass(), connectorName, captureExpected);
    } 
    
    @Test
	void AnalyzeSwagger() {
		System.out.println("");
		System.out.println(connectorName.toUpperCase());
		JSONObject fullSwaggerSchema = new JSONObject(new JSONTokener(this.getClass().getClassLoader().getResourceAsStream("resources/"+connectorName+"/cybersourcepayments-swagger.json")));
		SwaggerDocumentationUtil.analyzeOpenAPI(fullSwaggerSchema);
	}	 
    
//    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("Visa Cybersource Connector", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}
