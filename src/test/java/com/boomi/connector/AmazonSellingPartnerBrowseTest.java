// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.amazonsellingpartner.AmazonSellingPartnerConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;

/**
 * @author Dave Hock
 */
public class AmazonSellingPartnerBrowseTest {
    static final String connectorName = "amazonsellingpartner";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "https://raw.githubusercontent.com/amzn/selling-partner-api-models/main/models/orders-api-model/ordersV0.json";
    Map<String, Object> _connProps;
    
    @BeforeEach
    void init()
    {
        _connProps = new HashMap<String,Object>();
    	
    }
  
    @Test
    public void testBrowseTypesDownloadReportAmazonSellingPartner() throws JSONException, Exception
    {
    	AmazonSellingPartnerConnector connector = new AmazonSellingPartnerConnector();
    	ConnectorTester tester = new ConnectorTester(connector);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "downloadReport", _connProps, null);
        tester.setBrowseContext(sbc);
        String actual = tester.browseTypes();
        assertTrue(actual.length()>0);
    }
              
    @Test
    public void testBrowseTypesCREATEAmazonSellingPartner() throws JSONException, Exception
    {
    	AmazonSellingPartnerConnector connector = new AmazonSellingPartnerConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
              
    @Test
    public void testBrowseTypesQUERYAmazonSellingPartner() throws JSONException, Exception
    {
    	AmazonSellingPartnerConnector connector = new AmazonSellingPartnerConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesUPDATEAmazonSellingPartner() throws JSONException, Exception
    {
    	AmazonSellingPartnerConnector connector = new AmazonSellingPartnerConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "PUT", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesUPDATEPatchAmazonSellingPartner() throws JSONException, Exception
    {
    	AmazonSellingPartnerConnector connector = new AmazonSellingPartnerConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "PATCH", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEGETAmazonSellingPartner() throws JSONException, Exception
    {
    	AmazonSellingPartnerConnector connector = new AmazonSellingPartnerConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEDELETEAmazonSellingPartner() throws JSONException, Exception
    {
    	AmazonSellingPartnerConnector connector = new AmazonSellingPartnerConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }   
    
    @Test
    public void testBrowseDefinitionsCreateAmazonSellingPartner() throws JSONException, Exception
    {
        AmazonSellingPartnerConnector connector = new AmazonSellingPartnerConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "POST", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/orders/v0/orders/{orderId}/approvals");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateAmazonSellingPartner", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
   		SwaggerDocumentationUtil.analyzeSwagger(connectorName);
   		
    	SwaggerDocumentationUtil.generateDescriptorDoc("Amazon Selling Partner API", connectorName);
   		SwaggerDocumentationUtil.generateMultiSwaggerWikiDoc(connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}