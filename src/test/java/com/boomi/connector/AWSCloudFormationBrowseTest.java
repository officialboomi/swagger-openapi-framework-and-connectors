// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.awscloudformation.AWSCloudFormationBrowser;
import com.boomi.connector.awscloudformation.AWSCloudFormationConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class AWSCloudFormationBrowseTest {
    static final String connectorName = "awscloudformation";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "resources/"+connectorName+"/openapi.json";
    
    @Test
    public void testBrowseTypesQUERYAWSCloudFormation() throws JSONException, Exception
    {
    	AWSCloudFormationConnector connector = new AWSCloudFormationConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    }
    @Test
    public void testBrowseTypesEXECUTEGETAWSCloudFormation() throws JSONException, Exception
    {
    	AWSCloudFormationConnector connector = new AWSCloudFormationConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseDefinitionsQueryAWSCloudFormation() throws JSONException, Exception
    {
        AWSCloudFormationConnector connector = new AWSCloudFormationConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/#Action=DescribeAccountLimits");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryAWSCloudFormation", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsExecuteCreateAWSCloudFormation() throws JSONException, Exception
    {
        AWSCloudFormationConnector connector = new AWSCloudFormationConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(AWSCloudFormationBrowser.CFOperationProperties.TEMPLATE_URL.name(), "https://veloboomicf.s3-us-west-1.amazonaws.com/Velocloud+AWS+CloudFormation+Green+Field+(33120201014).json");
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "POST", connProps, opProps);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/#Action=CreateStack");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsExecutePostAWSCloudFormation", getClass(), connectorName, captureExpected);
    }    
        
    //@Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("AWS CloudFormation", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
   		SwaggerDocumentationUtil.analyzeSwagger(connectorName);
    }    
}