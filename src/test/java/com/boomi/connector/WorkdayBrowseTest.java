// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.blackboard.BlackboardConnector;
import com.boomi.connector.oraclesaas.OracleSaaSConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.workday.WorkdayConnector;

/**
 * @author Dave Hock
 */
public class WorkdayBrowseTest {
    static final String connectorName = "workday";
    private static final boolean captureExpected = false;
	String swaggerPath = "resources/"+connectorName+"/absenceManagement_v1_2020R2Swagger.json";
    
    @Test
    public void testBrowseTypesWorkday() throws JSONException, Exception
    {
        WorkdayConnector connector = new WorkdayConnector();    	
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "POST", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "PUT", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }   
    
    @Test
    public void testBrowseDefinitionsQuery() throws JSONException, Exception
    {
    	WorkdayConnector connector = new WorkdayConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        Map<String, Object> opProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 80L);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, opProps);
        actual = tester.browseProfiles("/workers");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQuery", getClass(), connectorName, captureExpected);
    } 
}
