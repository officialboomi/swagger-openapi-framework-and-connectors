package com.boomi.swaggerframework.swaggeroperations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ui.BrowseField;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.util.StringUtil;
import com.boomi.connector.util.BaseBrowser;

//TODO need simple cookie format that has
//Request and response mimetype
//number of select parameters...

public class SwaggerBrowser extends BaseBrowser  implements ConnectionTester {
	protected Logger logger;
	JSONObject _swaggerSchema;
	protected SwaggerOperationCookie _cookie;
	protected SwaggerAPIService _swaggerService;
    public enum OperationProperties {MAXBROWSEDEPTH}
    protected PropertyMap _opProps;
    OperationType operationType;
	
    public SwaggerBrowser(SwaggerConnection conn) {
        super(conn);
        logger = Logger.getLogger(this.getClass().getName());
		_opProps = this.getContext().getOperationProperties();
    }
    
    private void initSwagger()
    {
		try {
			_swaggerSchema = this.getConnection().getSwaggerFile(this.getContext().getOperationProperties());
		} catch (Exception e) {
			throw new ConnectorException(e);
		}
    }

	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId,
			Collection<ObjectDefinitionRole> roles)		
	{
		initSwagger();
		ObjectDefinitions objDefinitions = new ObjectDefinitions();
		String schemaString;
		String cookieString;
		JSONObject schema;
		String elementName;
		operationType=this.getContext().getOperationType();//.getCustomOperationType();
		String customOperationType = this.getContext().getCustomOperationType();
		if (operationType == OperationType.QUERY)
			customOperationType = getQueryMethod();
		_swaggerService = new SwaggerAPIService(objectTypeId, customOperationType, _swaggerSchema, getMaxBrowseDepth(_opProps));
		if (operationType != OperationType.QUERY) //TODO the platform does not yet support dynamic properties for QUERY so Filters are required to set properties
			SwaggerBrowseUtil.createPathParameterFields(objDefinitions, _swaggerService.getPathParameters());
		for(ObjectDefinitionRole role : roles)
		{
			_cookie=new SwaggerOperationCookie(_swaggerService);
			if (ObjectDefinitionRole.INPUT == role)
			{
				ObjectDefinition objDefinition = new ObjectDefinition();
				switch (operationType)
				{
				case EXECUTE:
					//TODO PATH PARAMETERS for update, upsert!!!!
					schemaString=this.getTypeJSONSchema(role, operationType, customOperationType);
					if (StringUtil.isBlank(schemaString))
					{
						schema = _swaggerService.getRequestBodySchema();
						if (schema!=null)
							schemaString = schema.toString();						
					}
					if (StringUtil.isBlank(schemaString))
					{
						objDefinition.setInputType(ContentType.NONE);
					} else {
						objDefinition.setInputType(ContentType.JSON);
						elementName=this.getTypeElementName(role, operationType, customOperationType);
						objDefinition.setElementName("/"+elementName); //specify root element as schema location
						objDefinition.setJsonSchema(schemaString);
					}
					objDefinitions.getDefinitions().add(objDefinition);	
					break;
				default:
					throw new ConnectorException("Operation " + operationType + "not supported");
				}	
				cookieString = _cookie.toString();
				logger.info("Input Operation Cookie Persisted: " + cookieString );
				objDefinition.setCookie(cookieString);
			} 
			else if (ObjectDefinitionRole.OUTPUT == role)
			{
				ObjectDefinition objDefinition = new ObjectDefinition();
				switch (operationType)
				{
				case EXECUTE:
					schemaString=this.getTypeJSONSchema(role, operationType, customOperationType);
					if (StringUtil.isBlank(schemaString))
					{
						schema = _swaggerService.getResponseBodySchema();
						if (schema!=null)
							schemaString = schema.toString();						
					}
					if (StringUtil.isBlank(schemaString))
					{
						objDefinition.setOutputType(ContentType.NONE);
					} else {
						objDefinition.setOutputType(ContentType.JSON);
						elementName=this.getTypeElementName(role, operationType, customOperationType);
						objDefinition.setElementName("/"+elementName); //specify root element as schema location
						objDefinition.setJsonSchema(schemaString);
					}
					objDefinitions.getDefinitions().add(objDefinition);								
					break;
				case QUERY:					
					JSONArray parameters = this.getMethodParameters(objectTypeId, this.getQueryMethod());
					if (parameters!=null && !isPaginatedQuery(parameters))
						_cookie.setIsPaginatedQuery(false);
					objDefinition.setOutputType(ContentType.JSON);
					
					schemaString=this.getTypeJSONSchema(role, operationType, customOperationType);
					String queryPaginationSplitPath = getQueryPaginationSplitPath(objectTypeId);
					if (StringUtil.isEmpty(queryPaginationSplitPath))
						queryPaginationSplitPath = getConnection().getQueryPaginationSplitPath();
					//Oracle Specific to get drill down to items for pagination
					//A response body is required for Query
					if (StringUtil.isBlank(schemaString))
					{
						schema = _swaggerService.getQueryPaginationResponseBodySchema(queryPaginationSplitPath);
						if (schema!=null)
							schemaString = schema.toString();	
						queryPaginationSplitPath = _swaggerService.getQueryPaginationSplitPath();
					}
					logger.info("split path" + queryPaginationSplitPath);
					_cookie.setQueryPaginationSplitPath(queryPaginationSplitPath);
					if (StringUtil.isBlank(schemaString))
					{
//						throw new ConnectorException("No response body schema was found for QUERY operation.");
						objDefinition.setOutputType(ContentType.NONE);
					} else {
						objDefinition.setOutputType(ContentType.JSON);
						elementName=this.getTypeElementName(role, operationType, customOperationType);
						objDefinition.setElementName("/"+elementName); //specify root element as schema location
						objDefinition.setJsonSchema(schemaString);
						
						this.getPathParameterFilterSpecs(_swaggerService, objDefinition.getFieldSpecFields());
						this.getFilterSortSpecs(_swaggerService, objDefinition.getFieldSpecFields());
					}
					objDefinitions.getDefinitions().add(objDefinition);								
					break;
				default:
					throw new ConnectorException("Operation " + operationType + "not supported");
				}
				if (schemaString!=null && schemaString.indexOf("\"$ref\"")>=1)
					throw new ConnectorException("Schema has unresolved $ref: " + role.toString());
				cookieString = _cookie.toString();
				logger.info("Output Operation Cookie Persisted: " + cookieString );
				objDefinition.setCookie(cookieString);
			}
		}
		if (operationType==OperationType.EXECUTE)
		{
			this.createHeaderParameterFields(objDefinitions);
			this.createQueryParameterFields(objDefinitions);
		}
		return objDefinitions;
	}

	/*
	 * Override if query operations use other methods such as POST
	 */
	protected String getQueryMethod() {
		return "GET";
	}

	protected String getTypeElementName(ObjectDefinitionRole role, OperationType operationType, String customOperationType) {
		return _swaggerService.getTypeElementName();
	}

	protected String getTypeJSONSchema(ObjectDefinitionRole role, OperationType operationType, String customOperationType) {
		return null;
	}

	private void createQueryParameterFields(ObjectDefinitions objectDefinitions)
	{
    	JSONArray parameters = this._swaggerService.getQueryParameters();
    	for (int i=0; i<parameters.length(); i++)
    	{
    		JSONObject parameter = parameters.getJSONObject(i);
    		String parameterName = parameter.getString("name");
    		if (!this.excludeQueryParameter(parameterName))
    		{
        		BrowseField browseField = SwaggerBrowseUtil.createQueryParameterField(parameter); 
        		if (browseField!=null)
        		{
        			objectDefinitions.getOperationFields().add(browseField);
        		}
    		}
		}
	}
	
	private void createHeaderParameterFields(ObjectDefinitions objectDefinitions)
	{
    	JSONArray parameters = this._swaggerService.getHeaderParameters();
    	for (int i=0; i<parameters.length(); i++)
    	{
    		JSONObject parameter = parameters.getJSONObject(i);
    		String parameterName = parameter.getString("name");
    		if (!this.excludeHeaderParameter(parameterName))
    		{
        		BrowseField browseField = SwaggerBrowseUtil.createHeaderParameterField(parameter); 
        		if (browseField!=null)
        		{
        			objectDefinitions.getOperationFields().add(browseField);
        		}
    		}
		}
	}
	
	protected boolean excludeHeaderParameter(String name)
	{
		return true;
	}
	
	protected boolean excludeQueryParameter(String name)
	{
		return this.operationType==OperationType.QUERY;
	}
	
	@Override
	public ObjectTypes getObjectTypes() 
	{
		initSwagger();
		OperationType operationType = this.getContext().getOperationType();
		String customOperationType = this.getContext().getCustomOperationType();
		List<String> excludedObjectTypeIDs = getConnection().getExcludedObjectTypeIDs();
		ObjectTypes objectTypes = new ObjectTypes();		
		JSONObject paths = _swaggerSchema.getJSONObject("paths");
		Iterator<String> pathIter = paths.keys();
		
		List<String> supportedMethods = new ArrayList<String>();
		supportedMethods.add("put");
		supportedMethods.add("patch");
		supportedMethods.add("get");
		supportedMethods.add("delete");
		supportedMethods.add("post");
		while(pathIter.hasNext())
		{
			String pathString=pathIter.next();
			JSONObject path = paths.getJSONObject(pathString);
			Iterator<String> methodIter = path.keys();
			while(methodIter.hasNext())
			{
				boolean doInclude=false;
				String methodName = methodIter.next();
				//ignore the parameters array for parameters shared across all methods
				if (supportedMethods.contains(methodName))
				{
					JSONArray parameters=null;
					JSONObject methodObject = path.getJSONObject(methodName);
					if (methodObject.has("parameters"))
						parameters = methodObject.getJSONArray("parameters");
					
					switch (operationType)
					{
					case QUERY:
						if (this.isQueryOperation(pathString, methodName, parameters, methodObject))
							doInclude=true;
						break;
					case EXECUTE:
//						if (!this.isQueryOperation(pathString, methodName, parameters, methodObject))
						{
							switch (customOperationType)
							{
							case "GET":
								if (methodName.toLowerCase().contentEquals("get"))
									doInclude=true;
								break;
							default:
								if (methodName.toLowerCase().contentEquals(customOperationType.toLowerCase()))
									doInclude=true;
								break;
							}
						}
						break;
					default:
						throw new ConnectorException("Operation " + operationType + "not supported");
					}						
					if (doInclude)
					{
						String objectTypeID = pathString;
						if (excludedObjectTypeIDs==null || !excludedObjectTypeIDs.contains(objectTypeID))
						{
							String updateOperationName = "";
							if (SwaggerBrowseUtil.isUpdate(methodName, pathString))
								updateOperationName = methodName.toUpperCase();
							if (methodObject.has("x-operations"))
							{
								JSONArray methods = methodObject.getJSONArray("x-operations");
								for (int i=0; i<methods.length(); i++)
								{
									JSONObject xOperation = methods.getJSONObject(i);
									//Append x-operations index to the objectTypeID
									String newObjectTypeID = objectTypeID+SwaggerBrowseUtil.OBJECTID_XOPERATION_DELIMITER+i;
									ObjectType objectType = this.getObjectType(xOperation, newObjectTypeID, updateOperationName);
									objectTypes.getTypes().add(objectType);
								}
							}
							else
							{
								ObjectType objectType = this.getObjectType(methodObject, objectTypeID, updateOperationName);
								objectTypes.getTypes().add(objectType);
							}
						}
					}
				}
			}
		}
		SwaggerBrowseUtil.sortObjectTypes(objectTypes);
		return objectTypes;
	}
	
	//TODO for oracle x-operations, we will call this multiple times per path/method
	private ObjectType getObjectType(JSONObject methodObject, String objectTypeId, String updateOperationName)
	{
		//TODO we might want an option to choose per connector whether to use tag/summary/description since so much variation between swagger implementations
		String summary = null;
		String description = null;
		List<String> tags = new ArrayList<String>();
		String operationId = null;
		
		if (methodObject.has("summary"))
			summary = methodObject.getString("summary");
		
		if (methodObject.has("description"))
			description = methodObject.getString("description");
		if (methodObject.has("tags"))
		{
			JSONArray tagArray = methodObject.getJSONArray("tags");	
			for (int i=0; i<tagArray.length(); i++)
				tags.add(tagArray.getString(i));
		}
		if (methodObject.has("operationId"))
			operationId = methodObject.getString("operationId");
		
		String objectTypeLabel=getObjectTypeLabel(operationId, summary, description, tags);
		
		if (objectTypeLabel==null)
		{
			if (StringUtil.isBlank(summary) && StringUtil.isNotBlank(description))
			{
				summary = description;	
			}
			if (StringUtil.isNotBlank(summary))
			{
				summary = summary.replaceAll("<p>", "");
				summary = summary.replaceAll("</p>", "");
				summary = summary.replaceAll("\r", "");
				summary = summary.replaceAll("\n", " ");
				
				int maxSummaryLength = 50;
				if (summary.length()>maxSummaryLength)
				{
					summary=summary.substring(0, maxSummaryLength)+"...";
				}
			}

			objectTypeLabel = SwaggerBrowseUtil.getObjectTypeLabel(methodObject);
			if (summary!=null)
				objectTypeLabel+=" - " + summary; //also description? also /tags[]/description
			if (updateOperationName!=null && updateOperationName.length()>0)
				objectTypeLabel+= " ("+updateOperationName+")";
		}

		ObjectType objectType = new ObjectType();
		objectType.setId(objectTypeId); 

		objectType.setLabel(objectTypeLabel);

		String objectTypeHelpText=getObjectTypeHelpText(operationId, summary, description, tags);
		if (objectTypeHelpText==null)
			objectTypeHelpText = SwaggerBrowseUtil.getObjectTypeHelpText(methodObject);
		objectType.setHelpText(objectTypeHelpText);
		
		return objectType;
	}
	
	JSONArray getMethodParameters(String objectTypeId, String methodName)
	{
		JSONArray parameters = null;
		String pathName = objectTypeId;
		methodName = methodName.toLowerCase();
		JSONObject paths = this._swaggerSchema.getJSONObject("paths");
		JSONObject path = paths.getJSONObject(pathName);
		JSONObject method = path.getJSONObject(methodName);
		if (method.has("parameters"))
			parameters = method.getJSONArray("parameters");
		return parameters;
	}
	
	protected PropertyMap getOperationProperties()
	{
		return _opProps;
	}
	
	/**
	 * @return Returns the maximum depth set by the user in the import operations page for resolving recursive $ref during Import/Browse
	*/
    protected long getMaxBrowseDepth(PropertyMap opProps)
    {
    	long maxDepth=10L;
    	long defaultDepth=5L;
    	long depth = opProps.getLongProperty(OperationProperties.MAXBROWSEDEPTH.name(), defaultDepth);
    	if (depth>maxDepth)
    		depth=maxDepth;
    	return depth;
    }

	@Override
	public void testConnection() {
		try {
			this.getConnection().testConnectionInternal();
        }
        catch (Exception e) {
            throw new ConnectorException("Could not establish a connection", e);
        }
	}

	@Override
    public SwaggerConnection getConnection() {
        return (SwaggerConnection) super.getConnection();
    }
		
	//To be overridden
	//For GET only
	//TODO pass in path value parameters so we can check for a offset or some other pagination parameter
	//for example, with blackboard look in parameters for  {"$ref": "#/parameters/RowBasedPagingParams.offset"},
	//for oracle: look for parameters 
	//     { "name":"offset",
	//    "description":"Used to define the starting position of the resource collection. If offset exceeds the resource count then no resources are returned. Default value is 0.",
	//    "in":"query",
	//    "type":"integer"
	//OpenBank needs responses dereferenced: responses->200AccountsRead->OBReadAccount2->Links->First
	//Best if we can rely on not having {} at the end so we default to that

	//We add pathParams as filter only fields. Note an exception will be thrown in QueryOperation if these not included in a top level AND filter
	private void getPathParameterFilterSpecs(SwaggerAPIService swaggerService, List<FieldSpecField> fields)
	{
		JSONArray pathParams = swaggerService.getPathParameters();
		if (pathParams != null && pathParams.length()>0)
		{
			for (int i=0; i<pathParams.length(); i++)
			{
				JSONObject param = pathParams.getJSONObject(i);
				//type better be string
				fields.add(new FieldSpecField().withName(param.getString("name")).withFilterable(true).withSortable(false).withType("string"));
			}
		}
	}
	
	/**
	 * Build a label for the object type from the Open API elements provided. Note parameters may be null.
	 * @param operationId
	 * @param summary
	 * @param description
	 * @param tags
	 * @return
	 */
	protected String getObjectTypeHelpText(String operationId, String summary, String description, List<String> tags) {
		return null;
	}

	/**
	 * Build help text for the object type from the  Open API elements provided. Note parameters may be null.
	 * @param operationId
	 * @param summary
	 * @param description
	 * @param tags
	 * @return
	 */
	protected String getObjectTypeLabel(String operationId, String summary, String description, List<String> tags) {
		return null;
	}

	/**
	 * Indicate whether a specific openAPI path/http method is a query operation which has pagination, etc
	 * @param pathString - the full path of the endpoint
	 * @param methodName - the method name for the path (ie get, post...)
	 * @param queryParameters - the query parameters for the method
	 * @param methodObject - the complete method object including request and response parameters
	 * @return a boolean indicating whether or not a path/method is a query operation
	 */
	protected boolean isQueryOperation(String pathString, String methodName, JSONArray queryParameters, JSONObject methodObject)
	{
		return SwaggerBrowseUtil.isQuery(methodName, pathString);
	}
	
	protected boolean isCreateOperation(String pathString, String methodName, JSONArray queryParameters, JSONObject methodObject)
	{
		return SwaggerBrowseUtil.isCreate(methodName, pathString);
	}
	
	/**
	 * Populate the fields that should appear in the Query Operation's Fields/Filter/Sort lists
	 * @param swaggerService a swaggerService object that provides access to options in the openAPI/swagger file
	 * @param fieldSpecFields the list of FieldSpecField objects that specify what fields appear in the Fields/Filter/Sort lists in the Query Operation page
	 */
	protected void getFilterSortSpecs(SwaggerAPIService swaggerService, List<FieldSpecField> fieldSpecFields) {
	}
	
	public static boolean hasQueryParameter(JSONArray queryParameters, String target)
	{
		for (int i=0; queryParameters !=null && i<queryParameters.length(); i++)
		{	JSONObject parameter = (JSONObject)queryParameters.get(i);
			if (parameter.has("in"))
				if (parameter.getString("in").contentEquals("query"))
					if (parameter.has("name"))
						if (parameter.getString("name").contentEquals(target))
							return true;
		}
		return false;
	}

	protected boolean isPaginatedQuery(JSONArray queryParameters) {
		return true;
	}	
	
	protected String getQueryPaginationSplitPath(String objectTypeId)
	{
		return null;
	}
}