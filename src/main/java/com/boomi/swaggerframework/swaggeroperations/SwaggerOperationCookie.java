package com.boomi.swaggerframework.swaggeroperations;

import org.json.JSONObject;

import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;

public class SwaggerOperationCookie {
	private JSONObject cookie;
	public SwaggerOperationCookie(String cookieString)
	{
		if (cookieString!=null && cookieString.length()>0)
			cookie=new JSONObject(cookieString);
//TODO ignore this for now since we default basepath to ""
//		else
//			throw new ConnectorException("Cookie is null, please reimport the operation");
	}
	
	public SwaggerOperationCookie(SwaggerAPIService swaggerService)
	{
		cookie=new JSONObject();
		cookie.put("basePath", swaggerService.getBasePath());
	}

	public String toString()
	{
		if (cookie!=null)
			return cookie.toString();
		return "";
	}
	
	public String toString(int indent)
	{
		if (cookie!=null)
			return cookie.toString(indent);
		return "";
	}
	
	public String getBasePath()
	{
		if (cookie!=null && cookie.has("basePath"))
			return this.cookie.getString("basePath");
		return "";
	}
	
	public boolean isPaginatedQuery()
	{
		if (cookie!=null && cookie.has("isPaginatedQuery"))
			return this.cookie.getBoolean("isPaginatedQuery");
		return true;
	}
	
	public void setIsPaginatedQuery(boolean isPaginatedQuery)
	{
		cookie.put("isPaginatedQuery", isPaginatedQuery);
	}
	
	public String getQueryPaginationSplitPath()
	{
		if (cookie!=null && cookie.has("queryPaginationSplitPath"))
			return this.cookie.getString("queryPaginationSplitPath");
		return null;
	}

	public void setQueryPaginationSplitPath(String queryPaginationSplitPath)
	{
		cookie.put("queryPaginationSplitPath", queryPaginationSplitPath);
	}
	
	public String getRequestMimeType()
	{
		if (cookie!=null && cookie.has("requestMimeType"))
			return this.cookie.getString("requestMimeType");
		return "application/json";
	}
	public String getResponseMimeType()
	{
		if (cookie!=null && cookie.has("responseMimeType"))
			return this.cookie.getString("responseMimeType");
		return "application/json";
	}
}
