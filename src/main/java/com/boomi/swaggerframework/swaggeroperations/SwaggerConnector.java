package com.boomi.swaggerframework.swaggeroperations;

import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerExecuteOperation;
import com.boomi.connector.util.BaseConnector;

public abstract class SwaggerConnector extends BaseConnector {
	
	static final PoolingHttpClientConnectionManager GLOBAL_CONNECTION_MANAGER = new PoolingHttpClientConnectionManager();

	static {
		GLOBAL_CONNECTION_MANAGER.setMaxTotal(50);
		GLOBAL_CONNECTION_MANAGER.setDefaultMaxPerRoute(10);
	}

	@Override
    public Browser createBrowser(BrowseContext context) {
        return new SwaggerBrowser(createConnection(context));
    }    

    @Override
    protected Operation createQueryOperation(OperationContext context) {
        throw new ConnectorException("Query Not Implemented");
    }

    @Override
    protected Operation createGetOperation(OperationContext context) {
        throw new ConnectorException("GET Not Implemented, use EXECUTE/GET");
    }

    //TODO DO NOT USE BECAUSE DELETE DOESN'T SUPPORT COOKIES FOR the BASEPATH    
    @Override
    protected Operation createDeleteOperation(OperationContext context) {
        throw new ConnectorException("Delete Not Implemented, , use EXECUTE/DELETE");
    }

    @Override
    protected Operation createCreateOperation(OperationContext context) {
        throw new ConnectorException("Create Not Implemented, use EXECUTE/CREATE");
    }

    @Override
    protected Operation createUpdateOperation(OperationContext context) {
        throw new ConnectorException("Update Not Implemented, use EXECUTE/UPDATE");
    }

    @Override
    protected Operation createExecuteOperation(OperationContext context) {
        return new SwaggerExecuteOperation(createConnection(context));
    }
   
    protected SwaggerConnection createConnection(BrowseContext context) {
        return null;
    }

	@Override
	protected void finalize() throws Throwable {
//TODO THIS BREAKS JUNIT WE MUST FIX JUNIT
//	   	IOUtil.closeQuietly(GLOBAL_CONNECTION_MANAGER);
	   	super.finalize();
	}
}