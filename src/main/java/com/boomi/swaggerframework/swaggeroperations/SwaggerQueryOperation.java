package com.boomi.swaggerframework.swaggeroperations;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.DynamicPropertyMap;
import com.boomi.connector.api.Expression;
import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.GroupingExpression;
import com.boomi.connector.api.GroupingOperator;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.api.QueryRequest;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.ConnectionProperties;
import com.boomi.swaggerframework.swaggerutil.JSONResponseSplitter;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.swaggerframework.swaggerutil.SwaggerUtil;
import com.boomi.connector.util.BaseQueryOperation;
import com.boomi.util.CollectionUtil;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;

public abstract class SwaggerQueryOperation extends BaseQueryOperation {
    public enum PaginationType {PAGINATION_TYPE_RECORD_OFFSET, PAGINATION_TYPE_PAGE_OFFSET, PAGINATION_TYPE_LAST_ID, PAGINATION_TYPE_LINK_HEADER, PAGINATION_TYPE_URL_FIELD, PAGINATION_TYPE_OTHER, PAGINATION_TYPE_TOKEN_PARAMETER}
    public enum OperationProperties {MAXDOCUMENTS, PAGESIZE, ALLOW_FIELD_SELECTION}
    	

	QueryFilter _queryFilter=null;
   	long _maxDocuments;
   	long _pageSize;
   	PropertyMap _opProps;
   	String _lastIdInPage;
   	CloseableHttpResponse httpResponse;
   	Logger logger;
   	SwaggerOperationCookie _cookie;
	
	protected SwaggerQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected void executeQuery(QueryRequest request, OperationResponse response) {
       	logger = response.getLogger();
		getConnection().setLogger(logger);
		String cookieString = this.getContext().getObjectDefinitionCookie(ObjectDefinitionRole.OUTPUT);
		_cookie = new SwaggerOperationCookie(cookieString);
		logger.info("Operation Cookie Loaded: " + cookieString );
        _opProps = getContext().getOperationProperties();
       	_maxDocuments = getMaxDocuments(_opProps);
       	_pageSize = getPageSize(_opProps);
		if (getPageSizeQueryParameterName()!=null && _pageSize<=0)
			throw new ConnectorException("Must specify the Page Size to be greater than 0");
		
        FilterData input = request.getFilter();
        if (input!=null)
        	_queryFilter = input.getFilter();

		boolean error=false;
		//TODO each pathParam must have a filter term in the top level AND....if not throw an error
		try {
			String path = getPathWithParameterValuesFromFilter();
			//TODO We want to support both filter props and dynamic props for query path params
			//This requires having one take priority and for it to progressively add to a path that still has pending params to fill in
			//TODO so error checking can't be done until both are complete.
			//TODO the platform does not yet support dynamic properties for QUERY so Filters are required to set properties
//			DynamicPropertyMap dynamicProperties = input.getDynamicOperationProperties();
//			path = SwaggerBrowseUtil.getURLPathWithParameterValues(getContext().getObjectTypeId(), _opProps, dynamicProperties, path);            		
			if (path.contains("{"))
				throw new ConnectorException("Path placeholder replacements not complete. Please add all parameters to the top filter grouping: "+ path);
			String uriParams="";
	        uriParams = appendPath(uriParams,input.getDynamicProperties().get("EXTRA_URL_PARAMETERS"));
	        uriParams = appendPath(uriParams,_opProps.getProperty("EXTRA_URL_PARAMETERS"));
	        uriParams = appendPath(uriParams, getSelectTermsQueryParam(this.getContext().getSelectedFields()));
	    	if (_queryFilter!=null)
	    	{
	    		String expressions = getFilterTermsQueryParam(_queryFilter.getExpression(), 0);
	    		if (StringUtil.isNotBlank(expressions) && this.getFilterQueryParameter()!=null)
	    			expressions = this.getFilterQueryParameter()+"="+expressions;
	    		uriParams = appendPath(uriParams, expressions);
	    		uriParams = appendPath(uriParams, getSortTermsQueryParam(_queryFilter.getSort()));
	    	}    		
  	
	       	uriParams = appendPath(uriParams, input.getDynamicProperties().get("extraURIParams"));
	       	if (this.getPageSizeQueryParameterName()!=null && _cookie.isPaginatedQuery())      		
	       		uriParams = appendPath(uriParams, this.getPageSizeQueryParameterName()+"=" + _pageSize);
	       	String fullpath = path + uriParams;
	       	//TODO pass dynamic op prop headers
	       	httpResponse = this.getConnection().doExecute(fullpath, null, "GET", _cookie, null, this.getContext());
	        long numDocuments = 0; 
	        long numInPage;
	        
	        //TODO Why the nested try/catch? 
	        try {
	        	JSONResponseSplitter respSplitter=null;
		        do {   
                	numInPage=0;
	                InputStream is = null;
	                try {
	                	if (numDocuments>0)//We already have some documents so get the next page from the API
	                	{
	                		//get the nextpage
	                		String nextPageURL = getNextPagePath(path, uriParams, respSplitter, numDocuments);
				           	httpResponse = getConnection().doExecute(nextPageURL, null, "GET", _cookie, null, this.getContext());
	                	}
	                    is = httpResponse.getEntity().getContent();
	                    int httpResponseCode = httpResponse.getStatusLine().getStatusCode();
	                    if (is != null && (httpResponseCode==200)) {
//	                    	if (true)
//	                    	{
//	                    		String testPayload = SwaggerUtil.inputStreamToString(is);
//	                    		is = new ByteArrayInputStream(testPayload.getBytes());
//	                    		System.out.println("Response:\r\n" + testPayload);
//	                    	}
	                    	String queryPaginationSplitPath = _cookie.getQueryPaginationSplitPath();
	                    	if (queryPaginationSplitPath==null)
	                    		queryPaginationSplitPath = getConnection().getQueryPaginationSplitPath();
	                    	if (queryPaginationSplitPath==null)
	                    		throw new ConnectorException ("Connection.queryPaginationSplitPath not set, please reimport the operation.");
	                    	respSplitter = new JSONResponseSplitter(is,
	                    			queryPaginationSplitPath, this.getNextPageElementPath(), this.getHasMoreElementPath());
	                    	for(Payload p : respSplitter) {	
				            	numDocuments++;
				            	numInPage++;
				            	if (numInPage==this._pageSize && this.getPaginationType()==PaginationType.PAGINATION_TYPE_LAST_ID)
				            	{
		                    		OutputStream tempOutputStream = getContext().createTempOutputStream();
		                    		OutputStream tempOutputStream2 = getContext().createTempOutputStream();
		                    		p.writeTo(tempOutputStream);

				            		this._lastIdInPage = SwaggerBrowseUtil.getLastID(this.getIdElementPath(), getContext().tempOutputStreamToInputStream(tempOutputStream), tempOutputStream2);
				            		p = PayloadUtil.toPayload(getContext().tempOutputStreamToInputStream(tempOutputStream2), null);
				            	}
			                    response.addPartialResult(input, OperationStatus.SUCCESS, httpResponseCode+"", httpResponse.getStatusLine().getReasonPhrase(), p);
			            		if (_maxDocuments > 0 && numDocuments >= _maxDocuments)
			                		break;
			                }
			                respSplitter.close();
				           	logger.log(Level.INFO, "Document index:" + numDocuments);
	                    }
	                    else {
//	                    	response.addResult(input, status, httpResponseCode+"",
//	                        		httpResponse.getStatusLine().getReasonPhrase(), ResponseUtil.toPayload(obj));
//	                        response.addEmptyResult(input, OperationStatus.APPLICATION_ERROR, httpResponseCode+"",
//	                        		httpResponse.getStatusLine().getReasonPhrase());
	                        response.addResult(input, OperationStatus.APPLICATION_ERROR, httpResponseCode+"",
	                        		httpResponse.getStatusLine().getReasonPhrase(), ResponseUtil.toPayload(is));
	                    	logger.severe(httpResponseCode + " " + httpResponse.getStatusLine().getReasonPhrase());
	                        error=true;
	                        break;
	                    }
	                }
	                finally {
	                    IOUtil.closeQuietly(is);
	                    IOUtil.closeQuietly(httpResponse);
	                }
            		if (_maxDocuments > 0 && numDocuments >= _maxDocuments)
                		break;
		        } while(hasMore(respSplitter, numInPage));	        
	        } catch (Exception e) {
	        	SwaggerUtil.handleException(e, logger);
	            ResponseUtil.addExceptionFailure(response, input, e);
            	logger.severe(e.getMessage());
	        }
	        if (!error)
	        	response.finishPartialResult(input);
		} catch (Exception e) {
        	SwaggerUtil.handleException(e, logger);
        	logger.severe(e.getMessage());
            ResponseUtil.addExceptionFailure(response, input, e);
		}
	}
	
	@Override
    public SwaggerConnection getConnection() {
        return (SwaggerConnection) super.getConnection();
    }	
		
	//Substitute path parameter path placeholdes with top level filters and remove filters for path parameters
	//TODO We don't enforce filter operator to be EQUALS for path parameter expressions
	//....but should we care. User might get confused if they use something other than EQUALS which we should throw an error for
	private String getPathWithParameterValuesFromFilter()
    {
		List<String> pathParams = SwaggerBrowseUtil.getPathParams(getContext().getObjectTypeId());
		String path = getContext().getObjectTypeId();
		//TODO Compatibility for when method was at end of path
		if (path.contains(SwaggerBrowseUtil.DEPRECATED_PATH_METHOD_OBJECTID_DELIMITER))
			path=path.substring(0, path.indexOf(SwaggerBrowseUtil.DEPRECATED_PATH_METHOD_OBJECTID_DELIMITER));
		if (pathParams.size()==0)
			return path;
             
		if((_queryFilter != null) && (_queryFilter.getExpression() != null)) {
			Expression baseExpression = _queryFilter.getExpression();
            if(baseExpression instanceof SimpleExpression) {
            	SimpleExpression simpleExpression = (SimpleExpression)baseExpression;
                // base expression is a single simple expression
            	if (pathParams.contains(simpleExpression.getProperty()))
    			{
            		if (simpleExpression.getArguments()==null || simpleExpression.getArguments().size()!=1 || StringUtil.isEmpty(simpleExpression.getArguments().get(0)))
            			throw new ConnectorException("One argument required for path parameter: " + simpleExpression.getProperty());
            		path=path.replace("{"+simpleExpression.getProperty()+"}", simpleExpression.getArguments().get(0));
                	//Null query filter as filter was consumed for path parameter
                	_queryFilter=null;            	
    			}
            } else {
                GroupingExpression groupingExpression = (GroupingExpression)baseExpression;
                // we only support "AND" groupings
                if(groupingExpression.getOperator() != GroupingOperator.AND) {
                    throw new IllegalStateException("Path parameters require a top level AND grouping " + groupingExpression.getOperator() + path);
                }
                //Iterate in reverse so elements can be deleted
                for (int i=groupingExpression.getNestedExpressions().size()-1; i>=0; i--)
                {
                	Expression nestedExpression = groupingExpression.getNestedExpressions().get(i);
                	if((nestedExpression instanceof SimpleExpression)) {
                    	SimpleExpression simpleExpression = (SimpleExpression)nestedExpression;
                    	if (pathParams.contains(simpleExpression.getProperty()))
            			{
                    		if (simpleExpression.getArguments()==null || simpleExpression.getArguments().size()!=1 || StringUtil.isEmpty(simpleExpression.getArguments().get(0)))
                    			throw new ConnectorException("One argument required for path parameter: " + simpleExpression.getProperty());
                    		path=path.replace("{"+simpleExpression.getProperty()+"}", simpleExpression.getArguments().get(0));
                    		groupingExpression.getNestedExpressions().remove(i);
            			}
                    }
                }
            }
 		}   		
    	return path;
    }
	
	static String appendPath(String uriParams, String newParams)
	{
		if (newParams != null && newParams.length()>0)
		{
			if (uriParams==null)
				uriParams = "";
			if (uriParams.length()>0)
				uriParams += "&" + newParams;
			else 
				uriParams = "?" + newParams;
		}
		return uriParams;
	}

	static void appendPath(StringBuilder uriParams, String newParams)
	{
		if (newParams != null && newParams.length()>0)
		{
			if (uriParams.length()>0)
				uriParams.append("&");
			else 
				uriParams.append("?");	
			uriParams.append(newParams);
		}
	}

	private boolean hasMore(JSONResponseSplitter respSplitter, long numInPage)
	{
		//if the query is not paginated, we risk an infinite loop if we apply offset/limit pagination
		if (!_cookie.isPaginatedQuery())
			return false;
		if (numInPage<this._pageSize)
			return false;
		boolean hasMore=false;
		PaginationType paginationType = this.getPaginationType();
		if (paginationType==null)
			paginationType=PaginationType.PAGINATION_TYPE_RECORD_OFFSET;
		switch(paginationType)
		{
			case PAGINATION_TYPE_LINK_HEADER:
				hasMore = getNextLinkHeaderNextPageURL()!=null;
				break;
			case PAGINATION_TYPE_URL_FIELD:
			case PAGINATION_TYPE_TOKEN_PARAMETER:
				hasMore = respSplitter.getNextPageElementValue()!=null;
				break;
			default:
				if (StringUtil.isNotBlank(getHasMoreElementPath()))
					hasMore = respSplitter.getHasMore()!=null && respSplitter.getHasMore().toLowerCase().contentEquals("true");
				else 
					hasMore = (numInPage == _pageSize); //TODO what if no pagination is supported? We might get infinite loop
		}
		logger.info("Has more: " + hasMore);
		logger.info(String.format("numInPage %d _pageSize: %d ", numInPage, _pageSize));
		return hasMore;
	}
	
	private String getNextPagePath(String basePath, String uriParams, JSONResponseSplitter respSplitter, long numDocuments)
	{
		String nextPagePath=null;
		String nextPageQueryParameterName;
		PaginationType paginationType = this.getPaginationType();
		
		if (paginationType==null)
			paginationType=PaginationType.PAGINATION_TYPE_RECORD_OFFSET;
		
		switch(paginationType)
		{
			case PAGINATION_TYPE_LINK_HEADER:
				nextPagePath=getNextLinkHeaderNextPageURL();
				if (StringUtil.isNotBlank(nextPagePath))
				{
		       		if (!nextPagePath.startsWith("https://")) //TODO an error if it does NOT start with https??
		       			throw new ConnectorException("Next Page URL NG: " + nextPagePath);
		       		nextPagePath=nextPagePath.substring(getConnection().getBaseUrl().length()+getConnection().getBasePath().length()); //Hack off the base URL to get just the path		
				}
				break;
			case PAGINATION_TYPE_URL_FIELD:
				if (StringUtil.isBlank(getNextPageElementPath()))
					throw new ConnectorException("Must implement getNextPageElementPath() for PAGINATION_TYPE_URL_FIELD");
				nextPagePath = respSplitter.getNextPageElementValue();
	       		if (!nextPagePath.startsWith("https://")) //TODO an error if it does NOT start with https??
	       			throw new ConnectorException("Next Page URL NG: " + nextPagePath);
	       		nextPagePath=nextPagePath.substring(getConnection().getBaseUrl().length()+getConnection().getBasePath().length()); //Hack off the base URL to get just the path		
		       	break;
			case PAGINATION_TYPE_LAST_ID:
				nextPageQueryParameterName = getNextPageQueryParameterName();
				if (StringUtil.isBlank(nextPageQueryParameterName))
					throw new ConnectorException("getNextPageQueryParameterName must be overridden to provide the name for example 'after', 'starting_after'...");
				nextPagePath = basePath+appendPath(uriParams, nextPageQueryParameterName+"="+_lastIdInPage);
				break;
			case PAGINATION_TYPE_RECORD_OFFSET:
				nextPageQueryParameterName = getNextPageQueryParameterName();
				if (StringUtil.isBlank(nextPageQueryParameterName))
					nextPageQueryParameterName="offset";
				long recordOffset=numDocuments;
				if (!this.isZeroBasedOffset())
					recordOffset++;
				nextPagePath = basePath+appendPath(uriParams, nextPageQueryParameterName+"="+recordOffset);
				break;
			case PAGINATION_TYPE_PAGE_OFFSET:
				nextPageQueryParameterName = getNextPageQueryParameterName();
				if (StringUtil.isBlank(nextPageQueryParameterName))
					throw new ConnectorException("getNextPageQueryParameterName must be overridden to provide the name for example 'pageNum'...");
				long pageOffset=numDocuments/this.getPageSize(this.getContext().getOperationProperties());
				if (!this.isZeroBasedOffset())
					pageOffset++;
				nextPagePath = basePath+appendPath(uriParams, nextPageQueryParameterName+"="+pageOffset);
				break;
			case PAGINATION_TYPE_TOKEN_PARAMETER:
				if (StringUtil.isBlank(getNextPageElementPath()))
					throw new ConnectorException("Must implement getNextPageElementPath() for PAGINATION_TYPE_TOKEN_PARAMETER");
				String nextPageToken = respSplitter.getNextPageElementValue();
				nextPageQueryParameterName = getNextPageQueryParameterName();
				if (StringUtil.isBlank(nextPageQueryParameterName))
					nextPageQueryParameterName="offset";
				nextPagePath = basePath+appendPath(uriParams, nextPageQueryParameterName+"="+URLEncoder.encode(nextPageToken));
				break;
			default:
		}
   		logger.info(paginationType + ":" + nextPagePath);
   		return nextPagePath;
	}
	
	//Link: <https://${yourOktaDomain}/api/v1/users?after=00ubfjQEMYBLRUWIEDKK>; rel="next",
	//  <https://${yourOktaDomain}/api/v1/users?after=00ub4tTFYKXCCZJSGFKM>; rel="self"
	private String getNextLinkHeaderNextPageURL()
	{
		Header[] headers = httpResponse.getHeaders("Link");
		for (Header header : headers)
		{
			String link = header.getValue();
            if (link.contains("next")) {
            	link = link.substring(0, link.lastIndexOf(";"));
            	link = link.substring(1, link.length() - 1); //lose the <>
            	return link;
            }
		}
		return null;
	}
	
	/**
	 * @return the maximum number of documents set by the user in the query operation page
	*/
    public long getMaxDocuments(PropertyMap opProps)
    {
    	return opProps.getLongProperty(OperationProperties.MAXDOCUMENTS.name(), -1L);
    }
    
    
	/**
	 * @return the page size set by the user in the query operation page
	*/
    public long getPageSize(PropertyMap opProps)
    {
        return opProps.getLongProperty(OperationProperties.PAGESIZE.name(), 100L);
    }

	
	protected PropertyMap getOperationProperties()
	{
		return _opProps;
	}

    /**
     * Override to specify the name of the parameter to specify the query parameter to specify what fields to return in the query
     * Defaults to "fields"
     * @return the field selection query parameter name
     */
	protected String getFieldSelectionQueryParam()
	{
		return "fields";
	}
	
    //TODO ideally we would know total fields so we could exclude select if default is all. We could set a cookie for that
	//TODO this could be moved to abstract class if we key on value of PAGINATION_SELECT_URIPARAM not null? Comma delimited fields seems to be the standard when selection is implemented
    /**
     * Build the API URL query parameter to indicate what fields the user selected in the Fields list in the Query Operation UI
     * Defaults to "fields=x,y,z"
     * @param selectedFields the list of fields the user selected for the query operation
     * @return the field URI parameter for the selection
     */
    protected String getSelectTermsQueryParam(List<String> selectedFields)
    {
    	StringBuilder terms= new StringBuilder();
    	
    	if (selectedFields!=null && selectedFields.size()>0)
    	{
    		terms.append(getFieldSelectionQueryParam()+"=");
    		for (int x=0; x < selectedFields.size(); x++)
    		{
    			if (x!=0) terms.append(",");
    				terms.append(selectedFields.get(x));
    		}
    	}  	
    	return terms.toString();
    }
    	
	//This default behavior is to build terms as individual query parameters.
	//AND is assumed
    /**
     * Build the Filter path parameter from the filters in the Query Operation UI
     * Defaults to setting individual path parameters for each simple expression with each parameter a term in a top level AND expression
     * @param queryFilter the filter expression specified by the user in the Query Operation Filter UI
     * @return the field URI parameter for the filter
     * @throws IOException 
     */
	protected String getFilterTermsQueryParam(Expression baseExpr, int depth) throws IOException
	{
		String queryParameter="";

         // see if base expression is a single expression or a grouping expression
        if (baseExpr!=null)
        {
        	//A single operator, no AND/OR
            if(baseExpr instanceof SimpleExpression) {                
                // base expression is a single simple expression
            	queryParameter=(buildSimpleExpression((SimpleExpression)baseExpr));
            } else {

                // handle single level of grouped expressions
                GroupingExpression groupExpr = (GroupingExpression)baseExpr;

                // parse all the simple expressions in the group
                for(Expression nestedExpr : groupExpr.getNestedExpressions()) {
                    if(nestedExpr instanceof GroupingExpression) {
                        queryParameter+=getFilterTermsQueryParam(nestedExpr, depth+1);
                    } else {
                        String term=(buildSimpleExpression((SimpleExpression)nestedExpr));
                        if (term!=null && term.length()>0)
                        {
//TODO Some APIs just use a single query param per ANDed expression, others use an AND or an OR between expressions      
//TODO How to handle both?                        	
                            if (StringUtil.isBlank(this.getFilterQueryParameter()))
                            {
                                if (queryParameter.length()>0)
                                	queryParameter+="&";
                            } else {
                                if (queryParameter.length()>0)
                                	queryParameter+="%20" + groupExpr.getOperator().toString().toLowerCase() + "%20";
                            }
                            queryParameter+=term;
                        }
                    }
                }
            }
        }       
        if (depth>0)
        	queryParameter = "(" + queryParameter + ")";
//        if (queryParameter.length()>0)
//        	queryParameter=URLEncoder.encode(queryParameter);
        return queryParameter;//.replace("+", "%20");
	}
       
    /**
     * Override to build a filter expression for the unique grammar of the API.
     * For example create=2021-01-01&createdCompare=lessThan.
     * The default behavior is simple ANDed expressions using the eq operation &email=j@email.com&active=true
     * @param simpleExpression the simple expression from which to construct the uri parameter and value
     * @return the URL query parameter and value for the filter compare expression
     */
	
	//TODO should we be url encoding params? Amazon selling partner doesn't like it but we can just override
	protected String buildSimpleExpression(SimpleExpression expr) {
        // this is the name of the queried object's property
    	String term="";
        String propName = expr.getProperty();
        String operator = expr.getOperator();
        
        if (StringUtil.isBlank(propName))
        	throw new ConnectorException("Filter field parameter required");
        // we only support 1 argument operations
        if (CollectionUtil.size(expr.getArguments()) != 1) 
            throw new IllegalStateException("Unexpected number of arguments for operation " + expr.getOperator() + "; found " +
                                            CollectionUtil.size(expr.getArguments()) + ", expected 1");

        // this is the single operation argument
       	String parameter=expr.getArguments().get(0);
        if (parameter==null || parameter.length()==0)
        	throw new ConnectorException(String.format("Filter parameter is required for field: %s ", propName));

        switch (operator)
        {
	        case "eq":
	        	term = propName + "=" + URLEncoder.encode(parameter);
	        	break;
	        default:
	        	throw new ConnectorException(String.format("Unknown filter operator %s for field: %s", operator, propName));
        }

        return term;
    }
	
	/**
     * Override to build a sort expression for the unique grammar of the API.
     * For example orderby=lastName,firstName
     * @param queryFilter provides the user settings from the Query Operation page
     * @return the URL query parameter and value for the sort expression
     */
    protected abstract String getSortTermsQueryParam(List<Sort> sortTerms);
		
	protected String getFilterQueryParameter()
	{
		return null;
	}
	
	/**
	 * Specify the pagination type for the query response in order to fetch the next pages
	 * @return on of PAGINATION_TYPE_RECORD_OFFSET, PAGINATION_TYPE_PAGE_OFFSET, PAGINATION_TYPE_LAST_ID, PAGINATION_TYPE_LINK_HEADER, PAGINATION_TYPE_URL_FIELD, PAGINATION_TYPE_OTHER
	 */
	protected abstract PaginationType getPaginationType();
	
	/**
	 * Override to specify offset pagination path parameters
	 * For example 'offset' for PAGINATION_TYPE_RECORD_OFFSET, 'after' for PAGINATION_TYPE_LAST_ID.
	 * Not required for  PAGINATION_TYPE_LINK_HEADER, PAGINATION_TYPE_URL_FIELD
	 * @return
	 */
	protected String getNextPageQueryParameterName() {
		return "offset";
	}
	
    /**
     * Override to specify the name of the parameter that indicates the page size query parameter to specify the number of records per page
     * Defaults to "limit"
     * @return the page size query parameter name
     */
	protected String getPageSizeQueryParameterName()
	{
		return "limit";
	}
			
	/**
	 * Override of pagination indexing is 1 based vs. the default of zero based
	 * @return
	 */
	protected boolean isZeroBasedOffset()
	{
		return true;
	}

   /**
     * Override to provide the JSON path to the next page URL field. 
     * For example /links/nextPage. TODO support JSON path such as /links[rel='next']/href
     * If this is set to null, getNextPageQueryParam us used
     * @return the JSON path to the next page URL field in the JSON response
     */
	protected String getNextPageElementPath()
	{
		return null;
	}
	
   /**
     * Override to provide the JSON path to a boolean field that indicates there are more pages to query. 
     * For example /hasMore
     * @return the JSON path to the hasMore field in the JSON response
     */
	protected String getHasMoreElementPath()
	{
		return null;
	}
	
	protected String getIdElementPath()
	{
		return "/id";
	}

}