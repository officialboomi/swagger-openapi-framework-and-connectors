package com.boomi.swaggerframework.swaggeroperations;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.OAuth2Context;
import com.boomi.connector.api.OAuth2Token;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.PropertyMap;
import com.boomi.swaggerframework.swaggerutil.JSONToFormEncodedStream;
import com.boomi.swaggerframework.swaggerutil.SwaggerUtil;
import com.boomi.connector.util.BaseConnection;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

public abstract class SwaggerConnection extends BaseConnection {
    public enum ConnectionProperties {URL, SWAGGERURL, SWAGGERALTURL, AUTHTYPE, USERNAME, PASSWORD, OAUTHOPTIONS, AUTHFORSWAGGERLOAD, DEBUG_LOGGING, OAUTH_TOKEN}
	
	public enum AuthType {
		NONE,BASIC,OAUTH,BEARER_TOKEN,CUSTOM
	}  
	
	public enum OAuthGrantType {code, client_credentials, password, implicit};// TODO SDK has a hyphen in this standard value???, jwt-bearer};
	
    protected final String _baseUrl;
    private AuthType _authenticationType=AuthType.NONE;
    private final String _username;
    private final String _password;
    protected final PropertyMap _connProps;
    SwaggerOperationCookie _cookie;
	protected CloseableHttpClient _httpClient = null;
	
	private String _url;
	private String _httpMethod;
    
    protected Logger logger = Logger.getLogger(this.getClass().getName());
	protected HttpClientContext _httpClientContext;
	private OperationContext _operationContext;
    
	public SwaggerConnection(BrowseContext context) {
		super(context);
		
		_connProps = context.getConnectionProperties();
        _baseUrl = _connProps.getProperty(ConnectionProperties.URL.name());
        _authenticationType = AuthType.valueOf(_connProps.getProperty(ConnectionProperties.AUTHTYPE.name(), AuthType.NONE.name()));
    	logger.info("Init AuthType: " + _authenticationType.name() + " " +_connProps.getProperty(ConnectionProperties.AUTHTYPE.name()));
        _username = _connProps.getProperty(ConnectionProperties.USERNAME.name());
        _password = _connProps.getProperty(ConnectionProperties.PASSWORD.name());
	}
	
	/**
	 * This will support swagger urls to be in the descriptor connection or the operation. Operation is useful when there are multiple swaggers per connector.
	 * @param opProps
	 * @return
	 */
    public JSONObject getSwaggerFile(PropertyMap opProps) 
    {
        String swaggerUrl = "";
        
		if (StringUtil.isBlank(swaggerUrl))
    		swaggerUrl = opProps.getProperty(ConnectionProperties.SWAGGERALTURL.name());
		
		if (StringUtil.isBlank(swaggerUrl))
			swaggerUrl = opProps.getProperty(ConnectionProperties.SWAGGERURL.name());
		
		if (StringUtil.isBlank(swaggerUrl))
    		swaggerUrl = _connProps.getProperty(ConnectionProperties.SWAGGERALTURL.name());
		
		if (StringUtil.isBlank(swaggerUrl))
    		swaggerUrl = _connProps.getProperty(ConnectionProperties.SWAGGERURL.name());
		
		if (StringUtil.isBlank(swaggerUrl))
			swaggerUrl = this.getSpec();
//    	logger.info("getSwaggerFile " + swaggerUrl);
		if (StringUtil.isBlank(swaggerUrl))
			throw new ConnectorException("Swagger URL is a required field");
		JSONObject schema = null;
	   	if (swaggerUrl.startsWith("resources"))
	   	{
	   		InputStream is = this.getClass().getClassLoader().getResourceAsStream(swaggerUrl);
	   		if (is==null)
	   			throw new ConnectorException("Resource not found: " + swaggerUrl);
	   		schema = loadSchema(swaggerUrl, is);   		
	   	}
	   	else
	   	{
//TODO we can't stomp on _authenticationType 
//			boolean useAuth = _connProps.getBooleanProperty(ConnectionProperties.AUTHFORSWAGGERLOAD.name(), false);
//			if (!useAuth)
//				this._authenticationType = AuthType.NONE;
//TODO need to allow turning auth on/off per connector
	    	HttpURLConnection conn;
			try {
				conn = (HttpURLConnection) openConnection(new URL(swaggerUrl), AuthType.NONE);//this.getAuthenticationType());
		    	schema = loadSchema(swaggerUrl, conn.getInputStream()); 
		    	if (conn.getResponseCode()!=200)
					throw new ConnectorException("Can not load resource:" + swaggerUrl+ " " +conn.getResponseCode() + " " + conn.getResponseMessage());		    		
			} catch (JSONException | IOException | GeneralSecurityException e) {
				// TODO Auto-generated catch block
				throw new ConnectorException("Can not load resource:" + swaggerUrl + " " + e.getMessage());
			}
	   	}
	   	return schema;
    }
    
    private static String convertYamlToJson(InputStream is) {
        try {
            ObjectMapper yamlReader = new ObjectMapper(new YAMLFactory());
            Object obj = yamlReader.readValue(is, Object.class);
            
            ObjectMapper jsonWriter = new ObjectMapper();
            return jsonWriter.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    private JSONObject loadSchema(String url, InputStream is)
    {
    	JSONObject schema = null;
    	if (url.toLowerCase().endsWith(".yml") || url.toLowerCase().endsWith(".yaml") || url.toLowerCase().contains(".yaml"))
    	{
    		String schemaString = convertYamlToJson(is);
    		schema = new JSONObject(schemaString);
    	}
    	else {
    		schema =  new JSONObject(new JSONTokener(is));	
    	}
    	return schema;
    }

    public PropertyMap getOperationProperties()
    {
    	if (this.getOpContext()==null)
    		return null;
    	return this.getOpContext().getOperationProperties();
    }
	
    protected OperationContext getOpContext() {
		return this._operationContext;
	}

	//Bullhornbrowser,BullhornConnection, swagger operations
	public CloseableHttpResponse doExecute(String path, InputStream data, String httpMethod,
			SwaggerOperationCookie cookie, Map<String, String> headers, OperationContext operationContext) throws IOException, GeneralSecurityException {
    	setCookie(cookie);
    	_operationContext = operationContext;
		return this.doExecute(null, path, data, httpMethod, this.getAuthenticationType(), headers);
	}

	//BoomiDCPConnection
	//KefronConnection
	//BullhornConnection
	public CloseableHttpResponse doExecuteWithoutAuthentication(String url, InputStream data, String httpMethod) throws IOException, GeneralSecurityException {
		return this.doExecute(url, null, data, httpMethod, AuthType.NONE, null);
	}		

	public CloseableHttpResponse doExecute(String url, InputStream data, String httpMethod) throws IOException, GeneralSecurityException {
		return this.doExecute(url, null, data, httpMethod, this.getAuthenticationType(), null);
	}
	
	private CloseableHttpResponse doExecute(String url, String path, InputStream input, String method, AuthType authType, Map<String, String> headers)
			throws IOException, GeneralSecurityException {

		this._httpMethod=method;
		
		if (url==null)
			_url = this.getBaseUrl() + this.getBasePath() + path;
		
		Map<String,String> customHeaders = new HashMap<String,String>();
		_httpClient = HttpClients.custom().setConnectionManager(SwaggerConnector.GLOBAL_CONNECTION_MANAGER).build();
		//We want the headers to be able to override method....this is for aws (yuck)
		input = this.insertCustomHeaders(customHeaders, input);	

		CloseableHttpResponse response = null;
		if(_httpClientContext ==null )
			_httpClientContext = new HttpClientContext();
		
		try {							
	//		PoolStats poolStats = HTTPClientConnector.GLOBAL_CONNECTION_MANAGER.getTotalStats();
	//		logger.info(String.format("Reusing PoolingHttpClientConnectionManager Available: %d Leased: %d, Pending: %d Max: %d"
	//				,poolStats.getAvailable(), poolStats.getLeased(), poolStats.getPending(), poolStats.getMax()));
			
			HttpRequestBase httpRequest = null;
			//_httpClientContext to preserve cookies for custom login
			switch (this.getHttpMethod()) {
			case "DELETE":
				httpRequest = new HttpDelete();
				break;
			case "GET":
				httpRequest = new HttpGet();
				break;
			case "POST":
				httpRequest = new HttpPost();
				break;
			case "PATCH":
				httpRequest = new HttpPatch();
				break;
			case "PUT":
				httpRequest = new HttpPut();
				break;
			}
			// add request headers
			String accept = this.getResponseContentType();
			if (accept!=null)
				httpRequest.addHeader("Accept", accept);
//			httpRequest.setProtocolVersion(HttpVersion.HTTP_1_0);
			String authHeader = this.getAuthHeader(authType);
			boolean contentTypeHeader=false;
			for (Map.Entry<String, String> entry : customHeaders.entrySet())
			{
				if ("Content-Type".contentEquals(entry.getKey()))
					contentTypeHeader=true;
				httpRequest.addHeader(entry.getKey(), entry.getValue());
			}
			
			if (url==null)
				url = this.getUrl();

			logger.info("url:" + url);

			httpRequest.setURI(new URI(url));
			if (authHeader != null)
				httpRequest.addHeader("Authorization", authHeader);
	
			if (headers != null)
			{
				for (Map.Entry<String, String> entry : headers.entrySet()) {
					httpRequest.addHeader(entry.getKey(), entry.getValue());
				}
			}
			HttpParams params = new BasicHttpParams();
		    params.setParameter(ClientPNames.HANDLE_REDIRECTS, false);
		    HttpClientParams.setRedirecting(params, false); // alternative

		    httpRequest.setParams(params);
		    
			if ("GET".contentEquals(_httpMethod))
			{
//				httpRequest.setHeader("Content-Type", getRequestContentType());
			} else if (!"DELETE".contentEquals(_httpMethod)) {
				if (input==null)
					throw new ConnectorException("Input required for operation: " + _httpMethod);
		    	if (getRequestContentType().indexOf("form-urlencoded")>-1)
		    	{
		    		input = JSONToFormEncodedStream.jsonToUrlFormEncodedStream(input);
		    	}
		    	if (this.getConnectionProperties().getBooleanProperty("DEBUG_LOGGING", false))
		    	{
		    		logger.info("PAYLOAD LOGGING FOR DEBUG");
		    		String payloadString = SwaggerUtil.inputStreamToString(input);
		    		logger.info(payloadString);
		    		input = new ByteArrayInputStream(payloadString.getBytes());
		    		
		    	}
		    	AbstractHttpEntity reqEntity;
		    	if (!useChunkedEntity())
		    	{
		    		//TODO Paylocity doesn't support chunking so we have to send bytes of known length
					reqEntity = new ByteArrayEntity(SwaggerUtil.inputStreamToString(input).getBytes());
		    	} else {
		    		reqEntity = new InputStreamEntity(input);
		    	}
				
		    	if (!contentTypeHeader)
		    		reqEntity.setContentType(getRequestContentType());
				

				switch (_httpMethod) {
				case "POST":
					((HttpPost) httpRequest).setEntity(reqEntity);
					break;
				case "PATCH":
					((HttpPatch) httpRequest).setEntity(reqEntity);
					break;
				case "PUT":
					((HttpPut) httpRequest).setEntity(reqEntity);
					break;
				}
			}
		
	   		logger.log(Level.INFO, String.format("Executing %s : %s", _httpMethod, url));
	   		logger.info("Request Headers: \n");
	   		for (Header header : httpRequest.getAllHeaders())
	   		{
	   			logger.info(header.getName());
	   		}
			response = _httpClient.execute(httpRequest, this._httpClientContext);
			logger.info("Status Code: " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
		} catch (URISyntaxException e) {
			throw new ConnectorException(e);
		} finally {
			if (input!=null)
				IOUtil.closeQuietly(input);
//			if (httpClient!=null)
//				httpClient.close();
		}
		return response;
	}
    
	private String getAuthHeader(AuthType authenticationType) throws IOException, JSONException, GeneralSecurityException
    {
    	String authHeader = null;
       	if (authenticationType != null)
    	{
           	if (AuthType.BASIC == authenticationType)
        	{
            	String userpass = getUsername() + ":" + getPassword();
            	authHeader = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));
        	} else if (AuthType.BEARER_TOKEN==authenticationType) {
        		authHeader = "Bearer " + getPassword();
        	} else if (AuthType.OAUTH==authenticationType) {
        		String accessToken = _connProps.getProperty(ConnectionProperties.OAUTH_TOKEN.name());
        		if (StringUtil.isBlank(accessToken))
        		{
            		OAuth2Context oAuth2Context = _connProps.getOAuth2Context(ConnectionProperties.OAUTHOPTIONS.name());
            		if (oAuth2Context == null)
            			throw new ConnectorException("OAuth 2 Context is null. Please configure OAuth for the connection.");
            		//TODO we true will force a refresh of the token...or we can do a 1 time 401 retry that forces an update isUseRefreshToken()==true
            		boolean forceTokenRefresh = false;
//            		if (oAuth2Context.isUseRefreshToken())
//            		{
//            			long expiration = oAuth2Context.getJwtAssertionParameters().getExpiration();
//            			long oAuthRefreshTimeLimit = 1000*60*60*24*2; //2 days TODO allow allow to be configured from override
//            			if (expiration-(new Date()).getTime()<oAuthRefreshTimeLimit)
//            				forceTokenRefresh=true;
//            		}
            		OAuth2Token oAuth2Token = oAuth2Context.getOAuth2Token(forceTokenRefresh); 
            		accessToken = oAuth2Token.getAccessToken();
        		}
        		authHeader = "Bearer " + accessToken;
           	} else if (AuthType.CUSTOM==authenticationType)
           	{
           		return getCustomAuthCredentials();
           	}
    	}
//       	logger.info("DEBUG Auth Header: " + authHeader);
       	return authHeader;
	}
    
    public HttpURLConnection openConnection(URL url, AuthType authenticationType) throws IOException, JSONException, GeneralSecurityException
    {
       HttpURLConnection connection = (HttpURLConnection) url.openConnection();
       String authHeader = this.getAuthHeader(authenticationType);
       if (authHeader!=null)
    	   connection.setRequestProperty ("Authorization", authHeader);	
       connection.setRequestProperty("Accept", "application/json");
       return connection;
    }
    
    public void testConnectionInternal() throws Exception
    {
    	this.testConnection();
    }
	/**
	 * Override to implement a Test Connection button on the Connections page. Default behavior is to open an authenticated connection to the base host URL
	*/
    protected void testConnection() throws Exception
    {
    	CloseableHttpClient httpClient = HttpClients.createDefault();
    	CloseableHttpResponse httpResponse = null;
    	try {
            HttpGet httpRequest = new HttpGet(this.getTestConnectionUrl());
            String authHeader = this.getAuthHeader(this.getAuthenticationType());
            if (authHeader!=null)
            	httpRequest.addHeader ("Authorization", authHeader);	
        	logger.info("Test Connection: " +getTestConnectionUrl()+ " " + this.getAuthenticationType().name());
        	httpResponse = httpClient.execute(httpRequest);
        	if (200 != httpResponse.getStatusLine().getStatusCode())
        		throw new Exception(String.format("Problem connecting to endpoint: %s %s %s", this.getTestConnectionUrl(), httpResponse.getStatusLine().getStatusCode(), httpResponse.getStatusLine().getReasonPhrase()));   			
    	} finally {
			if (httpResponse!=null)
				httpResponse.close();
			if (httpClient!=null)
				httpClient.close();
    	}
    }
           
	/**
	 * @return the username set by the user in the Connections Page
	*/
    protected String getUsername()
    {
    	if (StringUtil.isBlank(_username))
    		throw new ConnectorException("Username is required");
    	return _username;
    }
    
	/**
	 * @return the password set by the user in the Connections Page
	*/
    protected String getPassword()
    {
    	if (StringUtil.isBlank(_password))
    		throw new ConnectorException("Password is required");
    	return _password;
    }
    
    /**
     * Override to provide the JSON path to the paginated items to split as individual documents
     * For example /items/* /result/items/* /results/* ...
     * If override returns null, the path will be derived by finding the first array in the response profile
     * @return the JSON path to the paginated items in the JSON response
     */
    public abstract String getQueryPaginationSplitPath();
    
	/**
	 * For example, there may be APIs for authentication in the swagger file that should be hidden from the user 
	 * @return the list of ids to exclude in the format <path>___<http method>
	*/
    public List<String> getExcludedObjectTypeIDs()
    {
    	return null;
    }
    
	/**
	 * Returns the AuthType enumeration to set the default authentication type. Defaults to AuthType.NONE
	 * @return AuthType value NONE | BASIC | OAUTH20 | CUSTOM
	*/
    protected AuthType getAuthenticationType()
    {
    	logger.info("AuthType: " + _authenticationType.name());
    	return this._authenticationType;
    }
    
	/**
	 * Returns the service url/host set by the user in the Connections page. 
	 * Overridden by Bullhorn to resolve the url with session ID during the authentication steps
	 * @return the url for the service
	*/
    protected String getBaseUrl()
    {
		if (_baseUrl == null || _baseUrl.trim().length()==0)
			throw new ConnectorException("Service URL is a required field");
    	return this._baseUrl.trim();
    }
    
	/**
	 * Override to set a value for the openapi/swagger file. Useful when files are embedded in the jar
	 * @return the url for the swagger or open API files
	*/
    protected String getSpec()
    {
    	return _connProps.getProperty(ConnectionProperties.SWAGGERURL.name());
    }
    
	/**
	 * Used by operations to set the logger to the Process Log logger
	 * @param the Logger
	*/
    public void setLogger(Logger logger)
    {
    	this.logger = logger;
    }
    
	/**
	 * Returns the Java logger so methods can can log to the container logs during Import, or the Process Log during process execution.
	 * @return the Logger
	*/
    protected Logger getLogger()
    {
    	return this.logger;
    }

	/**
	 * Returns the Connection Properties set by the user in the connectors Connection page.
	 * @return the connection PropertyMap
	*/
    protected PropertyMap getConnectionProperties()
    {
    	return this._connProps;
    }
    
    //TODO this should come from swagger but fast hack to support stripe request form-encoded
    protected String getRequestContentType()
    {
    	if (this._cookie!=null)
    		return this._cookie.getRequestMimeType();
    	return "application/json";
    }
    
    //TODO this should come from swagger but fast hack to support stripe request form-encoded
    protected String getResponseContentType()
    {
    	if (this._cookie!=null)
    		return this._cookie.getResponseMimeType();
    	return "application/json";
    }
    
    protected String getTestConnectionUrl()
    {
    	return this.getBaseUrl();
    }
    
	/**
	 * Allows the addition of custom headers to the connection. For example for Visa Cybersource, this is used to implement HTTP Signature Authentication
	 * @param httpRequest the connection for which you can add headers via the addRequestProperty method
	 * @param data the InputStream for the request body 
	 * @return the header value ie. Bearer xxxxxxxxxxxxxxxxxxxxxxxxxx
	*/
	protected InputStream insertCustomHeaders(Map<String,String> headers, InputStream data)
	{
		return data;
	}
	/**
	 * Provide a custom Authorization header for AuthType.CUSTOM implementations.
	 * Useful for proprietary API driven authentication. 
	 * @return the header value ie. Bearer xxxxxxxxxxxxxxxxxxxxxxxxxx
	*/
    protected String getCustomAuthCredentials() throws JSONException, IOException, GeneralSecurityException
    {
    	return null;
    }    
    
    //Overridden by Workday to add the tenant ID to the path
    protected String getBasePath()
    {
    	if (this._cookie!=null)
    		return this._cookie.getBasePath();
    	return "";
    }

	protected SwaggerOperationCookie getCookie() {
		return _cookie;
	}

	private void setCookie(SwaggerOperationCookie _cookie) {
		this._cookie = _cookie;
	}

	//Used for add custom headers whereas payloads need to be signed. AWSCloudFormation, Cybersource...
	public String getUrl() {
		return _url;
	}

	//TODO FUGLY Used by AWSCloudFormation.addCustomHeaders because it actually munges the URL
	public void setUrl(String _url) {
		this._url = _url;
	}

	public String getHttpMethod() {
		return _httpMethod;
	}

	public void setHttpMethod(String _method) {
		this._httpMethod = _method;
	}
	
	protected boolean useChunkedEntity()
	{
		return true;
	}
}