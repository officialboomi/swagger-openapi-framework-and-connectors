package com.boomi.swaggerframework.templates;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class SwaggerConnectorGenerator {

	static String CLASSNAME = "Augury";
	static String PACKAGENAME = "augury";
	static String PRODUCTNAME = "Augury REST API";
	static String PRODUCTDESCRIPTION = "REST interface to Augury";
	
	static String resourceDir = "src/main/java/resources/";
	static String templateDir = "src/main/java/com/boomi/swaggerframework/templates/";
	static String targetSourceCodeDir = "src/main/java/com/boomi/connector/"+PACKAGENAME+"/";
	static String targetTestSourceCodeDir = "src/test/java/com/boomi/connector/";
	public static void main(String[] args) throws IOException {
		String content=readFileAndReplace(templateDir+"descriptor-PACKAGENAME.xml");
		writeToFile(resourceDir+PACKAGENAME,"/descriptor-"+PACKAGENAME+".xml", content);
		
		content=readFileAndReplace(templateDir+"connector-config-PACKAGENAME.xml");
		writeToFile(resourceDir+PACKAGENAME,"/connector-config-"+PACKAGENAME+".xml", content);
		
		content=readFileAndReplace(templateDir+"CLASSNAMEQueryOperation.txt");
		writeToFile(targetSourceCodeDir,CLASSNAME+"QueryOperation.java", content);
		
		content=readFileAndReplace(templateDir+"CLASSNAMEBrowser.txt");
		writeToFile(targetSourceCodeDir,CLASSNAME+"Browser.java", content);
		
		content=readFileAndReplace(templateDir+"CLASSNAMEConnection.txt");
		writeToFile(targetSourceCodeDir,CLASSNAME+"Connection.java", content);
		
		content=readFileAndReplace(templateDir+"CLASSNAMEConnector.txt");
		writeToFile(targetSourceCodeDir,CLASSNAME+"Connector.java", content);		
		
		content=readFileAndReplace(templateDir+"CLASSNAMEBrowseTest.txt");
		writeToFile(targetTestSourceCodeDir,CLASSNAME+"BrowseTest.java", content);		
	}
	
	static void writeToFile(String dirName, String fileName, String content) throws IOException
	{
        File dir = new File(dirName);
        if (!dir.exists())
        	dir.mkdirs();

	    BufferedWriter writer = new BufferedWriter(new FileWriter(dirName+fileName, false));
	    writer.append(content);
	    writer.close();
	}
	
	static String readFileAndReplace(String fileName) throws FileNotFoundException
	{
		 String file = fileName;
		 Scanner scanner = new Scanner(new File(file));
		 scanner.useDelimiter("\\Z");
		 String response =scanner.next();
		 scanner.close();
		 response=response.replaceAll("!!PACKAGENAME!!", PACKAGENAME);
		 response=response.replaceAll("!!CLASSNAME!!", CLASSNAME);
		 response=response.replaceAll("!!PRODUCTNAME!!", PRODUCTNAME);
		 response=response.replaceAll("!!PRODUCTDESCRIPTION!!", PRODUCTDESCRIPTION);
		 return response;
	}
}
