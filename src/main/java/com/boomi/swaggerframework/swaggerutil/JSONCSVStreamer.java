package com.boomi.swaggerframework.swaggerutil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.boomi.connector.api.ConnectorException;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class JSONCSVStreamer {
	JsonFactory JSON_FACTORY = new JsonFactory();
	List<String> csvHeader = new ArrayList<String>();
	StringBuilder row = new StringBuilder();
	JsonGenerator _jsonGenerator = null;
	boolean isFirstDocument = true;
	StringBuilder header = new StringBuilder();
	JSONCSVStreamer()
	{
		
	}
	
	//getContext().createTempOutputStream()
	//getContext().tempOutputStreamToInputStream(tempOutputStream)
	
	//TODO should we allow combining documents with a single header at top?
	//TODO or should we allow an array?
	//need to write to the outputstream
	public void jsonToCSVStream(InputStream input, OutputStream output)
	{	
		JsonParser parser = null;
		try {
			parser = JSON_FACTORY.createParser(input);
			JsonToken element = parser.getCurrentToken();
			if (element != JsonToken.START_OBJECT)
				throw new ConnectorException("JSON start object expected but got \" + element.name()");
			while (parser.nextToken() != null) {
				element = parser.getCurrentToken();
				if (element == JsonToken.FIELD_NAME) {
					String fieldName = parser.getCurrentName();
					if (isFirstDocument)
					{
						if (header.length()>0)
							header.append(",");
						header.append(fieldName);
					}
					element = parser.getCurrentToken();
					if (!element.name().startsWith("VALUE_"))
						throw new ConnectorException("JSON value expected but got " + element.name());	
					if (row.length()>0)
						row.append(",");
					//TODO surround text in double quotes? 
					//element.isBoolean() && element.i
					row.append(parser.getValueAsString());
				} else {
					throw new ConnectorException("JSON element expected but got " + element.name());
				}
				element = parser.getCurrentToken(); //skip start object
			}
			header.append("\r\n");
			row.append("\r\n");
			output.write(header.toString().getBytes(StandardCharsets.UTF_8.name()));
			output.write(row.toString().getBytes(StandardCharsets.UTF_8.name()));
			row.setLength(0); //reuse it to avoid GC
		} catch (IOException e) {
			throw new ConnectorException(e);
		}  finally {
			
		}
	}
}
