package com.boomi.swaggerframework.swaggerutil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.DynamicPropertyMap;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ui.AllowedValue;
import com.boomi.connector.api.ui.BrowseField;
import com.boomi.connector.api.ui.DataType;
import com.boomi.util.StringUtil;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class SwaggerBrowseUtil {
	public static final String DEPRECATED_PATH_METHOD_OBJECTID_DELIMITER = "___";
	public static final String OBJECTID_XOPERATION_DELIMITER = "!!!"; //for oracle x operations
	public static final String PATH_PARAMETER_FIELD_ID_PREFIX = "path_";
	public static final String DEPRECATED_PATH_PARAMETER_FIELD_ID_PREFIX = "PATHPARAM_";
	public static final String QUERY_PARAMETER_FIELD_ID_PREFIX = "query_";
	public static final String HEADER_PARAMETER_FIELD_ID_PREFIX = "header_";
	
	public static String getObjectTypeLabel(JSONObject method)
	{
		String objectLabel="";
		if (method.has("operationId"))
		{
			objectLabel=method.getString("operationId");
			//Pretty it up
			objectLabel = objectLabel.replaceAll("_", " ");
			objectLabel = objectLabel.replaceAll("  ", " ").trim();
			if (objectLabel.length()>0)
			{
				//Initial Caps for each word
				StringBuilder sb = new StringBuilder(objectLabel);
				int wordStart = 0;
				while(wordStart>-1)
				{
					char firstChar = (sb.charAt(wordStart)+"").toUpperCase().charAt(0);
					sb.setCharAt(wordStart, firstChar);
					wordStart = sb.indexOf(" ", wordStart+1);
					if (wordStart>-1) wordStart++;
				}
				objectLabel = sb.toString();
			}
		}
		else if (!method.isNull("tags") && method.getJSONArray("tags").length()>0)
		{
			objectLabel=method.getJSONArray("tags").getString(0);
		} 
		return objectLabel;
	}
	
	public static String getObjectTypeHelpText(JSONObject method)
	{
		String helpText="";
		
		if (!method.isNull("tags"))
		{
			for (int i=0; i< method.getJSONArray("tags").length(); i++)
			{
				helpText+=method.getJSONArray("tags").getString(i) + " ";
			}
		} 

		if (method.has("description"))
		{
			helpText+=method.getString("description") + " ";
		}
		
		if (method.has("summary"))
		{
			helpText+=method.getString("summary") + " ";
		}

		
		helpText = helpText.replace("\r", "");
		helpText = helpText.replace("\n", " ");
		int maxLength=2000;
		if (helpText.length()>maxLength)
			helpText = helpText.substring(0, maxLength) + "...";
		return helpText;
	}
	
	public static boolean isQuery(String methodString, String pathString)
	{
		boolean result=false;
		if (methodString.toLowerCase().contentEquals("get"))
				if(!pathString.endsWith("}"))
			result=true;
		return result;
	}
	
	//Has can have more than 1 Url path parameters so single ID cannot be passed to a boomi standard GET operation and input Profile is required
	public static boolean isGet(String methodString, String pathString)
	{
		boolean result=false;
		if ((pathString.endsWith("}") || pathString.endsWith("}/")) && methodString.toLowerCase().contentEquals("get"))
			result=true;
		return result;
	}
		
	public static boolean isCreate(String methodString, String pathString)
	{
		boolean result=false;
		if (!pathString.endsWith("}") && !pathString.endsWith("}/") && methodString.toLowerCase().contentEquals("post"))
			result=true;
		return result;
	}
		
	//TODO petstore.swagger.io has an update where the in body is form encoded. We should filter all but application/json?
	//"consumes": ["multipart/form-data",
	public static boolean isUpdate(String methodString, String pathString)
	{
		boolean isUpdate=false;
		if (methodString.toLowerCase().contentEquals("patch"))
			isUpdate=true;
		else if ((pathString.endsWith("}") || pathString.endsWith("}/")) && methodString.toLowerCase().contentEquals("post"))
			isUpdate=true;
		else if (methodString.toLowerCase().contentEquals("put"))
			isUpdate=true;
		return isUpdate;
	}
		
	//Has more than 1 Url path parameters so single ID cannot be passed to a boomi standard GET operation and input Profile is required
	public static boolean isDelete(String methodString, String pathString)
	{
		boolean result=false;
		if (methodString.toLowerCase().contentEquals("delete"))
			result=true;
		return result;
	}
	
	public static void sortObjectTypes(ObjectTypes objectTypes)
	{
		List<ObjectType> objectTypeList = objectTypes.getTypes();
		//Now sort them
		if (objectTypes!=null)
		{
		    Collections.sort(objectTypeList, new Comparator<ObjectType>() {
		        @Override
		        public int compare(ObjectType a, ObjectType b) {
		            return a.getLabel().compareToIgnoreCase(b.getLabel());
		        }
		    });
		}
	}
		
	public static int getNumberOfPathParameters(String path)
	{
		return path.split("\\{").length-1;
	}

	public static String getURLPathWithParameterValues (String objectTypeId, PropertyMap opProps, DynamicPropertyMap dynamicProperties) 
	{
		return getURLPathWithParameterValues(objectTypeId, opProps, dynamicProperties, null);
	}
	/**
	 * Iterates the path parameters in the path and gets the values from opProps overridable fields
	 * @param objectTypeId
	 * @param opProps
	 * @param path2 
	 * @return
	 * @throws Exception
	 */
	public static String getURLPathWithParameterValues (String objectTypeId, PropertyMap opProps, DynamicPropertyMap dynamicProperties, String path)
	{
		if (StringUtil.isBlank(path))
			path = objectTypeId;
		String parameters[] = path.split("\\{");
		
		for (int i=1; i<parameters.length; i++)
		{
			String parameterName = parameters[i];
			int end = parameterName.indexOf("}");
			if (end>0)
			{
				parameterName = parameterName.substring(0, end);
				String parameterValue = SwaggerBrowseUtil.getPropertyValue(parameterName, opProps, dynamicProperties, PATH_PARAMETER_FIELD_ID_PREFIX);
				if (StringUtil.isBlank(parameterValue))
					parameterValue = SwaggerBrowseUtil.getPropertyValue(parameterName, opProps, dynamicProperties, DEPRECATED_PATH_PARAMETER_FIELD_ID_PREFIX);
				if (StringUtil.isNotBlank(parameterValue))
					path=path.replace("{"+parameterName+"}", parameterValue);			
			}
		}
		
		if (path.contains("{"))
			throw new ConnectorException("Path placeholder replacements not complete: "+ path + ". Please ensure all path parameter Dynamic Properties are set");
		return path;
	}
	
	public static String getPropertyValue(String parameterName, PropertyMap opProps, DynamicPropertyMap dynamicProperties, String paramPrefix)
	{
		String parameterValue=null;
		String parameterId = paramPrefix+parameterName;
		if (dynamicProperties!=null && StringUtil.isBlank(parameterValue)) {
			parameterValue = dynamicProperties.getProperty(parameterId);
		}
		if (StringUtil.isBlank(parameterValue))
		{
			Object obj = opProps.get(parameterId);
			//DYNAMIC property takes priority
			if (obj!=null)
			{
				parameterValue = obj.toString();
			}
		}
		return parameterValue;
	}
	
	public static void createPathParameterFields(ObjectDefinitions objDefinitions, JSONArray pathParams)
	{
		for(int i=0; pathParams!=null && i<pathParams.length(); i++)
		{
			JSONObject pathParameter = (JSONObject) pathParams.get(i);
			//We always set the path params as strings is that OK? 
			BrowseField browseField = createPathParameterField(pathParameter);
			objDefinitions.getOperationFields().add(browseField);
		}						
	}

	public static BrowseField createPathParameterField(JSONObject parameter) {
		return createParameterField(parameter, PATH_PARAMETER_FIELD_ID_PREFIX, "Path", "RESTful path parameter value for ");
	}
	public static BrowseField createQueryParameterField(JSONObject parameter) {
		return createParameterField(parameter, QUERY_PARAMETER_FIELD_ID_PREFIX, "Query", "Query parameter value for ");
	}
	public static BrowseField createHeaderParameterField(JSONObject parameter) {
		return createParameterField(parameter, HEADER_PARAMETER_FIELD_ID_PREFIX, "Header", "Header parameter value for ");
	}
	
	/**
	* Creates a simple field input for the Operation.
	* @return BrowseField
	*/
	private static BrowseField createParameterField(JSONObject parameter, String prefix, String label, String defaultDescription) {
	   BrowseField simpleField = new BrowseField();
	   String parameterName = parameter.getString("name");
	   String parameterID = prefix+parameterName;
	   // Mandatory to set an ID
	   simpleField.setId(parameterID);
	   // User Friendly Label, defaults to ID if not given.
	   simpleField.setLabel(label + " - " + parameterName);
	   simpleField.setOverrideable(true);
	   // Mandatory to set a DataType. This Data Type can also be String, Boolean, Integer, Password
	   JSONObject typeDefinition = parameter;
	   if (parameter.has("schema"))
		   typeDefinition = parameter.getJSONObject("schema");
	   if (!typeDefinition.has("type"))
		   return null;
	   switch(typeDefinition.getString("type"))
	   {
	   case "integer":
		   simpleField.setType(DataType.INTEGER);
		   break;
	   case "boolean":
		   simpleField.setType(DataType.BOOLEAN);
		   break;
	   default:
		   simpleField.setType(DataType.STRING);
	   }
	   
	   if (typeDefinition.has("enum"))
	   {
		   int i=-1;
		   //if not required, we add a blank to the allowed list
		   if (parameter.has("required") && parameter.getBoolean("required"))
			   i=0;
		   JSONArray enums = typeDefinition.getJSONArray("enum");
		   for (;i<enums.length(); i++)
		   {
			   AllowedValue av = new AllowedValue();
			   String value="";
			   if (i>=0)
				   value = enums.getString(i);
			   av.setLabel(value);
			   av.setValue(value);
			   simpleField.getAllowedValues().add(av);
		   }
	   }
	   
	   // Optional Help Text for the String Field
	   String description = defaultDescription+parameterName;
	   //TODO XML ENCODE or does SDK handle that?
	   if (parameter.has("description"))
		   if (StringUtil.isNotBlank(parameter.getString("description")))
			   description = parameter.getString("description");
	   simpleField.setHelpText(description);
	   // Optional Default Value for String Field
//	   simpleField.setDefaultValue(servicePath);
	   return simpleField;
	}

	
	//Parse to get the value at the specific path and return the value
	public static String getLastID (String targetElementPath, InputStream is, OutputStream os) throws Exception
	{				
		String targetValue=null;
		StringBuilder pathPointer = new StringBuilder();
		JsonFactory JSON_FACTORY = new JsonFactory();
		// open the parser and generator as managed resources that are guaranteed to be closed
		try (JsonParser parser = JSON_FACTORY.createParser(is);
             JsonGenerator generator = JSON_FACTORY.createGenerator(os)) {
			//Prevent Embedded message: already closed
			generator.disable(JsonGenerator.Feature.AUTO_CLOSE_TARGET);
			while (parser.nextToken() != null) {
				JsonToken element = parser.getCurrentToken();
				if (element == JsonToken.END_ARRAY) {
					popElement(pathPointer);
					generator.writeEndArray();
				}
				if (element == JsonToken.START_OBJECT)
					generator.writeStartObject();

				if (element == JsonToken.FIELD_NAME) {
					String currentName = parser.getCurrentName();
					pushElement(pathPointer, currentName);
					//If we are inside the ___pathParameters element we will exclude it from the output so it will not be sent to the api server
					// grab the value token
					element = parser.nextToken();
					// write the current field name
					generator.writeFieldName(currentName);

					if (element == JsonToken.START_OBJECT)
						generator.writeStartObject();
					else if (element == JsonToken.START_ARRAY)
						generator.writeStartArray();
					else if (element.name().startsWith("VALUE_")) 
					{
			    		if ((element==JsonToken.VALUE_STRING || element==JsonToken.VALUE_NUMBER_INT)  
			        			&& pathPointer.toString().contentEquals(targetElementPath))
			        			targetValue = parser.getValueAsString();
						writeJsonTokenToGenerator(element, generator, parser);
						popElement(pathPointer);
					}
				}
				
				if (element == JsonToken.END_OBJECT && !parser.getParsingContext().inRoot())
				{
					generator.writeEndObject();
					popElement(pathPointer);
				}
			}
			// flush the generator
			generator.flush();
		}
		return targetValue;
	}
	
	public static List<String> getPathParams(String objectTypeId)
	{
		List<String> pathParams = new ArrayList<String>();
		String path = objectTypeId;
		String paramStarts[] = path.split("\\{");
		for (int i=1; i<paramStarts.length; i++)
		{
			String param = paramStarts[i].substring(0,paramStarts[i].indexOf("}"));
			pathParams.add(param);
		}
		return pathParams;
	}
	
	//Used to track location in the streaming json parsing
	private static void pushElement(StringBuilder pathPointer, String name) {
		pathPointer.append("/");
		pathPointer.append(name);
//		System.out.println("PUSHED " + pathPointer);
	}

	private static void popElement(StringBuilder pathPointer) {
		int lastPos = pathPointer.lastIndexOf("/");
		if (lastPos > -1)
			pathPointer.setLength(lastPos);
//		System.out.println("POPPED " + pathPointer);
	}

	private static void writeJsonTokenToGenerator(JsonToken element, JsonGenerator generator, JsonParser parser) throws IOException
	{
		switch (element)
		{
		case VALUE_STRING:
			generator.writeString(parser.getValueAsString());
			break;
		case VALUE_TRUE:
			generator.writeBoolean(true);
			break;
		case VALUE_FALSE:
			generator.writeBoolean(false);
			break;
		case VALUE_NUMBER_INT:
			generator.writeNumber(parser.getValueAsLong());
			break;
		case VALUE_NUMBER_FLOAT:
			generator.writeNumber(parser.getValueAsDouble());
			break;
		case VALUE_NULL:
			generator.writeNull();
			break;
		default:
			throw new ConnectorException("Unhandled JSON Value Type: " + element.name());								
		}
	}	
}

