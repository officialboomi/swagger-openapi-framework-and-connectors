package com.boomi.swaggerframework.swaggerutil;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.ConnectorException;

public class JSONToFormEncodedStream {
	//TODO use JSON Streamer
	public static InputStream jsonToUrlFormEncodedStream(InputStream jis)
	{
		return new ByteArrayInputStream(jsonToUrlFormEncoded(new JSONObject(new JSONTokener(jis))).getBytes());
	}
	
	public static String jsonToUrlFormEncoded(JSONObject in)
	{
		StringBuilder sb = new StringBuilder();
		
		jsonToUrlFormEncodedRecursive(sb, in, "");
		return sb.toString();
	}
	
	private static void jsonToUrlFormEncodedRecursive(StringBuilder sb, JSONObject in, String parentKey)
	{
		Iterator<String> keys = in.keys();
		while (keys.hasNext())
		{
			String key = keys.next();
			Object obj = in.get(key);
			if (obj instanceof JSONObject)
			{
				String newParentKey="";
				newParentKey = key;
//				if (parentKey.length()>0)
//				{
//					key="["+key+"]";
//				}
//				newParentKey+="["+key+"]";
				jsonToUrlFormEncodedRecursive(sb, (JSONObject) obj, newParentKey);
			} else if (obj instanceof JSONArray) {
				throw new ConnectorException("Form URL Encoding can not support json arrays");
			} else {
				if (sb.length()>0)
					sb.append("&");
				if (parentKey.length()>0)
				{
					key=parentKey+"["+key+"]";
				}
				sb.append(key+"="+obj.toString());
			}
		}
	}

}
