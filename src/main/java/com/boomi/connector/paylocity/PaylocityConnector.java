package com.boomi.connector.paylocity;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class PaylocityConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new PaylocityBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new PaylocityQueryOperation(createConnection(context));
    }
    protected PaylocityConnection createConnection(BrowseContext context) {
        return new PaylocityConnection(context);
    }
}