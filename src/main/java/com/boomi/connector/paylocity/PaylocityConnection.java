package com.boomi.connector.paylocity;

import java.io.InputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.AuthType;


public class PaylocityConnection extends SwaggerConnection {
    
	public PaylocityConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    public AuthType getAuthenticationType()
    {
    	return AuthType.OAUTH;
    }
    
	protected boolean useChunkedEntity()
	{
		return false;
	}
    
//    @Override
//    public List<String> getExcludedObjectTypeIDs()
//    {
//    	return Arrays.asList("/pet/{petId}", "/user/login", "/user/logout");
//    }
}