package com.boomi.connector.guidewire;

import java.util.List;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;

public class GuidewireQueryOperation extends SwaggerQueryOperation {
	

	protected GuidewireQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected String getSelectTermsQueryParam(List<String> selectedFields) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
    protected String getSortTermsQueryParam(List<Sort> sortTerms)
    {
    	String sortTermsString="";
    	if (sortTerms!=null)
    	{		            
        	for (int x=0; x<sortTerms.size(); x++)
        	{
        		Sort sort = (Sort)sortTerms.get(x);
        		String sortTerm = sort.getProperty();
        		//TODO Deep Sorts Supported?
    			sortTerm=sortTerm.replace('/', '.');
        		if (sortTerm != null && sortTerm.length()>0)
        		{
        			if (sortTermsString.length()==0)
        				sortTermsString+="sort"+"=";
        			else
        				sortTermsString+=",";
        			if ("desc".contentEquals(sort.getSortOrder()))
        				sortTermsString+="-";
        			sortTermsString += sortTerm;
        		}
        	}
    	}
    	return sortTermsString;
    }

	@Override
	protected PaginationType getPaginationType() {
		// TODO Auto-generated method stub
		return PaginationType.PAGINATION_TYPE_PAGE_OFFSET;
	}
}