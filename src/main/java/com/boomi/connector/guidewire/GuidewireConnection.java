package com.boomi.connector.guidewire;

import java.util.logging.Logger;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class GuidewireConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public GuidewireConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    public AuthType getAuthenticationType()
    {
    	return AuthType.BASIC;
    }

    @Override
    protected String getSpec()
    {
    	return "resources/guidewire/openapi.json";
    }    
    
    //TODO We need to implement get swagger file authentication
    
}