package com.boomi.connector.sap_concur;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.stripe.StripeBrowser;
import com.boomi.connector.stripe.StripeQueryOperation;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class SAPConcurConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new SAPConcurBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new SAPConcurQueryOperation(createConnection(context));
    }

    protected SAPConcurConnection createConnection(BrowseContext context) {
        return new SAPConcurConnection(context);
    }
}