package com.boomi.connector.sap_concur;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
public class SAPConcurConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    private String _authHeader;
    
	public SAPConcurConnection(BrowseContext context) {
		super(context);
	}
	
    public String getQueryPaginationSplitPath()
    {
    	return "/Items/*";
    }
    
    @Override
    protected AuthType getAuthenticationType()
    {
    	return AuthType.OAUTH;
    }
    
    //TODO this is actually standard Client Credentials oauth so doesn't need to be custom
//    @Override
//    protected String getCustomAuthHeader() throws JSONException, IOException 
//    {
//    	if (_authHeader!=null)
//    		return _authHeader;
//    	String clientSecret=this.getConnectionProperties().getProperty("CLIENTSECRET");
//    	String clientId=this.getConnectionProperties().getProperty("CLIENTID");
//    	if (clientSecret==null || clientSecret.trim().length()==0)
//    		throw new ConnectorException("Client Secret must be specified");
//    	if (clientId==null || clientId.trim().length()==0)
//    		throw new ConnectorException("Client ID must be specified");
//    	String request = String.format("client_secret=%s&client_id=%s&grant_type=password&username=%s&password=%s", clientSecret, clientId, getUsername(), getPassword());
//    	InputStream is = new ByteArrayInputStream(request.getBytes());
//    	CloseableHttpResponse apiResponse = this.doExecute("/oauth2/v0/token", is, "POST", AuthType.NONE);
//    			this.doSend("/oauth2/v0/token", is, "POST", AuthType.NONE, "application/x-www-form-urlencoded");
//    	JSONObject response = new JSONObject(new JSONTokener(apiResponse.getResponse()));
//    	_authHeader="Bearer ";
//    	if (response.has("access_token"))
//    		_authHeader+=response.getString("access_token");
//    	return _authHeader;
//    }

    @Override
    protected String getTestConnectionUrl()
    {
    	return this.getBaseUrl()+"/api/v3.0/common/users?id=xxx";
    }
}