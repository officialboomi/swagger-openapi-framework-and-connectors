package com.boomi.connector.procore;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class ProCoreConnector extends SwaggerConnector {
   
    protected ProCoreConnection createConnection(BrowseContext context) {
        return new ProCoreConnection(context);
    }
}