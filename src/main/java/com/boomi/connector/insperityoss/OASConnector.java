
// Copyright (c) 2022 Boomi, Inc.

package com.boomi.connector.insperityoss;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class OASConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new OASBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new OASQueryOperation(createConnection(context));
    }
    protected OASConnection createConnection(BrowseContext context) {
        return new OASConnection(context);
    }
}



