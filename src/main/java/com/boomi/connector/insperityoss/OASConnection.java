
// Copyright (c) 2022 Boomi, Inc.

package com.boomi.connector.insperityoss;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.AuthType;
import com.boomi.util.CollectionUtil;
import com.boomi.util.StringUtil;

public class OASConnection extends SwaggerConnection {
    private static final String PROPERTY_SPEC  = "spec";
    private static final String CUSTOM_SPEC = "resources/insperity/spec.json";
	public enum CustomConnectionProperties {apiKey}
    
    public OASConnection(BrowseContext context) {
        super(context);
    }

    @Override
    protected String getCustomAuthCredentials()
    {
    	String key = this.getConnectionProperties().getProperty(CustomConnectionProperties.apiKey.name());
    	if (StringUtil.isBlank(key))
    		throw new ConnectorException("API Key is required");
    	return "APIKey " + key;
    }

    @Override
    protected AuthType getAuthenticationType()
    {
    	return AuthType.CUSTOM;
    }
    
    //TODO all the gets require companyid and that is an operation param
    @Override 
    protected String getTestConnectionUrl()
    {
    	String companyId = this.getConnectionProperties().getProperty("companyId");
    	if (StringUtil.isBlank(companyId))
    		throw new ConnectorException("Company ID required for Test Connection");
    	return this.getBaseUrl()+String.format("/public/company/%s/employees/v1", companyId);
    }

    @Override
    public String getSpec() {

        String spec = getContext().getConnectionProperties().getProperty(PROPERTY_SPEC);
        if (StringUtil.isBlank(spec)) {
            spec = CUSTOM_SPEC;
        }
        
        return spec;
    }
    
	@Override
	public String getQueryPaginationSplitPath() {
		// TODO Auto-generated method stub
		return null;
	}
	
	   /**
     * Adds custom headers from Connection page.
     *
     * @return Key value pairs of all headers in the request
     */
 //   @Override
//    protected Iterable<Map.Entry<String, String>> getHeaders(ObjectData data) {
//        Iterable<Map.Entry<String, String>> originalHeaders = super.getHeaders(data);
//
//        Map<String, String> customHeaders = getConnection().getContext().getConnectionProperties().getCustomProperties(CUSTOM_HEADERS_PROPERTY);
//        Iterator<Map.Entry<String ,String>> originalHeaderIterator = originalHeaders.iterator();
//        ArrayList<Map.Entry<String, String>> headerList = new ArrayList<>();
//        
//        //add api key header?
//        String apiKey = getContext().getConnectionProperties().getProperty("apiKey");
//        if (apiKey != null && !apiKey.isEmpty()) {
//            headerList.add(new AbstractMap.SimpleEntry<>("Authorization", "Bearer " + apiKey));
//        }
//               
//        while (originalHeaderIterator.hasNext()) {
//            headerList.add(originalHeaderIterator.next());
//        }
//        if (!CollectionUtil.isEmpty(customHeaders)) {
//            for (Map.Entry<String, String> entry : customHeaders.entrySet()) {
//                String key = entry.getKey();
//                String value = entry.getValue();
//                headerList.add(new AbstractMap.SimpleEntry<>(key, value));
//            }
//        }
//        return headerList;
//    }
}






