package com.boomi.connector.salesforcemarketingcloud;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class SalesforceMarketingCloudConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public SalesforceMarketingCloudConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    protected String getSpec()
    {
    	return "resources/salesforcemarketingcloud/sfmc-openapi-v2.json";
    }    
    
    @Override 
    protected AuthType getAuthenticationType()
    {
    	return AuthType.OAUTH;
    }
}