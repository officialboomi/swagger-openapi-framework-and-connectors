package com.boomi.connector.salesforcemarketingcloud;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class SalesforceMarketingCloudConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new SalesforceMarketingCloudBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new SalesforceMarketingCloudQueryOperation(createConnection(context));
    }
    protected SalesforceMarketingCloudConnection createConnection(BrowseContext context) {
        return new SalesforceMarketingCloudConnection(context);
    }
}