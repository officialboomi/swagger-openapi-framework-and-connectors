package com.boomi.connector.hubspot;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class HubSpotConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public HubSpotConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    protected AuthType getAuthenticationType()
    {
    	return AuthType.OAUTH;
    }

    @Override
    protected String getSpec()
    {
    	return "resources/hubspot/openapi.json";
    }    
}