package com.boomi.connector.hubspot;

import java.util.List;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation.PaginationType;

public class HubSpotQueryOperation extends SwaggerQueryOperation {

	protected HubSpotQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected String getSelectTermsQueryParam(List<String> selectedFields) {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	protected String getFieldSelectionQueryParam()
//	{
//		return "properties";
//	}

	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getNextPageQueryParameterName()
	{
		return "after";
	}
	
	@Override
	protected PaginationType getPaginationType() {
		return PaginationType.PAGINATION_TYPE_TOKEN_PARAMETER;
	}
	
	@Override
	protected String getNextPageElementPath()
	{
		return "/paging/next/after";
	}
}