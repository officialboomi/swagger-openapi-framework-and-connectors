package com.boomi.connector.hubspot;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class HubSpotConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new HubSpotBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new HubSpotQueryOperation(createConnection(context));
    }
    protected HubSpotConnection createConnection(BrowseContext context) {
        return new HubSpotConnection(context);
    }
}