package com.boomi.connector.worldpay;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class WorldpayConnector extends SwaggerConnector {
	
    protected WorldpayConnection createConnection(BrowseContext context) {
        return new WorldpayConnection(context);
    }

}