package com.boomi.connector.bullhorn;

import java.util.List;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.util.CollectionUtil;
import com.boomi.util.StringUtil;

public class BullhornQueryOperation extends SwaggerQueryOperation {

	protected BullhornQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
		if (sortTerms.size()>0)
		{
			String terms="";
			for(Sort sort: sortTerms)
			{
				if (terms.length()>0)
					terms+=",";
				if ("desc".contentEquals(sortTerms.get(0).getSortOrder()))
					terms+="-";
				terms+=sort.getProperty();				
			}
			return "orderBy="+terms;
		}
		return null;
	}

	@Override
	protected PaginationType getPaginationType() {
		// TODO Auto-generated method stub
		return PaginationType.PAGINATION_TYPE_RECORD_OFFSET;
	}
	
	@Override
	protected String getNextPageQueryParameterName() {
		return "start";
	}
	
	   /**
     * Override to build a filter expression for the unique grammar of the API.
     * For example create=2021-01-01&createdCompare=lessThan.
     * The default behavior is simple ANDed expressions using the eq operation &email=j@email.com&active=true
     * @param simpleExpression the simple expression from which to construct the uri parameter and value
     * @return the URL query parameter and value for the filter compare expression
     */
	protected String buildSimpleExpression(SimpleExpression expr) {
        // this is the name of the queried object's property
    	String term="";
        String propName = expr.getProperty();
        String operator = expr.getOperator();
        
        if (StringUtil.isBlank(propName))
        	throw new ConnectorException("Filter field parameter required");
        // we only support 1 argument operations
        if (CollectionUtil.size(expr.getArguments()) != 1) 
            throw new IllegalStateException("Unexpected number of arguments for operation " + expr.getOperator() + "; found " +
                                            CollectionUtil.size(expr.getArguments()) + ", expected 1");

        // this is the single operation argument
       	String parameter=expr.getArguments().get(0);
        if (parameter==null || parameter.length()==0)
        	throw new ConnectorException(String.format("Filter parameter is required for field: %s ", propName));

        if("QUERY".contentEquals(this.getOperationProperties().getProperty("QUERY_TYPE")))
		{
            switch (operator)
            {
		        case "EQ":
		        	term = propName + "=" + parameter;
		        	break;
		        case "LT":
		        	term = propName + "<" + parameter;
		        	break;
		        case "GT":
		        	term = propName + ">" + parameter;
		        	break;
		        case "LE":
		        	term = propName + "<=" + parameter;
		        	break;
		        case "GE":
		        	term = propName + ">=" + parameter;
		        	break;
    	        default:
    	        	throw new ConnectorException(String.format("Unknown query filter operator %s for field: %s", operator, propName));
            }
		} else {
			//SEARCH API
	        switch (operator)
	        {
		        case "EQ":
		        	term = propName + ":" + parameter;
		        	break;
		        default:
		        	throw new ConnectorException(String.format("Unknown Lucene filter operator %s for field: %s", operator, propName));
	        }
		}

        return term;
    }

	
	@Override
	protected String getPageSizeQueryParameterName()
	{
		return "count";
	}
	
	@Override
    protected String getSelectTermsQueryParam(List<String> selectedFields)
    {
		if (selectedFields==null || selectedFields.size()==0)
			throw new ConnectorException("At least one field must be selected.");
		return super.getSelectTermsQueryParam(selectedFields);
    }

}