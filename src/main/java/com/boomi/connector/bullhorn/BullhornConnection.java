package com.boomi.connector.bullhorn;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentMap;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONObject;
import org.json.JSONTokener;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.OAuth2Context;
import com.boomi.connector.api.OAuth2Token;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;
import com.boomi.execution.ExecutionManager;

public class BullhornConnection extends SwaggerConnection {
	public enum CustomConnectionProperties {REGION, OTHER_REGION, CACHE_SCOPE, SESSION_TIMEOUT, CLIENT_ID, CLIENT_SECRET, BULLHORN_AUTHTYPE}
	public enum CacheScope {EXECUTION, ATOM}
	public enum BHAuthType {CUSTOM, OAUTH}
	SessionInfo sessionInfo;
	String clientId;
	String clientSecret;
    
 	public BullhornConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return "/data/*";
    }
    
    @Override
    protected void testConnection()
    {
    	CloseableHttpResponse httpTokenResponse = null;
    	try {
        	httpTokenResponse = doExecute("/ping", null, "GET", null, null, null);    		
    	} catch (GeneralSecurityException | IOException e) {
    		throw new ConnectorException(e);
    	} finally {
    		IOUtil.closeQuietly(httpTokenResponse);
    	}
    }

    private String getRegion()
    {
    	String region = this.getConnectionProperties().getProperty(CustomConnectionProperties.OTHER_REGION.name());
    	if (StringUtil.isEmpty(region))
    		region = this.getConnectionProperties().getProperty(CustomConnectionProperties.REGION.name());
    	if (StringUtil.isEmpty(region))
    		throw new ConnectorException("Data Center Region must be specified");
    	return region;
    }
    
    private String getAuthURL()
    {
    	return "https://auth-"+getRegion()+".bullhornstaffing.com";
    }
    
    @Override 
    protected InputStream insertCustomHeaders(Map<String,String> headers, InputStream data)
    {
    	if (sessionInfo!=null)
    		headers.put("BhRestToken", sessionInfo.sessionToken);
    	return data;
    }
    
    @Override
    //We use this call to authenticate and generate a base URL that includes the session path parameter with a valid session id
	protected String getBaseUrl()
	{
    	//preserve the operation method
    	String originalMethod = this.getHttpMethod();

        	
    		//lets cache the token this.getContext().getConnectorCache();
    		ConcurrentMap<Object, Object> cache = this.getContext().getConnectorCache();
//        	logger.info("Cache Key: " + cacheKeyBase); 

    		String cacheKey=this.getCacheKey();
    		
    		sessionInfo = (SessionInfo) cache.get(cacheKey);
    		if (sessionInfo!=null)
    		{
        		long timeout = this.getConnectionProperties().getLongProperty(CustomConnectionProperties.SESSION_TIMEOUT.name(), 12L);
        		if (timeout>24)
        			timeout=24;
        		if (timeout<0)
        			timeout=0;
        		long age = (new Date()).getTime()-sessionInfo.creationTime;
        		if (age>timeout*60*60*1000)
        		{
        			logger.info("Session Cache Expired");
        			sessionInfo=null;
        			cache.remove(cacheKey);
        		}
    		}
    		if (sessionInfo==null) {
    			
    			String accessToken = null;
        		if (this.getConnectionProperties().getProperty(CustomConnectionProperties.BULLHORN_AUTHTYPE.name(),"CUSTOM").contentEquals(BHAuthType.OAUTH.name()))
        		{
        			OAuth2Context oAuth2Context = _connProps.getOAuth2Context(ConnectionProperties.OAUTHOPTIONS.name());
    				OAuth2Token oAuth2Token;
    				//Client ID and Secret used for session cache key
    				clientId=oAuth2Context.getClientId();
    				clientSecret=oAuth2Context.getClientSecret();
					try {
						oAuth2Token = oAuth2Context.getOAuth2Token(true);
					} catch (IOException e) {
						throw new ConnectorException(e);
					} 
            		accessToken = oAuth2Token.getAccessToken();
       			
    			} else { //Use the hacky username/password auth
       	    		clientId = this.getConnectionProperties().getProperty(CustomConnectionProperties.CLIENT_ID.name());
    	    		clientSecret = this.getConnectionProperties().getProperty(CustomConnectionProperties.CLIENT_SECRET.name());
    	    		if (StringUtil.isBlank(clientId))
    	    			throw new ConnectorException("Client ID must be specified");
    	    		if (StringUtil.isBlank(clientSecret))
    	    			throw new ConnectorException("Client Secret must be specified");

    				accessToken = getAccessToken();
        		}
        	
            	if (StringUtil.isNotBlank(accessToken))
            	{
            		getSessionToken(accessToken);
            	}                			

            	if (sessionInfo==null)
            		throw new ConnectorException("Login error. Please verify username, password, client ID and endpoint");
            	this.setHttpMethod(originalMethod);
        		logger.info("Caching new session");
        		cache.put(cacheKey, sessionInfo);      	
        	} else {
        		logger.info("Session found in cache");
        	}

    	return sessionInfo.restUrl;
    } 
    
    private String getAccessToken()
    {
    	String accessToken=null;
    	CloseableHttpResponse httpAuthorizeResponse = null;
    	CloseableHttpResponse httpTokenResponse = null;
    	
    	try {
      		//https://auth-emea.bullhornstaffing.com
//        	String path = "/oauth/authorize";
//        	String body = String.format("action=Login&username=%s&password=%s&client_id=%s&response_type=code&redirect_URI=http://www.bullhorn.com/", getUsername(), getPassword(), clientId);
    		String path = String.format("/oauth/authorize?action=Login&username=%s&password=%s&client_id=%s&response_type=code&redirect_URI=http://www.bullhorn.com/", getUsername(), getPassword(), clientId);            	String url = getAuthURL() + path ;
        	httpAuthorizeResponse = this.doExecuteWithoutAuthentication(url, null, "GET");
        	//POST does not work...
//        	httpAuthorizeResponse = this.doExecuteWithoutAuthentiation(url, new ByteArrayInputStream(bmelody.getBytes()), "POST");
        	Header[] hdrs = httpAuthorizeResponse.getAllHeaders();
        	//oauth/authorize?action=Login&username=boomiapi.nes&password=1ronMan%23&client_id=fa32f181-670a-4809-88c5-9a866daa9a2d&response_type=code&redirect_URI=http://www.bullhorn.com/
        	String redirect = null;
        	
        	String authorizationCode = null;
//        	String resp = SwaggerUtil.inputStreamToString(httpAuthorizeResponse.getEntity().getContent());

        	if (httpAuthorizeResponse.getFirstHeader("Location")!=null)
        	{
        		redirect = httpAuthorizeResponse.getFirstHeader("Location").getValue();
        		logger.info("REDIRECT: " + redirect);
        	} else {
        		logger.info("ERROR: No Location redirect header");
        	}
        	if (StringUtil.isNotBlank(redirect))
        	{
            	String target="code=";
            	int codeLocation = redirect.indexOf(target);
            	if (codeLocation>-1)
            	{
            		//&client_id=987b70b6-7e21-4460-942d-8c549a5608cf&client_secret=4xh395KwkkYwvsIkKbLf2uujZ3X81vvd
            		authorizationCode=redirect.substring(codeLocation+target.length());
            		//Remove any trailing URI params
            		codeLocation=authorizationCode.indexOf("&");
            		if (codeLocation>-1)
            			authorizationCode = authorizationCode.substring(0, codeLocation);
            		//https://auth.bullhornstaffing.com
            		path = String.format("/oauth/token?grant_type=authorization_code&code=%s&client_id=%s&client_secret=%s&redirect_URI=http://www.bullhorn.com", authorizationCode, clientId, clientSecret);
            		url = getAuthURL() + path;
            		
            		httpTokenResponse = this.doExecuteWithoutAuthentication(url, new ByteArrayInputStream("".getBytes()), "POST");
                	JSONObject tokenResponse = new JSONObject(new JSONTokener(httpTokenResponse.getEntity().getContent()));
                	if (tokenResponse.has("access_token"))
                		accessToken=tokenResponse.getString("access_token");
//                	if (tokenResponse.has("refresh_token"))
//                		refreshToken=tokenResponse.getString("refresh_token");
            	}
        	}
    	} catch (Exception e) {
    		e.printStackTrace();
    		throw new ConnectorException(e);
    	} finally {
			IOUtil.closeQuietly(httpAuthorizeResponse);
			IOUtil.closeQuietly(httpTokenResponse);
    	}
    	return accessToken;
    }
    
    private void getSessionToken(String accessToken)
    {
		//POST https://rest.bullhornstaffing.com/rest-services/login?version=*& [rest.bullhornstaffing.com] access_token={xxxxxxxx}
		String params = String.format("version=*&access_token=%s", accessToken);
    	String path = String.format("/rest-services/login?%s", params);
    	
//    	path = "/rest-services/login";
    	
//		uri = new URI("https://auth-"+getRegion()+".bullhornstaffing.com"+path);
		String url = "https://rest-"+getRegion()+".bullhornstaffing.com"+path;
		//TODO does get region even work? Hardcode for now    	 httpSessionResponse = null;

		CloseableHttpResponse httpSessionResponse = null;
		
		try {
			httpSessionResponse = this.doExecuteWithoutAuthentication(url, null, "GET");
			//TODO  POST DOESNT WORK! but maybe because need Content-Type = form encoded?
//			httpSessionResponse = this.doExecuteWithoutAuthentication(url, new ByteArrayInputStream(params.getBytes()), "POST");
	    	JSONObject sessionResponse = new JSONObject(new JSONTokener(httpSessionResponse.getEntity().getContent()));
//	    	if (sessionResponse.has("BhRestToken"))
//	    		bhRestToken=tokenResponse.getString("BhRestToken");
	    	if (sessionResponse.has("restUrl"))
	    	{
				sessionInfo=new SessionInfo();
				sessionInfo.creationTime=(new Date()).getTime();
				
//	    		/https://rest.bullhornstaffing.com/rest-services/ [rest.bullhornstaffing.com]{corpToken}/"
				sessionInfo.restUrl=sessionResponse.getString("restUrl");
	    		if (sessionInfo.restUrl.endsWith("/"))
	    			sessionInfo.restUrl=sessionInfo.restUrl.substring(0,sessionInfo.restUrl.length()-1);
	    		sessionInfo.sessionToken = sessionResponse.getString("BhRestToken");
	    	}
		} catch (IOException | GeneralSecurityException e) {
			throw new ConnectorException (e);
		} finally {
    		IOUtil.closeQuietly(httpSessionResponse);
		}
    }
    
    private String getCacheKey()
    {
    	String cacheKey = this.getRegion()+clientId+clientSecret;
    	String cacheScope = this.getConnectionProperties().getProperty(CustomConnectionProperties.CACHE_SCOPE.name(),CacheScope.EXECUTION.name());
    	if (cacheScope.contentEquals(CacheScope.EXECUTION.name()))
    	{
    		String executionId=UUID.randomUUID().toString(); //Test Connection doesn't have an executionID
    		if (ExecutionManager.getCurrent()!=null)
    			executionId=ExecutionManager.getCurrent().getExecutionId();
        		cacheKey += executionId;
    	}
    	return cacheKey;
    }
    
    class SessionInfo {
    	String restUrl;
    	String sessionToken;
    	long creationTime;
    }
    
//    private String executeFormPost(String urlString, String method, String body){
//        CloseableHttpClient httpClient = null;
//        CloseableHttpResponse response = null;
//        String responseString;
//        try {
//            // executable client
//            httpClient  = HttpClientBuilder.create().build();
//            // client context
//            HttpClientContext context = HttpClientContext.create();
//            URI url = new URI(urlString);
//            HttpPost httpPost = new HttpPost();
//            httpPost.setURI(url);
//            httpPost.addHeader("Accept", "application/json");
//            InputStreamEntity reqEntity = new InputStreamEntity(new ByteArrayInputStream(body.getBytes()));
//            reqEntity.setContentType("application/x-www-form-urlencoded");
//            response = httpClient.execute(httpPost, context);
//            // Do stuff with Response
//            //return the actual data
//            responseString = SwaggerUtil.inputStreamToString(response.getEntity().getContent());
//            return responseString;
//        } catch (Exception e) {
//            throw new ConnectorException("Unable to browse ", e.getMessage());
//        } finally {
//            IOUtil.closeQuietly(httpClient, response);
//        }
//    }
}