package com.boomi.connector.bullhorn;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class BullhornConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new BullhornBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new BullhornQueryOperation(createConnection(context));
    }
    protected BullhornConnection createConnection(BrowseContext context) {
        return new BullhornConnection(context);
    }
}