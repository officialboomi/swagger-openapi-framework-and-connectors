package com.boomi.connector.stripe;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.util.StringUtil;

public class StripeConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public StripeConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    protected String getPassword()
    {
    	return "";
    }
    
    //Stripe puts secret key in user and leave password blank
    @Override
    protected AuthType getAuthenticationType()
    {
    	return AuthType.BASIC;
    }
    
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return "/data/*";
    }
    
    @Override 
    protected String getRequestContentType()
    {
    	return "application/x-www-form-urlencoded";
    }
}