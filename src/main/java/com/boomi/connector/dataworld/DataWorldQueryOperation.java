package com.boomi.connector.dataworld;

import java.util.List;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;

public class DataWorldQueryOperation extends SwaggerQueryOperation {

	protected DataWorldQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected String getNextPageElementPath()
	{
		return "/nextPageToken";
	}
	
	@Override
	protected String getNextPageQueryParameterName() {
		return "next";
	}

	@Override
	protected String getSelectTermsQueryParam(List<String> selectedFields) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PaginationType getPaginationType() {
		return PaginationType.PAGINATION_TYPE_TOKEN_PARAMETER;
	}
}