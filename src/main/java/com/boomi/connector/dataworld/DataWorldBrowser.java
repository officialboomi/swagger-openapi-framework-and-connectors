package com.boomi.connector.dataworld;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;

public class DataWorldBrowser extends SwaggerBrowser  implements ConnectionTester {
    public DataWorldBrowser(SwaggerConnection conn) {
        super(conn);
    }
        
    @Override
    protected boolean isPaginatedQuery(JSONArray queryParameters)
    {
    	return hasQueryParameter(queryParameters, "next");
    }

	@Override
    public DataWorldConnection getConnection() {
        return (DataWorldConnection) super.getConnection();
    }
	
	@Override
	protected void getFilterSortSpecs(SwaggerAPIService swaggerService, List<FieldSpecField> fields) {
		JSONArray queryParams = swaggerService.getQueryParameters();
		for (int i=0; i<queryParams.length(); i++)
		{
			JSONObject param = queryParams.getJSONObject(i);
			if (param.has("name"))
			{
				String name = param.getString("name");
				//TODO need to use schema/type to get integer/epoch datetime type
				String type = "string";
				switch (name)
				{
				case "next":
				case "limit":
					break;
				default:
					FieldSpecField filterable = new FieldSpecField().withName(name).withFilterable(true).withSortable(false).withSelectable(false).withType(type);				
					fields.add(filterable);						
				}
			}
		}
	}
}
