package com.boomi.connector.facebook_marketing;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class FacebookMarketingConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public FacebookMarketingConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return "/items/*";
    }

    @Override
    protected String getSpec()
    {
    	return "resources/facebook_marketing/openapi.json";
    }    
}