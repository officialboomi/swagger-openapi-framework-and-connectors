package com.boomi.connector.dunandbradstreet;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class DunAndBradstreetConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new DunAndBradstreetBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new DunAndBradstreetQueryOperation(createConnection(context));
    }
    protected DunAndBradstreetConnection createConnection(BrowseContext context) {
        return new DunAndBradstreetConnection(context);
    }
}