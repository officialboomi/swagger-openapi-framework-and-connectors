package com.boomi.connector.augury;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.logging.Logger;

import org.json.JSONException;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.AuthType;


public class AuguryConnection extends SwaggerConnection {
        
	public AuguryConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    protected String getSpec()
    {
    	return "resources/augury/openapi.json";
    }    
    
    @Override
    protected AuthType getAuthenticationType()
    {
    	return AuthType.CUSTOM;
    }
    
    @Override
    protected String getCustomAuthCredentials() throws JSONException, IOException, GeneralSecurityException
    {
    	return null;
    }    
	}