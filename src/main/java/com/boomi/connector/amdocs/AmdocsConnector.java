package com.boomi.connector.amdocs;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class AmdocsConnector extends SwaggerConnector {
   
    protected AmdocsConnection createConnection(BrowseContext context) {
        return new AmdocsConnection(context);
    }
}