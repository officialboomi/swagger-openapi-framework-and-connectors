package com.boomi.connector.kefron;

import java.util.List;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;

public class KefronQueryOperation extends SwaggerQueryOperation {

	protected KefronQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected String getSelectTermsQueryParam(List<String> selectedFields) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PaginationType getPaginationType() {
		// TODO Auto-generated method stub
		return PaginationType.PAGINATION_TYPE_PAGE_OFFSET;
	}
	
	@Override
	protected boolean isZeroBasedOffset()
	{
		return false;
	}
	
	/**
	 * Override to specify offset pagination path parameters
	 * For example 'offset' for PAGINATION_TYPE_RECORD_OFFSET, 'after' for PAGINATION_TYPE_LAST_ID.
	 * Not required for  PAGINATION_TYPE_LINK_HEADER, PAGINATION_TYPE_URL_FIELD
	 * @return
	 */
	protected String getNextPageQueryParameterName() {
		return "PageNumber";
	}
	
    /**
     * Override to specify the name of the parameter that indicates the page size query parameter to specify the number of records per page
     * Defaults to "limit"
     * @return the page size query parameter name
     */
	protected String getPageSizeQueryParameterName()
	{
		return "PageSize";
	}
}