package com.boomi.connector.kefron;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Logger;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.execution.ExecutionManager;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;


public class KefronConnection extends SwaggerConnection {
    
	String operationToken = null;
	
	public KefronConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return "/data/*";
    }

    @Override
    protected String getSpec()
    {
    	return "resources/kefron/openapi.json";
    }    
    
    @Override
    public AuthType getAuthenticationType()
    {
    	return AuthType.CUSTOM;
    }
    
    @Override
    protected String getTestConnectionUrl()
    {
    	return this.getBaseUrl()+"/comments";
    }
    
    @Override
    protected String getResponseContentType()
    {
    	return "*/*";
    }
  
	/**
	 * Provide a custom Authorization header for AuthType.CUSTOM implementations.
	 * Useful for proprietary API driven authentication. 
	 * @return the header value ie. Bearer xxxxxxxxxxxxxxxxxxxxxxxxxx
	*/
    protected String getCustomAuthCredentials() throws JSONException, IOException, GeneralSecurityException
    {
    	String httpMethod = this.getHttpMethod(); //KLUDGE if we let the subclass call doExecute, we have to preserve the method so it doesn't get stomped...GOTTA fix this
    	InputStream is = null;
    	CloseableHttpResponse httpResponse = null;
    	try {
    		String token = null;// DISABLE CACHING getCachedToken();
    		if (StringUtil.isBlank(token))
    		{
            	String request = String.format("{\"apiKey\":\"%s\",\"password\":\"%s\"}", getUsername(), getPassword());
            	is = new ByteArrayInputStream(request.getBytes());
            	httpResponse = this.doExecuteWithoutAuthentication(this.getBaseUrl()+"/v1/api", is, "POST");
            	JSONObject response = new JSONObject(new JSONTokener(httpResponse.getEntity().getContent()));
            	if (response.has("token"))
            	{
            		token = response.getString("token");
            		this.getContext().getConnectorCache().put(this.getCacheKey(), token);
            	}
            	this.setHttpMethod(httpMethod);
    		}
    		if (StringUtil.isBlank(token))
    			throw new ConnectorException("Authentication unsuccessful");
        	String header="Bearer ";
    		header+=token;

        	return header;
    	} finally {
    		if (is!=null)
    			IOUtil.closeQuietly(is);
    		if (httpResponse!=null)
    			httpResponse.close();
    	}
    }    
    
	private String getCachedToken()
	{
		String cacheKey = this.getCacheKey();
		ConcurrentMap<Object, Object> cache = this.getContext().getConnectorCache();
		if (cache==null)
		{
			logger.warning("Connector Cache is NULL");
			return null;
		}
		String token = (String) cache.get(cacheKey);
		if (token!=null)
		{
			logger.info("Cached Token Found");
		}
		return token;
	}
    
	private String getCacheKey()
	{
		String executionId=null;
   		if (ExecutionManager.getCurrent()!=null)
			executionId=ExecutionManager.getCurrent().getExecutionId();
		String cacheKey = "___CACHED_TOKEN_"+this.getBaseUrl()+"_"+this.getUsername()+"_"+this.getPassword()+"_"+executionId;
//		logger.info("cacheKey: " + cacheKey);
		return cacheKey;
	}
}