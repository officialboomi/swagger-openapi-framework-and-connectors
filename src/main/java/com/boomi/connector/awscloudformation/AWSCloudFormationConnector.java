package com.boomi.connector.awscloudformation;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class AWSCloudFormationConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new AWSCloudFormationBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new AWSCloudFormationQueryOperation(createConnection(context));
    }
    @Override
    protected Operation createExecuteOperation(OperationContext context) {
        return new AWSCloudFormationExecuteOperation(createConnection(context));
    }
    protected AWSCloudFormationConnection createConnection(BrowseContext context) {
        return new AWSCloudFormationConnection(context);
    }
}