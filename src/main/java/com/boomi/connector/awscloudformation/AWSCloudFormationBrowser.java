package com.boomi.connector.awscloudformation;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ui.AllowedValue;
import com.boomi.connector.api.ui.BrowseField;
import com.boomi.connector.api.ui.DataType;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.AuthType;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.util.StringUtil;

public class AWSCloudFormationBrowser extends SwaggerBrowser  implements ConnectionTester {
	public enum CFOperationProperties {TEMPLATE_BODY, TEMPLATE_URL}
	public static final String TEMPLATE_QUERY_PARAMETER_FIELD_ID_PREFIX = "TEMPLATE_QUERY_PARAMETER_";
    public AWSCloudFormationBrowser(SwaggerConnection conn) {
        super(conn);
    }

	@Override
	protected boolean isCreateOperation(String pathString, String methodName, JSONArray queryParameters, JSONObject methodObject)
	{
		if (!"get".contentEquals(methodName))
			return false;
		return !hasNextToken(queryParameters);
	}
	
	private boolean hasNextToken(JSONArray queryParameters)
	{
		for (int i=0; i<queryParameters.length(); i++)
		{	JSONObject parameter = (JSONObject)queryParameters.get(i);
			if (parameter.has("in"))
				if (parameter.getString("in").contentEquals("query"))
					if (parameter.has("name"))
						if (parameter.getString("name").contentEquals("NextToken"))
							return true;
		}
		return false;
	}
	
	//Check for pagination query parameters in the request schema. This allows for query operations that end with a path parameter
    //TODO there is no way to check if a query because we need to check for NextPage in the response schema...not supported...so use default way looking for lack of trailing {}
    //Concur has query operations that do not have offset as a parameter...but DOES have a nextpage response. Seems like a bug in their swagger
	@Override
	protected boolean isQueryOperation(String pathString, String methodName, JSONArray queryParameters, JSONObject methodObject)
	{
		if (!"get".contentEquals(methodName))
			return false;
		return hasNextToken(queryParameters);
	}
	
	//TODO TemplateURL and Template is a browse field but we need the value at execute-time
	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId,
			Collection<ObjectDefinitionRole> roles)	
	{
		ObjectDefinitions objectDefinitions = super.getObjectDefinitions(objectTypeId, roles);
		
    	JSONArray queryParameters = this._swaggerService.getQueryParameters();
    	for (int i=0; i<queryParameters.length(); i++)
    	{
    		JSONObject parameter = queryParameters.getJSONObject(i);
    		String parameterName = parameter.getString("name");
    		switch (parameterName)
    		{
    		//Parameters are filled in from the template file, not from swagger
    		case "Parameters":
    			JSONObject template=null;

    	    	String templateString=this._opProps.getProperty(CFOperationProperties.TEMPLATE_BODY.name());
    	    	if (StringUtil.isNotBlank(templateString))
    	    	{
    	    		try {
        	    		template =  new JSONObject(templateString);	
        			} catch (JSONException e) {
        				// TODO Auto-generated catch block
        				throw new ConnectorException(e.getMessage() + " Can not parse template body :" + template);
        			}
    	    	} else {
        	    	String templateURL=this._opProps.getProperty(CFOperationProperties.TEMPLATE_URL.name());
        	    	logger.info(templateURL);
        	    	if (StringUtil.isNotBlank(templateURL))
        	    	{
            			try {		
                	    	HttpURLConnection conn;
            				conn = (HttpURLConnection) getConnection().openConnection(new URL(templateURL), AuthType.NONE);
            				template =  new JSONObject(new JSONTokener(conn.getInputStream()));	
            		    	if (conn.getResponseCode()!=200)
            					throw new ConnectorException("Can not load resource:" + templateURL);		    		
            			} catch (JSONException | IOException | GeneralSecurityException e) {
            				// TODO Auto-generated catch block
            				throw new ConnectorException("Can not load resource:" + templateURL);
            			}
        	    	}
    	    	}
   			
    	    	if (template!=null)
    	    	{
    	        	logger.info(template.toString(2));
    	    		if (template.has("Parameters"))
    	    		{
    	    			JSONObject parameters = template.getJSONObject("Parameters");
    	    			Set<String> keySet = parameters.keySet();
    	    			for (String key : keySet)
    	    			{    	    		
    	    				BrowseField browseField = createQueryParameterField(key, parameters.getJSONObject(key));
    	    				if (browseField!=null)
    	    				{
    	    					objectDefinitions.getOperationFields().add(browseField);
    	    				}
    	    			}
    	    		}
    	    	}
				break;
    		}
    	}
		
		return objectDefinitions;
	}
	
	@Override
	protected boolean excludeHeaderParameter(String name)
	{
		return true;
	}
	
	@Override
	protected boolean excludeQueryParameter(String name)
	{
		//Templates are browse fields so that is appended to body in AWSCloudFormationConnection
		List<String> excludes= Arrays.asList("Parameters", "TemplateURL", "TemplateBody");
		return excludes.contains(name);
	}
	
	@Override
    public AWSCloudFormationConnection getConnection() {
        return (AWSCloudFormationConnection) super.getConnection();
    }
	
	private static BrowseField createQueryParameterField(String parameterName, JSONObject parameter) {
		   BrowseField simpleField = new BrowseField();
		   String parameterID = TEMPLATE_QUERY_PARAMETER_FIELD_ID_PREFIX+parameterName;
		   // Mandatory to set an ID
		   simpleField.setId(parameterID);
		   // User Friendly Label, defaults to ID if not given.
		   simpleField.setLabel("Template Parameter - " + parameterName);
		   simpleField.setOverrideable(true);
		   // Mandatory to set a DataType. This Data Type can also be String, Boolean, Integer, Password
		   JSONObject typeDefinition = parameter;
		   switch(typeDefinition.getString("Type"))
		   {
		   case "Number":
			   simpleField.setType(DataType.INTEGER);
			   break;
		   case "Boolean":
			   simpleField.setType(DataType.BOOLEAN);
			   break;
		   default:
			   simpleField.setType(DataType.STRING);
		   }
		   
		   if (typeDefinition.has("AllowedValues"))
		   {
			   JSONArray enums = typeDefinition.getJSONArray("AllowedValues");
			   for (int i=0; i<enums.length(); i++)
			   {
				   AllowedValue av = new AllowedValue();
				   String value = enums.getString(i);
				   av.setLabel(value);
				   av.setValue(value);
				   simpleField.getAllowedValues().add(av);
			   }
		   }
	   
		   // Optional Help Text for the String Field
		   String description ="";
		   //TODO XML ENCODE or does SDK handle that?
		   if (parameter.has("Description"))
			   if (StringUtil.isNotBlank(parameter.getString("Description")))
				   description = parameter.getString("Description");
		   
		   if (parameter.has("AllowedPattern"))
			   if (StringUtil.isNotBlank(parameter.getString("AllowedPattern")))
				   description += " Allowed Pattern: " + parameter.getString("AllowedPattern");

		   if (parameter.has("MinValue"))
			   if (StringUtil.isNotBlank(parameter.getString("MinValue")))
				   description += " Min Value: " + parameter.getString("MinValue");

		   if (parameter.has("MaxValue"))
			   if (StringUtil.isNotBlank(parameter.getString("MaxValue")))
				   description += " Max Value: " + parameter.getString("MaxValue");

		   if (parameter.has("MinLength"))
			   if (StringUtil.isNotBlank(parameter.getString("MinLength")))
				   description += " Min Length: " + parameter.getString("MinLength");

		   if (parameter.has("MaxLength"))
			   if (StringUtil.isNotBlank(parameter.getString("MaxLength")))
				   description += " Max Length: " + parameter.getString("MaxLength");

		   if (parameter.has("ConstraintDescription"))
			   if (StringUtil.isNotBlank(parameter.getString("ConstraintDescription")))
				   description += " Constraint: " + parameter.getString("ConstraintDescription");

		   simpleField.setHelpText(description);
		   // Optional Default Value for String Field
		   if (parameter.has("Default"))
			   simpleField.setDefaultValue(parameter.getString("Default"));
		   return simpleField;
		}

	
}