package com.boomi.connector.clovers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Map;
import java.util.logging.Logger;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.PropertyMap;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerUtil;
import com.boomi.util.StringUtil;


public class CloversConnection extends SwaggerConnection {
        
	public enum CustomConnectionProperties {SECRET, CLIENT_ID}
    
    String _host;
	private static final String GET_HEADER = "host (request-target) v-c-merchant-id";
	private static final String POST_HEADER = "host (request-target) digest v-c-merchant-id";
	private static final String algorithm="HmacSHA256";

	public CloversConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    protected String getSpec()
    {
    	return "resources/clovers/openapi.json";
    }    
    
	//String uri="/tss/v2/transactions/5756530710636693503006";
    //path must be the full path excluding only the domain name
    //For post, we need to read the stream...so we rebuilt it and return it
    //For get we just return the stream as is
	//TODO the swagger we have for transactions has a bad basePath(/TCS227/CBSE/1.0.0) so it was removed in file by hand.
    @Override
	protected InputStream insertCustomHeaders(Map<String,String> headers, InputStream data)
	{
    	
    	String secret = this.getConnectionProperties().getProperty(CustomConnectionProperties.SECRET.name());
    	if (StringUtil.isEmpty(secret))
    		throw new ConnectorException("A secret must be specified for the connection");
    	String clientId =  this.getConnectionProperties().getProperty(CustomConnectionProperties.CLIENT_ID.name());
		Logger logger = this.getLogger();
		
		try {
		    Mac mac = Mac.getInstance(algorithm);
		    SecretKeySpec secretKeySpec = new SecretKeySpec(secret.getBytes("UTF-8"), algorithm);
		    mac.init(secretKeySpec);
		    String payload = SwaggerUtil.inputStreamToString(data);
		    byte[] payloadBytes = payload.getBytes();
		    byte[] signature = mac.doFinal(payloadBytes);
		    String signatureString = String.format("%032x", new BigInteger(1, signature));
		    //Note we could reuse this for multiple posts by doing a mac.reset() before another doFinal

		    //TODO this does not scale!!
		    data= new ByteArrayInputStream(payload.getBytes());;
	    	headers.put("X-ATS-Signature", signatureString);
		} catch (InvalidKeyException | NoSuchAlgorithmException | IllegalStateException | IOException e) {
			throw new ConnectorException(e);
		}
    	headers.put("X-ATS-Signature-Alg", algorithm);
    	headers.put("X-ATS-Client", clientId);
					
		return data;
	}
}