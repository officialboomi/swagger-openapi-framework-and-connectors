package com.boomi.connector.clovers;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;

public class CloversBrowser extends SwaggerBrowser  implements ConnectionTester {
    public CloversBrowser(SwaggerConnection conn) {
        super(conn);
    }

	//Check for pagination query parameters in the request schema. This allows for query operations that end with a path parameter
    //TODO there is no way to check if a query because we need to check for NextPage in the response schema...not supported...so use default way looking for lack of trailing {}
    //Concur has query operations that do not have offset as a parameter...but DOES have a nextpage response. Seems like a bug in their swagger
//	@Override
//	protected boolean isQueryOperation(String pathString, String methodName, JSONArray queryParameters, JSONObject methodObject)
//	{
//		if (!"get".contentEquals(methodName) || queryParameters==null)
//			return false;
//		for (int i=0; i<queryParameters.length(); i++)
//		{	JSONObject parameter = (JSONObject)queryParameters.get(i);
//			if (parameter.has("in"))
//				if (parameter.getString("in").contentEquals("query"))
//					if (parameter.has("name"))
//						if (parameter.getString("name").contentEquals("offset"))
//							return true;
//		}
//		return false;
//	}

	@Override
    public CloversConnection getConnection() {
        return (CloversConnection) super.getConnection();
    }
	
	@Override
	protected void getFilterSortSpecs(SwaggerAPIService swaggerService, List<FieldSpecField> fields) {
		JSONArray queryParams = swaggerService.getQueryParameters();
		for (int i=0; i<queryParams.length(); i++)
		{
			JSONObject param = queryParams.getJSONObject(i);
			if (param.has("name"))
			{
				String name = param.getString("name");
				//TODO need to use schema/type to get integer/epoch datetime type
				String type = "string";
				switch (name)
				{
				case "offset":
				case "limit":
					break;
				default:
					FieldSpecField filterable = new FieldSpecField().withName(name).withFilterable(true).withSortable(false).withSelectable(false).withType(type);				
					fields.add(filterable);						
				}
			}
		}
	}
}