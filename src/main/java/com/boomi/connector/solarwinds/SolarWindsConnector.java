package com.boomi.connector.solarwinds;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class SolarWindsConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new SolarWindsBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new SolarWindsQueryOperation(createConnection(context));
    }
    protected SolarWindsConnection createConnection(BrowseContext context) {
        return new SolarWindsConnection(context);
    }
}