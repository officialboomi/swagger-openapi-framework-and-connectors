package com.boomi.connector.solarwinds;

import java.io.InputStream;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.http.client.methods.CloseableHttpResponse;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.automationanywhere.AutomationAnywhereConnection.CustomConnectionProperties;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.util.StringUtil;


public class SolarWindsConnection extends SwaggerConnection {
	public enum CustomConnectionProperties {TOKEN}    
	static final String AUTHHEADER = "X-Samanage-Authorization";

	public SolarWindsConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    public AuthType getAuthenticationType()
    {
    	return AuthType.CUSTOM;
    }

    @Override
    protected String getSpec()
    {
    	return "resources/solarwinds/openapi.json";
    }  
    
    @Override 
    protected InputStream insertCustomHeaders(Map<String,String> headers, InputStream data)
    {
    	CloseableHttpResponse httpTokenResponse = null;
		String authToken = this.getConnectionProperties().getProperty(CustomConnectionProperties.TOKEN.name());
    	if (StringUtil.isBlank(authToken))
    		throw new ConnectorException("An API Authentication Token is required");
    	headers.put(AUTHHEADER, "Bearer " + authToken);
    	return data;
    }
    
    @Override 
    protected String getTestConnectionUrl()
    {
    	return this.getBaseUrl()+"/incidents?per_page=20";
    }

}