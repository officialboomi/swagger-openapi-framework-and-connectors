package com.boomi.connector.solarwinds;

import java.net.URLEncoder;
import java.util.List;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.util.CollectionUtil;
import com.boomi.util.StringUtil;

public class SolarWindsQueryOperation extends SwaggerQueryOperation {

	protected SolarWindsQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected String getSelectTermsQueryParam(List<String> selectedFields) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PaginationType getPaginationType() {
		// TODO Auto-generated method stub
		return PaginationType.PAGINATION_TYPE_PAGE_OFFSET;
	}
	
	/**
	 * Override of pagination indexing is 1 based vs. the default of zero based
	 * @return
	 */
	@Override
	protected boolean isZeroBasedOffset()
	{
		return false;
	}
	
	@Override
    /**
     * Override to build a filter expression for the unique grammar of the API.
     * For example create=2021-01-01&createdCompare=lessThan.
     * The default behavior is simple ANDed expressions using the eq operation &email=j@email.com&active=true
     * @param simpleExpression the simple expression from which to construct the uri parameter and value
     * @return the URL query parameter and value for the filter compare expression
     */
	protected String buildSimpleExpression(SimpleExpression expr) {
        // this is the name of the queried object's property
    	String term="";
        String propName = expr.getProperty();
        String operator = expr.getOperator();
        
        if (StringUtil.isBlank(propName))
        	throw new ConnectorException("Filter field parameter required");
        // we only support 1 argument operations
        if (CollectionUtil.size(expr.getArguments()) != 1) 
            throw new IllegalStateException("Unexpected number of arguments for operation " + expr.getOperator() + "; found " +
                                            CollectionUtil.size(expr.getArguments()) + ", expected 1");

        // this is the single operation argument
       	String parameter=expr.getArguments().get(0);
        if (parameter==null || parameter.length()==0)
        	throw new ConnectorException(String.format("Filter parameter is required for field: %s ", propName));
        int lastSlashPos = propName.lastIndexOf("/");
        if (lastSlashPos>0)
        	propName = propName.substring(propName.lastIndexOf("/")+1);
        switch (operator)
        {
	        case "eq":
	        	term = propName + "=" + URLEncoder.encode(parameter);
	        	break;
	        default:
	        	throw new ConnectorException(String.format("Unknown filter operator %s for field: %s", operator, propName));
        }

        return term;
    }
	
    /**
     * Override to specify the name of the parameter that indicates the page size query parameter to specify the number of records per page
     * Defaults to "limit"
     * @return the page size query parameter name
     */
	@Override
	protected String getPageSizeQueryParameterName()
	{
		return "per_page";
	}
	
	/**
	 * Override to specify offset pagination path parameters
	 * For example 'offset' for PAGINATION_TYPE_RECORD_OFFSET, 'after' for PAGINATION_TYPE_LAST_ID.
	 * Not required for  PAGINATION_TYPE_LINK_HEADER, PAGINATION_TYPE_URL_FIELD
	 * @return
	 */
	@Override
	protected String getNextPageQueryParameterName() {
		return "page";
	}


}