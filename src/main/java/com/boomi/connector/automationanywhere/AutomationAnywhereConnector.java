package com.boomi.connector.automationanywhere;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class AutomationAnywhereConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new AutomationAnywhereBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new AutomationAnywhereQueryOperation(createConnection(context));
    }
    protected AutomationAnywhereConnection createConnection(BrowseContext context) {
        return new AutomationAnywhereConnection(context);
    }
}