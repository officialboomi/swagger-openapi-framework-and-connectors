package com.boomi.connector.automationanywhere;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.AuthType;
import com.boomi.swaggerframework.swaggerutil.SwaggerUtil;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;


public class AutomationAnywhereConnection extends SwaggerConnection {
        
	public enum CustomConnectionProperties {AA_AUTHTYPE, USERNAME, PASSWORD, APIKEY}
	public enum JWTType {PASSWORD, APIKEY}
	static final String AUTHHEADER = "X-Authorization";

	public AutomationAnywhereConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    
	/**
	 * Override to implement a Test Connection button on the Connections page. Default behavior is to open an authenticated connection to the base host URL
	*/
    @Override
    protected void testConnection() throws Exception
    {
    	CloseableHttpResponse httpResponse = null;
    	try {
    		httpResponse = this.doExecute("/v3/wlm/automations/list", new ByteArrayInputStream("{}".getBytes()), "POST", null, null, null);    		
		} catch (IOException e) {
			throw new ConnectorException(e);
		} finally {
			IOUtil.closeQuietly(httpResponse);
		}
    }
    
    @Override
    protected AuthType getAuthenticationType()
    {
    	return AuthType.CUSTOM;
    }  

    @Override 
    protected InputStream insertCustomHeaders(Map<String,String> headers, InputStream data)
    {
    	CloseableHttpResponse httpTokenResponse = null;
    	String authToken=null;
    	String url = this.getBaseUrl();
    	if (StringUtil.isBlank(url))
    		throw new ConnectorException("A control room URL is required");
    	url = url+"/v1/authentication";
		try {
			String username = this.getUsername();
			String authType = this.getConnectionProperties().getProperty(CustomConnectionProperties.AA_AUTHTYPE.name(),"");
			String request;
			if (JWTType.APIKEY.name().contentEquals(authType))
			{				
				String apiKey = this.getConnectionProperties().getProperty(CustomConnectionProperties.APIKEY.name());
		    	if (StringUtil.isBlank(apiKey))
		    		throw new ConnectorException("An API Key is required");
				request=String.format("{\"username\": \"%s\", \"apiKey\": \"%s\"}", username, apiKey);
			}
			else
			{
				request=String.format("{\"username\": \"%s\", \"password\": \"%s\"}", username, this.getPassword());
			}
	    	CloseableHttpClient httpClient = HttpClients.createDefault();
	    	try {
	            HttpPost httpRequest = new HttpPost(url);
	            HttpEntity entity = new ByteArrayEntity(request.getBytes());
	            httpRequest.setEntity(entity);
	            httpTokenResponse = httpClient.execute(httpRequest);
	        	if (200 != httpTokenResponse.getStatusLine().getStatusCode())
	        		throw new ConnectorException(String.format("Problem connecting to endpoint: %s %s %s", this.getTestConnectionUrl(), httpTokenResponse.getStatusLine().getStatusCode(), httpTokenResponse.getStatusLine().getReasonPhrase()));   			
		    	JSONObject tokenResponse = new JSONObject(new JSONTokener(httpTokenResponse.getEntity().getContent()));
		    	if (tokenResponse.has("token"))
		    	{
		    		authToken=tokenResponse.getString("token");
		    	}
	    	} finally {
				if (httpTokenResponse!=null)
					httpTokenResponse.close();
				if (httpClient!=null)
					httpClient.close();
	    	}
		} catch (IOException e) {
			throw new ConnectorException(e);
		} finally {
			IOUtil.closeQuietly(httpTokenResponse);
		}
		
    	if (StringUtil.isNotBlank(authToken))
    		headers.put(AUTHHEADER, authToken);
    	else
    		throw new ConnectorException("Authentication Error. No JWT Token returned");
    	
    	return data;
    }   
}