package com.boomi.connector.automationanywhere;

import java.io.IOException;
import java.io.InputStream;

import com.boomi.connector.api.ConnectorException;
import com.boomi.util.StringUtil;
import com.boomi.util.json.splitter.JsonSplitter;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class AutomationAnywhereResponseSplitter extends JsonSplitter{
    private String _totalRecords;
    private String itemPath;
    private String totalRecordsPath;
    private String responseStatusPath="/responseStatus";
    StringBuilder pathPointer;
    private boolean _hasError=false;

//TODO we use regex to qualify a next page element but that doesn't allow for a qualifier
//For example, the json below requires an xpath-like expression: /link/[relation=='next']/url
//    "link": [ {
//        "relation": "self",
//        "url": "http://hapi.fhir.org/baseR4/Patient"
//      }, {
//        "relation": "next",
//        "url": "http://hapi.fhir.org/baseR4?_getpages=4397d8e6-4cf0-47f2-b20d-80cbf6c8aa59&_getpagesoffset=20&_count=20&_pretty=true&_bundletype=searchset"
//      } ],
    //Split documents at a specific path base on the itemPathRegEx path value
    //Also capture the next page link based a simple path value
	//totalRecordsPath is optional if offset/page size pagination is used
    public AutomationAnywhereResponseSplitter(InputStream inputStream, String itemPath, String totalRecordsPath) throws IOException {
		super(inputStream);
		this.totalRecordsPath = totalRecordsPath;
		
		this.itemPath = itemPath;
		pathPointer = new StringBuilder();
	}

    //used for next URL or hasmore pagination
    public long getTotalRecords()
    {
    	if (_totalRecords==null)
    		return 0;
   		return Long.parseLong(_totalRecords);
    }
    
	@Override
	protected JsonToken findNextNodeStart() throws IOException {
		JsonParser jsonParser = this.getParser();
    	JsonToken element=null;
 
//We want to parse all the entries but grab the resource child object.
//    	"entry": [ {
//    	    "fullUrl": "http://hapi.fhir.org/baseR4/Patient/1092472",
//    	    "resource": {
//    	      "resourceType": "Patient",
//TODO /entry/*/resource will work but not sure about the end of line match with d+$ ... lose the $ when * in middle of string?
    		
    	element = jsonParser.nextToken();
       	JsonToken rootElement=element;

//    	System.out.println(element.name());
        while (element!=null)
        {
        	if (element == JsonToken.FIELD_NAME)
        	{
        		pushElement(jsonParser.getCurrentName());
        	} 
        	else if (element == JsonToken.END_ARRAY)
        	{
        		popElement();
        		popElement();
        	}
        	
			String name = jsonParser.getCurrentName();
			
    		if ((element==JsonToken.VALUE_STRING)  
        			&& pathPointer.toString().contentEquals(responseStatusPath))
    		{
    			if (!"SUCCESS".contentEquals(jsonParser.getValueAsString()))
    			{
    				_hasError=true;
    				this.itemPath = "/errors/*";
    			}
    		}

			if (name!=null && !StringUtil.isEmpty(totalRecordsPath))
			{
	        	if (_totalRecords==null)
	        	{
	        		if ((element==JsonToken.VALUE_NUMBER_INT)  
	        			&& pathPointer.toString().contentEquals(totalRecordsPath))
	        			_totalRecords = jsonParser.getValueAsString();
	        	}             	
			}
        	if (element==JsonToken.START_OBJECT) 
        	{
//        		System.out.println(pathPointer.toString());
//           		Matcher matcher = pageItemPattern.matcher(jsonPointer.toString());
//           		if (matcher.find()) {
//    	        		return element;//jsonParser.nextToken();
//           		}
        		if (pathPointer.toString().contentEquals(itemPath))
        		{
        			return element;
        		}
        	} 
        	if (element.name().startsWith("VALUE_") || element == JsonToken.END_OBJECT)
        	{
        		popElement();
        	}
        	if (element==JsonToken.START_ARRAY)
        	{
        		pushElement("*");
        	}
        	
        	element = jsonParser.nextToken();
		}		
        return null;
	}
	
	private void pushElement(String name)
	{
		pathPointer.append("/");
		pathPointer.append(name);
//		System.out.println(pathPointer);
	}
	
	private void popElement()
	{
		int lastPos = pathPointer.lastIndexOf("/");
		if (lastPos>-1)
			pathPointer.setLength(lastPos);
//		System.out.println(pathPointer);
	}

	public boolean hasError() {
		return _hasError;
	}
}
