package com.boomi.connector.automationanywhere;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.DynamicPropertyMap;
import com.boomi.connector.api.Expression;
import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.GroupingExpression;
import com.boomi.connector.api.GroupingOperator;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.api.QueryRequest;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.api.Sort;
import com.boomi.connector.util.BaseQueryOperation;
import com.boomi.swaggerframework.swaggeroperations.SwaggerOperationCookie;
import com.boomi.swaggerframework.swaggerutil.SwaggerUtil;
import com.boomi.util.CollectionUtil;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;

public class AutomationAnywhereQueryOperation extends BaseQueryOperation {
    public enum PaginationType {PAGINATION_TYPE_URL_FIELD}
    public enum OperationProperties {MAXDOCUMENTS, PAGESIZE, VERSION_FILTER_OPTIONS, CUSTOM_TERMS, FIND}
    public enum VersionFilterOption {LASTEST_VERSION, ALL_MATCHING_VERSIONS, LATEST_MATCHING_VERSION}

	QueryFilter _queryFilter=null;
   	long _maxDocuments;
   	long _pageSize;
   	PropertyMap _opProps;
   	String _lastIdInPage;
   	CloseableHttpResponse httpResponse;
   	Logger logger;
	private SwaggerOperationCookie _cookie;
	
	protected AutomationAnywhereQueryOperation(AutomationAnywhereConnection conn) {
		super(conn);
	}
	
	@Override
	
	protected void executeQuery(QueryRequest request, OperationResponse response) {
       	logger = response.getLogger();
		getConnection().setLogger(logger);
		String cookieString = this.getContext().getObjectDefinitionCookie(ObjectDefinitionRole.OUTPUT);
		_cookie = new SwaggerOperationCookie(cookieString);
		_opProps = getContext().getOperationProperties();
       	_maxDocuments = getMaxDocuments(_opProps);
       	_pageSize = getPageSize(_opProps);
		
        FilterData input = request.getFilter();
        if (input!=null)
        	_queryFilter = input.getFilter();

		boolean error=false;
		//TODO each pathParam must have a filter term in the top level AND....if not throw an error
		try {
			String jsonRequest = this.buildQueryRequest(_queryFilter, 0);

	       	httpResponse = this.getConnection().doExecute(getContext().getObjectTypeId(), new ByteArrayInputStream (jsonRequest.getBytes()) , "POST",_cookie,null,null);
//	        logger.info(BrowseUtil.inputStreamToString(httpResponse.getEntity().getContent()));
	       	
	       	long numDocuments = 0; 
	        long numInPage;
	        
	        //TODO Why the nested try/catch? 
	        try {
	        	AutomationAnywhereResponseSplitter respSplitter=null;
		        do {   
                	numInPage=0;
	                InputStream is = null;
	                try {
	                	if (numDocuments>0)//We already have some documents so get the next page from the API
	                	{
	                		jsonRequest=this.buildQueryRequest( _queryFilter, numDocuments);
	            	       	httpResponse = this.getConnection().doExecute(getContext().getObjectTypeId(), new ByteArrayInputStream (jsonRequest.getBytes()) , "POST",null,null,null);
	                	}
	                    is = httpResponse.getEntity().getContent();
	                    int httpResponseCode = httpResponse.getStatusLine().getStatusCode();
	                    if (is != null && (httpResponseCode==200)) {
//		                    	if (true)
//		                    	{
//		                    		String testPayload = SwaggerUtil.inputStreamToString(is);
//		                    		is = new ByteArrayInputStream(testPayload.getBytes());
//		                    		System.out.println("Response:\r\n" + testPayload);
//		                    	}
	                    	String queryPaginationSplitPath = "/list/*";
	                    	respSplitter = new AutomationAnywhereResponseSplitter(is,
	                    			queryPaginationSplitPath, "/page/totalFilter");

	                    	for(Payload p : respSplitter) {	
	                    		if (respSplitter.hasError())
			                        response.addPartialResult(input, OperationStatus.APPLICATION_ERROR, "ERROR", "Verify VQL parameters and priviledges for all selected fields. View response for details.", p);
	                    		else
	                    			response.addPartialResult(input, OperationStatus.SUCCESS, httpResponseCode+"", httpResponse.getStatusLine().getReasonPhrase(), p);
				            	numDocuments++;
				            	numInPage++;
			                    
			            		if (_maxDocuments > 0 && numDocuments >= _maxDocuments)
			                		break;
			                }

			                respSplitter.close();
				           	logger.log(Level.INFO, "Document index:" + numDocuments);
	                    }
	                    else {
	                        response.addResult(input, OperationStatus.APPLICATION_ERROR, httpResponseCode+"",
	                        		httpResponse.getStatusLine().getReasonPhrase(), ResponseUtil.toPayload(is));
	                    	logger.severe(httpResponseCode + " " + httpResponse.getStatusLine().getReasonPhrase());
	                        error=true;
	                        break;
	                    }
	                }
	                finally {
	                    IOUtil.closeQuietly(is);
	                    IOUtil.closeQuietly(httpResponse);
	                }
            		if (_maxDocuments > 0 && numDocuments >= _maxDocuments)
                		break;
		        } while(hasMore(respSplitter, numDocuments));	        
	        } catch (Exception e) {
	            ResponseUtil.addExceptionFailure(response, input, e);
            	logger.severe(e.getMessage());
            	error=true;
	        }
	        if (!error)
	        	response.finishPartialResult(input);
		} catch (Exception e) {
        	logger.severe(e.getMessage());
            ResponseUtil.addExceptionFailure(response, input, e);
		}
	}
	
	private String buildQueryRequest(QueryFilter queryFilter, long numDocuments) throws IOException {
		StringBuilder q = new StringBuilder();
		q.append("{");
		buildFieldSelectElements(q,this.getContext().getSelectedFields()); 
						
    	if (queryFilter!=null)
    	{
    		if (queryFilter.getExpression()!=null)
    		{
        		q.append(",\"filter\":");
        		buildFilterElements(q, queryFilter.getExpression());    				
    		}

    		buildSortElements(q, queryFilter.getSort());
    	}    
    	setPagination(q,numDocuments,this.getPageSize(_opProps));
		q.append("}");
		logger.info(q.toString());
		return q.toString();
	}

	private void setPagination(StringBuilder q, long numDocuments, long maxDocuments) {
		q.append(",\"page\":{\"offset\":"+numDocuments+",\"length\":"+maxDocuments+"}");
	}

	@Override
    public AutomationAnywhereConnection getConnection() {
        return (AutomationAnywhereConnection) super.getConnection();
    }	

	private boolean hasMore(AutomationAnywhereResponseSplitter respSplitter, long numDocuments)
	{
//		  "page": {
//			    "offset": 0,
//			    "total": 5,
//			    "totalFilter": 2
//			  }
		
		return (numDocuments<respSplitter.getTotalRecords());
	}
	
	/**
	 * @return the maximum number of documents set by the user in the query operation page
	*/
    public long getMaxDocuments(PropertyMap opProps)
    {
    	return opProps.getLongProperty(OperationProperties.MAXDOCUMENTS.name(), -1L);
    }
    
    
	/**
	 * @return the page size set by the user in the query operation page
	*/
    public long getPageSize(PropertyMap opProps)
    {
        return opProps.getLongProperty(OperationProperties.PAGESIZE.name(), 100L);
    }
	
    //TODO ideally we would know total fields so we could exclude select if default is all. We could set a cookie for that
	//TODO this could be moved to abstract class if we key on value of PAGINATION_SELECT_URIPARAM not null? Comma delimited fields seems to be the standard when selection is implemented
    /**
     * Build the API URL query parameter to indicate what fields the user selected in the Fields list in the Query Operation UI
     * Defaults to "fields=x,y,z"
     * @param selectedFields the list of fields the user selected for the query operation
     * @return the field URI parameter for the selection
     */
    private void buildFieldSelectElements(StringBuilder q, List<String> selectedFields)
    {
    	q.append("\"fields\":[");
    	   	
    	if (selectedFields!=null && selectedFields.size()>0)
    	{
    		for (int x=0; x < selectedFields.size(); x++)
    		{
    			String term = selectedFields.get(x);
    			if (x!=0) q.append(",");
    			q.append("\""+term+"\"");
    		}
    	}
    	q.append("]");
    }
    
    private void buildSortElements(StringBuilder q, List<Sort> sortTerms)
    {
    	q.append(",\"sort\":[");
    	if (sortTerms!=null)
    	{		            
        	for (int x=0; x<sortTerms.size(); x++)
        	{
        		Sort sort = (Sort)sortTerms.get(x);
        		String sortTerm = sort.getProperty();
        		if (x>0) q.append(","); 
//    			sortTerm=sortTerm.replace('/', '.');
//TODO can term be null? If so throw exception?
        		if (sortTerm != null && sortTerm.length()>0)
        		{
        			q.append("{\"field\":\""+sortTerm+"\",");
        			q.append("\"direction\":\""+sort.getSortOrder()+"\"}");
        		}
       		}
    	}
    	q.append("]");
    }
    	
	//This default behavior is to build terms as individual query parameters.
	//AND is assumed
    /**
     * Build the Filter path parameter from the filters in the Query Operation UI
     * Defaults to setting individual path parameters for each simple expression with each parameter a term in a top level AND expression
     * @param queryFilter the filter expression specified by the user in the Query Operation Filter UI
     * @return the field URI parameter for the filter
     * @throws IOException 
     */
	private void buildFilterElements(StringBuilder q, Expression baseExpr) throws IOException
	{
         // see if base expression is a single expression or a grouping expression
        if (baseExpr!=null)
        {
        	//A single operator, no AND/OR
            if(baseExpr instanceof SimpleExpression) {                
                // base expression is a single simple expression
            	buildSimpleExpression(q, (SimpleExpression)baseExpr);
            } else {

                // handle single level of grouped expressions
                GroupingExpression groupExpr = (GroupingExpression)baseExpr;

                // parse all the simple expressions in the group
                boolean first=true;
                for(Expression nestedExpr : groupExpr.getNestedExpressions()) {
                	if (first)
                    	q.append("{\"operator\":\""+groupExpr.getOperator().name().toLowerCase()+"\",\"operands\":[");
                	else
                		q.append(",");
                 	first=false;
                    if(nestedExpr instanceof GroupingExpression) {
                    	buildFilterElements(q, nestedExpr);
                    } else {
                        buildSimpleExpression(q, (SimpleExpression)nestedExpr);
                    }
                }
                q.append("]}");
            }
        }       
	}
       
    /**
     * Override to build a filter expression for the unique grammar of the API.
     * For example create=2021-01-01&createdCompare=lessThan.
     * The default behavior is simple ANDed expressions using the eq operation &email=j@email.com&active=true
     * @param simpleExpression the simple expression from which to construct the uri parameter and value
     * @return the URL query parameter and value for the filter compare expression
     */
	private void buildSimpleExpression(StringBuilder q, SimpleExpression expr) {
        // this is the name of the queried object's property
		SimpleDateFormat boomiDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZ");

    	String term=" ";
        String propName = expr.getProperty();
        String operatorName = expr.getOperator();
        
        if (StringUtil.isBlank(propName))
        	throw new ConnectorException("Filter field parameter required");
        // we only support 1 argument operations
        if (CollectionUtil.size(expr.getArguments()) != 1) 
            throw new IllegalStateException("Unexpected number of arguments for operation " + expr.getOperator() + "; found " +
                                            CollectionUtil.size(expr.getArguments()) + ", expected 1");

        // this is the single operation argument
       	String parameter=expr.getArguments().get(0);
        if (parameter==null || parameter.length()==0)
        	throw new ConnectorException(String.format("Filter parameter is required for field: %s ", propName));
        q.append("{\"operator\":\""+operatorName+"\"");
        q.append(",\"field\":\""+propName+"\"");
        q.append(",\"value\":\""+parameter+"\"}");
    }
}