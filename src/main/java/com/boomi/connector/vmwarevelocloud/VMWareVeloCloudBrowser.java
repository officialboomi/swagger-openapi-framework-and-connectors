package com.boomi.connector.vmwarevelocloud;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;

public class VMWareVeloCloudBrowser extends SwaggerBrowser  implements ConnectionTester {
	public enum VeloOperationProperties {VELO_SERVICE}
	public enum VeloServices{all,edge,enterprise,configuration,disasterRecovery,firewall,gateway,metrics,monitoring,network,role,userMaintenance}
	
    public VMWareVeloCloudBrowser(SwaggerConnection conn) {
        super(conn);
    }
	
	@Override 
	protected String getObjectTypeLabel(String operationId, String summary, String description, List<String> tags) {
		String selectedTag = null;
		for (int i=0; i < tags.size(); i++)
		{
			String tag = tags.get(i);
			if (!"all".contentEquals(tag))
			{
				selectedTag = tag;
			}
		}
		if (selectedTag != null)
			return selectedTag.toUpperCase() + "-" + summary;
		return summary;
	}

	@Override
	public ObjectTypes getObjectTypes() 
	{
		ObjectTypes objectTypes = super.getObjectTypes();
		
		String service = this.getOperationProperties().getProperty(VeloOperationProperties.VELO_SERVICE.name(), VeloServices.all.name());
		if (!VeloServices.all.name().contentEquals(service))
		{
			ObjectTypes filteredObjectTypes = new ObjectTypes();
			objectTypes.getTypes().forEach(objectType -> {
				if (objectType.getLabel().toUpperCase().startsWith(service.toUpperCase()))
					filteredObjectTypes.getTypes().add(objectType);
			});
			objectTypes = filteredObjectTypes;
		}
			
		return objectTypes;
	}
}