package com.boomi.connector.vmwarevelocloud;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerUtil;
import com.boomi.util.IOUtil;


public class VMWareVeloCloudConnection extends SwaggerConnection {
    
	public VMWareVeloCloudConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    protected String getSpec()
    {
    	return "resources/vmwarevelocloud/swagger.json";
    }    
    
    @Override
    protected AuthType getAuthenticationType()
    {
    	return AuthType.CUSTOM;
    }
    
    //TODO Maybe not the best method to use but we are doing this to login and set the cookies in _httpClient
    @Override
    protected String getCustomAuthCredentials()  
    {
    	InputStream is = null;
    	CloseableHttpResponse httpResponse = null;
    	try {
        	String request = String.format("{\"username\":\"%s\",\"password\":\"%s\"}", getUsername(), getPassword());
        	is = new ByteArrayInputStream(request.getBytes());
        	InputStreamEntity entity = new InputStreamEntity(is);
        	String url = this.getBaseUrl()+"/login/enterpriseLogin";
        	HttpPost httpRequest = new HttpPost(url);
 	       	httpRequest.setEntity(entity);
			logger.info("Executing Login : " + url);
			httpResponse = _httpClient.execute(httpRequest, this._httpClientContext);
			InputStream iso = httpResponse.getEntity().getContent();
			String responseString = "";
			if (iso.available()==0)
				iso.close();
			else
			{
				responseString = SwaggerUtil.inputStreamToString(iso);
			}
		
			logger.info(httpResponse.getStatusLine().getStatusCode() + " " + httpResponse.getStatusLine().getReasonPhrase() + " "+ responseString);
    	} catch (IOException e) {
    		throw new ConnectorException(e);
    	} finally {
    		if (is!=null)
    			IOUtil.closeQuietly(is);
    		if (httpResponse!=null)
				try {
					httpResponse.close();
				} catch (IOException e) {
		    		throw new ConnectorException(e);
				}
    	}
    	return null;//return null because we don't want an authorization header
    }
    
    //TODO We need an endpoint to use to test a connection
    @Override
    protected void testConnection() throws Exception
    {
    	CloseableHttpClient httpClient = HttpClients.createDefault();
    	CloseableHttpResponse httpResponse = null;
    	try {
            HttpGet httpRequest = new HttpGet(this.getTestConnectionUrl());
            String authHeader = this.getCustomAuthCredentials();
            if (authHeader!=null)
            	httpRequest.addHeader ("Authorization", authHeader);	
        	logger.info("Test Connection: " +getTestConnectionUrl()+ " " + this.getAuthenticationType().name());
        	httpResponse = httpClient.execute(httpRequest);
        	if (200 != httpResponse.getStatusLine().getStatusCode())
        		throw new Exception(String.format("Problem connecting to endpoint: %s %s %s", this.getTestConnectionUrl(), httpResponse.getStatusLine().getStatusCode(), httpResponse.getStatusLine().getReasonPhrase()));   			
    	} finally {
			if (httpResponse!=null)
				httpResponse.close();
			if (httpClient!=null)
				httpClient.close();
    	}
    }

    
    @Override
    public List<String> getExcludedObjectTypeIDs()
    {
    	return Arrays.asList("/login/operatorLogin", "/login/enterpriseLogin", "/logout");
    }
}