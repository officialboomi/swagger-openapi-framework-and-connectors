package com.boomi.connector.shopify;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class ShopifyConnector extends SwaggerConnector {
   
    protected ShopifyConnection createConnection(BrowseContext context) {
        return new ShopifyConnection(context);
    }
}