package com.boomi.connector.dropbox;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class DropboxConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public DropboxConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return "/items/*";
    }
    
    @Override
    protected String getSpec()
    {
    	return "resources/docusign/esignature.rest.swagger-v2.1.json";
    }
}