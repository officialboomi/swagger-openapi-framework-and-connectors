package com.boomi.connector.dropbox;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class DropboxConnector extends SwaggerConnector {
   
    protected DropboxConnection createConnection(BrowseContext context) {
        return new DropboxConnection(context);
    }
}