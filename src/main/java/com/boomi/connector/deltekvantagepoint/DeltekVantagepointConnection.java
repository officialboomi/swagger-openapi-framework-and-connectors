package com.boomi.connector.deltekvantagepoint;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class DeltekVantagepointConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public DeltekVantagepointConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    public AuthType getAuthenticationType()
    {
    	return AuthType.OAUTH;
    }

    @Override
    protected String getSpec()
    {
    	return "resources/deltekvantagepoint/swagger.json";
    }    
}