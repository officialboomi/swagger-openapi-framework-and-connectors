package com.boomi.connector.petstore;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class PetstoreConnector extends SwaggerConnector {
	   
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new PetstoreQueryOperation(createConnection(context));
    }
    
    protected PetstoreConnection createConnection(BrowseContext context) {
        return new PetstoreConnection(context);
    }
}