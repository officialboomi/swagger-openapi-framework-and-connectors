package com.boomi.connector.petstore;

import java.util.List;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;

public class PetstoreQueryOperation extends SwaggerQueryOperation {
//	private static final String PAGINATION_SELECT_URIPARAM = "fields";
//	private static final String PAGINATION_FILTER_URIPARAM = "q";
//	private static final String PAGINATION_SORT_URIPARAM = "orderBy";
//	private static final String PAGINATION_EXPAND_URIPARAM = "expand"; //TODO

	protected PetstoreQueryOperation(SwaggerConnection conn) {
		super(conn);
	}

	@Override
	protected String getHasMoreElementPath() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getPageSizeQueryParameterName()
	{
		return "limit";
	}
	// xpath supported: /links[rel='next']/href
	@Override
	protected String getNextPageElementPath()
	{
		return "/hasMore";
	}
	
	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String buildSimpleExpression(SimpleExpression nestedExpr) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PaginationType getPaginationType() {
		// TODO Auto-generated method stub
		return null;
	}
}