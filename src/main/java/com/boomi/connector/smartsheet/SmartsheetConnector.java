package com.boomi.connector.smartsheet;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class SmartsheetConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new SmartsheetBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new SmartsheetQueryOperation(createConnection(context));
    }
    protected SmartsheetConnection createConnection(BrowseContext context) {
        return new SmartsheetConnection(context);
    }
}