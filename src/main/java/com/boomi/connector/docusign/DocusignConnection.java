package com.boomi.connector.docusign;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.AuthType;


public class DocusignConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public DocusignConnection(BrowseContext context) {
		super(context);
	}
		
	@Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }
	
    @Override
    protected AuthType getAuthenticationType()
    {
       	return AuthType.OAUTH;
    }
}