package com.boomi.connector.amazonsellingpartner;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class AmazonSellingPartnerConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new AmazonSellingPartnerBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new AmazonSellingPartnerQueryOperation(createConnection(context));
    }
    @Override
    protected Operation createExecuteOperation(OperationContext context) {
        return new AmazonSellingPartnerExecuteOperation(createConnection(context));
    }
    protected AmazonSellingPartnerConnection createConnection(BrowseContext context) {
        return new AmazonSellingPartnerConnection(context);
    }
}