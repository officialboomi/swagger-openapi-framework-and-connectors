package com.boomi.connector.amazonsellingpartner;

import java.net.URLEncoder;
import java.util.List;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.util.CollectionUtil;
import com.boomi.util.StringUtil;

public class AmazonSellingPartnerQueryOperation extends SwaggerQueryOperation {

	protected AmazonSellingPartnerQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected String getSelectTermsQueryParam(List<String> selectedFields) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PaginationType getPaginationType() {
		// TODO Auto-generated method stub
		return PaginationType.PAGINATION_TYPE_TOKEN_PARAMETER;
	}
	
	@Override
	protected String getNextPageElementPath()
	{
		switch (this.getContext().getObjectTypeId())
		{
			case "/fba/outbound/2020-07-01/fulfillmentOrders/preview":
			case "/fba/outbound/2020-07-01/features/inventory/{featureName}":
				return "/payload/nextToken";
			default:
				return "/payload/NextToken";
		}		
	}
	
	@Override
	protected String getNextPageQueryParameterName()
	{
		switch (this.getContext().getObjectTypeId())
		{
			case "/fba/outbound/2020-07-01/fulfillmentOrders/preview":
			case "/fba/outbound/2020-07-01/features/inventory/{featureName}":
				return "nextToken";
			default:
				return "NextToken";
		}		
	}
	
	@Override
	protected String getPageSizeQueryParameterName()
	{
		switch (this.getContext().getObjectTypeId())
		{
		case "/fba/outbound/2020-07-01/fulfillmentOrders/preview":
		case "/fba/outbound/2020-07-01/features/inventory/{featureName}":
		case "/fba/inbound/v0/shipments":
		case "/fba/inbound/v0/shipmentItems":
				return null;
			default:
				return "MaxResultsPerPage";
		}		
	}
	
	@Override
	protected String buildSimpleExpression(SimpleExpression expr) {
        // this is the name of the queried object's property
    	String term="";
        String propName = expr.getProperty();
        String operator = expr.getOperator();
        
        if (StringUtil.isBlank(propName))
        	throw new ConnectorException("Filter field parameter required");
        // we only support 1 argument operations
        if (CollectionUtil.size(expr.getArguments()) != 1) 
            throw new IllegalStateException("Unexpected number of arguments for operation " + expr.getOperator() + "; found " +
                                            CollectionUtil.size(expr.getArguments()) + ", expected 1");

        // this is the single operation argument
       	String parameter=expr.getArguments().get(0);
        if (parameter==null || parameter.length()==0)
        	throw new ConnectorException(String.format("Filter parameter is required for field: %s ", propName));

        switch (operator)
        {
	        case "eq":
	        	term = propName + "=" + parameter;
	        	break;
	        default:
	        	throw new ConnectorException(String.format("Unknown filter operator %s for field: %s", operator, propName));
        }

        return term;
    }

}