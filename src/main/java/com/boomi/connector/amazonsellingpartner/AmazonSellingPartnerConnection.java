package com.boomi.connector.amazonsellingpartner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerUtil;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;


public class AmazonSellingPartnerConnection extends SwaggerConnection {
        
	public enum CustomConnectionProperties {refreshToken, clientId, clientSecret, scope, accessKey, secretKey, region, iamUser, apiRole, useTempCredentials, stsRegion}
	public enum CustomOperationProperties {useRestrictedDataToken, specificRDT}
	//	private final String grantType = "refresh_token";
	private String _refreshToken;
	private String _clientId;
	private String _clientSecret;
	private String _scope;
	
	private String _accessKey;
	private String _secretKey;
	private String _region;
	private String _iamUser;
	private String _accessToken;
	private String _stsSessionToken=null;
	private String _rdtToken=null;	
	Logger _logger = Logger.getLogger("AmazonSellingPartnerConnection");
	
	public AmazonSellingPartnerConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }
    
    @Override
	protected
    AuthType getAuthenticationType()
    {
		return AuthType.NONE;
    } 
    
    @Override
    protected String getSpec()
    {
    	return null;
    }
    
    @Override
	protected InputStream insertCustomHeaders(Map<String,String> headers, InputStream data)
	{
    	_logger = this.getLogger();
    	long sleepTime = this.getConnectionProperties().getLongProperty("SLEEPTIME", 0L);
    	if (sleepTime>0)
    	{
    		this.getLogger().info("Sleeping: " + sleepTime);
    		try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				_logger.severe(e.toString());
			}
    	}
    	if (StringUtil.isBlank(_accessToken)) //TODO we should use ConnectorCache
    	{
    		_refreshToken = this.getConnectionProperties().getProperty(CustomConnectionProperties.refreshToken.name());
    		if (StringUtil.isBlank(_refreshToken))
    			throw new ConnectorException("Refresh Token is required");

    		_clientId = this.getConnectionProperties().getProperty(CustomConnectionProperties.clientId.name());
    		if (StringUtil.isBlank(_clientId))
    			throw new ConnectorException("Client ID is required");

    		_clientSecret = this.getConnectionProperties().getProperty(CustomConnectionProperties.clientSecret.name());
    		if (StringUtil.isBlank(_clientSecret))
    			throw new ConnectorException("Client Secret is required");

    		_accessKey = this.getConnectionProperties().getProperty(CustomConnectionProperties.accessKey.name());
    		if (StringUtil.isBlank(_accessKey))
    			throw new ConnectorException("Access Key is required");

    		_secretKey = this.getConnectionProperties().getProperty(CustomConnectionProperties.secretKey.name());
    		if (StringUtil.isBlank(_secretKey))
    			throw new ConnectorException("Secret Key is required");

    		_region = this.getConnectionProperties().getProperty(CustomConnectionProperties.region.name());
    		if (StringUtil.isBlank(_region))
    			throw new ConnectorException("Region is required");


    		_scope = this.getConnectionProperties().getProperty(CustomConnectionProperties.scope.name());

//    		if (StringUtil.isBlank(scope))
//    			throw new ConnectorException("Scope is required");	
        	_accessToken = this.getOAuthAccessToken();
        	if (getConnectionProperties().getBooleanProperty(CustomConnectionProperties.useTempCredentials.name(), true))
        	{
        		_iamUser = this.getConnectionProperties().getProperty(CustomConnectionProperties.iamUser.name());
        		if (StringUtil.isBlank(_iamUser))
        			throw new ConnectorException("IAM User is required");
        		_stsSessionToken = this.getTemporarySTSCredentials();
        	}
        }
    	try {
    		
        	if (this.getOperationProperties()!=null)
        	{
        		if (this.getOperationProperties().getBooleanProperty(CustomOperationProperties.useRestrictedDataToken.name(),false))
        		{
        			boolean specificRDT = this.getOperationProperties().getBooleanProperty(CustomOperationProperties.specificRDT.name(),false);
        			if (StringUtil.isBlank(_rdtToken) || specificRDT)
        				_rdtToken = this.createRestrictedDataToken(this.getHttpMethod(), specificRDT);
        		}
        			
        	}     	
        	      	
    		String body = "";
    		String method = this.getHttpMethod();
    		if (!"get".contentEquals(method.toLowerCase()))
    		{
    			try {
    				body=SwaggerUtil.inputStreamToString(data);
    				data = new ByteArrayInputStream(body.getBytes("UTF-8"));
    			} catch (IOException e) {
    				throw new ConnectorException(e);
    			}
    		}
			insertSignatureHeaders(new URL(this.getUrl()), method, headers, "execute-api", body, null);
		} catch (MalformedURLException e) {
			throw new ConnectorException(e);
		}

    	return data;
	}
    
    @Override
    protected void testConnection() throws Exception
    {
    	CloseableHttpResponse httpResponse=null;
    	try{
        	httpResponse=this.doExecute("/v0/orders/XXX-4041236-7133848", null, "GET", null, null, null);    		
    	} finally {
        	IOUtil.closeQuietly(httpResponse);
    	}
    }
    
    private String getOAuthAccessToken()
    {
    	_logger.info("*** getOAuthAccessToken");
    	String oAuthAccessToken=null;
    	CloseableHttpResponse httpResponse = null;
    	try {
    		
    		StringBuilder payload = new StringBuilder();
    		HttpPost httpRequest = new HttpPost();
    		payload.append("grant_type=refresh_token");
    		payload.append("&refresh_token="+_refreshToken);
    		payload.append("&client_id="+_clientId);
    		payload.append("&client_secret="+_clientSecret);
    		payload.append("&scope="+_scope);
    		
    		httpRequest.setURI(new URI("https://api.amazon.com/auth/o2/token"));
    		httpRequest.addHeader("Accept", "application/json");

    		ByteArrayEntity reqEntity = new ByteArrayEntity(payload.toString().getBytes());
    		reqEntity.setContentType("application/x-www-form-urlencoded");
    		httpRequest.setEntity(reqEntity);
    		
    		httpResponse = _httpClient.execute(httpRequest);
    		
    		JSONObject jsonResponse = new JSONObject(new JSONTokener(httpResponse.getEntity().getContent()));
_logger.info("\r\ngetOAuthAccessToken: " + jsonResponse.toString(2));
        	if (jsonResponse.has("access_token"))
        		oAuthAccessToken=jsonResponse.getString("access_token");
        	else if (jsonResponse.has("error"))
        	{
        		throw new ConnectorException("Failure to fetch OAuth access token: "+jsonResponse.getString("error") + "-" + jsonResponse.getString("error_description"));
        	}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		throw new ConnectorException(e);
    	} finally {
			IOUtil.closeQuietly(httpResponse);
    	}
    	return oAuthAccessToken;
    }

    private String getTemporarySTSCredentials()
    {
    	_logger.info("*** getTemporarySTSCredentials");
    	CloseableHttpResponse httpResponse = null;
    	String stsAccessToken = null;
    	try {
    		
    		HttpGet httpRequest = new HttpGet();
    		String apiRole = this.getConnectionProperties().getProperty(CustomConnectionProperties.apiRole.name(),"SellingPartnerAPIRole");
    		String url = "https://sts.amazonaws.com?Action=AssumeRole&RoleArn=arn:aws:iam::"+_iamUser+":role/"+apiRole+"&RoleSessionName=SellingPartnerAPIRoleT&Version=2011-06-15";
    		httpRequest.setURI(new URI(url));
_logger.info("getTemporarySTSCredentials: " + url);    		
    		httpRequest.addHeader("Accept", "application/json");
    		Map<String,String> headers = new TreeMap<String,String>();
    		String stsRegion = this.getConnectionProperties().getProperty(CustomConnectionProperties.stsRegion.name(), _region);
    		insertSignatureHeaders(new URL(url), "GET", headers, "sts", "", stsRegion);
    		for (Entry<String, String> header:headers.entrySet())
    		{
    			httpRequest.addHeader(header.getKey(), header.getValue());
    		}
    		
    		httpResponse = _httpClient.execute(httpRequest);
    		
    		JSONObject stsResponse = new JSONObject(new JSONTokener(httpResponse.getEntity().getContent()));
//_logger.info("\n\ngetTemporarySTSCredentials: " + stsResponse.toString(2));
        	
			if (!stsResponse.has("AssumeRoleResponse"))
			{
				throw new ConnectorException("ERROR: AssumeRoleResponse not returned - " + stsResponse.toString());
			}
        	JSONObject credentials = stsResponse.getJSONObject("AssumeRoleResponse").getJSONObject("AssumeRoleResult").getJSONObject("Credentials");
        	this._accessKey=credentials.getString("AccessKeyId");
        	this._secretKey=credentials.getString("SecretAccessKey");
        	stsAccessToken = credentials.getString("SessionToken");
    	} catch (Exception e) {
    		throw new ConnectorException(e);
    	} finally {
			IOUtil.closeQuietly(httpResponse);
    	}
    	if (StringUtil.isBlank(stsAccessToken))
    		throw new ConnectorException("Failure to fetch Temporary STS Credential access token");
    	return stsAccessToken;
    }
    
    protected String createRestrictedDataToken(String method, boolean specificRDT) throws MalformedURLException
    {
    	_logger.info("*** createRestrictedDataToken");
    	String rdtToken = null;
    	String path = "/tokens/2021-03-01/restrictedDataToken";
    	String payloadTemplateTargetApplication = "{\r\n"
    			+ "  \"targetApplication\": \"%s\",\r\n"    			
    			+ "  \"restrictedResources\": [\r\n"
    			+ "    {\r\n"
    			+ "      \"method\": \"%s\",\r\n"
    			+ "      \"path\": \"%s\",\r\n"
    			+ "      \"dataElements\": [%s]\r\n"
    			+ "    }\r\n"
    			+ "  ]\r\n"
    			+ "}";
    	String payloadTemplate = "{\r\n"
    			+ "  \"restrictedResources\": [\r\n"
    			+ "    {\r\n"
    			+ "      \"method\": \"%s\",\r\n"
    			+ "      \"path\": \"%s\",\r\n"
    			+ "      \"dataElements\": [%s]\r\n"
    			+ "    }\r\n"
    			+ "  ]\r\n"
    			+ "}";
    	String resources = this.getOperationProperties().getProperty("restrictedDataElements");
    	String targetApplication = this.getOperationProperties().getProperty("targetApplication");
    	if (StringUtil.isBlank(resources))
    		throw new ConnectorException("Restricted Data Elements are required for Restricted Data Token authentication");
    	resources = "\""+resources.replace(" ", "").replace(",", "\",\"")+"\"";
    	String apiPath;
    	if (specificRDT)
    		apiPath = (new URL(this.getUrl())).getPath();
    	else
    		apiPath = this.getOpContext().getObjectTypeId();
    	
    	String payload = null;
    	if (StringUtil.isBlank(targetApplication))
    		payload = String.format(payloadTemplate, method.toUpperCase(), apiPath, resources);
    	else
    		payload = String.format(payloadTemplateTargetApplication, targetApplication, method.toUpperCase(), apiPath, resources);

    	CloseableHttpResponse httpResponse = null;
    	
    	try {
    		
    		HttpPost httpRequest = new HttpPost();
    		String url = this.getBaseUrl()+path;
_logger.info("\n\ncreateRestrictedDataToken\n" + payload);
_logger.info("getTemporarySTSCredentials:" + url);
    		httpRequest.setURI(new URI(url));
    		httpRequest.addHeader("Accept", "application/json");

    		ByteArrayEntity reqEntity = new ByteArrayEntity(payload.toString().getBytes());
    		reqEntity.setContentType("application/json");
    		httpRequest.setEntity(reqEntity);
    		
    		Map<String,String> headers = new HashMap<String,String>();
    		
    		this.insertSignatureHeaders(new URL(url), "POST", headers, "execute-api", payload, null);
    		for (String key : headers.keySet())
    		{
    			httpRequest.addHeader(key, headers.get(key));
    		}
    		httpResponse = _httpClient.execute(httpRequest);

//_logger.info(httpResponse.getStatusLine().getReasonPhrase());

			String responseString = SwaggerUtil.inputStreamToString(httpResponse.getEntity().getContent());
			
			try {
				JSONObject jsonResponse = new JSONObject(new JSONTokener(responseString));
//_logger.info("createRestrictedDataToken: " + jsonResponse.toString(2));
            	if (jsonResponse.has("restrictedDataToken"))
            		rdtToken=jsonResponse.getString("restrictedDataToken");
            	else 
            		throw new ConnectorException("ERROR: restrictedDataToken not returned - " + jsonResponse.toString());
			} catch (Exception e)
			{
				throw new ConnectorException("Failure to create restrictedDataToken: "+e.toString());
				//TODO Crappy API Returns array on error?
			}
			
			if (StringUtil.isBlank(_accessToken))
			{
				throw new ConnectorException("Failure to create restrictedDataToken: "+responseString);
			}
     	} catch (Exception e) {
    		e.printStackTrace();
    		throw new ConnectorException(e);
    	} finally {
			IOUtil.closeQuietly(httpResponse);
    	}	
    	return rdtToken;
    }
    
	protected void insertSignatureHeaders(URL url, String method, Map<String,String> headers, String service, String body, String overrideRegion)
	{
    	_logger.info("*** insertSignatureHeaders");
		String localRegion = this._region;
		if (StringUtil.isNotBlank(overrideRegion))
			localRegion = overrideRegion;
    	headers.put("x-amz-access-token", _accessToken);
    	if (StringUtil.isNotBlank(_rdtToken))
    		headers.put("x-amz-security-token", _rdtToken);
    	else if (StringUtil.isNotBlank(_stsSessionToken))
       		headers.put("x-amz-security-token", _stsSessionToken);


	    String signingAlgorithm = "AWS4-HMAC-SHA256";

		String host = url.getHost();
		String path = url.getPath();
		if (StringUtil.isBlank(path))
			path="/";
		String canonicalQueryString = "";
		if (url.getQuery()!=null)
		{
			String[] params=url.getQuery().split("&");
			ArrayList<String> list = new ArrayList<String>(Arrays.asList(params));
			Collections.sort(list);
			
			for (int i=0;i<list.size();i++)
			{
				String[] param = list.get(i).split("=");
				canonicalQueryString+=URLEncoder.encode(param[0])+"="+URLEncoder.encode(URLDecoder.decode(param[1]));
				if (i<list.size()-1)
					canonicalQueryString+="&";
			}
		}
//		_logger.info("url " + url.toString());
//		_logger.info("path " + path);
//		_logger.info("Query String " + canonicalQueryString);
		
		Date now = new Date();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
	    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
	    String amzdate = (sdf.format(now)).toString();
	    sdf = new SimpleDateFormat("yyyyMMdd");
	    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		String datestamp = (sdf.format(now)).toString();
		
	    String canonicalURI = path;
	    
	    String signedHeaders = null;
	    String contentType="application/x-www-form-urlencoded";
		if (!"get".contentEquals(method.toLowerCase()))
		{
			contentType="application/json; charset=UTF-8";
		}
	    String canonicalHeaders;

		String credentialScope = datestamp + "/" + localRegion + "/" + service + "/aws4_request";
//_logger.info("\n\n****credentialScope:\n"+credentialScope);	    

		signedHeaders = "host;x-amz-date";//;amz-sdk-invocation-id;amz-sdk-request;content-length;content-type";
		canonicalHeaders = "host:" + host + '\n' + "x-amz-date:" + amzdate + '\n';
		signedHeaders = "content-type;host;x-amz-date";
		canonicalHeaders = "content-type:"+contentType+"\nhost:" + host + '\n' + "x-amz-date:" + amzdate + '\n';

		String canonicalRequest;
	    
//_logger.info("Request Body: \r\n" + body);
	    byte[] hashedPayloadBytes = null;
	    try {
			hashedPayloadBytes = HmacSHA256(body.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e1) {
			throw new ConnectorException(e1);
		} 
	    
	    String hashedPayload = convertbyte(hashedPayloadBytes);
	    canonicalRequest = method + "\n" + canonicalURI + "\n" + canonicalQueryString + "\n" + canonicalHeaders + "\n" + signedHeaders + "\n" + hashedPayload;

//_logger.info("\n\n****canonicalHeaders:\n"+canonicalHeaders);	    
//_logger.info("\n\n****canonicalRequest:\n"+canonicalRequest);	    
	    // Calculate String to sign

	    byte[] hashedCanonicalRequestBytes;
		try {
			hashedCanonicalRequestBytes = HmacSHA256(canonicalRequest.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			throw new ConnectorException(e);
		}
	    String hashedCanonicalRequest = convertbyte(hashedCanonicalRequestBytes);
		String stringToSign = signingAlgorithm +"\n"+ amzdate + "\n" + credentialScope + "\n" + hashedCanonicalRequest;
//_logger.info("\n\n****stringToSign:\n"+stringToSign);	    

	    // Create a signing key.
	    byte[] signingKey;
		try {
			signingKey = getSignatureKey(_secretKey, datestamp, localRegion, service);
		} catch (Exception e) {
			throw new ConnectorException(e);
		}
	    // Use the signing key to sign the stringToSign using HMAC-SHA256 signing algorithm.
	    byte[] signatureBytes;
		try {
			signatureBytes = HmacSHA256(stringToSign, signingKey);
		} catch (Exception e) {
			throw new ConnectorException(e);
		}
	    String signature = convertbyte(signatureBytes);
//_logger.info("\n\n****signature:\n"+signature);	    
		
	    //Set authorization
		
	    String authorizationHeader = signingAlgorithm + " Credential=" + _accessKey+"/"+credentialScope+", SignedHeaders=" + signedHeaders+", Signature="+signature;
		headers.put("Authorization", authorizationHeader);
//_logger.info("\n\n****authorizationHeader:\n"+authorizationHeader+"\n|");	    
		headers.put("Content-Type", contentType);
	    headers.put("Host", host);
	    headers.put("X-Amz-Date", amzdate);
	    
//_logger.info("\n\nHeaders\n");
String debug="";
for(String header: headers.keySet())
{
	debug+="\n" + header + " : " + headers.get(header);
}
//_logger.info(debug);
	}
    
    private static byte[] HmacSHA256(byte[] key) throws NoSuchAlgorithmException {
        MessageDigest mac = MessageDigest.getInstance("SHA-256");
        byte[] signatureBytes = mac.digest(key);
        return signatureBytes;
    }
    
    private static String convertbyte(byte[] bytes) {
        StringBuffer hexString = new StringBuffer();
        for (int j=0; j<bytes.length; j++) {
            String hex=Integer.toHexString(0xff & bytes[j]);
            if(hex.length()==1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
    
    private static byte[] HmacSHA256(String data, byte[] key) throws Exception {
        String algorithm="HmacSHA256";
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        return mac.doFinal(data.getBytes("UTF-8"));
    }

    private static byte[] getSignatureKey(String key, String dateStamp, String regionName, String serviceName) throws Exception {
        byte[] kDate = HmacSHA256(dateStamp, ("AWS4" + key).getBytes("UTF-8"));
        byte[] kRegion = HmacSHA256(regionName, kDate);
        byte[] kService = HmacSHA256(serviceName, kRegion);
        byte[] kSigning = HmacSHA256("aws4_request", kService);
        return kSigning;
    }

}