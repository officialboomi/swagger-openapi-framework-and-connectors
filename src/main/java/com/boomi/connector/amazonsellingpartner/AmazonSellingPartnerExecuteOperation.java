package com.boomi.connector.amazonsellingpartner;

import java.net.HttpURLConnection;
import java.net.URL;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerExecuteOperation;
import com.boomi.util.StringUtil;

public class AmazonSellingPartnerExecuteOperation extends SwaggerExecuteOperation {

	public AmazonSellingPartnerExecuteOperation(SwaggerConnection conn) {
		super(conn);
	}
	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		if (AmazonSellingPartnerExecuteOperation.isReportDownloadOperation(this.getContext().getOperationType(), this.getContext().getCustomOperationType()))
		{
			for (ObjectData input : request) {
	            try {
	            	URL url = new URL(this.getUrl(input));
					HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();
					httpUrlConnection.setRequestMethod("GET");
					httpUrlConnection.setDoInput(true); //Input stream to get response
					httpUrlConnection.setDoOutput(false); //No data sent in request
					int applicationStatusCode = httpUrlConnection.getResponseCode();
					String applicationStatusMessage = httpUrlConnection.getResponseMessage();
					if (applicationStatusCode == 200) {
                        response.addResult(input, OperationStatus.SUCCESS, applicationStatusCode+"",
                        		applicationStatusMessage, ResponseUtil.toPayload(httpUrlConnection.getInputStream()));
					} 
					else {
						httpUrlConnection.getErrorStream();	
                        response.addResult(input, OperationStatus.FAILURE, applicationStatusCode+"",
                        		applicationStatusMessage, ResponseUtil.toPayload(httpUrlConnection.getErrorStream()));
					}
	            } catch (Exception e) {
	            	throw new ConnectorException(e);
	            } finally {
//	            	IOUtil.closeQuietly(null);
	            }
			}
		} else {
			super.executeUpdate(request, response);
		}

	}

	public String getUrl(ObjectData data) {
		String key = "url";
		String value = data.getDynamicOperationProperties().getProperty(key);
		if (StringUtil.isBlank(value))
			value = this.getContext().getOperationProperties().getProperty(key, "");
		return value;
	}
	
	public static boolean isReportDownloadOperation(OperationType operationType, String customOperationType)
	{
		return 	operationType==OperationType.EXECUTE && "downloadReport".contentEquals(customOperationType);
	}
}