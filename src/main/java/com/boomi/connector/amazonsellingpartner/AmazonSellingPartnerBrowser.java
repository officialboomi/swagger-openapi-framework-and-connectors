package com.boomi.connector.amazonsellingpartner;

import java.util.Collection;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;

public class AmazonSellingPartnerBrowser extends SwaggerBrowser  implements ConnectionTester {
    public AmazonSellingPartnerBrowser(SwaggerConnection conn) {
        super(conn);
    }

	//Check for pagination query parameters in the request schema. This allows for query operations that end with a path parameter
    //TODO there is no way to check if a query because we need to check for NextPage in the response schema...not supported...so use default way looking for lack of trailing {}
    //Concur has query operations that do not have offset as a parameter...but DOES have a nextpage response. Seems like a bug in their swagger
	@Override
	protected boolean isQueryOperation(String pathString, String methodName, JSONArray queryParameters, JSONObject methodObject)
	{
		if (!"get".contentEquals(methodName) || queryParameters==null)
			return false;
		for (int i=0; i<queryParameters.length(); i++)
		{	JSONObject parameter = (JSONObject)queryParameters.get(i);
			if (parameter.has("in"))
				if (parameter.getString("in").contentEquals("query"))
					if (parameter.has("name"))
					{
						if (this.isPaginationQueryParameter(parameter.getString("name")))
						{
							return true;
						}
					}
		}
		return false;
	}

	@Override
    public AmazonSellingPartnerConnection getConnection() {
        return (AmazonSellingPartnerConnection) super.getConnection();
    }
	
	
	//TODO amazon has some specs with limit/nextToken and others with MaxResultsPerPage/NextToken...
	@Override
	protected void getFilterSortSpecs(SwaggerAPIService swaggerService, List<FieldSpecField> fields) {
		JSONArray queryParams = swaggerService.getQueryParameters();
		for (int i=0; i<queryParams.length(); i++)
		{
			JSONObject param = queryParams.getJSONObject(i);
			if (param.has("name"))
			{
				String name = param.getString("name");
				//TODO need to use schema/type to get integer/epoch datetime type
				String type = "string";
				if (!this.isPaginationQueryParameter(name))
				{
					FieldSpecField filterable = new FieldSpecField().withName(name).withFilterable(true).withSortable(false).withSelectable(false).withType(type);				
					fields.add(filterable);						
				}
			}
		}
	}
	
	private boolean isPaginationQueryParameter(String name)
	{
		switch (name.toLowerCase())
		{
		case "nexttoken":
		case "maxresultsperpage":
		case "limit":
		case "pagetoken":
//		case "pagesize":
//		case "pagestartindex":
			this.logger.info(name);
			return true;
		}
		return false;
	}
	
	@Override
	protected boolean excludeQueryParameter(String name)
	{
		return false;
	}
	@Override
	public ObjectTypes getObjectTypes() 
	{
		if (AmazonSellingPartnerExecuteOperation.isReportDownloadOperation(this.getContext().getOperationType(), this.getContext().getCustomOperationType()))
		{
			ObjectTypes objectTypes = new ObjectTypes();
			ObjectType objectType = new ObjectType();
			objectType.setId("OBJECTID");
			objectType.setLabel("Download Report");
			objectTypes.getTypes().add(objectType);
			return objectTypes;
		} else {
			return super.getObjectTypes();
		}
	}

	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId,
			Collection<ObjectDefinitionRole> roles)		
	{
		if (AmazonSellingPartnerExecuteOperation.isReportDownloadOperation(this.getContext().getOperationType(), this.getContext().getCustomOperationType()))
		{
			ObjectDefinitions objDefinitions = new ObjectDefinitions();
			for(ObjectDefinitionRole role : roles)
			{
				ObjectDefinition objDefinition = new ObjectDefinition();
				if (ObjectDefinitionRole.INPUT == role)
				{
					objDefinition.setInputType(ContentType.NONE);
				} else {
					objDefinition.setOutputType(ContentType.BINARY);
				}
				objDefinitions.getDefinitions().add(objDefinition);
			}
			return objDefinitions;
		} else {
			return super.getObjectDefinitions(objectTypeId, roles);
		}
	}
}