package com.boomi.connector.avatax;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class AvataxConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new AvataxBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new AvataxQueryOperation(createConnection(context));
    }
    protected AvataxConnection createConnection(BrowseContext context) {
        return new AvataxConnection(context);
    }
}