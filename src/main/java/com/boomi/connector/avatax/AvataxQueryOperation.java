package com.boomi.connector.avatax;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.Expression;
import com.boomi.connector.api.GroupingExpression;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.util.CollectionUtil;

public class AvataxQueryOperation extends SwaggerQueryOperation {

	protected AvataxQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected String getSelectTermsQueryParam(List<String> selectedFields) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getPageSizeQueryParameterName()
	{
		return "$top";
	}	
	
	protected String getNextPageQueryParameterName() {
		return "$skip";
	}
	
	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
    	String sortTermsString="";

    	if (sortTerms!=null)
    	{		            
        	for (int x=0; x<sortTerms.size(); x++)
        	{
        		Sort sort = (Sort)sortTerms.get(x);
        		String sortTerm = sort.getProperty();
        		if (sortTerm != null && sortTerm.length()>0)
        		{
        			if (sortTermsString.length()!=0)
        			{
        				sortTermsString+=",";
        			}
        			sortTermsString += sortTerm;
        			if (sort.getSortOrder()!=null)
        				sortTermsString += " " + sort.getSortOrder();
        		}
        	}
    	}
    	if (sortTermsString.length()!=0)
    		sortTermsString = "$orderby=" + URLEncoder.encode(sortTermsString).replace("+", "%20");
    	return sortTermsString;
    }
	
	protected String getFilterTermsQueryParam(Expression baseExpr, int depth) throws IOException
	{
		String queryParameter="";

         // see if base expression is a single expression or a grouping expression
        if (baseExpr!=null)
        {
        	//A single operator, no AND/OR
            if(baseExpr instanceof SimpleExpression) {                
                // base expression is a single simple expression
            	queryParameter=(buildSimpleExpression((SimpleExpression)baseExpr));
            } else {

                // handle single level of grouped expressions
                GroupingExpression groupExpr = (GroupingExpression)baseExpr;

                // parse all the simple expressions in the group
                for(Expression nestedExpr : groupExpr.getNestedExpressions()) {
                    if(nestedExpr instanceof GroupingExpression) {
                        queryParameter+=getFilterTermsQueryParam(nestedExpr, depth+1);
                    } else {
                        String term=(buildSimpleExpression((SimpleExpression)nestedExpr));
                        if (term!=null && term.length()>0)
                        {
                            if (queryParameter.length()>0)
                            	queryParameter+=" " + groupExpr.getOperator().toString().toLowerCase() + " ";
                            queryParameter+=term;
                        }
                    }
                }
            }
        }       
        if (depth>0)
        	queryParameter = "(" + queryParameter + ")";
//        if (queryParameter.length()>0)
//        	queryParameter=URLEncoder.encode(queryParameter);
        return queryParameter.replace("+", "%20");
	}
    
    /**
     * Override to build a filter expression for the unique grammar of the API.
     * For example create=2021-01-01&createdCompare=lessThan.
     * The default behavior is simple ANDed expressions using the eq operation &email=j@email.com&active=true
     * @param simpleExpression the simple expression from which to construct the uri parameter and value
     * @return the URL query parameter and value for the filter compare expression
     * @throws IOException 
     */
	protected String buildSimpleExpression(SimpleExpression expr) {
        // this is the name of the queried object's property
    	String term="";
        String propName = expr.getProperty();
        String operator = expr.getOperator();
        
        if (propName==null || propName.length()==0)
        	throw new ConnectorException("Filter field parameter required");
        // we only support 1 argument operations
        if(CollectionUtil.size(expr.getArguments()) != 1) 
            throw new IllegalStateException("Unexpected number of arguments for operation " + expr.getOperator() + "; found " +
                                            CollectionUtil.size(expr.getArguments()) + ", expected 1");

        // this is the single operation argument
       	String parameter=expr.getArguments().get(0);
        if (parameter==null)
        	throw new ConnectorException(String.format("Filter parameter is required for field: %s ", propName));
        switch (operator)
        {
        case "startswith":
        case "endswith":
        	term = operator + "("+propName + "," + " " + parameter+") eq true"; //TODO do we really need eq true?
        	break;
        case "substring":
        	term = operator + "(" + parameter + "," + propName+") eq true";
        	break;
        default:
        	term = propName + " " + operator + " " + parameter+"";
        }

        return term;
    }


	@Override
	protected PaginationType getPaginationType() {
		// TODO Auto-generated method stub
		return PaginationType.PAGINATION_TYPE_RECORD_OFFSET;
	}
}