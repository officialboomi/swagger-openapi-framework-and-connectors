package com.boomi.connector.boomi_dcp;

import java.util.List;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.util.CollectionUtil;

public class BoomiDCPQueryOperation extends SwaggerQueryOperation {

	protected BoomiDCPQueryOperation(SwaggerConnection conn) {
		super(conn);
	}

	@Override
	protected String getNextPageQueryParameterName()
	{
		return "page";
	}

	@Override
	protected String getHasMoreElementPath() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getPageSizeQueryParameterName()
	{
		return "page_size";
	}
	// xpath supported: /links[rel='next']/href
	@Override
	protected String getNextPageElementPath()
	{
		return null;
	}
	
	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PaginationType getPaginationType() {
		return PaginationType.PAGINATION_TYPE_PAGE_OFFSET;
	}
	
	//Hack to force paginate parameter
//	@Override
//	protected String getSelectTermsQueryParam(List<String> selectedFields)
//	{
//		return "paginate=true";
//	}
}