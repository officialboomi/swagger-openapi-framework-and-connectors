package com.boomi.connector.googlebigquery;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.DynamicPropertyMap;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.googlebigquery.GoogleBigQueryBrowser.BQOperationTypes;
import com.boomi.swaggerframework.swaggeroperations.SwaggerExecuteOperation;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.ConnectionProperties;
import com.boomi.swaggerframework.swaggeroperations.SwaggerExecuteOperation.OperationProperties;
import com.boomi.swaggerframework.swaggerutil.SwaggerUtil;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;

public class GoogleBigQueryExecuteOperation extends SwaggerExecuteOperation{
	public enum BQOperationProperties{PROJECT_ID, DATASET_ID, BUCKET_NAME, OBJECT_NAME, WRITE_OPTION, TEMPTABLE_NAME, UPSERT_SQL, WRITE_DISPOSITION, FORMAT, CSV_DELIMETER, LOCATION}
	OperationType _operationType;
	String _customOperationType;
	PropertyMap _connProps;
	PropertyMap _opProps;
	String skipLeadingRows;
	long httpResponseCode;
	String httpResponseMessage;
	OperationStatus operationStatus;
	
	String tableId;
	String tempTable;
	String projectId;
	String datasetId;
	String location;
	String bucketObjectName;
	String bucketName = null;
	String bqUrl=this.getContext().getConnectionProperties().getProperty(ConnectionProperties.URL.name());
	String storageUrl="https://storage.googleapis.com/upload/storage/v1";
	
	public GoogleBigQueryExecuteOperation(GoogleBigQueryConnection conn) {
		super(conn);
		_operationType=this.getContext().getOperationType();//.getCustomOperationType();
		_customOperationType = this.getContext().getCustomOperationType();
		_connProps = getContext().getConnectionProperties();
		_opProps = getContext().getOperationProperties();
	}
        
	//Allow override with dynamic parameters
	private String getOperationProperty(String id, DynamicPropertyMap propMap)
	{
		String property = _opProps.getProperty(id);
		if (StringUtil.isBlank(property))
			property=propMap.getProperty(id);
		return property;
	}
	
	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		if (_operationType!=OperationType.EXECUTE || !BQOperationTypes.BATCH_LOAD.name().contentEquals(_customOperationType))	
		{
			super.executeUpdate(request, response);
			return;
		}
		JSONObject jsonResponse;
		String objectTypeId = getContext().getObjectTypeId();
		CloseableHttpResponse httpResponse = null;
		logger = response.getLogger();
		getConnection().setLogger(logger);

		projectId = _opProps.getProperty(BQOperationProperties.PROJECT_ID.name());
		datasetId = _opProps.getProperty(BQOperationProperties.DATASET_ID.name());
		location = _opProps.getProperty(BQOperationProperties.LOCATION.name());
		tableId = getContext().getObjectTypeId();
		tempTable = null;
		
		Payload payload;
		for (ObjectData input : request) {
            try {
        		bucketName = getOperationProperty(BQOperationProperties.BUCKET_NAME.name(), input.getDynamicOperationProperties());
        		bucketObjectName = getOperationProperty(BQOperationProperties.OBJECT_NAME.name(), input.getDynamicOperationProperties());
        		tempTable = getOperationProperty(BQOperationProperties.TEMPTABLE_NAME.name(), input.getDynamicOperationProperties());

        		String url=String.format(storageUrl+"/b/%s/o?uploadType=media&name=%s", bucketName, bucketObjectName);
 
                payload=null;
                try {

                    //Upload File
                	httpResponse = getConnection().doExecute(url, input.getData(), "POST");
                	payload = payloadFromHttpResponse(httpResponse);
                	if (httpResponseCode<300)
                	{
                		IOUtil.closeQuietly(payload);
                        payload = copyBucketToTable();
                        if (httpResponseCode<300)
                    	{
                        	if ("UPSERT".contentEquals(_opProps.getProperty("WRITE_OPTION","")))
                        	{
                        		IOUtil.closeQuietly(payload);
                        		payload = upsertFromTempToTable();
                        	}
                		}
                	}

                    response.addResult(input, operationStatus, httpResponseCode+" ",
                        		httpResponseMessage, payload);         		                     
                }
                finally {
//                    IOUtil.closeQuietly(httpResponse);
                }
            }
            catch (Exception e) {
            	logger.severe(e.getMessage());
                ResponseUtil.addExceptionFailure(response, input, e);
            }
        }
		return;
	}
	
	private Payload payloadFromHttpResponse(CloseableHttpResponse httpResponse) throws UnsupportedOperationException, IOException {
		Payload payload=null;
        operationStatus = OperationStatus.SUCCESS;
    	httpResponseCode = httpResponse.getStatusLine().getStatusCode();
    	httpResponseMessage = httpResponse.getStatusLine().getReasonPhrase();
    	if (httpResponseCode>=300)
    	{
    		operationStatus = OperationStatus.APPLICATION_ERROR;
    		if (httpResponse==null || httpResponse.getEntity()==null || httpResponse.getEntity().getContent()==null)
    		{
    			payload=ResponseUtil.toPayload("{\"error\":{\"code\":\""+httpResponseCode+"\",\"message\":\""+httpResponseMessage+"\"}}");
    		}
    	} else {
    		payload = PayloadUtil.toPayload(httpResponse.getEntity().getContent());
    	}
    	return payload;
	}

	private Payload upsertFromTempToTable() {
		//TODO don't we need to wait for load job to finish?
		String upsertSQLTemplate = "			{\r\n"
				+ "			  \"configuration\" : {\r\n"
				+ "			    \"query\" : {\r\n"
				+ "			      \"defaultDataset\" : {\r\n"
				+ "			        \"datasetId\" : \"%s\",\r\n"
				+ "			        \"projectId\" : \"%s\"\r\n"
				+ "			      },\r\n"
				+ "			      \"query\" : \"%s\",\r\n"
				+ "			      \"location\" : \"%s\",\r\n"
				+ "			      \"useLegacySql\" : false\r\n"
				+ "			    }\r\n"
				+ "			  }\r\n"
				+ "			}		\r\n"
				+ "";
		String upsertSQL = _opProps.getProperty(BQOperationProperties.UPSERT_SQL.name());

		String path = "/projects/"+projectId+"/jobs";
		String upsertRequest = String.format(upsertSQLTemplate,datasetId, projectId, upsertSQL, location);
		return executeJSONPost(bqUrl+path, upsertRequest);
	}

	private Payload copyBucketToTable() {
		String skipLeadingRows = "1";
		String fieldDelimiter;
		String format = _opProps.getProperty(BQOperationProperties.FORMAT.name(), "CSV");
		if (!"CSV".contentEquals(format))
		{
			fieldDelimiter="";
			skipLeadingRows = "null";
		} else {
			fieldDelimiter = _opProps.getProperty(BQOperationProperties.CSV_DELIMETER.name(), ",");
		}

		String writeDisposition = _opProps.getProperty(BQOperationProperties.WRITE_DISPOSITION.name(), "WRITE_TRUNCATE");
		// Write file from bucket to table		
        String tableName;            		
		if ("UPSERT".contentEquals(_opProps.getProperty("WRITE_OPTION","")))
			tableName=tempTable; //Execute RunJob/query
		else //INSERT
			tableName=tableId; // Execute RunJob Load

		String path = "/projects/"+projectId+"/jobs";
		String bucketTemplate = "		{\r\n"
				+ "			  \"configuration\" : {\r\n"
				+ "			    \"load\" : {\r\n"
				+ "			      \"allowQuotedNewlines\" : true,\r\n"
				+ "			      \"destinationTable\" : {\r\n"
				+ "			        \"datasetId\" : \"%s\",\r\n"
				+ "			        \"projectId\" : \"%s\",\r\n"
				+ "			        \"tableId\" : \"%s\"\r\n"
				+ "			      },\r\n"
				+ "			      \"fieldDelimiter\" : \"%s\",\r\n"
				+ "			      \"skipLeadingRows\" : %s,\r\n"
				+ "			      \"sourceFormat\" : \"%s\",\r\n"
				+ "			      \"sourceUris\" : [\r\n"
				+ "			        \"gs://%s/%s\"\r\n"
				+ "			      ],\r\n"
				+ "			      \"writeDisposition\" : \"%s\"\r\n"
				+ "			    }\r\n"
				+ "			  }\r\n"
				+ "			}\r\n"
				+ "";
		String uploadRequest = String.format(bucketTemplate, datasetId, projectId, tableName, fieldDelimiter, skipLeadingRows, format, bucketName, bucketObjectName, writeDisposition);
		return executeJSONPost(bqUrl+path, uploadRequest);
	}

	Payload executeJSONPost(String url, String jsonIn)
	{
		logger.info(jsonIn);
		Payload payload=null;
		JSONObject jsonResponse;
		CloseableHttpResponse httpResponse=null;
		try {
			httpResponse = getConnection().doExecute(url, new ByteArrayInputStream(jsonIn.getBytes()), "POST");
        	payload = payloadFromHttpResponse(httpResponse);
        	httpResponseCode = httpResponse.getStatusLine().getStatusCode();
        	httpResponseMessage = httpResponse.getStatusLine().getReasonPhrase();
			logger.info("httpResponseCode" + httpResponse.getStatusLine().getStatusCode() + " " + httpResponse.getStatusLine().getReasonPhrase());
        	if (httpResponseCode>=300)
        	{
        		operationStatus = OperationStatus.APPLICATION_ERROR;
        		if (httpResponse==null || httpResponse.getEntity()==null || httpResponse.getEntity().getContent()==null)
        		{
        			payload=ResponseUtil.toPayload("{\"error\":{\"code\":\""+httpResponseCode+"\",\"message\":\""+httpResponseMessage+"\"}}");
        			jsonResponse = new JSONObject(); 
        			logger.info(jsonResponse.toString(2));
        		}
        	} else {
        		payload = PayloadUtil.toPayload(httpResponse.getEntity().getContent());
        	}
		} catch (IOException | GeneralSecurityException e) {
			throw new ConnectorException(e);
		} finally {
//			if (httpResponse!=null)
//				IOUtil.closeQuietly(httpResponse);
		}		
		return payload;
	}
}
