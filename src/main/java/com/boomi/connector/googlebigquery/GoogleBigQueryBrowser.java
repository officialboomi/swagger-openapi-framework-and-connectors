package com.boomi.connector.googlebigquery;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.List;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ui.AllowedValue;
import com.boomi.connector.api.ui.BrowseField;
import com.boomi.connector.api.ui.DataType;
import com.boomi.connector.api.ui.DisplayType;
import com.boomi.connector.api.ui.ValueCondition;
import com.boomi.connector.api.ui.VisibilityCondition;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;

public class GoogleBigQueryBrowser extends SwaggerBrowser  implements ConnectionTester {
	public enum BQOperationTypes {BATCH_LOAD}
	public enum BQOperationFields {DATASET_ID, PROJECT_ID}
	OperationType _operationType;
	String _customOperationType="";
	String _projectId;
	String _datasetId;
	PropertyMap _opProps;
    public GoogleBigQueryBrowser(SwaggerConnection conn) {
        super(conn);
        _opProps=getContext().getOperationProperties();
		_operationType=this.getContext().getOperationType();//.getCustomOperationType();
		_customOperationType = this.getContext().getCustomOperationType();
		if (_customOperationType !=null && BQOperationTypes.BATCH_LOAD.name().contentEquals(_customOperationType))
		{
			_projectId = _opProps.getProperty(BQOperationFields.PROJECT_ID.name());
			if (StringUtil.isBlank(_projectId))
				throw new ConnectorException("A Project ID must be specified");
			_datasetId = _opProps.getProperty(BQOperationFields.DATASET_ID.name());
			if (StringUtil.isBlank(_datasetId))
				throw new ConnectorException("A Dataset ID must be specified");
		}
    }
    
    @Override
	public ObjectTypes getObjectTypes()
	{
		if (_customOperationType !=null && BQOperationTypes.BATCH_LOAD.name().contentEquals(_customOperationType))
		{
			ObjectTypes objectTypes=new ObjectTypes();
			String path = "/projects/"+_projectId+"/datasets/"+_datasetId+"/tables";
			CloseableHttpResponse response=null;
			try {
				response = getConnection().doExecute(path, null, "GET", null, null,null);
				//TODO we need to use JSONSplitter to paginate
				JSONObject responseObject=new JSONObject(new JSONTokener(response.getEntity().getContent()));
				logger.info(responseObject.toString(2));
				if (response.getStatusLine().getStatusCode()!=200)
					throw new ConnectorException(response.getStatusLine().getReasonPhrase());
				JSONArray tables = responseObject.getJSONArray("tables");
//				JSONObject schema = responseObject.getJSONObject("schema");
//				JSONArray fields = schema.getJSONArray("fields");
				for (int i=0; i<tables.length(); i++)
				{
					ObjectType objectType = new ObjectType();
					JSONObject table = tables.getJSONObject(i);
					JSONObject tableReference = table.getJSONObject("tableReference");
					objectType.setId(tableReference.getString("tableId"));
					if (table.has("friendlyName"))
						objectType.setLabel(table.getString("friendlyName"));
					else
						objectType.setLabel(tableReference.getString("tableId"));
					objectTypes.getTypes().add(objectType);
				}
			} catch (IOException | GeneralSecurityException e) {
				throw new ConnectorException(e);
			} finally {
				if (response!=null)
					IOUtil.closeQuietly(response);
			}
			return objectTypes;
		} else {
			return super.getObjectTypes();
		}		
	}
    
	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId,
			Collection<ObjectDefinitionRole> roles)		
	{
		ObjectDefinitions objectDefinitions = new ObjectDefinitions();
		if (_customOperationType !=null && BQOperationTypes.BATCH_LOAD.name().contentEquals(_customOperationType))
		{
			StringBuffer sbUpdateSet = new StringBuffer();
			StringBuffer sbInsert = new StringBuffer();
			for(ObjectDefinitionRole role : roles)
			{
				ObjectDefinition objDefinition = new ObjectDefinition();
				if (ObjectDefinitionRole.INPUT == role)
				{
					objDefinition.setInputType(ContentType.JSON);

					String path = "/projects/"+_projectId+"/datasets/"+_datasetId+"/tables/"+objectTypeId;
					CloseableHttpResponse response=null;
					try {
						objDefinition.setElementName("/"+objectTypeId); 							
						JSONObject schema = new JSONObject();
						schema.put("$schema", "http://json-schema.org/schema#");
						JSONObject root = new JSONObject();
						schema.put(objectTypeId, root);
						JSONObject properties = new JSONObject();
						root.put("type", "object");
						root.put("properties" , properties);
						
						response = getConnection().doExecute(path, null, "GET", null, null,null);
						//TODO we need to use JSONSplitter to paginate
						JSONObject responseObject=new JSONObject(new JSONTokener(response.getEntity().getContent()));
						logger.info(responseObject.toString(2));
						if (response.getStatusLine().getStatusCode()!=200)
							throw new ConnectorException(response.getStatusLine().getReasonPhrase());
						JSONObject schemaObject = responseObject.getJSONObject("schema");
						JSONArray fields = schemaObject.getJSONArray("fields");
						for (int i=0; i<fields.length(); i++)
						{
							JSONObject field = fields.getJSONObject(i);
							String fieldName = field.getString("name");
							if (i>0)
							{
								sbUpdateSet.append(", ");
								sbInsert.append(", ");
							}
							sbUpdateSet.append(String.format("%s=D.%s", fieldName, fieldName));
							sbInsert.append(fieldName);

							JSONObject property = new JSONObject();
							
							String type = field.getString("type").toLowerCase();
							switch (field.getString("type")) {
							case "FLOAT" :
							case "NUMERIC" :
								type="number";
								break;
							case "BOOLEAN" :
								type="boolean";
								break;
							default:
								type="string";								
							}
							if ("numeric".contentEquals(type))
								type="number";
							property.put("type", type);
							properties.put(fieldName, property);				
						}
						objDefinition.setJsonSchema(schema.toString());
						logger.info(schema.toString());
					} catch (IOException | GeneralSecurityException e) {
						throw new ConnectorException(e);
					} finally {
						if (response!=null)
							IOUtil.closeQuietly(response);
					}
					//TODO set visibility based on UPSERT? New sdk version?

				} else {
					objDefinition.setOutputType(ContentType.NONE);
				}
				objectDefinitions.getDefinitions().add(objDefinition);													
				BrowseField browseField = null;
				browseField = createBrowseField(GoogleBigQueryExecuteOperation.BQOperationProperties.PROJECT_ID.name(), "Project ID", "The Google project ID", _projectId, false);
				objectDefinitions.getOperationFields().add(browseField);
				browseField = createBrowseField(GoogleBigQueryExecuteOperation.BQOperationProperties.DATASET_ID.name(), "Dataset ID", "The Google Dataset ID", _datasetId, false);
				objectDefinitions.getOperationFields().add(browseField);
				String upsertSQL = buildUpsertSQL(objectTypeId, sbUpdateSet.toString(), sbInsert.toString());
				browseField = createBrowseField(GoogleBigQueryExecuteOperation.BQOperationProperties.UPSERT_SQL.name(), "Upsert SQL", "SQL to execute the merge", upsertSQL, true);
				VisibilityCondition visibilityCondition = new VisibilityCondition();
				ValueCondition valueCondition = new ValueCondition();
				valueCondition.setFieldId(GoogleBigQueryExecuteOperation.BQOperationProperties.WRITE_OPTION.name());
				valueCondition.getValues().add("UPSERT");
				visibilityCondition.getValueConditions().add(valueCondition);
				browseField.getVisibilityConditions().add(visibilityCondition);
				objectDefinitions.getOperationFields().add(browseField);
			}			
		} else {
			objectDefinitions =  super.getObjectDefinitions(objectTypeId, roles);
		}
		return objectDefinitions;
		//TODO if Upsert, generate dynamic field with the SQL statement (ideally read only)
	}
	
	@Override
	protected String getQueryPaginationSplitPath(String objectTypeId)
	{
		if (_operationType==OperationType.QUERY && "/projects/{projectId}/queries/{jobId}".contentEquals(objectTypeId))
		{
			return "/rows/*";
		}
		return null;
	}

	//Check for pagination query parameters in the request schema. This allows for query operations that end with a path parameter
    //TODO there is no way to check if a query because we need to check for NextPage in the response schema...not supported...so use default way looking for lack of trailing {}
    //Concur has query operations that do not have offset as a parameter...but DOES have a nextpage response. Seems like a bug in their swagger
	@Override
	protected boolean isQueryOperation(String pathString, String methodName, JSONArray queryParameters, JSONObject methodObject)
	{
		if (!"get".contentEquals(methodName) || queryParameters==null)
			return false;
		for (int i=0; i<queryParameters.length(); i++)
		{	JSONObject parameter = (JSONObject)queryParameters.get(i);
			if (parameter.has("in"))
				if (parameter.getString("in").contentEquals("query"))
					if (parameter.has("name"))
						if (parameter.getString("name").contentEquals("pageToken"))
							return true;
		}
		return false;
	}

	@Override
    public GoogleBigQueryConnection getConnection() {
        return (GoogleBigQueryConnection) super.getConnection();
    }
	
	@Override
	protected void getFilterSortSpecs(SwaggerAPIService swaggerService, List<FieldSpecField> fields) {
		JSONArray queryParams = swaggerService.getQueryParameters();
		for (int i=0; i<queryParams.length(); i++)
		{
			JSONObject param = queryParams.getJSONObject(i);
			if (param.has("name"))
			{
				String name = param.getString("name");
				//TODO need to use schema/type to get integer/epoch datetime type
				String type = "string";
				switch (name)
				{
				case "pageToken":
				case "oauth_token":
				case "userIp":
				case "prettyPrint":
				case "maxResults":
					break;
				default:
					FieldSpecField filterable = new FieldSpecField().withName(name).withFilterable(true).withSortable(false).withSelectable(false).withType(type);				
					fields.add(filterable);						
				}
			}
		}
	}
	
	private static BrowseField createBrowseField(String id, String parameterName, String description, String defaultValue, boolean doTextArea) {
	   BrowseField simpleField = new BrowseField();
	   simpleField.setId(id);
	   // User Friendly Label, defaults to ID if not given.
	   simpleField.setLabel(parameterName);
	   simpleField.setOverrideable(true);
	   // Mandatory to set a DataType. This Data Type can also be String, Boolean, Integer, Password
	   simpleField.setType(DataType.STRING);
	   simpleField.setDefaultValue(defaultValue);
	   simpleField.setHelpText(description);
	   if (doTextArea)
		   simpleField.setDisplayType(DisplayType.TEXTAREA);
	   // Optional Default Value for String Field
//		   simpleField.setDefaultValue(servicePath);
	   return simpleField;
	}
	
	private static String buildUpsertSQL(String objectTypeId, String updateSet, String insert)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("MERGE %s T USING %s_tmp D ON T.id = D.id \r\nWHEN MATCHED THEN UPDATE SET %s \r\n", objectTypeId, objectTypeId, updateSet));
		sb.append(String.format(" WHEN NOT MATCHED THEN \r\n INSERT(%s)\r\n VALUES(%s)", insert, insert));
		
		return sb.toString();
	}
}