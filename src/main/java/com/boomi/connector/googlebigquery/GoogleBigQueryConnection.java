package com.boomi.connector.googlebigquery;

import java.io.InputStream;
import java.util.Map;
import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.AuthType;


public class GoogleBigQueryConnection extends SwaggerConnection {
    
    public enum BQConnectionProperties {API_KEY}
    
	public GoogleBigQueryConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    protected String getSpec()
    {
    	return "resources/googlebigquery/openapi.json";
    }    
    
    @Override
    protected AuthType getAuthenticationType()
    {
    	return AuthType.OAUTH;
    }
    
    @Override
    protected String getTestConnectionUrl()
    {
    	return this.getBaseUrl()+"/projects";
    }
}