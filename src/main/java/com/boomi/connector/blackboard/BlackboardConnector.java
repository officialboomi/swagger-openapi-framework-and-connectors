package com.boomi.connector.blackboard;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class BlackboardConnector extends SwaggerConnector {
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new BlackboardBrowser(createConnection(context));
    }    

    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new BlackboardQueryOperation(createConnection(context));
    }
   
    protected BlackboardConnection createConnection(BrowseContext context) {
        return new BlackboardConnection(context);
    }
}