package com.boomi.connector.personio;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.logging.Logger;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.PropertyMap;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;


public class PersonioConnection extends SwaggerConnection {
	public enum CustomConnectionProperties {CLIENT_ID, CLIENT_SECRET}
        
	public PersonioConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    public AuthType getAuthenticationType()
    {
    	return AuthType.CUSTOM;
    }

    @Override
    protected String getSpec()
    {
    	return "resources/personio/openapi.json";
    }    
    
    protected String getCustomAuthCredentials() throws JSONException, IOException, GeneralSecurityException, UnsupportedOperationException 
    {
    	InputStream is = null;
    	CloseableHttpResponse httpResponse = null;
    	String originalMethod = this.getHttpMethod();

    	try {
    		PropertyMap connProps = this.getConnectionProperties();
    		String clientId = connProps.getProperty(CustomConnectionProperties.CLIENT_ID.name());
    		if (StringUtil.isBlank(clientId))
    			throw new ConnectorException("Client ID is required");
    		String clientSecret = connProps.getProperty(CustomConnectionProperties.CLIENT_SECRET.name());
    		if (StringUtil.isBlank(clientSecret))
    			throw new ConnectorException("Client Secret is required");
    		
        	String path = String.format("/auth?client_id=%s&client_secret=%s", clientId, clientSecret);
        	httpResponse = this.doExecuteWithoutAuthentication(this.getBaseUrl()+path, new ByteArrayInputStream("".getBytes()), "POST");
        	JSONObject response = new JSONObject(new JSONTokener(httpResponse.getEntity().getContent()));
        	String header="Bearer ";
        	if (response.has("data") && response.getJSONObject("data").has("token"))
        	{
        		header += response.getJSONObject("data").getString("token");
        	} else {
        		throw new ConnectorException("401 invalid Client ID/Client Secret");
        	}
        	this.setHttpMethod(originalMethod);
        	return header;
    	} finally {
    		if (is!=null)
    			IOUtil.closeQuietly(is);
    		if (httpResponse!=null)
    			httpResponse.close();
    	}
    }
}