package com.boomi.connector.okta;

import java.util.List;

import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;

public class OktaQueryOperation extends SwaggerQueryOperation {

	protected OktaQueryOperation(SwaggerConnection conn) {
		super(conn);
	}

	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override  
	protected String getNextPageQueryParameterName()
	{
		return "after";
	}
	
	@Override
	protected PaginationType getPaginationType() {
		return PaginationType.PAGINATION_TYPE_LINK_HEADER;
	}
}