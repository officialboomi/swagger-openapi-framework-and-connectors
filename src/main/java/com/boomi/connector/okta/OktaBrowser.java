package com.boomi.connector.okta;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;
import com.boomi.util.StringUtil;

public class OktaBrowser extends SwaggerBrowser  implements ConnectionTester {
	boolean isQuery=false;
	boolean isPaginated=false;
	String filters="";
	boolean hasExpand;
    public OktaBrowser(SwaggerConnection conn) {
        super(conn);
    }
    
    @Override
    protected String getObjectTypeLabel(String operationId, String summary, String description, List<String> tags)
    {
    	String label=tags.get(0) + ": ";
    	if (StringUtil.isNotBlank(summary) && !"Success".contentEquals(summary))
    		label+=summary;
    	else
    		label+=operationId;
    	if (isQuery && !isPaginated)
    		label+=" (Not Paginated)";
    	return label;
    }

    @Override
    protected String getObjectTypeHelpText(String operationId, String summary, String description, List<String> tags)
    {
    	String helpText="";
    	if (StringUtil.isNotBlank(description) && !"Success".contentEquals(description))
    		helpText=description;
    	if (isQuery)
    	{
    		if (!isPaginated)
    			helpText+=" (Not Paginated)";
    		if (filters.length()>0)
    			helpText += "\n\nFilters: " + filters;
    		if (hasExpand)
    			helpText += "\n\nSupports expand";
    	}

    	return helpText;
    }

    @Override
    protected boolean isPaginatedQuery(JSONArray queryParameters)
    {
    	return hasQueryParameter(queryParameters, "after");
    }

	//Check for pagination query parameters in the request schema. This allows for query operations that end with a path parameter
	@Override
	protected boolean isQueryOperation(String pathString, String methodName, JSONArray queryParameters, JSONObject methodObject)
	{
		isQuery=false;
		isPaginated=false;
		hasExpand=false;
		filters="";
		if (!"get".contentEquals(methodName) || queryParameters==null)
			isQuery = false;
		else if(isPaginatedQuery(queryParameters))
		{
			isQuery=true;
			isPaginated=true;
		} else {
			JSONObject schema = methodObject.getJSONObject("responses").getJSONObject("200").getJSONObject("schema");
			if (schema.has("type") && "array".contentEquals(schema.getString("type")))
				isQuery=true; //array with no pagination
		}
		if (isQuery)
		{
			hasExpand = hasQueryParameter(queryParameters, "expand");
			if (hasQueryParameter(queryParameters, "q"))
				filters+="q= ";
			if (hasQueryParameter(queryParameters, "search"))
				filters+="search= ";
			if (hasQueryParameter(queryParameters, "filter"))
				filters+="filter= ";
		}
		return isQuery;
	}

	@Override
    public OktaConnection getConnection() {
        return (OktaConnection) super.getConnection();
    }
	
	//TODO handle filters
	//TODO can you mix q,search,filter? Do they all paginate?

	@Override
	protected void getFilterSortSpecs(SwaggerAPIService swaggerService, List<FieldSpecField> fields) {
		JSONArray queryParams = swaggerService.getQueryParameters();
		for (int i=0; i<queryParams.length(); i++)
		{
			JSONObject param = queryParams.getJSONObject(i);
			if (param.has("name"))
			{
				String name = param.getString("name");
				//TODO need to use schema/type to get integer/epoch datetime type
				String type = param.getString("type");
				switch (name)
				{
				case "page":
				case "page_size":
					break;
				default:
					FieldSpecField filterable = new FieldSpecField().withName(name).withFilterable(true).withSortable(false).withSelectable(false).withType(type);				
					fields.add(filterable);						
				}
			}
		}
	}
}