package com.boomi.connector.okta;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class OktaConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public OktaConnection(BrowseContext context) {
		super(context);
	}
	
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }
    
    @Override
    protected String getSpec()
    {
    	return "resources/okta/swagger.json";
    }  
    
    @Override
	protected String getCustomAuthCredentials()
    {
    	return this.getPassword();
    }
}