package com.boomi.connector.aquarius;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class AquariusConnector extends SwaggerConnector {
   
    protected AquariusConnection createConnection(BrowseContext context) {
        return new AquariusConnection(context);
    }
}