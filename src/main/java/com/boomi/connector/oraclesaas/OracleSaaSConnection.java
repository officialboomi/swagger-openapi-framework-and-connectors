package com.boomi.connector.oraclesaas;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;

public class OracleSaaSConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public OracleSaaSConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return "/items/*";
    }
    
    @Override
    protected String getTestConnectionUrl()
    {
    	return this.getBaseUrl() + "/fscmRestApi/resources/11.13.18.05/invoices";
    }
}