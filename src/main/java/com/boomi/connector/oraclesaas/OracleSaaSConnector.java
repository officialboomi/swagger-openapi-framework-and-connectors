package com.boomi.connector.oraclesaas;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class OracleSaaSConnector extends SwaggerConnector {

    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new OracleSaaSQueryOperation(createConnection(context));
    }

    @Override
    protected OracleSaaSConnection createConnection(BrowseContext context) {
        return new OracleSaaSConnection(context);
    }
}