package com.boomi.connector.workday;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class WorkdayConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public WorkdayConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return "/data/*";
    }
    
    @Override
    public AuthType getAuthenticationType()
    {
    	return AuthType.OAUTH;
    }
    
    //For WD we need to append the tenent ID from the connection parameters
    @Override
    protected String getBasePath()
    {
    	String tenantID=this.getConnectionProperties().getProperty("TENANTID");
    	if (tenantID==null || tenantID.trim().length()==0)
    		throw new ConnectorException("Workday Tenant ID must be specified");
    	return super.getBasePath()+"/"+tenantID;
    }    

}