package com.boomi.connector.sage_people;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class SageConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public SageConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    protected String getSpec()
    {
    	return "resources/sage_people/sage-people-api.json";
    }    
    
    @Override
    protected AuthType getAuthenticationType()
    {
    	return AuthType.OAUTH;
    }
}