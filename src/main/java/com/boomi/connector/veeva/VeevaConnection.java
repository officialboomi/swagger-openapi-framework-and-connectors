package com.boomi.connector.veeva;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.concurrent.ConcurrentMap;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.execution.ExecutionManager;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;


public class VeevaConnection extends SwaggerConnection {
        
	String tmpContentType;
	public VeevaConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    public AuthType getAuthenticationType()
    {
    	return AuthType.CUSTOM;
    }

    @Override
	/**
	 * Provide a custom Authorization header for AuthType.CUSTOM implementations.
	 * Useful for proprietary API driven authentication. 
	 * @return the header value ie. Bearer xxxxxxxxxxxxxxxxxxxxxxxxxx
	*/
    protected String getCustomAuthCredentials() throws JSONException, IOException, GeneralSecurityException
    {
    	String httpMethod = this.getHttpMethod(); //KLUDGE if we let the subclass call doExecute, we have to preserve the method so it doesn't get stomped...GOTTA fix this
    	InputStream is = null;
    	CloseableHttpResponse httpResponse = null;
    	try {
    		String sessionId = null;// DISABLE CACHING getCachedToken();
    		if (StringUtil.isBlank(sessionId))
    		{
    			String creds = String.format("{\"username\":\"%s\",\"password\":\"%s\"}", getUsername(),getPassword());
             	is = new ByteArrayInputStream(creds.getBytes());
             	tmpContentType="application/x-www-form-urlencoded";
            	httpResponse = this.doExecuteWithoutAuthentication(this.getBaseUrl()+"/auth", is, "POST");
             	tmpContentType=null;
            	JSONObject response = new JSONObject(new JSONTokener(httpResponse.getEntity().getContent()));
                if (!"SUCCESS".contentEquals(response.getString("responseStatus")))
                	throw new ConnectorException(response.toString());
            	if (response.has("sessionId"))
            	{
            		sessionId = response.getString("sessionId");
            		this.getContext().getConnectorCache().put(this.getCacheKey(), sessionId);
            	} else {
            		throw new ConnectorException("Authentication Unsuccessful");
            	}
            	this.setHttpMethod(httpMethod);
    		}
        	return sessionId;
     	} finally {
    		if (is!=null)
    			IOUtil.closeQuietly(is);
    		if (httpResponse!=null)
    			httpResponse.close();
    	}
    }    

    //HATE THIS KLUDGE to override content type for login...
    @Override
    protected String getRequestContentType()
    {
    	if (StringUtil.isBlank(tmpContentType))
    		return super.getRequestContentType();
    	return tmpContentType;
    }
 
    @Override
    protected String getSpec()
    {
    	return "resources/veeva/openapi.json";
    }    
    
	private String getCachedToken()
	{
		String cacheKey = this.getCacheKey();
		ConcurrentMap<Object, Object> cache = this.getContext().getConnectorCache();
		if (cache==null)
		{
			logger.warning("Connector Cache is NULL");
			return null;
		}
		String token = (String) cache.get(cacheKey);
		if (token!=null)
		{
			logger.info("Cached Token Found");
		}
		return token;
	}
    
	private String getCacheKey()
	{
		String executionId=null;
   		if (ExecutionManager.getCurrent()!=null)
			executionId=ExecutionManager.getCurrent().getExecutionId();
		String cacheKey = "___CACHED_TOKEN_"+this.getBaseUrl()+"_"+this.getUsername()+"_"+this.getPassword()+"_"+executionId;
//		logger.info("cacheKey: " + cacheKey);
		return cacheKey;
	}

}