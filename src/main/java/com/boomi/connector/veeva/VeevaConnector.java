package com.boomi.connector.veeva;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class VeevaConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new VeevaBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new VeevaQueryOperation(createConnection(context));
    }
    protected VeevaConnection createConnection(BrowseContext context) {
        return new VeevaConnection(context);
    }
}