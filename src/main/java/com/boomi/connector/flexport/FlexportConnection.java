package com.boomi.connector.flexport;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;

public class FlexportConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public FlexportConnection(BrowseContext context) {
		super(context);
	}
	      
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }
    
    @Override
    public List<String> getExcludedObjectTypeIDs()
    {
    	return Arrays.asList();
    }
}