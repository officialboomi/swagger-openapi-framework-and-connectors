Browser
	has sort asc/desc "direction" query param
	Sort fields an enum in "sort" query param
	f.* query parms are filter fields (EQ only)
	f.<date>.gt etc are date compare capabilities

Query
	derive derive data compare query param name from operation specified and date field prefix
	"per" is the page size
	"page" is the page offset...this is a page number NOT a record number
	Need to set up the pagination item path /response/value/data/data*
	/response/value/data/next is null if no more. This element can be used as a next url to handle pagination and not deal with offset calculation