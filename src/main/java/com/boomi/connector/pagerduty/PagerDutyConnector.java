package com.boomi.connector.pagerduty;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class PagerDutyConnector extends SwaggerConnector {
   
    protected PagerDutyConnection createConnection(BrowseContext context) {
        return new PagerDutyConnection(context);
    }
}