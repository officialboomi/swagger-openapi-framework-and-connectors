package com.boomi.connector.coupa;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class CoupaConnection extends SwaggerConnection {
        
	public CoupaConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return "/*";
    }

    @Override
    protected AuthType getAuthenticationType()
    {
    	return AuthType.OAUTH;
    }

    @Override
    protected String getSpec()
    {
    	return "resources/coupa/openapi_r32.json";
    }    
}