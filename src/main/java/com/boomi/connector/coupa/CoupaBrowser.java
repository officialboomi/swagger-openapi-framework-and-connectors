package com.boomi.connector.coupa;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;

public class CoupaBrowser extends SwaggerBrowser  implements ConnectionTester {
    public CoupaBrowser(SwaggerConnection conn) {
        super(conn);
    }

//TODO Coupa has pagination even for GET operations! WTF so we just need to go with the default terminating } rule
    
//	@Override
//	protected boolean isQueryOperation(String pathString, String methodName, JSONArray queryParameters, JSONObject methodObject)
//	{
//		if (!"get".contentEquals(methodName) || queryParameters==null)
//			return false;
//		for (int i=0; i<queryParameters.length(); i++)
//		{	JSONObject parameter = (JSONObject)queryParameters.get(i);
//			if (parameter.has("in"))
//				if (parameter.getString("in").contentEquals("query"))
//					if (parameter.has("name"))
//						if (parameter.getString("name").contentEquals("offset"))
//							return true;
//		}
//		return false;
//	}

	@Override
    public CoupaConnection getConnection() {
        return (CoupaConnection) super.getConnection();
    }
	
	@Override
	protected void getFilterSortSpecs(SwaggerAPIService swaggerService, List<FieldSpecField> fields) {
		JSONArray queryParams = swaggerService.getQueryParameters();
		for (int i=0; i<queryParams.length(); i++)
		{
			JSONObject param = queryParams.getJSONObject(i);
			if (param.has("name"))
			{
				String name = param.getString("name");
				//TODO need to use schema/type to get integer/epoch datetime type
				String type = "string";
				switch (name)
				{
				case "dir":
				case "order_by":
				case "filter":
				case "return_object":
				case "offset":
				case "limit":
					break;
				default:
					FieldSpecField filterable = new FieldSpecField().withName(name).withFilterable(true).withSortable(false).withSelectable(false).withType(type);				
					fields.add(filterable);						
				}
			}
		}
	}
}