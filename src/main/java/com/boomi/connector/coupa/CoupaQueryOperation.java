package com.boomi.connector.coupa;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.Expression;
import com.boomi.connector.api.GroupingExpression;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.util.CollectionUtil;
import com.boomi.util.StringUtil;

public class CoupaQueryOperation extends SwaggerQueryOperation {

	protected CoupaQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected String getSelectTermsQueryParam(List<String> selectedFields) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
		String terms="";
		//Just a single term supported so you will loop once
		for (Sort term : sortTerms)
		{
			terms="order_by="+term.getProperty()+"&dir="+term.getSortOrder();
		}
		return terms;
	}

	@Override
	protected PaginationType getPaginationType() {
		// TODO Auto-generated method stub
		return PaginationType.PAGINATION_TYPE_RECORD_OFFSET;
	}
	
	//This default behavior is to build terms as individual query parameters.
	//AND is assumed
    /**
     * Build the Filter path parameter from the filters in the Query Operation UI
     * Defaults to setting individual path parameters for each simple expression with each parameter a term in a top level AND expression
     * @param queryFilter the filter expression specified by the user in the Query Operation Filter UI
     * @return the field URI parameter for the filter
     * @throws IOException 
     */
	protected String getFilterTermsQueryParam(Expression baseExpr, int depth) throws IOException
	{
		String queryParameter="";

         // see if base expression is a single expression or a grouping expression
        if (baseExpr!=null)
        {
        	//A single operator, no AND/OR
            if(baseExpr instanceof SimpleExpression) {                
                // base expression is a single simple expression
            	queryParameter=(buildSimpleExpression((SimpleExpression)baseExpr));
            } else {

                // handle single level of grouped expressions
                GroupingExpression groupExpr = (GroupingExpression)baseExpr;

                // parse all the simple expressions in the group
                for(Expression nestedExpr : groupExpr.getNestedExpressions()) {
                    if(nestedExpr instanceof GroupingExpression) {
                        queryParameter+=getFilterTermsQueryParam(nestedExpr, depth+1);
                    } else {
                        String term=(buildSimpleExpression((SimpleExpression)nestedExpr));
                        if (term!=null && term.length()>0)
                        {
//TODO Some APIs just use a single query param per ANDed expression, others use an AND or an OR between expressions      
//TODO How to handle both?                        	
                            if (StringUtil.isBlank(this.getFilterQueryParameter()))
                            {
                                if (queryParameter.length()>0)
                                	queryParameter+="&";
                            } else {
                                if (queryParameter.length()>0)
                                	queryParameter+="%20" + groupExpr.getOperator().toString().toLowerCase() + "%20";
                            }
                            queryParameter+=term;
                        }
                    }
                }
            }
        }       
        if (depth>0)
        	queryParameter = "(" + queryParameter + ")";
//        if (queryParameter.length()>0)
//        	queryParameter=URLEncoder.encode(queryParameter);
        return queryParameter;//.replace("+", "%20");
	}
       
    /**
     * Override to build a filter expression for the unique grammar of the API.
     * For example create=2021-01-01&createdCompare=lessThan.
     * The default behavior is simple ANDed expressions using the eq operation &email=j@email.com&active=true
     * @param simpleExpression the simple expression from which to construct the uri parameter and value
     * @return the URL query parameter and value for the filter compare expression
     */
	protected String buildSimpleExpression(SimpleExpression expr) {
        // this is the name of the queried object's property
    	String term="";
        String propName = expr.getProperty();
        String operator = expr.getOperator();
        
        if (StringUtil.isBlank(propName))
        	throw new ConnectorException("Filter field parameter required");
        // we only support 1 argument operations
        if (CollectionUtil.size(expr.getArguments()) != 1) 
            throw new IllegalStateException("Unexpected number of arguments for operation " + expr.getOperator() + "; found " +
                                            CollectionUtil.size(expr.getArguments()) + ", expected 1");

        // this is the single operation argument
       	String parameter=expr.getArguments().get(0);
        if (parameter==null || parameter.length()==0)
        	throw new ConnectorException(String.format("Filter parameter is required for field: %s ", propName));

        term=propName;
        switch (operator)
        {
	        case "eq":
	        	break;
	        default:
	        	term+="["+operator+"]";
        }
        term+="=" + URLEncoder.encode(parameter);
        return term;
    }

}