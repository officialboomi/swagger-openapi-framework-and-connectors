package com.boomi.connector.coupa;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class CoupaConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new CoupaBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new CoupaQueryOperation(createConnection(context));
    }
    protected CoupaConnection createConnection(BrowseContext context) {
        return new CoupaConnection(context);
    }
}