package com.boomi.connector.experian;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;

public class ExperianConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public ExperianConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return "/data/*";
    }
    
    @Override
    public List<String> getExcludedObjectTypeIDs()
    {
    	return Arrays.asList();
    }
}