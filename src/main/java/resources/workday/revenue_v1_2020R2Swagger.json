
{
  "swagger" : "2.0",
  "info" : {
    "description" : "The revenue Workday API.",
    "version" : "v1",
    "title" : "Revenue"
  },
  "host" : "<tenant hostname>",
  "basePath" : "/revenue/v1",
  "tags" : [ {
    "name" : "billableTransactions",
    "description" : "All Billable Transactions not invoiced in Workday."
  } ],
  "schemes" : [ "https" ],
  "paths" : {
    "/billableTransactions" : {
      "get" : {
        "tags" : [ "billableTransactions" ],
        "description" : "Retrieves a collection of billable transactions.\n\nSecured by: Manage: Project Details, Process: Project Billing\n\nScope: Project Billing",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "billingStatus",
          "in" : "query",
          "description" : "The current billing status of the transaction. (Awaiting Review, Do Not Bill, Hold, Ready to Bill, Invoiced)",
          "required" : false,
          "type" : "array",
          "items" : {
            "type" : "string"
          },
          "collectionFormat" : "multi"
        }, {
          "name" : "worker",
          "in" : "query",
          "description" : "Related ~worker~ tied to this billable transaction (i.e. ~worker~ on the expense report or the ~worker~ the time sheet).",
          "required" : false,
          "type" : "array",
          "items" : {
            "type" : "string"
          },
          "collectionFormat" : "multi"
        }, {
          "name" : "spendCategory",
          "in" : "query",
          "description" : "If the billable transaction is tied to an expense report line, then this field returns the spend category for that line.",
          "required" : false,
          "type" : "array",
          "items" : {
            "type" : "string"
          },
          "collectionFormat" : "multi"
        }, {
          "name" : "timeCode",
          "in" : "query",
          "description" : "If the billable transaction is tied to an time sheet line, then this field returns the time code for that line.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "phase",
          "in" : "query",
          "description" : "If the billable transaction is tied to an time sheet line, then this field returns the phase for that line.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "task",
          "in" : "query",
          "description" : "If the billable transaction is tied to an time sheet line, then this field returns the task for that line.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "customer",
          "in" : "query",
          "description" : "Customer tied to the ~project~ for this transaction.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "toDate",
          "in" : "query",
          "description" : "Date the transaction was incurred against.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "transactionSource",
          "in" : "query",
          "description" : "Identifies what type of transaction composes this billable transaction (i.e. expense report line versus time sheet line).",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "fromDate",
          "in" : "query",
          "description" : "Date the transaction was incurred against.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "project",
          "in" : "query",
          "description" : "The ~project~ this transaction is being billed against.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "The maximum number of objects in a single response. The default is 20. The maximum is 100.",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        }, {
          "name" : "offset",
          "in" : "query",
          "description" : "The zero-based index of the first object in a response collection. The default is 0. Use offset with the limit parameter to control paging of a response collection.",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        } ],
        "responses" : {
          "200" : {
            "description" : "getting response",
            "schema" : {
              "type" : "object",
              "description" : "collection something or other",
              "properties" : {
                "data" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/viewBillableTransaction_8ddea882065e100006b6b409b1b4008e"
                  }
                },
                "total" : {
                  "type" : "integer",
                  "format" : "int64"
                }
              }
            }
          },
          "default" : {
            "description" : "an error occurred",
            "schema" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    },
    "/billableTransactions/{ID}/billingRateApplication" : {
      "get" : {
        "tags" : [ "billableTransactions" ],
        "description" : "Retrieves a collection of billing rate applications for the specified billable transaction.\n\nSecured by: Manage: Project Details, Process: Project Billing\n\nScope: Project Billing",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "limit",
          "in" : "query",
          "description" : "The maximum number of objects in a single response. The default is 20. The maximum is 100.",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        }, {
          "name" : "offset",
          "in" : "query",
          "description" : "The zero-based index of the first object in a response collection. The default is 0. Use offset with the limit parameter to control paging of a response collection.",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        }, {
          "name" : "ID",
          "in" : "path",
          "description" : "Id of specified instance",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "getting response",
            "schema" : {
              "type" : "object",
              "description" : "collection something or other",
              "properties" : {
                "data" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/billingRateApplication_3d4d69e01dcc10001665940600ca00a1"
                  }
                },
                "total" : {
                  "type" : "integer",
                  "format" : "int64"
                }
              }
            }
          },
          "default" : {
            "description" : "an error occurred",
            "schema" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    },
    "/billableTransactions/{ID}" : {
      "get" : {
        "tags" : [ "billableTransactions" ],
        "description" : "Retrieves a billable transaction for the specified ID.\n\nSecured by: Manage: Project Details, Process: Project Billing\n\nScope: Project Billing",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "ID",
          "in" : "path",
          "description" : "Id of specified instance",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "getting response",
            "schema" : {
              "$ref" : "#/definitions/viewBillableTransaction_8ddea882065e100006b6b409b1b4008e"
            }
          },
          "default" : {
            "description" : "an error occurred",
            "schema" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      },
      "patch" : {
        "tags" : [ "billableTransactions" ],
        "description" : "Updates the billable transaction. Partially updates the existing billable transaction with the specified data in the request body.\n\nIf the billable transaction does not exist, the request fails with an HTTP error code 404.\n\nSecured by: Manage: Project Details, Process: Project Billing\n\nScope: Project Billing",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "ID",
          "in" : "path",
          "description" : "Id of specified instance",
          "required" : true,
          "type" : "string"
        }, {
          "in" : "body",
          "name" : "editBillableTransaction",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/editBillableTransaction_8ddea882065e100013bba4b3308300b4"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "update method response",
            "schema" : {
              "$ref" : "#/definitions/editBillableTransaction_8ddea882065e100013bba4b3308300b4"
            }
          },
          "default" : {
            "description" : "an error occurred",
            "schema" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          },
          "400" : {
            "description" : "a validation error occurred",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    }
  },
  "securityDefinitions" : {
    "OAuth2" : {
      "type" : "oauth2",
      "authorizationUrl" : "https://<tenant authorization hostname>",
      "flow" : "implicit"
    }
  },
  "definitions" : {
    "adjustmentReason_f572211fb69510000af1efb6fc260016" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      } ]
    },
    "billingStatus_32f06174ed841000162d26cfb2160076" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "editBillableTransaction_8ddea882065e100013bba4b3308300b4" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "adjustmentReason" : {
            "$ref" : "#/definitions/adjustmentReason_f572211fb69510000af1efb6fc260016"
          },
          "memo" : {
            "type" : "string",
            "description" : "Memo for the ~project~ billable transaction.",
            "x-workday-type" : "Text"
          },
          "hoursToBill" : {
            "type" : "integer",
            "description" : "Actual hours this transaction should bill.",
            "x-workday-type" : "Numeric"
          },
          "zeroAmountToBill" : {
            "type" : "boolean",
            "description" : "If flag set, change Amount To Bill passed in.",
            "x-workday-type" : "Boolean"
          },
          "billingStatus" : {
            "$ref" : "#/definitions/billingStatus_32f06174ed841000162d26cfb2160076"
          },
          "rateToBill" : {
            "type" : "integer",
            "description" : "Actual rate the transaction should bill.",
            "x-workday-type" : "Numeric"
          },
          "reasonForChange" : {
            "type" : "string",
            "description" : "Rate change reason for change",
            "x-workday-type" : "Text"
          },
          "amountToBill" : {
            "type" : "integer",
            "description" : "The amount to bill the customer, rounded for currency precision.  Excludes Extended Amounts on Tax Only Invoices.",
            "x-workday-type" : "Numeric"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "href" : {
            "type" : "string",
            "description" : "A link to the instance"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          }
        }
      } ]
    },
    "billingStatus_8ddea882065e100006b6b42acb1f008f" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "revenueStatus_daf7104c63ca10000acf246aea691263" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "category_8ddea882065e100006b6b45cc6c30094" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "customer_6c59ece2a7ed10001ed985eb8bc5011b" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "expenseDescriptor_24877ab6f6e4100007ceac86316f00ca" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "project_8ddea882065e1000139deaad4d1f00b1" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "transactionSource_8ddea882065e1000139deac2bff700b2" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "resourceProvider_32ea74fca4f3100013f567a61d010122" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "projectRole_184b770a949c1000116a331d71ff0020" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "adjustmentReason_184b770a949c1000116a33034b3d001f" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "worker_32ea74fca4f310001367ea0391ae0115" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "projectTask_184b770a949c1000116a3327928e0021" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "viewBillableTransaction_8ddea882065e100006b6b409b1b4008e" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "previouslyInvoiced" : {
            "type" : "boolean",
            "description" : "True if it’s a previously invoiced Billable Transaction.",
            "x-workday-type" : "Boolean"
          },
          "memo" : {
            "type" : "string",
            "description" : "Memo for the ~project~ billable transaction.",
            "x-workday-type" : "Text"
          },
          "invoiceDescription" : {
            "type" : "string",
            "description" : "Supplier invoice line item description for the billable transaction.",
            "x-workday-type" : "Text"
          },
          "adjustmentCount" : {
            "type" : "integer",
            "description" : "Count of Billing Rate Applications for this Billing Transaction.",
            "x-workday-type" : "Numeric"
          },
          "hoursToBill" : {
            "type" : "integer",
            "description" : "Actual hours this transaction should bill.",
            "x-workday-type" : "Numeric"
          },
          "zeroAmountToBill" : {
            "type" : "boolean",
            "description" : "If flag set, change Amount To Bill passed in.",
            "x-workday-type" : "Boolean"
          },
          "billableRate" : {
            "type" : "integer",
            "description" : "The original rate to bill the customer.",
            "x-workday-type" : "Numeric"
          },
          "billingStatus" : {
            "$ref" : "#/definitions/billingStatus_8ddea882065e100006b6b42acb1f008f"
          },
          "revenueStatus" : {
            "$ref" : "#/definitions/revenueStatus_daf7104c63ca10000acf246aea691263"
          },
          "reasonForChange" : {
            "type" : "string",
            "description" : "Rate change reason for change",
            "x-workday-type" : "Text"
          },
          "rateToBill" : {
            "type" : "integer",
            "description" : "Actual rate the transaction should bill.",
            "x-workday-type" : "Numeric"
          },
          "category" : {
            "$ref" : "#/definitions/category_8ddea882065e100006b6b45cc6c30094"
          },
          "customer" : {
            "$ref" : "#/definitions/customer_6c59ece2a7ed10001ed985eb8bc5011b"
          },
          "billableAmount" : {
            "type" : "integer",
            "description" : "The original amount to bill the customer.  Excludes Extended Amounts on Tax Only Invoices.",
            "x-workday-type" : "Numeric"
          },
          "expenseDescriptor" : {
            "$ref" : "#/definitions/expenseDescriptor_24877ab6f6e4100007ceac86316f00ca"
          },
          "project" : {
            "$ref" : "#/definitions/project_8ddea882065e1000139deaad4d1f00b1"
          },
          "transactionDate" : {
            "type" : "string",
            "format" : "date",
            "description" : "Date the transaction was incurred against.",
            "x-workday-type" : "Date"
          },
          "amountToBill" : {
            "type" : "integer",
            "description" : "The amount to bill the customer, rounded for currency precision.  Excludes Extended Amounts on Tax Only Invoices.",
            "x-workday-type" : "Numeric"
          },
          "transactionSource" : {
            "$ref" : "#/definitions/transactionSource_8ddea882065e1000139deac2bff700b2"
          },
          "resourceProvider" : {
            "$ref" : "#/definitions/resourceProvider_32ea74fca4f3100013f567a61d010122"
          },
          "billableHours" : {
            "type" : "integer",
            "description" : "The original hours to bill the customer.",
            "x-workday-type" : "Numeric"
          },
          "projectRole" : {
            "$ref" : "#/definitions/projectRole_184b770a949c1000116a331d71ff0020"
          },
          "adjustmentReason" : {
            "$ref" : "#/definitions/adjustmentReason_184b770a949c1000116a33034b3d001f"
          },
          "worker" : {
            "$ref" : "#/definitions/worker_32ea74fca4f310001367ea0391ae0115"
          },
          "projectTask" : {
            "$ref" : "#/definitions/projectTask_184b770a949c1000116a3327928e0021"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "href" : {
            "type" : "string",
            "description" : "A link to the instance"
          }
        }
      } ]
    },
    "billingRateApplication_3d4d69e01dcc10001665940600ca00a1" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "billingRateRuleType" : {
            "type" : "string",
            "description" : "The type of rate rule for this application.",
            "x-workday-type" : "Text"
          },
          "reasonForChange" : {
            "type" : "string",
            "description" : "The reason for change for this adjustment.",
            "x-workday-type" : "Text"
          },
          "amountToBill" : {
            "type" : "integer",
            "description" : "The total amount applied to the billing rate rule break on this transaction in the currency of the Billing Schedule of the Contract Line of the Billable Transaction the Billing Rate Application is for.",
            "x-workday-type" : "Numeric"
          },
          "ruleName" : {
            "type" : "string",
            "description" : "The billing rate rule name associated with this transaction.",
            "x-workday-type" : "Text"
          },
          "quantityToBill" : {
            "type" : "integer",
            "description" : "The units applied to the billing rate rule break on this transaction.",
            "x-workday-type" : "Numeric"
          },
          "rateAdjustmentAmount" : {
            "type" : "integer",
            "description" : "The rate adjustment amount for the billable transaction.",
            "x-workday-type" : "Numeric"
          },
          "rateStartingAmount" : {
            "type" : "integer",
            "description" : "The starting rate amount, before adjustment.",
            "x-workday-type" : "Numeric"
          },
          "applicationOrder" : {
            "type" : "integer",
            "description" : "The order of this adjustment in the change history of the billable transaction.",
            "x-workday-type" : "Numeric"
          },
          "rateEndingAmount" : {
            "type" : "integer",
            "description" : "The ending rate amount, after adjustment.",
            "x-workday-type" : "Numeric"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          }
        }
      } ]
    },
    "INSTANCE_MODEL_REFERENCE" : {
      "type" : "object",
      "required" : [ "id" ],
      "properties" : {
        "id" : {
          "type" : "string",
          "description" : "wid / id / reference id",
          "pattern" : "^(?:(?:[0-9a-f]{32})|(?:[0-9]+\\$[0-9]+)|(\\S+=\\S+))$"
        },
        "descriptor" : {
          "type" : "string",
          "description" : "A description of the instance",
          "readOnly" : true
        },
        "href" : {
          "type" : "string",
          "description" : "A link to the instance",
          "readOnly" : true
        }
      }
    },
    "ERROR_MODEL_REFERENCE" : {
      "type" : "object",
      "required" : [ "error" ],
      "properties" : {
        "error" : {
          "type" : "string",
          "description" : "A description of the error"
        }
      }
    },
    "VALIDATION_ERROR_MODEL_REFERENCE" : {
      "allOf" : [ {
        "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
      }, {
        "properties" : {
          "errors" : {
            "type" : "array",
            "description" : "An array of validation errors",
            "items" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      } ]
    },
    "FACETS_MODEL_REFERENCE" : {
      "type" : "array",
      "items" : {
        "type" : "object",
        "description" : "This object represents the possible facets for this resource",
        "readOnly" : true,
        "properties" : {
          "descriptor" : {
            "type" : "string",
            "description" : "A description of the facet"
          },
          "facetParameter" : {
            "type" : "string",
            "description" : "The alias used to select the facet"
          },
          "values" : {
            "type" : "array",
            "description" : "the facet values",
            "items" : {
              "type" : "object",
              "properties" : {
                "count" : {
                  "type" : "integer",
                  "format" : "int32",
                  "description" : "The number of instances returned by this facet"
                },
                "id" : {
                  "type" : "string",
                  "description" : "wid / id / reference id",
                  "pattern" : "^(?:(?:[0-9a-f]{32})|(?:[0-9]+\\$[0-9]+)|(\\S+=\\S+))$"
                },
                "descriptor" : {
                  "type" : "string",
                  "description" : "A description of the facet"
                },
                "href" : {
                  "type" : "string",
                  "description" : "A link to the instance",
                  "readOnly" : true
                }
              },
              "required" : [ "id" ]
            }
          }
        }
      }
    },
    "MULTIPLE_INSTANCE_MODEL_REFERENCE" : {
      "type" : "object",
      "properties" : {
        "total" : {
          "type" : "integer"
        },
        "data" : {
          "type" : "array",
          "items" : {
            "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
          }
        }
      }
    },
    "countryRegionDisplay_7c1e78fa078f100007abc05bc881005b" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "Country_Region_ID" : {
            "type" : "string",
            "description" : "Reference id of the instance"
          }
        }
      } ]
    },
    "country_7c1e78fa078f100007513fbef777004f" : {
      "allOf" : [ {
        "$ref" : "#/definitions/countryDisplay_7c1e78fa078f1000079a6bdd461d0057"
      }, { } ]
    },
    "countryRegion_7c1e78fa078f100007513fa05eb60048" : {
      "allOf" : [ {
        "$ref" : "#/definitions/countryRegionDisplay_7c1e78fa078f100007abc05bc881005b"
      }, { } ]
    },
    "addressDisplay_7c1e78fa078f100007513f8b03e40046" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "regionSubdivision2" : {
            "type" : "string",
            "description" : "Region Subdivision 2",
            "x-workday-type" : "Text"
          },
          "citySubdivision2" : {
            "type" : "string",
            "description" : "City Subdivision 2",
            "x-workday-type" : "Text"
          },
          "citySubdivision1" : {
            "type" : "string",
            "description" : "City Subdivision 1",
            "x-workday-type" : "Text"
          },
          "addressLine2" : {
            "type" : "string",
            "description" : "Address Line 2",
            "x-workday-type" : "Text"
          },
          "postalCode" : {
            "type" : "string",
            "description" : "Postal Code",
            "x-workday-type" : "Text"
          },
          "country" : {
            "$ref" : "#/definitions/country_7c1e78fa078f100007513fbef777004f"
          },
          "addressLine4" : {
            "type" : "string",
            "description" : "Address Line 4",
            "x-workday-type" : "Text"
          },
          "countryRegion" : {
            "$ref" : "#/definitions/countryRegion_7c1e78fa078f100007513fa05eb60048"
          },
          "addressLine3" : {
            "type" : "string",
            "description" : "Address Line 3",
            "x-workday-type" : "Text"
          },
          "addressLine1" : {
            "type" : "string",
            "description" : "Address Line 1",
            "x-workday-type" : "Text"
          },
          "regionSubdivision1" : {
            "type" : "string",
            "description" : "Region Subdivision 1",
            "x-workday-type" : "Text"
          },
          "city" : {
            "type" : "string",
            "description" : "City",
            "x-workday-type" : "Text"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          }
        }
      } ]
    },
    "countryDisplay_7c1e78fa078f1000079a6bdd461d0057" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "ISO_3166-1_Alpha-3_Code" : {
            "type" : "string",
            "description" : "Reference id of the instance"
          }
        }
      } ]
    },
    "companyDisplay_403c2a4e9ce210000601c146ed560173" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "Company_Reference_ID" : {
            "type" : "string",
            "description" : "Reference id of the instance"
          }
        }
      } ]
    },
    "salesItemDisplay_63adc930657810001bac3af8fbf90166" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "Sales_Item_ID" : {
            "type" : "string",
            "description" : "Reference id of the instance"
          }
        }
      } ]
    },
    "installmentBillingScheduleTemplateDisplay_a1748c0e6b3810000e43df975946002e" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "Billing_Schedule_Template_ID" : {
            "type" : "string",
            "description" : "Reference id of the instance"
          }
        }
      } ]
    },
    "unitOfMeasureDisplay_63adc930657810001bcf059828f3016b" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "UN_CEFACT_Common_Code_ID" : {
            "type" : "string",
            "description" : "Reference id of the instance"
          }
        }
      } ]
    },
    "taxCodeSummary_403c2a4e9ce2100006267faeaaa8017c" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "Tax_Code_ID" : {
            "type" : "string",
            "description" : "Reference id of the instance"
          }
        }
      } ]
    },
    "customerDisplay_403c2a4e9ce2100005f3fde67ba2016f" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "Customer_Reference_ID" : {
            "type" : "string",
            "description" : "Reference id of the instance"
          },
          "Customer_ID" : {
            "type" : "string",
            "description" : "Reference id of the instance"
          }
        }
      } ]
    },
    "taxApplicabilityDisplay_63adc930657810001bb6806032bd0168" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "Tax_Applicability_ID" : {
            "type" : "string",
            "description" : "Reference id of the instance"
          }
        }
      } ]
    },
    "contractLineTypeDisplay_63adc930657810001b95a73b87cf0161" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "Contract_Line_Type_ID" : {
            "type" : "string",
            "description" : "Reference id of the instance"
          }
        }
      } ]
    },
    "revenueCategoryDisplay_63adc930657810001ba59dee1c100164" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "Revenue_Category_ID" : {
            "type" : "string",
            "description" : "Reference id of the instance"
          }
        }
      } ]
    },
    "contractTypeDisplay_63adc930657810001b63a918b0f4015a" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          }
        }
      } ]
    },
    "worktagSummary_f712d0e0d34b10000e1907ce1dbb005c" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          }
        }
      } ]
    }
  }
}
