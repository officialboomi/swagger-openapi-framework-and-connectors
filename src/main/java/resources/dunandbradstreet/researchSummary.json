{
	"swagger": "2.0",
	"host": "plus.dnb.com",
	"schemes": [
		"https"
	],
	"info": {
		"description": "This method allows customers to get a list of the reasearch requests submitted by the users and the cases that were created towards the research requests.\r\n Note: This API is available as part of \"Research\" Add-Ons.",
		"title": "Research: Summary",
		"version": "1"
	},
	"basePath": "/",
	"paths": {
		"/v1/researches": {
			"x-DNB-Name": "Research: Summary",
			"x-DNB-ID": "researchSummary",
			"get": {
				"description": "This method allows customers to get a list of the reasearch requests submitted by the users and the cases that were created towards the research requests.\r\n Note: This API is available as part of \"Research\" Add-Ons.",
				"operationId": "researchSummary",
				"x-monitoring": "Not Available.",
				"x-monitoring-pdf": "Not Available.",
				"tags": [
					"researchSummary"
				],
				"produces": [
					"application/json;charset=utf-8"
				],
				"responses": {
					"200": {
						"description": "OK",
						"schema": {
							"properties": {
								"transactionDetail": {
									"type": "object",
									"description": "The information used to process this request.",
									"properties": {
										"transactionID": {
											"type": "string",
											"example": "rlh-hi9puyoijk-jop8u-kd-d-1",
											"description": "A value assigned by the Dun & Bradstreet application to uniquely identify this request."
										},
										"customerTransactionID": {
											"type": "string",
											"example": 1234,
											"description": "The customerTransactionID provided in the request"
										},
										"transactionTimestamp": {
											"type": "string",
											"example": "2017-02-21T17:46:19.839Z",
											"description": "The date and time, in ISO 8601 UTC Z standard, when this response was created."
										},
										"serviceVersion": {
											"type": "string",
											"example": 1,
											"description": "The product version number provided in the request."
										}
									},
									"additionalProperties": true
								},
								"inquiryDetail": {
									"type": "object",
									"description": "The details of the input data provided.",
									"properties": {
										"researchRequestID": {
											"type": "integer",
											"example": 123456789,
											"description": "A number assigned by D&B that uniquely identifies this research request."
										},
										"submissionTransactionID": {
											"type": "string",
											"example": "CUST123",
											"description": "The customer transaction identifier when they originally submit the research request."
										},
										"submissionStartDate": {
											"type": "string",
											"example": "2018-01-01",
											"description": "The date by when no requests submitted before this date will be returned. If this optional property is not specified, then there will be no constraint on how old a returned request may be."
										},
										"submissionEndDate": {
											"type": "string",
											"example": "2019-12-01",
											"description": "The date by when no requests submitted after this date will be returned. If this optional property is not specified, then the data considered for return, based on the supplied criteria, will be through the current date."
										},
										"completionStartDate": {
											"type": "string",
											"example": "2018-01-01",
											"description": "The date by when no requests closed before this date will be returned. If this optional property is not specified, then there will be no constraint on how old a returned request may be."
										},
										"completionEndDate": {
											"type": "string",
											"example": "2019-12-01",
											"description": "The date by when no requests closed after this date will be returned. If this optional property is not specified, then the data considered for return, based on the supplied criteria, will be through the current date."
										},
										"researchStatus": {
											"type": "string",
											"example": "Open",
											"description": "Denotes if the result set should be filtered to closed or open research requests. If this optional property is not provided, then both open and closed research requests are eligible to be returned in the response. <br/> <br/>Possible values:<br/>Open<br/>Closed<br/>Challenged"
										},
										"isRequestedByAllUsers": {
											"type": "boolean",
											"example": true,
											"description": "Indicates whether the result set should contain only research requests submitted by the current user or submitted by all users assigned to this subscriber.<br/><br/>- If true, result set should contain research requests submitted by all users under this subscriber<br/>- If false, result set should contain only research requests submitted by the current user"
										},
										"pageSize": {
											"type": "integer",
											"example": 25,
											"description": "The number of results to be returned per page in the response."
										},
										"pageMarker": {
											"type": "integer",
											"example": 12678,
											"description": "The last research request identifier that was returned in the previous page so that the results on the next page will start with the next research request. It is expected that all the other criteria defined in this schema will be identical from one page request to the next."
										},
										"sortDirection": {
											"type": "string",
											"example": "Ascending",
											"description": "Specify the order in which the research requests should be returned based on the submitted date."
										}
									},
									"additionalProperties": true
								},
								"hasNextPage": {
									"type": "boolean",
									"example": true,
									"description": "Indicates if the result set needs to continue to another 'page', meaning that another request has to be made if the retrieval requester wants to see more, or the rest, of the results. <br/><br/>- If true, it means that the result set needs to continue to another 'page'<br/>- If false, it means that the result set does not have any more pages left"
								},
								"pageMarker": {
									"type": "integer",
									"example": 32809,
									"description": "The last research request identifier that was returned in the previous page so that the results on the next page will start with the next research request. It is expected that all the other criteria defined in this schema will be identical from one page request to the next."
								},
								"openResearchRequestsCount": {
									"type": "integer",
									"example": 150,
									"description": "The number of Research Requests with \"Open\" status satisfying the request criteria whether or not they all fit in this response document."
								},
								"researchRequests": {
									"type": "array",
									"description": "Records details about the research requests that match the search criteria provided by the user.",
									"items": {
										"properties": {
											"researchRequestID": {
												"type": "integer",
												"example": 123456789,
												"description": "A number assigned by D&B that uniquely identifies this research request."
											},
											"submissionTransactionID": {
												"type": "string",
												"example": "CUST123",
												"description": "A number or text string provided by the requester identifying how this particular research request is known in their system. This is passthrough data that is not used by D&B for conducting research and is instead returned with the D&B resolution so the requester can put the returned information in the proper context, i.e., how the request is known at their location."
											},
											"userLoginKey": {
												"type": "string",
												"example": "DALQA",
												"description": "Text provided by the research submitter when making this request to uniquely identify themself (in the case of a person) or itself (in the case of a system) to the Dun & Bradstreet system."
											},
											"submissionTimestamp": {
												"type": "string",
												"example": "2017-02-21T17:46:19.839Z",
												"description": "The date and time when this research request was received by Dun & Bradstreet."
											},
											"researchStatus": {
												"type": "object",
												"description": "Records the current status that is communicated to the requester, e.g., received, in progress, pending subject company response, closed.",
												"properties": {
													"description": {
														"type": "string",
														"example": "Enhanced elements",
														"description": "Text identifying the current status that is communicated to the requester, e.g., received, in progress, pending subject company response, closed."
													},
													"dnbCode": {
														"type": "integer",
														"example": 33518,
														"description": "A code identifying the current status that is communicated to the requester, e.g., received, in progress, pending subject company response, closed.<br/>Refer to Reference code table \"764 [Research Status]\" for all possible values."
													}
												},
												"additionalProperties": true
											},
											"statusTimestamp": {
												"type": "string",
												"example": "2019-08-04T07:35:44.000+00:00",
												"description": "The date and time when the research request status was last updated."
											},
											"caseDetails": {
												"type": "array",
												"description": "Records details identifying all of the cases that were created for this request, assigned to each case. Typically there is just one case for a request, but there could be any number.",
												"items": {
													"properties": {
														"caseID": {
															"type": "integer",
															"example": 2501,
															"description": "A number uniquely identifying a research 'case' that was created for the research submission request."
														},
														"duns": {
															"type": "string",
															"example": "345678912",
															"description": "The D-U-N-S Number is D&B's identification number, which provides unique identification of this organization, in accordance with the Data Universal Numbering System (D-U-N-S)."
														},
														"primaryName": {
															"type": "string",
															"example": "Renault Group",
															"description": "The single name by which the organization is primarily known / identified."
														},
														"primaryAddress": {
															"type": "object",
															"description": "The details of the subject's primary address.",
															"properties": {
																"addressCountry": {
																	"type": "object",
																	"description": "Records the details of the country in which the business is located.",
																	"properties": {
																		"isoAlpha2Code": {
																			"type": "string",
																			"example": "US",
																			"description": "The 2-letter country code, defined by the International Organization for Standardization (ISO) ISO 3166-1 scheme identifying the country of the subject."
																		}
																	},
																	"additionalProperties": true
																},
																"addressLocality": {
																	"type": "object",
																	"description": "Records the details of a city, town, township, village, borough, etc. where the business is located.",
																	"properties": {
																		"name": {
																			"type": "string",
																			"example": "Cupertino",
																			"description": "Identifies the city, town, township, village, borough, etc. where the business is located."
																		}
																	},
																	"additionalProperties": true
																},
																"addressRegion": {
																	"type": "object",
																	"description": "Records details of the locally governed area which forms part of a centrally governed nation to identify where the business is located.",
																	"properties": {
																		"name": {
																			"type": "string",
																			"example": "California",
																			"description": "The name of a locally governed area which forms part of a centrally governed nation to identify where the business is located. <br/>Note: As a guiding principle this is a geographic area which could theoretically exist as a separate nation. In the U.S. this would be a State. In the UK this would be one of the Home Nations."
																		}
																	},
																	"additionalProperties": true
																},
																"addressCounty": {
																	"type": "object",
																	"description": "Records the details of the county where the business is located.",
																	"properties": {
																		"name": {
																			"type": "string",
																			"example": "Santa Clara County",
																			"description": "Text recording the name of a county where the business is located."
																		}
																	},
																	"additionalProperties": true
																}
															},
															"additionalProperties": true
														},
														"caseStatus": {
															"type": "object",
															"description": "Records the current status that is communicated to the requester, e.g., received, in progress, pending subject company response, closed.",
															"properties": {
																"description": {
																	"type": "string",
																	"example": "Enhanced elements",
																	"description": "Text identifying the current status that is communicated to the requester, e.g., received, in progress, pending subject company response, closed."
																},
																"dnbCode": {
																	"type": "integer",
																	"example": 33518,
																	"description": "A code identifying the current status that is communicated to the requester, e.g., received, in progress, pending subject company response, closed.<br/>Refer to Reference code table \"764 [Research Status]\" for all possible values."
																}
															},
															"additionalProperties": true
														},
														"statusTimestamp": {
															"type": "string",
															"example": "2019-08-04T07:35:44.000+00:00",
															"description": "The date and time when the case status was last updated."
														},
														"isChallengeable": {
															"type": "boolean",
															"example": true,
															"description": "Indicates if this case is challengeable.<br/>- If true; the resolution provided for this case can be challenged."
														},
														"subjectResearchTypes": {
															"type": "array",
															"description": "Records all of the research types and research sub types associated with this research case.",
															"items": {
																"properties": {
																	"researchType": {
																		"type": "object",
																		"description": "Records identifying the information that needs to be researched for the subject, e.g., Enhanced Elements, Financial Information, History, Public Records, Super 7 Elements.",
																		"properties": {
																			"description": {
																				"type": "string",
																				"example": "Enhanced elements",
																				"description": "Text identifying the information that needs to be researched for the subject, e.g., Enhanced Elements, Financial Information, History, Public Records, Super 7 Elements."
																			},
																			"dnbCode": {
																				"type": "integer",
																				"example": 33518,
																				"description": "A code identifying the information that needs to be researched for the subject, e.g., Enhanced Elements, Financial Information, History, Public Records, Super 7 Elements.<br/>Refer to Reference code table \"759 [Research Type]\" for all possible values."
																			}
																		},
																		"additionalProperties": true
																	},
																	"researchSubType": {
																		"type": "object",
																		"description": "Records precisely what information is to be researched. For example, if the Research Type Code is for 'Public Records' then the available choices are: Bankruptcy, Corporate Details/Business Registration, Judgement, Lien, Suit, UCC/Security.",
																		"properties": {
																			"description": {
																				"type": "string",
																				"example": "Diverse owned / socioeconomic classification",
																				"description": "Text specifying more precisely what information is to be researched. For example, if the Research Type Code is for 'Public Records' then the available choices are: Bankruptcy, Corporate Details/Business Registration, Judgement, Lien, Suit, UCC/Security."
																			},
																			"dnbCode": {
																				"type": "integer",
																				"example": 33533,
																				"description": "A code specifying more precisely what information is to be researched. For example, if the Research Type Code is for 'Public Records' then the available choices are: Bankruptcy, Corporate Details/Business Registration, Judgement, Lien, Suit, UCC/Security.<br/>Refer to Reference code table \"760 [Research Sub Type]\" for all possible values."
																			}
																		},
																		"additionalProperties": true
																	},
																	"isAddedByResearcher": {
																		"type": "boolean",
																		"example": true,
																		"description": "Indicates if the research type was added by the researcher in relation to the actual research request from the requestor.<br/><br/>- If true, it means that the researcher was responsible for adding the Research Type and Research Sub Type combination specified here, not the requester. This happens when the researcher determines that the research that actually needs to be conducted is not the same as what the requester had indicated. For example, if the requested research was regarding a business name change and the researcher found that a merger/acquisition had accounted for the name change, then the business name research sub-type will be cancelled with the Sub Resolution that the 'Research sub-type is not applicable' and this entry will specify the new M&A research added by the researcher."
																	},
																	"resolution": {
																		"type": "object",
																		"description": "Records the outcome of the research activity was, e.g., data was changed or added, update or change was not made because data was correct, no updates were made because requested change could not be verified.",
																		"properties": {
																			"description": {
																				"type": "string",
																				"example": "Data was changed or added",
																				"description": "Text identifying the outcome of the research activity was, e.g., data was changed or added, update or change was not made because data was correct, no updates were made because requested change could not be verified."
																			},
																			"dnbCode": {
																				"type": "integer",
																				"example": 33726,
																				"description": "A code identifying the outcome of the research activity was, e.g., data was changed or added, update or change was not made because data was correct, no updates were made because requested change could not be verified.<br/>Refer to Reference code table \"772 [Case Resolution]\" for all possible values."
																			}
																		},
																		"additionalProperties": true
																	},
																	"subResolution": {
																		"type": "object",
																		"description": "Records more information on the outcome of the research activity than provided in the Resolution.",
																		"properties": {
																			"description": {
																				"type": "string",
																				"example": "Verified through a company spokesperson",
																				"description": "Text specifying more information on the outcome of the research activity than provided in the Resolution."
																			},
																			"dnbCode": {
																				"type": "integer",
																				"example": 33730,
																				"description": "A code specifying more information on the outcome of the research activity than provided in the Resolution.<br/>Refer to Reference code table \"773 [Case Sub Resolution]\" for all possible values."
																			}
																		},
																		"additionalProperties": true
																	},
																	"resolutionTimestamp": {
																		"type": "string",
																		"example": "2019-08-04T07:35:44.000+00:00",
																		"description": "The date and time when the outcome of this research sub-type was recorded."
																	}
																},
																"additionalProperties": true,
																"type": "object"
															}
														}
													},
													"additionalProperties": true,
													"type": "object"
												}
											}
										},
										"additionalProperties": true,
										"type": "object"
									}
								}
							},
							"description": "",
							"type": "object"
						}
					}
				},
				"security": [],
				"parameters": [
					{
						"in": "header",
						"name": "Authorization",
						"description": "The access token provided by authentication.",
						"required": true,
						"type": "string",
						"x-example": "Bearer alphanumerictoken"
					},
					{
						"in": "query",
						"name": "researchRequestID",
						"description": "Up to 9 characters. A number assigned by D&B that uniquely identifies this research request.<br/><br/>Possible values: 10000 <= value <= 999999999",
						"required": false,
						"type": "integer",
						"x-example": 12345
					},
					{
						"in": "query",
						"name": "submissionTransactionID",
						"description": "Up to 32 characters. A number or text string provided by the requester identifying how this particular research request is known in their system.<br/>This is the customer transaction identifier (customerTransactionID) used by the customer when submitting the research request to Dun & Bradstreet.",
						"required": false,
						"type": "string",
						"x-example": "CUST123"
					},
					{
						"in": "query",
						"name": "submissionStartDate",
						"description": "Up to 10 characters. The date by when no requests submitted before this date will be returned.  If this optional property is not specified, then there will be no constraint on how old a returned request may be.<br/><br/>Possible values: YYYY-MM-DD only",
						"required": false,
						"type": "string",
						"x-example": "2018-01-01"
					},
					{
						"in": "query",
						"name": "submissionEndDate",
						"description": "Up to 10 characters. The date by when no requests submitted after this date will be returned.  If this optional property is not specified, then the data considered for return, based on the supplied criteria, will be through the current date.<br/><br/>Possible values: YYYY-MM-DD only",
						"required": false,
						"type": "string",
						"x-example": "2019-12-01"
					},
					{
						"in": "query",
						"name": "completionStartDate",
						"description": "Up to 10 characters. The date by when no requests closed before this date will be returned.  If this optional property is not specified, then there will be no constraint on how old a returned request may be.<br/><br/>Possible values: YYYY-MM-DD only",
						"required": false,
						"type": "string",
						"x-example": "2018-01-01"
					},
					{
						"in": "query",
						"name": "completionEndDate",
						"description": "Up to 10 characters. The date by when no requests closed after this date will be returned.  If this optional property is not specified, then the data considered for return, based on the supplied criteria, will be through the current date.<br/><br/>Possible values: YYYY-MM-DD only",
						"required": false,
						"type": "string",
						"x-example": "2019-12-01"
					},
					{
						"in": "query",
						"name": "researchStatus",
						"description": "Up to 16 characters. Denotes if the result set should be filtered to closed or open research requests.  If this optional property is not provided, then both open and closed research requests are eligible to be returned in the response. <br/><br/>Possible values:<br/>Open<br/>Closed<br/>Challenged",
						"required": false,
						"type": "string",
						"x-example": "Open"
					},
					{
						"in": "query",
						"name": "isRequestedByAllUsers",
						"description": "Up to  characters. Indicates whether the result set should contain only reseacrh requests submitted by the current user or submitted by all users assigned to this subscriber.<br/><br/>- If true, result set should contain reseacrh requests submitted by all users under this subscriber<br/>- If false, result set should contain only reseacrh requests submitted by the current user<br/><br/>Possible values: Default is false",
						"required": false,
						"type": "boolean",
						"x-example": true
					},
					{
						"in": "query",
						"name": "pageSize",
						"description": "Up to 4 characters. The number of requests to be returned on a page.<br/><br/>Possible values: max 1000 results/page<br/>Default 1000 results/page",
						"required": false,
						"type": "integer",
						"x-example": 25
					},
					{
						"in": "query",
						"name": "pageMarker",
						"description": "Up to 9 characters. The last research request identifier that was returned in the previous page so that the results on the next page will start with the next research request.  It is expected that all the other criteria defined in this schema will be identical from one page request to the next.",
						"required": false,
						"type": "integer",
						"x-example": 12678
					},
					{
						"in": "query",
						"name": "sortDirection",
						"description": "Up to 16 characters. Specify the order in which the research requests should be returned based on the submitted date.<br/><br/>Possible values: Ascending<br/>Descending<br/><br/>Default is Ascending",
						"required": false,
						"type": "string",
						"x-example": "Ascending"
					},
					{
						"in": "query",
						"name": "customerTransactionID",
						"description": "An ID assigned by the customer to uniquely identify this request.",
						"required": false,
						"type": "string",
						"x-example": 1234
					}
				]
			}
		}
	}
}