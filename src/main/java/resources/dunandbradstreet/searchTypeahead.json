{
	"swagger": "2.0",
	"host": "plus.dnb.com",
	"schemes": [
		"https"
	],
	"info": {
		"description": "Searches for companies",
		"title": "Search for a Company",
		"version": "1"
	},
	"basePath": "/",
	"paths": {
		"/v1/search/typeahead": {
			"x-DNB-Name": "Search: Typeahead",
			"x-DNB-ID": "searchTypeahead",
			"get": {
				"description": "Typeahead search enables users to quickly find company records without having to type the entire company information in the search request.\r\n\r\nData Coverage: Global\r\nNote: This API is available as part of \"Company Entity Resolution\" Non Standard Data Blocks.",
				"summary": "Endpoint",
				"operationId": "searchTypeahead",
				"x-monitoring": "Not Available.",
				"x-monitoring-pdf": "Not Available.",
				"tags": [
					"searchTypeahead"
				],
				"produces": [
					"application/json"
				],
				"responses": {
					"200": {
						"description": "OK",
						"schema": {
							"properties": {
								"transactionDetail": {
									"type": "object",
									"description": "The information used to process this request.",
									"properties": {
										"transactionID": {
											"type": "string",
											"example": "rlh-hi9puyoijk-jop8u-kd-d-1",
											"description": "A value assigned by the Dun & Bradstreet application to uniquely identify this request."
										},
										"transactionTimestamp": {
											"type": "string",
											"example": "2017-02-21T17:46:19.839Z",
											"description": "The date and time, in ISO 8601 UTC Z standard, when this response was created."
										},
										"inLanguage": {
											"type": "string",
											"example": "en-US",
											"description": "An IETF BCP 47 code value that defines the language in which this product was rendered."
										},
										"serviceVersion": {
											"type": "string",
											"example": 1,
											"description": "The product version number provided in the request."
										}
									},
									"additionalProperties": true
								},
								"inquiryDetail": {
									"type": "object",
									"description": "The details of the input data provided.",
									"properties": {
										"searchTerm": {
											"type": "string",
											"example": "The DUN & BRADSTREET",
											"description": "The search term provided in the request."
										},
										"countryISOAlpha2Code": {
											"type": "string",
											"example": "US",
											"description": "The country code provided in the request."
										},
										"isDelisted": {
											"type": "boolean",
											"example": false,
											"description": "Limits the search to entities with an isDelisted flag set to the specified value.<br/> <br/>* If \"true\" it indicates that the search result will return only companies that have requested that they not be included in any D&B marketing lists (e.g., mailing, telephone).<br/>* If \"false\" it indicates that the search result will return only companies that have not opted out of any D&B marketing lists (e.g., mailing, telephone)<br/>* Default value is \"null\" i.e. the search results are not filtered based on this parameter."
										},
										"candidateMaximumQuantity": {
											"type": "integer",
											"example": 25,
											"description": "The maximum number of candidates requested."
										}
									}
								},
								"candidatesMatchedQuantity": {
									"type": "integer",
									"example": 100,
									"description": "The total number of DUNS in which candidates are found with matching search criteria."
								},
								"candidatesReturnedQuantity": {
									"type": "integer",
									"example": 50,
									"description": "The total number of candidates returned in the response."
								},
								"searchCandidates": {
									"type": "array",
									"description": "The details for the entities returned in the search.",
									"items": {
										"properties": {
											"displaySequence": {
												"type": "integer",
												"example": 1,
												"description": "The order in which the entity appears in the result set. The lower the number the better the probability of the match."
											},
											"organization": {
												"type": "object",
												"description": "The details of the entity.",
												"properties": {
													"duns": {
														"type": "string",
														"example": 804735132,
														"description": "The D-U-N-S Number, assigned by Dun & Bradstreet, is an identification number that uniquely identifies the entity in accordance with the Data Universal Numbering System (D-U-N-S)."
													},
													"dunsControlStatus": {
														"type": "object",
														"description": "The details of the entity established to provide products and/or services in the marketplace or to the community.",
														"properties": {
															"isOutOfBusiness": {
																"type": "boolean",
																"example": false,
																"description": "Indicates whether the entity is longer functional or trading. <br/><br/>- If true, the entity is no longer functional or trading.<br/>- If false, the entity is actively functional or trading."
															}
														},
														"additionalProperties": true
													},
													"primaryName": {
														"type": "string",
														"example": "GORMAN MANUFACTURING COMPANY, INC.",
														"description": "The single name by which the entity is primarily known or identified."
													},
													"tradeStyleNames": {
														"type": "array",
														"description": "The details of the names that the entity trades under for commercial purposes although its registered legal name used for contracts and other formal situations may be another name.",
														"items": {
															"properties": {
																"name": {
																	"type": "string",
																	"example": "Alternate Company Name",
																	"description": "The name that the entity trades under for commercial purposes although its registered legal name used for contracts and other formal situations may be another name."
																},
																"priority": {
																	"type": "integer",
																	"example": 1,
																	"description": "The sequence in which the entity mostly uses this trade style name."
																}
															},
															"additionalProperties": true,
															"type": "object"
														}
													},
													"primaryAddress": {
														"type": "object",
														"description": "The details of the single primary or physical address at which the entity is located.",
														"properties": {
															"addressCountry": {
																"type": "object",
																"description": "The details of the country/market in which this address is located.",
																"properties": {
																	"isoAlpha2Code": {
																		"type": "string",
																		"example": "US",
																		"description": "The two-letter country code, defined by the International Organization for Standardization (ISO) ISO 3166-1 scheme identifying the country/market in which this address is located."
																	}
																},
																"additionalProperties": true
															},
															"addressLocality": {
																"type": "object",
																"description": "The details of the locality where the organization's Primary Address is located or conducts operations. Locality defined as a district population cluster. City, town, township, village, borough etc.",
																"properties": {
																	"name": {
																		"type": "string",
																		"example": "SAN FRANCISCO",
																		"description": "The name of the locality where the organization's Primary Address is located or conducts operations. Locality defined as a district population cluster. City, town, township, village, borough etc."
																	}
																},
																"additionalProperties": true
															},
															"addressRegion": {
																"type": "object",
																"description": "The details of the locally governed area that forms part of a centrally governed nation to identify where this address is located.",
																"properties": {
																	"name": {
																		"type": "string",
																		"example": "California",
																		"description": "The name of the locally governed area that forms part of a centrally governed nation to identify where this address is located."
																	}
																},
																"additionalProperties": true
															},
															"postalCode": {
																"type": "string",
																"example": 941109999,
																"description": "The postal code of the organization's Primary Address.<br/><br/>Also known locally in various English-speaking countries throughout the world as a postcode, post code, Eircode, PIN or ZIP Code) is a series of letters or digits or both, sometimes including spaces or punctuation, included in a postal address for the purpose of sorting mail. The code is used by the country&#39;s postal authority to identify the address where the organization is located or conducts operations ."
															},
															"streetAddress": {
																"type": "object",
																"description": "The details of the street address where the entity is located.",
																"properties": {
																	"line1": {
																		"type": "string",
																		"example": "492 KOLLER ST",
																		"description": "The first line of the address where the entity is located.<br/><br/>For example, &quot;41 Central Chambers&quot; in the address <br/>41 Central Chambers<br/>Dame Court<br/>Dublin 2"
																	}
																},
																"additionalProperties": true
															}
														},
														"additionalProperties": true
													},
													"financials": {
														"type": "array",
														"description": "The details of the standardized view of the financial results of the entity; this is usually extracted from a set of accounts.",
														"items": {
															"properties": {
																"yearlyRevenue": {
																	"type": "array",
																	"description": "The details of the income received from customers from the sale of the entity's goods and/or services. This is the gross sales minus any returns, rebates/discounts, allowances for damages or shortages, shipping expenses passed on to the customer, and amounts due where there is not a reasonable expectation of collection.",
																	"items": {
																		"properties": {
																			"value": {
																				"type": "number",
																				"example": 19945238,
																				"description": "The monetary value of income received from customers from the sale of the entity's goods and/or services. This is the gross sales minus any returns, rebates/discounts, allowances for damages or shortages, shipping expenses passed on to the customer."
																			},
																			"currency": {
																				"type": "string",
																				"example": "USD",
																				"description": "The three-letter currency code, defined in the ISO 4217 scheme published by International Organization for Standardization (ISO) identifying the type of money in which this amount is expressed (e.g. USD, CAD, GBP, EUR)."
																			}
																		},
																		"additionalProperties": true,
																		"type": "object"
																	}
																}
															},
															"additionalProperties": true,
															"type": "object"
														}
													},
													"corporateLinkage": {
														"type": "object",
														"description": "The details of the relationship between the entity and other entities in the legal family tree.",
														"properties": {
															"isBranch": {
																"type": "boolean",
																"example": false,
																"description": "Indicates if the entity is a Branch.<br/><br/>- If true, the entity is a branch.<br/>- If false, the entity is not a branch."
															},
															"globalUltimate": {
																"type": "object",
																"description": "The details of the top most entity in the family tree, tracing each parent up to its own parent. <br/><br/>For example, Company-A is based in the US and has Company-B as its parent also in the US. In turn Company-B is owned by Company-C, also in the US, which is owned by Company-D which is in the UK. Thus, the Global Ultimate of this family tree (and therefore of each member of the family tree) is Company-D.",
																"properties": {
																	"duns": {
																		"type": "string",
																		"example": 804735132,
																		"description": "The D-U-N-S Number, assigned by Dun & Bradstreet, is an identification number that uniquely identifies the entity in accordance with the Data Universal Numbering System (D-U-N-S)."
																	},
																	"primaryName": {
																		"type": "string",
																		"example": "GORMAN MANUFACTURING COMPANY, INC.",
																		"description": "The single name by which the entity is primarily known or identified."
																	}
																},
																"additionalProperties": true
															}
														},
														"additionalProperties": true
													},
													"primaryIndustryCodes": {
														"type": "array",
														"description": "The primary activity in which the organization is engaged as defined by the 4 digit 1987 US Standard Industry Classification (SIC). Additional coding schemes may be added in the future.",
														"items": {
															"properties": {
																"usSicV4": {
																	"type": "string",
																	"example": 7323,
																	"description": "The 1987 version of a 4-digit numeric coding system developed by the US Government for the classification of industrial activities to denote the industry in which the entity does most of its business."
																},
																"usSicV4Description": {
																	"type": "string",
																	"example": "Credit reporting services",
																	"description": "The business activity based on the scheme used for the industry code (e.g., 'highway and street construction' is the description of industry code 1611 in the U.S. SIC (Standard Industrial Classification) system)."
																}
															},
															"additionalProperties": true,
															"type": "object"
														}
													}
												},
												"additionalProperties": true
											}
										},
										"additionalProperties": true,
										"type": "object"
									}
								}
							},
							"description": "",
							"type": "object"
						}
					}
				},
				"security": [],
				"parameters": [
					{
						"in": "query",
						"name": "searchTerm",
						"description": "2 to 30 characters used to find entities by its primary name or one of its tradestyle names.",
						"required": true,
						"type": "string",
						"x-example": "name search"
					},
					{
						"in": "query",
						"name": "countryISOAlpha2Code",
						"description": "The 2-letter country/market code defined by the International Organization for Standardization (ISO) ISO 3166-1 scheme identifying the country of the entity.",
						"required": false,
						"type": "string",
						"x-example": "US"
					},
					{
						"in": "query",
						"name": "isOutOfBusiness",
						"description": "Limits the search to entities with an isOutOfBusiness flag set to the specified value.    \r<br/>    \r<br/>* true = the entity is no longer functional or trading.\r<br/>* false = the entity is actively functional or trading.",
						"required": false,
						"type": "boolean",
						"x-example": false
					},
					{
						"in": "query",
						"name": "isMarketable",
						"description": "Limits the search to entities with an isMarketable flag set to the specified value.<br/><br/>* true = the data on the entity satisfies Dun & Bradstreet's marketability rules for Sales & Marketing Solutions products. <br/>* false = the data on the entity does not satisfies Dun & Bradstreet's marketability rules.",
						"required": false,
						"type": "boolean",
						"x-example": true
					},
					{
						"in": "query",
						"name": "isDelisted",
						"description": "Limits the search to entities with an isDelisted flag set to the specified value.\r<br/>  \r<br/>* If \"true\" it indicates that the search result will return only companies that have requested that they not be included in any D&B marketing lists (e.g., mailing, telephone).\r<br/>* If \"false\" it indicates that the search result will return only companies  that have not opted out of any D&B marketing lists (e.g., mailing, telephone)\r<br/>* Default value is \"null\" i.e. the search results are not filtered based on this parameter.",
						"required": true,
						"type": "boolean",
						"x-example": false
					},
					{
						"in": "query",
						"name": "isMailUndeliverable",
						"description": "Limits the search to entities with an isMailDeliverable flag set to the specified value.       <br/>        <br/>* true = mail cannot be delivered to the address.<br/>* false = mail can be delivered to the address.",
						"required": false,
						"type": "boolean",
						"x-example": false
					},
					{
						"in": "query",
						"name": "addressLocality",
						"description": "1 to 50 characters that identifies the city, town, township, village, borough, etc. where the entity is located.",
						"required": false,
						"type": "string",
						"x-example": "short hills"
					},
					{
						"in": "query",
						"name": "addressRegion",
						"description": "The name of the locally governed area that forms part of a centrally governed nation to identify where the address is located.     <br/>     <br/>Note: As a guiding principle this is a geographic area which could theoretically exist as a separate nation. In the U.S. this would be a State. In the UK this would be one of the Home Nations.   <br/><br/>If country is US or CA, the Region must be 2 characters.<br/>For other countries, it can be up to 64 characters.",
						"required": false,
						"type": "string",
						"x-example": "NJ"
					},
					{
						"in": "query",
						"name": "streetAddressLine1",
						"description": "Up to 70 characters of the first line of the entity's street address.    <br/><br/>This value is required for &quot;Name and Address&quot; Match.",
						"required": false,
						"type": "string",
						"x-example": "street address"
					},
					{
						"in": "query",
						"name": "postalCode",
						"description": "Up to 10 characters of the identifier used by the local country's postal authority to identify where the address is located.    <br/>    <br/>This value is required for Postal Code Match.",
						"required": false,
						"type": "string",
						"x-example": "94536"
					},
					{
						"in": "query",
						"name": "radiusLat",
						"description": "The latitude to be used as the center of the radius search.<br/><br/>Only addresses with a street-centric latitude/longitude are included in search.",
						"required": false,
						"type": "number",
						"x-example": 40.7431319
					},
					{
						"in": "query",
						"name": "radiusLon",
						"description": "The longitude to be used as the center of the radius search.<br/><br/>Only addresses with a street-centric latitude/longitude are included in search.",
						"required": false,
						"type": "number",
						"x-example": -74.3620265
					},
					{
						"in": "query",
						"name": "radiusPostalCode",
						"description": "5 digits for the US postal code (an identifier used by the local country's postal authority to identify where the address is located) used as the center of the radius search.<br/><br/>Note: Currently, only 5-digit US postal codes can be used for location radius searches.",
						"required": false,
						"type": "string",
						"x-example": "94536"
					},
					{
						"in": "query",
						"name": "radiusDistance",
						"description": "The circle radius in the units specified to be considered along with the latitude and longitude to locate entities.<br/><br/>Maximum values:<br/>* 100mi<br/>* 160.9km<br/>* or equivalent for other units",
						"required": false,
						"type": "number",
						"x-example": 5
					},
					{
						"in": "query",
						"name": "radiusUnit",
						"description": "The unit of measure for the specified radius. <br/><br/>Valid values: <br/>* m = meters<br/>* km = kilometers<br/>* mi = miles<br/>* yd = yards<br/>* ft = feet",
						"required": false,
						"type": "string",
						"x-example": "km"
					},
					{
						"in": "query",
						"name": "candidateMaximumQuantity",
						"description": "The maximum number of results to be returned.<br/><br/>Valid values: 1 to 25<br/>If not specified, the default of 10 is used.",
						"required": false,
						"type": "integer",
						"x-example": 25
					},
					{
						"in": "query",
						"name": "customerReference",
						"description": "Up to 240 characters for a reference string to be linked to the request in order to support subsequent order reconciliation.",
						"required": false,
						"type": "string",
						"x-example": "customer reference text"
					},
					{
						"in": "header",
						"name": "Authorization",
						"description": "The access token provided by authentication.",
						"required": true,
						"type": "string",
						"x-example": "Bearer alphanumerictoken"
					}
				]
			}
		}
	}
}