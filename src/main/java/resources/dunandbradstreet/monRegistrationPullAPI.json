{
	"swagger": "2.0",
	"host": "plus.dnb.com",
	"schemes": [
		"https"
	],
	"info": {
		"description": "Pull notifications for a registration",
		"version": "1",
		"title": "Monitoring API"
	},
	"basePath": "/",
	"tags": [
		{
			"name": "registrations"
		}
	],
	"paths": {
		"/v1/monitoring/registrations/{reference}/notifications": {
			"x-DNB-Name": "Pull notifications for a registration",
			"x-DNB-ID": "monRegistrationPullAPI",
			"get": {
				"tags": [
					"registrations"
				],
				"summary": "Endpoint",
				"description": "Provides the ability to Pull notifications for a registration. The registration must have been created with deliveryTrigger set to API_PULL in order to use the pull API. If maxNotifications is not specified then one notification will be returned (if available). Notifications are available for up to 4 days to be pulled. The pull API method should only be used for registrations of up to 300,000 D-U-N-S. For larger portfolios it is more efficient and effective to have notifications delivered to STP or S3 locations. The maximum number of notifications that can be pulled in a single API call are 375 when the Notification Type is FULL_PRODUCT or 1,125 when the Notification Type is UPDATE. Notifications which are pulled are available for replay (to be re-pulled) via the replay API for 14 days. If notifications are not pulled for 4 days then they will be removed and a re-seed will be necessary.",
				"operationId": "registrationPullNotification",
				"produces": [
					"application/json"
				],
				"parameters": [
					{
						"in": "path",
						"name": "reference",
						"description": "A string used to identify the registration.  The reference must be unique for each subscriber.",
						"required": true,
						"type": "string",
						"x-example": "myregistration"
					},
					{
						"in": "query",
						"name": "maxNotifications",
						"description": "Request to pull a number of notifications, if available, up to the value specified. If maxNotifications is not specified then one notification will be returned (if available). If less notifications are available than the value of maxNotifications then all available will be returned. The Pull API can return a large number of small (non full product) notifications or a smaller number of large notifications. Where an error or timeout is encountered while pulling notifications it is recommended to reduce the max number of notifications being pulled.",
						"required": false,
						"type": "integer",
						"x-example": 10
					},
					{
						"in": "header",
						"name": "Authorization",
						"description": "The access token provided by authentication.",
						"required": true,
						"type": "string",
						"x-example": "Bearer alphanumerictoken"
					}
				],
				"responses": {
					"200": {
						"description": "Request success, one or more notifications were returned",
						"schema": {
							"description": "",
							"type": "object",
							"properties": {
								"transactionDetail": {
									"type": "object",
									"description": "The information used to process this request.",
									"properties": {
										"transactionID": {
											"description": "A value assigned by the Dun & Bradstreet application to uniquely identify this request.",
											"example": "rlh-hi9puyoijk-jop8u-kd-d-1",
											"type": "string"
										},
										"transactionTimestamp": {
											"description": "The date and time, in ISO 8601 UTC Z standard, when this response was created.",
											"example": "2017-02-21T17:46:19.839Z",
											"type": "string"
										},
										"inLanguage": {
											"description": "An IETF BCP 47 code value that defines the language in which this product was rendered.",
											"example": "en-US",
											"type": "string"
										}
									}
								},
								"information": {
									"description": "The details of the request.",
									"type": "object",
									"properties": {
										"code": {
											"description": "A unique code assigned by Dun & Bradstreet to identify the status of this request. See the Error and Information Code list for more information.",
											"example": 99999,
											"type": "string"
										},
										"message": {
											"description": "The description assigned to the information code.",
											"example": "A message appropriate to the request outcome.",
											"type": "string"
										}
									}
								},
								"notifications": {
									"type": "array",
									"items": {
										"description": "",
										"type": "object",
										"properties": {
											"type": {
												"description": "The type of notification.",
												"example": "UPDATE",
												"type": "string"
											},
											"organization": {
												"description": "",
												"type": "object",
												"properties": {
													"duns": {
														"type": "string",
														"example": 804735132,
														"description": "The D-U-N-S Number, assigned by Dun & Bradstreet, is an identification number that uniquely identifies the entity in accordance with the Data Universal Numbering System (D-U-N-S)."
													}
												}
											},
											"elements": {
												"type": "array",
												"items": {
													"description": "",
													"type": "object",
													"properties": {
														"element": {
															"description": "JSON Path of element that has been updated.",
															"example": "organization.latestFinancials.financialStatementToDate",
															"type": "string"
														},
														"previous": {
															"description": "Previous value of the element.See the specific product response for details on attributes and values included in this section.",
															"example": "2018-12-31",
															"type": "object"
														},
														"current": {
															"description": "Current value of the element.See the specific product response for details on attributes and values included in this section.",
															"example": "2019-12-31",
															"type": "object"
														}
													}
												}
											},
											"deliveryTimeStamp": {
												"description": "A String in valid ISO-8601 format \"yyyy-MM-dd'T'hh:mm:ss'Z'\" containing the time that this notification was first pulled via the API",
												"example": "2019-05-02T02:20:58.629Z",
												"type": "string"
											}
										}
									}
								}
							}
						}
					},
					"204": {
						"description": "Request success however no notifications are available at this time. The notifications array shall be empty.",
						"schema": {
							"description": "",
							"type": "object",
							"properties": {}
						}
					}
				}
			}
		}
	}
}