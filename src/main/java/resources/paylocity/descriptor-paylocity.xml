<?xml version="1.0" encoding="UTF-8"?>
<GenericConnectorDescriptor
   onPremiseBrowseOnly="false"
    dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ"
    requireConnectionForBrowse="true">
	<!-- For more information regarding SDK descriptor files, see https://help.boomi.com/bundle/connectors/page/int-Describing_the_capabilities_of_a_custom_connector.html -->
	<description>For general questions and support of the Paylocity API, contact: webservices@paylocity.com Overview: Paylocity Web Services API is an externally facing RESTful Internet protocol. The Paylocity API uses HTTP verbs and a RESTful endpoint structure. OAuth 2.0 is used as the API Authorization framework. Request and response payloads are formatted as JSON. Paylocity supports v1 and v2 versions of its API endpoints. v1, while supported, won't be enhanced with additional functionality. For direct link to v1 documentation, please click [here](https://docs.paylocity.com/weblink/guides/Paylocity_Web_Services_API/v1/Paylocity_Web_Services_API.htm). For additional resources regarding v1/v2 differences and conversion path, please contact webservices@paylocity.com. Setup: Paylocity will provide the secure client credentials and set up the scope (type of requests and allowed company numbers). You will receive the unique client id, secret, and Paylocity public key for the data encryption. The secret will expire in 365 days. Paylocity will send you an e-mail 10 days prior to the expiration date for the current secret. If not renewed, the second e-mail notification will be sent 5 days prior to secret's expiration. Each email will contain the code necessary to renew the client secret. You can obtain the new secret by calling API endpoint using your current not yet expired credentials and the code that was sent with the notification email. For details on API endpoint, please see Client Credentials section. Both the current secret value and the new secret value will be recognized during the transition period. After the current secret expires, you must use the new secret. If you were unable to renew the secret via API endpoint, you can still contact Service and they will email you new secret via secure email.rnrnrnWhen validating the request, Paylocity API will honor the defaults and required fields set up for the company default New Hire Template as defined in Web Pay. Authorization: Paylocity Web Services API uses OAuth2.0 Authentication with JSON Message Format. All requests of the Paylocity Web Services API require a bearer token which can be obtained by authenticating the client with the Paylocity Web Services API via OAuth 2.0.rnrnrnThe client must request a bearer token from the authorization endpoint:rnrnrnauth-server for production: https://api.paylocity.com/IdentityServer/connect/tokenrnrnrnauth-server for testing: https://apisandbox.paylocity.com/IdentityServer/connect/tokenrnrnPaylocity reserves the right to impose rate limits on the number of calls made to our APIs. Changes to API features/functionality may be made at anytime with or without prior notice.rnrn##### Authorization HeaderrnrnThe request is expected to be in the form of a basic authentication request, with the "Authorization" header containing the client-id and client-secret. This means the standard base-64 encoded user:password, prefixed with "Basic" as the value for the Authorization header, where user is the client-id and password is the client-secret.rnrn##### Content-Type HeaderrnrnThe "Content-Type" header is required to be "application/x-www-form-urlencoded".rnrn##### Additional ValuesrnrnThe request must post the following form encoded values within the request body:rnrn    grant_type = client_credentialsrn    scope = WebLinkAPIrnrn##### ResponsesrnrnSuccess will return HTTP 200 OK with JSON content:rnrn    {rn      "access_token": "xxx",rn      "expires_in": 3600,rn      "token_type": "Bearer"rn    }rnrn# EncryptionrnrnPaylocity uses a combination of RSA and AES cryptography. As part of the setup, each client is issued a public RSA key.rnrnPaylocity recommends the encryption of the incoming requests as additional protection of the sensitive data. Clients can opt-out of the encryption during the initial setup process. Opt-out will allow Paylocity to process unencrypted requests.rnrnThe Paylocity Public Key has the following properties:rnrn* 2048 bit key sizernrn* PKCS1 key formatrnrn* PEM encodingrnrn##### Propertiesrnrn* key (base 64 encoded): The AES symmetric key encrypted with the Paylocity Public Key. It is the key used to encrypt the content. Paylocity will decrypt the AES key using RSA decryption and use it to decrypt the content.rnrn* iv (base 64 encoded): The AES IV (Initialization Vector) used when encrypting the content.rnrn* content (base 64 encoded): The AES encrypted request. The key and iv provided in the secureContent request are used by Paylocity for decryption of the content.rnrnWe suggest using the following for the AES:rnrn* CBC cipher modernrn* PKCS7 paddingrnrn* 128 bit block sizernrn* 256 bit key sizernrn##### Encryption Flowrnrn* Generate the unencrypted JSON payload to POST/PUTrn* Encrypt this JSON payload using your _own key and IV_ (NOT with the Paylocity public key)rn* RSA encrypt the _key_ you used in step 2 with the Paylocity Public Key, then, base64 encode the resultrn* Base64 encode the IV used to encrypt the JSON payload in step 2rn* Put together a "securecontent" JSON object:rn rn{rn  'secureContent' : {rn    'key' : -- RSA-encrypted &amp; base64 encoded key from step 3,rn    'iv' : -- base64 encoded iv from step 4rn    'content' -- content encrypted with your own key from step 2, base64 encodedrn  }rn}rnrnSupport: Questions about using the Paylocity API? Please contact webservices@paylocity.com Deductions (v1): Deductions API provides endpoints to retrieve, add, update and delete deductions for a company's employees. For schema details, click https://docs.paylocity.com/weblink/guides/Paylocity_Web_Services_API/v1/Paylocity_Web_Services_API.htm OnBoarding (v1): Onboarding API sends employee data into Paylocity Onboarding to help ensure an easy and accurate hiring process for subsequent completion into Web Pay. For schema details, click https://docs.paylocity.com/weblink/guides/Paylocity_Web_Services_API/v1/Paylocity_Web_Services_API.htm</description>
	<field id="URL" label="Server URL" type="string">
		<helpText>The URL for the service server</helpText>
		<defaultValue>https://api.paylocity.com</defaultValue>
	</field>
	<field id="SWAGGERURL" label="Paylocity OpenAPI Service Definition URL" type="string">
		<helpText>The URL for the Paylocity API REST API Service server.</helpText>
		<defaultValue>https://api.paylocity.com/api/v2/openapi</defaultValue>
	</field>
	<!--
	<field id="AUTHFORSWAGGERLOAD" label="Use Authentication Loading Swagger" type="boolean">
		<helpText>Select this option if loading a swagger from a URL requires authentication.</helpText>
		<defaultValue/>
	</field>
	-->
	
	<field id="OAUTHOPTIONS" label="OAuth 2.0" type="oauth">
		<helpText>The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider</helpText>
		<oauth2FieldConfig>
			<authorizationTokenEndpoint>
				<url/>
			</authorizationTokenEndpoint>
			<authorizationParameters/>
			<accessTokenEndpoint>
				<url>
					<defaultValue>https://api.paylocity.com/IdentityServer/connect/token</defaultValue>
				</url>
			</accessTokenEndpoint>
			<scope>
				<helpText></helpText>
				<defaultValue>WebLinkAPI</defaultValue>
			</scope>
			<grantType access="hidden"> 
				<defaultValue>client_credentials</defaultValue>
			</grantType>

		</oauth2FieldConfig>
	</field>
	<testConnection method="CUSTOM"/>

	<operation types="EXECUTE" customTypeId="POST" customTypeLabel="CREATE">
		<field id="EXTRA_URL_PARAMETERS" label="Extra URL Query Parameters" type="string" scope="operationOnly">
			<helpText>Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &amp;. For example: extra1=value1&amp;extra2=&amp;value2</helpText>
		</field>
	</operation>

	<operation types="EXECUTE" customTypeId="PUT" customTypeLabel="UPDATE">
		<field id="EXTRA_URL_PARAMETERS" label="Extra URL Query Parameters" type="string" scope="operationOnly">
			<helpText>Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &amp;. For example: extra1=value1&amp;extra2=&amp;value2</helpText>
		</field>
	</operation>

	<operation types="EXECUTE" customTypeId="GET" customTypeLabel="GET">
		<field id="EXTRA_URL_PARAMETERS" label="Extra URL Query Parameters" type="string" scope="operationOnly">
			<helpText>Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &amp;. For example: extra1=value1&amp;extra2=&amp;value2</helpText>
		</field>
	</operation>

	<operation types="EXECUTE" customTypeId="DELETE" customTypeLabel="DELETE">
		<field id="EXTRA_URL_PARAMETERS" label="Extra URL Query Parameters" type="string" scope="operationOnly">
			<helpText>Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &amp;. For example: extra1=value1&amp;extra2=&amp;value2</helpText>
		</field>
	</operation>

	<!-- allowFieldSelection specifies whether the Fields tab appears in the Query Operation -->
	<!-- fieldSelectionLevels specifies the number of levels that appear in the Fields tab. 
	For deep selection where an 'expand' operation is supported, specify a depth greater than 1 -->
	<!-- fieldSelectionNone specifies whether the Fields tab is populated automatically from 
	the fields in the Response Profile for the operation -->
	<operation types="QUERY" allowFieldSelection="false" fieldSelectionLevels="1" fieldSelectionNone="false">
		<field id="PAGESIZE" label="Page Size" type="integer" scope="operationOnly">
			<helpText>Specifies the number of documents to retrieve with each page transaction.</helpText>
			<defaultValue>20</defaultValue>
		</field>
		<field id="MAXDOCUMENTS" label="Maximum Documents" type="integer" scope="operationOnly">
			<helpText>Limits the number of documents returned. If value is less than 1, all records are returned.</helpText>
			<defaultValue>-1</defaultValue>
		</field>
		<field id="EXTRA_URL_PARAMETERS" label="Extra URL Query Parameters" type="string" scope="operationOnly">
			<helpText>Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &amp;. For example: extra1=value1&amp;extra2=&amp;value2</helpText>
		</field>
		<!-- grouping = none | any | noNestingImplicitAnd noNestingImplicitOr singleNestingImplicitOr singleNestingImplicitAnd .... sorting = none | one | unbounded -->
		<!-- Only single operations are allowed. To implement NOT you need 2 explicit operators for both NOT and the positive condition -->
		<queryFilter grouping="noNestingImplicitAnd" sorting="none">
			<operator id="eq" label="Equal To"/>  
		</queryFilter>
	</operation>
	<!-- Inbound Dynamic Document Properties -->
	<dynamicProperty id="EXTRA_URL_PARAMETERS" label="Extra URL Query Parameters" type="string">
		<helpText>Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &amp;. For example: extra1=value1&amp;extra2=&amp;value2</helpText>
	</dynamicProperty>

	<!-- Outbound Dynamic Document Properties -->
	<!-- 
	<trackedProperty id="ETAG" label="ETag/Document Version Number (Returned by GET operations)">
		<helpText>The E-Tag represents the current version of the entity/document</helpText>
	</trackedProperty>
	 -->
</GenericConnectorDescriptor>